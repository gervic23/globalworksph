<?php

namespace App\Controller;
use Cake\Event\Event;

/*
*   Ajax Functionalities for Admin Pages.
*/
class AdminajaxController extends AppController {

    private $globalworks;
    
    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        
        $this->globalworks = new GlobalworksController();
        
        $this->Auth->allow(['init']);
    }

    public function init() {

        if ($this->request->is('post') && $this->request->isAjax()) {
            $operation = $this->request->data('operation');
        
            //Disable views.
            $this->autoRender = false;
            $this->viewBuilder()->setLayout(false);
            
            switch ($operation) {

                /*
                *   Count candidates/applicants and employers.
                *
                *   Associated Controller => AdminsController,
                *   Method => dashboard.
                */
                case 'users_count':

                    //Load Home Model.
                    $this->loadModel('Home');

                    //Query and count users.
                    $users_count = $candidates_count = $this->Home->find()
                        ->where(['role' => 'Applicant'])
                        ->orWhere(['role' => 'Employer'])
                        ->count();

                    echo $users_count;
                break;

                /*
                *   Count the number of applicants/candidates.
                *
                *   Associated Controller => AdminsController,
                *   Method => dashboard.
                */
                case 'candidates_count':

                    //Load Home Model.
                    $this->loadModel('Home');

                    //Query and count candidates.
                    $candidates_count = $this->Home->find()
                        ->where(['role' => 'Applicant'])
                        ->count();

                    echo $candidates_count;
                break;
                
                /*
                *   Count the number of employers.
                *
                *   Associated Controller => AdminsController,
                *   Method => dashboard.
                */
                case 'employers_count':

                    //Load Home Model.
                    $this->loadModel('Home');

                    //Query and count employers.
                    $employers_count =  $this->Home->find()
                        ->where(['role' => 'Employer'])
                        ->count();

                    echo $employers_count;
                break;

                /*
                *   Count the number of active jobposts.
                *
                *   Associated Controller => AdminsController,
                *   Method => dashboard.
                */
                case 'jobposts_active_count':

                    //Load Jobposts Model.
                    $this->loadModel('Jobposts');

                    //Query and count active jobposts.
                    $jobposts_count = $this->Jobposts->find()
                        ->where(['closing_date >=' => date( "Y-m-d H:i:s" )])
                        ->count();

                    echo $jobposts_count;
                break;

                /*
                *   Count the number of inactive jobposts.
                *
                *   Associated Controller => AdminsController,
                *   Method => dashboard.
                */
                case 'jobposts_inactive_count':

                    //Load Jobposts Model.
                    $this->loadModel('Jobposts');

                    //Query and count active jobposts.
                    $jobposts_count = $this->Jobposts->find()
                        ->where(['closing_date <' => date( "Y-m-d H:i:s" )])
                        ->count();

                    echo $jobposts_count;
                break;

                /*
                *   Count all visitor's hits.
                *
                *   Associated Controller => AdminsController,
                *   Method => dashboard.
                */
                case 'visitor_stats':

                    //Load Ip_logs Model.
                    $this->loadModel('Ip_logs');

                    //Query and Count all visitor's hits.
                    $logs = $this->Ip_logs->find()
                        ->select(['hits']);

                    //Value of Hits.
                    $hits = 0;

                    //Loop Results.
                    foreach ($logs as $log) {
                        $hits += $log['hits'];
                    }

                    echo $hits;
                break;

                /*
                *   Count unuque visitors.
                *
                *   Associated Controller => AdminsController,
                *   Method => dashboard.
                */
                case 'visitors_unique':

                    //Load Ip_logs Model.
                    $this->loadModel('Ip_logs');

                    //Query and Count Unique visitors.
                    $logs = $this->Ip_logs->find()
                        ->count();

                    echo $logs;
                break;

                /*
                *   Manage Users Pagination Numbers.
                *
                *   Associated Controller => AdminsController,
                *   Method => users.
                */
                case 'users_list_pagination':

                    //Page Number.
                    $page = $this->request->data('page');

                    //User's Role.
                    $role = $this->request->data('role');

                    //Max Results set.
                    $max_results = $this->request->data('max_results');

                    //Load Home Model.
                    $this->loadModel('Home');

                    //Count Results.
                    $users_count = $this->Home->find()
                        ->where(['role' => $role])
                        ->count();
                        
                    //Pagination Pages.
                    $pages = ceil($users_count / $max_results);

                    //Ajax Response.
                    $response = [
                        'current_page' => $page,
                        'pages' => $pages,
                        'users_count' => $users_count,
                    ];

                    //Print Response.
                    echo json_encode($response);
                break;
                
                /*
                *   Manage Users Pagination Numbers (Search).
                *
                *   Associated Controller => AdminsController,
                *   Method => users.
                */
                case 'users_list_pagination_search':

                    //Page Number.
                    $page = $this->request->data('page');

                    //User's Role.
                    $role = $this->request->data('role');

                    //Max Results set.
                    $max_results = $this->request->data('max_results');

                    //Search Keywords.
                    $keywords = $this->request->data('keywords');

                    //Explode keywords to array via spaces.
                    $k_words = explode(" ", $keywords);

                    //Load Home Model.
                    $this->loadModel('Home');

                    //Query to fetch results.
                    $users = $this->Home->find();
                    $users->contain(['Profiles']);
                    $users->where(['home.role' => $role]);

                    $i = 0;

                    foreach ($k_words as $keyword) {
                        $i++;

                        if ($i == 1) {
                            $users->andWhere(['home.id LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['home.email LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['home.firstname LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['home.lastname LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['home.gender LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['home.lastname LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['home.birthday LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['home.address LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['home.region LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['home.country LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['home.contact_number LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['profiles.nickname LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['profiles.job_title LIKE' => '%' . $keyword . '%']);

                            if ($role == 'Applicant') {
                                $users->orWhere(['profiles.nickname LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.skills LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.desire_salary LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.education LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.experience LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.employment_status LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.skype_id LIKE' => '%' . $keyword . '%']);
                            } else if ($role == 'Employer') {
                                $users->orWhere(['profiles.business_name LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.business_description LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.business_email LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.business_contact_number LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.business_website LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.business_address LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.business_state LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.business_country LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.job_title LIKE' => '%' . $keyword . '%']);
                            }
                        } else {
                            $users->orWhere(['home.id LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['home.email LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['home.firstname LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['home.lastname LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['home.gender LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['home.lastname LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['home.birthday LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['home.address LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['home.region LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['home.country LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['home.contact_number LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['profiles.nickname LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['profiles.job_title LIKE' => '%' . $keyword . '%']);

                            if ($role == 'Applicant') {
                                $users->orWhere(['profiles.nickname LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.skills LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.desire_salary LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.education LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.experience LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.employment_status LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.skype_id LIKE' => '%' . $keyword . '%']);
                            } else if ($role == 'Employer') {
                                $users->orWhere(['profiles.business_name LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.business_description LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.business_email LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.business_contact_number LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.business_website LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.business_address LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.business_state LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.business_country LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.job_title LIKE' => '%' . $keyword . '%']);
                            }
                        }
                    }

                    $users_count = 0;

                    foreach ($users as $user) {
                        if ($user['role'] == $role) {
                            $users_count += count($user);
                        }
                    }

                    //Pagination Pages.
                    $pages = ceil($users_count / $max_results);

                    //Ajax Response.
                    $response = [
                        'current_page' => $page,
                        'pages' => $pages,
                        'users_count' => $users_count,
                        'keywords' => $keywords,
                    ];

                    //Print Response.
                    echo json_encode($response);
                break;
                
                /*
                *   Manage Users show users information.
                *
                *   Associated Controller => AdminsController,
                *   Method => users.
                */
                case 'users_list':
                    
                    //Load Home Model.
                    $this->loadModel('Home');

                    //Current_page number.
                    $current_page = $this->request->data('current_page');

                    //Max Results Number.
                    $max_results = $this->request->data('max_results');

                    //User's Role.
                    $role = $this->request->data('role');

                    //Queryout to get users lists.
                    $users = $this->Home->find()
                        ->select(['home.id', 'home.email', 'home.firstname', 'home.lastname', 'home.suspend', 'profiles.profile_picture'])
                        ->contain(['Profiles'])
                        ->where(['home.role' => $role])
                        ->order(['home.id' => 'DESC'])
                        ->limit($max_results)
                        ->page($current_page);

                    //Enable View.
                    $this->autoRender = true;
                    $this->viewBuilder()->template('users_list');

                    $this->set('users', $users);
                break;
                
                /*
                *   View User's Information.
                */
                case 'user_view':

                    //Load Home Model.
                    $this->loadModel('Home');
                    
                    //User's ID.
                    $id = $this->request->data('id');

                    //Check if User's ID is exists.
                    $user_check = $this->Home->find()
                        ->where(['id' => $id])
                        ->count();

                    //If User's ID is existed.
                    if ($user_check > 0) {
                        
                        //Query out to get User's Information.
                        $users = $this->Home->find()
                            ->select([
                                'home.id', 
                                'home.email',
                                'home.role', 
                                'home.status', 
                                'home.firstname', 
                                'home.lastname', 
                                'home.gender', 
                                'home.birthday', 
                                'home.address', 
                                'home.region', 
                                'home.country', 
                                'home.contact_number', 
                                'home.confirmation_code', 
                                'home.date_created', 
                                'home.current_ip', 
                                'home.registered_ip', 
                                'home.last_login', 
                                'home.fb_user', 
                                'home.employer_review', 
                                'home.suspend', 
                                'home.suspend_time',
                                'profiles.profile_picture',
                                'profiles.nickname',
                                'profiles.job_title',
                                'profiles.skills',
                                'profiles.desire_salary',
                                'profiles.education',
                                'profiles.experience',
                                'profiles.employment_status',
                                'profiles.skype_id',
                                'profiles.job_subscribe',
                                'profiles.business_name',
                                'profiles.business_description',
                                'profiles.business_email',
                                'profiles.business_contact_number',
                                'profiles.business_address',
                                'profiles.business_state',
                                'profiles.business_country',
                                'resumes.id',
                                'resumes.filename',
                                'resumes.orig_filename',
                            ])
                            ->contain(['Profiles', 'Resumes'])
                            ->where(['home.id' => $id]);

                        //Enable View.
                        $this->autoRender = true;

                        //Load Custom Template.
                        $this->viewBuilder()->template('user_view');

                        $this->set('users', $users);
                    }
                break;
                
                /*
                *   Manage Users show users information (Search).
                *
                *   Associated Controller => AdminsController,
                *   Method => users.
                */
                case 'users_list_search':
                    
                    //Load Home Model.
                    $this->loadModel('Home');

                    //Current_page number.
                    $current_page = $this->request->data('current_page');

                    //Max Results Number.
                    $max_results = $this->request->data('max_results');

                    //User's Role.
                    $role = $this->request->data('role');

                    //Keywords.
                    $keywords = $this->request->data('keywords');

                    //Explode Keywords to array via spaces.
                    $k_words = explode(" ", $keywords);

                    //Queryout Search Results.
                    $users = $this->Home->find();
                    $users->select(['home.id', 'home.email', 'home.firstname', 'home.lastname', 'home.role', 'profiles.profile_picture']);
                    $users->contain(['Profiles']);
                    $users->where(['home.role' => $role]);

                    $i = 0;

                    foreach ($k_words as $keyword) {
                        $i++;

                        if ($i == 1) {
                            $users->andWhere(['home.id LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['home.email LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['home.firstname LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['home.lastname LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['home.gender LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['home.lastname LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['home.birthday LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['home.address LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['home.region LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['home.country LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['home.contact_number LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['profiles.nickname LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['profiles.job_title LIKE' => '%' . $keyword . '%']);

                            if ($role == 'Applicant') {
                                $users->orWhere(['profiles.nickname LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.skills LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.desire_salary LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.education LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.experience LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.employment_status LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.skype_id LIKE' => '%' . $keyword . '%']);
                            } else if ($role == 'Employer') {
                                $users->orWhere(['profiles.business_name LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.business_description LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.business_email LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.business_contact_number LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.business_website LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.business_address LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.business_state LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.business_country LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.job_title LIKE' => '%' . $keyword . '%']);
                            }
                        } else {
                            $users->orWhere(['home.id LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['home.email LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['home.firstname LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['home.lastname LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['home.gender LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['home.lastname LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['home.birthday LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['home.address LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['home.region LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['home.country LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['home.contact_number LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['profiles.nickname LIKE' => '%' . $keyword . '%']);
                            $users->orWhere(['profiles.job_title LIKE' => '%' . $keyword . '%']);

                            if ($role == 'Applicant') {
                                $users->orWhere(['profiles.nickname LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.skills LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.desire_salary LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.education LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.experience LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.employment_status LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.skype_id LIKE' => '%' . $keyword . '%']);
                            } else if ($role == 'Employer') {
                                $users->orWhere(['profiles.business_name LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.business_description LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.business_email LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.business_contact_number LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.business_website LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.business_address LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.business_state LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.business_country LIKE' => '%' . $keyword . '%']);
                                $users->orWhere(['profiles.job_title LIKE' => '%' . $keyword . '%']);
                            }
                        }
                    }

                    $users->order(['home.id' => 'DESC']);
                    $users->limit($max_results);
                    $users->page($current_page);

                    //Enable View.
                    $this->autoRender = true;
                    $this->viewBuilder()->template('users_list_search');

                    $this->set('users', $users);
                    $this->set('role', $role);
                break;
                
                /*
                *   Edit User Information.
                *
                *   Associated Controller => AdminsController,
                *   Method => users.
                */
                case 'user_edit':

                    //Load Home Model.
                    $this->loadModel('Home');

                    //User's ID.
                    $id = $this->request->data('id');

                    //Check if user's ID exists.
                    $users_count = $this->Home->find()
                        ->where(['id' => $id])
                        ->count();

                    //If user's ID exists.
                    if ($users_count > 0) {

                        //Query out user's information.
                        $users = $this->Home->find()
                            ->select([
                                'id',
                                'email',
                                'role', 
                                'status', 
                                'firstname', 
                                'lastname', 
                                'gender', 
                                'birthday', 
                                'address', 
                                'region', 
                                'country', 
                                'contact_number', 
                            ])
                            ->where(['id' => $id]);

                        //Enable View.
                        $this->autoRender = true;
                        $this->viewBuilder()->template('users_edit');

                        $this->set('users', $users);
                    }
                break;
                
                /*
                *   Save/Update User Information.
                *
                *   Associated Controller => AdminsController,
                *   Method => users.
                */
                case 'user_info_save':
                    
                    //Load Home Model.
                    $this->loadModel('Home');

                    //User's Row Record.
                    $user = $this->Home->get($this->request->data('id'));
                    
                    //Update Record.
                    $user->firstname = $this->request->data('firstname');
                    $user->lastname = $this->request->data('lastname');
                    
                    //If Password is not empty, update password.
                    if ($this->request->data('password')) {
                        $user->password = $this->request->data('password');
                    }

                    $user->birthday = $this->request->data('birthday');
                    $user->address = $this->request->data('address');
                    $user->region = $this->request->data('region');
                    $user->country = $this->request->data('country');
                    $user->contact_number = $this->request->data('contact_number');

                    //Save Record.
                    if ($this->Home->save($user)) {
                        
                        //If has user notification message.
                        if ($this->request->data('notify')) {

                            //Load Messages Model.
                            $this->loadModel('Messages');

                            //New Entity.
                            $messages = $this->Messages->newEntity();

                            //Message Data.
                            $data = [
                                'user_id' => $this->request->data('id'),
                                'sender_id' => $this->request->session()->read('Auth.User')['id'],
                                'unread' => 1,
                                'sent_item' => 0,
                                'message_type' => 'employer_msg_from_admin',
                                'subject' => 'Your Account Information has been modified.',
                                'message' => $this->request->data('notify_msg'),
                                'date' => date( "Y-m-d H:i:s" ),
                            ];

                            //Patch Entity.
                            $messages = $this->Messages->patchEntity($messages, $data);

                            //Send Message.
                            if ($this->Messages->save($messages)) {
                                
                                /*
                                *   Create Log Record.
                                */

                                //Admin's Full Name.
                                $admin_fullname = $this->request->session()->read('Auth.User')['firstname'] . ' ' . $this->request->session()->read('Auth.User')['lastname'];

                                //User's Fullname.
                                $user_fullname = $this->globalworks->getfirstname($this->request->data('user_id')) . ' ' . $this->globalworks->getlastname($this->request->data('user_id'));

                                //User's Email Address.
                                $email = $this->request->session()->read('Auth.User')['email'];

                                //Create Log Message.
                                $log_msg = $admin_fullname . ' update ' . $user_fullname . '\'s account details.';

                                //Save Log.
                                $this->globalworks->_log('admin_user_details_change', $log_msg);

                                
                                /*
                                *   Send Email Message to the user
                                *   and get the user's email address.
                                */

                                //Load Home Model.
                                $this->loadModel('Home');

                                //User's Email Value.
                                $user_email = '';

                                //Query to get email.
                                $home = $this->Home->find()
                                    ->select(['email'])
                                    ->where(['id' => $this->request->data('id')]);

                                //Loop to get Email.
                                foreach ($home as $user) {
                                    $user_email = $user['email'];
                                }

                                /*
                                *   Create Email Message.
                                */
                                $from = [$this->globalworks->_server_email_address, 'Globalworks'];
                                $to = $user_email;
                                $subject = 'Your account information has been modified';
                                $message = '<p>Your account information has been modified.</p> <p>Check your message panel.</p> <p><a href="' . $this->globalworks->_url . '/login">Click Here</a> to login.</p>';
                                
                                //Email Data.
                                $email_contents = [
                                    'from' => $from,
                                    'to' => $to,
                                    'subject' => $subject,
                                    'html' => true,
                                    'message' => $message,
                                ];

                                //Send Email Message.
                                if ($this->globalworks->mail($email_contents)) {
                                    echo 'ok';
                                }

                                echo 'ok';
                            }
                        }
                    }
                break;
                
                /*
                *   Edit User Prfofile.
                *
                *   Associated Controller => AdminsController,
                *   Method => users.
                */
                case 'user_edit_profile':
                    
                    //Load Profiles Model.
                    $this->loadModel('Profiles');

                    //Queryout to get user's profile.
                    $profiles = $this->Profiles->find()
                        ->select([
                            'profile_picture',
                            'nickname',
                            'job_title',
                            'skills',
                            'desire_salary',
                            'education',
                            'experience',
                            'employment_status',
                            'skype_id',
                            'job_subscribe',
                            'business_name',
                            'business_description',
                            'business_email',
                            'business_contact_number',
                            'business_website',
                            'business_address',
                            'business_state',
                            'business_country',
                        ])
                        ->where(['user_id' => $this->request->data('id')]);

                        //Enable View.
                        $this->autoRender = true;
                        $this->viewBuilder()->template('user_edit_profile');

                        //Insert profile values in the view.
                        foreach ($profiles as $profile) {
                            $this->set('profile_picture', $profile['profile_picture']);
                            $this->set('nickname', $profile['nickname']);
                            $this->set('job_title', $profile['job_title']);
                            $this->set('skills', $profile['skills']);
                            $this->set('desire_salary', $profile['desire_salary']);
                            $this->set('education', $profile['education']);
                            $this->set('experience', $profile['experience']);
                            $this->set('employment_status', $profile['employment_status']);
                            $this->set('skype_id', $profile['skype_id']);
                            $this->set('job_subscribe', $profile['job_subscribe']);
                            $this->set('business_name', $profile['business_name']);
                            $this->set('business_description', $profile['business_description']);
                            $this->set('business_email', $profile['business_email']);
                            $this->set('business_contact_number', $profile['business_contact_number']);
                            $this->set('business_website', $profile['business_website']);
                            $this->set('business_address', $profile['business_address']);
                            $this->set('business_state', $profile['business_state']);
                            $this->set('business_country', $profile['business_country']);
                        }

                        $this->set('user_id', $this->request->data('id'));
                        $this->set('role', $this->request->data('role'));
                break;

                /*
                *   Save User Prfofile.
                *
                *   Associated Controller => AdminsController,
                *   Method => users.
                */
                case 'user_profile_save':
                    
                    //Load Profiles Model.
                    $this->loadModel('Profiles');

                    //Check if User has already a profile record or not.
                    $profiles_count = $this->Profiles->find()
                        ->where(['user_id' => $this->request->data('user_id')])
                        ->count();

                    //If Profile record exists.
                    if ($profiles_count > 0) {
                        
                        //Profile Row ID.
                        $id = 0;

                        //Query out to get Profile ID.
                        $profiles = $this->Profiles->find()
                            ->select(['id'])
                            ->where(['user_id' => $this->request->data('user_id')]);
                        

                        //Loop results.
                        foreach ($profiles as $profile) {

                            //Append ID.
                            $id = $profile['id'];
                        }

                        //Get User's Profile record row.
                        $profile = $this->Profiles->get($id);

                        //Upate records.
                        $profile->nickname = $this->request->data('nickname');
                        $profile->job_title = $this->request->data('job_title');
                        $profile->skills = $this->request->data('skills');
                        $profile->desire_salary = $this->request->data('desire_salary');
                        $profile->education = $this->request->data('education');
                        $profile->experience = $this->request->data('experience');
                        $profile->skype_id = $this->request->data('skype_id');
                        $profile->employment_status = $this->request->data('employment_status');
                        $profile->job_subscribe = $this->request->data('job_subscribe');
                        $profile->business_name = $this->request->data('business_name');
                        $profile->business_description = $this->request->data('business_description');
                        $profile->business_email = $this->request->data('business_email');
                        $profile->business_contact_number = $this->request->data('business_contact_number');
                        $profile->business_website = $this->request->data('business_website');
                        $profile->business_address = $this->request->data('business_address');
                        $profile->business_state = $this->request->data('business_state');
                        $profile->business_country = $this->request->data('business_country');

                        //Save Record.
                        if ($this->Profiles->save($profile)) {
                            
                            /*
                            *   Create Log Record.
                            */

                            //Admin's Full Name.
                            $admin_fullname = $this->request->session()->read('Auth.User')['firstname'] . ' ' . $this->request->session()->read('Auth.User')['lastname'];

                            //User's Fullname.
                            $user_fullname = $this->globalworks->getfirstname($this->request->data('user_id')) . ' ' . $this->globalworks->getlastname($this->request->data('user_id'));

                            //User's Email Address.
                            $email = $this->request->session()->read('Auth.User')['email'];

                            //Create Log Message.
                            $log_msg = $admin_fullname . ' update ' . $user_fullname . '\'s profile.';

                            //Save Log.
                            $this->globalworks->_log('admin_user_profile_change', $log_msg);

                            
                            /*
                            *   Send user notification via messages.
                            */
                            if ($this->request->data('has_notification')) {
                                $this->globalworks->profile_user_notification($this->request->data('user_id'), $this->request->data('notification_msg'));

                                echo 'ok';
                            }

                            echo 'ok';
                        }
                    } else {
                        
                        //New Entity.
                        $profiles = $this->Profiles->newEntity();

                        //Values to Patch.
                        $new_entity = [
                            'user_id' => $this->request->data('user_id'),
                            'nickname' => $this->request->data('nickname'),
                            'job_title' => $this->request->data('job_title'),
                            'experience' => $this->request->data('experience'),
                            'skills' => $this->request->data('skills'),
                            'skype_id' => $this->request->data('skype_id'),
                            'desire_salary' => $this->request->data('desire_salary'),
                            'education' => $this->request->data('education'),
                            'employment_status' => $this->request->data('employment_status'),
                            'job_subscribe' => $this->request->data('job_subscribe'),
                            'business_name' => $this->request->data('business_name'),
                            'business_description' => $this->request->data('business_description'),
                            'business_contact_number' => $this->request->data('business_contact_number'),
                            'business_website' => $this->request->data('business_website'),
                            'business_address' => $this->request->data('business_address'),
                            'business_state' => $this->request->data('business_state'),
                            'business_country' => $this->request->data('business_country'),
                        ];

                        //Patch Entity.
                        $profiles = $this->Profiles->patchEntity($profiles, $new_entity);

                        //Save Record.
                        if ($this->Profiles->save($profiles)) {
                            
                            /*
                            *   Create Log Record.
                            */

                            //Admin's Full Name.
                            $admin_fullname = $this->request->session()->read('Auth.User')['firstname'] . ' ' . $this->request->session()->read('Auth.User')['lastname'];

                            //User's Fullname.
                            $user_fullname = $this->globalworks->getfirstname($this->request->data('user_id')) . ' ' . $this->globalworks->getlastname($this->request->data('user_id'));

                            //User's Email Address.
                            $email = $this->request->session()->read('Auth.User')['email'];

                            //Create Log Message.
                            $log_msg = $admin_fullname . ' update ' . $user_fullname . '\'s profile.';

                            //Save Log.
                            $this->globalworks->_log('admin_user_profile_change', $log_msg);
                            
                            /*
                            *   Send user notification via messages.
                            */
                            if ($this->request->data('has_notification')) {
                                $this->globalworks->profile_user_notification($this->request->data('user_id'), $this->request->data('notification_msg'));
                            
                                echo 'ok';
                            }

                            echo 'ok';
                        }
                    }
                break;
                
                /*
                *   Upload Profile Picture Page.
                *
                *   Associated Controller => AdminsController,
                *   Method => users.
                */
                case 'user_upload_profile_pic':
                    
                    //Enable View.
                    $this->autoRender = true;
                    $this->viewBuilder()->template('user_upload_profile_picture');
                    
                    $this->set('id', $this->request->data('id'));
                break;

                case 'user_upload_pic':

                    //Load Model.
                    $this->loadModel('Imgtmp');

                    //Filename.
                    $name = $size = $_FILES['ddu-upload']['name'];

                    //File Size.
                    $size = $_FILES['ddu-upload']['size'];

                    //File tmp.
                    $tmp = $_FILES['ddu-upload']['tmp_name'];

                    //Upload Error.
                    $error = $_FILES['ddu-upload']['error'];

                    //Path to upload directory.
                    $path = './img/users/tmp/';

                    if (!$error) {

                        //Get the file extension name.
                        $ext = explode('.', $name);
                        $ext = end($ext);
                        
                        //Random name value.
                        $random_name = '';
                        
                        //Characters to generate.
                        $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                        
                        //Generate random strings.
                        for ($p = 0; $p < 16; $p++) {
                            $random_name .= $chars[mt_rand(0, strlen($chars)-1)];
                        }
                        
                        //Random name for filename.
                        $random_name = $this->request->data('id') . date( "YmdHis" ) . $random_name;

                        //Count user's img_tmp record if exists or not.
                        $imgtmp_count = $this->Imgtmp->find()
                            ->where(['user_id' => $this->request->data('id')])
                            ->count();

                        //If user's img_tmp record not exists, create record.
                        if ($imgtmp_count == 0) {

                            //Move Uploaded file to /img/users/tmp directory.
                            if (move_uploaded_file($tmp, $path . $random_name . '.' . $ext)) {
                                
                                //New Entity.
                                $imgtmp = $this->Imgtmp->newEntity();
                                
                                //Records to be insert in img_tmp database table.
                                $new_entity = [
                                    'user_id' => $this->request->data('id'),
                                    'filename' => $random_name . '.' . $ext,
                                    'date' => date( "Y-m-d H:i:s" ),
                                ];
                                
                                //Patch Entity.
                                $imgtmp = $this->Imgtmp->patchEntity($imgtmp, $new_entity);
                                
                                //Create or Insert new Record.
                                if ($this->Imgtmp->save($imgtmp)) {
                                    echo 'ok';
                                }
                            }

                        //If User img_tmp record exists. Update record.
                        } else {

                            //Value of img_tmp id to append.
                            $id = '';
                            
                            //Value of image filename to append.
                            $old_img = '';
                            
                            //Get User's img_tmp record.
                            $imgtmps = $this->Imgtmp->find()
                                ->where(['user_id' => $this->request->data('id')]);
                            
                            //Loop User's Record.
                            foreach ($imgtmps as $t) {
                                
                                //Append ID.
                                $id .= $t['id'];
                                
                                //Append Filename.
                                $old_img .= $t['filename'];
                            }
                            
                            //File destination directory and filename.
                            $file = $path . $old_img;
                            
                            //Get User's row record from img_tmp table.
                            $imgtmp = $this->Imgtmp->get($id);
                            
                            //Update filename record.
                            $imgtmp->filename = $random_name . '.' . $ext;
                            
                            //Update date record.
                            $imgtmp->date = date( "Y-m-d H:i:s" );
                            
                            //Save Changes.
                            if ($this->Imgtmp->save($imgtmp)) {
                                
                                /*
                                *   Delete old uploaded image and move new uploaded image.
                                */
                                
                                //If old image file exists.
                                if (file_exists($file)) {
                                    
                                    //Delete old file image.
                                    if (unlink($file)) {
                                        
                                        //Move the new one to the tmp directory.
                                        if (move_uploaded_file($tmp, $path . $random_name . '.' . $ext)) {
                                            echo 'ok';
                                        }
                                    }
                                }
                            }
                        }
                    }
                break;
                
                /*
                *   Crop Profile Picture Page.
                *
                *   Associated Controller => AdminsController,
                *   Method => users.
                */
                case 'user_image_crop':

                    //Load Model.
                    $this->loadModel('Imgtmp');

                    //User's Session ID.
                    $user_id = $this->request->data('id');

                    //Get User's img_tmp record.
                    $imgtmp = $this->Imgtmp->find()
                        ->where(['user_id' => $user_id]);
                
                    //Filename val to append.
                    $filename = '';
                    
                    //Loop img_tmp results.
                    foreach ($imgtmp as $img) {
                        
                        //Append Filename.
                        $filename .= $img['filename'];
                    }
                    
                    //Enable View.
                    $this->autoRender = true;
                    $this->viewBuilder()->template('user_crop_image');

                    $this->set('user_id', $user_id);
                    $this->set('filename', $filename);
                break;
                
                /*
                *   Crop Profile Picture.
                *
                *   Associated Controller => AdminsController,
                *   Method => users.
                */
                case 'crop_image':
                    
                    //User's Session ID from post request.
                    $user_id = $this->request->data('user_id');
                    
                    //Original filename of image from tmp folder.
                    $filename_orig = $this->request->data('filename');
                    
                    //File format type.
                    $type = $_FILES['cropperImage']['type'];
                    
                    //File upload error.
                    $error = $_FILES['cropperImage']['error'];
                    
                    //The Image File.
                    $tmp = $_FILES['cropperImage']['tmp_name'];
                    
                    //Path to /img/profile_picrutes/ directory.
                    $profile_picture_path = './img/users/profile_pictures/';
                    
                    //Path to /img/profile_thumbnails/ directory.
                    $thumbnail_picture_path = './img/users/profile_thumbnails/';
                    
                    //Get the filename w/o dot extension.
                    $filename = explode('.', $filename_orig);
                    $f_name = $filename[0];
                    
                    //File image cropped extension name.
                    $ext = '.jpg';
                    
                    //The new filename of image cropped.
                    $new_filename = $f_name . $ext;
                    
                    //If mo upload error.
                    if ($error == 0) {
                        
                        /*
                        *   Resizing image.
                        */
                        
                        //Type of image.
                        switch ($type) {
                                
                            //If image is jpeg format.
                            case 'image/jpeg':
                                
                                //Get the image dimensions.
                                list($width, $height) = getimagesize($tmp);
                                
                                /*
                                *   For Dimension 370x370.
                                */
                                //Set a new dimension.
                                $image = imagecreatetruecolor(370, 370);
                                
                                //Get the source file.
                                $image_src = imagecreatefromjpeg($tmp);
                                
                                //Initiating resizing image.
                                imagecopyresampled($image, $image_src, 0, 0, 0, 0, 370, 370, $width, $height);
                                
                                //Convert to jpeg and move to destination path.
                                imagejpeg($image, $profile_picture_path . $new_filename);
                                
                                /*
                                *   For Dimension 70x70.
                                */
                                //Set a new dimension.
                                $thumb_image = imagecreatetruecolor(70, 70);
                                
                                //Get the source file.
                                $thumb_image_src = imagecreatefromjpeg($tmp);
                                
                                //Initiating resizing image.
                                imagecopyresampled($thumb_image, $thumb_image_src, 0, 0, 0, 0, 70, 70, $width, $height);
                                
                                //Convert to jpeg and move to destination path.
                                imagejpeg($thumb_image, $thumbnail_picture_path . $new_filename);
                            break;
                            
                            //If image is jpeg format.
                            case 'image/png':
                                
                                //Get the image dimensions.
                                list($width, $height) = getimagesize($tmp);
                                
                                /*
                                *   For Dimension 370x370.
                                */
                                //Set a new dimension.
                                $image = imagecreatetruecolor(370, 370);
                                
                                //Get the source file.
                                $image_src = imagecreatefrompng($tmp);
                                
                                //Initiating resizing image.
                                imagecopyresampled($image, $image_src, 0, 0, 0, 0, 370, 370, $width, $height);
                                
                                //Convert to jpeg and move to destination path.
                                imagejpeg($image, $profile_picture_path . $new_filename);
                                
                                /*
                                *   For Dimension 70x70.
                                */
                                //Set a new dimension.
                                $thumb_image = imagecreatetruecolor(70, 70);
                                
                                //Get the source file.
                                $thumb_image_src = imagecreatefrompng($tmp);
                                
                                //Initiating resizing image.
                                imagecopyresampled($thumb_image, $thumb_image_src, 0, 0, 0, 0, 70, 70, $width, $height);
                                
                                //Convert to jpeg and move to destination path.
                                imagejpeg($thumb_image, $thumbnail_picture_path . $new_filename);
                            break;
                        }
                        
                        /*
                        *   Checking if User's has a Profile record.
                        *   If User has no profile record create one.
                        *   If User has already a profile record, set record to profile_picture column.
                        *   If User Profile profile_picture is already set, update record and remove old images and replace with the new cropped images.
                        */
                        
                        //Load Model.
                        $this->loadModel('Profiles');
                        
                        //Count User's Profile record if exists or not.
                        $profiles_count = $this->Profiles->find()
                            ->where(['user_id' => $user_id])
                            ->count();
                        
                        //If User's Profile record does not exists, create record.
                        if ($profiles_count == 0) {
                            
                            //New Entity.
                            $profiles = $this->Profiles->newEntity();
                            
                            //Records to be insert in Profiles database table.
                            $new_entity = [
                                'user_id' => $user_id,
                                'profile_picture' => $new_filename,
                            ];
                            
                            //Patch Entity.
                            $profiles = $this->Profiles->patchEntity($profiles, $new_entity);
                            
                            //Create or Insert Record to the Profiles Table.
                            if ($this->Profiles->save($profiles)) {
                                
                                //Delete User's img_tmp record and temporary image from tmp directory.
                                if ($this->globalworks->deletetmpimg($user_id, $filename_orig)) {
                                    echo 'ok';
                                }
                            }
                            
                        //If User's Profile Record is Exists, Update Record.
                        } else {
                            
                            //Get User's Profile record.
                            $profiles = $this->Profiles->find()
                                ->where(['user_id' => $user_id]);
                            
                            //User's Profile ID row to append.
                            $id = '';
                            
                            //User's profile_picture value to append.
                            $profile_picture = '';
                            
                            //Loop User's Profile results.
                            foreach ($profiles as $pro) {
                                
                                //Append ID.
                                $id .= $pro['id'];
                                
                                //Append profile_picture.
                                $profile_picture .= $pro['profile_picture'];
                            }
                            
                            //Get User's Profile Row.
                            $profile = $this->Profiles->get($id);
                            
                            //If profile_picture is null or empty.
                            if ($profile_picture == '') {
                                
                                //Update profile_picture record.
                                $profile->profile_picture = $new_filename;                           
                                
                                //Save Changes.
                                if ($this->Profiles->save($profile)) {
                                    
                                    //Delete User's img_tmp record and delete image from tmp directory.
                                    if ($this->globalworks->deletetmpimg($user_id, $filename_orig)) {
                                        echo 'ok';
                                    }
                                }
                            
                            //If profile_picture is not empty or not null.
                            } else {
                                
                                //Update Record and Delete old profile pictures.
                                if ($this->globalworks->delete_exisiting_profile_picture($user_id, $profile_picture, $new_filename)) {
                                    
                                    //Update profile_picture record.
                                    $profile->profile_picture = $new_filename;
                                    
                                    //Delete User's img_tmp record and temporary image from tmp directory.
                                    if ($this->globalworks->deletetmpimg($user_id, $filename_orig)) {
                                        echo 'ok';
                                    }
                                }
                            }
                        }
                    }
                break;
                
                /*
                *   Delete User's Profile Picture.
                *
                *   Associated Controller => AdminsController,
                *   Method => users.
                */
                case 'user_delete_profile_pic':
                    
                    //Load Profiles Model
                    $this->loadModel('Profiles');

                    //Value to append.
                    $id = '';

                    //Value to append.
                    $filename = '';

                    //Query out to get user's Profile record.
                    $profile = $this->Profiles->find()
                        ->select(['id', 'profile_picture'])
                        ->where(['user_id' => $this->request->data('user_id')]);

                    //Loop Result.
                    foreach ($profile as $pro) {
                        $id .= $pro['id'];
                        $filename .= $pro['profile_picture'];
                    }

                    //Path to profile_pictures dir.
                    $pp_dir = './img/users/profile_pictures/';

                    //Path to profile_thumbnails dir.
                    $thumb_dir = './img/users/profile_thumbnails/';

                    //Check if Profile Picture is exists.
                    if (file_exists($pp_dir . $filename)) {
                        unlink($pp_dir . $filename);
                    }

                    //Check if Profile Thumbnail is exist.
                    if (file_exists($thumb_dir . $filename)) {
                        unlink($thumb_dir . $filename);
                    }

                    /*
                    *   Update User's Profile profile_picture column to empty content.
                    */

                    //Get User's Column Record.
                    $u_profile = $this->Profiles->get($id);

                    //Update profile_picture column.
                    $u_profile->profile_picture = '';

                    //Save User's Record.
                    if ($this->Profiles->save($u_profile)) {
                        echo 'ok';
                    }
                break;
                
                /*
                *   User change role page.
                *
                *   Associated Controller => AdminsController,
                *   Method => users.
                */
                case 'user_change_role':

                    //Load Home Model.
                    $this->loadModel('Home');

                    //Queryout some few user's info.
                    $users = $this->Home->find()
                        ->select([
                            'home.firstname',
                            'home.lastname',
                            'home.role',
                            'profiles.profile_picture',
                        ])
                        ->contain(['Profiles'])
                        ->where(['home.id' => $this->request->data('id')]);
                    
                    //Enable  View.
                    $this->autoRender = true;
                    $this->viewBuilder()->template('user_change_role');
                        
                    $this->set('user_id', $this->request->data('id'));
                    $this->set('users', $users);
                break;
                
                /*
                *   Change User's Role.
                *
                *   Associated Controller => AdminsController,
                *   Method => users.
                */
                case 'change_role':
                    
                    //Load Home Model.
                    $this->loadModel('Home');

                    //Get User's Record.
                    $user = $this->Home->get($this->request->data('user_id'));

                    //Update User's Role.
                    $user->role = $this->request->data('role');

                    //Save Record.
                    if ($this->Home->save($user)) {

                        /*
                        *   Create Log Record.
                        */

                        //Admin's Full Name.
                        $admin_fullname = $this->request->session()->read('Auth.User')['firstname'] . ' ' . $this->request->session()->read('Auth.User')['lastname'];

                        //User's Fullname.
                        $user_fullname = $this->globalworks->getfirstname($this->request->data('user_id')) . ' ' . $this->globalworks->getlastname($this->request->data('user_id'));

                        //Create Log Message.
                        $log_msg = $admin_fullname . ' change ' . $user_fullname . ' account role as ' . $this->request->data('role') . '.';

                        //Save Log.
                        $this->globalworks->_log('admin_user_change_role', $log_msg);

                        
                        //If has notification message.
                        if ($this->request->data('has_notify')) {
                            
                            //Load Messages Model.
                            $this->loadModel('Messages');

                            //New Entity.
                            $messages = $this->Messages->newEntity();

                            //Message Data.
                            $data = [
                                'user_id' => $this->request->data('user_id'),
                                'sender_id' => $this->request->session()->read('Auth.User')['id'],
                                'unread' => 1,
                                'sent_item' => 0,
                                'message_type' => 'employer_msg_from_admin',
                                'subject' => 'Your account role has been changed.',
                                'message' => $this->request->data('notify_msg'),
                                'date' => date( "Y-m-d H:i:s" ),
                            ];

                            //Patch Entity.
                            $messages = $this->Messages->patchEntity($messages, $data);

                            //Send Message.
                            if ($this->Messages->save($messages)) {
                                
                                /*
                                *   Send Email Message to the user
                                *   and get the user's email address.
                                */

                                //Load Home Model.
                                $this->loadModel('Home');

                                //User's Email Value.
                                $user_email = '';

                                //Query to get email.
                                $home = $this->Home->find()
                                    ->select(['email'])
                                    ->where(['id' => $this->request->data('user_id')]);

                                //Loop to get Email.
                                foreach ($home as $user) {
                                    $user_email = $user['email'];
                                }

                                /*
                                *   Create Email Message.
                                */
                                $from = [$this->globalworks->_server_email_address, 'Globalworks'];
                                $to = $user_email;
                                $subject = 'Your account role has been changed.';
                                $message = '<p>Your account role has been changed.</p> <p>Check your message panel.</p> <p><a href="' . $this->globalworks->_url . '/login">Click Here</a> to login.</p>';
                                
                                //Email Data.
                                $email_contents = [
                                    'from' => $from,
                                    'to' => $to,
                                    'subject' => $subject,
                                    'html' => true,
                                    'message' => $message,
                                ];

                                //Send Email Message.
                                if ($this->globalworks->mail($email_contents)) {
                                    echo 'ok';
                                }
                            }
                        } else {
                            echo  'ok';
                        }
                    }
                break;
                
                /*
                *   Change User's suspend status.
                *
                *   Associated Controller => AdminsController,
                *   Method => users.
                */ 
                case 'user_suspend':

                    //Load Home Model.
                    $this->loadModel('Home');

                    //Get User's Record.
                    $users = $this->Home->find()
                        ->select([
                            'home.firstname',
                            'home.lastname',
                            'home.suspend',
                            'profiles.profile_picture',
                        ])
                        ->contain(['Profiles'])
                        ->where(['home.id' => $this->request->data('id')]);
                    
                    //Enable View.
                    $this->autoRender = true;
                    $this->viewBuilder()->template('user_suspend');

                    $this->set('user_id', $this->request->data('id'));
                    $this->set('users', $users);
                break;

                case 'suspend_opt':
                    
                    //Enable View.
                    $this->autoRender = true;
                    $this->viewBuilder()->template('user_suspend_form');

                    $this->set('user_id', $this->request->data('id'));
                break;

                case 'suspend_user':
                    
                    //Load Home Model.
                    $this->loadModel('Home');

                    //Get User's Row Record.
                    $user = $this->Home->get($this->request->data('id')); 
                    $user->suspend = $this->request->data('suspend');
                    $user->suspend_time = ($this->request->data('suspend_time')) ? $this->request->data('suspend_time') : null;
                    
                    if ($this->Home->save($user)) {

                        /*
                        *   Create Log Record.
                        */

                        //Admin's Full Name.
                        $admin_fullname = $this->request->session()->read('Auth.User')['firstname'] . ' ' . $this->request->session()->read('Auth.User')['lastname'];

                        //User's Fullname.
                        $user_fullname = $this->globalworks->getfirstname($this->request->data('id')) . ' ' . $this->globalworks->getlastname($this->request->data('id'));

                        //Create Log Message.
                        if ($this->request->data('suspend') == 1) {
                            $log_msg = $admin_fullname . ' suspend.';
                        } else {
                            $log_msg = $admin_fullname . ' unsuspend ' . $user_fullname . '\'s account.';
                        }

                        //Save Log.
                        $this->globalworks->_log('admin_user_suspend', $log_msg);
                        
                        //If has notification message.
                        if ($this->request->data('has_notify')) {
                            
                            /*
                            *   Send Email Message to the user
                            *   and get the user's email address.
                            */

                            //Load Home Model.
                            $this->loadModel('Home');

                            //User's Email Value.
                            $user_email = '';

                            //Query to get email.
                            $home = $this->Home->find()
                                ->select(['email'])
                                ->where(['id' => $this->request->data('id')]);

                            //Loop to get Email.
                            foreach ($home as $user) {
                                $user_email = $user['email'];
                            }

                            /*
                            *   Create Email Message.
                            */
                            $from = [$this->globalworks->_server_email_address, 'Globalworks'];
                            $to = $user_email;
                            $subject = 'Your account has been suspended';
                            $message = $this->request->data('notify_msg');
                            
                            //Email Data.
                            $email_contents = [
                                'from' => $from,
                                'to' => $to,
                                'subject' => $subject,
                                'html' => true,
                                'message' => $message,
                            ];

                            //Send Email Message.
                            if ($this->globalworks->mail($email_contents)) {
                                echo 'ok';
                            }
                        } else {
                            echo 'ok';
                        }
                    }
                break;
                
                /*
                *   Delete User's Account.
                *
                *   Associated Controller => AdminsController,
                *   Method => users.
                */ 
                case 'delete_user':

                    //Check if account does exist to avoid errors.
                    if ($this->globalworks->is_account_exists($this->request->data('id'))) {
                        
                        //If exists delete and files from profile_picture
                        //and profile_thumbnail directory.
                        if ($this->globalworks->has_profile($this->request->data('id'))) {
                            
                            //Check if user has profile picture.
                            $profile_picture = $this->globalworks->user_profile_picture($this->request->data('id'));
                            if ($profile_picture) {
                                
                                //Delete files.
                                if (file_exists('./img/users/profile_thumbnails/' . $profile_picture)) {
                                    unlink('./img/users/profile_thumbnails/' . $profile_picture);
                                }

                                if (file_exists('./img/users/profile_pictures/' . $profile_picture)) {
                                    unlink('./img/users/profile_pictures/' . $profile_picture);
                                }
                            }
                            
                            //Delete user's profile record.

                            //Load Profiles Model.
                            $this->loadModel('Profiles');
                            
                            //Get the ID raw record.
                            $profiles = $this->Profiles->find()
                                ->select(['id'])
                                ->where(['user_id' => $this->request->data('id')]);

                            //ID value.
                            $id = 0;

                            //Loop results to get ID.
                            foreach ($profiles as $profile) {
                                
                                //Append value.
                                $id += $profile['id'];
                            }

                            //Get ID raw.
                            $profile = $this->Profiles->get($id);

                            //Delete profile record.
                            $this->Profiles->delete($profile);
                        }

                        //Check if user has messages.
                        if ($this->globalworks->has_messages($this->request->data('id'))) {
                            
                            //Load Messages Model.
                            $this->loadModel('Messages');

                            //Query User's Messages.
                            $messages = $this->Messages->find()
                                ->select(['id'])
                                ->where(['user_id' => $this->request->data('id')])
                                ->orWhere(['sender_id' => $this->request->data('id')]);

                            //Loop Results.
                            foreach ($messages as $message) {
                                
                                //User's Message ID.
                                $id = $message['id'];

                                //Get message raw record.
                                $msg = $this->Messages->get($id);

                                //Delete Message.
                                $this->Messages->delete($msg);
                            }
                        }

                        //check if user has resume.
                        if ($this->globalworks->has_resume($this->request->data('id'))) {
                            
                            //Load Resumes Model.
                            $this->loadModel('Resumes');

                            //Query User's Resume.
                            $resumes = $this->Resumes->find()
                                ->select(['id', 'filename'])
                                ->where(['user_id' => $this->request->data('id')]);

                            //Resume's ID Value.
                            $id = 0;

                            //Resume Filename Value.
                            $filename = '';

                            //Loop Results to get ID.
                            foreach ($resumes as $resume) {
                                
                                //Append ID.
                                $id += $resume['id'];

                                //Append Filename.
                                $filename = $resume['filename'];
                            }

                            //Check if resume file is exists.
                            if (file_exists('./resumes/' . $filename)) {
                                
                                //Delete Resume File.
                                if (unlink('./resumes/' . $filename)) {
                                    
                                    //Get user's resume row record.
                                    $res = $this->Resumes->get($id);

                                    //Delete Resume Record.
                                    $this->Resumes->delete($res);
                                }
                            }
                        }

                        //Check if user has job posts
                        if ($this->globalworks->has_job_posts($this->request->data('id'))) {

                            //Load Jobposts Model.
                            $this->loadModel('Jobposts');

                            //Query User's Jobposts.
                            $jobposts = $this->Jobposts->find()
                                ->select(['id', 'header_image', 'company_logo'])
                                ->where(['user_id' => $this->request->data('id')]);

                            //Loop Results.
                            foreach ($jobposts as $post) {

                                //Check if user's job posts has company_logo.
                                if ($post['company_logo']) {

                                    //Check if image company logo file is exists.
                                    if (file_exists('./img/users/company_logo/' . $post['company_logo'])) {
                                        
                                        //Delete company logo file.
                                        unlink('./img/users/company_logo/' . $post['company_logo']);
                                    }
                                }

                                //Check if user's job posts has job header image.
                                if ($post['header_image']) {
                                    
                                    //Check if image header file exists.
                                    if (file_exists('./img/users/job_header/' . $post['header_image'])) {
                                        
                                        //Delete image header file.
                                        unlink('./img/users/job_header/' . $post['header_image']);
                                    }
                                }

                                //Get Jobpost row record.
                                $jb = $this->Jobposts->get($post['id']);

                                //Delete User's Jobpost.
                                $this->Jobposts->delete($jb);
                            }
                        }

                        //Get User's Email to send notification message if admin set the notification message.
                        //Delete User's account from users table.
                        //Load Home Model.
                        $this->loadModel('Home');

                        //User's Email.
                        $user_email = '';

                        //Query to get Email.
                        $home = $this->Home->find()
                            ->select(['email'])
                            ->where(['id' => $this->request->data('id')]);

                        //Loop to get user's email.
                        foreach ($home as $h) {
                            
                            //Append Value.
                            $user_email .= $h['email'];
                        }

                        //Get User's Account raw record.
                        $user = $this->Home->get($this->request->data('id'));

                       //Delete User's Account.
                        if ($this->Home->delete($user)) {

                            /*
                            *   Create Log Record.
                            */

                            //Admin's Full Name.
                            $admin_fullname = $this->request->session()->read('Auth.User')['firstname'] . ' ' . $this->request->session()->read('Auth.User')['lastname'];

                            //User's Fullname.
                            $user_fullname = $this->globalworks->getfirstname($this->request->data('id')) . ' ' . $this->globalworks->getlastname($this->request->data('id'));

                            //User's Email Address.
                            $email = $this->globalworks->getemail($id);


                            //Create Log Message.
                            $log_msg = $admin_fullname . ' delete ' . $user_fullname . '\'s ('. $email .') account.';

                            //Save Log.
                            $this->globalworks->_log('admin_user_account_delete', $log_msg);

                            //If User has notification.
                            if ($this->request->data('has_notify')) {

                                /*
                                *   Send Email Message to the user
                                *   and get the user's email address.
                                */
    
                                /*
                                *   Create Email Message.
                                */
                                $from = [$this->globalworks->_server_email_address, 'Globalworks'];
                                $to = $user_email;
                                $subject = 'Your account has been deleted.';
                                $message = $this->request->data('notify_msg');
                                
                                //Email Data.
                                $email_contents = [
                                    'from' => $from,
                                    'to' => $to,
                                    'subject' => $subject,
                                    'html' => true,
                                    'message' => $message,
                                ];
    
                                //Send Email Message.
                                if ($this->globalworks->mail($email_contents)) {
                                    echo 'ok';
                                }
                            }
                        }
                    }
                break;
                
                /*
                *   View user's single job post.
                *
                *   Associated Controller => AdminsController,
                *   Method => jobposts or jobpostsInactive or jobpostsSearch or jobpostsInactiveSearch.
                */ 
                case 'admin_jobpost_view':
                    
                    //Load Jobposts Model.
                    $this->loadModel('Jobposts');

                    //Jobpost ID.
                    $id = $this->request->data('id');

                    //Query Jobpost.
                    $jobposts = $this->Jobposts->find()
                        ->contain(['Users'])
                        ->select([
                            'jobposts.user_id',
                            'jobposts.header_image',
                            'jobposts.company_logo',
                            'jobposts.job_title',
                            'jobposts.company_name',
                            'jobposts.job_category',
                            'jobposts.job_type',
                            'jobposts.job_migration',
                            'jobposts.country',
                            'jobposts.job_description',
                            'jobposts.job_responsibilities',
                            'jobposts.job_qualifications',
                            'jobposts.job_years_experience',
                            'jobposts.job_location',
                            'jobposts.job_website',
                            'jobposts.job_email',
                            'jobposts.job_contact_number',
                            'jobposts.googglemap',
                            'jobposts.created_at',
                            'jobposts.closing_date',
                            'jobposts.modified_at',
                            'users.firstname',
                            'users.lastname'
                        ])
                        ->where(['jobposts.id' => $id]);

                        //Enable View.
                        $this->autoRender = true;
                        $this->viewBuilder()->template('user_job_view');
                        
                        //Render to view.
                        $this->set('id', $id);
                        $this->set('jobposts', $jobposts);
                break;
                        
                /*
                *   View edit user's single job post.
                *
                *   Associated Controller => AdminsController,
                *   Method => jobposts or jobpostsInactive or jobpostsSearch or jobpostsInactiveSearch.
                */ 
                case 'admin_jobpost_edit':
                    
                    //Load Jobposts Model.
                    $this->loadModel('Jobposts');

                    //Jobpost ID.
                    $id = $this->request->data('id');

                    //Enable View.
                    $this->autoRender = true;
                    $this->viewBuilder()->template('admin_post_edit');

                    //Query Jobpost.
                    $jobposts = $this->Jobposts->find()
                        ->where(['id' => $id]);
                        

                    foreach ($jobposts as $post) {
                        $this->set('id', $post['id']);
                        $this->set('user_id', $post['user_id']);
                        $this->set('src_header_image', $post['header_image']);
                        $this->set('src_company_logo', $post['company_logo']);
                        $this->set('src_job_title', $post['job_title']);
                        $this->set('src_company_name', $post['company_name']);
                        $this->set('src_job_category', $post['job_category']);
                        $this->set('src_job_type', $post['job_type']);
                        $this->set('src_job_migration', $post['job_migration']);
                        $this->set('src_country', $post['country']);
                        $this->set('src_job_description', $post['job_description']);
                        $this->set('src_job_responsibilities', $post['job_responsibilities']);
                        $this->set('src_job_qualifications', $post['job_qualifications']);
                        $this->set('src_job_year_experience', $post['job_years_experience']);
                        $this->set('src_job_location', $post['job_location']);
                        $this->set('src_job_website', $post['job_website']);
                        $this->set('src_job_email', $post['job_email']);
                        $this->set('src_job_contact_number', $post['job_contact_number']);
                        $this->set('src_googglemap', $post['googglemap']);
                        $this->set('closing_date', $post['closing_date']);
                        $this->set('modified_at', $post['modifiled_at']);
                    }
                break;
                
                /*
                *   Jonposts Delete Header Image in header_tmp directory.
                *
                *   Associated Controller => AdminsController,
                *   Method => jobposts or jobpostsInactive or jobpostsSearch or jobpostsInactiveSearch.
                */ 
                case 'admin_jobpost_delete_header_tmp':

                    //Load Jobheader
                    $this->loadModel('Jobheaderimg_tmp');
                        
                    //Query out to check if user has record from the table.
                    $jobheader_count = $this->Jobheaderimg_tmp->find()
                        ->where(['user_id' => $this->request->data('user_id')])
                        ->andWhere(['filename' => $this->request->data('filename')])
                        ->count();

                    echo $jobheader_count;
                    
                    //If No Record.
                    if ($jobheader_count > 0) {

                        //Query out to find record.
                        $jobheader = $this->Jobheaderimg_tmp->find()
                            ->where(['user_id' => $this->request->data('user_id')])
                            ->andWhere(['filename' => $this->request->data('filename')]);
                        
                        //ID to append.
                        $id = '';
                        
                        //Loop Result.
                        foreach ($jobheader as $header) {

                            //Append ID.
                            $id .= $header['id'];
                        }
                        
                        //Get tmp Record.
                        $jh = $this->Jobheaderimg_tmp->get($id);
                        
                        //Delete Record and Delete tmp File.
                        if ($this->Jobheaderimg_tmp->delete($jh)) {

                            //Path to job_header dir.
                            $header_path = './img/users/job_header_tmp/';
                            
                            //IF file exists. delete image.
                            if (file_exists($header_path . $this->request->data('filename'))) {
                                if (unlink($header_path . $this->request->data('filename'))) {
                                    echo 'ok';
                                }
                            }
                        }
                    } else {
                        echo 'ok';
                    }
                break;
                
                /*
                *   Upload Jobposts header image process.
                *
                *   Associated Controller => AdminsController,
                *   Method => jobposts or jobpostsInactive or jobpostsSearch or jobpostsInactiveSearch.
                */ 
                case 'admin_jobpost_upload_new_header':
                    
                    //File Uploaded Properties.
                    $img = $_FILES['header-image'];
                    $type = $img['type'];
                    $tmp = $img['tmp_name'];
                    $error = $img['error'];

                    //If error is 0.
                    if ($error == 0) {

                        //Path to job_header_dir.
                        $path = './img/users/job_header_tmp/';

                        //USer's user ID.
                        $user_id = $this->request->data('user_id');

                        //To be append. for random strings.
                        $random_chars = '';

                        //Characters to generate.
                        $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

                        //Generate random strings.
                        for ($p = 0; $p < 16; $p++) {
                            $random_chars .= $chars[mt_rand(0, strlen($chars)-1)];
                        }

                        //New Image Filename.
                        $new_filename = $user_id . date( "YmdHis" ) . $random_chars . '.jpg';

                        //Get Image Dimansion.
                        list($width, $height) = getimagesize($tmp);

                        //Initiate GD Library.
                        $img = imagecreatetruecolor($width, $height);

                        //If Image format is jpeg.
                        if ($type == 'image/jpeg') {
                            $img_src = imagecreatefromjpeg($tmp);

                        //If Image format is png.
                        } else if ($type == 'image/png') {
                            $img_src = imagecreatefrompng($tmp);
                        }

                        //Create Image.
                        imagecopyresampled($img, $img_src, 0, 0, 0, 0, $width, $height, $width, $height);

                        //Move image from $path dir.
                        imagejpeg($img, $path . $new_filename);

                        //Load Jobheaderimg_tmp Model.
                        $this->loadModel('Jobheaderimg_tmp');

                        //New Entity.
                        $header = $this->Jobheaderimg_tmp->newEntity();

                        //Append New Entity.
                        $new_entity = [
                            'user_id' => $user_id,
                            'filename' => $new_filename,
                            'uploaded_at' => microtime(true),
                        ];

                        //Patch Entity.
                        $header = $this->Jobheaderimg_tmp->patchEntity($header, $new_entity);

                        //Save Record.
                        if ($this->Jobheaderimg_tmp->save($header)) {
                            echo $new_filename;
                        }
                    }
                break;
                
                /*
                *   Delete Jobposts company logo process.
                *
                *   Associated Controller => AdminsController,
                *   Method => jobposts or jobpostsInactive or jobpostsSearch or jobpostsInactiveSearch.
                */ 
                case 'admin_employers_jobpost_delete_logo_tmp':

                    //Load Companylogo_tmp Model.
                    $this->loadModel('Companylogo_tmp');
                        
                    //Check if file is exist from the record.
                    $companylogo_count = $this->Companylogo_tmp->find()
                        ->where(['user_id' => $this->request->data('user_id')])
                        ->andWhere(['filename' => $this->request->data('filename')])
                        ->count();
                    
                    //If file record exists.
                    if ($companylogo_count !== 0) {

                        //Query out file record.
                        $companylogo = $this->Companylogo_tmp->find()
                            ->where(['user_id' => $this->request->data('user_id')])
                            ->andWhere(['filename' => $this->request->data('filename')]);
                        
                        //ID to append.
                        $id = '';
                        
                        //Loop Result.
                        foreach ($companylogo as $cp) {

                            //Append ID.
                            $id .= $cp['id'];
                        }
                        
                        //Get Logo record column.
                        $logo = $this->Companylogo_tmp->get($id);
                        
                        //Delete record and delete file.
                        if ($this->Companylogo_tmp->delete($logo)) {
                            $logo_path = './img/users/company_logo_tmp/';
                            
                            if (file_exists($logo_path . $this->request->data('filename'))) {
                                if (unlink($logo_path . $this->request->data('filename'))) {
                                    echo 'ok';
                                }
                            }
                        }
                    } else {
                        echo 'ok';
                    }
                break;
                
                /*
                *   Upload jobposts company logo process.
                *
                *   Associated Controller => AdminsController,
                *   Method => jobposts or jobpostsInactive or jobpostsSearch or jobpostsInactiveSearch.
                */ 
                case 'admin_employers_jobpost_upload_new_logo':
                    //Uploaded File Properties.
                    $image = $_FILES['logo-image'];
                    $filename = $image['name'];
                    $type = $image['type'];
                    $tmp = $image['tmp_name'];
                    $error = $image['error'];
                    
                    //If Error is 0.
                    if ($error == 0) {

                        //Path to company_logo_tmp dir.
                        $path = './img/users/company_logo_tmp/';
                        
                        //User's User ID.
                        $user_id = $this->request->data('user_id');
                        
                        $random_chars = '';
                        
                        //Random Strings to Generate.
                        $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

                        //Generate random strings.
                        for ($p = 0; $p < 16; $p++) {
                            $random_chars .= $chars[mt_rand(0, strlen($chars)-1)];
                        }
                        
                        //New Filename.
                        $new_filename = $user_id . date( "YmdHis" ) . $random_chars . '.jpg';
                        
                        //Get Image Dimension.
                        list($width, $height) = getimagesize($tmp);

                        //Initiate GD Library.
                        $img = imagecreatetruecolor($width, $height);
                        
                        //Image file types. If image is jpeg format.
                        if ($type == 'image/jpeg') {
                            $img_src = imagecreatefromjpeg($tmp);
                        
                        //Image file types. If image is png format.
                        } else if ($type == 'image/png') {
                            $img_src = imagecreatefrompng($tmp);
                        }

                        //Create Image.
                        imagecopyresampled($img, $img_src, 0, 0, 0, 0, $width, $height, $width, $height);

                        //Move Image to the dirctory path as jpeg format.
                        imagejpeg($img, $path . $new_filename);
                        
                        //Load Companylogo_tmp Model.
                        $this->loadModel('Companylogo_tmp');
                        
                        //New Entity.
                        $logo = $this->Companylogo_tmp->newEntity();
                        
                        //New Entity Append.
                        $new_entity = [
                            'user_id' => $user_id,
                            'filename' => $new_filename,
                            'uploaded_at' => microtime(true),
                        ];
                        
                        //Patch Entity
                        $logo = $this->Companylogo_tmp->patchEntity($logo, $new_entity);
                        
                        //Save Record.
                        if ($this->Companylogo_tmp->save($logo)) {
                            echo $new_filename;
                        }
                    }
                break;
                
                /*
                *   Saving Edited Jobposts Process.
                *
                *   Associated Controller => AdminsController,
                *   Method => jobposts or jobpostsInactive or jobpostsSearch or jobpostsInactiveSearch.
                */ 
                case 'admin_employers_jobpost_edit_save':
                    
                    $this->loadModel('Jobposts');

                    //Get Job Post Requested.
                    $jobposts = $this->globalworks->get_job_post($this->request->data('id'));

                    $header_image = '';
                    $company_logo = '';

                    //Loop Results and Append values to get header_image and company_logo filenames.
                    foreach ($jobposts as $jobpost) {
                        $header_image .= $jobpost['header_image'];
                        $company_logo .= $jobpost['company_logo'];
                    }

                    /*
                    *   If Header Image Filename does not match to the current header image file.
                    *   delete current image file. But if this->request->data('header_image') is null or empty.
                    *   No Image to upload.
                    */
                    if ($this->request->data('header_image') !== $header_image) {
                        $this->globalworks->header_image_delete($header_image);
                        $this->globalworks->header_image_move($this->request->data('header_image'));
                    }

                    /*
                    *   If company_logo Image Filename does not match to the current company_logo image file.
                    *   delete current image file. But if this->request->data('company_logo') is null or empty.
                    *   No Image to upload.
                    */
                    if ($this->request->data('company_logo') !== $company_logo) {
                        $this->globalworks->company_logo_delete($company_logo);
                        $this->globalworks->company_logo_move($this->request->data('company_logo'));
                    }

                    //To be append.
                    $job_years_experience = '';

                    //If years_exp_end is not null or empty.
                    if ($this->request->data('years_exp_end') !== '') {
                        $job_years_experience = $this->request->data('years_exp_start') . ' to ' . $this->request->data('years_exp_end');
                    } else {
                        $job_years_experience = $this->request->data('years_exp_start');
                    }

                    //Qualifcation value to append.
                    $qualifications = '';

                    //Responsibilities value to append.
                    $responsibilities = '';

                    //If Qualifications is not empty.
                    if ($this->request->data('qualifications')) {
                        $qualifications = json_encode($this->request->data('qualifications'));
                    }
                    
                    //If Responsibilities is not empty.
                    if ($this->request->data('responsibilities')) {
                        $responsibilities = json_encode($this->request->data('responsibilities'));
                    }

                    //Get Jobpost ID.
                    $jobpost = $this->Jobposts->get($this->request->data('id'));

                    //Apply as New Entities.
                    $jobpost->header_image = $this->request->data('header_image');
                    $jobpost->company_logo = $this->request->data('company_logo');
                    $jobpost->job_title = $this->request->data('job_title');
                    $jobpost->company_name = $this->request->data('company_name');
                    $jobpost->job_category = $this->request->data('job_category');
                    $jobpost->job_mirgation = $this->request->data('job_base');
                    $jobpost->country = $this->request->data('country');
                    $jobpost->job_description = $this->request->data('description');
                    $jobpost->job_qualifications = $qualifications;
                    $jobpost->job_responsibilities = $responsibilities;
                    $jobpost->job_years_experience = $job_years_experience;
                    $jobpost->job_location = $this->request->data('job_location');
                    $jobpost->job_website = $this->request->data('job_website');
                    $jobpost->job_contact_number = $this->request->data('job_contact_number');
                    $jobpost->googglemap = $this->request->data('googglemap');
                    $jobpost->closing_date = $this->request->data('closing_date');
                    $jobpost->modified_at = date( "Y-m-d H:i:s" );

                    //Save Changes.
                    if ($this->Jobposts->save($jobpost)) {

                        /*
                        *   Create Log Record.
                        */

                        //Admin's Full Name.
                        $admin_fullname = $this->request->session()->read('Auth.User')['firstname'] . ' ' . $this->request->session()->read('Auth.User')['lastname'];

                        //Job Title.
                        $job_title = $this->globalworks->get_job_title($this->request->data('id'));

                        //Create Log Message.
                        $log_msg = $admin_fullname . ' edit a jobpost ('. $job_title .')';

                        //Save Log.
                        $this->globalworks->_log('admin_jobpost_edit_log', $log_msg);

                        
                        /*
                        *   Send Notification to the user via messages.
                        */

                        //If has user notification.
                        if ($this->request->data('has_notify_user')) {
                            
                            //Load Messages Model.
                            $this->loadModel('Messages');

                            //New Entity.
                            $messages = $this->Messages->newEntity();

                            //Message Data.
                            $data = [
                                'user_id' => $this->request->data('user_id'),
                                'sender_id' => $this->request->session()->read('Auth.User')['id'],
                                'unread' => 1,
                                'sent_item' => 0,
                                'message_type' => 'employer_msg_from_admin',
                                'subject' => 'Jobposts: (' . $this->request->data('job_title') . ') has been modified.',
                                'message' => $this->request->data('notify_user_message'),
                                'date' => date( "Y-m-d H:i:s" ),
                            ];

                            //Patch Entity.
                            $messages = $this->Messages->patchEntity($messages, $data);

                            //Insert Message to the messages table.
                            if ($this->Messages->save($messages)) {

                                /*
                                *   Send Email Message to the user
                                *   and get the user's email address.
                                */

                                //Load Home Model.
                                $this->loadModel('Home');

                                //User's Email Value.
                                $user_email = '';

                                //Query to get email.
                                $home = $this->Home->find()
                                    ->select(['email'])
                                    ->where(['id' => $this->request->data('user_id')]);

                                //Loop to get Email.
                                foreach ($home as $user) {
                                    $user_email = $user['email'];
                                }

                                /*
                                *   Create Email Message.
                                */
                                $from = [$this->globalworks->_server_email_address, 'Globalworks'];
                                $to = $user_email;
                                $subject = 'Jobposts: (' . $this->request->data('job_title') . ') has been modified.';
                                $message = '<p>Jobposts: (' . $this->request->data('job_title') . ') has been modified.</p> <p>Check your message panel.</p> <p><a href="' . $this->globalworks->_url . '/login">Click Here</a> to login.</p>';
                                
                                //Email Data.
                                $email_contents = [
                                    'from' => $from,
                                    'to' => $to,
                                    'subject' => $subject,
                                    'html' => true,
                                    'message' => $message,
                                ];

                                //Send Email Message.
                                if ($this->globalworks->mail($email_contents)) {
                                    echo 'ok';
                                }
                            }
                        } else {
                            echo 'ok';
                        }
                    }
                break;

                /*
                *   Delete User's Jobpost.
                *
                *   Associated Controller => AdminsController,
                *   Method => jobposts or jobpostsInactive or jobpostsSearch or jobpostsInactiveSearch.
                */ 
                case 'admin_employers_jobpost_delete':

                    //Load Jobposts Model.
                    $this->loadModel('Jobposts');

                    //Query out to find Jobpost record, And Check header_image and company_logo is set.
                    $jobposts = $this->Jobposts->find()
                        ->where(['user_id' => $this->request->data('user_id')])
                        ->andWhere(['id' => $this->request->data('id')]);

                    //To be Append.
                    $header_file = '';

                    //To be Append.
                    $logo_file = '';

                    //Loop Results.
                    foreach ($jobposts as $post) {

                        //Append Header Filename.
                        $header_file .= $post['header_image'];

                        //Append company_logo Filename
                        $logo_file .= $post['company_logo'];
                    }

                    //Path to job_header dir.
                    $header_path = './img/users/job_header/';

                    //Path to company_logo dir.
                    $logo_path = './img/users/company_logo/';

                    //If File exists delete image file.
                    if ($header_file && file_exists($header_path . $header_file)) {
                        unlink($header_path . $header_file);
                    }

                    //If File exists delete image file.
                    if ($logo_file && file_exists($logo_path . $logo_file)) {
                        unlink($logo_path . $logo_file);
                    }

                    //Get Record Column.
                    $jb = $this->Jobposts->get($this->request->data('id'));

                    //Delete Record.
                    if ($this->Jobposts->delete($jb)) {

                        /*
                        *   Create Log Record.
                        */

                        //Admin's Full Name.
                        $admin_fullname = $this->request->session()->read('Auth.User')['firstname'] . ' ' . $this->request->session()->read('Auth.User')['lastname'];

                        //Job Title.
                        $job_title = $this->globalworks->get_job_title($this->request->data('id'));

                        //Create Log Message.
                        $log_msg = $admin_fullname . ' delete a jobpost ('. $job_title .')';

                        //Save Log.
                        $this->globalworks->_log('admin_jobpost_delete_log', $log_msg);
                        
                        //If has user notification message.
                        if ($this->request->data('del_notify')) {
                            
                            //Load Messages Model.
                            $this->loadModel('Messages');

                            //New Entity.
                            $messages = $this->Messages->newEntity();

                            //Message Data.
                            $data = [
                                'user_id' => $this->request->data('user_id'),
                                'sender_id' => $this->request->session()->read('Auth.User')['id'],
                                'unread' => 1,
                                'sent_item' => 0,
                                'message_type' => 'employer_msg_from_admin',
                                'subject' => 'Jobposts: (' . $this->request->data('job_title') . ') has been deleted.',
                                'message' => $this->request->data('del_notify_msg'),
                                'date' => date( "Y-m-d H:i:s" ),
                            ];

                            //Patch Entity.
                            $messages = $this->Messages->patchEntity($messages, $data);

                            if ($this->Messages->save($messages)) {
                               
                                /*
                                *   Send Email Message to the user
                                *   and get the user's email address.
                                */

                                //Load Home Model.
                                $this->loadModel('Home');

                                //User's Email Value.
                                $user_email = '';

                                //Query to get email.
                                $home = $this->Home->find()
                                    ->select(['email'])
                                    ->where(['id' => $this->request->data('user_id')]);

                                //Loop to get Email.
                                foreach ($home as $user) {
                                    $user_email = $user['email'];
                                }

                                /*
                                *   Create Email Message.
                                */
                                $from = [$this->globalworks->_server_email_address, 'Globalworks'];
                                $to = $user_email;
                                $subject = 'Jobposts: (' . $this->request->data('job_title') . ') has been deleted.';
                                $message = '<p>Jobposts: (' . $this->request->data('job_title') . ') has been deleted.</p> <p>Check your message panel.</p> <p><a href="' . $this->globalworks->_url . '/login">Click Here</a> to login.</p>';
                                
                                //Email Data.
                                $email_contents = [
                                    'from' => $from,
                                    'to' => $to,
                                    'subject' => $subject,
                                    'html' => true,
                                    'message' => $message,
                                ];

                                //Send Email Message.
                                if ($this->globalworks->mail($email_contents)) {
                                    echo 'ok';
                                }
                            }
                        }
                    }
                break;

                //Log List.
                case 'log_list':

                    //Load Logs Model.
                    $this->loadModel('Logs');

                    //Log Type.
                    $log_type = $this->request->data('log_type');

                    //Limit Request.
                    $limit = $this->request->data('limit');

                    //If log type is empty or not.
                    if ($log_type == '') {

                        //Query Log.
                        $logs = $this->Logs->find()
                            ->select(['message', 'date'])
                            ->limit($limit)
                            ->order(['id' => 'DESC']);
                    } else {

                        //Query Log.
                        $logs = $this->Logs->find()
                            ->select(['message', 'date'])
                            ->where(['log_type' => $log_type])
                            ->limit($limit)
                            ->order(['id' => 'DESC']);
                    }

                    //Enable View.
                    $this->autoRender = true;
                    $this->viewBuilder()->template('log_list');

                    $this->set('logs', $logs);
                break;

                //Clear Logs.
                case 'clear_logs':

                    //Load Logs Model.
                    $this->loadModel('Logs');

                    //Delete All Logs.
                    if ($this->Logs->deleteAll([], false)) {

                        /*
                        *   Create Log Record.
                        */

                        //Admin's Full Name.
                        $admin_fullname = $this->request->session()->read('Auth.User')['firstname'] . ' ' . $this->request->session()->read('Auth.User')['lastname'];

                        //Create Log Message.
                        $log_msg = $admin_fullname . ' Cleared Logs.';

                        //Save Log.
                        $this->globalworks->_log('admin_clear_logs', $log_msg);

                        echo 'ok';
                    }
                break;

                //Load Announcement list.
                case 'announcement_list':
                    
                    //Load Announcements Model.
                    $this->loadModel('Announcements');

                    //Query Announcements.
                    $ann_published = $this->Announcements->find()
                        ->where(['published' => 1]);

                    $ann_notpublished = $this->Announcements->find()
                        ->where(['published' => 0])
                        ->order(['id' => 'DESC']);

                    //Enable View.
                    $this->autoRender = true;
                    $this->viewBuilder()->template('announcements_list');

                    //Render to view.
                    $this->set('ann_published', $ann_published);
                    $this->set('ann_notpublished', $ann_notpublished);
                break;
                
                //Create Announcement.
                case 'create_announcement':
                    
                    //Load Announcement Model.
                    $this->loadModel('Announcements');

                    /*
                    *   Reset published column to 0.
                    */

                    //Query Announcements.
                    $announcements = $this->Announcements->find()
                        ->select(['id']);

                    //Loop Results.
                    foreach ($announcements as  $announcement) {

                        //Get the ID.
                        $ann = $this->Announcements->get($announcement['id']);

                        //Update Published.
                        $ann->published = 0;

                        //Save Changes.
                        $this->Announcements->save($ann);
                    }

                    //New Entity.
                    $announcements = $this->Announcements->newEntity();

                    //Entries.
                    $data = [
                        'announcement' => $this->request->data('announcement'),
                        'published' => 1,
                        'created_at' => date( "Y-m-d H:i:s" ),
                        'user_id' => $this->request->session()->read('Auth.User')['id']
                    ];

                    //Patch Entity.
                    $announcements = $this->Announcements->patchEntity($announcements, $data);

                    //Save Announcement.
                    if ($this->Announcements->save($announcements)) {

                        /*
                        *   Create Log Record.
                        */

                        //Admin's Full Name.
                        $admin_fullname = $this->request->session()->read('Auth.User')['firstname'] . ' ' . $this->request->session()->read('Auth.User')['lastname'];

                        //Create Log Message.
                        $log_msg = $admin_fullname . ' create an announcement ('. $this->globalworks->ann_shorten(strip_tags($this->request->data('announcement'))) .').';

                        //Save Log.
                        $this->globalworks->_log('admin_create_announcement_logs', $log_msg);

                        echo 'ok';
                    }
                break;

                //Published  an announcement.
                case 'announcement_published':
                    
                    //Load Announcements Model.
                    $this->loadModel('Announcements');

                    /*
                    *   Reset published column to 0.
                    */

                    //Query Announcements.
                    $announcements = $this->Announcements->find()
                        ->select(['id', 'announcement']);

                    //Announcement Value.
                    $announcement_val = '';

                    //Loop Results.
                    foreach ($announcements as $announcement) {

                        //Append Value.
                        $announcement_val .= $announcement['announcement'];

                        //Get the ID.
                        $ann = $this->Announcements->get($announcement['id']);

                        //Update Published.
                        $ann->published = 0;

                        //Save Changes.
                        $this->Announcements->save($ann);
                    }

                    //Get announcement ID row record.
                    $anno = $this->Announcements->get($this->request->data('id'));

                    //Update published.
                    $anno->published = 1;

                    if ($this->Announcements->save($anno)) {

                        /*
                        *   Create Log Record.
                        */

                        //Admin's Full Name.
                        $admin_fullname = $this->request->session()->read('Auth.User')['firstname'] . ' ' . $this->request->session()->read('Auth.User')['lastname'];

                        //Create Log Message.
                        $log_msg = $admin_fullname . ' published an announcement ('. $this->globalworks->ann_shorten(strip_tags($announcement_val)) .').';

                        //Save Log.
                        $this->globalworks->_log('admin_published_announcement_logs', $log_msg);

                        echo 'ok';
                    }
                break;

                //Unpublished announcement.
                case 'announcement_unpublished':

                    //Load Announcements Model.
                    $this->loadModel('Announcements');

                    /*
                    *   Reset published column to 0.
                    */

                    //Query Announcements.
                    $announcements = $this->Announcements->find()
                        ->select(['id', 'announcement']);

                    //Loop Results.
                    foreach ($announcements as $announcement) {

                        //Get the ID.
                        $ann = $this->Announcements->get($announcement['id']);

                        //Update Published.
                        $ann->published = 0;

                        //Save Changes.
                        $this->Announcements->save($ann);
                    }

                    /*
                    *   Create Log Record.
                    */

                    //Admin's Full Name.
                    $admin_fullname = $this->request->session()->read('Auth.User')['firstname'] . ' ' . $this->request->session()->read('Auth.User')['lastname'];

                    //Create Log Message.
                    $log_msg = $admin_fullname . ' published an announcement.';

                    //Save Log.
                    $this->globalworks->_log('admin_unpublish_announcement_logs', $log_msg);
                break;
                
                //Save Edit Announcement.
                case 'save_edit_announcement':

                    //Load Announcements Model.
                    $this->loadModel('Announcements');

                    //If Publish is checked.
                    if ($this->request->data('publish')) {
                        
                        /*
                        *   Reset published column to 0.
                        */

                        //Query Announcements.
                        $announcements = $this->Announcements->find()
                            ->select(['id', 'announcement']);

                        //Announcement Value.
                        $announcement_val = '';

                        //Loop Results.
                        foreach ($announcements as $announcement) {

                            //Append Value.
                            $announcement_val = $announcement['announcement'];

                            //Get the ID.
                            $ann = $this->Announcements->get($announcement['id']);

                            //Update Published.
                            $ann->published = 0;

                            //Save Changes.
                            $this->Announcements->save($ann);
                        }

                        //Get Announcement record.
                        $announcement = $this->Announcements->get($this->request->data('id'));
                        
                        //Update record.
                        $announcement->announcement = $this->request->data('announcement');
                        $announcement->published = $this->request->data('publish');

                        //Save Changes.
                        if ($this->Announcements->save($announcement)) {

                            /*
                            *   Create Log Record.
                            */

                            //Admin's Full Name.
                            $admin_fullname = $this->request->session()->read('Auth.User')['firstname'] . ' ' . $this->request->session()->read('Auth.User')['lastname'];

                            //Create Log Message.
                            $log_msg = $admin_fullname . ' edit an announcement. ('. $this->globalworks->ann_shorten(strip_tags($announcement_val)) .').';

                            //Save Log.
                            $this->globalworks->_log('admin_edit_announcement_logs', $log_msg);
                            
                            echo 'ok';
                        }
                    } else {

                        //Get Announcement record.
                        $announcement = $this->Announcements->get($this->request->data('id'));
                        
                        //Update record.
                        $announcement->announcement = $this->request->data('announcement');

                        //Save Changes.
                        if ($this->Announcements->save($announcement)) {

                            /*
                            *   Create Log Record.
                            */

                            //Admin's Full Name.
                            $admin_fullname = $this->request->session()->read('Auth.User')['firstname'] . ' ' . $this->request->session()->read('Auth.User')['lastname'];

                            //Create Log Message.
                            $log_msg = $admin_fullname . ' edit an announcement. ('. $this->globalworks->ann_shorten(strip_tags($this->request->data('announcement'))) .').';

                            //Save Log.
                            $this->globalworks->_log('admin_edit_announcement_logs', $log_msg);

                            echo 'ok';
                        }
                    }
                break;

                //Delete announcement.
                case 'announcement_delete':
                    
                    //Load Announcements Model.
                    $this->loadModel('Announcements');

                    //Query to get Announcement Content.
                    $announcements = $this->Announcements->find()
                        ->select(['announcement'])
                        ->where(['id' => $this->request->data('id')]);

                    //Announcement Value.
                    $announcement_val = '';

                    //Loop Result.
                    foreach ($announcement as $announcement) {

                        //Append Value.
                        $announcement_val .= $announcement['announcement'];
                    }

                    //Get column record.
                    $ann = $this->Announcements->get($this->request->data('id'));

                    //Delete Announcement.
                    if ($this->Announcements->delete($ann)) {

                        /*
                        *   Create Log Record.
                        */

                        //Admin's Full Name.
                        $admin_fullname = $this->request->session()->read('Auth.User')['firstname'] . ' ' . $this->request->session()->read('Auth.User')['lastname'];

                        //Create Log Message.
                        $log_msg = $admin_fullname . ' edit an announcement. ('. $this->globalworks->ann_shorten(strip_tags($announcement_val)) .').';

                        //Save Log.
                        $this->globalworks->_log('admin_edit_announcement_logs', $log_msg);

                        echo 'ok';
                    }
                break;
                
                //IP log list.
                case 'ip_logs_list':

                    //Load Ip_logs Model.
                    $this->loadModel('Ip_logs');

                    //Query logs.
                    $logs = $this->Ip_logs->find()
                        ->select(['id', 'query', 'country', 'region_name', 'hits'])
                        ->order(['id' => 'DESC']);

                    //Enable View.
                    $this->autoRender = true;
                    $this->viewBuilder()->template('ip_logs');

                    $this->set('logs', $logs);
                break;

                //IP log details.
                case 'ip_log_view_details':

                    //Load Ip_logs Model.
                    $this->loadModel('Ip_logs');

                    //Query logs.
                    $logs = $this->Ip_logs->find()
                        ->select(['id', 'query', 'country', 'city', 'isp', 'lon', 'lat', 'region_name', 'timezone', 'zip', 'hits'])
                        ->where(['id' => $this->request->data('id')]);

                    //Enable View.
                    $this->autoRender = true;
                    $this->viewBuilder()->template('ip_details');

                    $this->set('logs', $logs);
                break;

                //Malinglist List.
                case 'malinglist_list':
                    
                    //Load Mailinglists Model.
                    $this->loadModel('Mailinglists');

                    //Query Mailinglists
                    $lists = $this->Mailinglists->find()
                        ->select(['id', 'subject', 'created_at'])
                        ->limit($this->request->data('limit'))
                        ->order(['id' => 'DESC']);

                    //Enable View.
                    $this->autoRender = true;
                    $this->viewBuilder()->template('mailinglist_list');

                    $this->set('lists', $lists);
                break;
                
                //Create Mailing List.
                case 'create_malinglist':
                    
                    //Load Mailing List Model.
                    $this->loadModel('Mailinglists');

                    //New Entity.
                    $lists = $this->Mailinglists->newEntity();

                    //Data Entries.
                    $data = [
                        'content' => $this->request->data('content'),
                        'subject' => $this->request->data('subject'),
                        'created_at' => date( "Y-m-d H:i:s" ),
                    ];

                    //Patch Entity.
                    $lists = $this->Mailinglists->patchEntity($lists, $data);

                    //Save Mailing List and Send to all emails.
                    if ($this->Mailinglists->save($lists)) {

                        /*
                        *   Get Users who is susbscribed in mailing list.
                        */

                        //Load Home Model.
                        $this->loadModel('Home');

                        //Count Subscribe Users..
                        $users_count = $this->Home->find()
                            ->contain(['Profiles'])
                            ->where(['profiles.job_subscribe' => 1])
                            ->count();

                        //Query Users subscribe to mailing list.
                        $users = $this->Home->find()
                            ->select(['home.email'])
                            ->contain(['Profiles'])
                            ->where(['profiles.job_subscribe' => 1]);


                        $i = 0;

                        //Loop Results.
                        foreach ($users as $user) {
                            $i++;
                            
                            //Email Properties.
                            $email = [
                                'from' => [$this->globalworks->_server_email_address, 'Globalworks'],
                                'html' => true,
                                'subject' => $this->request->data('subject'),
                                'to' => $user['home']['email'],
                                'message' => $this->request->data('content'),
                            ];

                            //Send Email Mesage.
                            $this->globalworks->mail($email);

                            if ($i == $users_count) {

                                /*
                                *   Send Message to Subscribers.
                                */
                                if ($this->globalworks->send_subscribers($this->request->data('subject'), $this->request->data('content'))) {
                                    
                                    /*
                                    *   Create Log Record.
                                    */

                                    //Admin's Full Name.
                                    $admin_fullname = $this->request->session()->read('Auth.User')['firstname'] . ' ' . $this->request->session()->read('Auth.User')['lastname'];

                                    //Create Log Message.
                                    $log_msg = $admin_fullname . ' created a mailing list. ('. $this->request->data('subject') .').';

                                    //Save Log.
                                    $this->globalworks->_log('admin_create_mailinglist', $log_msg);

                                    echo 'ok';
                                }
                            }
                        }
                    }
                break;
                
                //Resend Mailinglist.
                case 'resend_mailinglist':

                    //Load Malinglists Model.
                    $this->loadModel('Mailinglists');

                    //Query Requested Mailinglist.
                    $lists = $this->Mailinglists->find()
                        ->select(['subject', 'content'])
                        ->where(['id' => $this->request->data('id')]);

                    //Subject Value.
                    $subject = '';
                    
                    //Content Value.
                    $content = '';

                    //Loop Results.
                    foreach ($lists as $list) {

                        //Append Values.
                        $subject .= $list['subject'];
                        $content .= $list['content'];
                    }


                    /*
                    *   Get Users who is susbscribed in mailing list.
                    */

                    //Load Home Model.
                    $this->loadModel('Home');

                    //Count Subscribe Users..
                    $users_count = $this->Home->find()
                        ->contain(['Profiles'])
                        ->where(['profiles.job_subscribe' => 1])
                        ->count();

                    //Query Users subscribe to mailing list.
                    $users = $this->Home->find()
                        ->select(['home.email'])
                        ->contain(['Profiles'])
                        ->where(['profiles.job_subscribe' => 1]);


                    $i = 0;

                    //Loop Results.
                    foreach ($users as $user) {
                        $i++;
                        
                        //Email Properties.
                        $email = [
                            'from' => [$this->globalworks->_server_email_address, 'Globalworks'],
                            'html' => true,
                            'subject' => $subject,
                            'to' => $user['home']['email'],
                            'message' => $content,
                        ];

                        //Send Email Mesage.
                        $this->globalworks->mail($email);

                        if ($i == $users_count) {

                            /*
                            *   Send Message to Subscribers.
                            */
                            if ($this->globalworks->send_subscribers($subject, $content)) {

                                /*
                                *   Create Log Record.
                                */

                                //Admin's Full Name.
                                $admin_fullname = $this->request->session()->read('Auth.User')['firstname'] . ' ' . $this->request->session()->read('Auth.User')['lastname'];

                                //Create Log Message.
                                $log_msg = $admin_fullname . ' resend a mailing list. ('. $subject .').';

                                //Save Log.
                                $this->globalworks->_log('admin_resend_mailinglist', $log_msg);

                                echo 'ok';
                            }
                        }
                    }
                break;

                //Save Edit Mailinglist.
                case 'save_edit_malinglist':
                    
                    //Load Mailinglists Model.
                    $this->loadModel('Mailinglists');

                    //Query Maiilinglist.
                    $lists = $this->Mailinglists->find()
                        ->select(['subject'])
                        ->where(['id' => $this->reqeust->data('id')]);

                    //Subject Value.
                    $subject = '';

                    //Loop Results
                    foreach ($lists as $l) {

                        //Append Value.
                        $subject .= $l['subject'];
                    }

                    //Get Column Record.
                    $list = $this->Mailinglists->get($this->request->data('id'));

                    //Update record.
                    $list->subject = $this->request->data('subject');
                    $list->content = $this->request->data('content');

                    //Save Record.
                    if ($this->Mailinglists->save($list)) {

                        //If Send value is 1. Send Email.
                        if ($this->request->data('send')) {

                            /*
                            *   Get Users who is susbscribed in mailing list.
                            */

                            //Load Home Model.
                            $this->loadModel('Home');

                            //Count Subscribe Users..
                            $users_count = $this->Home->find()
                                ->contain(['Profiles'])
                                ->where(['profiles.job_subscribe' => 1])
                                ->count();

                            //Query Users subscribe to mailing list.
                            $users = $this->Home->find()
                                ->select(['home.email'])
                                ->contain(['Profiles'])
                                ->where(['profiles.job_subscribe' => 1]);


                            $i = 0;

                            //Loop Results.
                            foreach ($users as $user) {
                                $i++;
                                
                                //Email Properties.
                                $email = [
                                    'from' => [$this->globalworks->_server_email_address, 'Globalworks'],
                                    'html' => true,
                                    'subject' => $this->request->data('subject'),
                                    'to' => $user['home']['email'],
                                    'message' => $this->request->data('content'),
                                ];

                                //Send Email Mesage.
                                $this->globalworks->mail($email);

                                if ($i == $users_count) {

                                    /*
                                    *   Create Log Record.
                                    */

                                    //Admin's Full Name.
                                    $admin_fullname = $this->request->session()->read('Auth.User')['firstname'] . ' ' . $this->request->session()->read('Auth.User')['lastname'];

                                    //Create Log Message.
                                    $log_msg = $admin_fullname . ' edit a mailing list. ('. $subject .') and send.';

                                    //Save Log.
                                    $this->globalworks->_log('admin_edit_mailinglist', $log_msg);

                                    echo 'ok';
                                }
                            }
                        } else {

                            /*
                            *   Create Log Record.
                            */

                            //Admin's Full Name.
                            $admin_fullname = $this->request->session()->read('Auth.User')['firstname'] . ' ' . $this->request->session()->read('Auth.User')['lastname'];

                            //Create Log Message.
                            $log_msg = $admin_fullname . ' edit a mailing list. ('. $subject .').';

                            //Save Log.
                            $this->globalworks->_log('admin_edit_mailinglist', $log_msg);

                            echo 'ok';
                        }
                    }
                break;

                //Delete Mailinglist.
                case 'delete_malinglist':

                    //Load Mailinglists Model.
                    $this->loadModel('Mailinglists');

                    //Query Mailinglists.
                    $lists = $this->Mailinglists->find()
                        ->select(['subject'])
                        ->where(['id' => $this->request->data('id')]);

                    //Subject Value.
                    $subject = '';

                    //Loop Results
                    foreach ($lists as $l) {

                        //Append Value.
                        $subject .= $l['subject'];
                    }

                    //Get Column Record.
                    $list = $this->Mailinglists->get($this->request->data('id'));

                    //Delete Record.
                    $this->Mailinglists->delete($list);

                    /*
                    *   Create Log Record.
                    */

                    //Admin's Full Name.
                    $admin_fullname = $this->request->session()->read('Auth.User')['firstname'] . ' ' . $this->request->session()->read('Auth.User')['lastname'];

                    //Create Log Message.
                    $log_msg = $admin_fullname . ' delete a mailing list. ('. $subject .').';

                    //Save Log.
                    $this->globalworks->_log('admin_delete_mailinglist', $log_msg);
                break;

                //Subscribers Lists.
                case 'subscribers_list':
                    
                    //Load Subscribers Model.
                    $this->loadModel('Subscribers');

                    //Query Subscribers.
                    $subscribers = $this->Subscribers->find()
                        ->order(['id' => 'DESC']);

                    //Enable View.
                    $this->autoRender = true;
                    $this->viewBuilder()->template('subscribers_list');

                    $this->set('subscribers', $subscribers);
                break;
                
                //Remove Subscriber.
                case 'remove_subscriber':
                    
                    //Load Subscribers Model.
                    $this->loadModel('Subscribers');

                    //Get Record.
                    $subscriber = $this->Subscribers->get($this->request->data('id'));

                    //Delete Subscriber.
                    if ($this->Subscribers->delete($subscriber)) {
                        echo 'ok';
                    }
                break;

                //Upload Profile Picture Page.
                case 'upload_pic_page':
                    
                    //Enable View.
                    $this->autoRender = true;
                    $this->viewBuilder()->template('profile_pic_upload');
                break;

                //Upload Profile Picture.
                case 'image_upload':

                    //Load Model.
                    $this->loadModel('Imgtmp');
                    
                    //User's Session ID.
                    $user_id = $this->request->session()->read('Auth.User')['id'];
                    
                    //Uploaded File properties.
                    $name = $_FILES['ddu-upload']['name'];
                    $error = $_FILES['ddu-upload']['error'];
                    $tmp = $_FILES['ddu-upload']['tmp_name'];
                    
                    //Path to upload directory.
                    $path = './img/users/tmp/';
                    
                    //If no upload error.
                    if ($error == 0) {
                        
                        //Get the file extension name.
                        $ext = explode('.', $name);
                        $ext = end($ext);
                        
                        //Random name value.
                        $random_name = '';
                        
                        //Characters to generate.
                        $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                        
                        //Generate random strings.
                        for ($p = 0; $p < 16; $p++) {
                            $random_name .= $chars[mt_rand(0, strlen($chars)-1)];
                        }
                        
                        //Random name for filename.
                        $random_name = $user_id . date( "YmdHis" ) . $random_name;
                        
                        //Count user's img_tmp record if exists or not.
                        $imgtmp_count = $this->Imgtmp->find()
                            ->where(['user_id' => $user_id])
                            ->count();
                        
                        //If user's img_tmp record not exists, create record.
                        if ($imgtmp_count == 0) {
                            
                            //Move Uploaded file to /img/users/tmp directory.
                            if (move_uploaded_file($tmp, $path . $random_name . '.' . $ext)) {
                                
                                //New Entity.
                                $imgtmp = $this->Imgtmp->newEntity();
                                
                                //Records to be insert in img_tmp database table.
                                $new_entity = [
                                    'user_id' => $user_id,
                                    'filename' => $random_name . '.' . $ext,
                                    'date' => date( "Y-m-d H:i:s" ),
                                ];
                                
                                //Patch Entity.
                                $imgtmp = $this->Imgtmp->patchEntity($imgtmp, $new_entity);
                                
                                //Create or Insert new Record.
                                if ($this->Imgtmp->save($imgtmp)) {
                                    echo 'ok';
                                }
                            }
                            
                        //If User img_tmp record exists. Update record.
                        } else {
                            
                            //Value of img_tmp id to append.
                            $id = '';
                            
                            //Value of image filename to append.
                            $old_img = '';
                            
                            //Get User's img_tmp record.
                            $imgtmps = $this->Imgtmp->find()
                                ->where(['user_id' => $user_id]);
                            
                            //Loop User's Record.
                            foreach ($imgtmps as $t) {
                                
                                //Append ID.
                                $id .= $t['id'];
                                
                                //Append Filename.
                                $old_img .= $t['filename'];
                            }
                            
                            //File destination directory and filename.
                            $file = $path . $old_img;
                            
                            //Get User's row record from img_tmp table.
                            $imgtmp = $this->Imgtmp->get($id);
                            
                            //Update filename record.
                            $imgtmp->filename = $random_name . '.' . $ext;
                            
                            //Update date record.
                            $imgtmp->date = date( "Y-m-d H:i:s" );
                            
                            //Save Changes.
                            if ($this->Imgtmp->save($imgtmp)) {
                                
                                /*
                                *   Delete old uploaded image and move new uploaded image.
                                */
                                
                                //If old image file exists.
                                if (file_exists($file)) {
                                    
                                    //Delete old file image.
                                    if (unlink($file)) {
                                        
                                        //Move the new one to the tmp directory.
                                        if (move_uploaded_file($tmp, $path . $random_name . '.' . $ext)) {
                                            echo 'ok';
                                        }
                                    }
                                }
                            }
                        }
                    }
                break;

                //Crop Admin Profile Picture Page.
                case 'crop_admin_profile_picture':
                    //Load Img_tmp Model
                    $this->loadModel('Img_tmp');

                    //Count to check if User has Img_tmp Record.
                    $img_count = $this->Img_tmp->find()
                        ->where(['user_id' => $this->request->session()->read('Auth.User')['id']])
                        ->count();

                    //If Count is greaterthan 0. Means User has record.
                    if ($img_count > 0) {

                        //Render view.
                        $this->autoRender = true;

                        //Set Template.
                        $this->viewBuilder()->template('admin_image_crop');

                        //Query out to get User's Record.
                        $imgs = $this->Img_tmp->find()
                            ->select(['filename'])
                            ->where(['user_id' => $this->request->session()->read('Auth.User')['id']]);

                        //To be append.
                        $filename = '';

                        //Loop Result.
                        foreach ($imgs as $img) {
                            $filename .= $img['filename'];
                        }

                        //Split filename via dot.
                        $explode = explode('.', $filename);

                        //If Filename extension is not .jpg, change file extension to .jpg as filename
                        if (end($explode) !== '.jpg') {
                            $new_filename = $explode[0] . '.jpg';
                        } else {
                            $new_filename = $filename;
                        }

                        //Render to view.
                        $this->set('filename', $filename);
                        $this->set('new_filename', $new_filename);
                    }
                break;

                //Admin Crop Profile Picture.
                case 'crop_image':

                    //User's Session ID from post request.
                    $user_id = $this->request->data('user_id');
                    
                    //Original filename of image from tmp folder.
                    $filename_orig = $this->request->data('filename');
                    
                    //File format type.
                    $type = $_FILES['cropperImage']['type'];
                    
                    //File upload error.
                    $error = $_FILES['cropperImage']['error'];
                    
                    //The Image File.
                    $tmp = $_FILES['cropperImage']['tmp_name'];
                    
                    //Path to /img/profile_picrutes/ directory.
                    $profile_picture_path = './img/users/profile_pictures/';
                    
                    //Path to /img/profile_thumbnails/ directory.
                    $thumbnail_picture_path = './img/users/profile_thumbnails/';
                    
                    //Get the filename w/o dot extension.
                    $filename = explode('.', $filename_orig);
                    $f_name = $filename[0];
                    
                    //File image cropped extension name.
                    $ext = '.jpg';
                    
                    //The new filename of image cropped.
                    $new_filename = $f_name . $ext;
                    
                    //If mo upload error.
                    if ($error == 0) {
                        
                        /*
                        *   Resizing image.
                        */
                        
                        //Type of image.
                        switch ($type) {
                                
                            //If image is jpeg format.
                            case 'image/jpeg':
                                
                                //Get the image dimensions.
                                list($width, $height) = getimagesize($tmp);
                                
                                /*
                                *   For Dimension 370x370.
                                */
                                //Set a new dimension.
                                $image = imagecreatetruecolor(370, 370);
                                
                                //Get the source file.
                                $image_src = imagecreatefromjpeg($tmp);
                                
                                //Initiating resizing image.
                                imagecopyresampled($image, $image_src, 0, 0, 0, 0, 370, 370, $width, $height);
                                
                                //Convert to jpeg and move to destination path.
                                imagejpeg($image, $profile_picture_path . $new_filename);
                                
                                /*
                                *   For Dimension 70x70.
                                */
                                //Set a new dimension.
                                $thumb_image = imagecreatetruecolor(70, 70);
                                
                                //Get the source file.
                                $thumb_image_src = imagecreatefromjpeg($tmp);
                                
                                //Initiating resizing image.
                                imagecopyresampled($thumb_image, $thumb_image_src, 0, 0, 0, 0, 70, 70, $width, $height);
                                
                                //Convert to jpeg and move to destination path.
                                imagejpeg($thumb_image, $thumbnail_picture_path . $new_filename);
                            break;
                            
                            //If image is jpeg format.
                            case 'image/png':
                                
                                //Get the image dimensions.
                                list($width, $height) = getimagesize($tmp);
                                
                                /*
                                *   For Dimension 370x370.
                                */
                                //Set a new dimension.
                                $image = imagecreatetruecolor(370, 370);
                                
                                //Get the source file.
                                $image_src = imagecreatefrompng($tmp);
                                
                                //Initiating resizing image.
                                imagecopyresampled($image, $image_src, 0, 0, 0, 0, 370, 370, $width, $height);
                                
                                //Convert to jpeg and move to destination path.
                                imagejpeg($image, $profile_picture_path . $new_filename);
                                
                                /*
                                *   For Dimension 70x70.
                                */
                                //Set a new dimension.
                                $thumb_image = imagecreatetruecolor(70, 70);
                                
                                //Get the source file.
                                $thumb_image_src = imagecreatefrompng($tmp);
                                
                                //Initiating resizing image.
                                imagecopyresampled($thumb_image, $thumb_image_src, 0, 0, 0, 0, 70, 70, $width, $height);
                                
                                //Convert to jpeg and move to destination path.
                                imagejpeg($thumb_image, $thumbnail_picture_path . $new_filename);
                            break;
                        }
                        
                        /*
                        *   Checking if User's has a Profile record.
                        *   If User has no profile record create one.
                        *   If User has already a profile record, set record to profile_picture column.
                        *   If User Profile profile_picture is already set, update record and remove old images and replace with the new cropped images.
                        */
                        
                        //Load Model.
                        $this->loadModel('Profiles');
                        
                        //Count User's Profile record if exists or not.
                        $profiles_count = $this->Profiles->find()
                            ->where(['user_id' => $user_id])
                            ->count();
                        
                        //If User's Profile record does not exists, create record.
                        if ($profiles_count == 0) {
                            
                            //New Entity.
                            $profiles = $this->Profiles->newEntity();
                            
                            //Records to be insert in Profiles database table.
                            $new_entity = [
                                'user_id' => $user_id,
                                'profile_picture' => $new_filename,
                            ];
                            
                            //Patch Entity.
                            $profiles = $this->Profiles->patchEntity($profiles, $new_entity);
                            
                            //Create or Insert Record to the Profiles Table.
                            if ($this->Profiles->save($profiles)) {
                                
                                //Delete User's img_tmp record and temporary image from tmp directory.
                                if ($this->globalworks->deletetmpimg($user_id, $filename_orig)) {
                                    echo 'ok';
                                }
                            }
                            
                        //If User's Profile Record is Exists, Update Record.
                        } else {
                            
                            //Get User's Profile record.
                            $profiles = $this->Profiles->find()
                                ->where(['user_id' => $user_id]);
                            
                            //User's Profile ID row to append.
                            $id = '';
                            
                            //User's profile_picture value to append.
                            $profile_picture = '';
                            
                            //Loop User's Profile results.
                            foreach ($profiles as $pro) {
                                
                                //Append ID.
                                $id .= $pro['id'];
                                
                                //Append profile_picture.
                                $profile_picture .= $pro['profile_picture'];
                            }
                            
                            //Get User's Profile Row.
                            $profile = $this->Profiles->get($id);
                            
                            //If profile_picture is null or empty.
                            if ($profile_picture == '') {
                                
                                //Update profile_picture record.
                                $profile->profile_picture = $new_filename;                           
                                
                                //Save Changes.
                                if ($this->Profiles->save($profile)) {
                                    
                                    //Delete User's img_tmp record and delete image from tmp directory.
                                    if ($this->globalworks->deletetmpimg($user_id, $filename_orig)) {
                                        echo 'ok';
                                    }
                                }
                            
                            //If profile_picture is not empty or not null.
                            } else {
                                
                                //Update Record and Delete old profile pictures.
                                if ($this->globalworks->delete_exisiting_profile_picture($user_id, $profile_picture, $new_filename)) {
                                    
                                    //Update profile_picture record.
                                    $profile->profile_picture = $new_filename;
                                    
                                    //Delete User's img_tmp record and temporary image from tmp directory.
                                    if ($this->globalworks->deletetmpimg($user_id, $filename_orig)) {
                                        echo 'ok';
                                    }
                                }
                            }
                        }
                    }
                break;
            }
        }
    }
}
