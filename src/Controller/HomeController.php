<?php

namespace App\Controller;

use Cake\Event\Event;

class HomeController extends AppController {
    
    private $globalworks;
    
    public function beforeFilter(Event $event) {

        parent::beforeFilter($event);
        
        $this->globalworks = new GlobalworksController();
        
        //Allowed Page without Auth.
        $this->Auth->allow([
            'index', 
            'register', 
            'logout', 
            'login', 
            'fblogin', 
            'candidateCallback', 
            'employerCallback', 
            'noEmail', 
            'fbUserPasswordAuth', 
            'forgotPassword', 
            'passwordrecovery',
            'aboutus',
            'privacypolicy',
            'tos',
            'disclaimer',
            'subscribe',
            'unsubscribe',
            'administrators',
        ]);

        //Check Visitor's IP Info.
        $this->globalworks->ip_info_init();

        //Check User's Account If Exists.
        $this->globalworks->check_auth();
 
        //Check if FB User has not yet complete filling his basic information.
        if (!$this->globalworks->fb_user_check()) {
            return $this->redirect('/basicinfo');
        }

        //Check User has not confirm his account yet.
        if ($this->globalworks->check_confirmed_user()) {
            $this->redirect('/account_confirmation');
        }

        //Announcement.
        $this->set('pop_announcement', $this->globalworks->show_announcement());

        //Check if user is suspended. Auto logout user.
        return $this->globalworks->is_suspended();
    }
    
	public function index() {
        $title = 'Home - Globalworks';

        $this->set('title', $title);   
        $this->set('username', $this->globalworks->welcome_greetings());
        $this->set('thumbnail', $this->globalworks->image_thumbnail());
	}
    
    /*
    *   Main Login Action.
    *   Route: => "/login"
    */
    public function login() {
        //Check if user has UserSession.
        if (!$this->request->session()->read('Auth.User')) {

            $title = 'Login - Globalworks';
            
            //Model Entity.
            $home = $this->Home->newEntity(['validate' => 'login']);
            
            //If has post request.
            if ($this->request->is('post')) {

                //Abort Session started by Facebook Component to avoid Session Conflicts.
                session_abort();
                
                //Patch Entity to apply form validation and changes.
                $home = $this->Home->patchEntity($home, $this->request->data, ['validate' => 'login']);
                
                /*
                *   Flash Error Messages.
                */
                
                //If Email field is empty.
                if (isset($home->errors()['email']['_empty'])) {
                    
                    //Flash Email error message.
                    $this->Flash->error($home->errors()['email']['_empty']);
                
                //If Password field is empty.
                } else if (isset($home->errors()['password']['_empty'])) {
                    
                    //Flash Password error message.
                    $this->Flash->error($home->errors()['password']['_empty']);
                } else {
                    
                    //Recaptcha Validation.
                    if (!$this->Recaptcha->verify()) {
                        
                        //Flash Recaptcha error message.
                        $this->Flash->error(__('Please Confirm that you are a human.'));
                    } else {

                        //Identify email and password.
                        $user = $this->Auth->identify();
                        
                        //If Email and Password matches.
                        if ($user) {

                            //Check if user is suspended or not.
                            $users = $this->Home->find()
                                ->select(['suspend'])
                                ->where(['email' => $this->request->data('email')]);

                            $suspend = 0;

                            foreach ($users as $u) {
                                $suspend = $u['suspend'];
                            }

                            //If User is suspend.
                            if ($suspend) {
                                $this->Flash->error(__('Your account has been suspended.'));
                            } else {
                                //Get users row from DB Users Table.
                                $home2 = $this->Home->get($user['id']);
                                
                                //Fields to be update.
                                $home2->current_ip = $_SERVER['REMOTE_ADDR'];
                                $home2->last_login = date( "Y-m-d H:i:s" );

                                //Update last_login and current_ip.
                                $this->Home->save($home2);
                                
                                //User's Role.
                                $role = $user['role'];
                                
                                //Set user loggin session.
                                $this->Auth->setUser($user);

                                //Create Log Message.
                                $log_msg = $this->request->data('email') . ' Logged in.';

                                //Save Log.
                                $this->globalworks->_log('login_log', $log_msg);
                                
                                //Redirect Logged user in right page. depends on role.
                                if ($role == 'Applicant') {

                                    if (isset($_GET['page']) && isset($_GET['jobID'])) {

                                        //Redirect to the preview page that requires login auth.
                                        return $this->redirect('/jobs/category/'. $_GET['page'] . '?jobID=' . $_GET['jobID']);
                                    } else {
                                    
                                        //Redirect to default login redirect.
                                        return $this->redirect($this->Auth->redirectUrl());
                                    }
                                } else if ($role == 'Employer') {
                                    
                                    //Redirect employer to employer's dashboard.
                                    return $this->redirect('/employers/dashboard');
                                } else if ($role == 'Admin' || $role == 'Super Admin') {
                                    
                                    //Redirect employer to employer's dashboard.
                                    return $this->redirect('/admin/dashboard');
                                }
                            }
                        
                        //If Email and Password Missmatch.
                        } else {
                            
                            //Flash Error Message.
                            $this->Flash->error(__('Invalid email or password.'));
                        }   
                    }
                }
            }

            $this->set('title', $title);
            $this->set('home', $home);   
        } else {
            return $this->globalworks->redirect2();
        }
    }
    
    /*
    *   User's Registration Action.
    *   Route: => "/Register"
    */
    public function register() {
        if (!$this->request->session()->read('Auth.User')) {
            //Page title.
            $title = 'Register - Globalworks';

            //Model Entity.
            $home = $this->Home->newEntity();

            //If has post request.
            if ($this->request->is('post')) {

                //Abort Session started by Facebook Component to avoid Session Conflicts.
                session_abort();

                //Characters to generate.
                $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

                //To be Append.
                $code = '';
                        
                //Generate random strings.
                for ($p = 0; $p < 6; $p++) {
                    $code .= $chars[mt_rand(0, strlen($chars)-1)];
                }

                //Value of user's birthday.
                $birthday = $this->request->data('month') . '/' . $this->request->data('day') . '/' . $this->request->data('year');
                
                //If Request URI has employer.
                if ($_SERVER['REQUEST_URI'] == '/register/employers' || $_SERVER['REQUEST_URI'] == '/register/employers/') {
                    $role = 'Employer';
                    $status = 'Inactive';
                    
                //If Request URI has no employer.
                } else {
                    $role = 'Applicant';
                    $status = 'Active';
                }
                
                //Fields new entity.
                $new_entity = [
                    'email' => $this->request->data('email'),
                    'password' => $this->request->data('password'),
                    'rpassword' => $this->request->data('rpassword'),
                    'role' => $role,
                    'status' => $status,
                    'firstname' => $this->request->data('firstname'),
                    'lastname' => $this->request->data('lastname'),
                    'gender' => $this->request->data('gender'),
                    'birthday' => $birthday,
                    'address' => $this->request->data('address'),
                    'region' => $this->request->data('region'),
                    'country' => $this->request->data('country'),
                    'contact_number' => $this->request->data('contact_number'),
                    'confirmation_code' => $code,
                    'date_created' => date( "Y-m-d H:i:s" ),
                    'current_ip' => $_SERVER['REMOTE_ADDR'],
                    'registered_ip' => $_SERVER['REMOTE_ADDR'],
                    'last_login' => date( "Y-m-d H:i:s" ),
                    'fb_user' => 0,
                    'fb_complete_reg' => 1,
                ];

                //Patch Entity. To apply form validation.
                $home = $this->Home->patchEntity($home, $new_entity);

                //Recaptcha Validation.
                if ($this->Recaptcha->verify()) {

                    //Saving Data.
                    if ($this->Home->save($home)) {

                        //Create Message Log.
                        $msg_log = $_SERVER['REMOTE_ADDR'] . ' create an account. Email: ' . $this->request->data('email');

                        //Save Log.
                        $this->globalworks->_log('register_log', $msg_log);

                        /*
                        *   Send Confirmation code to user's email.
                        */
                        $from = [$this->globalworks->_server_email_address, 'Globalworks'];
                        $subject = 'Globalworks Account Confirmation.';
                        $to = $this->request->data('email');
                        $message = '
                            <h1 style="texta-align: center;">Thank you for using Globalworks.</h1>
                            <p>Your Confirmation Code:</p>
                            <p style="border: 1px solid #ccc; font-size: 20px; padding: 10px; text-align: center;">
                                ' . $code . '
                            </p>
                        ';

                        //Email Message.
                        $mail = [
                            'from' => $from,
                            'html' => true,
                            'subject' => $subject,
                            'to' => $to,
                            'message' => $message,
                        ];

                        //Send Mail.
                        $this->globalworks->mail($mail);

                        //Identify New User.
                        $user = $this->Auth->identify();

                        //Login or setUser new user.
                        if ($user) {

                            /*
                            *   Check if email is subscribe to the mailing list.
                            *   If Exist delete email.
                            */
                            if ($this->globalworks->is_subscribe($this->request->data('email'))) {

                                //Delete Email from Subscribers Table.
                                $this->globalworks->unsubscribe($this->request->data('email'));
                            }
                            
                            //Set user loggin session.
                            $this->Auth->setUser($user);

                            $this->Flash->success(__(' A confirmation code has been sent to ' . $this->request->data('email') . '.'));
                            
                            //Redirect New User to set profile.
                            return $this->redirect('/account_confirmation');

                        } else {
                            $this->Flash->error(__('Failed to login.'));
                        }
                    }
                } else {
                    $this->Flash->error(__('Please verify that you are a human.'));
                }
            }

            //Render title to view.
            $this->set('title', $title);

            //Render home model to view.
            $this->set('home', $home);

            //Validation errors rendender to view.
            if ($home->errors() !== null) {
                $this->set('errors', $home->errors());
            }   
        } else {
            return $this->globalworks->redirect2();
        }
    }

    /*
    *   Confirmation Account Page.
    */
    public function confirmAccount() {

        //If User has Login Session.
        if ($this->request->session()->read('Auth.User')) {

            $title = 'Confirm Account - Globalworks';

            //Toggle if Resend Button is clicked.
            $resend = false;

            //Query out to get User's Email.
            $users = $this->Home->find()
                ->select(['email', 'confirmation_code'])
                ->where(['id' => $this->request->session()->read('Auth.User')['id']]);

            //To be Append.
            $email = '';

            //To be Append.
            $confirmation_code = '';

            //Loop results to get information.
            foreach ($users as $user) {

                //Append Email.
                $email .= $user['email'];

                //Append confirmation_code.
                $confirmation_code .= $user['confirmation_code'];
            }

            //New Entity.
            $confirm = $this->Home->newEntity(['validate' => 'confirmaccount']);

            //If has POST or PUT request.
            if ($this->request->is('post') || $this->request->is('put')) {

                /*
                *  Confirming User's Account.
                */
                if ($this->request->data('option') == 'confirm') {
                   
                    //Patch Entity.
                    $confirm = $this->Home->patchEntity($confirm, $this->request->data, ['validate' => 'confirmaccount']);

                    //If no Validation Errors.
                    if (!$confirm->errors()) {

                        //If Confirmation Matched.
                        if ($this->request->data('confirmation_code') !== $confirmation_code) {
                            $this->Flash->error(__('Invalid Confirmation Code.'));
                        } else {

                            //Get User's Column via ID.
                            $user = $this->Home->get($this->request->session()->read('Auth.User')['id']);

                            //Update confirmation_code.
                            $user->confirmation_code = '';

                            //Save Update.
                            if ($this->Home->save($user)) {

                                /*
                                *   Retrive User's Information and Change User's Login Session.
                                */

                                //Abort Session.
                                session_abort();

                                //To be append.
                                $info;

                                //Query out User's Information.
                                $users = $this->Home->find()
                                    ->where(['email' => $email]);

                                //Loop results to append in $info.
                                foreach ($users as $user => $val) {

                                    //Append Value.
                                    $info = $val;
                                }

                                //Set User Login Session.
                                $this->Auth->setUser($info);

                                //Create Log Message.
                                $log_msg = $email . ' confirmed his/her account';

                                //Save Log.
                                $this->globalworks->_log('account_confirm_log', $log_msg);

                                //If Account is new.
                                if ($this->globalworks->uri(2) == 'new') {
                                    return $this->redirect('/setprofile');
                                } else {
                                    //Redirect to dashboard depends on the role.
                                    if ($info['role'] == 'Applicant') {
                                        return $this->redirect('/candidates/dashboard');
                                    } else if ($info['role'] == 'Employer') {
                                        return $this->redirect('/employers/dashboard');
                                    }
                                }
                            }
                        }
                    }
                
                /*
                *   Resend Confirmation Code.
                */
                } else if ($this->request->data('option') == 'resend') {
                    
                    //Email Message.
                    $from = [$this->globalworks->_server_email_address, 'Globalworks'];
                    $subject = 'Globalworks Account Confirmation.';
                    $to = $email;
                    $message = '
                        <h3 style="texta-align: center;">You requested for a confirmation code.</h3>
                        <p>Your Confirmation Code:</p>
                        <p style="border: 1px solid #ccc; font-size: 20px; padding: 10px; text-align: center;">
                            ' . $confirmation_code . '
                        </p>
                    ';

                    $new_mail = [
                        'from' => $from,
                        'html' => true,
                        'subject' => $subject,
                        'to' => $to,
                        'message' => $message,
                    ];

                    //Send Email.
                    if ($this->globalworks->mail($new_mail)) {
                        $this->Flash->success(__('Confirmation Code Sent!'));

                        $resend = true;
                    }
                }
            }
            
            $this->set('title', $title);
            $this->set('confirm', $confirm);
            $this->set('resend', $resend);
            $this->set('email', $email);
            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());
        } else {
            return $this->globalworks->redirect2();
        }
    }
    
    public function logout() {

        //Abort Session started by Facebook Component to avoid Session Conflicts.
        session_abort();
        
        //Redirect to login page.
        return $this->redirect($this->Auth->logout());
    }
    
    /*
    *   Set User's Profile Action.
    *   Route: => "/setuser"
    *
    *   Associated Templates: Home/setprofile and Home/setprofile_employer.
    */
    public function setprofile() {

        //Page Title.
        $title = 'Edit Profile - Globalworks Philippines';
        
        //Load Profiles Model.
        $this->loadModel('Profiles');
        
        //Check if user has already a profile.
        $has_profile = $this->Profiles->find()
            ->where(['user_id' => $this->request->session()->read('Auth.User')['id']])
            ->count();

            //Redirect user to dashboard if user has already a profile.
        if ($has_profile) {
            return $this->globalworks->redirect2();
        }
        
        //User's Session ID.
        $user_id = $this->request->session()->read('Auth.User')['id'];
        
        //Get User Role.
        $role = $this->request->session()->read('Auth.User')['role'];
        
        //Set templates for user's depends on the role.
        if ($role == 'Applicant') {
            $this->viewBuilder()->template('setprofile');
        } else if ($role == 'Employer') {
            $this->viewBuilder()->template('setprofile_employer');
        }
        
        //Count profile rows to get user's profile row.
        $profiles_count = $this->Profiles->find()
            ->where(['user_id' => $user_id])
            ->count();
        
        //If Employer's profile exists. 
        if ($profiles_count && $role == 'Employer') {
            
            //Get User's Profile.
            $user_profile = $this->Profiles->find()
                ->where(['user_id', $user_id]);
            
            //Loop and render to view.
            foreach ($user_profile as $pro) {
                $this->set('business_name', $pro['business_name']);
                $this->set('business_description', $pro['business_description']);
                $this->set('business_email', $pro['business_email']);
                $this->set('business_contact_number', $pro['business_contact_number']);
                $this->set('business_website', $pro['business_website']);
                $this->set('business_address', $pro['business_address']);
                $this->set('business_state', $pro['business_state']);
                $this->set('business_country', $pro['business_country']);
            }
        }
        
        $this->set('title', $title);
        $this->set('username', $this->globalworks->welcome_greetings());
        $this->set('thumbnail', $this->globalworks->image_thumbnail());
    }
    
    /*
    *   User Profile Picture Upload Page.
    *   Route: "/uploadpicture/*"
    *   
    *   Associated Controllers, Actions and Ajax Operation.
    *   Controller => Ajax, Action => init, Ajax Operation => image_upload.
    */
    public function uploadpicture() {

        //Load Profiles Model.
        $this->loadModel('Profiles');

        //Check if user has already a profile picture.
        $profiles_count = $this->Profiles->find()
            ->where(['user_id' => $this->request->session()->read('Auth.User')['id']])
            ->where(['profile_picture !=' => ''])
            ->count();

        //Redirect user to dashboard if user has already a profile picture.
        if ($profiles_count) {
            return $this->globalworks->redirect2();
        }

        //Get User Role.
        $role = $this->request->session()->read('Auth.User')['role'];
        
        $title = 'Upload Profile Picture - Globalworks Philippines';
        $this->set('title', $title);
        $this->set('username', $this->globalworks->welcome_greetings());
        $this->set('thumbnail', $this->globalworks->image_thumbnail());
        $this->set('role', $role);
    }
    
    /*
    *   Users Image Croping Page.
    *   Route: "/imagecrop/*";
    *
    *   Associated Controllers, Actions and Ajax Operation.
    *   Controller => Ajax, Action => init, Ajax Operation => crop_image.
    */
    public function imagecrop() {

        //Load Profiles Model.
        $this->loadModel('Profiles');

        //Check if user has already a profile picture.
        $profiles_count = $this->Profiles->find()
            ->where(['user_id' => $this->request->session()->read('Auth.User')['id']])
            ->where(['profile_picture !=' => ''])
            ->count();

        //Redirect user to dashboard if user has already a profile picture.
        if ($profiles_count) {
            return $this->globalworks->redirect2();
        }

        //Get User Role.
        $role = $this->request->session()->read('Auth.User')['role'];
        
        //Page Title.
        $title = 'Crop Profile Picture - Globalworks Philippines';
        
        //Load Model.
        $this->loadModel('Imgtmp');
        
        //User's Session ID.
        $user_id = $this->request->session()->read('Auth.User')['id'];
        
        /*
        *   Uploaded profile pictures are stored in "/img/users/tmp" as temporary storage.
        *   Image will be removed if user is already cropped the image.
        */
        
        //Count img_tmp user's table row.
        $imgtmp_count = $this->Imgtmp->find()
            ->where(['user_id' => $user_id])
            ->count();
        
        //If User's img_tmp record is exists.
        if ($imgtmp_count !== 0) {
            
            //Get User's img_tmp record.
            $imgtmp = $this->Imgtmp->find()
                ->where(['user_id' => $user_id]);
            
            //Filename val to append.
            $filename = '';
            
            //Loop img_tmp results.
            foreach ($imgtmp as $img) {
                
                //Append Filename.
                $filename .= $img['filename'];
            }
            
            //Render filename to view.
            $this->set('filename', $filename);
            
        //If User's img_tmp record is not exists.
        } else {
            return $this->redirect($this->Auth->redirectUrl());
        }
        
        $this->set('title', $title);
        $this->set('username', $this->globalworks->welcome_greetings());
        $this->set('thumbnail', $this->globalworks->image_thumbnail());
        $this->set('role', $role);
    }
    
    /*
    *   User's Upload Resume Page.
    *   Route: "/uploadresume/*"
    *
    *   Associated Controllers, Actions and Ajax Operation.
    *   Controller => Ajax, Action => init, Ajax Operation => resume_upload.
    */
    public function uploadresume() {
        //Get User Role.
        $role = $this->request->session()->read('Auth.User')['role'];
        
        //If role is Applicant.
        if ($role == 'Applicant') {
            //Page Title.
            $title = 'Upload Resume - Globalworks Philippines';

            //User's Session ID.
            $user_id = $this->request->session()->read('Auth.User')['id'];

            //Load Model.
            $this->loadModel('Resumes');

            //Count User's Resume record.
            $resumes_count = $this->Resumes->find()
                ->where(['user_id' => $user_id])
                ->count();

            //If User's Resume record is exists.
            if ($resumes_count == 1) {

                //Get User's Resume Record.
                $resumes = $this->Resumes->find()
                    ->where(['user_id' => $user_id]);

                //Loop Records and render to view.
                foreach ($resumes as $resume) {
                    $this->set('resume_file', $resume['filename']);
                    $this->set('resume_details', $resume['details']);
                }
            }

            $this->set('title', $title);
            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());   
        } else {
            //redirect to somewhere.
            $this->autoRender = false;
            $this->viewBuilder()->setLayout(false);
        }
    }

    /*
    *   Facebook Login Candidate's Callback.
    */
    public function candidateCallback() {

        //If User Click Cancel while attempting to login.
        if ($this->request->query('error') == 'access_denied') {
            return $this->redirect('/');
        }

        //Disable View.
        $this->autoRender = false;
        $this->viewBuilder()->setLayout(false);
        
        //The Callback.
        return $this->globalworks->users_callback('Applicant');
    }

    /*
    *   Facebook Login Employer's Callback.
    */
    public function employerCallback() {
        if ($this->request->query('error') == 'access_denied') {
            return $this->redirect('/');
        }

        //Disable View.
        $this->autoRender = false;
        $this->viewBuilder()->setLayout(false);
        
        //The Callback.
        return $this->globalworks->users_callback('Employer');
    }

    /*
    *   If User uncheck email while attempting to login with facebook. redirect in this page.
    */
    public function noEmail() {
        
        //If User has Auth Session. redirect them.
        if ($this->request->session()->read('Auth.User')) {
            return $this->globalworks->redirect2();
        }
    }

    /*
    *   Basic Information Page. User will be redirect here when Login with Facebook when 1st time logged in.
    */
    public function basicinfo() {
        
        //If User has Applicant Role and If User has no Basic Information.
        if ($this->request->session()->read('Auth.User')['fb_user'] == 1 && ($this->request->session()->read('Auth.User')['fb_complete_reg'] == 0 || !$this->request->session()->read('Auth.User')['fb_complete_reg'])) {

            //Load Ip_logs Table.
            $this->loadModel('Ip_logs');

            //Query out to get User's IP Information.
            $iplogs = $this->Ip_logs->find()
                ->select(['city', 'country_code', 'region_name'])
                ->where(['query' => $_SERVER['REMOTE_ADDR']]);

            //To be append.
            $city = '';

            //To be append.
            $country_code = '';


            //To be append.
            $region = '';

            //Loop results to get IP information.
            foreach ($iplogs as $log) {
                
                //Append City.
                $city .=  $log['city'];

                //Append country_code.
                $country_code .= $log['country_code'];

                //Append region.
                $region .= $log['region_name'];
            }

            //New Entity.
            $users = $this->Home->newEntity(['validate' => 'basic']);

            //If has POST or PUT request.
            if ($this->request->is('post') || $this->request->is('put')) {
                
                //Get User's record via ID.
                $user = $this->Home->get($this->request->session()->read('Auth.User')['id']);

                //Append columns for update.
                $user->firstname = $this->request->data('firstname');
                $user->lastname = $this->request->data('lastname');
                $user->gender = $this->request->data('gender');
                $user->birthday = $this->request->data('month') . '/' . $this->request->data('day') . '/' . $this->request->data('year');
                $user->address = $this->request->data('address');
                $user->region = $this->request->data('region');
                $user->country = $this->request->data('country');
                $user->contact_number = $this->request->data('contact_number');
                $user->fb_complete_reg = 1;

                //Patch Entity.
                $users = $this->Home->patchEntity($user, $this->request->data, ['validate' => 'basic']);

                //Save Updates.
                if ($this->Home->save($users)) {
                    
                    //Redirect to profile setup.
                    return $this->redirect('/setprofile');
                } else {

                    //Last form state.
                    $this->set('firstname', $this->request->data('firstname'));
                    $this->set('lastname', $this->request->data('lastname'));
                    $this->set('gender', $this->request->data('gender'));
                    $this->set('address', $this->request->data('address'));
                    $this->set('region', $this->request->data('region'));
                    $this->set('country', $this->request->data('country'));
                    $this->set('city', $city);
                    $this->set('contact_number', $this->request->data('contact_number'));
                    $this->set('region', $region);
                }
            } else {
                $this->set('firstname', $this->request->session()->read('Auth.User')['firstname']);
                $this->set('lastname', $this->request->session()->read('Auth.User')['lastname']);
                $this->set('gender', $this->request->session()->read('Auth.User')['gender']);
                $this->set('city', $city);
                $this->set('country_code', $country_code);
                $this->set('region', $region);
            }

            $this->set('users', $users);
            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());
        } else {
            return $this->globalworks->redirect2();
        }
    }

    /*
    *   Password Authentication for Users who login with Facebook.
    */
    public function fbUserPasswordAuth($email = null) {
        if (!$email) {
            return $this->redirect('/');
        }

        $title = 'Verify your Account - Globalworks';

        //Verify email is already exists in the database and fb_user != 1.
        $email_count = $this->Home->find()
            ->where(['email' => $email])
            ->andWhere(['fb_user !=' => 1])
            ->count();

        if ($email_count > 0) {

            //New Entity.
            $users = $this->Home->newEntity(['validate' => 'fbpassword']);

            //If has POST or PUT Request.
            if ($this->request->is('post') || $this->request->is('put')) {

                //Patch Entity.
                $users = $this->Home->patchEntity($users, $this->request->data, ['validate' => 'fbpassword']);

                //If Recaptcha is checked.
                if ($this->Recaptcha->verify()) {

                    //Verifying User's Credentials. Email and Password.
                    $user = $this->Auth->identify();

                    //If Identify returned True.
                    if ($user) {
                        
                        //Get User's Account Informations.
                        $fb_users = $this->Home->find()
                            ->where(['email' => $email]);

                        //Abort Last Session.
                        session_abort();

                        //To be Append.
                        $new_user;

                        //Loop Results to get iformations.
                        foreach ($fb_users as $fb_user => $val) {
                            
                            //Append new_user.
                            $new_user = $val;
                        }

                        //Get User's Table Column.
                        $user = $this->Home->get($new_user['id']);

                        //Update some records.
                        $user->fb_user = 1;
                        $user->fb_complete_reg = 1;
                        $user->current_ip = $_SERVER['REMOTE_ADDR'];
                        $user->last_login = date( "Y-m-d H:i:s" );

                        //Save Update.
                        if ($this->Home->save($user)) {

                            //Set User's Login Session.
                            $this->Auth->setUser($new_user);

                            //Redirect to dashboard.
                            if ($new_user['role'] == 'Applicant') {
                                return $this->redirect('/candidates/dashboard');
                            } else if ($new_user['role'] == 'Employer') {
                                return $this->redirect('/employers/dashboard');
                            }
                        }

                    //If Returned False.
                    } else {
                        
                        //Set Error Message.
                        $this->Flash->error(__('Password is invalid.'));
                    }
                } else {
                    //Flash Recaptcha error message.
                    $this->Flash->error(__('Please Confirm that you are a human.'));
                }
            }

            $this->set('title', $title);
            $this->set('users', $users);
            $this->set('email', $email);
            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());
        }
    }

    /*
    *   Forgot Password Page.
    */
    public function forgotPassword() {

        $title = 'Forgot Password - Globalworks';

        //If User has no Login Session.
        if (!$this->request->session()->read('Auth.User')) {

            //New Entity.
            $users = $this->Home->newEntity(['validate' => 'forgotpassword']);

            //Disable Form rederer.
            $this->set('disabled', false);
            
            //If has POST or PUT Request.
            if ($this->request->is('post') || $this->request->is('put')) {

                $users = $this->Home->patchEntity($users, $this->request->data, ['validate' => 'forgotpassword']);
                
                //If has Form Validation Error.
                if (isset($users->errors()['email']['format'])) {
                    $this->Flash->error($users->errors()['email']['format']);
                } else {

                    /*
                    *   Check if Email is existed in the database.
                    */
                    $email_count = $this->Home->find()
                        ->where(['email' => $this->request->data('email')])
                        ->count();

                    //If count is = 0. (Not Exists).
                    if ($email_count == 0) {
                        $this->Flash->error(__('Email address is not existed in the database.'));
                    } else {
                        /*
                        *   Generate Random String for password recovery.
                        */

                        //Random name value.
                        $code = '';
                            
                        //Characters to generate.
                        $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                        
                        //Generate random strings.
                        for ($p = 0; $p < 16; $p++) {
                            $code .= $chars[mt_rand(0, strlen($chars)-1)];
                        }

                        $message = '
                            <p>You requested for a password recovery. Click the link below.</p>
                            <p><a href="' . $this->globalworks->_url . '/change_password/'. $this->request->data('email') . '/' . $code .'">' . $this->globalworks->_url . '/change_password/'. $this->request->data('email') . '/' . $code .'</a></p>
                        ';

                        /*
                        *   Insert Random String to password_recovery_code.
                        */
                        //Query out to find ID via Email.
                        $users = $this->Home->find()
                            ->select(['id'])
                            ->where(['email' => $this->request->data('email')]);

                        //To be append.
                        $id = '';

                        //Loop to get result.
                        foreach ($users as $user) {
                            
                            //Append ID.
                            $id .= $user['id'];
                        }

                        //Get User's Record column.
                        $user = $this->Home->get($id);

                        //Append Random String Code.
                        $user->password_recovery_code = $code;

                        //Save password_recover_code.
                        if ($this->Home->save($user)) {
                            $from = [$this->globalworks->_server_email_address, 'Globalworks'];
                            $to = $this->request->data('email');
                            $subject = 'Password Recovery.';

                            $email_contents = [
                                'from' => $from,
                                'to' => $to,
                                'subject' => $subject,
                                'html' => true,
                                'message' => $message,
                            ];

                            if ($this->globalworks->mail($email_contents)) {
                                $this->Flash->success(__('A password recovery link has been sent to ' . $this->request->data('email') . '.'));
                            
                                //Disable Form renderer.
                                $this->set('disabled', true);

                                //Create Log Message.
                                $log_msg = $this->request->data('email') . ' request for password.';

                                //Save Log.
                                $this->globalworks->_log('forgot_password_log', $log_msg);
                            }
                        }
                    }
                }
            }

            $this->set('title', $title);
            $this->set('users', $users);
        } else {
            return $this->globalworks->redirect2();
        }
    }

    /*
    *   Password Recovery Page.
    */
    public function passwordrecovery($email, $code) {

        $title = 'Password Recovery - Globalworks';

        //If Email and Code is null or empty.
        if (!$email && !$code) {
            return $this->globalworks->redirect2();
        } else {
            //Authenticate the code.
            $users_count = $this->Home->find()
                ->where(['email' => $email])
                ->andWhere(['password_recovery_code' => $code])
                ->count();

            //If Confirmation code matches password_recovery_code. If $users_count returned == 0.
            if ($users_count == 0) {
                return $this->globalworks->redirect2();
            } else {

                //Javascript page redirect.
                $this->set('redirect', false);
                
                //New Entity.
                $passwords = $this->Home->newEntity(['validate' => 'passwordrecovery']);

                //If has POST or PUT request.
                if ($this->request->is('post') || $this->request->is('put')) {
                    
                    //Patch Entity.
                    $passwords = $this->Home->patchEntity($passwords, $this->request->data, ['validate' => 'passwordrecovery']);

                    //If has no validation errors.
                    if (!$passwords->errors()) {

                        //Query out to get ID.
                        $users = $this->Home->find()
                            ->select(['id'])
                            ->where(['email' => $email])
                            ->andWhere(['password_recovery_code' => $code]);

                        //To be append.
                        $id = '';

                        //Loop result to get ID.
                        foreach ($users as $user) {
                            
                            //Append ID.
                            $id .= $user['id'];
                        }

                        //Get User's Column via ID.
                        $user = $this->Home->get($id);

                        //Apply Changes.
                        $user->password = $this->request->data('new-password');
                        $user->password_recovery_code = '';

                        //Save Changes.
                        if ($this->Home->save($user)) {
                            $this->Flash->success(__('Password has been changed! Redirect to homepage.'));

                            //Create Log Message.
                            $log_msg = $email . ' recovered its password.';

                            //Save Log.
                            $this->globalworks->_log('password_recovery_log', $log_msg);
                            
                            //Enable Javascript page redirect.
                            $this->set('redirect', true);
                        }
                    }
                }

                $this->set('title', $title);
                $this->set('passwords', $passwords);
            }
        }
    }

    /*
    *   About Us Page.
    */
    public function aboutus() {
        $title = 'About Us - Globalworks';

        $this->set('title', $title);   
        $this->set('username', $this->globalworks->welcome_greetings());
        $this->set('thumbnail', $this->globalworks->image_thumbnail());
    }

    /*
    *   Privacy Policy Page.
    */
    public function privacypolicy() {
        $title = 'Privacy Policy - Globalworks';

        $this->set('title', $title);   
        $this->set('username', $this->globalworks->welcome_greetings());
        $this->set('thumbnail', $this->globalworks->image_thumbnail());
    }

    /*
    *   Terms of Use Page.
    */
    public function tos() {
        $title = 'Terms of Use - Globalworks';

        $this->set('title', $title);   
        $this->set('username', $this->globalworks->welcome_greetings());
        $this->set('thumbnail', $this->globalworks->image_thumbnail());
    }

    /*
    *   Disclaimer  Page.
    */
    public function disclaimer() {
        $title = 'Disclaimer - Globalworks';

        $this->set('title', $title);   
        $this->set('username', $this->globalworks->welcome_greetings());
        $this->set('thumbnail', $this->globalworks->image_thumbnail());
    }

    /*
    *   Subscribe Page.
    */
    public function subscribe() {
        $title = 'Subscribe - Globalworks';

        //Load Subscribers Model.
        $this->loadModel('Subscribers');

        //New Entity.
        $subscribers = $this->Subscribers->newEntity();

        if ($this->request->is('post')) {

            /*
            *   Check if email is already subscribed or email is already registered as User Account.
            */

            $check_sub = $this->Subscribers->find()
                ->where(['email' => $this->request->data('email')])
                ->count();
            
            //Load Home Model.
            $this->loadModel('Home');

            $check_user = $this->Home->find()
                ->where(['email' => $this->request->data('email')])
                ->count();

            if (!$check_sub && !$check_user) {
                    
                //Patch Entity.
                $subscribers = $this->Subscribers->patchEntity($subscribers, $this->request->data);

                //Save Data.
                if ($this->Subscribers->save($subscribers)) {

                    //Create Log Message.
                    $log_msg = $this->request->data('email') . ' subscribed to our mailing joblist.';

                    //Save Log.
                    $this->globalworks->_log('user_subscribe_mailing_list', $log_msg);

                    $this->Flash->success(__('You are now subscribed to our Mailing Joblist.'));
                }
            } else {
                $this->Flash->error(__($this->request->data('email') . ' is already subscribed or already registered as user account.'));
            }
            
            $subscribers = $this->Subscribers->patchEntity($subscribers, $this->request->data);
        }

        $this->set('title', $title);   
        $this->set('subscribers', $subscribers);
        $this->set('username', $this->globalworks->welcome_greetings());
        $this->set('thumbnail', $this->globalworks->image_thumbnail());
    }

    /*
    *   Unsubscribe Page.
    */
    public function unsubscribe($email = null) {

        if (!$email) {
            return $this->redirect('/');
        }

        $title = 'Unsubscribe - Globalworks';

        //Load Subscribers Model.
        $this->loadModel('Subscribers');

        //Message Flash.
        $message = $email . ' was not subscribed.';

        //If unsubscribe success value set to 1.
        $success = 0;

        //Check if email is exisist from the Subscribers Table.
        $subscribers_count = $this->Subscribers->find()
            ->where(['email' => $email])
            ->count();

        if ($subscribers_count && $this->globalworks->unsubscribe($email)) {
            $success = 1;
            $message = 'You have unsubscribed from our mailing job list. Please wait...';
        }

        $this->set('title', $title);
        $this->set('success', $success);
        $this->set('message', $message);
        $this->set('username', $this->globalworks->welcome_greetings());
        $this->set('thumbnail', $this->globalworks->image_thumbnail());
    }

    /*
    *   Admins Page.
    */
    public function administrators() {
        $title = 'Admins - Globalworks';

        $this->set('title', $title);
        $this->set('username', $this->globalworks->welcome_greetings());
        $this->set('thumbnail', $this->globalworks->image_thumbnail());
    }
}