<?php
namespace App\Controller;

use Cake\Event\Event;

class JobsController extends AppController {
    
    private $globalworks;
    
    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        
        $this->globalworks = new GlobalworksController();
        
        $this->Auth->allow(['index', 'job', 'category', 'search']);

        //Check Visitor's IP Info.
        $this->globalworks->ip_info_init();

        //Check User's Account If Exists.
        $this->globalworks->check_auth();

        //Check User has not confirm his account yet.
        if ($this->globalworks->check_confirmed_user()) {
            return $this->redirect('/account_confirmation');
        }

        //Announcement.
        $this->set('pop_announcement', $this->globalworks->show_announcement());

        //Check if user is suspended. Auto logout user.
        return $this->globalworks->is_suspended();
    }

    public function index() {

        $title = 'Jobs - Globalworks';
        
        //Load Jobposts Model.
        $this->loadModel('Jobposts');

        $limit = 10;

        if ($this->request->query('page')) {
            $page = (int)$this->request->query('page');
        } else {
            $page = 1;
        }
        
        //Count Jobposts.
        $jobposts_count = $this->Jobposts->find()
            ->count();
        
        //Query out all Jobposts.
        $jobposts = $this->Jobposts->find()
            ->order(['pos' => 'DESC'])
            ->limit($limit)
            ->page($page);
        
        $this->set('title', $title);
        $this->set('jobposts_count', $jobposts_count);
        $this->set('limit', $limit);
        $this->set('page', $page);
        $this->set('jobposts', $jobposts);
        $this->set('username', $this->globalworks->welcome_greetings());
        $this->set('thumbnail', $this->globalworks->image_thumbnail());
    }
    
    /*
    *   Job List sort by cateogory.
    */
    public function category($cat) {
        
        //Load Jobposts Model.
        $this->loadModel('Jobposts');

        $title = $this->globalworks->urldecode($cat) . ' - Globalworks';
        
        $cat = $this->globalworks->urldecode($cat);

        $limit = 2;

        if ($this->request->query('page')) {
            $page = (int)$this->request->query('page');
        } else {
            $page = 1;
        }
        
        switch ($cat) {
            case 'Accounting Finance':
                $jobposts_count = $this->Jobposts->find()
                    ->where(['job_category' => 'Accounting Finance'])
                    ->andWhere(['closing_date >=' => date( "Y-m-d H:i:s" )])
                    ->count();
                $jobposts = $this->Jobposts->find()
                    ->where(['job_category' => 'Accounting Finance'])
                    ->andWhere(['closing_date >=' => date( "Y-m-d H:i:s" )])
                    ->limit($limit)
                    ->page($page)
                    ->order(['pos' => 'DESC']);
            break;
                
            case 'Arts/Media':
                $jobposts_count = $this->Jobposts->find()
                    ->where(['job_category' => 'Arts/Media'])
                    ->andWhere(['closing_date >=' => date( "Y-m-d H:i:s" )])
                    ->count();
                $jobposts = $this->Jobposts->find()
                    ->where(['job_category' => 'Arts/Media'])
                    ->andWhere(['closing_date >=' => date( "Y-m-d H:i:s" )])
                    ->limit($limit)
                    ->page($page)
                    ->order(['pos' => 'DESC']);
            break;
                
            case 'Computer/I.T':
                $jobposts_count = $this->Jobposts->find()
                    ->where(['job_category' => 'Computer/I.T'])
                    ->andWhere(['closing_date >=' => date( "Y-m-d H:i:s" )])
                    ->limit($limit)
                    ->count();
                $jobposts = $this->Jobposts->find()
                    ->where(['job_category' => 'Computer/I.T'])
                    ->andWhere(['closing_date >=' => date( "Y-m-d H:i:s" )])
                    ->limit($limit)
                    ->page($page)
                    ->order(['pos' => 'DESC']);
            break;
                
            case 'Engineering':
                $jobposts_count = $this->Jobposts->find()
                    ->where(['job_category' => 'Engineering'])
                    ->andWhere(['closing_date >=' => date( "Y-m-d H:i:s" )])
                    ->count();
                $jobposts = $this->Jobposts->find()
                    ->where(['job_category' => 'Engineering'])
                    ->andWhere(['closing_date >=' => date( "Y-m-d H:i:s" )])
                    ->limit($limit)
                    ->page($page)
                    ->order(['pos' => 'DESC']);
            break;
                
            case 'Admin/Office':
                $jobposts_count = $this->Jobposts->find()
                    ->where(['job_category' => 'Admin/Office'])
                    ->andWhere(['closing_date >=' => date( "Y-m-d H:i:s" )])
                    ->count();
                $jobposts = $this->Jobposts->find()
                    ->where(['job_category' => 'Admin/Office'])
                    ->andWhere(['closing_date >=' => date( "Y-m-d H:i:s" )])
                    ->limit($limit)
                    ->page($page)
                    ->order(['pos' => 'DESC']);
            break;
                
            case 'Building/Construction':
                $jobposts_count = $this->Jobposts->find()
                    ->where(['job_category' => 'Building/Construction'])
                    ->andWhere(['closing_date >=' => date( "Y-m-d H:i:s" )])
                    ->count();
                $jobposts = $this->Jobposts->find()
                    ->where(['job_category' => 'Building/Construction'])
                    ->andWhere(['closing_date >=' => date( "Y-m-d H:i:s" )])
                    ->limit($limit)
                    ->page($page)
                    ->order(['pos' => 'DESC']);
            break;
                
            case 'Education/Training':
                $jobposts_count = $this->Jobposts->find()
                    ->where(['job_category' => 'Eduation/Training'])
                    ->andWhere(['closing_date >=' => date( "Y-m-d H:i:s" )])
                    ->count();
                $jobposts = $this->Jobposts->find()
                    ->where(['job_category' => 'Education/Training'])
                    ->andWhere(['closing_date >=' => date( "Y-m-d H:i:s" )])
                    ->limit($limit)
                    ->page($page)
                    ->order(['pos' => 'DESC']);
            break;
                
            case 'Health Care':
                $jobposts_count = $this->Jobposts->find()
                    ->where(['job_category' => 'Health Care'])
                    ->andWhere(['closing_date >=' => date( "Y-m-d H:i:s" )])
                    ->count();
                $jobposts = $this->Jobposts->find()
                    ->where(['job_category' => 'Health Care'])
                    ->andWhere(['closing_date >=' => date( "Y-m-d H:i:s" )])
                    ->limit($limit)
                    ->page($page)
                    ->order(['pos' => 'DESC']);
            break;
                
            case 'Hotel/Restaurant':
                $jobposts_count = $this->Jobposts->find()
                    ->where(['job_category' => 'Hotel/Restaurant'])
                    ->andWhere(['closing_date >=' => date( "Y-m-d H:i:s" )])
                    ->count();
                $jobposts = $this->Jobposts->find()
                    ->where(['job_category' => 'Hotel/Restaurant'])
                    ->andWhere(['closing_date >=' => date( "Y-m-d H:i:s" )])
                    ->limit($limit)
                    ->page($page)
                    ->order(['pos' => 'DESC']);
            break;
                
            case 'Manufacturing':
                $jobposts_count = $this->Jobposts->find()
                    ->where(['job_category' => 'Manufacturing'])
                    ->andWhere(['closing_date >=' => date( "Y-m-d H:i:s" )])
                    ->count();
                $jobposts = $this->Jobposts->find()
                    ->where(['job_category' => 'Manufacturing'])
                    ->andWhere(['closing_date >=' => date( "Y-m-d H:i:s" )])
                    ->limit($limit)
                    ->page($page)
                    ->order(['pos' => 'DESC']);
            break;
                
            case 'Sales Marketing':
                $jobposts_count = $this->Jobposts->find()
                    ->where(['job_category' => 'Sales Marketing'])
                    ->andWhere(['closing_date >=' => date( "Y-m-d H:i:s" )])
                    ->count();
                $jobposts = $this->Jobposts->find()
                    ->where(['job_category' => 'Sales Marketing'])
                    ->andWhere(['closing_date >=' => date( "Y-m-d H:i:s" )])
                    ->limit($limit)
                    ->page($page)
                    ->order(['pos' => 'DESC']);
            break;
                
            case 'Services/Others':
                $jobposts_count = $this->Jobposts->find()
                    ->where(['job_category' => 'Services/Others'])
                    ->andWhere(['closing_date >=' => date( "Y-m-d H:i:s" )])
                    ->count();
                $jobposts = $this->Jobposts->find()
                    ->where(['job_category' => 'Services/Others'])
                    ->andWhere(['closing_date >=' => date( "Y-m-d H:i:s" )])
                    ->limit($limit)
                    ->page($page)
                    ->order(['pos' => 'DESC']);
            break;
                
            default:
                $jobposts_count = '';
                $jobposts = '';
                
                $this->viewBuilder()->template('category_unknown');
        }
        
        $this->set('title', $title);
        $this->set('jobposts_count', $jobposts_count);
        $this->set('page', $page);
        $this->set('limit', $limit);
        $this->set('jobposts', $jobposts);
        $this->set('username', $this->globalworks->welcome_greetings());
        $this->set('thumbnail', $this->globalworks->image_thumbnail());
    }
    
    /*
    *   Method to provider url for jobpost.
    */
    public function job($id = null) {
        if (!$id) {
            return $this->globalworks->redirect2();
        }
        
        //Load Jobposts Model.
        $this->loadModel('Jobposts');
        
        //Get Jobpost count if id jobpost is exists or not.
        $jobpost_count = $this->Jobposts->find()
            ->where(['id' => $id])
            ->andWhere(['closing_date >=' => date( "Y-m-d H:i:s" )])
            ->count();
        
        //If count is 0 redirect user to a right page.
        if ($jobpost_count == 0) {
            return $this->globalworks->redirect2();
        } else {
            
            //Query out jobposts to get post data.
            $jobpost = $this->Jobposts->find()
                ->where(['id' => $id]);

            $title = '';

            foreach ($jobpost as $job) {
                $title .= $job['job_title'] . ' - Globalworks';
            }
            
            $this->set('jobpost', $jobpost);
        }
        
        $this->set('title', $title);
        $this->set('id', $id);
        $this->set('username', $this->globalworks->welcome_greetings());
        $this->set('thumbnail', $this->globalworks->image_thumbnail());
    }
    
    /*
    *   Job Search Page.
    */
    public function search() {

        $title = 'Job Search - Globalworks';

        if ($this->request->query && strlen($this->request->query('keywords')) >= 2) {

            //Load Jobposts Model.
            $this->loadModel('Jobposts');

            $limit = 30;

            if ($this->request->query('page')) {
                $page = (int)$this->request->query('page');
            } else {
                $page = 1;
            }

            if ($this->request->query('searchType') == 'basic') {
                
                //Keywords.
                $keywords = $this->request->query('keywords');
                
                //Jobclass
                $job_class = $this->request->query('job_class');
                
                //Location.
                $location = $this->request->query('location');

                //Page Title.
                $title = $keywords . ' - Globalworks';

                //Query to count results.
                $jobposts_count = $this->Jobposts->find()
                    ->where(['job_title LIKE' => '%' . $keywords . '%'])
                    ->orWhere(['job_description LIKE' => '%' . $keywords . '%'])
                    ->orWhere(['job_responsibilities LIKE' => '%' . $keywords . '%'])
                    ->orWhere(['job_qualifications LIKE' => '%' . $keywords . '%'])
                    ->count();

                
                //Query out Search results.
                $jobposts = $this->Jobposts->find();
                $jobposts->select(['id', 'company_logo', 'job_title', 'company_name', 'job_description', 'created_at']);
                $jobposts->where(['job_title LIKE' => '%' . $keywords . '%']);
                $jobposts->orWhere(['job_description LIKE' => '%' . $keywords . '%']);
                $jobposts->orWhere(['job_responsibilities LIKE' => '%' . $keywords . '%']);
                $jobposts->orWhere(['job_qualifications LIKE' => '%' . $keywords . '%']);
                
                if ($job_class !== 'All') {
                    $jobposts->andWhere(['job_type' => $job_class]);   
                }
                
                if ($location !== 'All') {
                    $jobposts ->andWhere(['job_migration' => $location]);
                }
                
                $jobposts->andWhere(['closing_date >=' => date( "Y-m-d H:i:s" )]);
                $jobposts->order(['pos' => 'DESC']);
                $jobposts->limit($limit);
                $jobposts->page($page);
                
                $this->viewBuilder()->template('search_results');
                $this->set('count', $jobposts_count);
                $this->set('limit', $limit);
                $this->set('page', $page);
                $this->set('jobposts', $jobposts);
            } else {
                
                //Keywords.
                $keywords = $this->request->query('keywords');
                
                //Job class.
                $job_class = $this->request->query('job-class');
                
                //Localization.
                $localization = $this->request->query('localization');
                
                //Categories.
                $category = $this->request->query('category');
                
                //Country.
                $country = $this->request->query('country');

                //Query to count results.
                $jobposts_count = $this->Jobposts->find()
                    ->where(['job_title LIKE' => '%' . $keywords . '%'])
                    ->orWhere(['job_description LIKE' => '%' . $keywords . '%'])
                    ->orWhere(['job_responsibilities LIKE' => '%' . $keywords . '%'])
                    ->orWhere(['job_qualifications LIKE' => '%' . $keywords . '%'])
                    ->count();
                
                //Query out to get search results.
                $jobposts = $this->Jobposts->find();
                $jobposts->select(['id', 'company_logo', 'job_title', 'company_name', 'job_description', 'created_at']);
                $jobposts->where(['job_title LIKE' => '%' . $keywords . '%']);
                $jobposts->orWhere(['job_description LIKE' => '%' . $keywords . '%']);
                $jobposts->orWhere(['job_responsibilities LIKE' => '%' . $keywords . '%']);
                $jobposts->orWhere(['job_qualifications LIKE' => '%' . $keywords . '%']);
                
                if ($category !== 'All') {
                    $jobposts->andWhere(['job_category' => $category]);
                }
                
                if ($job_class !== 'All') {
                    $jobposts->andWhere(['job_type' => $job_class]);
                }
                
                if ($localization !== 'All') {
                    $jobposts->andWhere(['job_migration' => $localization]);
                }
                
                if ($country !== 'All') {
                    $jobposts->andWhere(['country' => $country]);
                }
                
                $jobposts->andWhere(['closing_date >=' => date( "Y-m-d H:i:s" )]);
                $jobposts->limit($limit);
                $jobposts->page($page);
                $jobposts->order(['pos' => 'DESC']);
                
                $this->viewBuilder()->template('search_results');
                $this->set('count', $jobposts_count);
                $this->set('limit', $limit);
                $this->set('page', $page);
                $this->set('jobposts', $jobposts);
            }
        }

        $this->set('title', $title);
        $this->set('username', $this->globalworks->welcome_greetings());
        $this->set('thumbnail', $this->globalworks->image_thumbnail());
    }
}
