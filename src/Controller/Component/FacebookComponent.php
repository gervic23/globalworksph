<?php

/*
*   Facebook component to integrate Facebook SDK.
*   SDK files are located in /vender/facebook DIR.
*/

namespace App\Controller\Component;

session_start();

use Cake\Controller\Component;
use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;

class FacebookComponent extends Component {

    /*
    *   Default Facebook SDK Configuration.
    */
    protected $_defaultConfig = [
        'app_id' => '139287030070755',
        'app_secret' => '643af9d9df96d3093b0bb213eec8d7a5',
        'default_graph_version' => 'v2.2',
    ];

    /*
    *   Facebook Main Class Property.
    */
    private $facebook;

    /*
    *   Facebook Helper Property.
    */
    private $helper;

    /*
    *   Initialize Facebook SDK.
    */
    public function initialize(array $config) {

        $this->config($config);

        //Facebook Main Class.
        $this->facebook = new Facebook($this->_config);

        //Facebook Login Helper.
        $this->helper = $this->facebook->getRedirectLoginHelper();

        //Default Permission.
        $permissions = ['email', 'public_profile']; // Optional permissions

        //If Requested Url is from canidate or employer.
        if ($_SERVER['REQUEST_URI'] == '/login' || $_SERVER['REQUEST_URI'] == '/login/') {
            
            //Login Url.
            $loginUrl = $this->helper->getLoginUrl('https://globalworks.local/candidate_callback', $permissions);
        } else if ($_SERVER['REQUEST_URI'] == '/login/employers' || $_SERVER['REQUEST_URI'] == '/login/employers/') {

            //Login Url.
            $loginUrl = $this->helper->getLoginUrl('https://globalworks.local/employer_callback', $permissions);
        } else {

            //Login Url.
            $loginUrl = $this->helper->getLoginUrl('https://globalworks.local/candidate_callback', $permissions);
        }

        //Render to Helper.
        $this->_registry->getController()->viewBuilder()->helpers(['Facebook' => [$loginUrl]]);
    }

    /*
    *   Facebook Login Callback.
    */
    public function callback() {

        /*
        *   Obtaining Access Token From Facebook.
        */
        try {
            $accessToken = $this->helper->getAccessToken();
        } catch (FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch (FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        /*
        *   Check if no AccessToken or Invalid AccessToken.
        */
        if (!isset($accessToken)) {
            if ($this->helper->getError()) {
                header('HTTP/1.0 401 Unauthorized');
                echo "Error: " . $helper->getError() . "\n";
                echo "Error Code: " . $helper->getErrorCode() . "\n";
                echo "Error Reason: " . $helper->getErrorReason() . "\n";
                echo "Error Description: " . $helper->getErrorDescription() . "\n";
            } else {
                header('HTTP/1.0 400 Bad Request');
                echo 'Bad request';
            }

            exit;
        }


        /*
        *   Get Response From Facebook to get User's Basic Information.
        */ 
        try {

            //Get Basic Information from the user.
            $response = $this->facebook->get('/me?fields=id,email,first_name,last_name,gender', $accessToken);
        } catch (Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch (FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        //User's Information.
        $user = $response->getGraphUser();

        //Aboart Session if FB User gathers all information.
        session_abort();

        return $user;
    }
}

