<?php
namespace App\Controller;

use Cake\Event\Event;

class CronsController extends AppController {

    private $globalworks;

    public function beforeFilter(Event $event) {
        $this->Auth->allow(['init']);

        $this->globalworks = new GlobalworksController();
    }

    /*
    *   Initalizer.
    */
    public function init() {
        $this->autoRender = false;
        $this->viewBuilder()->setTemplate(false);

        $this->remove_tmp_pics();
        $this->remove_job_imgs();
        $this->remove_log_msg();
        $this->unsuspend_user();
    }

    /*
    *   Removes uncroppred profile images from /img/users/tmp/ directory.
    */
    private function remove_tmp_pics() {
        
        //Load Img_tmp Model.
        $this->loadModel('Img_tmp');

        //Query out Img_tmp items.
        $tmps = $this->Img_tmp->find()
            ->select(['id', 'filename', 'date']);

        //Path to tmp dir.
        $dir = './img/users/tmp/';

        //Loop Results.
        foreach ($tmps as $tmp) {

            //Date Instance.
            $tmp_date = date_create($tmp['date']);

            //One hour pass.
            date_modify($tmp_date, '+1 hour');

            //One hour pass value.
            $one_hour_pass = date_format($tmp_date, "Y-m-d H:i:s");

            if (date( "Y-m-d H:i:s" ) > $one_hour_pass && $one_hour_pass < $tmp['date']) {
                
                //If file exist.
                if (file_exists($dir . $tmp['filename'])) {
                    
                    //Delete File.
                    if (unlink($dir . $tmp['filename'])) {

                        //Delete Record.
                        $img_tmp = $this->Img_tmp->get($tmp['id']);

                        $this->Img_tmp->delete($img_tmp);
                    }
                }
            }
        }
    }

    /*
    *   Removes Temporary company logo and job banner.
    */
    private function remove_job_imgs() {
         
        //Load Jobheaderimg_tmp Model.
        $this->loadModel('Jobheaderimg_tmp');

        //Path to job_header_tmp directory.
        $header_dir = './img/users/job_header_tmp/';

        //Path to company_logo_tmp dir.
        $logo_dir = './img/users/company_logo_tmp/';

        //Load Companylogo_tmp Model.
        $this->loadModel('Companylogo_tmp');

        //Expiration time. 60 minutes.
        $exp = time() - 60 * 60;

        //Query out to get expired items.
        $headers = $this->Jobheaderimg_tmp->find()
            ->select(['id', 'filename'])
            ->where(['uploaded_at <' => $exp]);

        //Loop Results.
        foreach ($headers as $header) {

            //If file exists.
            if (file_exists($header_dir . $header['filename'])) {

                //Delete File.
                if (unlink($header_dir . $header['filename'])) {

                    //Get record column.
                    $h_tmp = $this->Jobheaderimg_tmp->get($header['id']);

                    //Delete Record.
                    $this->Jobheaderimg_tmp->delete($h_tmp);
                }
            }
        }

        //Query out expired Company Logos.
        $logos = $this->Companylogo_tmp->find()
            ->select(['id', 'filename'])
            ->where(['uploaded_at <' => $exp]);

        //Loop Results.
        foreach ($logos as $logo) {
            
            //If file exists.
            if (file_exists($logo_dir . $logo['filename'])) {

                //Delete File.
                if (unlink($logo_dir . $logo['filename'])) {

                    //Get record column.
                    $l_tmp = $this->Companylogo_tmp->get($logo['id']);

                    //Delete Record.
                    $l_tmp = $this->Companylogo_tmp->delete($l_tmp);
                }
            }
        }
    }

    /*
    *   Removes expired log msg.
    */
    private function remove_log_msg() {

        //Load User_msg_log Model.
        $this->loadModel('User_log_msg');

        //Expiration time. 1 day.
        // $exp = time() - 60 * 60 * 24;
        $exp = time() - 60;

        //Query out expired logs.
        $logs = $this->User_log_msg->find()
            ->select(['id'])
            ->where(['microtime <' => $exp]);

        //Loop Results.
        foreach ($logs as $log) {
            
            //Get record column.
            $l_msg = $this->User_log_msg->get($log['id']);

            //Delete Record.
            $this->User_log_msg->delete($l_msg);
        }
    }

    /*
    *   Unsuspend User depends on the time set.
    */
    private function unsuspend_user() {

        //Load Home Model.
        $this->loadModel('Home');

        //Query Suspended Users with Suspension time.
        $users = $this->Home->find()
            ->select(['id', 'email', 'suspend', 'suspend_time'])
            ->where(['suspend' => 1])
            ->andWhere(['suspend_time <=' => date( "Y-m-d H:i:s" )]);

        //Loop Results.
        foreach ($users as $user) {
            
            //Get User's record via ID.
            $u = $this->Home->get($user['id']);

            //Update records.
            $u->suspend = 0;
            $u->suspend_time = null;

            //Save Changes.
            if ($this->Home->save($u)) {

                /*
                *   Send Email Message.
                */

                $e_subject = 'Your account has been unsuspend.';

                $e_message = '
                    <p>Your account has been unsuspend. You can now login and use globalworks service.</p>
                ';

                $e_email = [
                    'from' => [$this->globalworks->_server_email_address, 'Globalworks'],
                    'html' => true,
                    'subject' => $e_subject,
                    'to' => $user['email'],
                    'message' => $e_message,
                ];

                //Send Email Message.
                $this->globalworks->mail($e_email);
            }
        }
    }
}
