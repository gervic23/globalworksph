<?php
namespace App\Controller;

use Cake\Mailer\Email;

/*
*   This Controller consist of functionality methods only.
*   Not for routing.
*/
class GlobalworksController extends AppController {

    /*
    *   Full Url Path Property.
    */
    public $_url;

    /*
    *   Domain Name Property.
    */
    public $_domain;

    /*
    *   Website's Email Address Property.
    */
    public $_server_email_address;

    public function initialize() {
        parent::initialize();
        
        $this->_url = 'https://' . $_SERVER['SERVER_NAME'];
        $this->_domain = $_SERVER['SERVER_NAME'];
        $this->_server_email_address = 'noreply@' . $this->_domain;
    }

    /*
    *   Method to register visitor's IP information.
    */
    public function ip_info_init() {

        //Visitor's IP Address.
        $ip = $_SERVER['REMOTE_ADDR'];

        //Load Ip_log Model.
        $this->loadModel('Ip_logs');

        //Check if Visitor's IP Address is exist from the log.
        $logs_count = $this->Ip_logs->find()
            ->where(['query' => $ip])
            ->count();

        //If Visitor's IP Address does not exist, register it.
        if ($logs_count == 0) {
            $this->insert_ip_info();
        } else {
            
            /*
            *   Update or increment visitor's hits.
            */

            //Query IP Address's column.
            $logs = $this->Ip_logs->find()
                ->select(['id', 'hits'])
                ->where(['query' => $ip]);

            //Record ID.
            $id = 0;

            //Hits count.
            $hits = 0;

            //Loop Results.
            foreach ($logs as $log) {

                //Append ID.
                $id += $log['id'];
                
                //Append Value.
                $hits = $log['hits'] + 1;
            }

            //Get Record.
            $ll = $this->Ip_logs->get($id);
            
            //Update record.
            $ll->hits = $hits;

            //Save Record.
            $this->Ip_logs->save($ll);
        }

        return true;
    }

    /*
    *   Get data from User's IP.
    */
    public function ip_get_info($info) {

        //Visitor's IP Address.
        $ip = $_SERVER['REMOTE_ADDR'];

        //Load Ip_log Model.
        $this->loadModel('Ip_logs');

        //Call this method just incase visitor's IP info is not already registered.
        $this->ip_info_init();

        //Query out to get IP info.
        $infos = $this->Ip_logs->find();

        //Look up information depends on $info value.
        switch ($info) {

            //If $info = city.
            case 'city':

                //Select Column city.
                $infos->select(['city']);

                //Where accociated by IP.
                $infos->where(['query' => $ip]);

                //To be append.
                $city = '';

                //Loop result to get city.
                foreach ($infos as $inf) {
                    $city = $inf['city'];
                }

                return $city;
            break;
            
            //If $info = country_code.
            case 'country_code':

                //Select Column country_code.
                $infos->select(['country_code']);

                //Where accociated by IP.
                $infos->where(['query' => $ip]);

                //To be append.
                $country_code = '';

                //Loop result to get country_code.
                foreach ($infos as $inf) {
                    $country_code = $inf['country_code'];
                }

                return $country_code;
            break;

            //If $info = isp.
            case 'isp':

                //Select isp Column.
                $infos->select(['isp']);

                //Where accociated IP.
                $infos->where(['query' => $ip]);

                //To be append.
                $isp = '';

                //Loop result to get isp.
                foreach ($infos as $inf) {
                    $isp = $inf['isp'];
                }

                return $isp;
            break;

            //If $info = lat.
            case 'lat':

                //Select Column lat.
                $infos->select(['lat']);

                //Where accociated IP.
                $infos->where(['query' => $ip]);

                //To be append.
                $lat = '';

                //Loop result to get lat.
                foreach ($infos as $inf) {
                    $lat = $inf['lat'];
                }

                return $lat;
            break;
            
            //If $info = lon.
            case 'lon':

                //Select lon Column.
                $infos->select(['lon']);

                //Where accociated IP.
                $infos->where(['query' => $ip]);

                //To be append.
                $lon = '';

                //Loop result to get lon.
                foreach ($infos as $inf) {
                    $lon = $inf['lon'];
                }

                return $lon;
            break;
            
            //If $info = region.
            case 'region':

                //Select region_name Column.
                $infos->select(['region_name']);

                //Where accociated IP.
                $infos->where(['query' => $ip]);

                //To be Append.
                $region = '';

                //Loop result to get region_name.
                foreach ($infos as $inf) {
                    $region = $inf['region_name'];
                }

                return $region;
            break;
        }
    }

    /*
    *   IP-API Initializer.
    */
    private function insert_ip_info() {

        //Visitor's IP Address.
        $ip = $_SERVER['REMOTE_ADDR'];

        //Load Ip_log Model.
        $this->loadModel('Ip_logs');

        //Call ip-api.com to fetch IP information (API).
        $infos = json_decode(file_get_contents('http://ip-api.com/json'), true);

        //New Entity.
        $logs = $this->Ip_logs->newEntity();

        //Append Entity.
        $ip_infos = [
            'assoc' => $infos['as'],
            'city' => $infos['city'],
            'country' => $infos['country'],
            'country_code' => $infos['countryCode'],
            'isp' => $infos['isp'],
            'lat' => $infos['lat'],
            'lon' => $infos['lon'],
            'org' => $infos['org'],
            'query' => $ip,
            'region' => $infos['region'],
            'region_name' =>  $infos['regionName'],
            'timezone' => $infos['timezone'],
            'zip' => $infos['zip'],
            'hits' => 1,
        ];

        //Patch Entity.
        $logs = $this->Ip_logs->patchEntity($logs, $ip_infos);

        //Save Log.
        if ($this->Ip_logs->save($logs)) {
            return true;
        }
    }

    /*
    *   Check if user's account is exists.
    */
    public function is_account_exists($id) {

        //Load Home Model.
        $this->loadModel('Home');

        //Get Count of User Account.
        //If Count is 1 account exists.
        //If Count is 0 account does not exists.
        $home_count = $this->Home->find()
            ->where(['id' => $id])
            ->count();

        return $home_count;
    }

    /*
    *   Check User Account if exists in the database.
    *   If not found. login session will destroy.
    */
    public function check_auth() {

        //If User has Login Session.
        if ($this->request->session()->read('Auth.User')) {

            //Load Home Model.
            $this->loadModel('Home');

            //Count if User ID is exists in the database.
            $home_count = $this->Home->find()
                ->where(['id' => $this->request->session()->read('Auth.User')['id']])
                ->count();

            if ($home_count == 0) {

                //Destroy Session.
                return $this->redirect($this->Auth->logout());
            }
        }
    }

    /*
    *   Check if User has not confirmed his account.
    */
    public function check_confirmed_user() {
        
        //Check if user has Login Session and Confirmation Code.
        if ($this->request->session()->read('Auth.User') && $this->request->session()->read('Auth.User')['confirmation_code'] !== '' && $this->request->session()->read('Auth.User')['confirmation_code'] !== null && $this->uri(1) !== 'account_confirmation' && $this->uri(1) !== 'logout') {
            return true;
        }

        return false;
    }

    /*
    *   Check FB User if user has basic information.
    *   If User has no basic information, redirect to basic information page.
    */
    public function fb_user_check() {

        //If User has Session Login.
        if ($this->request->session()->read('Auth.User')) {

            //Load Home Model.
            $this->loadModel('Home');

            //Check if User is Logged with FB.
            $fb_user_count = $this->Home->find()
                ->where(['id' => $this->request->session()->read('Auth.User')['id']])
                ->andWhere(['fb_user' => 1])
                ->count();

            //If Count is not 0.
            if ($fb_user_count > 0) {

                //Count to check if User has basic information or not.
                $users_count = $this->Home->find()
                ->where(['id' => $this->request->session()->read('Auth.User')['id']])
                ->andWhere(['fb_user' => 1])
                ->andWhere(['fb_complete_reg' => 1])
                ->count();

                //Redirect User if count is 0.
                if ($users_count == 0 && $this->uri(1) !== 'basicinfo' && $this->uri(1) !== 'logout') {
                    return false;
                }
            }
        }

        return true;
    }

    /*
    *   Facebook User's Callback.
    */
    public function users_callback($role) {
        //User's information from callback.
        $user_callback = $this->Facebook->callback();

        //To be append.
        $email = '';

        //Firstname Value.
        $firstname = $user_callback['first_name'];

        //Lastname Value.
        $lastname = $user_callback['last_name'];

        //If Email is set.
        if (isset($user_callback['email'])) {
            $email = $user_callback['email'];
        }

        //If email is empty. or did not set by the user.
        if (!$email) {
            return $this->redirect('/facebook-email-required');
        }

        //Load Home Model.
        $this->loadModel('Home');

        //Count to check Email if exists or not in the database.
        $email_count = $this->Home->find()
            ->where(['email' => $email])
            ->count();
            
        //If Email exists in the database.
        if ($email_count > 0) {
            
            //Get User's Information.
            $users = $this->Home->find()
                ->select(['password', 'fb_user', 'fb_complete_reg'])
                ->where(['email' => $email]);

            //To be append.
            $password = '';

            //To be append.
            $fb_user = '';

            //To be append.
            $fb_complete_reg = '';

            //Loop results.
            foreach ($users as $user) {

                //Append Password.
                $password .= $user['password'];

                //Append fb_user.
                $fb_user .= $user['fb_user'];

                //Append fb_complete_reg.
                $fb_complete_reg = $user['fb_complete_reg'];
            }

            /*
            *   If email is already registered and has a password already.
            *   Make user to authenticate with his password.
            */
            if ($password !== '' && ($fb_user == 0 || $fb_user === null)) {
                
                //Make User to enter his password.
                return $this->redirect('/fb_password_auth/' . $email);
            } else if ($fb_complete_reg == 0 || $fb_complete_reg === null) {

                /*
                *   If User did not complete Basic Information, Redirect User to a basic information page.
                */
               
                //Query out to get User's Information.
                $users = $this->Home->find()
                    ->where(['email' => $email]);

                //To be append.
                $user_info;

                //Loop results and store user's information to $user_info.
                foreach ($users as $user => $val) {

                    //Append Value.
                    $user_info = $val;
                }

                //Set User Login Sesion.
                $this->Auth->setUser($user_info);

                //Redirect to Basic Information Page.
                return $this->redirect('/basicinfo');
            } else {
                
                //Query out to get user's information for Login Session.
                $users = $this->Home->find()
                    ->where(['email' => $email]);

                //To be append.
                $user_info;

                //Loop results to get user's information to $user_info.
                foreach ($users as $user => $val) {
                    
                    //Append Value.
                    $user_info = $val;
                }

                //Set User Login Sesion.
                $this->Auth->setUser($user_info);

                //Redirect to Candidate Dashboard.
                return $this->redirect('/candidates/dashboard');
            }
        } else {
            /*
            *   If User is 1st time to logged in using faceboobk outh without email record from the database.
            */
            
            //New Entity.
            $users = $this->Home->newEntity();

            //User's Few Basic Info.
            $new_user = [
                'email' => $email,
                'role' => $role,
                'firstname' => $firstname,
                'lastname' => $lastname,
                'date_created' => date( "Y-m-d H:i:s" ),
                'current_ip' => $_SERVER['REMOTE_ADDR'],
                'registered_ip' => $_SERVER['REMOTE_ADDR'],
                'last_login' => date( "Y-m-d H:i:s" ),
                'confirmation_code' => '',
                'fb_user' => 1,
                'fb_complete_reg' => 0,
            ];
            
            //Patch Entity.
            $users = $this->Home->patchEntity($users, $new_user);

            //Save New Data.
            if ($this->Home->save($users)) {

                //User's ID.
                $id = $this->get_user_id($email);

                //Create User's Basic Information.
                $new_user = [
                    'id' => $id,
                    'email' => $email,
                    'role' => $role,
                    'firstname' => $firstname,
                    'lastname' => $lastname,
                    'confirmation_code' => '',
                    'date_created' => date( "Y-m-d H:i:s" ),
                    'current_ip' => $_SERVER['REMOTE_ADDR'],
                    'registered_ip' => $_SERVER['REMOTE_ADDR'],
                    'last_login' => date( "Y-m-d H:i:s" ),
                    'fb_user' => 1,
                    'fb_complete_reg' => 0,
                ];
                
                //Create Login Session.
                $this->Auth->setUser($new_user);

                //Redirect to Basic Information Page.
                return $this->redirect('/basicinfo');
            }
        }
    }

    /*
    *   Get User's ID via Email.
    */
    public function get_user_id($email) {
        
        //Load Home Model.
        $this->loadModel('Home');

        //Query out to get User's ID.
        $users = $this->Home->find()
            ->select(['id'])
            ->where(['email' => $email]);

        //To be Append.
        $id = '';

        //Loop results to get ID.
        foreach ($users as $user) {
            
            //Append ID.
            $id .= $user['id'];
        }

        return $id;
    }

    /*
    *   Get User's Role via ID.
    */
    public function get_user_role($id) {

        //Load Home Model.
        $this->loadModel('Home');

        //Query to get user's role.
        $users = $this->Home->find()
            ->select(['role'])
            ->where(['id' => $id]);

        //Value of role.
        $role = '';

        //Loop Results
        foreach ($users as $user) {
            $role .= $user['role'];
        }

        return $role;
    }

    /*
    *   Method to set a proper name for the user.
    *   If user set a nickname returns nickname.
    *   If user does not set a nickname return firstname.
    *   See the navigation bar on the upper right of the website if you are logged in.
    */
    public function welcome_greetings() {
        
        //If User has logged in.
        if ($this->request->session()->read('Auth.User')) {
            
            //Load Model.
            $this->loadModel('Profiles');
            
            //User's Session ID.
            $user_id = $this->request->session()->read('Auth.User')['id'];
            
            //Count User's Profile Record if exists or not.
            $profiles_count = $this->Profiles->find()
                ->where(['user_id' => $user_id])
                ->count();
            
            //If User's Profile record does not exists, return firstname.
            if ($profiles_count == 0) {
                
                //Return firstname.
                return $this->getfirstname($user_id);
                
            //If User's Profile exists.
            } else {
                
                //Get User's Profile record.
                $profiles = $this->Profiles->find()
                    ->where(['user_id' => $user_id]);
                
                //Nickname to append.
                $nickname = '';
                
                //Loop results.
                foreach ($profiles as $profile) {
                    
                    //Append nickname.
                    $nickname .= $profile['nickname'];
                }
                
                //If nickname is empty or null return firstname. If not empty or null, return nickname.
                if ($nickname !== '') {
                    return ucfirst($nickname);
                } else {
                    return $this->getfirstname($user_id);
                }
            }
        }
    }
    
    /*
    *   Method to return User's firstname.
    */
    public function getfirstname($user_id) {
        
        //Load Model.
        $this->loadModel('Home');
        
        //Get User's Info.
        $home = $this->Home->find()
            ->where(['id' => $user_id]);
        
        //Firstname to append.
        $firstname = '';
        
        //Loop Results.
        foreach ($home as $user) {
            
            //Append fistname.
            $firstname .= $user['firstname'];
        }
        
        //Return firstname.
        return $firstname;
    }
    
    /*
    *   Method to Get User's Lastnamme.
    */
    public function getlastname($user_id) {
        
        //Load Model;
        $this->loadModel('Home');
        
        //Get User Info.
        $home = $this->Home->find()
            ->where(['id' => $user_id]);
        
        //Value to append.
        $lastname = '';
        
        //Loop Results.
        foreach ($home as $user) {
            
            //Append Value.
            $lastname .= $user['lastname'];
        }
        
        return $lastname;
    }

    /*
    *   Method to get User's Email via ID.
    */
    public function getemail($id) {

        //Load Home Model.
        $this->loadModel('Home');

        //Query Table.
        $home = $this->Home->find()
            ->select(['email'])
            ->where(['id' => $id]);

        //Email value.
        $email = '';

        //Loop Results.
        foreach ($home as $h) {

            //Append Value.
            $email .= $h['email'];
        }

        return $email;
    }
    
    /*
    *   Method to Get User's Profile Nickname.
    */
    public function getnickname($user_id) {
        
        //Load Model.
        $this->loadModel('Profiles');
        
        //Check if User has profile.
        $profiles_count = $this->Profiles->find()
            ->where(['user_id' => $user_id])
            ->count();
        
        if ($profiles_count == 0) {
            
            //Return Nothing.
            return '';
        } else {
            
            //Get User Profile info.
            $profiles = $this->Profiles->find()
                ->where(['user_id' => $user_id]);
            
            //Value to append.
            $nickname = '';
            
            //Loop Results.
            foreach ($profiles as $profile) {
                
                //Append Value.
                $nickname .= $profile['nickname'];
            }
            
            //If Nickname is empty or null.
            if ($nickname == '') {
                
                //Return Nothing/
                return '';
            } else {
                return $nickname;
            }
        }
    }

    /*
    *   Method to send Email Message.
    */
    public function mail($param) {

        //Check if $param is array.
        if (is_array($param)) {

            $from = $param['from'];
            $to = $param['to'];
            $subject = $param['subject'];
            
            if (isset($param['html'])) {
                $html = $param['html'];
            } else {
                $html = '';
            }

            $message = $param['message'];

            $email = new Email('default');
            $email->from([$from[0] => $from[1]]);
            
            if (!$html) {
                $email->emailFormat('text');
            } else {
                $email->emailFormat('html');
            }

            $email->to($to);
            $email->subject($subject);
            
            if ($email->send($message)) {
                return true;
            }
        }
    }
    
    /*
    *   Method to set Image Thumbnail.
    *   If User's has Profile Picture, set image thumbnail.
    *   If User's has no Profule Picture, set default image thumbnail.
    */
    public function image_thumbnail() {
        
        //Load Model.
        $this->loadModel('Profiles');
        
        //User's Session;s ID.
        $user_id = $this->request->session()->read('Auth.User')['id'];
        
        //Count User's Profile Record if exists or not.
        $profiles_count = $this->Profiles->find()
            ->where(['user_id' => $user_id])
            ->count();
        
        //If User's Profile Record is exists.
        if ($profiles_count !== 0) {
            
            //Get User's Profile Record.
            $profiles = $this->Profiles->find()
                ->where(['user_id' => $user_id]);
            
            //profile_picture tp be append.
            $profile_picture = '';
            
            //Loop Results.
            foreach ($profiles as $profile) {
                
                //Append profile_picture.
                $profile_picture = $profile['profile_picture'];
            }
            
            //If profile_picture is not empty or set, return profile_picture.
            if ($profile_picture !== '') {
                return $profile_picture;
                
            //If profile_picture is empty or null, return empty string.
            } else {
                return '';
            }
        
        //If User's Profile does not extist, return empty string.
        } else {
            return '';
        }
    }
    
    /*
    *   Method to Delete temporary image from /img/users/tmp directory
    *   and delete record from img_tmp database table.
    */
    public function deletetmpimg($user_id, $filename) {
        
        //Load Model.
        $this->loadModel('Imgtmp');
        
        //Get User's img_tmp record.
        $imgtmp = $this->Imgtmp->find()
            ->where(['user_id' => $user_id]);
        
        //Value to be append.
        $id = '';

        foreach ($imgtmp as $tmp) {
            
            //Append ID.
            $id .= $tmp['id'];
        }
        
        //Get User's img_tmp row.
        $img_tmp = $this->Imgtmp->get($id);
        
        //Delete User's img_tmp record and temporary image.
        if ($this->Imgtmp->delete($img_tmp)) {
            if (file_exists('./img/users/tmp/' . $filename)) {
                unlink('./img/users/tmp/' . $filename);
                return true;
            }
        }
    }
    
    /*
    *   Method to update User's Profile's profile_picture record if User has an old profile picture.
    *   Delete old profile pictures and update Profiles profile_picture record.
    */
    public function delete_exisiting_profile_picture($user_id, $profile_picture, $new_filename) {
        
        //Load Model.
        $this->loadModel('Profiles');
        
        //Get User's Profile Records.
        $profiles = $this->Profiles->find()
            ->where(['user_id' => $user_id]);
        
        //Value to append.
        $id = '';
        
        //Loop Results.
        foreach ($profiles as $pro) {
            
            //Append ID.
            $id .= $pro['id'];
        }
        
        //Get User's Profile row.
        $profile = $this->Profiles->get($id);
        
        //Update profile_picture.
        $profile->profile_picture = $new_filename;
        
        //Save Changes.
        if ($this->Profiles->save($profile)) {
            
            //If file exists delete old profile picture.
            if (file_exists('./img/users/profile_pictures/' . $profile_picture)) {
                unlink('./img/users/profile_pictures/' . $profile_picture);
            }
            
            //If file exists delete old thumbnail picture.
            if (file_exists('./img/users/profile_thumbnails/' . $profile_picture)) {
                unlink('./img/users/profile_thumbnails/' . $profile_picture);
            }
                
            return true;
        }
    }
    
    //Redirect Users depends on the role.
    public function redirect2() {
        //Check if user has login session.
        if ($role = $this->request->session()->read('Auth.User')) {
            
            //User's Role.
            $role = $this->request->session()->read('Auth.User')['role'];
            
            //If Applicant.
            if ($role == 'Applicant') {
                return $this->redirect('/candidates/dashboard');
                
            //If Employer.
            } else if ($role == 'Employer') {
                return $this->redirect('/employers/dashboard');
                
            //If Admin or Super Admin.
            } else if ($role == 'Admin' || $role == 'Super Admin') {
                return $this->redirect('/admin/dashboard');
            }
            
        //If No Login Session.
        } else {
            return $this->redirect('/');
        }
    }
    
    //Get User's Profile Image Thumbnail.
    public function get_users_profile_thumb($user_id) {
        
        //Load Model.
        $this->loadModel('Profiles');
        
        //
        $img_check = $this->Profiles->find()
            ->where(['user_id' => $user_id])
            ->count();
        
        if ($img_check == 1) {
            $image = $this->Profiles->find()
                ->where(['user_id' => $user_id]);
            
            $image_thumb = '';
            
            foreach ($image as $img) {
                $image_thumb .= $img['profile_picture'];
            }
            
            if ($image_thumb !== '') {
                return '<img src="/img/users/profile_thumbnails/' . $image_thumb . '" />';
            } else {
                return '<img src="/img/users/nopropic.png" />';
            }
        } else {
            return '<img src="/img/users/nopropic.png" />';
        }
    }
    
    //Return URI requested segment.
    public function uri($num = 0) {
        $uri = $_SERVER['REQUEST_URI'];
        $uri = explode('/', $uri);
        
        if (isset($uri[$num])) {
            $pos = strpos($uri[$num], '?');
            
            if ($pos) {
                $str = substr($uri[$num], 0 , $pos);
            
                return $str;
            } else {
                return $uri[$num];
            }
        } else {
            return '';
        }
        
        return '';
    }
    
    //Convert and return from country code to country name.
    public function country_to_str($country_code) {
        switch ($country_code) {
            case 'AF';
                return 'AFGANISTAN';
            break;
                
            case 'AL':
                return 'ALBANIA';
            break;
                
            case 'AL':
                return 'ALGERIA';
            break;
                
            case 'AS':
                return 'AMERICAN SAMOA';
            break;
                
            case 'AD':
                return 'ANDORRA';
            break;
                
            case 'AO':
                return 'ANGOLA';
            break;
                
            case 'AI':
                return 'ANGUILLA';
            break;
                
            case 'AQ':
                return 'ANTARCTICA';
            break;
                
            case 'AG':
                return 'ANTIGUA AND BARBUDA';
            break;
                
            case 'AR':
                return 'ARGENTINA';
            break;
                
            case 'AM':
                return 'ARMENIA';
            break;
                
            case 'AW':
                return 'ARUBA';
            break;
                
            case 'AUSTRALIA':
                return 'AUSTRALIA';
            break;
            
            case 'AT':
                return 'AUSTRIA';
            break;
                
            case 'AZ':
                return 'AZERBAIJAN';
            break;
                
            case 'BS':
                return 'BAHAMAS';
            break;
                
            case 'BH':
                return 'BAHRAIN';
            break;
                
            case 'BD':
                return 'BANGLADESH';
            break;
                
            case 'BB':
                return 'BARBADOS';
            break;
                
            case 'BY':
                return 'BELARUS';
            break;
                
            case 'BE':
                return 'BELGIUM';
            break;
                
            case 'BZ':
                return 'BELIZE';
            break;
                
            case 'BJ':
                return 'BENIN';
            break;
                
            case 'BM':
                return 'BERMUDA';
            break;
                
            case 'BT':
                return 'BHUTAN';
            break;
                
            case 'BO':
                return 'BOLIVIA';
            break;
                
            case 'BA':
                return 'BOSNIA AND HERZEGOVINA';
            break;
                
            case 'BW':
                return 'BOTSWANA';
            break;
                
            case 'BV':
                return 'BOUVET ISLAND';
            break;
                
            case 'BR':
                return 'BRAZIL';
            break;
                
            case 'IO':
                return 'BRITISH INDIAN OCEAN TERRITORY';
            break;
                
            case 'BN':
                return 'BRUNEI DARUSSALAM';
            break;
                
            case 'BG':
                return 'BULGARIA';
            break;
                
            case 'BF':
                return 'BURKINA FASO';
            break;
                
            case 'BI':
                return 'BURUNDI';
            break;
                
            case 'KH':
                return 'CAMBODIA';
            break;
                
            case 'CM':
                return 'CAMEROON';
            break;
                
            case 'CA':
                return 'CANADA';
            break;
                
            case 'CV':
                return 'CAPE VERDE';
            break;
                
            case 'KY':
                return 'CAYMAN ISLANDS';
            break;
                
            case 'CF':
                return 'CENTRAL AFRICAN REPUBLIC';
            break;
                
            case 'TD':
                return 'CHAD';
            break;
                
            case 'CL':
                return 'CHILE';
            break;
                
            case 'CN':
                return 'CHINA';
            break;
                
            case 'CX':
                return 'CHRISTMAS ISLAND';
            break;
                
            case 'CC':
                return 'COCOS (KEELING) ISLANDS';
            break;
                
            case 'CO':
                return 'COLOMBIA';
            break;
            
            case 'KM':
                return 'COMOROS';
            break;
                
            case 'CG':
                return 'CONGO';
            break;
                
            case 'CD':
                return 'CONGO, THE DEMOCRATIC REPUBLIC OF THE';
            break;
                
            case 'CK':
                return 'COOK ISLANDS';
            break;
                
            case 'CR':
                return 'COSTA RICA';
            break;
                
            case 'CI':
                return 'COTE D IVOIRE';
            break;
                
            case 'HR':
                return 'CROATIA';
            break;
                
            case 'CU':
                return 'CUBA';
            break;
                
            case 'CY':
                return 'CYPRUS';
            break;
                
            case 'CZ':
                return 'CZECH REPUBLIC';
            break;
                
            case 'DK':
                return 'DENMARK';
            break;
                
            case 'DJ':
                return 'DJIBOUTI';
            break;
                
            case 'DM':
                return 'DOMINICA';
            break;
                
            case 'DO':
                return 'DOMINICAN REPUBLIC';
            break;
                
            case 'TP':
                return 'EAST TIMOR';
            break;
                
            case 'EC':
                return 'ECUADOR';
            break;
                
            case 'EG':
                return 'EGYPT';
            break;
                
            case 'SV':
                return 'EL SALVADOR';
            break;
                
            case 'GQ':
                return 'EQUATORIAL GUINEA';
            break;
                
            case 'ER':
                return 'ERITREA';
            break;
                
            case 'EE':
                return 'ESTONIA';
            break;
                
            case 'ET':
                return 'ETHIOPIA';
            break;
                
            case 'FK':
                return 'FALKLAND ISLANDS (MALVINAS)';
            break;
                
            case 'FO':
                return 'FAROE ISLANDS';
            break;
                
            case 'FJ':
                return 'FIJI';
            break;
                
            case 'FI':
                return 'FINLAND';
            break;
                
            case 'FR':
                return 'FRANCE';
            break;
                
            case 'GF':
                return 'FRENCH GUIANA';
            break;
                
            case 'PF':
                return 'FRENCH POLYNESIA';
            break;
                
            case 'TF':
                return 'FRENCH SOUTHERN TERRITORIES';
            break;
                
            case 'GA':
                return 'GABON';
            break;
                
            case 'GM':
                return 'GAMBIA';
            break;
                
            case 'GE':
                return 'GEORGIA';
            break;
                
            case 'DE':
                return 'GERMANY';
            break;
                
            case 'GH':
                return 'GHANA';
            break;
                
            case 'GI':
                return 'GIBRALTAR';
            break;
                
            case 'GR':
                return 'GREECE';
            break;
                
            case 'GR':
                return 'GREENLAND';
            break;
                
            case 'GD':
                return 'GRENADA';
            break;
                
            case 'GP':
                return 'GUADELOUPE';
            break;
                
            case 'GU':
                return 'GUAM';
            break;
                
            case 'GT':
                return 'GUATEMALA';
            break;
                
            case 'GM':
                return 'GUINEA';
            break;
                
            case 'GW':
                return 'GUINEA-BISSAU';
            break;
                
            case 'GY':
                return 'GUYANA';
            break;
                
            case 'HT':
                return 'HAITI';
            break;
                
            case 'HM':
                return 'HEARD ISLAND AND MCDONALD ISLANDS';
            break;
                
            case 'VA':
                return 'HOLY SEE (VATICAN CITY STATE)';
            break;
                
            case 'HN':
                return 'HONDURAS';
            break;
                
            case 'HK':
                return 'HONG KONG';
            break;
                
            case 'HU':
                return 'HUNGARY';
            break;
                
            case 'IS':
                return 'ICELAND';
            break;
                
            case 'IN':
                return 'INDIA';
            break;
                
            case 'ID':
                return 'INDONESIA';
            break;
                
            case 'IR':
                return 'IRAN, ISLAMIC REPUBLIC O';
            break;
            
            case 'IQ':
                return 'IRAQ';
            break;
                
            case 'IE':
                return 'IRELAND';
            break;
                
            case 'IL':
                return 'ISRAEL';
            break;
                
            case 'IT':
                return 'ITALY';
            break;
                
            case 'JM':
                return 'JAMAICA';
            break;
                
            case 'JP':
                return 'JAPAN';
            break;
                
            case 'JO':
                return 'JORDAN';
            break;
                
            case 'KZ':
                return 'KAZAKSTAN';
            break;
                
            case 'KENYA':
                return 'KENYA';
            break;
                
            case 'KI':
                return 'KIRIBATI';
            break;
                
            case 'KP':
                return 'KOREA DEMOCRATIC PEOPLES REPUBLIC OF';
            break;
                
            case 'KR':
                return 'KOREA REPUBLIC OF';
            break;
                
            case 'KW':
                return 'KUWAIT';
            break;
                
            case 'KG':
                return 'KYRGYZSTAN';
            break;
                
            case 'LA':
                return 'LAO PEOPLES DEMOCRATIC REPUBLIC';
            break;
                
            case 'LV':
                return 'LATVIA';
            break;
                
            case 'LB':
                return 'LEBANON';
            break;
                
            case 'LS':
                return 'LESOTHO';
            break;
                
            case 'LR':
                return 'LIBERIA';
            break;
                
            case 'LY':
                return 'LIBYAN ARAB JAMAHIRIYA';
            break;
                
            case 'LI':
                return 'LIECHTENSTEIN';
            break;
                
            case 'LT':
                return 'LITHUANIA';
            break;
                
            case 'LU':
                return 'LUXEMBOURG';
            break;
                
            case 'MO':
                return 'MACAU';
            break;
                
            case 'MK':
                return 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF';
            break;
                
            case 'MG':
                return 'MADAGASCAR';
            break;
                
            case 'MW':
                return 'MALAWI';
            break;
                
            case 'MY':
                return 'MALAYSIA';
            break;
                
            case 'MV':
                return 'MALDIVES';
            break;
                
            case 'ML':
                return 'MALI';
            break;
                
            case 'MT':
                return 'MALTA';
            break;
                
            case 'MH':
                return 'MARSHALL ISLANDS';
            break;
                
            case 'MQ':
                return 'MARTINIQUE';
            break;
                
            case 'MR':
                return 'MAURITANIA';
            break;
                
            case 'MU':
                return 'MAURITIUS';
            break;
                
            case 'YT':
                return 'MAYOTTE';
            break;
                
            case 'MX':
                return 'MEXICO';
            break;
                
            case 'FM':
                return 'MICRONESIA, FEDERATED STATES OF';
            break;
                
            case 'MD':
                return 'MOLDOVA, REPUBLIC O';
            break;
                
            case 'MC':
                return 'MONACO';
            break;
                
            case 'MN':
                return 'MONGOLIA';
            break;
                
            case 'MS':
                return 'MONTSERRAT';
            break;
                
            case 'MA':
                return 'MOROCCO';
            break;
                
            case 'MZ':
                return 'MOZAMBIQUE';
            break;
                
            case 'MN':
                return 'MYANMAR';
            break;
                
            case 'NA':
                return 'NAMIBIA';
            break;
                
            case 'NR':
                return 'NAURU';
            break;
                
            case 'NP':
                return 'NEPAL';
            break;
                
            case 'NL':
                return 'NETHERLANDS';
            break;
                
            case 'AN':
                return 'NETHERLANDS ANTILLES';
            break;
                
            case 'NC':
                return 'NEW CALEDONIA';
            break;
                
            case 'NZ':
                return 'NEW ZEALAND';
            break;
                
            case 'NI':
                return 'NICARAGUA';
            break;
                
            case 'NI':
                return 'NIGER';
            break;
                
            case 'NG':
                return 'NIGERIA';
            break;
                
            case 'NF':
                return 'NIUE';
            break;
                
            case 'MP':
                return 'NORTHERN MARIANA ISLANDS';
            break;
                
            case 'NO':
                return 'NORWAY';
            break;
                
            case 'OM':
                return 'OMAN';
            break;
                
            case 'PK':
                return 'PAKISTAN';
            break;
                
            case 'PW':
                return 'PALAU';
            break;
                
            case 'PS':
                return 'PALESTINIAN TERRITORY, OCCUPIED';
            break;
                
            case 'PA':
                return 'PANAMA';
            break;
                
            case 'PG':
                return 'PAPUA NEW GUINEA';
            break;
                
            case 'PY':
                return 'PARAGUAY';
            break;
                
            case 'PE':
                return 'PERU';
            break;
                
            case 'PH':
                return 'PHILIPPINES';
            break;
                
            case 'PN':
                return 'PITCAIRN';
            break;
                
            case 'PL':
                return 'POLAND';
            break;
                
            case 'PT':
                return 'PORTUGAL';
            break;
                
            case 'PR':
                return 'PUERTO RICO';
            break;
                
            case 'QA':
                return 'QATAR';
            break;
                
            case 'RE':
                return 'REUNION';
            break;
                
            case 'RO':
                return 'ROMANIA';
            break;
                
            case 'RU':
                return 'RUSSIAN FEDERATION';
            break;
                
            case 'RW':
                return 'RWANDA';
            break;
                
            case 'SH':
                return 'SAINT HELENA';
            break;
                
            case 'KN':
                return 'SAINT KITTS AND NEVIS';
            break;
                
            case 'LC':
                return 'SAINT LUCIA';
            break;
                
            case 'PN':
                return 'SAINT PIERRE AND MIQUELON';
            break;
                
            case 'VC':
                return 'SAINT VINCENT AND THE GRENADINES';
            break;
                
            case 'WS':
                return 'SAMOA';
            break;
                
            case 'SM':
                return 'SAN MARINO';
            break;
                
            case 'ST':
                return 'SAO TOME AND PRINCIPE';
            break;
                
            case 'SA':
                return 'SAUDI ARABIA';
            break;
                
            case 'SN':
                return 'SENEGAL';
            break;
                
            case 'SC':
                return 'SEYCHELLES';
            break;
                
            case 'SL':
                return 'SIERRA LEONE';
            break;
                
            case 'SG':
                return 'SINGAPORE';
            break;
                
            case 'SK':
                return 'SLOVAKIA';
            break;
                
            case 'SI':
                return 'SLOVENIA';
            break;
                
            case 'SB':
                return 'SOLOMON ISLANDS';
            break;
                
            case 'SO':
                return 'SOMALIA';
            break;
                
            case 'ZA':
                return 'SOUTH AFRICA';
            break;
                
            case 'GS':
                return 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS';
            break;
                
            case 'ES':
                return 'SPAIN';
            break;
                
            case 'LK':
                return 'SRI LANKA';
            break;
                
            case 'SD':
                return 'SUDAN';
            break;
                
            case 'SR':
                return 'SURINAME';
            break;
                
            case 'SJ':
                return 'SVALBARD AND JAN MAYEN';
            break;
                
            case 'SZ':
                return 'SWAZILAND';
            break;
                
            case 'SE':
                return 'SWEDEN';
            break;
                
            case 'CH':
                return 'SWITZERLAND';
            break;
                
            case 'SY':
                return 'SYRIAN ARAB REPUBLIC';
            break;
                
            case 'TW':
                return 'TAIWAN, PROVINCE OF CHINA';
            break;
                
            case 'TJ':
                return 'TAJIKISTAN';
            break;
                
            case 'TZ':
                return 'TANZANIA, UNITED REPUBLIC OF';
            break;
                
            case 'TH':
                return 'THAILAND';
            break;
                
            case 'TG':
                return 'TOGO';
            break;
                
            case 'TK':
                return 'TOKELAU';
            break;
                
            case 'TO':
                return 'TONGA';
            break;
                
            case 'TT':
                return 'TRINIDAD AND TOBAGO';
            break;
                
            case 'TN':
                return 'TUNISIA';
            break;
                
            case 'TR':
                return 'TURKEY';
            break;
                
            case 'TM':
                return 'TURKMENISTAN';
            break;
                
            case 'TM':
                return 'TURKS AND CAICOS ISLANDS';
            break;
                
            case 'TV':
                return 'TUVALU';
            break;
                
            case 'UG':
                return 'UGANDA';
            break;
                
            case 'AE':
                return 'UNITED ARAB EMIRATES';
            break;
                
            case 'GB':
                return 'UNITED KINGDOM';
            break;
                
            case 'US':
                return 'UNITED STATES';
            break;
                
            case 'UM':
                return 'UNITED STATES MINOR OUTLYING ISLANDS';
            break;
                
            case 'UY':
                return 'URUGUAY';
            break;
                
            case 'UZ':
                return 'UZBEKISTAN';
            break;
                
            case 'VU':
                return 'VANUATU';
            break;
                
            case 'VE':
                return 'VENEZUELA';
            break;
                
            case 'VN':
                return 'VIET NAM';
            break;
                
            case 'VG':
                return 'VIRGIN ISLANDS, BRITISH';
            break;
                
            case 'VI':
                return 'VIRGIN ISLANDS, U.S.';
            break;
                
            case 'WF':
                return 'WALLIS AND FUTUNA';
            break;
                
            case 'EH':
                return 'WESTERN SAHARA';
            break;
                
            case 'YE':
                return 'YEMEN';
            break;
                
            case 'YU':
                return 'YUGOSLAVIA';
            break;
                
            case 'ZM':
                return 'ZAMBIA';
            break;
                
            case 'ZW':
                return 'ZIMBABWE';
            break;
                
            default:
                return $country_code;
        }
    }
    
    //Set Jobpost Position Number.
    public function set_job_order_num($id = null) {
        $this->loadModel('Jobposts');
        
        $jobposts_count = $this->Jobposts->find()
            ->count();
        
        if (!$id) {
            $order_num = $jobposts_count + 1;
            
            return $order_num;
        } else {
            $jobpost = $this->Jobposts->get($id);
            $jobpost->pos = 0;
            
            if ($this->Jobposts->save($jobpost)) {
                $this->jobs_reorder();
            }
        }
    }
    
    //Reorder Jobposts position.
    private function jobs_reorder() {
        $this->loadModel('Jobposts');    
        
        $i = 0;
        
        $jobposts = $this->Jobposts->find()
            ->order(['pos' => 'DESC']);
        
        foreach ($jobposts as $post) {
            $i++;
                
            $job = $this->Jobposts->get($post['id']);
            $job->pos = $i;
            $this->Jobposts->save($job);
        }
    }
    
    //Converts date time to string that how long the datetime pass from the current time.
    public function when($datetime) {
        $second = 1;
        $minute = 60 * $second;
        $hour = 60 * $minute;
        $day = 24 * $hour;
        
        $current = time() - strtotime($datetime);
        
        if ($current < 1 * $minute) {
            return 'About few seconds ago.';
        }
        
        if ($current < 1 * $minute) {
            return 'About a minute ago';
        }
        
        if ($current < 60 * $minute) {
            return floor($current / $minute) . ' minutes ago.';
        }
        
        if ($current < 119 * $minute) {
            return 'About an hour ago';
        }
        
        if ($current < 24 * $hour) {
            return floor($current / $hour) . ' hours ago.';
        }
        
        if ($current < 60 * $hour) {
            return 'About a day ago.';
        }
        
        if ($current < 30 * $day) {
            return floor($current / $day) . ' days ago.';
        } else {
            return 'Posted on: ' . date('M j, Y h:i:a', strtotime($datetime));
        }
    }
    
    //Returns datetime format to a readable date.
    public function datetimetodate($datetime) {
        return date('F j, Y h:i A', strtotime($datetime));
    }

    /*
    *   Get All Job Information.
    */
    public function get_job_post($id) {
        $this->loadModel('Jobposts');

        $jobposts = $this->Jobposts->find()
            ->where(['id' => $id]);

        return $jobposts;
    } 

    /*
    *   Method to delete Jobpost header image.
    */
    public function header_image_delete($filename) {
        if ($filename) {
            $path = './img/users/job_header/';

            if (file_exists($path . $filename)) {
                if (unlink($path . $filename)) {
                    return true;
                }
            }
        }
    }

    /*
    *   Method to move Header Image from temp folder to job_header dir.
    */
    public function header_image_move($filename) {
        if ($filename) {
            $tmp_path = './img/users/job_header_tmp/';
            $header_path = './img/users/job_header/';

            if (file_exists($tmp_path . $filename)) {
                if (copy($tmp_path . $filename, $header_path . $filename)) {
                    $this->loadModel('Jobheaderimg_tmp');
                    
                    $jobheader = $this->Jobheaderimg_tmp->find()
                        ->where(['filename' => $filename]);

                    $id = '';

                    foreach ($jobheader as $header) {
                        $id .= $header['id'];
                    }

                    $tmp = $this->Jobheaderimg_tmp->get($id);

                    if ($this->Jobheaderimg_tmp->delete($tmp)) {
                        if (file_exists($tmp_path . $filename)) {
                            if (unlink($tmp_path . $filename)) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
    }

    /*
    *   Delete Company Logo Method.
    */
    public function company_logo_delete($filename) {
        if ($filename) {
            $path = './img/users/company_logo/';

            if (file_exists($path . $filename)) {
                if (unlink($path . $filename)) {
                    return true;
                }
            }
        }
    }

    /*
    *   Method to move company logo from temp dir to company_logo dir.
    */
    public function company_logo_move($filename) {
        if ($filename) {
            $tmp_path = './img/users/company_logo_tmp/';
            $logo_path = './img/users/company_logo/';

            if (file_exists($tmp_path . $filename)) {
                if (copy($tmp_path . $filename, $logo_path . $filename)) {
                    $this->loadModel('Companylogo_tmp');

                    $companylogo = $this->Companylogo_tmp->find()
                        ->where(['filename' => $filename]);

                    $id = '';

                    foreach ($companylogo as $logo) {
                        $id .= $logo['id'];
                    }

                    $tmp = $this->Companylogo_tmp->get($id);

                    if ($this->Companylogo_tmp->delete($tmp)) {
                        if (file_exists($tmp_path . $filename)) {
                            if (unlink($tmp_path . $filename)) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
    }

    /*
    *   Filter Html Tags that can cause xss attact.
    */
    public function filter_description($str) {
        $old = [];
        $old[] = '/<script[^>]+>/i';
        $old[] = '</script>';
        $old[] = '/<form[^>]+>/i';
        $old[] = '</form>';
        $old[] = "/<img[^>]+>/i";
        $old[] = "/<input[^>]+>/i";
        $old[] = '/<textarea[^>]+>/i';
        $old[] = '/<style[^>]+>/i';
        $old[] = 'javascript:';
        $old[] = '/<a[^>]+>/i';

        $new = [];
        $new[] = '';
        $new[] = '';
        $new[] = '';
        $new[] = '';
        $new[] = '';
        $new[] = '';
        $new[] = '';
        $new[] = '';
        $new[] = '';
        $new[] = '';

        return str_ireplace($old, $new, $str);
    }
    
    public function urldecode($str) {
        $old = [];
        $old[] = '_';
        $old[] = '-';
        
        $new = [];
        $new[] = '/';
        $new[] = ' ';
        
        return str_ireplace($old, $new, $str);
    }
    
    public function urlencode($str) {
        $old = [];
        $old[] = '/';
        $old[] = ' ';
        
        $new = [];
        $new[] = '_';
        $new[] = '_';
        
        return str_ireplace($old, $new, $str);
    }

    //Get Jobpost's Job Title via ID.
    public function get_job_title($id) {

        //Load Jobposts Model.
        $this->loadModel('Jobposts');

        //Query Jobpost.
        $jobposts = $this->Jobposts->find()
            ->select(['job_title'])
            ->where(['id' => $id]);

        //Job Title Value.
        $job_title = '';

        //Loop Result.
        foreach ($jobposts as $post) {
            
            //Append Value.
            $job_title = $post['job_title'];
        }

        return $job_title;
    }

    //User's Unread Message Count. 
    public function unread_messages_count($user_id) {

        //Load Messages Model.
        $this->loadModel('Messages');

        $messages_count = $this->Messages->find()
            ->where(['user_id' => $user_id])
            ->andWhere(['unread' => 1])
            ->count();

        return $messages_count;
    }

    //Display User's Unread Messages count as Notification to nav bars.
    public function display_unread_msgs_count() {
        
        //Load Messages Model.
        $this->loadModel('Messages');

        //Count User's Unread Messages.
        $count = $this->Messages->find()
            ->where(['user_id' => $this->request->session()->read('Auth.User')['id']])
            ->andWhere(['unread' => 1])
            ->count();

        //If Count not equal to 0.
        if ($count !== 0) {
            //If Unread Messages Count is greaterthan 100.
            if ($count < 100) {
                return $count;
            } else {
                return 100 . '+';
            }
        }

        return '';
    }
    
    //User's job invitation count.
    public function ji_unread_msgs_count($user_id) {
        
        //Load Messages Model.
        $this->loadModel('Messages');

        $messages_count = $this->Messages->find()
            ->where(['user_id' => $user_id])
            ->andWhere(['message_type' => 'job_invitation'])
            ->andWhere(['sent_item !=' => 1])
            ->count();

        return $messages_count;
    }

    public function ja_unread_msgs_count($user_id) {

        //Load Messages Model.
        $this->loadModel('Messages');

        $messages_count = $this->Messages->find()
            ->where(['user_id' => $user_id])
            ->andWhere(['message_type' => 'job_application'])
            ->andWhere(['sent_item !=' => 1])
            ->count();

        return $messages_count;
    }

    public function from_admin_unread_msg_count($user_id) {
        
        //Load Messages Model.
        $this->loadModel('Messages');

        $messages_count = $this->Messages->find()
            ->where(['user_id' => $user_id])
            ->andWhere(['message_type' => 'employer_msg_from_admin'])
            ->andWhere(['sent_item !=' => 1])
            ->count();

        return $messages_count;
    }

    public function employer_sent_items($user_id) {

        //Load Messages Model.
        $this->loadModel('Messages');

        $messages_count = $this->Messages->find()
            ->where(['sender_id' => $user_id])
            ->andWhere(['sent_item' => 1])
            ->count();

        return $messages_count;
    }
    
    public function candidate_sent_items_count($user_id) {
        
        //Load Messages Model.
        $this->loadModel('Messages');

        $messages_count = $this->Messages->find()
            ->where(['sender_id' => $user_id])
            ->andWhere(['sent_item' => 1])
            ->count();

        return $messages_count;
    }

    //Check if User is suspended.
    public function is_suspended() {
        if ($this->request->session()->read('Auth.User')) {
            
            //Load Home Model.
            $this->loadModel('Home');

            //Get user's suspend status.
            $users = $this->Home->find()
                ->select(['suspend'])
                ->where(['email' => $this->request->session()->read('Auth.User')['email']]);

            $suspend = 0;

            foreach ($users as $u) {
                $suspend = $u['suspend'];
            }

            if ($suspend) {
                return $this->redirect('/logout');
            }

            return true;
        }
    }

    //Check if user has profile.
    public function has_profile($user_id) {

        //Load Profiles Model.
        $this->loadModel('Profiles');

        //Count user's profile.
        //If Count is 0, User's profile does not exists.
        //If Count is 1, User's profile is exists.
        $profiles_count = $this->Profiles->find()
            ->where(['user_id' => $user_id])
            ->count();

        return $profiles_count;
    }

    //Check if user has profile picture.
    //Return profile_picture filename or null.
    public function user_profile_picture($user_id) {

        //Load Profile Model.
        $this->loadModel('Profiles');

        //Get User's Row Record.
        $profiles = $this->Profiles->find()
            ->where(['user_id' => $user_id]);

        //Profile picture filename.
        $profile_picture = '';

        //Loop Result to get profile_picture filename.
        foreach ($profiles as $profile) {

            //Append value.
            $profile_picture .= $profile['profile_picture'];
        }

        if ($profile_picture) {
            return $profile_picture;
        }

        return null;
    }

    //Check if user has messages.
    public function has_messages($user_id) {

        //Load Messages Model.
        $this->loadModel('Messages');

        //Count User's Messages.
        $messages = $this->Messages->find()
            ->where(['user_id' => $user_id])
            ->orWhere(['sender_id' => $user_id])
            ->count();

        return $messages;
    }

    //Check if user has resume.
    public function has_resume($user_id) {

        //Load Resume Model.
        $this->loadModel('Resumes');

        //Count User's Resume.
        $resumes = $this->Resumes->find()
            ->where(['user_id' => $user_id])
            ->count();

        return $resumes;
    }

    //Check if user has job posts.
    public function has_job_posts($user_id) {
        
        //Load Jobposts Model.
        $this->loadModel('Jobposts');

        //Count User's Jobposts.
        $jobposts = $this->Jobposts->find()
            ->where(['user_id' => $user_id])
            ->count();

        return $jobposts;
    }

    public function profile_user_notification($user_id, $msg) {

        //Load Messages Model.
        $this->loadModel('Messages');

        //New Entity.
        $messages = $this->Messages->newEntity();

        //Message Data.
        $data = [
            'user_id' => $user_id,
            'sender_id' => $this->request->session()->read('Auth.User')['id'],
            'unread' => 1,
            'sent_item' => 0,
            'message_type' => 'employer_msg_from_admin',
            'subject' => 'Your profile has been modified',
            'message' => $msg,
            'date' => date( "Y-m-d H:i:s" ),
        ];

        //Patch Entity.
        $messages = $this->Messages->patchEntity($messages, $data);

        //Send Message.
        if ($this->Messages->save($messages)) {

            /*
            *   Send Email Message to the user
            *   and get the user's email address.
            */

            //Load Home Model.
            $this->loadModel('Home');

            //User's Email Value.
            $user_email = '';

            //Query to get email.
            $home = $this->Home->find()
                ->select(['email'])
                ->where(['id' => $user_id]);

            //Loop to get Email.
            foreach ($home as $user) {
                $user_email = $user['email'];
            }

            /*
            *   Create Email Message.
            */
            $from = [$this->_server_email_address, 'Globalworks'];
            $to = $user_email;
            $subject = 'Your profile has been modified';
            $message = '<p>Your profile has been modified.</p> <p>Check your message panel.</p> <p><a href="' . $this->_url . '/login">Click Here</a> to login.</p>';
            
            //Email Data.
            $email_contents = [
                'from' => $from,
                'to' => $to,
                'subject' => $subject,
                'html' => true,
                'message' => $message,
            ];

            //Send Email Message.
            if ($this->mail($email_contents)) {
                echo 'ok';
            }
        }
    }

    //Shorten announcement output.
    public function ann_shorten($ann) {
        
        //Announcement string length.
        $len = strlen($ann);

        if ($len > 50) {
            return substr($ann, 0, 50) . ' ...';
        }

        return $ann;
    }

    public function test() {
        return "hello";
    }

    //Print out published announcement.
    public function show_announcement() {

        //Load Announcements Model.
        $this->loadModel('Announcements');

        //Expiration 1 year.
        $expiration = time() + (86400 * 30 * 12);

        //Query Published announcement.
        $announcements = $this->Announcements->find()
            ->select(['id', 'announcement'])
            ->where(['published' => 1]);

        //Create announcement cookie if not existed.
        if (!isset($_COOKIE['announcement'])) {

            //Loop results
            foreach ($announcements as $announcement) {

                //Register announcement ID in the cookie.
                setcookie('announcement', '[' . $announcement['id'] . ']', $expiration, '/');

                return $announcement['announcement'];
            }

            return '';
        } else {

            //Decode JSON.
            $arr = json_decode($_COOKIE['announcement']);

            //Loop Results.
            foreach ($announcements as $announcement) {

                //If announcement ID is not on the array list.
                if (!in_array($announcement['id'], $arr)) {
                    array_push($arr, $announcement['id']);

                    setcookie('announcement', json_encode($arr), $expiration, '/');

                    return $announcement['announcement'];
                }

                return '';
            }
        }
    }

    //Create Log Activities.
    public function _log($log_type, $msg) {

        //Load Logs Model.
        $this->loadModel('Logs');

        //New Entity.
        $logs = $this->Logs->newEntity();

        //Entry data.
        $data = [
            'log_type' => $log_type,
            'message' => $msg,
            'date' => date( "Y-m-d H:i:s" ),
        ];

        //Patch Entity.
        $logs = $this->Logs->patchEntity($logs, $data);

        //Save Log.
        if ($this->Logs->save($logs)) {
            return true;
        }

        return false;
    }

    /*
    *   Employers Suggested Candidates. Depends on jobposts qualifications.
    */
    public function suggested_candidates() {

        /*
        *   Check if user has jobposts.
        */

        //Load Jobposts Model.
        $this->loadModel('Jobposts');

        //Query Jobposts.
        $jobposts_count = $this->Jobposts->find()
            ->where(['user_id' => $this->request->session()->read('Auth.User')['id']])
            ->count();

        //If has Jobposts.
        if ($jobposts_count) {

            /*
            *   Get Jobposts Qualifications. This will be use as keywords to find matched candidates.
            */
            
            //Query Jobposts.
            $jobposts = $this->Jobposts->find()
                ->select(['job_qualifications'])
                ->where(['user_id' => $this->request->session()->read('Auth.User')['id']])
                ->andWhere(['job_qualifications !=' => ''])
                ->andWhere(['closing_date >=' => date( "Y-m-d H:i:s" )]);

            //Qualifications Value.
            $qualifications = [];

            //Loop Results.
            foreach ($jobposts as $post) {

                //Loop Arrays and push to qualifications value.
                for ($x = 0; $x < count(json_decode($post['job_qualifications'])); $x++) {
                    array_push($qualifications, json_decode($post['job_qualifications'])[$x]);
                }
            }

            //Implode and Explode Qualifications.
            $qualifications = implode(' ', $qualifications);
            $qualifications = explode(' ', $qualifications);

            /*
            *   Generate candidates matches the qualifications.
            */

            //Load Profiles Model.
            $this->loadModel('Profiles');

            //Query Profiles.
            $profiles = $this->Profiles->find();
            $profiles->contain(['Users']);
            $profiles->select(['profiles.user_id', 'profiles.profile_picture', 'users.firstname', 'users.lastname', 'profiles.nickname', 'profiles.job_title']);
            $profiles->where(['users.role' => 'Applicant']);
            $profiles->andWhere(['users.suspend' => 0]);

            $i = 0;

            /*
            *   Loop Qualification's arrays to generate results.
            */
            foreach ($qualifications as $q) {

                $i++;

                if ($i == 1) {
                    $profiles->andWhere(['profiles.skills LIKE' => '%' . $q . '%']);
                } else {
                    $profiles->orWhere(['profiles.skills LIKE' => '%' . $q . '%']);
                }
            }

            $profiles->limit(5);
            $profiles->order(['rand()']);

            $count = 0;

            foreach ($profiles as $profile) {
                $count++;
            }

            if ($count) {

               return $profiles;
            } else {
                return null;
            }
        } else {
           return null;

        }
    }

    /*
    *   Candidates Recommended Jobs. Depends on user's profile.
    */
    public function suggested_jobs() {

        /*
        *   Check if user has profile.
        */
        
        //Load Profiles Model.
        $this->loadModel('Profiles');

        //Query and count profile.
        $profiles_count = $this->Profiles->find()
            ->where(['user_id' => $this->request->session()->read('Auth.User')['id']])
            ->count();

        if ($profiles_count) {
            
            /*
            *   Check if Skills is set and not empty.
            *   Use Experience as keywords.
            */
            
            //Query Profiles.
            $profiles = $this->Profiles->find()
                ->select(['skills'])
                ->where(['user_id' => $this->request->session()->read('Auth.User')['id']]);

            //Skills Value.
            $skills = '';

            //Loop Results.
            foreach ($profiles as $profile) {
                $skills .= $profile['skills'];
            }

            //If Skill is not null or empty, find a job that matches jobposts qualifications.
            if ($skills) {

                //Implode Skills and Explode Skills via spaces.
                $skills = implode(' ', json_decode($skills));
                $skills = explode(' ', $skills);

                //Load Jobposts Model.
                $this->loadModel('Jobposts');

                //Query match Jobposts.
                $jobposts = $this->Jobposts->find();
                $jobposts->contain(['Users']);
                $jobposts->select(['jobposts.id', 'jobposts.company_logo', 'jobposts.job_title', 'jobposts.company_name', 'jobposts.job_description', 'users.firstname', 'users.lastname', 'jobposts.created_at']);
                $jobposts->where(['jobposts.closing_date >=' => date( "Y-m-d H:i:s" )]);
                $jobposts->andWhere(['.users.suspend' => 0]);

                $i = 0;

                foreach ($skills as $skill) {
                    $i++;

                    if ($i == 1) {
                        $jobposts->andWhere(['jobposts.job_qualifications LIKE' => '%' . $skill . '%']);
                    } else {
                        $jobposts->orWhere(['jobposts.job_qualifications LIKE' => '%' . $skill . '%']);
                    }
                }

                $jobposts->limit(5);
                $jobposts->order(['rand()']);

                $count = 0;

                foreach ($jobposts as $post) {
                    $count++;
                }

                if ($count) {
                    return $jobposts;
                }

                return null;
            } else {
                return null;
            }

        } else {
            return null;
        }
    }

    /*
    *   Show Latest Candidates.
    */
    public function lastest_candidates() {
        
        //Load Home Model.
        $this->loadModel('Profiles');

        //Query Home.
        $profiles = $this->Profiles->find()
            ->contain(['Users'])
            ->select(['profiles.user_id', 'profiles.profile_picture', 'users.firstname', 'users.lastname', 'profiles.nickname', 'profiles.job_title'])
            ->andWhere(['users.suspend' => 0])
            ->andWhere(['users.role' => 'Applicant'])
            ->order(['profiles.id' => 'DESC'])
            ->limit(5);

        return $profiles;
    }
    
    /*
    *   Show Latest Jobposts.
    */
    public function last_jobposts() {

        //Load Jobposts Model.
        $this->loadModel('Jobposts');

        //Query Jobposts.
        $jobposts = $this->Jobposts->find()
            ->contain(['Users'])
            ->select(['jobposts.id', 'jobposts.company_logo', 'jobposts.job_title', 'jobposts.company_name', 'jobposts.job_description', 'users.firstname', 'users.lastname', 'jobposts.created_at'])
            ->where(['jobposts.closing_date >=' => date( "Y-m-d H:i:s" )])
            ->limit(5)
            ->order(['pos' => 'DESC']);

        return $jobposts;
    }

    /*
    *   Remove Email From Subscribers Table.
    */
    public function unsubscribe($email) {

        //Load Subscribers Table.
        $this->loadModel('Subscribers');

        /*
        *   Get record ID.
        */

        //ID Value.
        $id = 0;

        //Query to get ID.
        $subscribers = $this->Subscribers->find()
            ->select(['id'])
            ->where(['email' => $email]);

        //Loop result to append ID.
        foreach ($subscribers as $sub) {

            //Append ID.
            $id += $sub['id'];
        }

        /*
        *   Get Record.
        */
        $subscriber = $this->Subscribers->get($id);

        //Delete Record.
        if ($this->Subscribers->delete($subscriber)) {
            
            return true;
        }

        return false;
    }

    /*
    *   Check Email is subscribed or not in Subscribers Table via Email.
    */
    public function is_subscribe($email) {

        //Load Subscribers Model.
        $this->loadModel('Subscribers');

        //Query and Count Email.
        $subscribers_count = $this->Subscribers->find()
            ->where(['email' => $email])
            ->count();

        if ($subscribers_count) {
            
            return true;
        }

        return false;
    }

    /*
    *   Send Message to subscribers.
    */
    public function send_subscribers($subject, $message) {
        
        //Load Subscribers Model.
        $this->loadModel('Subscribers');

        //Query Subscribers.
        $subscribers = $this->Subscribers->find()
            ->order(['id' => 'DESC']);

        //Loop Results.
        foreach ($subscribers as $subscriber) {

             //Append Message.
            $message .= '<p>To unsubscribe <a href="' . $this->_url . '/unsubscribe/' . $subscriber['email'] .'">Click Here</a>.</p>';
             
            //Email Message Properties.
            $email = [
                'from' => [$this->_server_email_address, 'Globalworks'],
                'html' => true,
                'subject' => $subject,
                'to' => $subscriber['email'],
                'message' => $message,
            ];

            //Send Message.
            if ($this->mail($email)) {
                return true;
            }

            return false;
        }
    }

    /*
    *   Page Pagination.
    */
    public function _paginate($total_pages, $limit, $page) {

        $prev = $page - 1;
        $next = $page + 1;
        $lastpage = ceil($total_pages / $limit);
        $lastpagem1 = $lastpage - 1;
        $stages = 4;

        $paginate = '';
        $link = '?';

        if (isset($_GET)) {
            if (isset($_GET['page'])) {
                unset($_GET['page']);
            }

            foreach ($_GET as $query => $val) {
                $link .= $query . '='. $val . '&';
            }
        }

        if ($lastpage > 1) {

            $paginate .= '<div class="paginate">';
            
                //Preview Button
                if ($page > 1) {
                    $paginate .= '<a href="'. $link .'page='.$prev.'">Prev</a>';
                }
                else {
                    $paginate .= '<span class="current">Prev</span>';
                }
                //Pages
                if ($lastpage <= 7 + ($stages * 2)) {
                    for ($counter = 1; $counter <= $lastpage; $counter++) {
                        if ($counter == $page) {
                            $paginate .= '<span class="current">'.$counter.'</span>';
                        }
                        else {
                            $paginate .= '<a href="'. $link .'page='.$counter.'">'.$counter.'</a>';
                        }
                    }
                }
                else if ($lastpage > 5 + ($stages * 2)) {
                    //Beginning only hide later page
                    if ($page < 5 + ($stages * 2)) {
                        for ($counter = 1; $counter < 2 + ($stages * 2); $counter++) {
                            if ($counter == $page) {
                                $paginate .= '<span class="current">'.$counter.'</span>';
                            }
                            else {
                                $paginate .= '<a href="'. $link .'page='.$counter.'">'.$counter.'</a>';
                            }
                        }
                        $paginate .= '...';
                        $paginate .= '<a href="'. $link .'page='.$lastpagem1.'">'.$lastpagem1.'</a>';
                        $paginate .= '<a href="'. $link .'page='.$lastpage.'">'.$lastpage.'</a>';
                    }
                    //Middle hide some front and some back
                    else if ($lastpage - ($stages * 2) > $page && $page > ($stages * 2)) {
                        $paginate .= '<a href="'. $link .'page=1">1 </a>';
                        $paginate .= '<a href="'. $link .'page=2">2 </a>';
                        $paginate .= '...';
                        for ($counter = $page - $stages; $counter <= $page + $stages; $counter++) {
                            if ($counter == $page) {
                                $paginate .= '<span class="current">'.$counter.'</span>';
                            }
                            else {
                                $paginate .= '<a href="?page='.$counter.'">'.$counter.'</a>';
                            }
                        }
                        $paginate .= '...';
                        $paginate .= '<a href="'. $link .'page='.$lastpagem1.'">'.$lastpagem1.'</a>';
                        $paginate .= '<a href="'. $link .'page='.$lastpage.'">'.$lastpage.'</a>';
                    }
                    //End only early pages
                    else {
                        $paginate .= '<a href="'. $link .'page=1">1</a>';
                        $paginate .= '<a href="'. $link .'page=2">2</a>';
                        $paginate .= '...';
                        for ($counter = $lastpage - (5 + ($stages * 2)); $counter <= $lastpage; $counter++) {
                            if ($counter == $page) {
                                $paginate .= '<span class="current">'.$counter.'</span>';
                            }
                            else {
                                $paginate .= '<a href="'. $link .'page='.$counter.'">'.$counter.'</a>';
                            }
                        }
                    }
                }
            if ($page < $counter - 1) {
                $paginate .= '<a href="'. $link .'page='.$next.'">Next</a>';
            }
            else {
                $paginate .= '<span class="current">Next</span>';
            }
            
            $paginate .= '</div>';

            return $paginate;
        }
    }

    /*
    *   Page Pagination Mobile.
    */
    public function _m_paginate($total_pages, $limit, $page) {
        $prev = $page - 1;
        $next = $page + 1;
        $lastpage = ceil($total_pages / $limit);

        $link = '?';

        if (isset($_GET)) {
            if (isset($_GET['page'])) {
                unset($_GET['page']);
            }

            foreach ($_GET as $query => $val) {
                $link .= $query . '='. $val . '&';
            }
        }

        if ($lastpage > 1) {
            $paginate = '<div class="m_paginate">';

            if ($page == 1) {
                $paginate .= '<span class="disabled">Prev</span>';
            } else {
                $paginate .= '<a href="'.$link .'page='. $prev .'">Prev</a>';
            }

            $paginate .= '<select id="m_pagi">';
            
            for ($i = 0; $i < $lastpage; $i++) {

                $i2 = $i + 1;

                if ($i2 == $page) {
                    $paginate .= '<option value="'. $i2 .'" selected>Page '. $i2 .'</option>';
                } else {
                    $paginate .= '<option value="'. $i2 .'">Page '. $i2 .'</option>';
                }
            }

            $paginate .= '</select>';

            if ($page == $lastpage) {
                $paginate .= '<span class="disabled">Next</span>';
            } else {
                $paginate .= '<a href="'.$link .'page='.$next.'">Next</a>';
            }

            $paginate .= '</div>';

            $paginate .= '
                <script>
                    $("#m_pagi").on("change", function() {
                        location = "'.$link .'page=" + $(this).val();
                    });
                </script>
            ';

            return $paginate;
        }
    }
}
