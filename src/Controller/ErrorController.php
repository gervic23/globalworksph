<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         3.3.4
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Event\Event;

/**
 * Error Handling Controller
 *
 * Controller used by ExceptionRenderer to render error responses.
 */
class ErrorController extends AppController {

    private $globalworks;

    /**
     * Initialization hook method.
     *
     * @return void
     */
    public function initialize() {
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Csrf');
    }

    /**
     * beforeFilter callback.
     *
     * @param \Cake\Event\Event $event Event.
     * @return \Cake\Http\Response|null|void
     */
    public function beforeFilter(Event $event) {
        $this->globalworks = new GlobalworksController();
    }

    /**
     * beforeRender callback.
     *
     * @param \Cake\Event\Event $event Event.
     * @return \Cake\Http\Response|null|void
     */
    public function beforeRender(Event $event) {

        parent::beforeRender($event);

        $title = '404 Page Not Found - Globalworks';

        //Set View Template.
        $this->viewBuilder()->setTemplatePath('error');

        $this->set('title', $title);   
        $this->set('username', $this->globalworks->welcome_greetings());
        $this->set('thumbnail', $this->globalworks->image_thumbnail());
    }

    /**
     * afterFilter callback.
     *
     * @param \Cake\Event\Event $event Event.
     * @return \Cake\Http\Response|null|void
     */
    public function afterFilter(Event $event) {

    }
}
