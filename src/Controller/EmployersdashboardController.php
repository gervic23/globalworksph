<?php
namespace App\Controller;

use Cake\Event\Event;

/*
*   Controller for Employers.
*/
class EmployersdashboardController extends AppController {
    private $globalworks;
    
    public $paginate = ['limit' => 30];
    
    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->loadComponent('Paginator');
        $this->globalworks = new GlobalworksController();
        
        //Check Visitor's IP Info.
        $this->globalworks->ip_info_init();

        //Check User's Account If Exists.
        $this->globalworks->check_auth();

        //Check User has not confirm his account yet.
        if ($this->globalworks->check_confirmed_user()) {
            return $this->redirect('/account_confirmation');
        }

        //Announcement.
        $this->set('pop_announcement', $this->globalworks->show_announcement());

        //Check if user is suspended. Auto logout user.
        return $this->globalworks->is_suspended();
    }
    
    /*
    *   Employer's Dashboard Page.
    */
    public function index() {
        //Get User Role.
        $role = $this->request->session()->read('Auth.User')['role'];
        
        if ($role == 'Employer') {

            $title = 'Dashboard - Globalworks';

            $this->loadModel('Users');
        
            $recent_candidates = $this->Users->find()
                ->where(['role' => 'Applicant'])
                ->limit(10)
                ->order(['id' => 'DESC']);

            $this->set('title', $title);
            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());
            $this->set('recent_candidates', $recent_candidates);
        } else {
            return $this->globalworks->redirect2();
        }
    }
    
    /*
    *   Employer's Job Post.
    */
    public function jobpost() {

        //If User has Employer Role.
        if ($this->request->session()->read('Auth.User')['role'] == 'Employer') {

            $title = 'Post Job - Globalworks';

            //Load Jobposts Model.
            $this->loadModel('Jobposts');

            //Load Profiles Model.
            $this->loadModel('Profiles');

            //New Entity.
            $jobpost = $this->Jobposts->newEntity();

            $company_name = '';

            //Count if User has Profiles Record.
            $profiles_count = $this->Profiles->find()
                ->where(['user_id' => $this->request->session()->read('Auth.User')['id']])
                ->count();

            //If Count is not 0. User has Profile Record.
            if ($profiles_count > 0) {

                //Query out to get business_name.
                $profiles = $this->Profiles->find()
                    ->select(['business_name'])
                    ->where(['user_id' => $this->request->session()->read('Auth.User')['id']]);

                //Loop Result.
                foreach ($profiles as $pro) {
                    $company_name .= $pro['business_name'];
                }
            }

            //Render company_name to view.
            $this->set('company_name', $company_name);
            
            $this->set('title', $title);
            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());
        } else {
            return $this->globalworks->redirect2();
        }
    }

    /*
    *   Messages Page.
    */
    public function messages() {

        //If User has Employer Role.
        if ($this->request->session()->read('Auth.User')['role'] == 'Employer') {
            
            $title = 'Messages - Globalworks';

            $this->set('title', $title);
            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());
        } else {
            return $this->globalworks->redirect2();
        }
    }

    /*
    *   Search Candidates Page.
    */
    public function search() {

        //If User has Employer Role.
        if ($this->request->session()->read('Auth.User')['role'] == 'Employer') {

            $title = 'Search Candidate - Globalworks';

            //If has GET Request.
            if ($this->request->query && strlen($this->request->query('keywords')) >= 3) {

                //Set Template.
                $this->viewBuilder()->template('search_results');

                $title = $this->request->query('keywords') . ' - Globalworks';

                //Load Profiles Model.
                $this->loadModel('Profiles');

                $limit = 30;

                if ($this->request->query('page')) {
                    $page = (int)$this->request->query('page');
                } else {
                    $page = 1;
                }

                //Result Count.
                $count = 0;

                //Query out to count results.
                $users_count = $this->Profiles->find();
                $users_count ->contain(['Users']);
                $users_count ->where(['profiles.experience LIKE' => '%' . $this->request->query('keywords') . '%']);
                $users_count ->orWhere(['profiles.skills LIKE' => '%' . $this->request->query('keywords') . '%']);
                $users_count ->orWhere(['profiles.job_title LIKE' => '%' . $this->request->query('keywords') . '%']);

                if ($this->request->query('desire_salary')) {
                    $d_salary = explode(',', $this->request->query('desire_salary'));

                    foreach ($d_salary as $salary) {
                        $users_count->andWhere(['profiles.desire_salary LIKE' => '%' . $salary . '%']);
                    }
                }

                $users_count->andWhere(['users.role' => 'Applicant']);

                //If employment_status is set.
                if ($this->request->query('employment_status') !== 'All') {
                    $users_count->andWHere(['profiles.employment_status' => $this->request->query('employment_status')]);
                }

                //If country is set.
                if ($this->request->query('country')) {
                    $users_count->andWhere(['users.country' => $this->request->query('country')]);
                }

                foreach ($users_count as $user) {
                    $count++;
                }

                //Query out Search Results.
                $users = $this->Profiles->find();
                $users->select(['users.firstname', 'users.lastname', 'profiles.user_id', 'profiles.nickname', 'profiles.job_title', 'profiles.profile_picture']);
                $users->contain(['Users']);
                $users->where(['profiles.experience LIKE' => '%' . $this->request->query('keywords') . '%']);
                $users->orWhere(['profiles.skills LIKE' => '%' . $this->request->query('keywords') . '%']);
                $users->orWhere(['profiles.job_title LIKE' => '%' . $this->request->query('keywords') . '%']);

                if ($this->request->query('desire_salary')) {
                    $d_salary = explode(',', $this->request->query('desire_salary'));

                    foreach ($d_salary as $salary) {
                        $users->andWhere(['profiles.desire_salary LIKE' => '%' . $salary . '%']);
                    }
                }

                $users->andWhere(['users.role' => 'Applicant']);

                //If employment_status is set.
                if ($this->request->query('employment_status') !== 'All') {
                    $users->andWHere(['profiles.employment_status' => $this->request->query('employment_status')]);
                }

                //If country is set.
                if ($this->request->query('country')) {
                    $users->andWhere(['users.country' => $this->request->query('country')]);
                }

                $users->order(['users.id' => 'DESC']);
                $users->limit($limit);
                $users->page($page);

                $this->set('result_count', $count);
                $this->set('limit', $limit);
                $this->set('page', $page);
                $this->set('users', $users);
            }
            
            $this->set('title', $title);
            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());
        } else {
            return $this->globalworks->redirect2();
        }
    }
    
    /*
    *   Employer's Joblist Active Jobpost list.
    */
    public function joblist() {

        //If User has Employer Role.
        if ($this->request->session()->read('Auth.User')['role'] == 'Employer') {

            //Load Jobposts Model.
            $this->loadModel('Jobposts');

            $title = 'Job List - Globalworks';

            $limit = 30;

            if ($this->request->query('page')) {
                $page = (int)$this->request->query('page');
            } else {
                $page = 1;
            }
            
            //Query out to count employer's jobposts.
            $jobposts_count = $this->Jobposts->find()
                ->where(['user_id' => $this->request->session()->read('Auth.User')['id']])
                ->andWhere(['closing_date >=' => date( "Y-m-d H:i:s" )])
                ->count();
            
            //Query out employer;s jobposts.
            $jobposts = $this->Jobposts->find()
                ->where(['user_id' => $this->request->session()->read('Auth.User')['id']])
                ->andWhere(['closing_date >=' => date( "Y-m-d H:i:s" )])
                ->order(['pos' => 'DESC'])
                ->limit($limit)
                ->page($page);
            
            $this->set('title', $title);
            $this->set('jobposts_count', $jobposts_count);
            $this->set('limit', $limit);
            $this->set('page', $page);
            $this->set('jobposts', $jobposts);
            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());
        } else {
            return $this->globalworks->redirect2();
        }
    }
    
    /*
    *   Employer's Joblist Inactive Jobpost list.
    */
    public function joblistInactive() {

        //If User has Employer Role.
        if ($this->request->session()->read('Auth.User')['role'] == 'Employer') {

            //Load Jobposts Model.
            $this->loadModel('Jobposts');

            $title = 'Inactive Job List - Globalworks';

            $limit = 30;

            if ($this->request->query('page')) {
                $page = (int)$this->request->query('page');
            } else {
                $page = 1;
            }
            
            //Query out to count employer's jobposts.
            $jobposts_count = $this->Jobposts->find()
                ->where(['user_id' => $this->request->session()->read('Auth.User')['id']])
                ->andWhere(['closing_date <' => date( "Y-m-d H:i:s" )])
                ->count();
            
             //Query out employer;s jobposts.
            $jobposts = $this->Jobposts->find()
                ->where(['user_id' => $this->request->session()->read('Auth.User')['id']])
                ->andWhere(['closing_date <' => date( "Y-m-d H:i:s" )])
                ->order(['pos' => 'DESC'])
                ->limit($limit)
                ->page($page);
            
            $this->set('title', $title);
            $this->set('jobposts_count', $jobposts_count);
            $this->set('limit', $limit);
            $this->set('page', $page);
            $this->set('jobposts', $jobposts);
            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());
        } else {
            return $this->globalworks->redirect2();
        }
    }

    /*
    *   Employer's Profile Page.
    */
    public function profile() {

        //If User has Employer Role.
        if ($this->request->session()->read('Auth.User')['role'] == 'Employer') {

            $title = 'Profile - Globalworks';

            //Load Profiles Model.
            $this->loadModel('Profiles');
            
            //Count if User has profile record.
            $profile_count = $this->Profiles->find()
                ->where(['user_id' => $this->request->session()->read('Auth.User')['id']])
                ->count();

            //Query out User's Profile.
            $profile = $this->Profiles->find()
                ->where(['user_id' => $this->request->session()->read('Auth.User')['id']]);

            //New Entity.
            $profiles = $this->Profiles->newEntity();

            //If has post request.
            if ($this->request->is('post')) {
                
                //If User has no profile record, create one.
                if ($profile_count == 0) {

                    //Append New Entity.
                    $new_entity = [
                        'nickname' => $this->request->data('nickname'),
                        'business_name' => $this->request->data('business_name'),
                        'business_description' => $this->request->data('business_description'),
                        'business_email' => $this->request->data('business_name'),
                        'business_website' => $this->request->data('business_website'),
                        'business_contact_number' => $this->request->data('business_contact_number'),
                        'business_address' => $this->request->data('business_address'),
                        'business_state' => $this->request->data('business_region'),
                        'business_country' => $this->request->data('business_country'),
                    ];

                    //Patch Entity.
                    $profiles = $this->Profiles->patchEntity($profiles, $new_entity);

                    //Create Record.
                    if ($this->Profiles->save($profiles)) {

                        /*
                        *   Create Log Record.
                        */

                        //User's Fullname.
                        $fullname = $this->request->session()->read('Auth.User')['firstname'] . ' ' . $this->request->session()->read('Auth.User')['lastname'];

                        //User's Email Address.
                        $email = $this->request->session()->read('Auth.User')['email'];

                        //Create Log Message.
                        $log_msg = $fullname . ' (' . $email . ') create his/her profile.';

                        $this->Flash->success(__('Your Profile has been saved.'));
                    }

                //Update Existing Profile Record.
                } else {
                    
                    //Value to append.
                    $id = '';

                    //Query out to get User's exsisting profile record.
                    $s_profile = $this->Profiles->find()
                        ->select(['id'])
                        ->where(['user_id' => $this->request->session()->read('Auth.User')['id']]);

                    //Loop Result.
                    foreach ($s_profile as $pro) {
                        
                        //Append Value.
                        $id .= $pro['id'];
                    }

                    //Get User's Record Column.
                    $the_profile = $this->Profiles->get($id);

                    //Append New Records.
                    $the_profile->nickname = $this->request->data('nickname');
                    $the_profile->business_name = $this->request->data('business_name');
                    $the_profile->business_description = $this->request->data('business_description');
                    $the_profile->business_email = $this->request->data('business_email');
                    $the_profile->business_website = $this->request->data('business_website');
                    $the_profile->business_contact_number = $this->request->data('business_contact_number');
                    $the_profile->business_address = $this->request->data('business_address');
                    $the_profile->business_state = $this->request->data('business_region');
                    $the_profile->business_country = $this->request->data('business_country');

                    //Save Record.
                    if ($this->Profiles->save($the_profile)) {

                        /*
                        *   Create Log Record.
                        */

                        //User's Fullname.
                        $fullname = $this->request->session()->read('Auth.User')['firstname'] . ' ' . $this->request->session()->read('Auth.User')['lastname'];

                        //User's Email Address.
                        $email = $this->request->session()->read('Auth.User')['email'];

                        //Create Log Message.
                        $log_msg = $fullname . ' (' . $email . ') update his/her profile.';

                        //Save Log.
                        $this->globalworks->_log('update_account_profile_log', $log_msg);

                        $this->Flash->success(__('Your Profile has been saved.'));
                    }
                }
            }

            $this->set('title', $title);
            $this->set('profile_count', $profile_count);
            $this->set('profile', $profile);
            $this->set('profiles', $profiles);
            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());
        } else {
            return $this->globalworks->redirect2();
        }
    }

    /*
    *   User's Account Page.
    */
    public function account() {
        
        //If User has Employer Role.
        if ($this->request->session()->read('Auth.User')['role'] == 'Employer') {

            $title = 'Your Account - Globalworks';

            //Load Users Model.
            $this->loadModel('Home');

            //Query out User's Information.
            $users = $this->Home->find()
                ->select(['firstname', 'lastname', 'birthday', 'address', 'region', 'country', 'contact_number'])
                ->where(['id' => $this->request->session()->read('Auth.User')['id']])
                ->andWhere(['role' => 'Employer']);

            //Render Results to the view.
            foreach ($users as $user) {
                $this->set('firstname', $user['firstname']);
                $this->set('lastname', $user['lastname']);
                $this->set('birthday', $user['birthday']);
                $this->set('address', $user['address']);
                $this->set('region', $user['region']);
                $this->set('country', $user['country']);
                $this->set('contact_number', $user['contact_number']);
            }

            //New Entity.
            $user2 = $this->Home->newEntity(['validate' => 'updateinfo']);

            //If Has Post or PUT Request.
            if ($this->request->is('post') || $this->request->is('put')) {
                
                //Get User's Column.
                $user3 = $this->Home->get($this->request->session()->read('Auth.User')['id']);

                //Append Changes.
                $user3->firstname = $this->request->data('firstname');
                $user3->lastname = $this->request->data('lastname');
                $user3->birthday = $this->request->data('month') . '/' . $this->request->data('day') . '/' . $this->request->data('year');
                $user3->address = $this->request->data('address');
                $user3->region = $this->request->data('region');
                $user3->country = $this->request->data('country');
                $user3->contact_number = $this->request->data('contact_number');

                //Patch Entity.
                $user2 = $this->Home->patchEntity($user3, $this->request->data, ['validate' => 'updateinfo']);

                //Save Updates.
                if ($this->Home->save($user2)) {
                    
                    /*
                    *   Create Log Record.
                    */

                    //User's Fullname.
                    $fullname = $this->request->session()->read('Auth.User')['firstname'] . ' ' . $this->request->session()->read('Auth.User')['lastname'];

                    //User's Email Address.
                    $email = $this->request->session()->read('Auth.User')['email'];

                    //Create Log Message.
                    $log_msg = $fullname . ' (' . $email . ') update his/her account details.';

                    //Save Log.
                    $this->globalworks->_log('update_account_details_log', $log_msg);

                    $this->Flash->success('Your Account Information saved!');
                }

                //If Validation Error. Apply Last Form field inputs.
                $this->set('firstname', $this->request->data('firstname'));
                $this->set('lastname', $this->request->data('lastname'));
                $this->set('birthday', $this->request->data('month') . '/' . $this->request->data('day') . '/' . $this->request->data('year'));
                $this->set('address', $this->request->data('address'));
                $this->set('region', $this->request->data('region'));
                $this->set('country', $this->request->data('country'));
                $this->set('contact_number', $this->request->data('contact_number'));
            }
            
            $this->set('title', $title);
            $this->set('user2', $user2);
            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());
        } else {
            return $this->globalworks->redirect2();
        }
    }

    /*
    *   Password Change Page.
    */
    public function changepassword() {

        //If User has Employer Role.
        if ($this->request->session()->read('Auth.User')['role'] == 'Employer') {

            if ($this->request->session()->read('Auth.User')['fb_user'] || ($this->request->session()->read('Auth.User')['fb_user'] && $this->request->session()->read('Auth.User')['password'] == '')) {
                return $this->redirect('/employers/dashboard');
            }

            $title = 'Change Password - Globalworks';

            //Load Home Model.
            $this->loadModel('Home');
            
            //Query out to fetch User's Info.
            $user = $this->Home->get($this->request->session()->read('Auth.User')['id']);

            //If request has Post or PUT.
            if ($this->request->is('post') || $this->request->is('put')) {
                $user = $this->Home->patchEntity($user, [
                    'current_password' => $this->request->data('current_password'),
                    'password' => $this->request->data('new_password'),
                    'new_password' => $this->request->data('new_password'),
                    'rpassword' => $this->request->data('rpassword'),
                ], ['validate' => 'password']);

                if ($this->Home->save($user)) {

                    /*
                    *   Create Log Record.
                    */

                    //User's Fullname.
                    $fullname = $this->request->session()->read('Auth.User')['firstname'] . ' ' . $this->request->session()->read('Auth.User')['lastname'];

                    //User's Email Address.
                    $email = $this->request->session()->read('Auth.User')['email'];

                    //Create Log Message.
                    $log_msg = $fullname . ' (' . $email . ') change his/her password.';

                    //Save Log.
                    $this->globalworks->_log('password_change_log', $log_msg);

                    $this->Flash->success(__('Password has been changed!'));
                }
            }

            $this->set('title', $title);
            $this->set('user', $user);
            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());
        } else {
            return $this->globalworks->redirect2();
        }
    }
}
