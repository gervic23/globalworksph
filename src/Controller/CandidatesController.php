<?php
namespace App\Controller;

use Cake\Event\Event;

class CandidatesController extends AppController {
    private $globalworks;
    
    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        
        $this->globalworks = new GlobalworksController();

        $this->Auth->allow(['download']);

        //Check Visitor's IP Info.
        $this->globalworks->ip_info_init();

        //Check User's Account If Exists.
        $this->globalworks->check_auth();

        //Check if FB User has not yet complete filling his basic information.
        if (!$this->globalworks->fb_user_check()) {
            return $this->redirect('/basicinfo');
        }

        //Check User has not confirm his account yet.
        if ($this->globalworks->check_confirmed_user()) {
            return $this->redirect('/account_confirmation');
        }

        //Announcement.
        $this->set('pop_announcement', $this->globalworks->show_announcement());

        //Check if user is suspended. Auto logout user.
        return $this->globalworks->is_suspended();
    }
    
    public function index() {
        //Get User Role.
        $role = $this->request->session()->read('Auth.User')['role'];
        
        if ($role == 'Applicant') {

            $title = 'Dashboard - Globalworks';
            
            $this->set('title', $title);
            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());
        } else {
            return $this->globalworks->redirect2();
        }
    }
    
    /*
    *   Job Search Page.
    */
    public function search() {
        
        //If User has Applicant role.
        if ($this->request->session()->read('Auth.User')['role'] == 'Applicant') {

            $title = 'Search Jobs - Globalworks';
            
            //If has GET request.
            if ($this->request->query) {
                
                //Load Jobposts Model.
                $this->loadModel('Jobposts');

                $limit = 30;

                if ($this->request->query('page')) {
                    $page = (int)$this->request->query('page');
                } else {
                    $page = 1;
                }
                
                //Keywords.
                $keywords = $this->request->query('keywords');

                $title = $keywords . ' - Globalworks';
                
                //Job class.
                $job_class = $this->request->query('job-class');
                
                //Localization.
                $localization = $this->request->query('localization');
                
                //Categories.
                $category = $this->request->query('category');
                
                //Country.
                $country = $this->request->query('country');

                //Query to Count results.
                $jobposts_count = $this->Jobposts->find();
                $jobposts_count->where(['job_title LIKE' => '%' . $keywords . '%']);
                $jobposts_count->orWhere(['job_description LIKE' => '%' . $keywords . '%']);
                $jobposts_count->orWhere(['job_responsibilities LIKE' => '%' . $keywords . '%']);
                $jobposts_count->orWhere(['job_qualifications LIKE' => '%' . $keywords . '%']);
                
                if ($category !== 'All') {
                    $jobposts_count->andWhere(['job_category' => $category]);
                }
                
                if ($job_class !== 'All') {
                    $jobposts_count->andWhere(['job_type' => $job_class]);
                }
                
                if ($localization !== 'All') {
                    $jobposts_count->andWhere(['job_migration' => $localization]);
                }
                
                if ($country !== 'All') {
                    $jobposts_count->andWhere(['country' => $country]);
                }
                
                //Query out to get search results.
                $jobposts = $this->Jobposts->find();
                $jobposts->select(['id', 'company_logo', 'job_title', 'company_name', 'job_description', 'created_at']);
                $jobposts->where(['job_title LIKE' => '%' . $keywords . '%']);
                $jobposts->orWhere(['job_description LIKE' => '%' . $keywords . '%']);
                $jobposts->orWhere(['job_responsibilities LIKE' => '%' . $keywords . '%']);
                $jobposts->orWhere(['job_qualifications LIKE' => '%' . $keywords . '%']);
                
                if ($category !== 'All') {
                    $jobposts->andWhere(['job_category' => $category]);
                }
                
                if ($job_class !== 'All') {
                    $jobposts->andWhere(['job_type' => $job_class]);
                }
                
                if ($localization !== 'All') {
                    $jobposts->andWhere(['job_migration' => $localization]);
                }
                
                if ($country !== 'All') {
                    $jobposts->andWhere(['country' => $country]);
                }
                
                $jobposts->andWhere(['closing_date >=' => date( "Y-m-d H:i:s" )]);
                $jobposts->limit($limit);
                $jobposts->page($page);
                $jobposts->order(['pos' => 'DESC']);
                
                //Count Results.
                $i = 0;
                
                //Loop to Count Results.
                foreach ($jobposts_count as $post) {
                    $i++;
                }
                
                //Set Custom Template.
                $this->viewBuilder()->template('search_results');
                
                $this->set('count', $i);
                $this->set('limit', $limit);
                $this->set('page', $page);
                $this->set('jobposts', $jobposts);
            }
            
            $this->set('title', $title);
            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());
        } else {
            return $this->globalworks->redirect2();
        }
    }

    /*
    *   Messages Page.
    */
    public function messages() {

        //If User has Applicant role.
        if ($this->request->session()->read('Auth.User')['role'] == 'Applicant') {
            $title = 'Messages - Globalworks';

            $this->set('title', $title);
            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());
        } else {
            return $this->globalworks->redirect2();
        }
    }
    
    /*
    *   Profile Page.
    */
    public function profile() {

        //If User has Applicant role.
        if ($this->request->session()->read('Auth.User')['role'] == 'Applicant') {

            $title = 'Profile - Globalworks';
            
            //Load Profiles Model.
            $this->loadModel('Profiles');
            
            //Load Resumes Model.
            $this->loadModel('Resumes');
            
            //Count if User's Profile is exists.
            $profile_count = $this->Profiles->find()
                ->where(['user_id' => $this->request->session()->read('Auth.User')['id']])
                ->count();
            
            //Count if User has Resumes record.
            $resume_count = $this->Resumes->find()
                ->where(['user_id' => $this->request->session()->read('Auth.User')['id']])
                ->count();
            
            //If User's Profile exists. Render User's Profile to the view.
            if ($profile_count > 0) {
                
                //Query out to get User's Profile.
                $profiles = $this->Profiles->find()
                    ->select(['profile_picture', 'nickname', 'job_title', 'skills', 'desire_salary', 'education', 'experience', 'employment_status', 'skype_id', 'job_subscribe'])
                    ->where(['user_id' => $this->request->session()->read('Auth.User')['id']]);
                
                //Loop Results and render results to the view.
                foreach ($profiles as $profile) {
                    $this->set('profile_picture', $profile['profile_picture']);
                    $this->set('nickname', $profile['nickname']);
                    $this->set('job_title', $profile['job_title']);
                    $this->set('skills', $profile['skills']);
                    $this->set('desire_salary', $profile['desire_salary']);
                    $this->set('education', $profile['education']);
                    $this->set('experience', $profile['experience']);
                    $this->set('employment_status', $profile['employment_status']);
                    $this->set('skype_id', $profile['skype_id']);
                    $this->set('job_subscribe', $profile['job_subscribe']);
                }
            } else {
                $this->set('profile_picture', '');
            }
            
            //If User has Resumes record, render details to the view.
            if ($resume_count > 0) {
                $resumes = $this->Resumes->find()
                    ->select(['id', 'filename', 'date', 'orig_filename'])
                    ->where(['user_id' => $this->request->session()->read('Auth.User')['id']]);
                
                //Loop result to append record.
                foreach ($resumes as $res) {
                    $this->set('resume_id', $res['id']);
                    $this->set('resume_filename', $res['orig_filename']);
                    $this->set('resume_date', $res['date']);
                }
            }
            
            //If Has Post Request.
            if ($this->request->is('post')) {
                
                //If User has no Profile record create 1.
                if ($profile_count == 0) {
                    
                    //New Entity.
                    $profiles_updates = $this->Profiles->newEntity();

                    //To be append.
                    $skills = '';

                    //If Skills is set.
                    if ($this->request->data('skills')) {
                        $skills = json_encode($this->request->data('skills'));
                    }
                    
                    //Append Entity.
                    $new_entity = [
                        'user_id' => $this->request->session()->read('Auth.User')['id'],
                        'nickname' => $this->request->data('nickname'),
                        'job_title' => $this->request->data('job_title'),
                        'skype_id' => $this->request->data('skype_id'),
                        'experience' => $this->request->data('s_tags'),
                        'employment_status' => $this->request->data('employment_status'),
                        'skills' => $skills,
                        'desire_salary' => $this->request->data('desire_salary'),
                        'education' => $this->request->data('education'),
                        'job_subscribe' => $this->request->data('job_subscribe'),
                    ];

                    //Patch Entity.
                    $profiles_updates = $this->Profiles->patchEntity($profiles_updates, $new_entity);

                    //Save Profile Record.
                    if ($this->Profiles->save($profiles_updates)) {

                        /*
                        *   Create Log Record.
                        */

                        //User's Fullname.
                        $fullname = $this->request->session()->read('Auth.User')['firstname'] . ' ' . $this->request->session()->read('Auth.User')['lastname'];

                        //User's Email Address.
                        $email = $this->request->session()->read('Auth.User')['email'];

                        //Create Log Message.
                        $log_msg = $fullname . ' (' . $email . ') create his/her profile.';

                        //Save Log.
                        $this->globalworks->_log('create_account_profile_log', $log_msg);

                        $this->Flash->success(__('Profile Saved!'));
                    }

                //If User has Profile Record.
                } else {
                
                    //Query out to find User's ID.
                    $resumes = $this->Profiles->find()
                        ->select(['id'])
                        ->where(['user_id' => $this->request->session()->read('Auth.User')['id']]);

                    //To be append.
                    $id = '';

                    //Loop Results to get ID.
                    foreach ($resumes as $res) {
                        $id .= $res['id'];
                    }

                    //To be append.
                    $skills = '';

                    //If Skills is set.
                    if ($this->request->data('skills')) {
                        $skills = json_encode($this->request->data('skills'));
                    }

                    //Get User's Column.
                    $resume = $this->Profiles->get($id);

                    //Append Changes.
                    $resume->nickname = $this->request->data('nickname');
                    $resume->job_title = $this->request->data('job_title');
                    $resume->skype_id = $this->request->data('skype_id');
                    $resume->experience = $this->request->data('s_tags');
                    $resume->employment_status = $this->request->data('employment_status');
                    $resume->skills = $skills;
                    $resume->desire_salary = $this->request->data('desire_salary');
                    $resume->education = $this->request->data('education');
                    $resume->job_subscribe = $this->request->data('job_subscribe');

                    //Save Profile Record.
                    if ($this->Profiles->save($resume)) {

                        /*
                        *   Create Log Record.
                        */

                        //User's Fullname.
                        $fullname = $this->request->session()->read('Auth.User')['firstname'] . ' ' . $this->request->session()->read('Auth.User')['lastname'];

                        //User's Email Address.
                        $email = $this->request->session()->read('Auth.User')['email'];

                        //Create Log Message.
                        $log_msg = $fullname . ' (' . $email . ') update his/her profile.';

                        //Save Log.
                        $this->globalworks->_log('update_account_profile_log', $log_msg);

                        $this->Flash->success(__('Profile Saved!'));
                    }
                }
            }
            
            $this->set('title', $title);
            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());
        } else {
            return $this->globalworks->redirect2();
        }
    }
    
    /*
    *   Account Page.
    */
    public function account() {
        
        //If User has Applicant role.
        if ($this->request->session()->read('Auth.User')['role'] == 'Applicant') {

            $title = 'My Account - Globalworks';
            
            //Load Home Model.
            $this->loadModel('Home');
            
            //Query out to get User's Details.
            $users = $this->Home->find()
                ->select(['firstname', 'lastname', 'birthday', 'address', 'region', 'country', 'contact_number'])
                ->where(['id' => $this->request->session()->read('Auth.User')['id']]);
            
            //Loop Results and render to view.
            foreach ($users as $u) {
                $this->set('firstname', $u['firstname']);
                $this->set('lastname', $u['lastname']);
                $this->set('birthday', $u['birthday']);
                $this->set('address', $u['address']);
                $this->set('region', $u['region']);
                $this->set('country', $u['country']);
                $this->set('contact_number', $u['contact_number']);
            }
            
            //New Entity.
            $user2 = $this->Home->newEntity(['validate' => 'updateinfo']);
            
            //If has Post or Put Request.
            if ($this->request->is('post') || $this->request->is('put')) {
                
                //Get User's Column.
                $user3 = $this->Home->get($this->request->session()->read('Auth.User')['id']);
                
                //Append Changes.
                $user3->firstname = $this->request->data('firstname');
                $user3->lastname = $this->request->data('lastname');
                $user3->birthday = $this->request->data('month') . '/' . $this->request->data('day') . '/' . $this->request->data('year');
                $user3->address = $this->request->data('address');
                $user3->region = $this->request->data('region');
                $user3->country = $this->request->data('country');
                $user3->contact_number = $this->request->data('contact_number');
                
                $user2 = $this->Home->patchEntity($user3, $this->request->data, ['validate' => 'updateinfo']);
                
                if ($this->Home->save($user2)) {

                    /*
                    *   Create Log Record.
                    */

                    //User's Fullname.
                    $fullname = $this->request->session()->read('Auth.User')['firstname'] . ' ' . $this->request->session()->read('Auth.User')['lastname'];

                    //User's Email Address.
                    $email = $this->request->session()->read('Auth.User')['email'];

                    //Create Log Message.
                    $log_msg = $fullname . ' (' . $email . ') update his/her account details.';

                    //Save Log.
                    $this->globalworks->_log('update_account_details_log', $log_msg);

                    $this->Flash->success(__('Your Account Information saved!'));
                }
                
                //If Validation error. Applu Last Form field inputs.
                $this->set('firstname', $this->request->data('firstname'));
                $this->set('lastname', $this->request->data('lastname'));
                $this->set('birthday', $this->request->data('month') . '/' . $this->request->data('day') . '/' . $this->request->data('year'));
                $this->set('address', $this->request->data('address'));
                $this->set('region', $this->request->data('region'));
                $this->set('country', $this->request->data('country'));
                $this->set('contact_number', $this->request->data('contact_number'));
            }
            
            $this->set('title', $title);
            $this->set('user2', $user2);
            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());
        } else {
            return $this->globalworks->redirect2();
        }
    }
    
    /*
    *   Change Password Page.
    */
    public function changepassword() {
        
        //If User has Applicant role.
        if ($this->request->session()->read('Auth.User')['role'] == 'Applicant') {
            
            $title = 'Change Password - Globalworks';

            //Load Home Model.
            $this->loadModel('Home');
            
            //Get User's Column.
            $user = $this->Home->get($this->request->session()->read('Auth.User')['id']);
            
            //If has POST request or PUT.
            if ($this->request->is('post') || $this->request->is('put')) {
                
                //Patch Entity and apply changes.
                $user = $this->Home->patchEntity($user, [
                    'current_password' => $this->request->data('current_password'),
                    'password' => $this->request->data('new_password'),
                    'new_password' => $this->request->data('new_password'),
                    'rpassword' => $this->request->data('rpassword'),
                ], ['validate' => 'password']);
                
                //Saving Changes.
                if ($this->Home->save($user)) {

                    /*
                    *   Create Log Record.
                    */

                    //User's Fullname.
                    $fullname = $this->request->session()->read('Auth.User')['firstname'] . ' ' . $this->request->session()->read('Auth.User')['lastname'];

                    //User's Email Address.
                    $email = $this->request->session()->read('Auth.User')['email'];

                    //Create Log Message.
                    $log_msg = $fullname . ' (' . $email . ') change his/her password.';

                    //Save Log.
                    $this->globalworks->_log('password_change_log', $log_msg);

                    $this->Flash->success(__('Password has been changed!'));
                }
            }
            
            $this->set('title', $title);
            $this->set('user', $user);
            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());
        } else {
            return $this->globalworks->redirect2();
        }
    }
    
    /*
    *   Secure Download action for resumes.
    */
    public function download($id) {
        
        //Disable View.
        $this->autoRender = false;
        $this->viewBuilder()->setLayout(false);
        
        //Load Resumes Model.
        $this->loadModel('Resumes');
        
        //Count to check if the downloader is an applicant and resume is belongs to the user.
        $candidate_count = $this->Resumes->find()
            ->where(['id' => $id])
            ->andWhere(['user_id' => $this->request->session()->read('Auth.User')['id']])
            ->count();
        
        //If Downloader is not the owner of the resume or User is not an Employer. Redirect User.
        if (($candidate_count == 0 && $this->request->session()->read('Auth.User')['role'] == 'Applicant') || ($candidate_count == 0 && $this->request->session()->read('Auth.User')['role'] !== 'Employer' && $this->request->session()->read('Auth.User')['role'] !== 'Admin' && $this->request->session()->read('Auth.User')['role'] !== 'Super Admin')) {
            return $this->globalworks->redirect2();
        } else {
            //If Download ID is not set. redirect user.
            if (!$id) {
                return $this->globalworks->redirect2();
            }

            //Count if ID's Resume Record is exists.
            $resume_count = $this->Resumes->find()
                ->where(['id' => $id])
                ->count();

            //If Resume Record is not exists. redirect user.
            if ($resume_count == 0) {
                return $this->globalworks->redirect2();
            } else {

                //Query out to get Resume's Details.
                $resumes = $this->Resumes->find()
                    ->select(['filename', 'orig_filename'])
                    ->where(['id' => $id]);
                
                //To be append.
                $filename = '';
                
                //To be append.
                $orig_filename = '';
                
                //Loop Results to get selected columns.
                foreach ($resumes as $res) {
                    
                    //Append Filename.
                    $filename .= $res['filename'];
                    
                    //Append Orig Filename.
                    $orig_filename .= $res['orig_filename'];
                }
                
                //Path to resume's directory.
                $path = './resumes/';
                
                //Get Filename ext.
                $ext = explode('.', $filename);
                $ext = end($ext);
                
                //Specify File Type Base on file extension.
                switch ($ext) {
                    case 'doc':
                        $type = 'Content-type: application/msword';
                    break;
                        
                    case 'docx':
                        $type = 'Content-type: application/vnd.openxmlformats-officedocument.wordprocessingml.document';
                    break;
                }
                
                //Create Secure Downloader.
                header($_SERVER['SERVER_PROTOCOL'] . " 200 OK");
                header("Cache-Control: public");
                header($type);
                header("Content-Transfer-Encoding: Binary");
                header("Content-Length:" . filesize($path . $filename));
                header("Content-Disposition: attachment; filename=$orig_filename");
                readfile($path . $filename);
            }
        }
    }
}
