<?php
namespace App\Controller;

use Cake\Event\Event;

/*
*   Sitemap Controller for search engines.
*/
class SitemapsController extends AppController {

    private $globalworks;

    private $url;
    
    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);

        $this->globalworks = new GlobalworksController();

        $this->url = $this->globalworks->_url;

        $this->Auth->allow(['pages', 'jobs', 'rssjobs']);
    }
    
    public function pages() {
        $this->viewBuilder()->setLayout(false);
        $this->viewBuilder()->template('pages');
        $this->set('url', $this->url);
    }
    
    public function jobs() {
        $this->viewBuilder()->setLayout(false);
        $this->viewBuilder()->template('jobs');
        $this->loadModel('Jobposts');
        
        $jobposts = $this->Jobposts->find()
            ->select(['id'])
            ->where(['closing_date >='  => date( "Y-m-d H:i:s" )])
            ->order(['pos' => 'DESC']);
        
        $this->set('jobposts', $jobposts);
        $this->set('url', $this->url);
    }
    
    public function rssjobs() {
        $this->viewBuilder()->setLayout(false);
        $this->viewBuilder()->template('rssjobs');
        $this->loadModel('Jobposts');
        
        $jobposts = $this->Jobposts->find()
            ->where(['closing_date >='  => date( "Y-m-d H:i:s" )])
            ->order(['pos' => 'DESC']);
        
        $this->set('jobposts', $jobposts);
        $this->set('url', $this->url);
    }
}
