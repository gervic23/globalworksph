<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Routing\Router;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $helpers = [
        'AkkaCKEditor.CKEditor'
    ];

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'loginAction' => [
                'controller' => 'Home',
                'action' => 'login'
            ],
            'logoutRedirect' => [
                'controller' => 'Home',
                'action' => 'login'
            ],
            'loginRedirect' => ('/candidates/dashboard'),
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'email']
                ]
            ]
        ]);
        $this->loadComponent('Facebook', [
            'app_id' => '139287030070755',
            'app_secret' => '643af9d9df96d3093b0bb213eec8d7a5',
            'default_graph_version' => 'v2.2',
        ]);
        
        //Recaptcha Component.
        $this->loadComponent('Recaptcha.Recaptcha', [
            'enable' => true,     // true/false
            'sitekey' => '6Lfd3TAUAAAAAAXAU8nxOcqDNRiKv1mQJxzZhBCB', //if you don't have, get one: https://www.google.com/recaptcha/intro/index.html
            'secret' => '6Lfd3TAUAAAAAMSGPtiBCB94HqXivzj7XC7N6ytK',
            'type' => 'image',  // image/audio
            'theme' => 'light', // light/dark
            'lang' => 'english',      // default en
            'size' => 'normal'  // normal/compact
        ]);

        /*
         * Enable the following components for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security', ['blackHoleCallback' => 'forceSSL']);
        $this->loadComponent('Csrf');
    }

    // public function beforeFilter(Event $event) {
    //     if (!$_SERVER['REQUEST_URI'] !== '/crons') {
    //         $this->Security->requireSecure();
    //     }
    // }

    // public function forceSSL() {
    //     return $this->redirect('https://' . env('SERVER_NAME') . Router::url($this->request->getRequestTarget()));
    // }
}
