<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/*
*   This Controller is exclusive only for ajax operation.
*/
class AjaxController extends AppController {
    
    private $globalworks;
    
    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        
        $this->globalworks = new GlobalworksController();
        
        $this->Auth->allow(['init']);
    }
    
    public function init() {
        
        //Disable layouts and templates render.
        $this->autoRender = false;
        $this->viewBuilder()->setLayout(false);
        
        //If request is post and ajax.
        if ($this->request->is('post') && $this->request->isAjax()) {
            
            //The Operation of Ajax.
            $operation = $this->request->data('operation');
            
            switch ($operation) {
                    
                /*
                *   Operation for image upload.
                *   Main Route: "/uploadresume/*"
                * 
                *   Associated Controller and Action.
                *   Controller => Home, Action => uploadpicture.
                *   Controller => Employersdashboard, Action => profile.
                */
                case 'image_upload':
                    
                    //Load Model.
                    $this->loadModel('Imgtmp');
                    
                    //User's Session ID.
                    $user_id = $this->request->session()->read('Auth.User')['id'];
                    
                    //Uploaded File properties.
                    $name = $_FILES['ddu-upload']['name'];
                    $error = $_FILES['ddu-upload']['error'];
                    $tmp = $_FILES['ddu-upload']['tmp_name'];
                    
                    //Path to upload directory.
                    $path = './img/users/tmp/';
                    
                    //If no upload error.
                    if ($error == 0) {
                        
                        //Get the file extension name.
                        $ext = explode('.', $name);
                        $ext = end($ext);
                        
                        //Random name value.
                        $random_name = '';
                        
                        //Characters to generate.
                        $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                        
                        //Generate random strings.
                        for ($p = 0; $p < 16; $p++) {
                            $random_name .= $chars[mt_rand(0, strlen($chars)-1)];
                        }
                        
                        //Random name for filename.
                        $random_name = $user_id . date( "YmdHis" ) . $random_name;
                        
                        //Count user's img_tmp record if exists or not.
                        $imgtmp_count = $this->Imgtmp->find()
                            ->where(['user_id' => $user_id])
                            ->count();
                        
                        //If user's img_tmp record not exists, create record.
                        if ($imgtmp_count == 0) {
                            
                            //Move Uploaded file to /img/users/tmp directory.
                            if (move_uploaded_file($tmp, $path . $random_name . '.' . $ext)) {
                                
                                //New Entity.
                                $imgtmp = $this->Imgtmp->newEntity();
                                
                                //Records to be insert in img_tmp database table.
                                $new_entity = [
                                    'user_id' => $user_id,
                                    'filename' => $random_name . '.' . $ext,
                                    'date' => date( "Y-m-d H:i:s" ),
                                ];
                                
                                //Patch Entity.
                                $imgtmp = $this->Imgtmp->patchEntity($imgtmp, $new_entity);
                                
                                //Create or Insert new Record.
                                if ($this->Imgtmp->save($imgtmp)) {
                                    echo 'ok';
                                }
                            }
                            
                        //If User img_tmp record exists. Update record.
                        } else {
                            
                            //Value of img_tmp id to append.
                            $id = '';
                            
                            //Value of image filename to append.
                            $old_img = '';
                            
                            //Get User's img_tmp record.
                            $imgtmps = $this->Imgtmp->find()
                                ->where(['user_id' => $user_id]);
                            
                            //Loop User's Record.
                            foreach ($imgtmps as $t) {
                                
                                //Append ID.
                                $id .= $t['id'];
                                
                                //Append Filename.
                                $old_img .= $t['filename'];
                            }
                            
                            //File destination directory and filename.
                            $file = $path . $old_img;
                            
                            //Get User's row record from img_tmp table.
                            $imgtmp = $this->Imgtmp->get($id);
                            
                            //Update filename record.
                            $imgtmp->filename = $random_name . '.' . $ext;
                            
                            //Update date record.
                            $imgtmp->date = date( "Y-m-d H:i:s" );
                            
                            //Save Changes.
                            if ($this->Imgtmp->save($imgtmp)) {
                                
                                /*
                                *   Delete old uploaded image and move new uploaded image.
                                */
                                
                                //If old image file exists.
                                if (file_exists($file)) {
                                    
                                    //Delete old file image.
                                    if (unlink($file)) {
                                        
                                        //Move the new one to the tmp directory.
                                        if (move_uploaded_file($tmp, $path . $random_name . '.' . $ext)) {
                                            echo 'ok';
                                        }
                                    }
                                }
                            }
                        }
                    }
                break;
                
                /*
                *   Operation for cropping profile image after upload.
                *   Main Route: "/imagecrop/*"
                *   
                *   Associated Controller and Action.
                *   Controller => Home, Action => uploadpicture.
                *   Controller => Employersdashboard, Action => profile.
                */
                case 'crop_image':
                    
                    //User's Session ID from post request.
                    $user_id = $this->request->data('user_id');
                    
                    //Original filename of image from tmp folder.
                    $filename_orig = $this->request->data('filename');
                    
                    //File format type.
                    $type = $_FILES['cropperImage']['type'];
                    
                    //File upload error.
                    $error = $_FILES['cropperImage']['error'];
                    
                    //The Image File.
                    $tmp = $_FILES['cropperImage']['tmp_name'];
                    
                    //Path to /img/profile_picrutes/ directory.
                    $profile_picture_path = './img/users/profile_pictures/';
                    
                    //Path to /img/profile_thumbnails/ directory.
                    $thumbnail_picture_path = './img/users/profile_thumbnails/';
                    
                    //Get the filename w/o dot extension.
                    $filename = explode('.', $filename_orig);
                    $f_name = $filename[0];
                    
                    //File image cropped extension name.
                    $ext = '.jpg';
                    
                    //The new filename of image cropped.
                    $new_filename = $f_name . $ext;
                    
                    //If mo upload error.
                    if ($error == 0) {
                        
                        /*
                        *   Resizing image.
                        */
                        
                        //Type of image.
                        switch ($type) {
                                
                            //If image is jpeg format.
                            case 'image/jpeg':
                                
                                //Get the image dimensions.
                                list($width, $height) = getimagesize($tmp);
                                
                                /*
                                *   For Dimension 370x370.
                                */
                                //Set a new dimension.
                                $image = imagecreatetruecolor(370, 370);
                                
                                //Get the source file.
                                $image_src = imagecreatefromjpeg($tmp);
                                
                                //Initiating resizing image.
                                imagecopyresampled($image, $image_src, 0, 0, 0, 0, 370, 370, $width, $height);
                                
                                //Convert to jpeg and move to destination path.
                                imagejpeg($image, $profile_picture_path . $new_filename);
                                
                                /*
                                *   For Dimension 70x70.
                                */
                                //Set a new dimension.
                                $thumb_image = imagecreatetruecolor(70, 70);
                                
                                //Get the source file.
                                $thumb_image_src = imagecreatefromjpeg($tmp);
                                
                                //Initiating resizing image.
                                imagecopyresampled($thumb_image, $thumb_image_src, 0, 0, 0, 0, 70, 70, $width, $height);
                                
                                //Convert to jpeg and move to destination path.
                                imagejpeg($thumb_image, $thumbnail_picture_path . $new_filename);
                            break;
                            
                            //If image is jpeg format.
                            case 'image/png':
                                
                                //Get the image dimensions.
                                list($width, $height) = getimagesize($tmp);
                                
                                /*
                                *   For Dimension 370x370.
                                */
                                //Set a new dimension.
                                $image = imagecreatetruecolor(370, 370);
                                
                                //Get the source file.
                                $image_src = imagecreatefrompng($tmp);
                                
                                //Initiating resizing image.
                                imagecopyresampled($image, $image_src, 0, 0, 0, 0, 370, 370, $width, $height);
                                
                                //Convert to jpeg and move to destination path.
                                imagejpeg($image, $profile_picture_path . $new_filename);
                                
                                /*
                                *   For Dimension 70x70.
                                */
                                //Set a new dimension.
                                $thumb_image = imagecreatetruecolor(70, 70);
                                
                                //Get the source file.
                                $thumb_image_src = imagecreatefrompng($tmp);
                                
                                //Initiating resizing image.
                                imagecopyresampled($thumb_image, $thumb_image_src, 0, 0, 0, 0, 70, 70, $width, $height);
                                
                                //Convert to jpeg and move to destination path.
                                imagejpeg($thumb_image, $thumbnail_picture_path . $new_filename);
                            break;
                        }
                        
                        /*
                        *   Checking if User's has a Profile record.
                        *   If User has no profile record create one.
                        *   If User has already a profile record, set record to profile_picture column.
                        *   If User Profile profile_picture is already set, update record and remove old images and replace with the new cropped images.
                        */
                        
                        //Load Model.
                        $this->loadModel('Profiles');
                        
                        //Count User's Profile record if exists or not.
                        $profiles_count = $this->Profiles->find()
                            ->where(['user_id' => $user_id])
                            ->count();
                        
                        //If User's Profile record does not exists, create record.
                        if ($profiles_count == 0) {
                            
                            //New Entity.
                            $profiles = $this->Profiles->newEntity();
                            
                            //Records to be insert in Profiles database table.
                            $new_entity = [
                                'user_id' => $user_id,
                                'profile_picture' => $new_filename,
                            ];
                            
                            //Patch Entity.
                            $profiles = $this->Profiles->patchEntity($profiles, $new_entity);
                            
                            //Create or Insert Record to the Profiles Table.
                            if ($this->Profiles->save($profiles)) {
                                
                                //Delete User's img_tmp record and temporary image from tmp directory.
                                if ($this->globalworks->deletetmpimg($user_id, $filename_orig)) {
                                    echo 'ok';
                                }
                            }
                            
                        //If User's Profile Record is Exists, Update Record.
                        } else {
                            
                            //Get User's Profile record.
                            $profiles = $this->Profiles->find()
                                ->where(['user_id' => $user_id]);
                            
                            //User's Profile ID row to append.
                            $id = '';
                            
                            //User's profile_picture value to append.
                            $profile_picture = '';
                            
                            //Loop User's Profile results.
                            foreach ($profiles as $pro) {
                                
                                //Append ID.
                                $id .= $pro['id'];
                                
                                //Append profile_picture.
                                $profile_picture .= $pro['profile_picture'];
                            }
                            
                            //Get User's Profile Row.
                            $profile = $this->Profiles->get($id);
                            
                            //If profile_picture is null or empty.
                            if ($profile_picture == '') {
                                
                                //Update profile_picture record.
                                $profile->profile_picture = $new_filename;                           
                                
                                //Save Changes.
                                if ($this->Profiles->save($profile)) {
                                    
                                    //Delete User's img_tmp record and delete image from tmp directory.
                                    if ($this->globalworks->deletetmpimg($user_id, $filename_orig)) {
                                        echo 'ok';
                                    }
                                }
                            
                            //If profile_picture is not empty or not null.
                            } else {
                                
                                //Update Record and Delete old profile pictures.
                                if ($this->globalworks->delete_exisiting_profile_picture($user_id, $profile_picture, $new_filename)) {
                                    
                                    //Update profile_picture record.
                                    $profile->profile_picture = $new_filename;
                                    
                                    //Delete User's img_tmp record and temporary image from tmp directory.
                                    if ($this->globalworks->deletetmpimg($user_id, $filename_orig)) {
                                        echo 'ok';
                                    }
                                }
                            }
                        }
                    }
                break;
                
                /*
                *   Operation to delete user's profile picture.
                *   Main Route: "/uploadresume/*"
                *
                *   Associated Controller and Action.
                *   Controller => Employersdashboard, Action => profile.
                */
                case 'image_delete':
                    
                    //Load Profiles Model
                    $this->loadModel('Profiles');

                    //Value to append.
                    $id = '';

                    //Value to append.
                    $filename = '';

                    //Query out to get user's Profile record.
                    $profile = $this->Profiles->find()
                        ->select(['id', 'profile_picture'])
                        ->where(['user_id' => $this->request->session()->read('Auth.User')['id']]);

                    //Loop Result.
                    foreach ($profile as $pro) {
                        $id .= $pro['id'];
                        $filename .= $pro['profile_picture'];
                    }

                    //Path to profile_pictures dir.
                    $pp_dir = './img/users/profile_pictures/';

                    //Path to profile_thumbnails dir.
                    $thumb_dir = './img/users/profile_thumbnails/';

                    //Check if Profile Picture is exists.
                    if (file_exists($pp_dir . $filename)) {
                        unlink($pp_dir . $filename);
                    }

                    //Check if Profile Thumbnail is exist.
                    if (file_exists($thumb_dir . $filename)) {
                        unlink($thumb_dir . $filename);
                    }

                    /*
                    *   Update User's Profile profile_picture column to empty content.
                    */

                    //Get User's Column Record.
                    $u_profile = $this->Profiles->get($id);

                    //Update profile_picture column.
                    $u_profile->profile_picture = '';

                    //Save User's Record.
                    if ($this->Profiles->save($u_profile)) {
                        echo 'ok';
                    }

                break;
                
                /*
                *   Operation to upload resumes.
                *   Main Route: "/uploadresume/*"
                *
                *   Associated Controller and Action.
                *   Controller => Home, Action => uploadresume.
                */
                case 'resume_upload':
                    
                    //User's Session ID.
                    $user_id = $this->request->session()->read('Auth.User')['id'];
                    
                    //Details value from post request.
                    $details = $this->request->data('details');
                    
                    //Original Resume Filename.
                    $filename_orig = $_FILES['ddu-upload']['name'];
                    
                    //Upload error.
                    $error = $_FILES['ddu-upload']['error'];
                    
                    //Resume file itself.
                    $tmp = $_FILES['ddu-upload']['tmp_name'];
                    
                    //If no Upload Error.
                    if ($error == 0) {
                        
                        //Explode filename to array.
                        $filename = explode('.', $filename_orig);
                        
                        //Get Filename Extension.
                        $ext = '.' . end($filename);
                        
                        //Random Name Value.
                        $random_name = '';
                        
                        //Strings to make it random.
                        $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                        
                        //Generate random strings.
                        for ($p = 0; $p < 16; $p++) {
                            
                            //Append random strings to the value.
                            $random_name .= $chars[mt_rand(0, strlen($chars)-1)];
                        }
                        
                        //Create new filename.
                        $random_name = $user_id . date( "YmdHis" ) . $random_name . $ext;
                        
                        //Destination path and filename.
                        $path_to = './resumes/' . $random_name;
                        
                        //Move uploaded file to the destination path.
                        if (move_uploaded_file($tmp, $path_to)) {
                            
                            //Load Model.
                            $this->loadModel('Resumes');
                            
                            //Count User's Resumes record if exists or not.
                            $resumes_count = $this->Resumes->find()
                                ->where(['user_id' => $user_id])
                                ->count();
                            
                            //If User's Resumes record does not exists, create record.
                            if ($resumes_count == 0) {
                                
                                //New Entity.
                                $resumes = $this->Resumes->newEntity();
                                
                                //Records to be insert in Resumes database table.
                                $new_entity = [
                                    'user_id' => $user_id,
                                    'filename' => $random_name,
                                    'details' => $details,
                                    'orig_filename' => $filename_orig,
                                    'date' => date( "Y-m-d H:i:s" ),
                                ];
                                
                                //Patch Entity.
                                $resumes = $this->Resumes->patchEntity($resumes, $new_entity);
                                
                                //Create or Insert record to Resumes Database Table.
                                if ($this->Resumes->save($resumes)) {
                                    echo 'ok';
                                }
                                
                            //If User's Resumes record already exist, update record.
                            } else {
                                
                                //Get User's Resumes Record.
                                $resumes = $this->Resumes->find()
                                    ->where(['user_id' => $user_id]);
                                
                                //User's Resumes id row to be append.
                                $id = '';
                                
                                //Old Filename to be append.
                                $old_file = '';
                                
                                //Loop results.
                                foreach ($resumes as $res) {
                                    
                                    //Append ID.
                                    $id .= $res['id'];
                                    
                                    //Append old filename.
                                    $old_file .= $res['filename'];
                                }
                                
                                //Get User's Resumes row.
                                $resume = $this->Resumes->get($id);
                                
                                //Update filename record.
                                $resume->filename = $random_name;
                                
                                //Update details record.
                                $resume->details = $details;
                                
                                //Original Filename.
                                $resume->orig_filename = $filename_orig;
                                
                                //Update date record.
                                $resume->date = date( "Y-m-d H:i:s" );
                                
                                //Save Changes.
                                if ($this->Resumes->save($resume)) {
                                    
                                    //Path to the resumes directory and old filename.
                                    $resume_path_to = './resumes/' . $old_file;
                                    
                                    //If file exists delete old resume file.
                                    if (file_exists($resume_path_to)) {
                                        if (unlink($resume_path_to)) {
                                            echo 'ok';
                                        }
                                    }
                                }
                            }
                        }
                    }
                break;
                
                /*
                *   Operation to upload banner/header image on job post.
                *   Main Route: "/employers/jobpost"
                *
                *   Associated Controller and Action.
                *   Controller => Employersdashboard, Action => jobpost.
                */
                case 'employer_header_image':
                    
                    //If User has Employer role.
                    if ($this->request->session()->read('Auth.User')['role'] == 'Employer') {
                        
                        //File Uploaded Information value.
                        $image = $_FILES['header-image'];
                        
                        //Image file type.
                        $type = $image['type'];
                        
                        //Image file itself.
                        $tmp = $image['tmp_name'];
                        
                        //Upload Error.
                        $error = $image['error'];
                        
                        //If Upload Error is 0
                        if ($error == 0) {

                            //Path to job_header_tmp dir.
                            $path = './img/users/job_header_tmp/';
                            
                            //Employer's User ID.
                            $user_id = $this->request->session()->read('Auth.User')['id'];
                            
                            //Random Char value to append.
                            $random_chars = '';
                        
                            //Characters to generate.
                            $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

                            //Generate random strings.
                            for ($p = 0; $p < 16; $p++) {
                                $random_chars .= $chars[mt_rand(0, strlen($chars)-1)];
                            }
                            
                            //New Filename for header image.
                            $new_filename = $user_id . date( "YmdHis" ) . $random_chars . '.jpg';
                            
                            //Get Image Dimension.
                            list($width, $height) = getimagesize($tmp);

                            //Initiate GD Library.
                            $img = imagecreatetruecolor($width, $height);
                            
                            //Image file types. If image is jpeg format.
                            if ($type == 'image/jpeg') {
                                $img_src = imagecreatefromjpeg($tmp);

                            //If image is png format.
                            } else if ($type == 'image/png') {
                                $img_src = imagecreatefrompng($tmp);
                            }

                            //Cereate Image.
                            imagecopyresampled($img, $img_src, 0, 0, 0, 0, $width, $height, $width, $height);

                            //Move Image to the dirctory path as jpeg format.
                            imagejpeg($img, $path . $new_filename);
                            
                            $this->loadModel('Jobheaderimg_tmp');
                            
                            //New Entity.
                            $header = $this->Jobheaderimg_tmp->newEntity();
                            
                            //Create New Entity.
                            $new_entity = [
                                'user_id' => $this->request->session()->read('Auth.User')['id'],
                                'filename' => $new_filename,
                                'uploaded_at' => microtime(true),
                            ];
                            
                            //Patch Entity.
                            $header = $this->Jobheaderimg_tmp->patchEntity($header, $new_entity);
                            
                            //Save Image Record.
                            if ($this->Jobheaderimg_tmp->save($header)) {
                                echo $new_filename;
                            }
                        }
                    }
                break;
                
                /*
                *   Operation to Delete Header Image from Create Jobpost.
                *   Main Route: "/employers/jobpost"
                *
                *   Associated Controller and Action.
                *   Controller => Employersdashboard, Action => jobpost.
                */
                case 'delete_job_header_img':
                    
                    //If User has Employer Role.
                    if ($this->request->session()->read('Auth.User')['role'] == 'Employer') {

                        //Filename from POST Request.
                        $filename = $this->request->data('filename');

                        //Path to job_header_tmp dir.
                        $path = './img/users/job_header_tmp/';
                        
                        //Load Jobheaderimg_tmp Model.
                        $this->loadModel('Jobheaderimg_tmp');
                        
                        //ID Value to append.
                        $id = '';

                        //Query out to find users column.
                        $img_tmp = $this->Jobheaderimg_tmp->find()
                            ->where(['filename' => $filename]);
                        
                        //Loop Result.
                        foreach ($img_tmp as $tmp) {

                            //Append ID.
                            $id .= $tmp['id'];
                        }
                        
                        //If File exists. delete file and delete record.
                        if (file_exists($path . $filename)) {
                            if (unlink($path . $filename)) {
                                $header = $this->Jobheaderimg_tmp->get($id);
                                
                                if ($this->Jobheaderimg_tmp->delete($header)) {
                                    echo 'ok';
                                }
                            }
                        }
                    }
                break;
                
                /*
                *   Operation to Upload Company Logo.
                *   Main Route: "/employers/jobpost"
                *
                *   Associated Controller and Action.
                *   Controller => Employersdashboard, Action => jobpost.
                */
                case 'upload_company_logo':

                    //If User's Role is Employer.
                    if ($this->request->session()->read('Auth.User')['role'] == 'Employer') {

                        //Image Properties.
                        $image = $_FILES['logo-image'];
                        $filename = $image['name'];
                        $type = $image['type'];
                        $tmp = $image['tmp_name'];
                        $error = $image['error'];
                        
                        //If Error is 0.
                        if ($error == 0) {

                            //Path to company_logo_tmp dir.
                            $path = './img/users/company_logo_tmp/';
                            
                            //USer's User ID.
                            $user_id = $this->request->session()->read('Auth.User')['id'];
                            
                            //Value to append for random string.
                            $random_chars = '';
                            
                            $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

                            //Generate random strings.
                            for ($p = 0; $p < 16; $p++) {
                                $random_chars .= $chars[mt_rand(0, strlen($chars)-1)];
                            }
                            
                            //New Filename.
                            $new_filename = $user_id . date( "YmdHis" ) . $random_chars . '.jpg';
                            
                            //Get Image Dimension.
                            list($width, $height) = getimagesize($tmp);

                            //Initiate GD Library.
                            $img = imagecreatetruecolor($width, $height);
                            
                            //Image file types. If image is jpeg format.
                            if ($type == 'image/jpeg') {
                                $img_src = imagecreatefromjpeg($tmp);

                            //Image file types. If image is png format.
                            } else if ($type == 'image/png') {
                                $img_src = imagecreatefrompng($tmp);
                            }

                            //Create Image.
                            imagecopyresampled($img, $img_src, 0, 0, 0, 0, $width, $height, $width, $height);

                            //Move Image to the dirctory path as jpeg format.
                            imagejpeg($img, $path . $new_filename);
                            
                            //Load Companylogo_tmp Model.
                            $this->loadModel('Companylogo_tmp');
                            
                            //New Entity.
                            $logo = $this->Companylogo_tmp->newEntity();
                            
                            //New Entity Append.
                            $new_entity = [
                                'user_id' => $user_id,
                                'filename' => $new_filename,
                                'uploaded_at' => microtime(true),
                            ];
                            
                            //Patch Entity
                            $logo = $this->Companylogo_tmp->patchEntity($logo, $new_entity);
                            
                            //Save Record.
                            if ($this->Companylogo_tmp->save($logo)) {
                                echo $new_filename;
                            }
                        }
                    }
                break;
                
                /*
                *   Operation to Delete Company Logo.
                *   Main Route: "/employers/jobpost"
                *
                *   Associated Controller and Action.
                *   Controller => Employersdashboard, Action => jobpost.
                */
                case 'delete_company_logo':

                    //If User has Employers Role.
                    if ($this->request->session()->read('Auth.User')['role'] == 'Employer') {

                        //Filename from POST Request.
                        $filename = $this->request->data('filename');

                        //Path to company_logo_tmp dir.
                        $path = './img/users/company_logo_tmp/';

                        //ID Value to append.
                        $id = '';
                        
                        //Load Companylogo_tmp Model.
                        $this->loadModel('Companylogo_tmp');
                        
                        //Query out to find user's column.
                        $image = $this->Companylogo_tmp->find()
                            ->where(['filename' => $filename]);
                        
                        //Loop Result.
                        foreach ($image as $img) {

                            //Append ID.
                            $id .= $img['id'];
                        }
                        
                        //Delete Image File if file exists.
                        if (file_exists($path . $filename)) {
                            if (unlink($path . $filename)) {
                                $logo = $this->Companylogo_tmp->get($id);
                                
                                if ($this->Companylogo_tmp->delete($logo)) {
                                    echo 'ok';
                                }
                            }
                        }
                    }
                break;
                
                /*
                *   Operation to view employer's jobpost.
                *   Main Route: "/employers/joblist/"
                *
                *   Associated Controller and Action.
                *   Controller => Employersdashboard, Action => joblist
                *   Controller -> Ajax, Action => employers_viewpost
                */
                case 'employers_jobpost_view':

                    //Load Model.
                    $this->loadModel('Jobposts');
                    
                    //Enable view render.
                    $this->autoRender = true;

                    //Set Template. (.ctp)
                    $this->viewBuilder()->template('employers_viewpost');
                    
                    //ID Value from POST Request. (Jobpost ID)
                    $id = $this->request->data('id');
                    
                    //User's Nickname.
                    $nickname = $this->globalworks->getnickname($this->request->session()->read('Auth.User')['id']);
                    
                    //If Nickname is null use firstname and lastname.
                    if (!$nickname) {
                        $fname = $this->globalworks->getfirstname($this->request->session()->read('Auth.User')['id']);
                        $lastname = $this->globalworks->getlastname($this->request->session()->read('Auth.User')['id']);
                        
                        $nickname = $fname . ' ' . $lastname;
                    }
                    
                    //Query out to find user's requested jobpost count.
                    $jobpost_count = $this->Jobposts->find()
                        ->where(['id' => $id])
                        ->andWhere(['user_id' => $this->request->session()->read('Auth.User')['id']])
                        ->count();
                    
                    //Query out to find user's requested jobpost.
                    $jobpost = $this->Jobposts->find()
                        ->where(['id' => $id])
                        ->andWhere(['user_id' => $this->request->session()->read('Auth.User')['id']]);
                    
                    $this->set('nickname', $nickname);
                    $this->set('jobpost_count', $jobpost_count);
                    $this->set('jobpost', $jobpost);
                break;
                
                /*
                *   Operation to edit employer's jobpost.
                *   Main Route: "/employers/joblist/"
                *
                *   Associated Controller and Action.
                *   Controller => Employersdashboard, Action => joblist.
                *   Controller -> Ajax, Action => employers_post_edit.
                */
                case 'employers_jobpost_edit':

                    //Load Model.
                    $this->loadModel('Jobposts');

                    //Enable view render.
                    $this->autoRender = true;

                    //Set Template. (.ctp)
                    $this->viewBuilder()->template('employers_post_edit');
                    
                    //ID Value from POST Request. (Jobpost ID)
                    $id = $this->request->data('id');
                    
                    //Query out to find requested jobpost.
                    $jobposts = $this->Jobposts->find()
                        ->where(['id' => $id])
                        ->andWhere(['user_id' => $this->request->session()->read('Auth.User')['id']]);

                    //Loop Results and Render to view.
                    foreach ($jobposts as $post) {
                        $this->set('id', $post['id']);
                        $this->set('user_id', $post['user_id']);
                        $this->set('src_header_image', $post['header_image']);
                        $this->set('src_company_logo', $post['company_logo']);
                        $this->set('src_job_title', $post['job_title']);
                        $this->set('src_company_name', $post['company_name']);
                        $this->set('src_job_category', $post['job_category']);
                        $this->set('src_job_type', $post['job_type']);
                        $this->set('src_job_migration', $post['job_migration']);
                        $this->set('src_country', $post['country']);
                        $this->set('src_job_description', $post['job_description']);
                        $this->set('src_job_responsibilities', $post['job_responsibilities']);
                        $this->set('src_job_qualifications', $post['job_qualifications']);
                        $this->set('src_job_year_experience', $post['job_years_experience']);
                        $this->set('src_job_location', $post['job_location']);
                        $this->set('src_job_website', $post['job_website']);
                        $this->set('src_job_email', $post['job_email']);
                        $this->set('src_job_contact_number', $post['job_contact_number']);
                        $this->set('closing_date', $post['closing_date']);
                        $this->set('googglemap', $post['googglemap']);
                        $this->set('modified_at', $post['modifiled_at']);
                    }
                break;
                
                /*
                *   Operation to delete header post on jobpost edit.
                *   Main Route: "/employers/joblist/"
                *
                *   Associated Controller and Action.
                *   Controller => Employersdashboard, Action => joblist.
                */
                case 'employers_jobpost_delete_header_tmp':

                    //If User has Employer Role.
                    if ($this->request->session()->read('Auth.User')['role'] == 'Employer') {

                        //Load Jobheader
                        $this->loadModel('Jobheaderimg_tmp');
                        
                        //Query out to check if user has record from the table.
                        $jobheader_count = $this->Jobheaderimg_tmp->find()
                            ->where(['user_id' => $this->request->session()->read('Auth.User')['id']])
                            ->andWhere(['filename' => $this->request->data('filename')])
                            ->count();
                        
                        //If No Record.
                        if ($jobheader_count > 0) {

                            //Query out to find record.
                            $jobheader = $this->Jobheaderimg_tmp->find()
                                ->where(['user_id' => $this->request->session()->read('Auth.User')['id']])
                                ->andWhere(['filename' => $this->request->data('filename')]);
                            
                            //ID to append.
                            $id = '';
                            
                            //Loop Result.
                            foreach ($jobheader as $header) {

                                //Append ID.
                                $id .= $header['id'];
                            }
                            
                            //Get tmp Record.
                            $jh = $this->Jobheaderimg_tmp->get($id);
                            
                            //Delete Record and Delete tmp File.
                            if ($this->Jobheaderimg_tmp->delete($jh)) {

                                //Path to job_header dir.
                                $header_path = './img/users/job_header_tmp/';
                                
                                //IF file exists. delete image.
                                if (file_exists($header_path . $this->request->data('filename'))) {
                                    if (unlink($header_path . $this->request->data('filename'))) {
                                        echo 'ok';
                                    }
                                }
                            }
                        } else {
                            echo 'ok';
                        }
                    }
                break;
                
                /*
                *   Operation to Upload header image from jobpostedit.
                *   Main Route: "/employers/jobedit"
                *
                *   Associated Controller and Action.
                *   Controller => Employersdashboard, Action => jobedit.
                */
                case 'employers_jobpost_upload_new_header':

                    //If User has Employer Role.
                    if ($this->request->session()->read('Auth.User')['role'] == 'Employer') {

                        //File Uploaded Properties.
                        $img = $_FILES['header-image'];
                        $type = $img['type'];
                        $tmp = $img['tmp_name'];
                        $error = $img['error'];

                        //If error is 0.
                        if ($error == 0) {

                            //Path to job_header_dir.
                            $path = './img/users/job_header_tmp/';

                            //USer's user ID.
                            $user_id = $this->request->session()->read('Auth.User')['id'];

                            //To be append. for random strings.
                            $random_chars = '';

                            //Characters to generate.
                            $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

                            //Generate random strings.
                            for ($p = 0; $p < 16; $p++) {
                                $random_chars .= $chars[mt_rand(0, strlen($chars)-1)];
                            }

                            //New Image Filename.
                            $new_filename = $user_id . date( "YmdHis" ) . $random_chars . '.jpg';

                            //Get Image Dimansion.
                            list($width, $height) = getimagesize($tmp);

                            //Initiate GD Library.
                            $img = imagecreatetruecolor($width, $height);

                            //If Image format is jpeg.
                            if ($type == 'image/jpeg') {
                                $img_src = imagecreatefromjpeg($tmp);

                            //If Image format is png.
                            } else if ($type == 'image/png') {
                                $img_src = imagecreatefrompng($tmp);
                            }

                            //Create Image.
                            imagecopyresampled($img, $img_src, 0, 0, 0, 0, $width, $height, $width, $height);

                            //Move image from $path dir.
                            imagejpeg($img, $path . $new_filename);

                            //Load Jobheaderimg_tmp Model.
                            $this->loadModel('Jobheaderimg_tmp');

                            //New Entity.
                            $header = $this->Jobheaderimg_tmp->newEntity();

                            //Append New Entity.
                            $new_entity = [
                                'user_id' => $this->request->session()->read('Auth.User')['id'],
                                'filename' => $new_filename,
                                'uploaded_at' => microtime(true),
                            ];

                            //Patch Entity.
                            $header = $this->Jobheaderimg_tmp->patchEntity($header, $new_entity);

                            //Save Record.
                            if ($this->Jobheaderimg_tmp->save($header)) {
                                echo $new_filename;
                            }
                        }
                    }
                break;
                
                /*
                *   Operation to Delete Header Image from Create Jobedit.
                *   Main Route: "/employers/joblist"
                *
                */
                case 'employers_jobpost_delete_logo_tmp':

                    //If User has Employers Role.
                    if ($this->request->session()->read('Auth.User')['role'] == 'Employer') {

                        //Load Companylogo_tmp Model.
                        $this->loadModel('Companylogo_tmp');
                        
                        //Check if file is exist from the record.
                        $companylogo_count = $this->Companylogo_tmp->find()
                            ->where(['user_id' => $this->request->session()->read('Auth.User')['id']])
                            ->andWhere(['filename' => $this->request->data('filename')])
                            ->count();
                        
                        //If file record exists.
                        if ($companylogo_count !== 0) {

                            //Query out file record.
                            $companylogo = $this->Companylogo_tmp->find()
                                ->where(['user_id' => $this->request->session()->read('Auth.User')['id']])
                                ->andWhere(['filename' => $this->request->data('filename')]);
                            
                            //ID to append.
                            $id = '';
                            
                            //Loop Result.
                            foreach ($companylogo as $cp) {

                                //Append ID.
                                $id .= $cp['id'];
                            }
                            
                            //Get Logo record column.
                            $logo = $this->Companylogo_tmp->get($id);
                            
                            //Delete record and delete file.
                            if ($this->Companylogo_tmp->delete($logo)) {
                                $logo_path = './img/users/company_logo_tmp/';
                                
                                if (file_exists($logo_path . $this->request->data('filename'))) {
                                    if (unlink($logo_path . $this->request->data('filename'))) {
                                        echo 'ok';
                                    }
                                }
                            }
                        } else {
                            echo 'ok';
                        }
                    }
                break;
                
                /*
                *   Operation to Upload Company Logo on jobedit.
                *   Main Route: "/employers/joblist"
                *
                *   Associated Controller and Action.
                *   Controller => Employersdashboard, Action => joblist.
                */
                case 'employers_jobpost_upload_new_logo':

                    //If User has Employers Role.
                    if ($this->request->session()->read('Auth.User')['role'] == 'Employer') {

                        //Uploaded File Properties.
                        $image = $_FILES['logo-image'];
                        $filename = $image['name'];
                        $type = $image['type'];
                        $tmp = $image['tmp_name'];
                        $error = $image['error'];
                        
                        //If Error is 0.
                        if ($error == 0) {

                            //Path to company_logo_tmp dir.
                            $path = './img/users/company_logo_tmp/';
                            
                            //User's User ID.
                            $user_id = $this->request->session()->read('Auth.User')['id'];
                            
                            $random_chars = '';
                            
                            //Random Strings to Generate.
                            $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

                            //Generate random strings.
                            for ($p = 0; $p < 16; $p++) {
                                $random_chars .= $chars[mt_rand(0, strlen($chars)-1)];
                            }
                            
                            //New Filename.
                            $new_filename = $user_id . date( "YmdHis" ) . $random_chars . '.jpg';
                            
                            //Get Image Dimension.
                            list($width, $height) = getimagesize($tmp);

                            //Initiate GD Library.
                            $img = imagecreatetruecolor($width, $height);
                            
                            //Image file types. If image is jpeg format.
                            if ($type == 'image/jpeg') {
                                $img_src = imagecreatefromjpeg($tmp);
                            
                            //Image file types. If image is png format.
                            } else if ($type == 'image/png') {
                                $img_src = imagecreatefrompng($tmp);
                            }

                            //Create Image.
                            imagecopyresampled($img, $img_src, 0, 0, 0, 0, $width, $height, $width, $height);

                            //Move Image to the dirctory path as jpeg format.
                            imagejpeg($img, $path . $new_filename);
                            
                            //Load Companylogo_tmp Model.
                            $this->loadModel('Companylogo_tmp');
                            
                            //New Entity.
                            $logo = $this->Companylogo_tmp->newEntity();
                            
                            //New Entity Append.
                            $new_entity = [
                                'user_id' => $user_id,
                                'filename' => $new_filename,
                                'uploaded_at' => microtime(true),
                            ];
                            
                            //Patch Entity
                            $logo = $this->Companylogo_tmp->patchEntity($logo, $new_entity);
                            
                            //Save Record.
                            if ($this->Companylogo_tmp->save($logo)) {
                                echo $new_filename;
                            }
                        }
                    }
                break;
                
                /*
                *   Operation to Save Edit Jobpost.
                *   Main Route: "/employers/joblist"
                *
                *   Associated Controller and Action.
                *   Controller => Employersdashboard, Action => joblist.
                */
                case 'employers_jobpost_edit_save':
                    if ($this->request->session()->read('Auth.User')['role'] == 'Employer') {
                        $this->loadModel('Jobposts');

                        //Get Job Post Requested.
                        $jobposts = $this->globalworks->get_job_post($this->request->data('id'));

                        $header_image = '';
                        $company_logo = '';

                        //Loop Results and Append values to get header_image and company_logo filenames.
                        foreach ($jobposts as $jobpost) {
                            $header_image .= $jobpost['header_image'];
                            $company_logo .= $jobpost['company_logo'];
                        }

                        /*
                        *   If Header Image Filename does not match to the current header image file.
                        *   delete current image file. But if this->request->data('header_image') is null or empty.
                        *   No Image to upload.
                        */
                        if ($this->request->data('header_image') !== $header_image) {
                            $this->globalworks->header_image_delete($header_image);
                            $this->globalworks->header_image_move($this->request->data('header_image'));
                        }

                        /*
                        *   If company_logo Image Filename does not match to the current company_logo image file.
                        *   delete current image file. But if this->request->data('company_logo') is null or empty.
                        *   No Image to upload.
                        */
                        if ($this->request->data('company_logo') !== $company_logo) {
                            $this->globalworks->company_logo_delete($company_logo);
                            $this->globalworks->company_logo_move($this->request->data('company_logo'));
                        }

                        //To be append.
                        $job_years_experience = '';

                        //If years_exp_end is not null or empty.
                        if ($this->request->data('years_exp_end') !== '') {
                            $job_years_experience = $this->request->data('years_exp_start') . ' to ' . $this->request->data('years_exp_end');
                        } else {
                            $job_years_experience = $this->request->data('years_exp_start');
                        }

                        //Qualifcation value to append.
                        $qualifications = '';

                        //Responsibilities value to append.
                        $responsibilities = '';

                        //If Qualifications is not empty.
                        if ($this->request->data('qualifications')) {
                            $qualifications = json_encode($this->request->data('qualifications'));
                        }
                        
                        //If Responsibilities is not empty.
                        if ($this->request->data('responsibilities')) {
                            $responsibilities = json_encode($this->request->data('responsibilities'));
                        }

                        //Get Jobpost ID.
                        $jobpost = $this->Jobposts->get($this->request->data('id'));

                        //Apply as New Entities.
                        $jobpost->header_image = $this->request->data('header_image');
                        $jobpost->company_logo = $this->request->data('company_logo');
                        $jobpost->job_title = $this->request->data('job_title');
                        $jobpost->company_name = $this->request->data('company_name');
                        $jobpost->job_category = $this->request->data('job_category');
                        $jobpost->job_mirgation = $this->request->data('job_base');
                        $jobpost->country = $this->request->data('country');
                        $jobpost->job_description = $this->request->data('description');
                        $jobpost->job_qualifications = $qualifications;
                        $jobpost->job_responsibilities = $responsibilities;
                        $jobpost->job_years_experience = $job_years_experience;
                        $jobpost->job_location = $this->request->data('job_location');
                        $jobpost->job_website = $this->request->data('job_website');
                        $jobpost->job_contact_number = $this->request->data('job_contact_number');
                        $jobpost->googglemap = $this->request->data('googglemap');
                        $jobpost->closing_date = $this->request->data('closing_date');
                        $jobpost->modified_at = date( "Y-m-d H:i:s" );

                        //Save Changes.
                        if ($this->Jobposts->save($jobpost)) {

                            /*
                            *   Create Log Record.
                            */

                            //Poster's Fullname.
                            $fullname = $this->request->session()->read('Auth.User')['firstname'] . ' ' . $this->request->session()->read('Auth.User')['lastname'];

                            //Poster's Email.
                            $email = $this->request->session()->read('Auth.User')['email'];
                            
                            //Create Log Message.
                            $log_msg = $fullname . ' edit a Job Post "(' .$email . ')" "' . $this->request->data('job_title') .'"';

                            //Save Log Message.
                            $this->globalworks->_log('edit_jobposts_log', $log_msg);

                            $this->Flash->success(__('Job Post successfully updated.'));
                            echo 'ok';
                        }
                    }
                break;
                
                /*
                *   Operation to Delete Jobpost.
                *   Main Route: "/employers/joblist"
                *
                *   Associated Controller and Action.
                *   Controller => Employersdashboard, Action => joblist.
                */
                case 'employers_jobpost_delete':

                    //If User has Employer Role.
                    if ($this->request->session()->read('Auth.User')['role'] == 'Employer') {

                        //Load Jobposts Model.
                        $this->loadModel('Jobposts');

                        //Query out to find Jobpost record, And Check header_image and company_logo is set and get job_title.
                        $jobposts = $this->Jobposts->find()
                            ->where(['user_id' => $this->request->session()->read('Auth.User')['id']])
                            ->andWhere(['id' => $this->request->data('id')]);

                        //To be Append.
                        $job_title = '';

                        //To be Append.
                        $header_file = '';

                        //To be Append.
                        $logo_file = '';

                        //Loop Results.
                        foreach ($jobposts as $post) {

                            //Append Post Title.
                            $job_title .= $post['job_title'];

                            //Append Header Filename.
                            $header_file .= $post['header_image'];

                            //Append company_logo Filename
                            $logo_file .= $post['company_logo'];
                        }

                        //Path to job_header dir.
                        $header_path = './img/users/job_header/';

                        //Path to company_logo dir.
                        $logo_path = './img/users/company_logo/';

                        //If File exists delete image file.
                        if ($header_file && file_exists($header_path . $header_file)) {
                            unlink($header_path . $header_file);
                        }

                        //If File exists delete image file.
                        if ($logo_file && file_exists($logo_path . $logo_file)) {
                            unlink($logo_path . $logo_file);
                        }

                        //Get Record Column.
                        $jb = $this->Jobposts->get($this->request->data('id'));

                        //Delete Record.
                        if ($this->Jobposts->delete($jb)) {

                            /*
                            *   Create Log Record.
                            */

                            //Poster's Fullname.
                            $fullname = $this->request->session()->read('Auth.User')['firstname'] . ' ' . $this->request->session()->read('Auth.User')['lastname'];

                            //Poster's Email.
                            $email = $this->request->session()->read('Auth.User')['email'];
                            
                            //Create Log Message.
                            $log_msg = $fullname . ' "(' .$email . ')" delete a Job Post "' . $job_title .'"';

                            //Save Log Message.
                            $this->globalworks->_log('delete_jobposts_log', $log_msg);
                            
                            echo 'ok';
                            $this->Flash->success(__('Job Post successfully deleted.'));
                        }
                    }
                break;
                
                /*
                *   Operation to view selected jobpost.
                *   Main Route: "/jobs/category/*"
                *
                *   Associated Controller and Action.
                *   Controller => Jobs, Action => category.
                */
                case 'jobpost_view':
                    //Load Jobposts Model.
                    $this->loadModel('Jobposts');
                    
                    //Count if requested post id is exist.
                    $jobpost_count = $this->Jobposts->find()
                        ->where(['id' => $this->request->data('id')])
                        ->count();
                    
                    //Query out to get requested jobpost.
                    $jobpost = $this->Jobposts->find()
                        ->where(['id' => $this->request->data('id')]);
                    
                    $this->autoRender = true;
                    $this->viewBuilder()->template('category');
                    
                    //Load User_log_msg Model.
                    $this->loadModel('User_log_msg');
                    
                    /*
                    *   Check if Candidate is already applyed for this job.
                    */
                    //Count to check if User has already a log for the jobpost.
                    $log_msg_count = $this->User_log_msg->find()
                        ->where(['user_id' => $this->request->session()->read('Auth.User')['id']])
                        ->andWhere(['job_app' => $this->request->data('id')])
                        ->count();
                    
                    $this->set('jobpost_count', $jobpost_count);
                    $this->set('jobpost', $jobpost);
                    $this->set('log_msg', $log_msg_count);
                break;
                
                /*
                *   Operation to view Employer's Profile Picture Upload page.
                *   Main Route: "/employers/profile/*"
                *
                *   Associated Controller and Action.
                *   Controller => Employersdashboard, Action => profile.
                */
                case 'upload_employers_profile_picture':

                    //If User has Employer role.
                    if ($this->request->session()->read('Auth.User')['role'] == 'Employer') {

                        //Render Template.
                        $this->autoRender = true;

                        //Select Template.
                        $this->viewBuilder()->template('employers_uploadpicture');
                    }
                break;
                
                /*
                *   Operation to view Applicants's Profile Picture Upload page.
                *   Main Route: "/candidates/profile/*"
                *
                *   Associated Controller and Action.
                *   Controller => Candidatesdashboard, Action => profile.
                */
                case 'upload_candidates_profile_picture':
                    
                    //If User has Applicant role.
                    if ($this->request->session()->read('Auth.User')['role'] == 'Applicant') {

                        //Render Template.
                        $this->autoRender = true;

                        //Select Template.
                        $this->viewBuilder()->template('applicant_uploadpicture');
                    }
                break;
                
                /*
                *   Operation to view Employer's Profile Picture Upload Image for Cropping.
                *   Main Route: "/employers/profile/*"
                *
                *   Associated Controller and Action.
                *   Controller => Employersdashboard, Action => profile.
                */
                case 'crop_employers_profile_picture':
                    
                    //If User has Employer role.
                    if ($this->request->session()->read('Auth.User')['role'] == 'Employer') {

                        //Load Img_tmp Model
                        $this->loadModel('Img_tmp');

                        //Count to check if User has Img_tmp Record.
                        $img_count = $this->Img_tmp->find()
                            ->where(['user_id' => $this->request->session()->read('Auth.User')['id']])
                            ->count();

                        //If Count is greaterthan 0. Means User has record.
                        if ($img_count > 0) {

                            //Render view.
                            $this->autoRender = true;

                            //Set Template.
                            $this->viewBuilder()->template('employers_image_crop');

                            //Query out to get User's Record.
                            $imgs = $this->Img_tmp->find()
                                ->select(['filename'])
                                ->where(['user_id' => $this->request->session()->read('Auth.User')['id']]);

                            //To be append.
                            $filename = '';

                            //Loop Result.
                            foreach ($imgs as $img) {
                                $filename .= $img['filename'];
                            }

                            //Split filename via dot.
                            $explode = explode('.', $filename);

                            //If Filename extension is not .jpg, change file extension to .jpg as filename
                            if (end($explode) !== '.jpg') {
                                $new_filename = $explode[0] . '.jpg';
                            } else {
                                $new_filename = $filename;
                            }

                            //Render to view.
                            $this->set('filename', $filename);
                            $this->set('new_filename', $new_filename);
                        }
                    }
                break;
                
                /*
                *   Operation to view Applicants's Profile Picture Upload Image for Cropping.
                *   Main Route: "/candidates/profile/*"
                *
                *   Associated Controller and Action.
                *   Controller => Candidatesdashboard, Action => profile.
                */
                case 'crop_applicants_profile_picture':
                    
                    //If User's Has Applicant Role.
                    if ($this->request->session()->read('Auth.User')['role'] == 'Applicant') {
                        
                        //Load Img_tmp Model
                        $this->loadModel('Img_tmp');

                        //Count to check if User has Img_tmp Record.
                        $img_count = $this->Img_tmp->find()
                            ->where(['user_id' => $this->request->session()->read('Auth.User')['id']])
                            ->count();

                        //If Count is greaterthan 0. Means User has record.
                        if ($img_count > 0) {

                            //Render view.
                            $this->autoRender = true;

                            //Set Template.
                            $this->viewBuilder()->template('applicants_image_crop');

                            //Query out to get User's Record.
                            $imgs = $this->Img_tmp->find()
                                ->select(['filename'])
                                ->where(['user_id' => $this->request->session()->read('Auth.User')['id']]);

                            //To be append.
                            $filename = '';

                            //Loop Result.
                            foreach ($imgs as $img) {
                                $filename .= $img['filename'];
                            }

                            //Split filename via dot.
                            $explode = explode('.', $filename);

                            //If Filename extension is not .jpg, change file extension to .jpg as filename
                            if (end($explode) !== '.jpg') {
                                $new_filename = $explode[0] . '.jpg';
                            } else {
                                $new_filename = $filename;
                            }

                            //Render to view.
                            $this->set('filename', $filename);
                            $this->set('new_filename', $new_filename);
                        }
                    }
                break;
                
                /*
                *   Operation to load resume upload page.
                *   Main Route: "/candidates/profile/*"
                *
                *   Associated Controller and Action.
                *   Controller => Candidatesdashboard, Action => profile.
                */
                case 'upload_candidates_resume':
                    
                    //If User has Applicant Role.
                    if ($this->request->session()->read('Auth.User')['role'] == 'Applicant') {
                        
                        //Set View for resume upload page.
                        $this->autoRender = true;
                        $this->viewBuilder()->template('applicants_resume_upload');
                    }
                break;
                
                /*
                *   Operation to check if user has an uploaded resume.
                *   Main Route: "/candidates/profile/*"
                *
                *   Associated Controller and Action.
                *   Controller => Candidatesdashboard, Action => profile.
                */
                case 'user_resume_checker':
                    
                    //If User has Applicant Role.
                    if ($this->request->session()->read('Auth.User')['role'] == 'Applicant') {

                        //Load Resumes Model.
                        $this->loadModel('Resumes');
                        
                        //Count if user's has resume.
                        $resume_count = $this->Resumes->find()
                            ->where(['user_id' => $this->request->session()->read('Auth.User')['id']])
                            ->count();
                        
                        //echo String vals.
                        if ($resume_count > 0) {
                            echo 1;
                        } else {
                            echo 0;
                        }
                    }
                break;
                
                /*
                *   Operation to print out user's resume details.
                *   Main Route: "/candidates/profile/*"
                *
                *   Associated Controller and Action.
                *   Controller => Candidatesdashboard, Action => profile.
                */
                case 'user_resume_load':

                    //If User has Applicant Role.
                    if ($this->request->session()->read('Auth.User')['role'] == 'Applicant') {

                        //Load Resume Model.
                        $this->loadModel('Resumes');
                        
                        //Query out to get user's resume details.
                        $resumes = $this->Resumes->find()
                            ->select(['id', 'orig_filename', 'date'])
                            ->where(['user_id' => $this->request->session()->read('Auth.User')['id']]);
                        
                        //To be append.
                        $id = '';

                        //To be append.
                        $orig_filename= '';

                        //To be append.
                        $date = '';
                        
                        //Loop to get results.
                        foreach ($resumes as $resume) {

                            //Append Value.
                            $id .= $resume['id'];

                            //Append Value.
                            $orig_filename .= $resume['orig_filename'];

                            //Append Value.
                            $date .= $resume['date'];
                        }
                        
                        //Ajax Response.
                        echo $id . '|' . $orig_filename . '|' . 'Uploaded on ' . $this->globalworks->datetimetodate($date);
                    }
                break;
                
                /*
                *   Operation to delete user's resume.
                *   Main Route: "/candidates/profile/*"
                *
                *   Associated Controller and Action.
                *   Controller => Candidatesdashboard, Action => profile.
                */
                case 'delete_resume':
                    
                    //If User has Applicant Role.
                    if ($this->request->session()->read('Auth.User')['role'] == 'Applicant') {
                        $this->loadModel('Resumes');
                        
                        //Query out to get user's record.
                        $resumes = $this->Resumes->find()
                            ->select(['id', 'filename'])
                            ->where(['user_id' => $this->request->session()->read('Auth.User')['id']]);
                        
                        //ID Value to be append.
                        $id = '';
                        
                        //Filename Value to be append.
                        $filename = '';
                        
                        //Loop Results to get user's column ID and Filename.
                        foreach ($resumes as $res) {
                            
                            //Appen ID.
                            $id .= $res['id'];
                                
                            //Append Filename.
                            $filename .= $res['filename'];
                        }
                        
                        //Path to resumes directory.
                        $dir = './resumes/';
                        
                        //Check if user's resume is exists. If exists delete it.
                        if (file_exists($dir . $filename)) {
                            
                            //Delete File.
                            if (unlink($dir . $filename)) {
                                
                                //Get User's Column Record via ID.
                                $resume = $this->Resumes->get($id);
                                
                                //Delete Record.
                                if ($this->Resumes->delete($resume)) {
                                    echo 'ok';
                                }
                            }
                        }
                    }
                break;

                /*
                *   Operation for employers to view candidates information in search results page.
                *   Main Route: "/employers/search/*"
                *
                *   Associated Controller and Action.
                *   Controller => Employersdashboard, Action => search.
                */
                case 'candidate_show_info':
                    
                    //If user has Employer role.
                    if ($this->request->session()->read('Auth.User')['role'] == 'Employer') {

                        //Load Profiles Model.
                        $this->loadModel('Profiles');

                        //Load User_log_msg Model.
                        $this->loadModel('User_log_msg');

                        //Count result to check if user_id is exists.
                        $users_count = $this->Profiles->find()
                            ->where(['user_id' => $this->request->data('id')])
                            ->count();
                        
                        //Check if requested user_id is exists.
                        if ($users_count > 0) {

                            //Query out to get Candidate's Info.
                            $users = $this->Profiles->find()
                                ->select([
                                    'users.id',
                                    'users.firstname',
                                    'users.lastname',
                                    'users.gender',
                                    'users.birthday',
                                    'users.address',
                                    'users.region',
                                    'users.country',
                                    'users.contact_number',
                                    'profiles.nickname',
                                    'profiles.job_title',
                                    'profiles.skills',
                                    'profiles.desire_salary',
                                    'profiles.education',
                                    'profiles.experience',
                                    'profiles.employment_status',
                                    'profiles.skype_id',
                                    'profiles.profile_picture',
                                    'resumes.id',
                                    'resumes.orig_filename',
                                ])
                                ->contain(['Users', 'Resumes'])
                                ->where(['profiles.user_id' => $this->request->data('id')])
                                ->andWhere(['users.role' => 'Applicant']);

                            //Query out to get Candidate's User_log_msg Info.
                            //This will prevent Employers to make multiple invites in one day.
                            $has_user_log_msg = $this->User_log_msg->find()
                                ->where(['user_id' => $this->request->session()->read('Auth.User')['id']]) //Employer's User ID.
                                ->andWhere(['job_inv' => $this->request->data('id')]) //Candidate's User ID.
                                ->count();
                            
                            //Load Custom Template.
                            $this->autoRender = true;
                            $this->viewBuilder()->template('employers_candidate_view');

                            //Render results to view.
                            foreach ($users as $user) {
                                $this->set('user_id', $this->request->data('user_id'));
                                $this->set('candidate_id', $user['users']['id']);
                                $this->set('firstname', $user['users']['firstname']);
                                $this->set('lastname', $user['users']['lastname']);
                                $this->set('gender', $user['users']['gender']);
                                $this->set('address', $user['users']['address']);
                                $this->set('region', $user['users']['region']);
                                $this->set('country', $user['users']['country']);
                                $this->set('contact_number', $user['users']['contact_number']);
                                $this->set('nickname', $user['profiles']['nickname']);
                                $this->set('job_title', $user['profiles']['job_title']);
                                $this->set('skills', $user['profiles']['skills']);
                                $this->set('desire_salary', $user['profiles']['desire_salary']);
                                $this->set('education', $user['profiles']['education']);
                                $this->set('experience', $user['profiles']['experience']);
                                $this->set('employment_status', $user['profiles']['employment_status']);
                                $this->set('skype_id', $user['profiles']['skype_id']);
                                $this->set('profile_picture', $user['profiles']['profile_picture']);
                                $this->set('resume_id', $user['resumes']['id']);
                                $this->set('orig_filename', $user['resumes']['orig_filename']);
                            }

                            $this->set('has_user_log_msg', $has_user_log_msg);
                        } 
                    }
                break;
                
                /*
                *   Operation to view user's invitation page.
                *   Main Route: "/employers/profile/*"
                *
                *   Associated Controller and Action.
                *   Controller => Employersdashboard, action => search.
                */
                case 'employers_job_invitation':

                    //If User has Employer role.
                    if ($this->request->session()->read('Auth.User')['role'] == 'Employer') {

                        //Load Custom Template.
                        $this->autoRender = true;
                        $this->viewBuilder()->template('employers_job_invitation');

                        //Load Home Model.
                        $this->loadModel('Profiles');

                        //Query out to get Candidate's Details.
                        $users = $this->Profiles->find()
                            ->select(['users.firstname', 'users.lastname', 'profiles.profile_picture'])
                            ->contain(['Users'])
                            ->where(['profiles.user_id' => $this->request->data('id')]);

                        //Loop results and render to view.
                        foreach ($users as $user) {
                            $this->set("id", $this->request->data('id'));
                            $this->set('firstname', $user['users']['firstname']);
                            $this->set('lastname', $user['users']['lastname']);
                            $this->set('profile_picture', $user['profiles']['profile_picture']);
                        }
                    }
                break;
                
                /*
                *   Operation to send job invitation.
                *   Main Route: "/employers/profile/*"
                *
                *   Associated Controller and Action.
                *   Controller => Employersdashboard, action => search.
                */
                case 'send_job_invitation':
                    
                    //If User has Employer role.
                    if ($this->request->session()->read('Auth.User')['role'] == 'Employer') {

                        //Load Home Model.
                        $this->loadModel('Home');

                        //Query out to get Sender Data.
                        $users = $this->Home->find()
                            ->select(['email', 'firstname', 'lastname'])
                            ->where(['id' => $this->request->session()->read('Auth.User')['id']]);

                        //To be append.
                        $firstname = '';

                        //To be append.
                        $lastname = '';

                        //Loop to get Firstname and Lastname.
                        foreach ($users as $user) {

                            //Append Firstname.
                            $firstname .= $user['firstname'];
                            

                            //Append Lastname.
                            $lastname .= $user['lastname'];
                        }

                        //Create Subject.
                        $subject = $firstname . ' ' . $lastname . ' invites you to be part of their business.';
                        
                        //Load Messages Model.
                        $this->loadModel('Messages');

                        /*
                        *   Send Message to the User.
                        */

                        //New Entity.
                        $messages1 = $this->Messages->newEntity();

                        //Append New Entity.
                        $new_entity1 = [
                            'user_id' => $this->request->data('id'),
                            'sender_id' => $this->request->session()->read('Auth.User')['id'],
                            'unread' => 1,
                            'sent_item' => 0,
                            'message_type' => 'job_invitation',
                            'subject' => $subject,
                            'message' => $this->globalworks->filter_description($this->request->data('message')),
                            'date' => date( "Y-m-d H:i:s" )
                        ];

                        $messages1 = $this->Messages->patchEntity($messages1, $new_entity1);

                        //Save Message.

                        if ($this->Messages->save($messages1)) {

                            /*
                            *   Create Log Record.
                            */

                            //User's Fullname.
                            $fullname = $this->request->session()->read('Auth.User')['firstname'] . ' ' . $this->request->session()->read('Auth.User')['lastname'];

                            //User's Email Address.
                            $email = $this->request->session()->read('Auth.User')['email'];

                            //Create Log Message.
                            $log_msg = $fullname . ' (' . $email . ') send job invitation to ' . $this->globalworks->getfirstname($this->request->data('id')) . ' ' . $this->globalworks->getlastname($this->request->data('id') . '.');

                            //Save Log.
                            $this->globalworks->_log('job_invitation_log', $log_msg);
                            
                            
                            /*
                            *   Send Email Message.
                            */

                            //Query out to get recipent few information.
                            $email_recipeints = $this->Home->find()
                                ->select(['email'])
                                ->where(['id' => $this->request->data('id')]);

                            //To be append.
                            $e_email = '';

                            //Loop results.
                            foreach ($email_recipeints as $email_recipeint) {

                                //Append email.
                                $e_email .= $email_recipeint['email'];
                            }

                            $e_subject = 'You have a job invitation.';

                            $e_message = '
                                <p style="text-align: center; font-size: 20px;">You have a job invitation from ' . $firstname . ' ' . $lastname . '.</p>
                                <div style="text-align: center; margin-top: 30px;">
                                <a style="border: 1px solid red; background-color: red; color: #fff; padding: 10px; tex-align: center; text-decoration: none;" href="' . $this->globalworks->_url . '/login">Check Now</a>
                                </div>
                            ';

                            $e_email = [
                                'from' => [$this->globalworks->_server_email_address, 'Globalworks'],
                                'html' => true,
                                'subject' => $e_subject,
                                'to' => $e_email,
                                'message' => $e_message,
                            ];

                            //Send Email Message.
                            $this->globalworks->mail($e_email);
                        }

                        /*
                        *   Create sent items for the sender.
                        */

                        //Get Recipent firstname and lastname.
                        $recipients = $this->Home->find()
                            ->select(['email', 'firstname', 'lastname'])
                            ->where(['id' => $this->request->data('id')]);

                        //Recipient firstname to append.
                        $r_fname = '';

                        //Recipent lastname to append.
                        $r_lname = '';

                        //Recipent email to append.
                        $r_email = '';

                        //Loop results.
                        foreach ($recipients as $recipient) {

                            //Append recipent email.
                            $r_email .= $recipient['email'];

                            //Append recipient firstname
                            $r_fname .= $recipient['firstname'];

                            //Append recipient lastname.
                            $r_lname .= $recipient['lastname'];
                        }

                        //Subject.
                        $subject = 'You invited ' . $r_fname . ' ' . $r_lname . ' to your business.';

                        //New Entity.
                        $messages2 = $this->Messages->newEntity();
                        
                        //Append New Entity.
                        $new_entity2 = [
                            'user_id' => $this->request->data('id'),
                            'sender_id' => $this->request->session()->read('Auth.User')['id'],
                            'unread' => 0,
                            'sent_item' => 1,
                            'message_type' => 'job_invitation',
                            'subject' => $subject,
                            'message' => $this->request->data('message'),
                            'date' => date( "Y-m-d H:i:s" )
                        ];

                        //Patch Entity.
                        $messages2 = $this->Home->patchEntity($messages2, $new_entity2);

                        //Save Message.
                        $this->Messages->save($messages2);

                        /*
                        *   Create Log Message. This is for blocking User for inviting multiple on 1 day.
                        */

                        //Load User_log_msg Model.
                        $this->loadModel('User_log_msg');

                        //New Entity.
                        $user_log_msg = $this->User_log_msg->newEntity();

                        //Appen New Entity.
                        $new_entity3 = [
                            'user_id' => $this->request->session()->read('Auth.User')['id'],
                            'job_inv' => $this->request->data('id'),
                            'microtime' => microtime(true),
                        ];

                        //PatchEntity.
                        $user_log_msg = $this->User_log_msg->patchEntity($user_log_msg, $new_entity3);

                        //Save Log.
                        $this->User_log_msg->save($user_log_msg);

                        echo 1;
                    }
                break;
                
                /*
                *   Operation to view employer's unread messages.
                *   Main Route: "/employers/profile/*"
                *
                *   Associated Controller and Action.
                *   Controller => Employersdashboard, action => messages.
                */
                case 'employer_msg_unread':
                    
                    //If User has Employers role.
                    if ($this->request->session()->read('Auth.User')['role'] == 'Employer') {

                        //Load Messages Model.
                        $this->loadModel('Messages');

                        //Set Custom Template.
                        $this->autoRender = true;
                        $this->viewBuilder()->template('users_messages');

                        //Query out to get user's unread messages.
                        $messages = $this->Messages->find()
                            ->select(['id', 'unread', 'message_type', 'subject', 'date'])
                            ->where(['user_id', $this->request->session()->read('Auth.User')['id']])
                            ->andWhere(['unread' => 1])
                            ->order(['id' => 'DESC']);

                        $this->set('name', 'Unread');
                        $this->set('count', $this->globalworks->unread_messages_count($this->request->session()->read('Auth.User')['id']));
                        $this->set('messages', $messages);
                    }
                break;
                
                /*
                *   Operation to view candidate's unread messages.
                *   Main Route: "/candidates/profile/*"
                *
                *   Associated Controller and Action.
                *   Controller => Candidates, action => messages.
                */
                case 'candidate_msg_unread':
                    
                    //If User has Applicant role.
                    if ($this->request->session()->read('Auth.User')['role'] == 'Applicant') {
                        
                        //Load Messages Model.
                        $this->loadModel('Messages');

                        //Set Custom Template.
                        $this->autoRender = true;
                        $this->viewBuilder()->template('users_messages');

                        //Query out to get user's unread messages.
                        $messages = $this->Messages->find()
                            ->select(['id', 'unread', 'message_type', 'subject', 'date'])
                            ->where(['user_id', $this->request->session()->read('Auth.User')['id']])
                            ->andWhere(['unread' => 1])
                            ->order(['id' => 'DESC']);

                        $this->set('name', 'Unread');
                        $this->set('count', $this->globalworks->unread_messages_count($this->request->session()->read('Auth.User')['id']));
                        $this->set('messages', $messages);
                   
                    }
                break;
                
                /*
                *   Operation to view employer's job application.
                *   Main Route: "/employers/profile/*"
                *
                *   Associated Controller and Action.
                *   Controller => Employersdashboard, action => messages.
                */
                case 'employer_msg_job_application':

                    //If User has Employer role.
                    if ($this->request->session()->read('Auth.User')['role'] == 'Employer') {

                        //Load Messages Model.
                        $this->loadModel('Messages');

                        //Set Custom Template.
                        $this->autoRender = true;
                        $this->viewBuilder()->template('users_messages');
                        
                        //Query out to get user's unread messages.
                        $messages = $this->Messages->find()
                            ->select(['id', 'unread', 'message_type', 'subject', 'date'])
                            ->where(['user_id' => $this->request->session()->read('Auth.User')['id']])
                            ->andWhere(['message_type' => 'job_application '])
                            ->andWhere(['sent_item !=' => 1])
                            ->order(['id' => 'DESC']);
                        
                        $this->set('name', 'Job Application');
                        $this->set('count', $this->globalworks->ja_unread_msgs_count($this->request->session()->read('Auth.User')['id']));
                        $this->set('messages', $messages);
                    }
                break;
                
                /*
                *   Operation to view candidate's Admin Messages.
                *   Main Route: "/candidates/profile/*"
                *
                *   Associated Controller and Action.
                *   Controller => Candidates, action => messages.
                */
                case 'candidate_msg_from_admin':
                    
                    //If user has Applicant role.
                    if ($this->request->session()->read('Auth.User')['role'] == 'Applicant') {
                        
                        //Load Messages Model.
                        $this->loadModel('Messages');
                        
                        //Set Custom Template.
                        $this->autoRender = true;
                        $this->viewBuilder()->template('users_messages');
                        
                        //Query out to get user's announcement messages.
                        $messages = $this->Messages->find()
                            ->where(['user_id' => $this->request->session()->read('Auth.User')['id']])
                            ->select(['id', 'unread', 'message_type', 'subject', 'date'])
                            ->andWhere(['message_type' => 'employer_msg_from_admin'])
                            ->andWhere(['sent_item !=' => 1])
                            ->order(['id' => 'DESC']);
                        
                        $this->set('name', 'Admin Messages');
                        $this->set('count', $this->globalworks->from_admin_unread_msg_count($this->request->session()->read('Auth.User')['id']));
                        $this->set('messages', $messages);
                    }
                break;
                
                /*
                *   Operation to view candidate's job invitation.
                *   Main Route: "/candidates/profile/*"
                *
                *   Associated Controller and Action.
                *   Controller => Candidates, action => messages.
                */
                case 'candidate_msg_job_invitation':
                    
                    //If user has Applicant role.
                    if ($this->request->session()->read('Auth.User')['role'] == 'Applicant') {
                        
                        //Load Messages Model.
                        $this->loadModel('Messages');
                        
                        //Set Custom Template.
                        $this->autoRender = true;
                        $this->viewBuilder()->template('users_messages');
                        
                        //Query out to get user's unread messages.
                        $messages = $this->Messages->find()
                            ->select(['id', 'unread', 'message_type', 'subject', 'date'])
                            ->where(['user_id' => $this->request->session()->read('Auth.User')['id']])
                            ->andWhere(['message_type' => 'job_invitation'])
                            ->andWhere(['sent_item !=' => 1])
                            ->order(['id' => 'DESC']);
                        
                        $this->set('name', 'Job Invitation');
                        $this->set('count', $this->globalworks->ji_unread_msgs_count($this->request->session()->read('Auth.User')['id']));
                        $this->set('messages', $messages);
                    }
                break;
                
                /*
                *   Operation to view employer's Admin Messages.
                *   Main Route: "/employers/profile/*"
                *
                *   Associated Controller and Action.
                *   Controller => Employersdhasboard, action => messages.
                */
                case 'employer_msg_from_admin':

                    //If User has Employer Role.
                    if ($this->request->session()->read('Auth.User')['role'] == 'Employer') {

                        //Load Messages Model.
                        $this->loadModel('Messages');

                        //Set Custom Template.
                        $this->autoRender = true;
                        $this->viewBuilder()->template('users_messages');

                        //Query out to get user's announcement messages.
                        $messages = $this->Messages->find()
                            ->where(['user_id' => $this->request->session()->read('Auth.User')['id']])
                            ->andWhere(['message_type' => 'employer_msg_from_admin'])
                            ->andWhere(['sent_item !=' => 1])
                            ->order(['id' => 'DESC']);

                        $this->set('name', 'Admin Messages');
                        $this->set('count', $this->globalworks->from_admin_unread_msg_count($this->request->session()->read('Auth.User')['id']));
                        $this->set('messages', $messages);
                    }
                break;
                
                /*
                *   Operation to view candidate's sent items.
                *   Main Route: "/candidates/profile/*"
                *
                *   Associated Controller and Action.
                *   Controller => Candidates, action => messages.
                */
                case 'candidate_msg_sent_items':
                    
                    //If User has Applicant Role.
                    if ($this->request->session()->read('Auth.User')['role'] == 'Applicant') {
                        
                        //Load Messages Model.
                        $this->loadModel('Messages');
                        
                        //Set Custom Template.
                        $this->autoRender = true;
                        $this->viewBuilder()->template('users_messages');
                        
                        //Queryout to get user's sent items.
                        $messages = $this->Messages->find()
                            ->select(['id', 'unread', 'message_type', 'subject', 'date'])
                            ->where(['sender_id' => $this->request->session()->read('Auth.User')['id']])
                            ->andWhere(['sent_item' => 1])
                            ->order(['id' => 'DESC']);
                        
                        $this->set('name', 'Sent Items');
                        $this->set('count', $this->globalworks->candidate_sent_items_count($this->request->session()->read('Auth.User')['id']));
                        $this->set('messages', $messages);
                    }
                break;
                
                /*
                *   Operation to view employer's sent items.
                *   Main Route: "/employers/profile/*"
                *
                *   Associated Controller and Action.
                *   Controller => Employersdashboard, action => messages.
                */
                case 'employer_msg_sent_items':

                    //If User has Employer Role.
                    if ($this->request->session()->read('Auth.User')['role'] == 'Employer') {

                        //Load Messages Model.
                        $this->loadModel('Messages');
                        
                        //Set Custom Template.
                        $this->autoRender = true;
                        $this->viewBuilder()->template('users_messages');

                        //Query out to get user's unread messages.
                        $messages = $this->Messages->find()
                            ->select(['id', 'unread', 'message_type', 'subject', 'date'])
                            ->where(['sender_id' => $this->request->session()->read('Auth.User')['id']])
                            ->andWhere(['sent_item' => 1])
                            ->order(['id' => 'DESC']);

                        $this->set('name', 'Sent Items');
                        $this->set('count', $this->globalworks->employer_sent_items($this->request->session()->read('Auth.User')['id']));
                        $this->set('messages', $messages);
                    }
                break;
                
                /*
                *   Operation for candidate read message.
                *   Main Route: "/candidates/profile/*"
                *
                *   Associated Controller and Action.
                *   Controller => Candidates, action => messages.
                */
                case 'candidate_read_message':

                    //If User has Applicant role.
                    if ($this->request->session()->read('Auth.User')['role'] == 'Applicant') {
                        
                        //Load Messages Model.
                        $this->loadModel('Messages');
                        
                        //Count to check if message are sent items.
                        $count_msg = $this->Messages->find()
                            ->where(['id' => $this->request->data('id')])
                            ->andWhere(['sent_item' => 1])
                            ->count();
                        
                        //If messages are sent items.
                        if ($count_msg) {
                            
                            //Load Custom Template.
                            $this->autoRender = true;
                            $this->viewBuilder()->template('users_message_sent_item');
                            
                            //Query out to get sent items.
                            $messages = $this->Messages->find()
                                ->select(['user_id', 'subject', 'message', 'date'])
                                ->where(['id' => $this->request->data('id')])
                                ->andWhere(['sender_id' => $this->request->session()->read('Auth.User')['id']])
                                ->andWHere(['sent_item' => 1]);
                            
                            //To be append.
                            $recipient_id = '';
                            
                            //Loop resulkts and render to view.
                            foreach ($messages as $message) {
                                $this->set('subject', $message['subject']);
                                $this->set('message', $message['message']);
                                $this->set('date', $message['date']);
                                
                                //Append Value.
                                $recipient_id = $message['user_id'];
                            }
                            
                            /*
                            *   Get Recipient Info.
                            */
                            
                            //Load Home Model.
                            $this->loadModel('Home');
                            
                            //Query out to get recipient info.
                            $recipients = $this->Home->find()
                                ->select(['home.firstname', 'home.lastname', 'profiles.profile_picture'])
                                ->contain(['Profiles'])
                                ->where(['home.id' => $recipient_id]);
                            
                            //Loop results and render to view.
                            foreach ($recipients as $recipient) {
                                $this->set('firstname', $recipient['home']['firstname']);
                                $this->set('lastname', $recipient['home']['lastname']);
                                $this->set('profile_picture', $recipient['profiles']['profile_picture']);
                            }
                        } else {
                            
                            //Load Custom Template.
                            $this->autoRender = true;
                            $this->viewBuilder()->template('users_message');

                            //To be Append.
                            $sender_id = '';

                            //Query out to get user's message.
                            $messages = $this->Messages->find()
                                ->select(['sender_id', 'subject', 'message', 'date'])
                                ->where(['id' => $this->request->data('id')])
                                ->andWhere(['user_id' => $this->request->session()->read('Auth.User')['id']]);

                            //Loop and render to view.
                            foreach ($messages as $message) {
                                $this->set('subject', $message['subject']);
                                $this->set('message', $message['message']);
                                $this->set('date', $message['date']);

                                //Append Sender ID.
                                $sender_id .= $message['sender_id'];
                            }

                            /*
                            *   Get Sender Info.
                            */

                            //Load Home Model.
                            $this->loadModel('Home');

                            //Query out to get sender info.
                            $senders = $this->Home->find()
                                ->select(['home.firstname', 'home.lastname', 'profiles.profile_picture'])
                                ->contain(['Profiles'])
                                ->where(['home.id' => $sender_id]);


                            //Loop results and render to view.
                            foreach ($senders as $sender) {
                                $this->set('firstname', $sender['home']['firstname']);
                                $this->set('lastname', $sender['home']['lastname']);
                                $this->set('profile_picture', $sender['profiles']['profile_picture']);
                            }

                            /*
                            *   Update message to unread => 0.
                            */
                            //Get Message Column.
                            $msg = $this->Messages->get($this->request->data('id'));

                            //Update unread column.
                            $msg->unread = 0;

                            //Save.
                            $this->Messages->save($msg);
                        }
                    }
                break;
                
                /*
                *   Operation for empoyer read message.
                *   Main Route: "/employers/profile/*"
                *
                *   Associated Controller and Action.
                *   Controller => Employersdashboard, action => messages.
                */
                case 'employer_read_message':

                    //If User has Employer role.
                    if ($this->request->session()->read('Auth.User')['role'] == 'Employer') {
                        
                        //Load Messages Model.
                        $this->loadModel('Messages');

                        //Count to check if message is sent item or not.
                        $count_msg = $this->Messages->find()
                            ->where(['id' => $this->request->data('id')])
                            ->andWhere(['sent_item' => 1])
                            ->count();

                        //If Message is sent item.
                        if ($count_msg) {

                            //Load Custom Template.
                            $this->autoRender = true;
                            $this->viewBuilder()->template('users_message_sent_item');
                            
                            //Query out to get sent items.
                            $messages = $this->Messages->find()
                                ->select(['user_id', 'subject', 'message', 'date'])
                                ->where(['id' => $this->request->data('id')])
                                ->andWhere(['sender_id' => $this->request->session()->read('Auth.User')['id']])
                                ->andWhere(['sent_item' => 1]);

                            //To be Append.
                            $recipient_id = '';

                            //Loop results and render to view.
                            foreach ($messages as $message) {
                                $this->set('subject', $message['subject']);
                                $this->set('message', $message['message']);
                                $this->set('date', $message['date']);

                                //Append Value.
                                $recipient_id .= $message['user_id'];
                            }

                            /*
                            *   Get Recipient Info.
                            */
                            
                            //Load Home Model.
                            $this->loadModel('Home');

                            //Query out to get recipient info.
                            $recipients = $this->Home->find()
                                ->select(['home.firstname', 'home.lastname', 'profiles.profile_picture'])
                                ->contain(['Profiles'])
                                ->where(['home.id' => $recipient_id]);

                            //Loop results and render to view.
                            foreach ($recipients as $recipient) {
                                $this->set('firstname', $recipient['home']['firstname']);
                                $this->set('lastname', $recipient['home']['lastname']);
                                $this->set('profile_picture', $recipient['profiles']['profile_picture']);
                            }
                        } else {

                            //Load Custom Template.
                            $this->autoRender = true;
                            $this->viewBuilder()->template('users_message');

                            //To be Append.
                            $sender_id = '';

                            //Query out to get user's message.
                            $messages = $this->Messages->find()
                                ->select(['sender_id', 'subject', 'message', 'date'])
                                ->where(['id' => $this->request->data('id')])
                                ->andWhere(['user_id' => $this->request->session()->read('Auth.User')['id']]);

                            //Loop and render to view.
                            foreach ($messages as $message) {
                                $this->set('subject', $message['subject']);
                                $this->set('message', $message['message']);
                                $this->set('date', $message['date']);

                                //Append Sender ID.
                                $sender_id .= $message['sender_id'];

                                //Append sent_item.
                                $send_item = $message['sent_item'];
                            }

                            /*
                            *   Get Sender Info.
                            */
                            
                            //Load Home Model.
                            $this->loadModel('Home');

                            //Query out to get sender info.
                            $senders = $this->Home->find()
                                ->select(['home.firstname', 'home.lastname', 'profiles.profile_picture'])
                                ->contain(['Profiles'])
                                ->where(['home.id' => $sender_id]);


                            //Loop results and render to view.
                            foreach ($senders as $sender) {
                                $this->set('firstname', $sender['home']['firstname']);
                                $this->set('lastname', $sender['home']['lastname']);
                                $this->set('profile_picture', $sender['profiles']['profile_picture']);
                            }

                            /*
                            *   Update message to unread => 0.
                            */
                            //Get Message Column.
                            $msg = $this->Messages->get($this->request->data('id'));

                            //Update unread column.
                            $msg->unread = 0;

                            //Save.
                            $this->Messages->save($msg);
                        }
                    }
                break;
                
                /*
                *   Operation to delete message.
                *   Main Route: "/candidates/profile/*"
                */
                case 'user_message_delete':
                    
                    //If User has Login Session.
                    if ($this->request->session()->read('Auth.User')) {
                        
                        //Load Messages Model.
                        $this->loadModel('Messages');
                        
                        //Get Mesage Column Record.
                        $msg = $this->Messages->get($this->request->data('id'));
                        
                        //Delete Message.
                        if ($this->Messages->delete($msg)) {
                            echo 'ok';
                        }
                    }
                break;
                
                /*
                *   Operation to view Candidate job application page.
                *   Main Route: "/candidates/profile/*"
                *
                *   Associated Controller and Action.
                *   Controller => Candidates, action => messages.
                */
                case 'candidate_application':
                    
                    //Load Jobposts Model.
                    $this->loadModel('Jobposts');
                    
                    //Count if Jobpost ID is exists.
                    $jobposts_count = $this->Jobposts->find()
                        ->where(['id' => $this->request->data('id')])
                        ->count();
                    
                    //If User has Applicant role and Jobposts Count not equal to zero.
                    if ($this->request->session()->read('Auth.User')['role'] == 'Applicant' && $jobposts_count !== 0) {
                        
                        //Load Custom Template.
                        $this->autoRender = true;
                        $this->viewBuilder()->template('candidate_job_application');
                        
                        //Queryout to get some columns.
                        $jobposts = $this->Jobposts->find()
                            ->select(['users.firstname', 'users.lastname', 'jobposts.company_logo', 'jobposts.company_name', 'jobposts.job_title'])
                            ->contain(['Users'])
                            ->where(['jobposts.id' => $this->request->data('id')]);
                        
                        //Loop results and render to view.
                        foreach ($jobposts as $post) {
                            $this->set('firstname', $post['users']['firstname']);
                            $this->set('lastname', $post['users']['lastname']);
                            $this->set('company_logo', $post['jobposts']['company_logo']);
                            $this->set('company_name', $post['jobposts']['company_name']);
                            $this->set('job_title', $post['jobposts']['job_title']);
                        }
                    }
                break;
                
                /*
                *   Operation to candidate send application to the employer.
                *   Main Route: "/candidates/search/*"
                *
                *   Associated Controller and Action.
                *   Controller => Candidates, action => search_results.
                */
                case 'candidate_send_application':
                    
                    //If user has Applicant role.
                    if ($this->request->session()->read('Auth.User')['role'] == 'Applicant') {
                        
                        //Load Messages Model.
                        $this->loadModel('Messages');
                        
                        //Load Home Model.
                        $this->loadModel('Home');
                        
                        //Load Jobposts Model.
                        $this->loadModel('Jobposts');
                        
                        //Load User_log_msg Model.
                        $this->loadModel('User_log_msg');
                        
                        /*
                        *   Get Candidate Info.
                        */
                        
                        //Query out to get Candidate Info.
                        $candidates = $this->Home->find()
                            ->select(['firstname', 'lastname'])
                            ->where(['id' => $this->request->session()->read('Auth.User')['id']]);
                        
                        //Firstname value to append.
                        $firstname = '';
                        
                        //Lastname value to append.
                        $lastname = '';
                        
                        //Loop Results to get firstname and lastname.
                        foreach ($candidates as $candidate) {
                            $firstname .= $candidate['firstname'];
                            $lastname .= $candidate['lastname'];
                        }
                        
                        //Query out to get Jobposts ID's Info.
                        $jobposts = $this->Jobposts->find()
                            ->select(['jobposts.user_id', 'jobposts.company_name', 'jobposts.job_title', 'users.email'])
                            ->contain(['Users'])
                            ->where(['jobposts.id' => $this->request->data('jobpost_id')]);
                        
                        //Employer's User ID to append.\
                        $user_id = '';
                        
                        //company_name value to append.
                        $company_name = '';
                        
                        //job_title to append/
                        $job_title = '';

                        //Employer's Email to append.
                        $email = '';
                        
                        //Loop to get company_name and job_title.
                        foreach ($jobposts as $post) {
                            $user_id .= $post['jobposts']['user_id'];
                            $company_name .= $post['jobposts']['company_name'];
                            $job_title .= $post['jobposts']['job_title'];
                            $email .= $post['users']['email'];
                        }
                        
                        //Create Subject.
                        $subject = $firstname . ' ' .$lastname . ' wants to be part of your company.';
                        
                        //Create Default Message.
                        $d_msg = '
                            <div class="intro2"><a href="javascript:" onclick="javascript: viewCandidate(' . $this->request->session()->read('Auth.User')['id'] . ')">' . $firstname . ' ' . $lastname . '</a>  
                            want\'s to be part of your company ('. $company_name .') with a job title of '. $job_title .'.</div>
                            <div class="link"><span onclick="javascript: viewCandidate(' . $this->request->session()->read('Auth.User')['id'] . ')">View '. $firstname . ' ' . $lastname .' Information</span></div>
                        ';
                        
                        //If Candidate has message.
                        if ($this->request->data('message')) {
                            $d_msg .= '
                                <div class="c_head">'. $firstname .' '. $lastname .' has a message.</div>
                                <div class="c_msg">
                                    '. $this->globalworks->filter_description($this->request->data('message')) .'
                                </div>
                            ';
                        }
                        
                        /*
                        *   Save Message to message table.
                        */
                        
                        //New Entity.
                        $messages = $this->Messages->newEntity();
                        
                        //Append Entity.
                        $new_entity = [
                            'user_id' => $user_id,
                            'sender_id' => $this->request->session()->read('Auth.User')['id'],
                            'unread' => 1,
                            'sent_item' => 0,
                            'message_type' => 'job_application',
                            'subject' => $subject,
                            'message' => $d_msg,
                            'date' => date( "Y-m-d H:i:s" ),
                        ];
                        
                        //Patch Entity.
                        $messages = $this->Messages->patchEntity($messages, $new_entity);
                        
                        //Save Message.
                        if ($this->Messages->save($messages)) {
                            
                            
                            /*
                            *   Create Log Record.
                            */

                            //User's Fullname.
                            $fullname = $this->request->session()->read('Auth.User')['firstname'] . ' ' . $this->request->session()->read('Auth.User')['lastname'];

                            //User's Email Address.
                            $email = $this->request->session()->read('Auth.User')['email'];

                            //Create Log Message.
                            $log_msg = $fullname . ' (' . $email . ') send job application to ' . $this->globalworks->getfirstname($user_id) . ' ' . $this->globalworks->getlastname($user_id) . '. <strong>Job Title:</strong> ' . $job_title . ' <strong>Company Name:</strong> ' . $company_name . '.';

                            //Save Log.
                            $this->globalworks->_log('job_application_log', $log_msg); //dito

                            
                            /*
                            *   Create Email Message.
                            */
                            $message = '
                                <p style="text-align: center; font-size: 20px;">' . $firstname . ' ' . $lastname . ' is applying to ' . $company_name . ',  ' . $job_title . '</p>
                                <div style="text-align: center; margin-top: 30px;">
                                    <a style="border: 1px solid red; background-color: red; color: #fff; padding: 10px; tex-align: center; text-decoration: none;" href="' . $this->globalworks->_url . '/login/employers">Check Now</a>
                                </div>
                            ';

                            $subject = 'A candidate wants to apply.';

                            $email = [
                                'from' => [$this->globalworks->_server_email_address, 'Globalworks'],
                                'subject' => $subject,
                                'html' => true,
                                'to' => $email,
                                'message' => $message,
                            ];

                            //Send Message.
                            $this->globalworks->mail($email);
                        }
                        
                        /*
                        *   Create Sent Item.
                        */
                        
                        //New Entity.
                        $messages2 = $this->Messages->newEntity();
                        
                        //Subject.
                        $subject = 'Your application in ' . $company_name . '.';
                        
                        $d_msg = '
                            <div class="intro2">You\'ve submitted an application to ' . $company_name . '</div>
                        ';
                        
                        if ($this->request->data('message')) {
                            $d_msg .= '
                                <div class="c_msg">
                                    '. $this->globalworks->filter_description($this->request->data('message')) .'
                                </div>
                            ';
                        }
                        
                        //Append Entity.
                        $new_entity2 = [
                            'user_id' => $user_id,
                            'sender_id' => $this->request->session()->read('Auth.User')['id'],
                            'unread' => 0,
                            'sent_item' => 1,
                            'message_type' => 'job_application',
                            'subject' => $subject,
                            'message' => $d_msg,
                            'date' => date( "Y-m-d H:i:s" ),
                        ];
                        
                        //Patch Entity.
                        $messages2 = $this->Messages->patchEntity($messages2, $new_entity2);
                        
                        //Save Message.
                        $this->Messages->save($messages2);
                        
                        /*
                        *   Create Log Message. This is for blocking User for inviting multiple on 1 day.
                        */
                        
                        //New Entity.
                        $messages3 = $this->User_log_msg->newEntity();
                        
                        //New Entity.
                        $new_entity3 = [
                            'user_id' => $this->request->session()->read('Auth.User')['id'],
                            'job_app' => $this->request->data('jobpost_id'),
                            'microtime' => microtime(true)
                        ];
                        
                        //Patch Entity.
                        $messages3 = $this->User_log_msg->patchEntity($messages3, $new_entity3);
                        
                        //Save Log.
                        $this->User_log_msg->save($messages3);
                        
                        echo 'ok';
                    }
                break;
                
                /*
                *   Operation for employer to view candidate's information.
                *   Main Route: "/employersdashboard/profile/*"
                *
                *   Associated Controller and Action.
                *   Controller => Employersdashboard, action => messages.
                */
                case 'employer_candidate_view':
                    
                    //If User has Employer Role.
                    if ($this->request->session()->read('Auth.User')['role'] == 'Employer') {
                        
                        //Load Home Model.
                        $this->loadModel('Home');

                        //Load Custom Template.
                        $this->autoRender = true;
                        $this->viewBuilder()->template('employer_candidate_view');

                        //Count to check if ID is exists.
                        $users_count = $this->Home->find()
                            ->where(['id' => $this->request->data('id')])
                            ->andWhere(['role' => 'Applicant'])
                            ->count();

                        //If ID is exists.
                        if ($users_count > 0) {

                            //Query out to get Candidate information.
                            $users = $this->Home->find()
                                ->select([
                                    'home.email',
                                    'home.firstname',
                                    'home.lastname',
                                    'home.gender',
                                    'home.birthday',
                                    'home.address',
                                    'home.region',
                                    'home.country',
                                    'home.contact_number',
                                    'profiles.profile_picture',
                                    'profiles.nickname',
                                    'profiles.job_title',
                                    'profiles.employment_status',
                                    'profiles.skills',
                                    'profiles.desire_salary',
                                    'profiles.education',
                                    'profiles.experience',
                                    'profiles.skype_id',
                                    'resumes.id',
                                    'resumes.filename',
                                    'resumes.orig_filename',
                                ])
                                ->contain(['Profiles', 'Resumes'])
                                ->where(['home.id' => $this->request->data('id')])
                                ->andWhere(['home.role' => 'Applicant']);

                            //Loop results and render to view.
                            foreach ($users as $user) {
                                $this->set('email', $user['home']['email']);
                                $this->set('firstname', $user['home']['firstname']);
                                $this->set('lastname', $user['home']['lastname']);
                                $this->set('employment_status', $user['profiles']['employment_status']);
                                $this->set('gender', $user['home']['gender']);
                                $this->set('birthday', $user['home']['birthday']);
                                $this->set('address', $user['home']['address']);
                                $this->set('region', $user['home']['region']);
                                $this->set('country', $user['home']['country']);
                                $this->set('contact_number', $user['home']['contact_number']);
                                $this->set('profile_picture', $user['profiles']['profile_picture']);
                                $this->set('nickname', $user['profiles']['nickname']);
                                $this->set('job_title', $user['profiles']['job_title']);
                                $this->set('skills', $user['profiles']['skills']);
                                $this->set('desire_salary', $user['profiles']['desire_salary']);
                                $this->set('education', $user['profiles']['education']);
                                $this->set('experience', $user['profiles']['experience']);
                                $this->set('skype_id', $user['profiles']['skype_id']);
                                $this->set('resume_id', $user['resumes']['id']);
                                $this->set('resume_filename', $user['resumes']['filename']);
                                $this->set('orig_filename', $user['resumes']['orig_filename']);
                            }
                        }
                    }
                break;
                
                /*
                * Response unread messages number.
                */
                case 'manage_unread_counts':

                    //If User has Login Session.
                    if ($this->request->session()->read('Auth.User')) {
                        echo $this->globalworks->display_unread_msgs_count();
                    }
                break;

                /*
                *   Operation to preview google map on Jobposts Create and Jobposts Edit.
                */
                case 'map_preview':

                    //Location.
                    $location = urlencode($this->request->data('location') . ' ' . $this->globalworks->country_to_str($this->request->data('country')));

                    echo '
                        <iframe
                            frameborder="0" style="border:0"
                            src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDg3sqfQCdmusqPP_M4j40R_xuIBpiUSG8
                            &q='. $location .'" allowfullscreen>
                        </iframe>
                    ';
                break;
                
                /*
                *   Employer Preview Jobposts before publish.
                */
                case 'jobpost_preview':
                    
                    $this->autoRender = true;
                    $this->viewBuilder()->template('jobpost_preview');

                    //Render values to the view.
                    $this->set('header_image', $this->request->data('header_image'));
                    $this->set('company_logo', $this->request->data('company_logo'));
                    $this->set('job_title', $this->request->data('job_title'));
                    $this->set('company_name', $this->request->data('company_name'));
                    $this->set('job_category', $this->request->data('job_category'));
                    $this->set('job_type', $this->request->data('job_type'));
                    $this->set('job_migration', $this->request->data('job_migration'));
                    $this->set('country', $this->request->data('country'));
                    $this->set('jobs_description', $this->request->data('jobs_description'));
                    $this->set('job_qualifications', $this->request->data('job_qualifications'));
                    $this->set('job_responisilities', $this->request->data('job_responisilities'));
                    $this->set('years_exp_start', $this->request->data('years_exp_start'));
                    $this->set('years_exp_end', $this->request->data('years_exp_end'));
                    $this->set('job_location', $this->request->data('job_location'));
                    $this->set('googglemap', $this->request->data('googglemap'));
                    $this->set('job_website', $this->request->data('job_website'));
                    $this->set('job_email', $this->request->data('job_email'));
                    $this->set('job_contact_number', $this->request->data('job_contact_number'));
                    $this->set('closing_date', $this->request->data('closing_date'));
                break;

                /*
                *   Publish or Save Jobpost.
                */
                case 'save_jobpost':

                    //Load Profiles Model.
                    $this->loadModel('Jobposts');

                    //To be Append.
                    $qualifications = '';

                    //To be set.
                    $responsibilities = '';
                    
                    //If Qualifications is not null or empty.
                    if ($this->request->data('job_qualifications')) {

                        //Set Value.
                        $qualifications = json_encode($this->request->data('job_qualifications'));
                    }
                    
                    //If Responsibilites is not null or empty.
                    if ($this->request->data('job_responisilities')) {

                        //Set Value.
                        $responsibilities = json_encode($this->request->data('job_responisilities'));
                    }
                    
                    //Set years Experience.
                    $exp = (!$this->request->data('years_exp_start')) ? $this->request->data('years_exp_start') : $this->request->data('years_exp_end') . ' to ' . $this->request->data('years_exp_end');
                    
                    //New Entity.
                    $jobpost = $this->Jobposts->newEntity();

                    //Append New Entity.
                    $new_entity = [
                        'user_id' => $this->request->session()->read('Auth.User')['id'],
                        'header_image' => $this->request->data('header_image'),
                        'company_logo' => $this->request->data('company_logo'),
                        'job_title' => $this->request->data('job_title'),
                        'company_name' => $this->request->data('company_name'),
                        'job_category' => $this->request->data('job_category'),
                        'job_type' => $this->request->data('job_type'),
                        'job_migration' => $this->request->data('job_migration'),
                        'country' => $this->request->data('country'),
                        'job_description' => $this->request->data('jobs_description'),
                        'job_qualifications' => $qualifications,
                        'job_responsibilities' => $responsibilities,
                        'job_years_experience' => $exp,
                        'job_location' => $this->request->data('job_location'),
                        'job_website' => $this->request->data('job_website'),
                        'job_email' => $this->request->data('job_email'),
                        'job_contact_number' => $this->request->data('job_contact_number'),
                        'googlemap' => $this->request->data('googglemap'),
                        'closing_date' => $this->request->data('closing_date'),
                        'created_at' => date( "Y-m-d H:i:s" ),
                        'modified_at' => date( "Y-m-d H:i:s" ),
                        'pos' => $this->globalworks->set_job_order_num(),
                        'googglemap' => $this->request->data('googlemap')
                    ];

                     //Patch Entity.
                     $jobpost = $this->Jobposts->patchEntity($jobpost, $new_entity);

                     //Save Jobpost.
                    if ($this->Jobposts->save($jobpost)) {
                        
                        /*
                        *   At this point. Move header image and company logo to the right dir.
                        *   If User did set image header or company logo.
                        */

                        //Load Jobheaderimg_tmp Model.
                        $this->loadModel('Jobheaderimg_tmp');
                        
                        //Load Companylogo_tmp Model.
                        $this->loadModel('Companylogo_tmp');
                        
                        //Path to job_header_tmp dir.
                        $header_tmp = './img/users/job_header_tmp/';

                        //Path to company_logo_tmp dir.
                        $logo_tmp = './img/users/company_logo_tmp/';

                        //Path to job_header dir.
                        $header_dir = './img/users/job_header/';

                        //Path to company_logo dir.
                        $logo_dir = './img/users/company_logo/';
                        
                        //If $this->request->data('header-img') File exists.
                        if ($this->request->data('header_image') && file_exists($header_tmp . $this->request->data('header-img'))) {

                            //Copy Image from the job_header_tmp to job_header.
                            if (copy($header_tmp . $this->request->data('header_image'), $header_dir . $this->request->data('header_image'))) {
                                
                                //Delete Image file from job_header_tmp.
                                if (unlink($header_tmp . $this->request->data('header_image'))) {

                                    //To be Append.
                                    $id = '';

                                    //Query out to get image record.
                                    $headers = $this->Jobheaderimg_tmp->find()
                                        ->where(['user_id' => $this->request->session()->read('Auth.User')['id']])
                                        ->andWhere(['filename' => $this->request->data('header_image')]);
                                    
                                    //Loop Result.
                                    foreach ($headers as $header) {

                                        //Append ID.
                                        $id .= $header['id'];
                                    }
                                    
                                    //Get Record column.
                                    $header_tmp = $this->Jobheaderimg_tmp->get($id);
                                    
                                    //Delete Record.
                                    $this->Jobheaderimg_tmp->delete($header_tmp);
                                }
                            }
                        }
                        
                         //If $this->request->data('logo-img') File exists.
                        if ($this->request->data('company_logo') && file_exists($logo_tmp . $this->request->data('company_logo'))) {

                            //Copy Image from the company_logo_tmp to company_logo.
                            if (copy($logo_tmp . $this->request->data('company_logo'), $logo_dir . $this->request->data('company_logo'))) {

                                //Delete Image file from company_logo_tmp.
                                if (unlink($logo_tmp . $this->request->data('company_logo'))) {

                                    //To be Append.
                                    $id = '';

                                    //Query out to get image record.
                                    $company_logo = $this->Companylogo_tmp->find()
                                        ->where(['user_id' => $this->request->session()->read('Auth.User')['id']])
                                        ->andWhere(['filename' => $this->request->data('company_logo')]);
                                    
                                    //Loop Record.
                                    foreach ($company_logo as $logo) {

                                        //Append ID.
                                        $id .= $logo['id'];
                                    }
                                    
                                    //Get Record column.
                                    $companylogo = $this->Companylogo_tmp->get($id);
                                    
                                    //Delete Record.
                                    $this->Companylogo_tmp->delete($companylogo);
                                }
                            }
                        }

                        /*
                        *   Create Log Record.
                        */

                        //Poster's Fullname.
                        $fullname = $this->request->session()->read('Auth.User')['firstname'] . ' ' . $this->request->session()->read('Auth.User')['lastname'];

                        //Poster's Email.
                        $email = $this->request->session()->read('Auth.User')['email'];
                        
                        //Create Log Message.
                        $log_msg = $fullname . ' "(' .$email . ')" created a Job Post "' . $this->request->data('job-title') . '".';

                        //Save Log Message.
                        $this->globalworks->_log('create_jobposts_log', $log_msg);


                        $this->Flash->success(__('Job Post successfully created!'));
                        
                        echo 'ok';
                    }
                break;
                
                /*
                *   Create User Profile.
                */
                case 'setprofile':

                    //Load Profiles Model.
                    $this->loadModel('Profiles');
                    
                    //User's Role.
                    $role = $this->request->session()->read('Auth.User')['role'];

                    //New Entity.
                    $profiles = $this->Profiles->newEntity();

                    /*
                    *   If Role is Applicant.
                    */
                    if ($role == 'Applicant') {
                        
                        //Entries.
                        $new_data = [
                            'user_id' => $this->request->session()->read('Auth.User')['id'],
                            'nickname' => $this->request->data('nickname'),
                            'job_title' => $this->request->data('job_title'),
                            'skills' => $this->request->data('s_tags'),
                            'skype_id' => $this->request->data('skype_id'),
                            'employment_status' => $this->request->data('employment_status'),
                            'skills' => json_encode($this->request->data('skills')),
                            'education' => $this->request->data('education'),
                            'desire_salary' => $this->request->data('desire_salary'),
                            'job_subscribe' => $this->request->data('job_subscribe'),
                        ];

                        //Patch Entity.
                        $profiles = $this->Profiles->patchEntity($profiles, $new_data);

                        if ($this->Profiles->save($profiles)) {
                            echo 'ok';
                        }

                    } else  if ($role == 'Employer') {
                        
                        //Entries.
                        $new_data = [
                            'business_name' => $this->request->data('business_name'),
                            'business_description' => $this->request->data('business_description'),
                            "business_email" => $this->request->data('business_email'),
                            'business_website' => $this->request->data('business_website'),
                            'business_contact_number' => $this->request->data('business_contact_number'),
                            'business_address' => $this->request->data('business_address'),
                            'business_country' => $this->request->data('business_country')
                        ];

                        $profiles = $this->Profiles->patchEntity($profiles, $new_data);

                        if ($this->Profiles->save($profiles)) {
                            echo 'ok';
                        }
                    }
                break;
            }
        }
    }
}

