<?php
namespace App\Controller;

use Cake\Event\Event;

class AdminsController extends AppController {
    
    private $globakworks;

    public $paginate = ['limit' => 50];
    
    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        
        $this->globalworks = new GlobalworksController();
        
        $this->Auth->allow(['dashboard']);
        
        $this->viewBuilder()->setLayout('admin');

        //Check if user is suspended. Auto logout user.
        return $this->globalworks->is_suspended();
    }
    
    /*
    *   Admin Dashboard Page.
    */
    public function dashboard() {
        
        //If User role is Admin or Super Admin.
        if ($this->request->session()->read('Auth.User')['role'] == 'Admin' || $this->request->session()->read('Auth.User')['role'] == 'Super Admin') {
            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());   
        } else {
            return $this->globalworks->redirect2();
        }
    }
    
    /*
    *   Admin Jobposts Page.
    */
    public function jobposts($page = null) {
        
        //If User role is Admin or Super Admin.
        if ($this->request->session()->read('Auth.User')['role'] == 'Admin' || $this->request->session()->read('Auth.User')['role'] == 'Super Admin') {
            
            //Load Jobposts Model.
            $this->loadModel('Jobposts');

            //Count Jobposts.
            $jobposts_count = $this->Jobposts->find()
            ->where(['closing_date >=' => date( "Y-m-d H:i:s" )])
            ->count();
        
            //Query out all Jobposts.
            $jobposts = $this->Jobposts->find()
                ->select([
                    'jobposts.id',
                    'jobposts.company_logo',
                    'jobposts.job_title',
                    'jobposts.job_description',
                    'jobposts.company_name',
                    'jobposts.created_at',
                    'users.firstname',
                    'users.lastname',
                ])
                ->contain(['Users'])
                ->where(['closing_date >=' => date( "Y-m-d H:i:s" )])
                ->order(['pos' => 'DESC']);

            //New Entity.
            $jps = $this->Jobposts->newEntity();

            $this->set('jobposts_count', $jobposts_count);
            $this->set('jobposts', $this->paginate($jobposts));
            $this->set('jps', $jps);
            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());
            
        } else {
            return $this->globalworks->redirect2();
        }
    }

    /*
    *   Jobposts Search Page.
    */
    public function jobpostsSearch() {

        //If user has admin or super admin role and has get request.
        if (($this->request->session()->read('Auth.User')['role'] == 'Admin' || $this->request->session()->read('Auth.User')['role'] == 'Super Admin') && $this->request->query('search')) {
            
            //Load Jobposts Model.
            $this->loadModel('Jobposts');

            //Entity.
            $jps = $this->Jobposts->patchEntity($this->Jobposts->newEntity(), $this->request->query());

            //Jobposts value to append.
            $jobposts = '';

            //Value to Count Results.
            $i = 0;

            //Search Keywords.
            $keywords = $this->request->query('search');

            if (!$jps->errors()) {

                //Query out search.
                $jobposts = $this->Jobposts->find();
                $jobposts->contain(['Users']);
                $jobposts->select([
                    'jobposts.id',
                    'jobposts.company_logo',
                    'jobposts.job_title',
                    'jobposts.company_name',
                    'jobposts.job_description',
                    'jobposts.created_at',
                    'users.firstname',
                    'users.lastname'
                ]);
                $jobposts->where(['jobposts.job_title LIKE' => '%' . $keywords . '%']);
                $jobposts->orWhere(['jobposts.company_name LIKE' => '%' . $keywords . '%']);
                $jobposts->orWhere(['jobposts.job_description LIKE' => '%' . $keywords . '%']);
                $jobposts->orWhere(['jobposts.job_responsibilities LIKE' => '%' . $keywords . '%']);
                $jobposts->orWhere(['jobposts.job_qualifications LIKE' => '%' . $keywords . '%']);
                $jobposts->orWhere(['users.firstname LIKE' => '%' . $keywords . '%']);
                $jobposts->orWhere(['users.lastname LIKE' => '%' . $keywords . '%']);
                $jobposts->andWhere(['jobposts.closing_date >=' => date( "Y-m-d H:i:s" )]);
                $jobposts->order(['jobposts.id' => 'DESC']);
                
                //Generate Result Count.
                foreach ($jobposts as $post) {
                    $i += count($post);
                }
            }

            $this->set('jps', $jps);
            $this->set('error', $jps->errors());
            $this->set('keywords', $keywords);
            $this->set('jobposts_count', $i);
            $this->set('jobposts', $this->paginate($jobposts));
            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());
        } else {

            //Redirect to dashboard.
            $this->redirect('/admin/dashboard');
        }
    }

    public function jobpostsInactive() {
        
        //If User role is Admin or Super Admin.
        if (($this->request->session()->read('Auth.User')['role'] == 'Admin' || $this->request->session()->read('Auth.User')['role'] == 'Super Admin')) {
            
            //Load Jobposts Model.
            $this->loadModel('Jobposts');

            //Count Jobposts.
            $jobposts_count = $this->Jobposts->find()
            ->where(['closing_date >=' => date( "Y-m-d H:i:s" )])
            ->count();
        
            //Query out all Jobposts.
            $jobposts = $this->Jobposts->find()
                ->select([
                    'jobposts.id',
                    'jobposts.company_logo',
                    'jobposts.job_title',
                    'jobposts.job_description',
                    'jobposts.company_name',
                    'jobposts.created_at',
                    'users.firstname',
                    'users.lastname',
                ])
                ->contain(['Users'])
                ->where(['closing_date <' => date( "Y-m-d H:i:s" )])
                ->order(['pos' => 'DESC']);

            //New Entity.
            $jps = $this->Jobposts->newEntity();

            $this->set('jobposts_count', $jobposts_count);
            $this->set('jobposts', $this->paginate($jobposts));
            $this->set('jps', $jps);
            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());


            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());
        }
    }

    public function jobpostsInactiveSearch() {
        //If user has admin or super admin role and has get request.
        if (($this->request->session()->read('Auth.User')['role'] == 'Admin' || $this->request->session()->read('Auth.User')['role'] == 'Super Admin') && $this->request->query('search')) {
            
            //Load Jobposts Model.
            $this->loadModel('Jobposts');

            //Entity.
            $jps = $this->Jobposts->patchEntity($this->Jobposts->newEntity(), $this->request->query());

            //Jobposts value to append.
            $jobposts = '';

            //Value to Count Results.
            $i = 0;

            //Search Keywords.
            $keywords = $this->request->query('search');

            if (!$jps->errors()) {

                //Query out search.
                $jobposts = $this->Jobposts->find();
                $jobposts->contain(['Users']);
                $jobposts->select([
                    'jobposts.id',
                    'jobposts.company_logo',
                    'jobposts.job_title',
                    'jobposts.company_name',
                    'jobposts.job_description',
                    'jobposts.created_at',
                    'users.firstname',
                    'users.lastname'
                ]);
                $jobposts->where(['jobposts.job_title LIKE' => '%' . $keywords . '%']);
                $jobposts->orWhere(['jobposts.company_name LIKE' => '%' . $keywords . '%']);
                $jobposts->orWhere(['jobposts.job_description LIKE' => '%' . $keywords . '%']);
                $jobposts->orWhere(['jobposts.job_responsibilities LIKE' => '%' . $keywords . '%']);
                $jobposts->orWhere(['jobposts.job_qualifications LIKE' => '%' . $keywords . '%']);
                $jobposts->orWhere(['users.firstname LIKE' => '%' . $keywords . '%']);
                $jobposts->orWhere(['users.lastname LIKE' => '%' . $keywords . '%']);
                $jobposts->andWhere(['jobposts.closing_date <' => date( "Y-m-d H:i:s" )]);
                $jobposts->order(['jobposts.id' => 'DESC']);
                
                //Generate Result Count.
                foreach ($jobposts as $post) {
                    $i += count($post);
                }
            }

            $this->set('jps', $jps);
            $this->set('error', $jps->errors());
            $this->set('keywords', $keywords);
            $this->set('jobposts_count', $i);
            $this->set('jobposts', $this->paginate($jobposts));
            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());
        } else {

            //Redirect to dashboard.
            return $this->redirect('/admin/dashboard');
        }
    }
    
    /*
    *   Admin Users Page.
    */
    public function users($page = null) {
        
        //If User role is Admin or Super Admin.
        if ($this->request->session()->read('Auth.User')['role'] == 'Admin' || $this->request->session()->read('Auth.User')['role'] == 'Super Admin') {
            
            //Candidates Page.
            if ($page == 'candidates') {
            
                //Set custom template for this page.
                $this->viewBuilder()->setTemplate('candidates');
            
            //Employers Page
            } else if ($page == 'employers') {
                
                //Set custom template for this page.
                $this->viewBuilder()->setTemplate('employers');
            
            //Admin Page.
            } else if ($page == 'admins' && $this->request->session()->read('Auth.User')['role'] == 'Super Admin') {
                
                //Set custom template for this page.
                $this->viewBuilder()->setTemplate('admins');
            } else {
                return $this->redirect('/admin/users/candidates');
            }
            
            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());   
        } else {
            return $this->globalworks->redirect2();
        }
    }
    
    /*
    *   Admin announcements page.
    */
    public function announcements() {
        
        //If User role is Admin or Super Admin.
        if ($this->request->session()->read('Auth.User')['role'] == 'Admin' || $this->request->session()->read('Auth.User')['role'] == 'Super Admin') {
            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());   
        } else {
            return $this->globalworks->redirect2();
        }
    }

    /*
    *   Create Announcement Page.
    */
    public function createAnnouncement() {

        //If User role is Admin or Super Admin.
        if ($this->request->session()->read('Auth.User')['role'] == 'Admin' || $this->request->session()->read('Auth.User')['role'] == 'Super Admin') {
            
            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());   
        } else {
            return $this->globalworks->redirect2();
        }
    }

    /*
    *   Edit Announcement.
    */
    public function editAnnouncement($id = null) {

        //If User role is Admin or Super Admin.
        if ($this->request->session()->read('Auth.User')['role'] == 'Admin' || $this->request->session()->read('Auth.User')['role'] == 'Super Admin') {
            if (!$id) {
                $this->redirect('/admin/dashboard');
            }

            //Load Announcements Model.
            $this->loadModel('Announcements');

            //Query to get requested announcement.
            $announcements = $this->Announcements->find()
                ->select(['announcement', 'published'])
                ->where(['id' => $id]);
            
            $this->set('id', $id);
            $this->set('announcements', $announcements);
            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());   
        } else {
            return $this->globalworks->redirect2();
        }
    }
    
    /*
    *   Admin Logs Page.
    */
    public function logs() {
        
        //If User role is Admin or Super Admin.
        if ($this->request->session()->read('Auth.User')['role'] == 'Admin' || $this->request->session()->read('Auth.User')['role'] == 'Super Admin') {
            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());   
        } else {
            return $this->globalworks->redirect2();
        }
    }

    /*
    *   Visitor's Statistics.
    */
    public function visitorsStatistics() {
        
        //If User role is Admin or Super Admin.
        if ($this->request->session()->read('Auth.User')['role'] == 'Admin' || $this->request->session()->read('Auth.User')['role'] == 'Super Admin') {
            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());   
        } else {
            return $this->globalworks->redirect2();
        }
    }
    
    /*
    *   Mailing List Page.
    */
    
    public function mailinglist() {
        
        //If User role is Admin or Super Admin.
        if ($this->request->session()->read('Auth.User')['role'] == 'Admin' || $this->request->session()->read('Auth.User')['role'] == 'Super Admin') {
            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());   
        } else {
            return $this->globalworks->redirect2();
        }
    }

    /*
    *   Create Mailinglist.
    */
    public function mailinglistCreate() {

        //If User role is Admin or Super Admin.
        if ($this->request->session()->read('Auth.User')['role'] == 'Admin' || $this->request->session()->read('Auth.User')['role'] == 'Super Admin') {
            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());   
        } else {
            return $this->globalworks->redirect2();
        }
    }

    /*
    *   Subscribers List.
    */
    public function subscribers() {

        //If User role is Admin or Super Admin.
        if ($this->request->session()->read('Auth.User')['role'] == 'Admin' || $this->request->session()->read('Auth.User')['role'] == 'Super Admin') {
            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());   
        } else {
            return $this->globalworks->redirect2();
        }
    }

    /*
    *   Edit Mailinglist.
    */
    public function mailinglistEdit($id = null) {

        //If User role is Admin or Super Admin.
        if ($this->request->session()->read('Auth.User')['role'] == 'Admin' || $this->request->session()->read('Auth.User')['role'] == 'Super Admin') {
            
            //If no ID request.
            if (!$id) {
                return $this->redirect('/admin');
            }

            //Load Mailinglists Model.
            $this->loadModel('Mailinglists');

            //Query requested Mailinglist.
            $lists = $this->Mailinglists->find()
                ->select(['subject', 'content'])
                ->where(['id' => $id]);


            //Subject Value.
            $subject = '';

            //Content Vakue.
            $content = '';

            //Loop Results.
            foreach ($lists as $list) {

                //Append Values.
                $subject .= $list['subject'];
                $content .= $list['content'];
            }

            $this->set('id', $id);
            $this->set('subject', $subject); 
            $this->set('content', $content);
            
            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());   
        } else {
            return $this->globalworks->redirect2();
        }
    }
    
    /*
    *   Admin Settings Page.
    */
    public function settings() {
        
        //If User role is Admin or Super Admin.
        if ($this->request->session()->read('Auth.User')['role'] == 'Admin' || $this->request->session()->read('Auth.User')['role'] == 'Super Admin') {
            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());   
        } else {
            return $this->globalworks->redirect2();
        }
    }

    /*
    *   My Account Page
    */
    public function myaccount() {

        //If User role is Admin or Super Admin.
        if ($this->request->session()->read('Auth.User')['role'] == 'Admin' || $this->request->session()->read('Auth.User')['role'] == 'Super Admin') {
           
            //Load Home Model.
            $this->loadModel('Home');

           //Query and get Admin Details.
           $users = $this->Home->find()
            ->select(['email', 'firstname', 'lastname', 'birthday', 'address', 'region', 'country', 'contact_number',])
            ->where(['id' => $this->request->session()->read('Auth.User')['id']]);
           
            $this->set('users', $users);
            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());   
        } else {
            return $this->globalworks->redirect2();
        }
    }

    /*
    *   Edit My Account Details Page.
    */
    public function editMyaccount($id = null) {
        
        //If User role is Admin or Super Admin.
        if ($this->request->session()->read('Auth.User')['role'] == 'Admin' || $this->request->session()->read('Auth.User')['role'] == 'Super Admin') {

            //Load Home Model.
            $this->loadModel('Home');

            //Query and get Admin's Account Details.
            $users = $this->Home->find()
                ->select(['firstname', 'lastname', 'birthday', 'address', 'region', 'country', 'contact_number'])
                ->where(['id' => $this->request->session()->read('Auth.User')['id']]);

            //New Entity.
            $users2 = $this->Home->newEntity(['validate' => 'updateinfo']);

            //Loop and render results to the view.
            foreach ($users as $user) {
                $this->set('firstname', $user['firstname']);
                $this->set('lastname', $user['lastname']);
                $this->set('birthday', $user['birthday']);
                $this->set('address', $user['address']);
                $this->set('region', $user['region']);
                $this->set('country', $user['country']);
                $this->set('contact_number', $user['contact_number']);
            }

            if ($this->request->is('post') || $this->request->is('put')) {

                //Get Admin's Record via ID.
                $user3 = $this->Home->get($this->request->session()->read('Auth.User')['id']);

                //Append Record.
                $user3->firstname = $this->request->data('firstname');
                $user3->lastname = $this->request->data('lastname');
                $user3->birthday = $this->request->data('month') . '/' . $this->request->data('day') . '/' . $this->request->data('year');
                $user3->address = $this->request->data('address');
                $user3->region = $this->request->data('region');
                $user3->country = $this->request->data('country');
                $user3->contact_number = $this->request->data('contact_number');

                //Patch Entity.
                $users2 = $this->Home->patchEntity($user3, $this->request->data, ['validate' => 'updateinfo']);

                if ($this->Home->save($users2)) {
                    
                    $this->Flash->success(_('Account Details Saved!'));

                    return $this->redirect('/admin/myaccount');
                }

                //Render request values when validation errors occurs.
                $this->set('firstname', $this->request->data('firstname'));
                $this->set('lastname', $this->request->data('lastname'));
                $this->set('birthday', $this->request->data('month') . '/' . $this->request->data('day') . '/' . $this->request->data('year'));
                $this->set('address', $this->request->data('address'));
                $this->set('region', $this->request->data('region'));
                $this->set('country', $this->request->data('country'));
                $this->set('contact_number', $this->request->data('contact_number'));
            }
            
            $this->set('users2', $users2);
            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());   
        } else {
            return $this->globalworks->redirect2();
        }
    }

    /*
    *   Profiles Page.
    */
    public function profiles() {

        //If User role is Admin or Super Admin.
        if ($this->request->session()->read('Auth.User')['role'] == 'Admin' || $this->request->session()->read('Auth.User')['role'] == 'Super Admin') {
            
            //Load Profles Model.
            $this->loadModel('Profiles');

            //Check if admin has profile record.
            $profiles_count = $this->Profiles->find()
                ->where(['user_id' => $this->request->session()->read('Auth.User')['id']])
                ->count();

            //Query and get admin's Profiles.
            $profiles = $this->Profiles->find()
                ->select(['profile_picture', 'nickname', 'job_subscribe'])
                ->where(['user_id' => $this->request->session()->read('User.Auth')['id']]);
            
            $this->set('profiles_count', $profiles_count);
            $this->set('profiles', $profiles);
            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());   
        } else {
            return $this->globalworks->redirect2();
        }
    }

    /*
    *   Profiles Edit Page.
    */
    public function profilesEdit() {
        //If User role is Admin or Super Admin.
        if ($this->request->session()->read('Auth.User')['role'] == 'Admin' || $this->request->session()->read('Auth.User')['role'] == 'Super Admin') {
            
            //Load Profles Model.
            $this->loadModel('Profiles');

            //New Entity.
            $profiles2 = $this->Profiles->newEntity();

            //Check if admin has profile record.
            $profiles_count = $this->Profiles->find()
                ->where(['user_id' => $this->request->session()->read('Auth.User')['id']])
                ->count();

            //If Admin has no profile record. Render an empty values to the view.
            if (!$profiles_count) {
                $this->set('profile_picture', '');
                $this->set('nickname', '');
                $this->set('job_subscribe', 0);

                //If has POST request.
                if ($this->request->is('post')) {

                    //POST entries.
                    $data = [
                        'user_id' => $this->request->session()->read('Auth.User')['id'],
                        'nickname' => $this->request->data('nickname'),
                        'job_subscribe' => $this->request->data('job_subscribe'),
                    ];

                    //Patch Entity.
                    $profiles2 = $this->Profiles->patchEntity($profiles2, $data);

                    //Save Profile.
                    if ($this->Profiles->save($profiles2)) {

                        /*
                        *   Create Log Record.
                        */

                        //User's Fullname.
                        $fullname = $this->request->session()->read('Auth.User')['firstname'] . ' ' . $this->request->session()->read('Auth.User')['lastname'];

                        //User's Email Address.
                        $email = $this->request->session()->read('Auth.User')['email'];

                        //Create Log Message.
                        $log_msg = $fullname . ' (' . $email . ') create his/her profile.';

                        //Save Log.
                        $this->globalworks->_log('create_account_profile_log', $log_msg);

                        $this->Flash->success(__('Profile Saved!'));

                        return $this->redirect('/admin/myaccount/profiles');
                    }
                }
            } else {
                
                /*
                *   Render existing record values to the view.
                */
                //Query and get admin's Profiles.
                $profiles = $this->Profiles->find()
                    ->select(['profile_picture', 'nickname', 'job_subscribe'])
                    ->where(['user_id' => $this->request->session()->read('Auth.User')['id']]);

                //Loop results and render to view.
                foreach ($profiles as $profile) {
                    $this->set('profile_picture', $profile['profile_picture']);
                    $this->set('nickname', $profile['nickname']);
                    $this->set('job_subscribe', $profile['job_subscribe']);
                }
            }
            
            $this->set('profiles_count', $profiles_count);
            $this->set('profiles2', $profiles2);
            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());   
        } else {
            return $this->globalworks->redirect2();
        }
    }

    /*
    *   Change Password Page.
    */
    public function changepassword() {

        //If User role is Admin or Super Admin.
        if ($this->request->session()->read('Auth.User')['role'] == 'Admin' || $this->request->session()->read('Auth.User')['role'] == 'Super Admin') {
            
            //Load Home Model.
            $this->loadModel('Home');

            //New Entity.
            $users = $this->Home->newEntity(['validate' => 'password']);

            //If has post or put request.
            if ($this->request->is('post') || $this->request->is('put')) {

                //Get Admin's record via ID.
                $user = $this->Home->get($this->request->session()->read('Auth.User')['id']);
                
                //Patch Entity.
                $users = $this->Home->patchEntity($user, [
                    'current_password' => $this->request->data('current_password'),
                    'password' => $this->request->data('new_password'),
                    'new_password' => $this->request->data('new_password'),
                    'rpassword' => $this->request->data('rpassword'),
                ], ['validate' => 'password']);

                //Save New Password.
                if ($this->Home->save($users)) {
                    
                    //User's Fullname.
                    $fullname = $this->request->session()->read('Auth.User')['firstname'] . ' ' . $this->request->session()->read('Auth.User')['lastname'];

                    //User's Email Address.
                    $email = $this->request->session()->read('Auth.User')['email'];

                    //Create Log Message.
                    $log_msg = $fullname . ' (' . $email . ') change his/her password.';

                    //Save Log.
                    $this->globalworks->_log('password_change_log', $log_msg);

                    $this->Flash->success(__('Password has been changed!'));

                    return $this->redirect('/admin/myaccount');
                }
            }
    
            $this->set('users', $users);
            $this->set('username', $this->globalworks->welcome_greetings());
            $this->set('thumbnail', $this->globalworks->image_thumbnail());   
        } else {
            return $this->globalworks->redirect2();
        }
    }
}
