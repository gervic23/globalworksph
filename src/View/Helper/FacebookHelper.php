<?php

namespace App\View\Helper;

/*
*   Facebook Helper View.
*   Just to output the Login Button.
*/

use Cake\View\Helper;
use Cake\View\View;

class FacebookHelper extends Helper {

    /*
    *   Login URL Property.
    */
    private $loginUrl;

    /*
    *   Initialize Helper.
    */ 
    public function initialize(array $config) {

        //Set Config to the Property.
        $this->loginUrl = $config[0];
    }

    /*
    *   Method to Output Facebook Login Button.
    */
    public function fblogin() {

        return '<a class="fb-login-btn" href="'. htmlspecialchars($this->loginUrl) .'" /><span class="fb-logo"></span> Login with Facebook</a>';
    }
}