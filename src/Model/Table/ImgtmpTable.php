<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class ImgtmpTable extends Table {
    
    public function initialize(array $config) {
        $this->addBehavior('Timestamp');
        
        $this->setTable('img_tmp');
    }
}