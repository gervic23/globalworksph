<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class AdminsTable extends Table {
    public function initialize(array $config) {
        $this->addBehavior('Timestamp');
        
        $this->setTable('jobposts');
    }

    public function jobpostsearchValidation(Validator $validator) {
        $validator
            ->notEmpty('search', 'Please write your search keywords.')
            ->add('search',  [
                'minimum' => [
                    'rule' => ['minLength' => 3],
                    'message' => 'keywords is too short.'
                ]
            ]);
        
        return $validator;
    }
}