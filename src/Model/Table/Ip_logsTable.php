<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class Ip_logsTable extends Table {
    public function initalize(array $config) {
        $this->addBehavior('Timestamp');
    }
}