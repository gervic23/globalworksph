<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class MailinglistsTable extends Table {
    public function initalize(array $config) {
        $this->addBehavior('Timestamp');
    }
}