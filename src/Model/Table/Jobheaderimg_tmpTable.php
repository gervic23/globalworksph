<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class Jobheaderimg_tmpTable extends Table {
    public function initalize(array $config) {
        $this->addBehaviour('Timestamp');
    }
}