<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\RulesChecker;
use Cake\ORM\IsUnique;

class HomeTable extends Table {
    public function initialize(array $config) {
        $this->addBehavior('Timestamp');

        $this->hasOne('Profiles')
            ->setForeignKey('user_id');

        $this->hasOne('Resumes')
            ->setForeignKey('user_id');
        
        $this->setTable('users');
    }
    
    public function validationDefault(Validator $validator) {
        $validator
            ->notEmpty('email')
            ->add('email', 'valid', ['rule' => 'email', 'message' => 'This is not a valid email address.'])
            ->notEmpty('password')
            ->add('password', ['minimum' => [
                    'rule' => ['minLength', 6],
                    'message' => 'Password must be 6 characters or more.'
                ]
            ])
            ->sameAs('password', 'rpassword', 'Password did not match.')
            ->notEmpty('rpassword')
            ->notEmpty('firstname')
            ->add('firstname', ['minimum' => [
                    'rule' => ['minLength', 3],
                    'message' => 'First Name should be 2 characters long or more.'
                ]
            ])
            ->notEmpty('lastname')
            ->add('lastname', [
                'minimum' => [
                    'rule' => ['minLength', 3],
                    'message' => 'Last Name should be 3 characters long or more.'
                ]
            ])
            ->notEmpty('gender')
            ->notEmpty('month')
            ->notEmpty('day')
            ->notEmpty('year')
            ->notEmpty('address')
            ->add('address', ['minimum' => [
                    'rule' => [$this, 'customAddress'],
                    'message' => 'Adress should be 8 characters long or more.'
                ]
            ])
            ->notEmpty('region')
            ->add('region',[
                'minimum' => [
                    'rule' => ['minLength', 2],
                    'message' => 'State/Region should be 3 charcters long or more.'
                ]
            ])
            ->notEmpty('country')
            ->notEmpty('contact_number')
            ->add('contact_number', [
                'minimum' => [
                    'rule' => ['minLength', 6],
                    'message' => 'Contact Number should be 6 digits or more.'
                ]
            ])
            ->notEmpty('accept_terms', 'You must agree to the terms of service.');
            
        return $validator;
    }
    
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->isUnique(['email'], 'Email already exists.'));
        
        return $rules;
    }
    
    public function validationLogin($validator) {
        $validator
            ->notEmpty('email', 'Please enter your email address.')
            ->notEmpty('password', 'Please enter your password.');
        
        return $validator;
    }

    public function validationUpdateinfo(Validator $validator) {
        $validator
            ->notEmpty('firstname')
            ->add('firstname', ['minimum' => [
                    'rule' => ['minLength', 3],
                    'message' => 'Firstname should be 2 characters long or more.'
                ]
            ])
            ->notEmpty('lastname')
            ->add('lastname', [
                'minimum' => [
                    'rule' => ['minLength', 3],
                    'message' => 'Lastname should be 3 characters long or more.'
                ]
            ])
            ->notEmpty('address')
            ->add('address', ['minimum' => [
                    'rule' => [$this, 'customAddress'],
                    'message' => 'Adress should be 8 characters long or more.'
                ]
            ])
            ->notEmpty('region')
            ->add('region', [
                'minimum' => [
                    'rule' => ['minLength', 2],
                    'message' => 'State/Region should be 2 characters or more.'
                ]
            ])
            ->notEmpty('contact_number')
            ->add('contact_number', [
                'minumum' => [
                    'rule' => ['minLength', 6],
                    'message' => 'Contact Number should be 6 characters long or more.'
                ]
            ]);
        
        return $validator;
    }

    public function validationPassword(Validator $validator) {
        $validator
            ->add('current_password', 'custom', [
                'rule' => function($value, $context) {
                    $user = $this->get($context['data']['id']);

                    if ($user) {
                        if ((new DefaultPasswordHasher)->check($value, $user->password)) {
                            return true;
                        }
                    }

                    return false;
                },
                'message' => 'Current Password is invalid.'
            ])
            ->notEmpty('current_password')
            ->add('new_password', [
                'length' => [
                    'rule' => ['minLength', 6],
                    'message' => 'Password should be 6 characters long ore more.',
                ]
            ])
            ->notEmpty('new_password')
            ->add('rpassword', [
                'match' => [
                    'rule' => ['compareWith', 'new_password'],
                    'message' => 'Passwords did not match.'
                ]
            ])
            ->notEmpty('rpassword');

        return $validator;
    }

    public function validationBasic(Validator $validator) {
        $validator
            ->notEmpty('firstname')
            ->add('firstname', ['minimum' => [
                    'rule' => ['minLength', 3],
                    'message' => 'First Name should be 2 characters long or more.'
                ]
            ])
            ->notEmpty('lastname')
            ->add('lastname', [
                'minimum' => [
                    'rule' => ['minLength', 3],
                    'message' => 'Last Name should be 3 characters long or more.'
                ]
            ])
            ->notEmpty('gender')
            ->notEmpty('month')
            ->notEmpty('day')
            ->notEmpty('year')
            ->notEmpty('address')
            ->add('address', ['minimum' => [
                    'rule' => [$this, 'customAddress'],
                    'message' => 'Adress should be 8 characters long or more.'
                ]
            ])
            ->notEmpty('region')
            ->add('region',[
                'minimum' => [
                    'rule' => ['minLength', 2],
                    'message' => 'State/Region should be 3 charcters long or more.'
                ]
            ])
            ->notEmpty('country')
            ->notEmpty('contact_number')
            ->add('contact_number', [
                'minimum' => [
                    'rule' => ['minLength', 6],
                    'message' => 'Contact Number should be 6 digits or more.'
                ]
            ])
            ->notEmpty('accept_terms', 'You must agree to the terms of service.');

        return $validator;
    }

    public function validationFbpassword(Validator $validator) {
        $validator
            ->notEmpty('password');

        return $validator;
    }

    public function validationForgotpassword(Validator $validator) {
        $validator
            ->notEmpty('email')
            ->add('email', [
                'format' => [
                    'rule' => 'email',
                    'message' => 'Invalid Email Format.'
                ],
            ]);

        return $validator;
    }

    public function validationPasswordrecovery(Validator $validator) {
        $validator
            ->notEmpty('new-password')
            ->add('new-password', [
                'minimum' => [
                    'rule' => ['minLength', 6],
                    'message' => 'Password should be 6 characters long ore more.',
                ],
            ])
            ->notEmpty('rpassword')
            ->add('rpassword', [
                'match' => [
                    'rule' => ['compareWith', 'new-password'],
                    'message' => 'Passwords did not match.',
                ],
            ]);

        return $validator;
    }

    public function validationConfirmaccount(Validator $validator) {
        $validator
            ->notEmpty('confirmation_code');

        return $validator;
    }

    //Custom Rule for $_POST['address'].
    public function customAddress($value, $context) {
        if (strlen($value) <= 7) {
            return false;
        }
        
        return true;
    }
}