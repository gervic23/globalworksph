<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class User_log_msgTable extends Table {
    
    public function initialize(array $config) {
        $this->addBehavior('Timestamp');
    }
}