<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class Companylogo_tmpTable extends Table {
    public function initialize(array $config) {
        $this->addBehavior('Timestamp');
    }
}