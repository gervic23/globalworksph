<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class MessagesTable extends Table {
    public function initalize(array $config) {
        $this->addBehavior('Timestamp');
    }
}