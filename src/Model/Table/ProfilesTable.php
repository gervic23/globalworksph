<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class ProfilesTable extends Table {
    
    public function initialize(array $config) {
        $this->addBehavior('Timestamp');

        $this->belongsTo('Users');

        $this->hasOne('Resumes')
            ->setForeignKey('user_id');
    }
}