<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class JobpostsTable extends Table {
    public function initialize(array $config) {
        $this->addBehavior('Timestamp');

        $this->belongsTo('Users')
            ->setForeignkey('user_id');
    }

    public function validationDefault(Validator $validator) {
        $validator
            ->notEmpty('search', 'Please write your search keywords.')
            ->add('search',  [
                'minimum' => [
                    'rule' => ['minLength', 3],
                    'message' => 'keywords is too short.'
                ]
            ]);
        
        return $validator;
    }
}