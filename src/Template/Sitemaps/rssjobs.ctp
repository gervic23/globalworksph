<?php use App\Controller\GlobalworksController; $globalworks = new GlobalworksController(); ?>
<?php header('Content-Type: application/rss+xml; charset=utf-8'); ?>

<?xml version="1.0" encoding="UTF-8" encoding='ISO-8859-1'?>
<rss version="2.0"
	xmlns:content="http://purl.org/rss/1.0/modules/content/"
	xmlns:wfw="http://wellformedweb.org/CommentAPI/"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:atom="http://www.w3.org/2005/Atom"
	xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
	xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
	>
<channel>
    <title>Globalworks Job List</title>
    <atom:link href="<?= $url . '/rss/jobs' ?>" rel="self" type="application/rss+xml" />
    <link><?= $url ?></link>
    <description>&nbsp;</description>
    <language>en-US</language>
	<sy:updatePeriod>hourly</sy:updatePeriod>
	<sy:updateFrequency>1</sy:updateFrequency>
    
    <?php foreach ($jobposts as $post): ?>
        <item>
            <title><?= $post['job_title'] ?></title>
            <description><?= htmlentities($post['job_description']) ?></description>
            <link><?= $url . '/job/' . $post['id'] ?></link>
            <category><?= $post['job_category'] ?></category>
            <pubDate><?= $globalworks->datetimetodate($post['created_at']) ?></pubDate>
        </item>
    <?php endforeach; ?>
</channel>
</rss>