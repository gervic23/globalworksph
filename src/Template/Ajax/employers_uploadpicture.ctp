<div class="employers-upload-pic">
    <div class="progress">
        <span class="employers-setup-profile-pic">Upload Your Profile Picture</span>
        <span class="employers-crop-profile-pic">Crop Your Profile Picture</span>
        <div class="clear"></div>
    </div>
    <div class="uploader"></div>
    <div class="btn">
        <span id="close">Close This Window</span>
    </div>
</div>

<script>
    var label;
    var label_font_size;

    resizeUploadPic();
    _uploaderAttr();

    $(window).resize(function() {
        resizeUploadPic();
        _uploaderAttr();
    });

    $("#close").on("click", function() {
        $(".upload-profile-pic-view").animate({
            top: "-100%"
        }, 500, function() {
            $(this).html("");
            $(".main-header").show();
            $("body").css({
                "overflow-y": "auto"
            });
        });
    });

    $(".employers-upload-pic .uploader").dragdropupload('/ajax', {
        label: label,
        width: "80%",
        height: "300px",
        border_size: "3px",
        background_color: "#e3f8fd",
        label_font_size: label_font_size,
        error_font_size: "100%",
        post_data: {
            operation: "image_upload",
            _csrfToken: "<?= $this->request->getParam('_csrfToken'); ?>",
        },
        validation: {
            file_types: {
                types: ["image/jpeg", "image/png"],
                validation_message: "is not an image file. Only jpeg and png formats are allowed."
            },
            file_sizes: {
                max: {
                    size: 1024,
                    validation_message: "size to large."
                }
            },
            image_dimension: {
                min: {
                    width: 370,
					height: 370,
					validation_message: "dimension should be 370x370 or higher."
                }
            }
        }
    }, function(r) {
        if (r == "ok") {
            $(".main-header").hide();
        
            $(".upload-profile-pic-view").animate({
                top: "0"
            }, 500, function() {
                var data = {
                    "operation": "crop_employers_profile_picture",
                    "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>"
                };
                
                var new_height2 = new_height / 2 + 300;
                
                $("body").css({
                    "overflow": "hidden"
                });
                
                $(this).css({
                    "line-height": new_height2 + "px",
                    "text-align": "center"
                }).html("<img src='/img/loading.gif' />");
                
                $.post("/ajax", data, function(r) {
                    $(".upload-profile-pic-view").css({
                        "line-height": "normal",
                        "text-align": "left"
                    }).html(r);
                });
            });
        }
    });

    function resizeUploadPic() {
        var height = $(window).height();
        var new_height = height - 25;
        var upload_pic = $(".employers-upload-pic").height();
        
        if (new_height > upload_pic) {
            $(".employers-upload-pic").css({
                "height": new_height + "px"
            });
        }
    }

    function _uploaderAttr() {
        if ($(window).width() > 630) {
            label = "Drag Image file here or click here to browse image.";
            label_font_size = "130%"
        } else {
            label = "Click here to upload.";
            label_font_size = "100%";
        }
    }
</script>