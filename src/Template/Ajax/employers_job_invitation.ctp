<div class="invitation-main">
    <div class="flash"></div>
    <div class="intro">
        <?= ($profile_picture) ? '<img src="/img/users/profile_thumbnails/' . $profile_picture . '" />' : ''; ?> Send job invitaion to <?= $firstname . ' ' . $lastname ?>.
    </div>
    <div class="form">
        <div class="input">
            <?= $this->Form->text('message', ['id' => 'message']) ?>
        </div>
        <div class="btn-container">
            <span id="send">Send</span> <span id="cancel">Cancel</span>
        </div>
    </div>
</div>

<script>
    CKEDITOR.env.isCompatible = true;

    CKEDITOR.replace('message', {
        on : {
            instanceReady : function( ev ) {
                this.dataProcessor.writer.setRules( 'p', {
                    indent : false,
                    breakBeforeOpen : false,
                    breakAfterOpen : false,
                    breakBeforeClose : false,
                    breakAfterClose : false
                });
            }
        },
        toolbar: [
            { name: 'basicstyles', groups: [ 'basicstyles'], items: [ 'Bold', 'Italic', 'Underline', 'Strike', '-', 'TextColor'] },
            { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
            { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] }
        ]
    });

    $("#cancel, .black-screen").on("click", function() {
        close_message();
    });

    $("#send").on("click", function() {
        if (CKEDITOR.instances["message"].getData() == "") {
            $(".flash").css("color", "red").html("Please write your message.");
            _scrollUp();
        } else {
            var data = {
                "operation": "send_job_invitation",
                "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>",
                "id": <?= $id ?>,
                "message": CKEDITOR.instances["message"].getData()
            };

            $.post("/ajax", data, function(r) {
                if (r == 1) {
                    $(".flash").css("color", "green").html("Job Invitation Sent!").focus();
                    $("#hire, #send").hide();
                    _scrollUp();
                    setTimeout(function() {
                        close_message();
                    }, 3000);
                }
            });
        }
    });

    for (var i in CKEDITOR.instances) {
        CKEDITOR.instances[i].on('change', function(e) {
            $("#messsage").attr("value", CKEDITOR.instances["message"].getData());
            $(".flash").html("");
        });
    }

    function close_message() {
        $(".employers-candidate-view").css({
            "overflow-y": "auto"
        });

        $(".employers-job-invitation").slideUp(300, function() {
            $(this).html("");
            $(".black-screen").hide();
        });
    }
</script>