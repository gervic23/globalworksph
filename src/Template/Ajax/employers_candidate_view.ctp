<?php
    use App\Controller\GlobalworksController; $globalworks = new GlobalworksController();

    if ($experience) {
        $experience = explode(',', $experience);
    }

    if ($skills) {
        $skills = json_decode($skills, true);
    }

    if ($desire_salary) {
        $desire_salary = explode(',', $desire_salary);
    }
?>

<div class="black-screen"></div>
<div class="employers-job-invitation"></div>
<div class="employers-candidate-ajax">
    <div class="profile-picture">
        <?php if ($profile_picture): ?>
            <img src="/img/users/profile_pictures/<?= $profile_picture ?>" />
        <?php else: ?>
            <img src="/img/users/nopropic.png" />
        <?php endif; ?>
    </div>
    <div class="details">
        <div class="head">
            <ul>
                <li><?= $firstname . ' ' . $lastname ?></li>
                <?php if ($job_title): ?>
                    <li><?= $job_title ?></li>
                <?php endif; ?>
                <?php if ($nickname): ?>
                    <li>(<?= $nickname ?>)</li>
                <?php endif; ?>
            </ul>
        </div>
        <div class="table">
            <table cellpadding="0" cellspacing="0">
                <tbody>
                    <?php if ($employment_status): ?>
                        <tr>
                            <td>Employment Status:</td>
                            <td><?= $employment_status ?></td>
                        </tr>
                    <?php endif; ?>
                    <?php if ($education): ?>
                        <tr>
                            <td>Education Attainment:</td>
                            <td><?= $education ?></td>
                        </tr>
                    <?php endif; ?>
                    <?php if ($experience): ?>
                        <tr>
                            <td>Experience:</td>
                            <td>
                                <ul>
                                    <?php foreach ($experience as $exp): ?>
                                        <li><?= $exp ?></li>
                                    <?php endforeach; ?>
                                </ul>
                            </td>
                        </tr>
                    <?php endif; ?>
                    <?php if ($skills): ?>
                        <tr>
                            <td>Skills:</td>
                            <td>
                                <ul>
                                    <?php foreach ($skills as $skill): ?>
                                        <li><?= $skill ?></li>
                                    <?php endforeach; ?>
                                </ul>
                            </td>
                        </tr>
                    <?php endif; ?>
                    <?php if ($desire_salary): ?>
                        <tr>
                            <td>Desire Salary:</td>
                            <td>
                                <ul>
                                    <?php if (in_array('Php 8000 Php - Php 10000', $desire_salary)): ?>
                                        <li>Php 8000 - Php 10000</li>
                                    <?php endif; ?>

                                    <?php if (in_array('Php 10000 - Php 15000', $desire_salary)): ?>
                                        <li>Php 10000 - Php 15000</li>
                                    <?php endif; ?>

                                    <?php if (in_array('Php 15000 - Php 20000', $desire_salary)): ?>
                                        <li>Php 15000 - Php 20000</li>
                                    <?php endif; ?>

                                    <?php if (in_array('Php 20000 - Php 25000', $desire_salary)): ?>
                                        <li>Php 20000 - Php 25000</li>
                                    <?php endif; ?>

                                    <?php if (in_array('Php 25000 - Php 30000', $desire_salary)): ?>
                                        <li> Php 25000 - Php 30000</li>
                                    <?php endif; ?>

                                    <?php if (in_array('Php 30000 - Php 35000', $desire_salary)): ?>
                                        <li>Php 30000 - Php 35000</li>
                                    <?php endif; ?>

                                    <?php if (in_array('Php 35000 - Php 40000', $desire_salary)): ?>
                                        <li>Php 35000 - Php 40000</li>
                                    <?php endif; ?>

                                    <?php if (in_array('Php 40000 - Php 45000', $desire_salary)): ?>
                                        <li>Php 40000 - Php 45000</li>
                                    <?php endif; ?>

                                    <?php if (in_array('Php 45000 - Php 50000', $desire_salary)): ?>
                                        <li>Php 45000 - Php 50000</li>
                                    <?php endif; ?>

                                    <?php if (in_array('Morethan Php 50000', $desire_salary)): ?>
                                        <li>Morethan Php 50000</li>
                                    <?php endif; ?>
                                </ul>
                            </td>
                        </tr>
                    <?php endif; ?>
                    <tr>
                        <td>Gender:</td>
                        <td><?= $gender ?></td>
                    </tr>
                    <tr>
                        <td>Location:</td>
                        <td>
                            <div><?= $address ?></div>
                            <div><?= $region ?></div>
                            <div><?= $globalworks->country_to_str($country) ?></div>
                        </td>
                    </tr>
                    <tr>
                        <td>Contact Number:</td>
                        <td><?= $contact_number ?></td>
                    </tr>
                    <?php if ($skype_id): ?>
                        <tr>
                            <td>Skype ID:</td>
                            <td><?= $skype_id ?></td>
                        </tr>
                    <?php endif; ?>
                    <?php if ($resume_id && $orig_filename): ?>
                        <tr>
                            <td>Resume Document:</td>
                            <td>
                                <div class="resume-row">
                                    <a href="/download/<?= $resume_id ?>"><img src="/img/resume-icon.png" /></a> <a href="/download/<?= $resume_id ?>"><span id="orig-filename"></span></a>
                                </div>
                            </td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
            <div class="m-details">
                <ul>
                    <?php if ($employment_status): ?>
                        <li class="m_employment_status">
                            <div>Employment Status:</div>
                            <div><?= $employment_status ?></div>
                        </li>
                    <?php endif; ?>
                   
                    <?php if ($education): ?>
                        <li class="m_education">
                            <div>Educational Attainment:</div>
                            <div><?= $education ?></div>
                        </li>
                    <?php endif; ?>
                    
                    <?php if ($skills): ?>
                        <li class="m_skills">
                            <div>Skills:</div>
                            <div>
                                <ul>
                                    <?php foreach ($skills as $skill): ?>
                                        <li><?= $skill ?></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </li>
                    <?php endif; ?>
                    
                    <?php if ($desire_salary): ?>
                        <li class="m_desire_salary">
                            <div>Desire Salary:</div>
                            <div>
                                <ul>
                                    <?php if (in_array('Php 8000 Php - Php 10000', $desire_salary)): ?>
                                        <li>Php 8000 - Php 10000</li>
                                    <?php endif; ?>

                                    <?php if (in_array('Php 10000 - Php 15000', $desire_salary)): ?>
                                        <li>Php 10000 - Php 15000</li>
                                    <?php endif; ?>

                                    <?php if (in_array('Php 15000 - Php 20000', $desire_salary)): ?>
                                        <li>Php 15000 - Php 20000</li>
                                    <?php endif; ?>

                                    <?php if (in_array('Php 20000 - Php 25000', $desire_salary)): ?>
                                        <li>Php 20000 - Php 25000</li>
                                    <?php endif; ?>

                                    <?php if (in_array('Php 25000 - Php 30000', $desire_salary)): ?>
                                        <li> Php 25000 - Php 30000</li>
                                    <?php endif; ?>

                                    <?php if (in_array('Php 30000 - Php 35000', $desire_salary)): ?>
                                        <li>Php 30000 - Php 35000</li>
                                    <?php endif; ?>

                                    <?php if (in_array('Php 35000 - Php 40000', $desire_salary)): ?>
                                        <li>Php 35000 - Php 40000</li>
                                    <?php endif; ?>

                                    <?php if (in_array('Php 40000 - Php 45000', $desire_salary)): ?>
                                        <li>Php 40000 - Php 45000</li>
                                    <?php endif; ?>

                                    <?php if (in_array('Php 45000 - Php 50000', $desire_salary)): ?>
                                        <li>Php 45000 - Php 50000</li>
                                    <?php endif; ?>

                                    <?php if (in_array('Morethan Php 50000', $desire_salary)): ?>
                                        <li>Morethan Php 50000</li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                        </li>
                    <?php endif; ?>
                    <li class="m_gender">
                        <div>Gender:</div>
                        <div><?= $gender ?></div>
                    </li>
                    <li class="m_location">
                        <div>Location:</div>
                        <div>
                            <ul>
                                <li><?= $address ?></li>
                                <li><?= $region ?></li>
                                <li><?= $globalworks->country_to_str($country) ?></li>
                            </ul>
                        </div>
                    </li>
                    <li class="m_contact_number">
                        <div>Contact No:</div>
                        <div><?= $contact_number ?></div>
                    </li>

                    <?php if ($resume_id && $orig_filename): ?>
                        <li class="m_resume">
                            <div>Resume Document:</div>
                            <div>
                                <a href="/download/<?= $resume_id ?>"><img src="/img/resume-icon.png" /></a> <a href="/download/<?= $resume_id ?>"><span id="orig-filename2"></span></a>
                            </div>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
        <div class="btn-container">
            <ul>
                <li><span id="close">Close This Window</span></li>
                <li><?= (!$has_user_log_msg) ? '<span id="hire">Hire This Candidate</span>' : '';  ?></li>
            </ul> 
        </div>
    </div>
</div>
<script>
    <?php if ($orig_filename): ?>
       $("#orig-filename").html( _split_end("<?= $orig_filename ?>", ".", 20));
       $("#orig-filename2").html( _split_end("<?= $orig_filename ?>", ".", 20));
    <?php endif; ?>

    invitationAttr();

    $(window).on("resize", function() {
        invitationAttr();
    });

    $("#close").on("click", function() {
        $(".employers-candidate-view").animate({
            top: "-100%"
        }, 500, function() {
            $(this).html("");
            
            if ($(window).width() > 800) {
                $(".main-header").show();
            }

            $("body").css({
                "overflow-y": "auto"
            });
        });
    });

    $("#hire").on("click", function() {
        $(".employers-candidate-view").css({
            "overflow-y": "hidden"
        });

        $(".black-screen").show();

        $(".employers-job-invitation").slideDown(500, function() {
            var data = {
                "operation": "employers_job_invitation",
                "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>",
                "id": "<?= $candidate_id ?>"
            };

            $.post("/ajax", data, function(r) {
                $(".employers-job-invitation").css({
                    "text-align": "left",
                    "line-height": "normal"
                }).html(r);
            });
        });
    });

    function _split_end(str, splitter, len) {
        var str_split = str.split(splitter);
        var last = str_split.length - 1;

        var new_string = '';

        for (var i = 0; i < last; i++) {
            new_string += str_split[i] + splitter;
        }

        if (new_string.length > len) {
            return new_string.substring(0, len) + ".." + str_split[last];
        }

        return new_string + str_split[last];
    }

    function invitationAttr() {
        var height = $(window).height();
        var new_height = height - 120;

        $(".employers-job-invitation").css({
            "height": new_height + "px"
        });
    }

    function _scrollUp() {
        $(".employers-job-invitation").animate({
            scrollTop: 0
        }, 80);
    }
</script>