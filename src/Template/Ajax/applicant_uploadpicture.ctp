<div class="applicants-upload-pic">
    <div class="progress">
        <span class="applicants-setup-profile-pic">Upload Your Profile Picture</span>
        <span class="applicants-crop-profile-pic">Crop Your Profile Picture</span>
        <div class="clear"></div>
    </div>
    <div class="uploader"></div>
    <div class="clear"></div>
    <div class="btn">
        <span id="close">Close This Window</span>
    </div>
</div>

<script>
    var label;
    var label_font_size;

    resizeUploadPic();
    uploadpicAttr();

    $(window).resize(function() {
        resizeUploadPic();
        uploadpicAttr();
    });

    $("#close").on("click", function() {
        $(".upload-profile-pic-view").animate({
            top: "-100%"
        }, 500, function() {
            $(this).html("");
            
            if ($(window).width() > 800) {
                $(".main-header").show();
            }

            $("body").css({
                "overflow-y": "auto"
            });
        });
    });

    //$(".applicants-upload-pic .uploader").dragdropupload();

    $(".applicants-upload-pic .uploader").dragdropupload('/ajax', {
        label: label,
        width: "80%",
        height: "300px",
        border_size: "3px",
        background_color: "#e3f8fd",
        label_font_size: label_font_size,
        error_font_size: "100%",
        post_data: {
            operation: "image_upload",
            _csrfToken: "<?= $this->request->getParam('_csrfToken'); ?>",
        },
        validation: {
            file_types: {
                types: ["image/jpeg", "image/png"],
                validation_message: "is not an image file. Only jpeg and png formats are allowed."
            },
            file_sizes: {
                max: {
                    size: 1024,
                    validation_message: "size to large."
                }
            },
            image_dimension: {
                min: {
                    width: 370,
					height: 370,
					validation_message: "dimension should be 370x370 or higher."
                }
            }
        }
    }, function(r) {
        if (r == "ok") {
            var data = {
                "operation": "crop_applicants_profile_picture",
                "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>"
            };

            var new_height2 = new_height / 2 + 300;

            $(".upload-profile-pic-view").css({
                "line-height": new_height2 + "px",
                "text-align": "center"
            }).html("<img src='/img/loading.gif' />");
            
            $.post("/ajax", data, function(r) {
                $(".upload-profile-pic-view").css({
                    "line-height": "normal",
                    "text-align": "left"
                }).html(r);
            });
        }
    });

    function uploadpicAttr() {
        if ($(window).width() >= 500) {
            label = "Drag Image file here or click here to browse image.";
            label_font_size = "130%";
        } else {
            label = "Click Here to Upload";
            label_font_size = "100%";
        }
    }

    function resizeUploadPic() {
        var height = $(window).height();
        var new_height = height - 25;
        var upload_pic = $(".applicants-upload-pic").height();
        
        if (new_height > upload_pic) {
            $(".applicants-upload-pic").css({
                "height": new_height + "px"
            });
        }
    }
</script>