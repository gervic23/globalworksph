<div class="candidate-job-application">
    <div class="flash"></div>
    <div class="intro">
        <?php if ($company_logo): ?>
            <div class="logo">
                <img src="/img/users/company_logo/<?= $company_logo ?>" />
            </div>
        <?php endif; ?>
        <div class="job-title"><?= $job_title ?></div>
        <div class="company-name"><?= $company_name ?></div>
    </div>
    <div class="info">
        You are applying for this job. This will sends your infromation to <?= $firstname . ' ' . $lastname ?> to review your qualification. 
        You can send message to <?= $firstname . ' ' . $lastname ?> add aditional information regarding to your application.
    </div>
    <div class="message">
        <?= $this->Form->text('message', ['id' => 'message']) ?>
    </div>
    <div class="btn">
        <span id="send">Send Application</span> <span id="close">Close This Window</span>
    </div>
</div>

<script>
    
    CKEDITOR.env.isCompatible = true;

    CKEDITOR.replace('message', {
        on : {
            instanceReady : function( ev ) {
                this.dataProcessor.writer.setRules( 'p', {
                    indent : false,
                    breakBeforeOpen : false,
                    breakAfterOpen : false,
                    breakBeforeClose : false,
                    breakAfterClose : false
                });
            }
        },
        toolbar: [
            { name: 'basicstyles', groups: [ 'basicstyles'], items: [ 'Bold', 'Italic', 'Underline', 'Strike', '-', 'TextColor'] },
            { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
            { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] }
        ]
    });
    
    $("#close, .black-screen2").on("click", function() {
        close_message();
    });
    
    $("#send").on("click", function() {
        var data = {
            "operation": "candidate_send_application",
            "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
            "jobpost_id": <?= $this->request->data('id') ?>,
            "message": CKEDITOR.instances["message"].getData()
        };
            
        $.post("/ajax", data, function(r) {
            if (r == "ok") {
                $(".flash").html("<span class='sent-img'><img src='/img/sent.png' /></span> Application Sent!");
                $(".apply, #send").hide();
                setTimeout(function() {
                    close_message();
                }, 3000);
            }
        });
    });
    
    function close_message() {
        $(".job-app-window").slideUp(500, function() {
            $(this).html("");
            $(".black-screen2").hide();
        });
    }
</script>