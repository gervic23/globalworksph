<?php
    use App\Controller\GlobalworksController;
    use Cake\I18n\FrozenTime;

    $globalworks = new GlobalworksController();
    
    $categories = [
        '' => 'Select Category',
        'Accounting Finance' => 'Accounting Finance',
        'Arts/Media' => 'Arts/Media',
        'Computer/I.T' => 'Computer/I.T',
        'Engineering' => 'Engineering',
        'Admin/Office' => 'Admin/Office',
        'Building/Construction' => 'Building/Construction',
        'Education/Training' => 'Education/Training',
        'Health Care' => 'Health Care',
        'Hotel/Restaurant' => 'Hotel Restaurant',
        'Manufacturing' => 'Manufacturing',
        'Sales Marketing' => 'Sales Marketing',
        'Services/Others' => 'Services/Others',
    ];

    $job_types = [
        '' => 'Select Job Type',
        'Homebase/Full Time' => 'Homebased/Full Time',
        'Homebase/Part Time' => 'Homebased/Part Time',
        'Office/Full Time' => 'Office/Full Time',
        'Office/Part Time' => 'Office/Part Time',
        'All' => 'All',
    ];

    $job_base = [
        '' => 'Select Job Base',
        'Local' => 'Local',
        'Abroad' => 'Abroad',
        'All' => 'All',
    ];

    $job_year_exp_start = [
        'No Experience' => 'No Experience',
        '6 Months' => '6 Months',
        '1 Year' => '1 Year',
        '2 Years' => '2 Years',
        '3 Years' => '3 Years',
        '4 Years' => '4 Years',
        '5 Years' => '5 Years',
        '6 Years' => '6 Years',
        '7 Years' => '7 Years',
        '8 Years' => '8 Years',
        '9 Years' => '9 Years',
        '10 Years' => '10 Years',
    ];

    $job_year_exp_end = [
        '' => '',
        '6 Months' => '6 Months',
        '1 Year' => '1 Year',
        '2 Years' => '2 Years',
        '3 Years' => '3 Years',
        '4 Years' => '4 Years',
        '5 Years' => '5 Years',
        '6 Years' => '6 Years',
        '7 Years' => '7 Years',
        '8 Years' => '8 Years',
        '9 Years' => '9 Years',
        '10 Years' => '10 Years',
        'Morethan 10 Years' => 'Morethan 10 Years',
    ];

    $countries = [
    '' => 'Select Country',
    'AF'=>'AFGHANISTAN',
    'AL'=>'ALBANIA',
    'DZ'=>'ALGERIA',
    'AS'=>'AMERICAN SAMOA',
    'AD'=>'ANDORRA',
    'AO'=>'ANGOLA',
    'AI'=>'ANGUILLA',
    'AQ'=>'ANTARCTICA',
    'AG'=>'ANTIGUA AND BARBUDA',
    'AR'=>'ARGENTINA',
    'AM'=>'ARMENIA',
    'AW'=>'ARUBA',
    'AU'=>'AUSTRALIA',
    'AT'=>'AUSTRIA',
    'AZ'=>'AZERBAIJAN',
    'BS'=>'BAHAMAS',
    'BH'=>'BAHRAIN',
    'BD'=>'BANGLADESH',
    'BB'=>'BARBADOS',
    'BY'=>'BELARUS',
    'BE'=>'BELGIUM',
    'BZ'=>'BELIZE',
    'BJ'=>'BENIN',
    'BM'=>'BERMUDA',
    'BT'=>'BHUTAN',
    'BO'=>'BOLIVIA',
    'BA'=>'BOSNIA AND HERZEGOVINA',
    'BW'=>'BOTSWANA',
    'BV'=>'BOUVET ISLAND',
    'BR'=>'BRAZIL',
    'IO'=>'BRITISH INDIAN OCEAN TERRITORY',
    'BN'=>'BRUNEI DARUSSALAM',
    'BG'=>'BULGARIA',
    'BF'=>'BURKINA FASO',
    'BI'=>'BURUNDI',
    'KH'=>'CAMBODIA',
    'CM'=>'CAMEROON',
    'CA'=>'CANADA',
    'CV'=>'CAPE VERDE',
    'KY'=>'CAYMAN ISLANDS',
    'CF'=>'CENTRAL AFRICAN REPUBLIC',
    'TD'=>'CHAD',
    'CL'=>'CHILE',
    'CN'=>'CHINA',
    'CX'=>'CHRISTMAS ISLAND',
    'CC'=>'COCOS (KEELING) ISLANDS',
    'CO'=>'COLOMBIA',
    'KM'=>'COMOROS',
    'CG'=>'CONGO',
    'CD'=>'CONGO, THE DEMOCRATIC REPUBLIC OF THE',
    'CK'=>'COOK ISLANDS',
    'CR'=>'COSTA RICA',
    'CI'=>'COTE D IVOIRE',
    'HR'=>'CROATIA',
    'CU'=>'CUBA',
    'CY'=>'CYPRUS',
    'CZ'=>'CZECH REPUBLIC',
    'DK'=>'DENMARK',
    'DJ'=>'DJIBOUTI',
    'DM'=>'DOMINICA',
    'DO'=>'DOMINICAN REPUBLIC',
    'TP'=>'EAST TIMOR',
    'EC'=>'ECUADOR',
    'EG'=>'EGYPT',
    'SV'=>'EL SALVADOR',
    'GQ'=>'EQUATORIAL GUINEA',
    'ER'=>'ERITREA',
    'EE'=>'ESTONIA',
    'ET'=>'ETHIOPIA',
    'FK'=>'FALKLAND ISLANDS (MALVINAS)',
    'FO'=>'FAROE ISLANDS',
    'FJ'=>'FIJI',
    'FI'=>'FINLAND',
    'FR'=>'FRANCE',
    'GF'=>'FRENCH GUIANA',
    'PF'=>'FRENCH POLYNESIA',
    'TF'=>'FRENCH SOUTHERN TERRITORIES',
    'GA'=>'GABON',
    'GM'=>'GAMBIA',
    'GE'=>'GEORGIA',
    'DE'=>'GERMANY',
    'GH'=>'GHANA',
    'GI'=>'GIBRALTAR',
    'GR'=>'GREECE',
    'GL'=>'GREENLAND',
    'GD'=>'GRENADA',
    'GP'=>'GUADELOUPE',
    'GU'=>'GUAM',
    'GT'=>'GUATEMALA',
    'GN'=>'GUINEA',
    'GW'=>'GUINEA-BISSAU',
    'GY'=>'GUYANA',
    'HT'=>'HAITI',
    'HM'=>'HEARD ISLAND AND MCDONALD ISLANDS',
    'VA'=>'HOLY SEE (VATICAN CITY STATE)',
    'HN'=>'HONDURAS',
    'HK'=>'HONG KONG',
    'HU'=>'HUNGARY',
    'IS'=>'ICELAND',
    'IN'=>'INDIA',
    'ID'=>'INDONESIA',
    'IR'=>'IRAN, ISLAMIC REPUBLIC OF',
    'IQ'=>'IRAQ',
    'IE'=>'IRELAND',
    'IL'=>'ISRAEL',
    'IT'=>'ITALY',
    'JM'=>'JAMAICA',
    'JP'=>'JAPAN',
    'JO'=>'JORDAN',
    'KZ'=>'KAZAKSTAN',
    'KE'=>'KENYA',
    'KI'=>'KIRIBATI',
    'KP'=>'KOREA DEMOCRATIC PEOPLES REPUBLIC OF',
    'KR'=>'KOREA REPUBLIC OF',
    'KW'=>'KUWAIT',
    'KG'=>'KYRGYZSTAN',
    'LA'=>'LAO PEOPLES DEMOCRATIC REPUBLIC',
    'LV'=>'LATVIA',
    'LB'=>'LEBANON',
    'LS'=>'LESOTHO',
    'LR'=>'LIBERIA',
    'LY'=>'LIBYAN ARAB JAMAHIRIYA',
    'LI'=>'LIECHTENSTEIN',
    'LT'=>'LITHUANIA',
    'LU'=>'LUXEMBOURG',
    'MO'=>'MACAU',
    'MK'=>'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF',
    'MG'=>'MADAGASCAR',
    'MW'=>'MALAWI',
    'MY'=>'MALAYSIA',
    'MV'=>'MALDIVES',
    'ML'=>'MALI',
    'MT'=>'MALTA',
    'MH'=>'MARSHALL ISLANDS',
    'MQ'=>'MARTINIQUE',
    'MR'=>'MAURITANIA',
    'MU'=>'MAURITIUS',
    'YT'=>'MAYOTTE',
    'MX'=>'MEXICO',
    'FM'=>'MICRONESIA, FEDERATED STATES OF',
    'MD'=>'MOLDOVA, REPUBLIC OF',
    'MC'=>'MONACO',
    'MN'=>'MONGOLIA',
    'MS'=>'MONTSERRAT',
    'MA'=>'MOROCCO',
    'MZ'=>'MOZAMBIQUE',
    'MM'=>'MYANMAR',
    'NA'=>'NAMIBIA',
    'NR'=>'NAURU',
    'NP'=>'NEPAL',
    'NL'=>'NETHERLANDS',
    'AN'=>'NETHERLANDS ANTILLES',
    'NC'=>'NEW CALEDONIA',
    'NZ'=>'NEW ZEALAND',
    'NI'=>'NICARAGUA',
    'NE'=>'NIGER',
    'NG'=>'NIGERIA',
    'NU'=>'NIUE',
    'NF'=>'NORFOLK ISLAND',
    'MP'=>'NORTHERN MARIANA ISLANDS',
    'NO'=>'NORWAY',
    'OM'=>'OMAN',
    'PK'=>'PAKISTAN',
    'PW'=>'PALAU',
    'PS'=>'PALESTINIAN TERRITORY, OCCUPIED',
    'PA'=>'PANAMA',
    'PG'=>'PAPUA NEW GUINEA',
    'PY'=>'PARAGUAY',
    'PE'=>'PERU',
    'PH'=>'PHILIPPINES',
    'PN'=>'PITCAIRN',
    'PL'=>'POLAND',
    'PT'=>'PORTUGAL',
    'PR'=>'PUERTO RICO',
    'QA'=>'QATAR',
    'RE'=>'REUNION',
    'RO'=>'ROMANIA',
    'RU'=>'RUSSIAN FEDERATION',
    'RW'=>'RWANDA',
    'SH'=>'SAINT HELENA',
    'KN'=>'SAINT KITTS AND NEVIS',
    'LC'=>'SAINT LUCIA',
    'PM'=>'SAINT PIERRE AND MIQUELON',
    'VC'=>'SAINT VINCENT AND THE GRENADINES',
    'WS'=>'SAMOA',
    'SM'=>'SAN MARINO',
    'ST'=>'SAO TOME AND PRINCIPE',
    'SA'=>'SAUDI ARABIA',
    'SN'=>'SENEGAL',
    'SC'=>'SEYCHELLES',
    'SL'=>'SIERRA LEONE',
    'SG'=>'SINGAPORE',
    'SK'=>'SLOVAKIA',
    'SI'=>'SLOVENIA',
    'SB'=>'SOLOMON ISLANDS',
    'SO'=>'SOMALIA',
    'ZA'=>'SOUTH AFRICA',
    'GS'=>'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS',
    'ES'=>'SPAIN',
    'LK'=>'SRI LANKA',
    'SD'=>'SUDAN',
    'SR'=>'SURINAME',
    'SJ'=>'SVALBARD AND JAN MAYEN',
    'SZ'=>'SWAZILAND',
    'SE'=>'SWEDEN',
    'CH'=>'SWITZERLAND',
    'SY'=>'SYRIAN ARAB REPUBLIC',
    'TW'=>'TAIWAN, PROVINCE OF CHINA',
    'TJ'=>'TAJIKISTAN',
    'TZ'=>'TANZANIA, UNITED REPUBLIC OF',
    'TH'=>'THAILAND',
    'TG'=>'TOGO',
    'TK'=>'TOKELAU',
    'TO'=>'TONGA',
    'TT'=>'TRINIDAD AND TOBAGO',
    'TN'=>'TUNISIA',
    'TR'=>'TURKEY',
    'TM'=>'TURKMENISTAN',
    'TC'=>'TURKS AND CAICOS ISLANDS',
    'TV'=>'TUVALU',
    'UG'=>'UGANDA',
    'UA'=>'UKRAINE',
    'AE'=>'UNITED ARAB EMIRATES',
    'GB'=>'UNITED KINGDOM',
    'US'=>'UNITED STATES',
    'UM'=>'UNITED STATES MINOR OUTLYING ISLANDS',
    'UY'=>'URUGUAY',
    'UZ'=>'UZBEKISTAN',
    'VU'=>'VANUATU',
    'VE'=>'VENEZUELA',
    'VN'=>'VIET NAM',
    'VG'=>'VIRGIN ISLANDS, BRITISH',
    'VI'=>'VIRGIN ISLANDS, U.S.',
    'WF'=>'WALLIS AND FUTUNA',
    'EH'=>'WESTERN SAHARA',
    'YE'=>'YEMEN',
    'YU'=>'YUGOSLAVIA',
    'ZM'=>'ZAMBIA',
    'ZW'=>'ZIMBABWE',
    ];

    $src_job_year_experience_index = strpos($src_job_year_experience, 'to');
    $years_exp_start = $src_job_year_experience;
    $years_exp_end = '';

    if ($src_job_year_experience_index) {
        $years_exp_start = substr($src_job_year_experience, 0, $src_job_year_experience_index - 1);
        $pos = $src_job_year_experience_index + 3;
        $years_exp_end = substr($src_job_year_experience, $pos);
    }

    $time = new FrozenTime($closing_date);
    $closing_date = $time->format('Y-m-d H:i:s');
    $closing_date_pos = strpos($closing_date, '00:00:00');
    $closing_date = substr($closing_date, 0, $closing_date_pos - 1);
?>

<div class="employers-joboist-edit-btn">
    <span onclick="javascript: job_view(<?= $id ?>)">Go Back</span>
</div>
<div class="employers-jobpost-edit">
    <div class="post-jobs-form">
        <field>
            <div class="postjobs-headers">
                <div class="header-upload">
                    <div class="header-img-container"></div>
                    <div class="loading">
                        <img src="/img/loading.gif" />
                    </div>
                    <?= $this->Form->hidden('id', ['id' => 'id', 'value' => $id]); ?>
                    <?= $this->Form->label('header-image', 'Upload Your Banner Image'); ?>
                    <?= $this->Form->file('header-image', ['id' => 'header-image']); ?>
                    <?= $this->Form->hidden('header-img', ['id' => 'header-img', 'value' => $src_header_image]); ?>
                </div>
                <div class="header-btn-cancel-container">
                    <span class="header-cancel">Cancel</span>
                </div>
                <div class="the_logo">
                    <div class="company-logo-container">
                        <div class="logo-img-container"></div>
                        <div class="loading">
                            <img src="/img/loading.gif" />
                        </div>
                        <?= $this->Form->label('logo', 'Upload Company Logo Here') ?>
                        <?= $this->Form->file('logo-image', ['id' => 'logo']) ?>
                        <?= $this->Form->hidden('logo-img', ['id' => 'logo-img', 'value' => $src_company_logo]) ?>
                    </div>
                        <div class="flash-error-logo"></div>
                        <div class="logo-cancel-container">
                        <span class="logo-cancel">Cancel</span>
                    </div>
                </div>
            </div>
            <div class="inputs">
                <div class="company-name-container">
                    <div class="input">
                        <?= $this->Form->label('job-title', 'Job Title:') ?>
                        <?= $this->Form->text('job-title', ['id' => 'job-title', 'class' => 'job-title', 'maxlength' => 255, "value" => $src_job_title]) ?>
                        <div class="postjob-error" id="job-title-error"></div>
                    </div>
                    <div class="input">
                        <?= $this->Form->label('company-name', 'Company Name:'); ?>
                        <?= $this->Form->text('company-name', ['id' => 'company-name', 'class' => 'company-name', 'maxlength' => 255, 'value' => $src_company_name]) ?>
                        <div class="postjob-error" id="company-name-error"></div>
                    </div>
                    <div class="input">
                        <?= $this->Form->label('job-category', 'Job Category:') ?>
                        <?= $this->Form->select('job-category', $categories, ['id' => 'job-category', 'default' => $src_job_category]) ?>
                        <div class="postjob-error" id="job-category-error"></div>
                    </div>
                    <div class="input">
                        <?= $this->Form->label('job-type', 'Job Type:') ?>
                        <?= $this->Form->select('job-type', $job_types, ['id' => 'job-type', 'default' => $src_job_type]) ?>
                        <div class="postjob-error" id="job-type-error"></div>
                    </div>
                    <div class="input">
                        <?= $this->Form->label('job-base', 'Job Base:') ?>
                        <?= $this->Form->select('job-base', $job_base, ['id' => 'job-base', 'default' => $src_job_migration]) ?>
                        <div class="postjob-error" id="job-base-error"></div>
                    </div>
                    <div class="input">
                        <?= $this->Form->label('base-on', 'Base on:') ?>
                        <?= $this->Form->select('country', $countries, ['id' => 'country', 'default' => $src_country]) ?>
                        <div class="postjob-error" id="country-error"></div>
                    </div>
                    <div class="input ck-container">
                        <?= $this->Form->label('description', 'Description:') ?>
                        <?= $this->Form->textarea('description', ['id' => 'description', 'value' => $src_job_description]) ?>
                        <div class="postjob-error" id="job-description-error"></div>
                    </div>

                    <h3>Add Qualifications</h3>
                    <div class="qualifications">
                        <div class="qlf_contents"></div>
                        <div class="btn-container">
                            <span id="add-qlf">Add Qualification</span>
                        </div>
                    </div>

                    <h3>Add Responsibilities</h3>
                    <div class="responsibilities">
                        <div class="res-contents"></div>
                        <div class="btn-container">
                            <span id="add-res">Add Responsibility</span>
                        </div>
                    </div>

                    <div class="input">
                        <?= $this->Form->label('years-exp', 'Years of Working Experience:') ?>
                        <div class="years-exps">
                            <?= $this->Form->select('years-exp-start', $job_year_exp_start, ['id' => 'years-exp-start', 'class' => 'years-exp-start', 'default' => $years_exp_start]) ?> to 
                            <?= $this->Form->select('years-exp-end', $job_year_exp_end, ['id' => 'years-exp-end', 'class' => 'years-exp-end', 'default' => $years_exp_end]) ?>
                        </div>
                    </div>
                    <div class="input">
                        <?= $this->Form->label('job-location', 'Location:') ?>
                        <?= $this->Form->textarea('job-location', ['id' => 'job-location', 'class' => 'job-location' , 'value' => $src_job_location]) ?>
                    </div>
                    <div class="input google_map_container">
                        <?= $this->Form->control('google_map', ['type' => 'checkbox', 'id' => 'googlemap', 'label' => 'Enable Google Map', ($googglemap) ? 'checked' : '']); ?>
                        <div id="map"></div>
                    </div>
                    <div class="input">
                        <?= $this->Form->label('job-website', 'Company Website:') ?>
                        <?= $this->Form->text('job-website', ['id' => 'job-website', 'class' => 'job-website', 'maxlength' => 255, 'value' => $src_job_website]) ?>
                    </div>
                    <div class="input">
                        <?= $this->Form->label('job-email', 'Company Email:') ?>
                        <?= $this->Form->text('job-email', ['id' => 'job-email', 'class' => 'job-email', 'value' => $src_job_email]) ?>
                    </div>
                    <div class="input">
                        <?= $this->Form->label('job-contact-number', 'Company Contact Number:') ?>
                        <?= $this->Form->text('job-contact-number', ['id' => 'job-contact-number', 'class' => 'job-contact-number', 'maxlength' => 255, 'value' => $src_job_contact_number]) ?>
                    </div>
                    <div class="input">
                        <?= $this->Form->label('closing-date', 'Closing Date:') ?>
                        <?= $this->Form->text('closing-date', ['id' => 'closing-date', 'class' => 'closing-date', 'maxlength' => 255, 'value' => $closing_date]) ?>
                        <div class="postjob-error" id="job-closing-error"></div>
                    </div>
                </div>
                <?= $this->Form->hidden('googlemap', ['id' => 'googlemap_val', 'value' => $googglemap]); ?>
                <div class="submit-btns">
                    <?= $this->Form->button('Save', ['id' => 'save', 'type' => 'button']); ?> <?= $this->Form->button('Go Back', ['id' => 'go-back2', 'type' => 'button', 'onclick' => 'javascript: job_view(' . $id . ')']); ?>
                </div>
            </div>
        </field>
    </div>
</div>

<?= $this->Html->script('add-qualifications'); ?>
<?= $this->Html->script('add-responsibilities') ?>

<script>
    var header_tmp = false;
    var logo_tmp = false;
    
    var header_file = "";
    var logo_file = "";

    var googgle_map_bool = false;

    <?php if ($googglemap): ?>
        _map();
    <?php endif; ?>

    $("#googlemap").on("change", function() {
        _map();
    });

    function _map() {
        if ($("#googlemap").is(":checked") && $("#job-location").val() !== "" && !googgle_map_bool) {
            googgle_map_bool = true;

            var loc = $("#job-location").val();
            $("#googlemap_val").attr("value", 1);

            var data = {
                "operation": "map_preview",
                "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
                "location": loc,
                "country": $("#country").val()
            };

            $.post("/ajax", data, function(r) {
                $("#map").show().html(r);
            });
        } else {
            googgle_map_bool = false;

            $("#googglemap_val").attr("value", 0);
            $("#map").hide();
        }
    }
    
    if ($("#header-img").val() !== "") {
        has_header_img();
    }
    
    if ($("#logo-img").val() !== "") {
        has_logo_img();
    }


    $("#job-title").on("click focus", function() {
        inputAnimate("job-title");
    }).on("focusout", function() {
        focusoutAnimate("job-title");
    });

    $("#company-name").on("click focus", function() {
        inputAnimate("company-name");
    }).on("focusout", function() {
        focusoutAnimate("company-name");
    });

    $("#job-website").on("click focus", function() {
        inputAnimate("job-website");
    }).on("focusout", function() {
        focusoutAnimate("job-website");
    });

    $("#job-email").on("click focus", function() {
        inputAnimate("job-email");
    }).on("focusout", function() {
        focusoutAnimate("job-email");
    });

    $("#job-contact-number").on("click focus", function() {
        inputAnimate("job-contact-number");
    }).on("focusout", function() {
        focusoutAnimate("job-contact-number");
    });

    // $("#closing-date").on("click focus", function() {
    //     inputAnimate("closing-date");
    // }).on("focusout", function() {
    //     focusoutAnimate("closing-date");
    // });

    $("#job-location").on("click focus", function() {
        $(this).animate({
            height: "100px"
        }, 500, function() {
            $(this).css({
                "border": "2px solid #ccc"
            });
        });
    }).on("focusout", function() {
        $(this).animate({
            height: "20px"
        }, 500, function() {
            $(this).css({
                "border": 0,
                "border-bottom": "2px solid #ccc"
            });
        });
    });

    CKEDITOR.env.isCompatible = true;

    CKEDITOR.replace('description', {
        on : {
            instanceReady : function( ev ) {
                this.dataProcessor.writer.setRules( 'p', {
                    indent : false,
                    breakBeforeOpen : false,
                    breakAfterOpen : false,
                    breakBeforeClose : false,
                    breakAfterClose : false
                });
            }
        },
        toolbar: [
            { name: 'basicstyles', groups: [ 'basicstyles'], items: [ 'Bold', 'Italic', 'Underline', 'Strike', '-', 'TextColor'] },
            { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
            { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] }
        ],
        height: "250px"
    });

    CKEDITOR.instances['description'].setData($("#description").val());
    
    <?php if ($src_job_qualifications): ?>
        <?php foreach (json_decode($src_job_qualifications, true) as $qlf): ?>
            add_qlf("<?= $qlf ?>");
        <?php endforeach; ?>
    <?php endif; ?>
    
    <?php if ($src_job_responsibilities): ?>
        <?php foreach (json_decode($src_job_responsibilities, true) as $res): ?>
            add_res("<?= $res ?>");
        <?php endforeach; ?>
    <?php endif; ?>
    
    $("#closing-date").datepicker({
        dateFormat: "yy-mm-dd"
    });


    
    $(".header-upload").on("dragover", function() {
         $(this).css({
            "border": "1px solid #ff4338",
            "background-color": "#f5c8c5"
        });
        
        return false;
    }).on("dragleave", function() {
        $(this).css({
            "border": "2px solid #ccc",
            "background-color": "#fff"
        });
        
        return false;
    }).on("drop", function(e) {
        e.preventDefault();
        
        $(this).css({
            "border": "2px solid #ccc",
            "background-color": "#fff"
        });
        
        header_file = e.originalEvent.dataTransfer.files[0];
        
        header_validation_upload();
    });
    
    $("#header-image").on("change", function() {
        header_file = $(this).get(0).files[0];
        header_validation_upload();
    });
    
    $(".header-cancel").on("click", function() {
        if ($("#header-img").val() !== "") {
            var data = {
                "operation": "employers_jobpost_delete_header_tmp",
                "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>",
                "filename": $("#header-img").val()
            };
            
            $.post("/ajax", data, function(r) {
                if (r == "ok") {
                    $("#header-img").attr("value", "");
                }
            });
        } else {
            $("#header-img").attr("value", "");
        }
        
        $(".header-upload").css({
            "border": "2px solid #ccc",
            "background-color": "#fff"
        });
        
        $("#header-image").attr("value", "");
        header_file = "";
        $(".header-btn-cancel-container").hide();
        $(".header-upload .header-img-container").html("");
        $(".header-upload label").css("display", "block"); 
        header_tmp = true;
    });
    
    $(".logo-cancel").on("click", function() {
        if ($("#logo-img").val() !== "") {
            var data = {
                "operation": "employers_jobpost_delete_logo_tmp",
                "_csrfToken": "<?= $this->request->getParam("_csrfToken") ?>",
                "filename": $("#logo-img").val()
            };
            
            $.post("/ajax", data, function(r) {
                if (r == "ok") {
                    $("#logo-img").attr("value", "");
                }
            });
        }
        
        $("#logo").attr("value", "");
        logo_file = "";
        $(".logo-img-container").html("");
        $(".logo-cancel-container").hide();
        $(".company-logo-container label").show();
    });
    
    $("#logo").on("change", function() {
        logo_file = $(this).get(0).files[0];
        logo_validation_upload();
    });
    
    $(".company-logo-container label").on("dragover", function() {
        $(this).css({
            "border": "1px solid #ff4338",
            "background-color": "#f5c8c5"
        });
        
        return false;
    }).on("dragleave", function() {
        $(this).css({
            "border": "1px solid #ccc",
            "background-color": "#fff"
        });
        
        return false;
    }).on("drop", function(e) {
        $(this).css({
            "border": "1px solid #ccc",
            "background-color": "#fff"
        });
        
        e.preventDefault();
        
        logo_file = e.originalEvent.dataTransfer.files[0];
        
        logo_validation_upload();
    });
    
    $("#save").on("click", function() {
        if (form_validation()) {
            var id = $("#id").val();
            var header_img = $("#header-img").val();
            var company_logo = $("#logo-img").val();
            var job_title = $("#job-title").val();
            var company_name = $("#company-name").val();
            var job_category = $("#job-category").val();
            var job_type = $("#job-type").val();
            var job_base = $("#job-base").val();
            var country = $("#country").val();
            var description = $("#description").val();
            var qualifications = [];
            var responsibilities = [];
            
            if ($(".qlf-entry").length > 0) {

                $(".qlf-entry").each(function(i) {
                    qualifications[i] = $(this).val();
                });
            }

            if ($(".res").length > 0) {
                $(".res").each(function(i) {
                    responsibilities[i] = $(this).val();
                });
            }

            var years_exp_start = $("#years-exp-start").val();
            var years_exp_end = $("#years-exp-end").val();
            var job_location = $("#job-location").val();
            var job_website = $("#job-website").val();
            var job_email = $("#job-email").val();
            var job_contact_number = $("#job-contact-number").val();
            var googglemap = $("#googlemap_val").val();
            var closing_date = $("#closing-date").val();
            
            var data = {
                "operation": "employers_jobpost_edit_save",
                "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>",
                "id": id,
                "header_image": header_img,
                "company_logo": company_logo,
                "job_title": job_title,
                "company_name": company_name,
                "job_category": job_category,
                "job_base": job_base,
                "country": country,
                "description": description,
                "qualifications": qualifications,
                "responsibilities": responsibilities,
                "years_exp_start": years_exp_start,
                "years_exp_end": years_exp_end,
                "job_location": job_location,
                "job_email": job_email,
                "job_website": job_website,
                "job_contact_number": job_contact_number,
                "googglemap": googglemap,
                "closing_date": closing_date
            };

            $.post("/ajax", data, function(r) {
                if (r == "ok") {
                    $(".employers-jobpost-view").animate({
                        top: "-100%"
                    }, 500, function() {
                        $(this).html("");
                        
                        if ($(window).width() > 800) {
                            $(".main-header").show();
                        }

                        $("body").css({
                            "overflow": "auto"
                        });

                        location.reload();
                        $(window).scrollTop(0);
                    });
                }
            });
        }
    });
    
    function header_validation_upload() {
        var filename = header_file['name'];
        var size = parseFloat(header_file.size / 1024).toFixed(2);
        var type = header_file.type;
        
        var file_types = ["image/jpeg", "image/png"];
        
        if ($.inArray(type, file_types) == -1) {
            $(".header-upload label").css({
                "color": "red",
                "background-color": "#fde1e1"
            }).html("This is not an image file. jpg and png only.");
            header_file = "";
        } else if (size > 2048) {
            $(".header-upload label").css({
                "color": "red",
                "background-color": "#fde1e1"
            }).text("Image size too large.");
            header_file = "";
        } else {
            var image = new Image();
            image.src = window.URL.createObjectURL(header_file);
            image.onload = function() {
                var width = this.width;
                var height = this.height;
                
                if (width < 900) {
                    $(".header-upload label").css({
                        "color": "red",
                        "background-color": "#fde1e1"
                    }).text("Image width should be 900 pixels or heigher.");
                    header_file = "";
                } else if (height < 200) {
                    $(".header-upload label").css({
                        "color": "red",
                        "background-color": "#fde1e1"
                    }).text("Image height should be 200 pixels or heigher.");
                    header_file = "";
                } else {
                    var formData = new FormData();
                    
                    formData.append("operation", "employers_jobpost_upload_new_header");
                    formData.append("_csrfToken", "<?= $this->request->getParam('_csrfToken') ?>");
                    formData.append("header-image", header_file);
                    
                    $.ajax({
                        url: "/ajax",
                        method: "post",
                        contentType: false,
                        catch: false,
                        processData: false,
                        data: formData,
                        xhr: function() {
                            var xhr = $.ajaxSettings.xhr();
                            xhr.upload.addEventListener("progress", function() {
                                $(".header-upload .loading").hide();
                            }, false);
                            
                            return xhr;
                        },
                        success: function(r) {
                            $(".header-upload .loading").show();
                            $(".progress-container, .flash-error-header").hide();
                            $(".header-upload label").hide();
                            $(".header-upload .header-img-container").css({"display": "none"});
                            
                            setTimeout(function() {
                                $(".header-upload label").css({
                                    "border": "1px solid #ccc",
                                    "background-color": "#fff",
                                    "color": "#000"
                                }).html("Upload Your Banner Image");
                                
                                $(".header-upload .loading").hide();
                                $(".header-btn-cancel-container").show();
                                $(".header-upload").css({
                                    "border": "none",
                                    "background-color": "transparent"
                                });
                                $("#header-img").attr("value", r);
                                $(".header-upload .header-img-container").fadeIn(500, function() {
                                    dashboardHeight();
                                }).html("<img src='/img/users/job_header_tmp/" + r + "' />");
                            }, 3000);
                        }
                    });
                }
            }
        }
    }
    
    function has_header_img() {
        $(".header-upload .loading").show();
        $(".progress-container, .flash-error-header").hide();
        $(".header-btn-cancel-container").show();
        $(".header-upload label").hide();
        $(".header-upload .header-img-container").css({"display": "none"});

        setTimeout(function() {
            $(".header-upload .loading").hide();
            $(".header-upload").css({
                "border": "none",
                "background-color": "transparent"
            });
            $("#header-img").attr("value", $(".header-img").val());
            $(".header-upload .header-img-container").fadeIn(500).html("<img src='/img/users/job_header/" + $("#header-img").val() + "' />");
        }, 1000);
    }
    
    function logo_validation_upload() {
        var filename = logo_file['name'];
        var size = parseFloat(logo_file.size / 1024).toFixed(2);
        var type = logo_file.type;
        
        var file_types = ["image/jpeg", "image/png"];
        
        if ($.inArray(type, file_types) == -1) {
            $(".flash-error-logo").show().css("color", "red").html("This is not an image file.");
            logo_file = "";
        } else if (size > 1024) {
            $(".flash-error-logo").show().css("color", "red").html("Image size too large.");
            logo_file = "";
        } else {
            var formData = new FormData();
            
            formData.append("operation", "employers_jobpost_upload_new_logo");
            formData.append("_csrfToken", "<?= $this->request->getParam('_csrfToken') ?>");
            formData.append("logo-image", logo_file);
            
            $.ajax({
                url: "/ajax",
                method: "post",
                contentType: false,
                processData: false,
                catch: false,
                data: formData,
                xhr: function() {
                    var xhr = $.ajaxSettings.xhr();
                    xhr.upload.addEventListener("progress", function() {
                        $(".company-logo-container .loading").hide();
                    }, false);
                    
                    return xhr;
                },
                success: function(r) {
                    $(".company-logo-container label").hide();
                    $(".logo-progress-container").hide();
                    $(".company-logo-container .loading").show();
                    $(".flash-error-logo").hide();
                    
                    setTimeout(function() {
                        $("#logo-img").attr("value", r);
                        $(".company-logo-container .loading").hide();
                        $(".logo-cancel-container").show();
                        $(".logo-img-container").fadeIn(500, function() {
                            dashboardHeight();
                        }).html("<img src='/img/users/company_logo_tmp/" + r + "' />");
                    }, 3000);
                }
            });
        }
    }
    
    function has_logo_img() {
        $(".company-logo-container label").hide();
        $(".logo-progress-container").hide();
        $(".company-logo-container .loading").show();
        $(".flash-error-logo").hide();
        $(".logo-img-container").hide();

        setTimeout(function() {
            $(".company-logo-container .loading").hide();
            $("#logo-img").attr("value", $("#logo-img").val());
            $(".logo-cancel-container").show();
            $(".logo-img-container").fadeIn(500, function() {
                dashboardHeight();
            }).html("<img src='/img/users/company_logo/" + $("#logo-img").val() + "' />");
        }, 1000);
    }
    
    function form_validation() {
        var bool = true;
        
        if ($("#job-title").val() == "") {
            bool = false;
            $("#job-title").css("border-color", "red").focus();
            $("#job-title-error").css("color", "red").html("Job Title is required.");
        } else if ($("#job-title").val().length < 3) {
            bool = false;
            $("#job-title").css("border-color", "red").focus();
            $("#job-title-error").css("color", "red").html("Job Title is too short.");
        } else if ($("#company-name").val() == "") {
            bool = false;
            $("#company-name").css("border-color", "red").focus();
            $("#company-name-error").css("color", "red").html("Company Name is required.");
        } else if ($("#job-category").val() == "") {
            bool = false;
            $("#job-category").css("border-color", "red").focus();
            $("#job-category-error").css("color", "red").html("Job Category is required.");
        } else if ($("#job-type").val() == "") {
            bool = false;
            $("#job-type").css("border-color", "red").focus();
            $("#job-type-error").css("color", "red").html("Job Title is too short.");
        } else if ($("#job-base").val() == "") {
            bool = false;
            $("#job-base").css("border-color", "red").focus();
            $("#job-base-error").css("color", "red").html("Job Base is required."); 
        } else if ($("#country").val() == "") {
            bool = false;
            $("#country").css("border-color", "red").focus();
            $("#country-error").css("color", "red").html("Base On is required."); 
        } else if (CKEDITOR.instances["description"].getData() == "") {
            bool = false;
            $("#country").focus();
            $("#job-description-error").css("color", "red").html("Description is required."); 
        } else if ($("#closing-date").val() == "") {
            bool = false;
            $("#closing-date").css("border-color", "red").focus();
            $("#job-closing-error").css("color", "red").html("Closing Date is required."); 
        }
        
        return bool;
    }
    
    $("#job-title").on("keyup input", function() {
        $("#job-title").css("border-color", "#ccc");
        $("#job-title-error").html("");
    });
    
    $("#company-name").on("keyup input", function() {
        $("#job-title").css("border-color", "#ccc");
        $("#job-title-error").html("");
    });
    
    $("#job-category").on("change", function() {
        $("#job-category").css("border-color", "#ccc");
        $("#job-category-error").html("");
    });
    
    $("#job-type").on("change", function() {
        $("#job-type").css("border-color", "#ccc");
        $("#job-type-error").html("");
    });
    
    $("#job-base").on("change", function() {
        $("#job-base").css("border-color", "#ccc");
        $("#job-base-error").html("");
    });
    
    $("#description").on("keyup input", function() {
        $("#description").css("border-color", "#ccc");
        $("#description-error").html("");
    });
    
    $("#closing-date").on("change input", function() {
        $("#closing-date").css("border-color", "#ccc");
        $("#job-closing-error").html("");
    });

    for (var i in CKEDITOR.instances) {
        CKEDITOR.instances[i].on('change', function(e) {
            $("#description").attr("value", CKEDITOR.instances["description"].getData());
            $("#job-description-error").html("");
        });
    }

    function inputAnimate(field) {
        $("#" + field).animate({
            padding: "10px 5px"
        }, 500, function() {
            $("#" + field).css({
                "font-size": "18px"
            });
        });
    }

    function focusoutAnimate(field) {
        $("#" + field).animate({
            padding: "0px"
        }, 500, function() {
            $("#" + field).css({
                "font-size": "15px"
            });
        });
    }
</script>