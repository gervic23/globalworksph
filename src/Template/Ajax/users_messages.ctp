<?php use App\Controller\GlobalworksController; $globalworks = new GlobalworksController(); ?>

<div class="msgs-options">
    <ul>
        <li><span id="check-all">Check All</span></li>
        <li><span id="del">Delete</span></li>
    </ul>
</div>
<div class="message-list">
    <?php if ($count == 0): ?>
        <div class="no-msgs">
            There are no <?= $name; ?> at this time.
        </div>
    <?php else: ?>
        <table>
            <?php foreach ($messages as $message): ?>
                <?php if ($message['unread'] == 1): ?>
                    <tr>
                        <td><?= $this->Form->checkbox('message', ['id' => $message['id'], 'class' => 'message']) ?></td>
                        <td><strong><span onclick="javascript: readMessage(<?= $message['id'] ?>)"><?= $message['subject'] ?></span></strong></td>
                        <td><?= $globalworks->when($message['date']) ?></td>
                    </tr>
                <?php else: ?>
                    <tr>
                        <td><?= $this->Form->checkbox('message', ['id' => $message['id'], 'class' => 'message']) ?></td>
                        <td><span onclick="javascript: readMessage(<?= $message['id'] ?>)"><?= $message['subject'] ?></span></td>
                        <td><?= $globalworks->when($message['date']) ?></td>
                    </tr>
                <?php endif; ?>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
</div>

<script>
    var check_bool = false;

    $("#check-all").on("click", function() {
        if (!check_bool) {
            check_bool = true;

            $(this).html("Uncheck All");

            $(".message-list table td [type=checkbox]").each(function(i) {
                $(this).prop("checked", true);
            });
        } else {
            check_bool = false;

            $(this).html("Check All");

            $(".message-list table td [type=checkbox]").each(function(i) {
                $(this).prop("checked", false);
            });
        }
    });
    
    $("#close, .black-screen").on("click", function() {
        $(".message-view").slideUp(500, function() {
            $(".main-header").css({
                "z-index": 15
            });
        
            $("body").css({
                "overflow-y": "auto"
            });
        
            $(".black-screen").hide();
            
            $(".message-view").css({
               "text-align": "center",
               "line-height": new_height + "px" 
            }).html("");
        });
    });
    
    $("#del").on("click", function() {
        $(".message-list table td [type=checkbox]").each(function(i) {
            if ($(this).is(":checked")) {
                var id = $(this).attr("id");
                var data2 = {
                    "operation": "user_message_delete",
                    "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
                    "id": id
                };
                
                $.post("/ajax", data2, function(r2) {
                    if (r2) {
                        $(".messages-contents").css({
                            "text-align": "center"
                        }).html('<img src="/img/loading.gif" />');

                        var data3 = {
                            "operation": page,
                            "_csrfToken": "<?= $this->request->param('_csrfToken') ?>"
                        };

                        $.post("/ajax", data3, function(r) {
                            $(".messages-contents").css({
                                "text-align": "left"
                            }).html(r);
                        });
                    }
                });
            }
        });
    });
</script>