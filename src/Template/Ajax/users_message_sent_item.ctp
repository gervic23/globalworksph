<?php use App\Controller\GlobalworksController; $globalworks = new GlobalworksController(); ?>

<div class="message-main">
    <div class="subject"><?= $subject ?></div>
    <div class="from">
        <?= ($profile_picture) ? '<img src="/img/users/profile_thumbnails/'. $profile_picture .'" />' : '<img src="/img/users/nopropic.png" />'; ?>
        <?= $firstname . ' ' . $lastname ?>
    </div>
    <div class="date"><?= $globalworks->when($date) ?></div>
    <div class="mesage-container">
        <div class="message-cnt"><?= $message ?></div>
    </div>
    <div class="btns">
        <span id="delete">Delete</span> <span id="close">Close</span> 
    </div>
</div>

<script>
    
    setMessageSize();
    
    function setMessageSize() {
        var height = $(".message-view").height();
        var new_height = height - 170 + "px";
        
        $(".message-cnt").slimScroll({
            height: new_height,
            color: "#ccc",
            alwaysVisible: true,
            size: "10px"
        });
    }

    $("#delete").on("click", function() {
        var data2 = {
            "operation": "user_message_delete",
            "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
            "id": <?= $this->request->data('id') ?>
        };
            
        $.post("/ajax", data2, function(r2) {
            if (r2 == "ok") {
                $(".message-view").slideUp(500, function() {
                    $(".main-header").css({
                    "z-index": 15
                });

                $("body").css({
                    "overflow-y": "auto"
                });

                $(".black-screen").hide();

                    $(this).css({
                        "text-align": "center",
                        "line-height": new_height + "px" 
                    }).html("");
                });

                var data3 = {
                    "operation": page,
                    "_csrfToken": "<?= $this->request->param('_csrfToken') ?>"
                };
    
                $(".messages-contents").css({
                    "text-align": "center"
                }).html('<img src="/img/loading.gif" />');

                $.post("/ajax", data3, function(r) {
                    $(".messages-contents").css({
                        "text-align": "left"
                    }).html(r);
                });
            }
        });
    });
</script>
