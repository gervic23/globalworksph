<?php use App\Controller\GlobalworksController; $globalworks = new GlobalworksController(); ?>
<div class="employers-jobopostview-btn">
    <span id="close">Close This Window</span>
</div>

<?php if ($jobpost_count == 0): ?>
    <!-- -->
<?php else: ?>
    <div class="employers-jobpostview">
        <?php foreach ($jobpost as $post): ?>
            <div class="em-jobpost-main-header">
                <div class="header-logo">
                    <?php if ($post['header_image']): ?>
                        <div class="header">
                            <img src="/img/users/job_header/<?= $post['header_image'] ?>" />
                        </div>
                    <?php else: ?>
                        <script>
                            $(".employers-jobpostview .logo").css({
                                "margin-top": "0px",
                                "position": "inherit"
                            });
                        </script>
                    <?php endif; ?>

                    <?php if ($post['company_logo']): ?>
                        <div class="logo">
                            <img src="/img/users/company_logo/<?= $post['company_logo'] ?>" />
                        </div>
                    <?php endif; ?>
                </div>
                
                <h1><?= $post['job_title'] ?></h1>
                <h4><?= $post['company_name'] ?></h4>
            </div>
            
            <div class="contents">
                <div class="left">
                    <div class="description">
                        <h2>Job Description</h2>
                        <?= $globalworks->filter_description($post['job_description']) ?>
                    </div>
                    
                    <?php if ($post['job_website']): ?>
                        <div class="website">
                            <h2>Company Website</h2>
                            <?php
                                $str1 = substr($post['job_website'], 0, 7) ;
                                $str2 = substr($post['job_website'], 0, 8) ;

                                if ($str1 == 'http://' || $str2 == 'https://') {
                                    echo '<a target="_blank" href="' . $post['job_website'] . '">' . $post['job_website'] . '</a>';
                                } else {
                                    echo '<a target="_blank" href="http://' . $post['job_website'] . '">http://' . $post['job_website'] . '</a>';
                                }
                            ?>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="right">
                    <div class="job-details">
                        <h2>Job Details</h2>
                        <table>
                            <tr>
                                <td>Job Category:</td>
                                <td><?= $post['job_category'] ?></td>
                            </tr>
                            <tr>
                                <td>Job Type:</td>
                                <td><?= $post['job_type'] ?></td>
                            </tr>
                            <tr>
                                <td>Job Base:</td>
                                <td><?= $globalworks->country_to_str($post['country']) ?></td>
                            </tr>
                            <tr>
                                <td>Working Experience:</td>
                                <td><?= $post['job_years_experience'] ?></td>
                            </tr>
                            <tr>
                                <td>Posted By:</td>
                                <td><?= $nickname ?></td>
                            </tr>
                            <tr>
                                <td>Closing Date:</td>
                                <td><?= $globalworks->datetimetodate($post['closing_date']) ?></td>
                            </tr>
                            <tr>
                                <td>Created At:</td>
                                <td><?= $globalworks->when($post['created_at']) ?></td>
                            </tr>
                            <tr>
                                <td>Modified At:</td>
                                <td><?= $globalworks->when($post['modified_at']) ?></td>
                            </tr>
                        </table>
                    </div>
                    <?php if ($post['job_email'] || $post['job_contct_number']): ?>
                        <div class="contact-info">
                            <h2>Contact Information</h2>
                            <table>
                                <?php if ($post['job_email']): ?>
                                    <tr>
                                        <td>Email:</td>
                                        <td><?= $post['job_email'] ?></td>
                                    </tr>
                                <?php endif; ?>

                                <?php if ($post['job_contact_number']): ?>
                                    <tr>
                                        <td>Contact Number:</td>
                                        <td><?= $post['job_contact_number'] ?></td>
                                    </tr>
                                <?php endif; ?>
                            </table>
                        </div>
                    <?php endif; ?>
                    
                    <?php if ($post['job_location']): ?>
                        <div class="location">
                            <h2>Location</h2>
                            <?= $post['job_location'] ?>
                            <div class="country"><?= $globalworks->country_to_str($post['country']) ?>.</div>
                        </div>
                    <?php endif; ?>

                    <?php if ($post['googglemap']): $location = urlencode($post['job_location'] . ' ' . $post['county']); ?>
                        <div class="googlemap">
                            <h2>Map</h2>
                            <iframe
                                frameborder="0" style="border:0"
                                src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDg3sqfQCdmusqPP_M4j40R_xuIBpiUSG8
                                &q='<?= $location ?>" allowfullscreen>
                            </iframe>
                        </div>
                    <?php endif; ?>
                    
                    <?php if ($post['job_qualifications']): $qualifications = json_decode($post['job_qualifications'], true); ?>
                        <div class="qualifications">
                            <h2>Qualifications</h2>
                            <ul>
                                <?php foreach ($qualifications as $qlf): ?>
                                    <li><?= $qlf ?></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                    
                    <?php if ($post['job_responsibilities']): $responsibilities = json_decode($post['job_responsibilities'], true) ?>
                        <div class="responsibilities">
                            <h2>Responsibilities</h2>
                            <ul>
                                <?php foreach ($responsibilities as $res): ?>
                                    <li><?= $res ?></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    <?php endif; ?>

                    <div class="url-container">
                        <h2>Job Post URL</h2>
                        <a target="_blank" href="<?= $globalworks->_url . '/job/' . $post['id'] ?>"><?= $globalworks->_url . '/job/' . $post['id'] ?></a>
                        <div class="fb-share">
                            <iframe src="https://www.facebook.com/plugins/share_button.php?href=<?= urldecode($globalworks->_url . '/job/' . $post['id']) ?>&layout=button_count&size=large&mobile_iframe=true&appId=595785607158325&width=106&height=28" width="106" height="28" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                        </div>
                    </div>

                    <div class="options">
                        <h2>Job Post Options</h2>
                        <ul>
                            <li><div onclick="javascript: edit_post(<?= $post['id'] ?>)">Edit Job Post</div></li>
                            <li><div onclick="javascript: delete_post(<?= $post['id'] ?>)">Delete Job Post</div></li>
                            <li><div id="close2">Close This Window</div></li>
                        </ul>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        <?php endforeach; ?>
    </div>
    
    <script>
        
        $("#close, #close2").on("click", function() {
            $(".employers-jobpost-view").animate({
                top: "-100%"
            }, 500, function() {
                $(this).html("");
                
                if ($(window).width() > 800) {
                    $(".main-header").show();
                }

                $("body").css({
                    "overflow": "auto"
                });
            });
        });
        
        function edit_post(id) {
            var height = $(window).height();
            var new_height = (height - 3) / 2 + 300;
            
            var data = {
                "operation": "employers_jobpost_edit",
                "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>",
                "id": id
            };
            
            $(".employers-jobpost-view").css({
                "line-height": new_height + "px",
                "text-align": "center"
            }).html("<img src='/img/loading.gif' />");
            
            $.post("/ajax", data, function(r) {
                $(".employers-jobpost-view").css({
                    "line-height": "normal",
                    "text-align": "left"
                }).html(r);
            });
        }
        
        function delete_post(id) {
            var c = confirm("Are your sure you want to delete this Job Post?");

            if (c) {
                var data = {
                    "operation": "employers_jobpost_delete",
                    "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
                    "id": id
                };

                $.post("/ajax", data, function(r) {
                    if (r == "ok") {
                        $(".employers-jobpost-view").animate({
                            top: "-100%"
                        }, 500, function() {
                            $(this).html("");
                            $(".main-header").show();
                            $("body").css({
                                "overflow": "auto"
                            });

                            location.reload();
                            $(window).scrollTop(0);
                        });
                    }
                });
            }
        }
    </script>
<?php endif; ?>