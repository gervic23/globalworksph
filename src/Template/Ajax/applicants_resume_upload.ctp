<div class="applicants-upload-resume">
    <div class="resume-upload"></div>
    <div class="requirements">
        <ul>
            <li>File size maximum of 500 kb.</li>
            <li>Only word documents .doc, .docx are allowed.</li>
        </ul>
    </div>
    <div class="btn-container">
        <span id="close">Close This Window</span>
    </div>
</div>
<script>

    var label;
    var label_font_size;
    
    resizeResumeUpload();
    resumeAttr();
    
    $(window).resize(function() {
        resizeResumeUpload();
        resumeAttr();
    });
    
    $("#close").on("click", function() {
        $(".upload-profile-resume-view").animate({
            top: "-100%"
        }, 500, function() {
            $(this).html("");
            $(".main-header").show();
            $("body").css({
                "overflow-y": "auto"
            });
        });
    });
    
    $(".resume-upload").dragdropupload('/ajax', {
        label: label,
        width: "80%",
        height: "300px",
        border_size: "3px",
        background_color: "#e3f8fd",
        label_font_size: label_font_size,
        error_font_size: label_font_size,
        post_data: {
            operation: "resume_upload",
            _csrfToken: "<?= $this->request->getParam('_csrfToken'); ?>",
        },
        validation: {
            file_types: {
                types: ["application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/msword"],
                validation_message: "is not a word document file."
            },
            file_sizes: {
                max: {
                    size: 1024,
                    validation_message: "File size to large."
                }
            }
        }
    }, function(r) {
        if (r == "ok") {
            loadResume();
        }
    });

    function resumeAttr() {
        if ($(window).width() >= 500) {
            label = "Drag resume file here or click to upload.";
            label_font_size = "130%";
        } else {
            label = "Click Here to Upload";
            label_font_size = "100%";
        }
    }
    
    function resizeResumeUpload() {
        var height = $(window).height();
        var new_height = height - 25;
        var upload_pic = $(".applicants-upload-resume").height();
        
        if (new_height > upload_pic) {
            $(".applicants-upload-resume").css({
                "height": new_height + "px"
            });
        }
    }
</script>