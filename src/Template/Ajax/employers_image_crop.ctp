<div class="employers-image-crop">
    <div class="progress">
        <span class="employers-setup-profile-pic complete2">Upload Your Profile Picture</span>
        <span class="employers-crop-profile-pic">Crop Your Profile Picture</span>
        <div class="clear"></div>
    </div>

    <div class="image">
        <img id="image" alt="crop" src="/img/users/tmp/<?= $filename ?>" />
    </div>
    <div class="btns">
        <span id="save">Save</span>
    </div>
</div>

<script>

    // if (detectOs.isWindows() && $.browser.mozilla) {

    // }

    if (detectOs.isLinux() && $.browser.chrome) {
        $("head").append("<style>.employers-image-crop .progress .complete2::after { border-top: 14px solid transparent; } </style>");
    }

    $(function() {
        $("#image").cropper();

        $("#save").on("click", function() {
            $(this).fadeOut();
            $("#image").cropper("getCroppedCanvas").toBlob(function(blob) {
                var filename = "<?= $filename ?>";
                var new_filename = "<?= $new_filename ?>";
                var formData = new FormData();

                formData.append("cropperImage", blob);
                formData.append("operation", "crop_image");
                formData.append("_csrfToken", "<?= $this->request->getParam('_csrfToken') ?>");
                formData.append("user_id", "<?= $this->request->session()->read('Auth.User')['id'] ?>");
                formData.append("filename", "<?= $filename ?>");

                $.ajax("/ajax", {
                    method: "post",
					data: formData,
					processData: false,
					contentType: false,
                    success: function(r) {
                        if (r == "ok") {
                            loadProfilePic(new_filename, true);
                        }
                    },
					error: function () {
					  console.log('Upload error');
					}
                });
            });
        });
    });

    uploadImageCropSize();

    $(window).resize(function() {
        uploadImageCropSize();
    });

    function uploadImageCropSize() {
        var height = $(window).height();
        var new_height = height - 25;
        var image_crop = $(".employers-image-crop").height();

        if (new_height > image_crop) {
            $(".employers-image-crop").css({
                "height": new_height + "px",
            });
        }
    }
</script>