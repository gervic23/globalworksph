<?php use App\Controller\GlobalworksController; $globalworks = new GlobalworksController(); ?>
<div class="job-post-preview">
    <div>&nbsp;</div>
    <div class="prev-main-header">
        <div class="header-logo">
            <?php if ($header_image): ?>
                <div id="header" class="header">
                    <img src="/img/users/job_header_tmp/<?= $header_image ?>" />
                </div>
            <?php endif; ?>

            <?php if ($company_logo): ?>
                <div class="logo">
                    <img src="/img/users/company_logo_tmp/<?= $company_logo ?>" />
                </div>
            <?php endif; ?>
        </div>
        <h1><?= $job_title ?></h1>
        <h4><?= $company_name ?></h4>
    </div>
    
    <div class="contents">
        <div class="left">
            <div class="description">
                <h2>Job Description</h2>
                <?= nl2br($jobs_description) ?>
            </div>
            
            <?php if ($job_website): ?>
                <div class="website">
                    <h2>Company Website</h2>
                    <?php
                        $str1 = substr($job_website, 0, 7) ;
                        $str2 = substr($job_website, 0, 8) ;
                    
                        if ($str1 == 'http://' || $str2 == 'https://') {
                            echo '<a target="_blank" href="' . $job_website . '">' . $job_website . '</a>';
                        } else {
                            echo '<a target="_blank" href="http://' . $job_website . '">http://' . $job_website . '</a>';
                        }
                    ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="right">
            <div class="job-details">
                <h2>Job Details</h2>
                <table>
                    <tr>
                        <td>Job Category:</td>
                        <td><?= $job_category ?></td>
                    </tr>
                    <tr>
                        <td>Job Type:</td>
                        <td><?= $job_type?></td>
                    </tr>
                    <?php if ($this->request->data('job-base')): ?>
                        <tr>
                            <td>Job Base:</td>
                            <td><?= $this->request->data('job-base') ?></td>
                        </tr>
                    <?php endif; ?>
                    <tr>
                        <td>Working Experience:</td>
                        <td><?= $years_exp_start ?> <?= ($years_exp_end !== '') ? 'to ' . $years_exp_end : ''; ?></td>
                    </tr>
                    <tr>
                        <td>Closing Date:</td>
                        <td><?= $closing_date ?></td>
                    </tr>
                </table>
            </div>
            <?php if ($job_email || $job_contact_number): ?>
                <div class="contact-info">
                    <h2>Contact Information</h2>
                    <table>
                        <?php if ($job_email): ?>
                            <tr>
                                <td>Email:</td>
                                <td><?= $job_email ?></td>
                            </tr>
                        <?php endif; ?>

                        <?php if ($job_contact_number): ?>
                            <tr>
                                <td>Contact Number:</td>
                                <td><?= $job_contact_number ?></td>
                            </tr>
                        <?php endif; ?>
                    </table>
                </div>
            <?php endif; ?>
            
            <?php if ($job_location): ?>
                <div class="location">
                    <h2>Location</h2>
                    <?= $job_location ?>
                    <div class="country"><?= $globalworks->country_to_str($country) ?>.</div>
                </div>
            <?php endif; ?>

            <?php if ($googglemap): $location = urlencode($job_location. ' ' . $country); ?>
                <div class="googlemap">
                    <h2>Map</h2>
                    <iframe
                        frameborder="0" style="border:0"
                        src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDg3sqfQCdmusqPP_M4j40R_xuIBpiUSG8
                        &q='<?= $location ?>" allowfullscreen>
                    </iframe>
                </div>
            <?php endif; ?>

            <?php if ($job_qualifications): ?>
                <div class="qualifications">
                    <h2>Qualifications</h2>
                    <ul>
                        <?php foreach ($job_qualifications as $qlf): ?>
                            <?php if ($qlf !== ''): ?>
                                <li><?= $qlf ?></li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>
            
            <?php if ($job_responisilities): ?>
                <div class="responsibilities">
                    <h2>Responsibilities</h2>
                    <ul>
                        <?php foreach ($job_responisilities as $res): ?>
                            <?php if ($res !== ''): ?>
                                <li><?= $res ?></li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>
        </div>
        <div class="clear"></div>
    </div>

    <div class="btn-container">
        <?= $this->Form->button('Go Back', ['type' => 'button', 'id' => 'go-back']) ?>
    </div>
    <?= $this->Form->end() ?>
</div>

<script>
    resize_job_preview();
    
    $(window).resize(function() {
        resize_job_preview();   
    });
    
    if (!$("#header").parent().length) {
        $(".job-post-preview .prev-main-header .header-logo .logo").css({
            "position": "inherit",
            "margin-top": "0px"
        });
    }
    
    $("#go-back").on("click", function() {
        $(".jobpost-prev").animate({
            top: "-101%"
        }).html("");
        $("body").css({
            "overflow": "auto"
        });

        if ($(window).width() > 800) {
            $(".main-header").show();
        }
    });
    
    function resize_job_preview() {
        var height = $(window).height();
        var new_height = height - 33;
        var job_post_preview = $(".job-post-preview").height();
        
        if (job_post_preview <= height) {
            $(".job-post-preview").css({
                "height": new_height + "px"
            });   
        }
    }
</script>