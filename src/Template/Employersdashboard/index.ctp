<?php use App\Controller\GlobalworksController; $globalworks = new GlobalworksController(); ?>
<?= $this->CKEditor->loadJs() ?>

<div class="employers-candidate-view"></div>
<div class="employers-dashboard">
    <div class="left">
        <?= $this->element('Nav/dashboard-employer') ?>
    </div>
    <div class="right">
        <div class="contents">
            <?php if ($globalworks->suggested_candidates()): ?>
                <div class="suggested_candidates">
                    <h1>Suggested Candidates</h1>
                    <ul>
                        <?php foreach ($globalworks->suggested_candidates() as $profile): ?>
                            <li>
                                <div class="content">
                                    <div class="profile_pic">
                                        <?php if ($profile['profiles']['profile_picture']): ?>
                                            <a href="javascript:" onclick="javascript: view_candidate(<?= $profile['profiles']['user_id'] ?>)"><img src="/img/users/profile_thumbnails/<?= $profile['profiles']['profile_picture'] ?>" /></a>
                                        <?php else: ?>
                                            <img src="/img/users/nopropic.png" />
                                        <?php endif; ?>
                                    </div>
                                    <div class="fullname">
                                        <a href="javascript:" onclick="javascript: view_candidate(<?= $profile['profiles']['user_id'] ?>)"><?= $profile['users']['firstname'] . ' ' . $profile['users']['lastname'] ?></a>
                                    </div>
                                    <?php if ($profile['profiles']['job_title']): ?>
                                        <div class="job_title">
                                            <?= $profile['profiles']['job_title'] ?>
                                        </div>
                                    <?php endif; ?>
                                    <?php if ($profile['profiles']['nickname']): ?>
                                        <div class="nickname">(<?= $profile['profiles']['nickname'] ?>)</div>
                                    <?php endif; ?>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php else: ?>
                <div class="latest_candidates">
                    <h1>Latest Candidates</h1>
                    <ul>
                        <?php foreach ($globalworks->lastest_candidates() as $profile): ?>
                            <li>
                                <div class="content">
                                    <div class="profile_pic">
                                        <?php if ($profile['profiles']['profile_picture']): ?>
                                            <a href="javascript:" onclick="javascript: view_candidate(<?= $profile['profiles']['user_id'] ?>)"><img src="/img/users/profile_thumbnails/<?= $profile['profiles']['profile_picture'] ?>" /></a>
                                        <?php else: ?>
                                            <img src="/img/users/nopropic.png" />
                                        <?php endif; ?>
                                    </div>
                                    <div class="fullname">
                                        <a href="javascript:" onclick="javascript: view_candidate(<?= $profile['profiles']['user_id'] ?>)"><?= $profile['users']['firstname'] . ' ' . $profile['users']['lastname'] ?></a>
                                    </div>
                                    <?php if ($profile['profiles']['job_title']): ?>
                                        <div class="job_title">
                                            <?= $profile['profiles']['job_title'] ?>
                                        </div>
                                    <?php endif; ?>
                                    <?php if ($profile['profiles']['nickname']): ?>
                                        <div class="nickname">(<?= $profile['profiles']['nickname'] ?>)</div>
                                    <?php endif; ?>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="clear"></div>
</div>
<script>
    var new_height = 0;

    $(".suggested_candidates ul li, .latest_candidates ul li").scrollAnimate({animation: "fadeInDown"});

    canidate_window();

    $(window).resize(function() {
        canidate_window();
    });

    function canidate_window() {
        var height = $(window).height();
        var width = $(window).width();
        new_height = height - 2;
        var new_width = width + 15;

        $(".employers-candidate-view").css({
            "height": new_height + "px",
            "width": new_width + "px"
        });
    }

    function view_candidate(id) {
        var data = {
            "operation": "candidate_show_info",
            "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>",
            "id": id
        };

        $(".main-header").hide();

        $("body").css({
            "overflow": "hidden"
        });

        var new_height2 = new_height / 2 + 300;

        $(".employers-candidate-view").animate({
            top: 0
        }, 500, function() {
            $(this).html('<img src="/img/loading.gif" />').css({
                "text-align": "center",
                "line-height": new_height2 + "px"
            });

            $.post("/ajax", data, function(r) {
                $(".employers-candidate-view").css({
                    "text-align": "left",
                    "line-height": "normal"
                }).html(r);
            });
            
        });
    }
</script>
<?= $this->Html->script('employersdashboard') ?>