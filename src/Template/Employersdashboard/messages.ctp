<?php
    use App\Controller\GlobalworksController; $globalworks = new GlobalworksController();
    $this->Html->script('jquery.slimscroll.min', ['block' => true]);
?>

<div class="employers-dashboard">
    <div class="black-screen"></div>
    <div class="message-view"></div>
    <div class="candidate-view"></div>
    <div class="left">
        <?= $this->element('Nav/dashboard-employer') ?>
    </div>
    <div class="right">
        <div class="contents">
            <div class="message-nav">
                <ul>
                    <li class="msg-btn" id="unread"><span class="btn">Unread</span><?= ($globalworks->display_unread_msgs_count()) ? '<span class="unread-notification">'. $globalworks->display_unread_msgs_count() .'</span>' : ''; ?></li>
                    <li class="msg-btn" id="ja"><span class="btn">Job Application</span></li>
                    <li class="msg-btn" id="from-admin"><span class="btn">From Admin</span></li>
                    <li class="msg-btn" id="si"><span class="btn">Sent Items</span></li>
                </ul>
            </div>
        </div>
        <div class="m-message-nav">
            <select id="page">
                <option value="unread">Unread</option>
                <option value="ja">Job Application</option>
                <option value="from-admin">From Admin</option>
                <option value="si">Sent Items</option>
            </select>
        </div>
        <div class="messages-contents"></div>
    </div>
    <div class="clear"></div>
</div>
<?= $this->Html->script('employersdashboard') ?>
<script>
    var height = 0;

    var new_height = 0;

    var page = 'employer_msg_unread';

    messageViewSize();
    candidateView();
    unread_notification();
    readMessageAttr()

    $(window).resize(function() {
        messageViewSize();
        candidateView();
        readMessageAttr();
    });

    $("#page").on("change", function() {
        removeBorder();

        switch ($(this).val()) {
            case "unread":
                page = 'employer_msg_unread';
            break;

            case "ja":
                page = 'employer_msg_job_application';
            break;

            case "from-admin":
                page = "employer_msg_from_admin";
            break;

            case "si":
                page = "employer_msg_sent_items";
            break;
        }

        $(".messages-contents").css({
            "text-align": "center"
        }).html('<img src="/img/loading.gif" />');

        var data2 = {
            "operation": page,
            "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>"
        };

        $.post("/ajax", data2, function(r) {
            $(".messages-contents").css({
                "text-align": "left"
            }).html(r);
        });
    });

    var data = {
        "operation": page,
        "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>"
    };
    
    $(".messages-contents").css({
        "text-align": "center"
    }).html('<img src="/img/loading.gif" />');

    $.post("/ajax", data, function(r) {
        $(".messages-contents").css({
            "text-align": "left"
        }).html(r);
    });

    $("#unread").addClass("message-active");

    $("#unread").on("click", function() {
        removeBorder();

        page = 'employer_msg_unread';

        $(this).addClass("message-active");

        $(".messages-contents").css({
            "text-align": "center"
        }).html('<img src="/img/loading.gif" />');

        var data2 = {
            "operation": page,
            "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>"
        };

        $.post("/ajax", data2, function(r) {
            $(".messages-contents").css({
                "text-align": "left"
            }).html(r);
        });
    });

    $("#ja").on("click", function() {
        removeBorder();

        page = 'employer_msg_job_application';

        $(this).addClass("message-active");

        $(".messages-contents").css({
            "text-align": "center"
        }).html('<img src="/img/loading.gif" />');

        var data2 = {
            "operation": page,
            "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>"
        };

        $.post("/ajax", data2, function(r) {
            $(".messages-contents").css({
                "text-align": "left"
            }).html(r);
        });
    });

    $("#from-admin").on("click", function() {
        removeBorder();

        page = 'employer_msg_from_admin';

        $(this).addClass("message-active");

        $(".messages-contents").css({
            "text-align": "center"
        }).html('<img src="/img/loading.gif" />');

        var data2 = {
            "operation": page,
            "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>"
        };

        $.post("/ajax", data2, function(r) {
            $(".messages-contents").css({
                "text-align": "left"
            }).html(r);
        });
    });

    $("#si").on("click", function() {
        removeBorder();

        page = 'employer_msg_sent_items';

        $(this).addClass("message-active");

        $(".messages-contents").css({
            "text-align": "center"
        }).html('<img src="/img/loading.gif" />');

        var data2 = {
            "operation": page,
            "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>"
        };

        $.post("/ajax", data2, function(r) {
            $(".messages-contents").css({
                "text-align": "left"
            }).html(r);
        });
    });

    function messageViewSize() {
        var height = $(window).height();
        new_height = height - 200;

        $(".message-view").css({
            "height": new_height + "px"
        });
    }

    function candidateView() {
        height = $(window).height();
        var width = $(window).width() + 13;

        $(".candidate-view").css({
            "width": width + "px",
            "height": height + "px"
        });
    }

    function removeBorder() {
        $(".msg-btn").each(function(i) {
            $(this).removeClass("message-active");
        });
    }

    function readMessage(msg_id) {
        var data2 = {
            "operation": "employer_read_message",
            "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>",
            "id": msg_id
        };

        $(".main-header").css({
            "z-index": 1
        });

        $("body").css({
            "overflow-y": "hidden"
        });

        $(".black-screen").show();

        $(".message-view").slideDown(500, function() {
            $(this).css({
               "text-align": "center",
               "line-height": new_height + "px" 
            }).html('<img src="/img/loading.gif" />');

            $.post("/ajax", data2, function(r) {
                $(".message-view").css({
                    "text-align": "left",
                    "line-height": "normal"
                }).html(r);

                var data3 = {
                    "operation": page,
                    "_csrfToken": "<?= $this->request->param('_csrfToken') ?>"
                };

                $.post("/ajax", data3, function(r) {
                    $(".messages-contents").html(r);
                    unread_notification();
                });
            });
        });
    }

    function readMessageAttr() {
        var height = $(window).height();
        var new_height = height - 130;

        $(".message-view").css({
            "height": new_height + "px"
        });
    }

    function viewCandidate(id) {
        var data2 = {
            "operation": "employer_candidate_view",
            "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
            "id": id
        };

        $(".candidate-view").animate({
            top: 0
        }, 500, function() {
            $(this).css({
                "text-align": "center",
                "line-height": height + "px"
            }).html('<img src="/img/loading.gif" />');

            $.post("/ajax", data2, function(r2) {
                $(".candidate-view").css({
                    "text-align": "left",
                    "line-height": "normal"
                }).html(r2);
            });
        });
    }

    function unread_notification() {
        var data = {
            "operation": "manage_unread_counts",
            "_csrfToken": "<?= $this->request->param('_csrfToken') ?>"
        };

        if ($(".unread-notification").length > 0 && $(".nav-unread-notification").length > 0 && $(".dashboard-unread-notification").length > 0 && $(".top-unread-notification").length > 0) {
            var noti = $(".unread-notification").html();
        
            if (noti.length == 2) {
                $(".unread-notification, .nav-unread-notification, .dashboard-unread-notification, .top-unread-notification").css({
                    "padding": "2px 4px"
                });
            }
        }

        $.post("/ajax", data, function(r) {
            if (r == "") {
                $(".unread-notification, .nav-unread-notification, .dashboard-unread-notification, .top-unread-notification").hide();
            } else {
                $(".unread-notification, .nav-unread-notification, .dashboard-unread-notification, .top-unread-notification").html(r);
            }
        });
    }
</script>