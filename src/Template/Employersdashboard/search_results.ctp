<?php 
    use App\Controller\GlobalworksController; $globalworks = new GlobalworksController();
    echo $this->CKEditor->loadJs();
?>

<div class="employers-candidate-view"></div>
<div class="employers-dashboard">
    <div class="left">
        <?= $this->element('Nav/dashboard-employer') ?>
    </div>
    <div class="right">
        <div class="contents">
            <?php if ($result_count == 0): ?>
                <div class="employers-search-empty">
                    There are no results found.
                </div>
            <?php else: ?>
                <div class="employers-search-results">
                    <div class="search-count">
                        You have <?= $result_count ?> results found.
                    </div>
                    <?php foreach ($users as $user): ?>
                        <div class="search-entry">
                            <div class="img">
                                <?php if ($user['profiles']['profile_picture']): ?>
                                    <a href="javascript:" onclick="javascript: view_candidate(<?= $user['profiles']['user_id'] ?>)"><img src="/img/users/profile_thumbnails/<?= $user['profiles']['profile_picture'] ?>" /></a>
                                <?php else: ?>
                                    <a href="javascript:" onclick="javascript: view_candidate(<?= $user['profiles']['user_id'] ?>)"><img src="/img/users/nopropic.png" /></a>
                                <?php endif; ?>
                            </div>
                            <ul>
                                <li><a href="javascript:" onclick="javascript: view_candidate(<?= $user['profiles']['user_id'] ?>)"><?= $user['users']['firstname'] . ' ' . $user['users']['lastname'] ?></a></li>
                                <?php if ($user['profiles']['job_title']): ?>
                                    <li><?= $user['profiles']['job_title'] ?></li>
                                <?php endif; ?>
                                <?php if ($user['profiles']['nickname']): ?>
                                    <li>(<?= $user['profiles']['nickname'] ?>)</li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    <?php endforeach; ?>
                    <?= $globalworks->_paginate($result_count, $limit, $page) ?>
                    <?= $globalworks->_m_paginate($result_count, $limit, $page) ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="clear"></div>
</div>
<?= $this->Html->script('employersdashboard') ?>
<script>
    var new_height = 0;

    $(".search-count, .search-entry").scrollAnimate({animation: "fadeInDown"});

    canidate_window();

    $(window).resize(function() {
        canidate_window();
    });

    function canidate_window() {
        var height = $(window).height();
        var width = $(window).width();
        new_height = height - 2;
        var new_width = width + 15;

        $(".employers-candidate-view").css({
            "height": new_height + "px",
            "width": new_width + "px"
        });
    }

    function view_candidate(id) {
        var data = {
            "operation": "candidate_show_info",
            "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>",
            "id": id
        };

        $(".main-header").hide();

        $("body").css({
            "overflow": "hidden"
        });

        var new_height2 = new_height / 2 + 300;

        $(".employers-candidate-view").animate({
            top: 0
        }, 500, function() {
            $(this).html('<img src="/img/loading.gif" />').css({
                "text-align": "center",
                "line-height": new_height2 + "px"
            });

            $.post("/ajax", data, function(r) {
                $(".employers-candidate-view").css({
                    "text-align": "left",
                    "line-height": "normal"
                }).html(r);
            });
            
        });
    }
</script>