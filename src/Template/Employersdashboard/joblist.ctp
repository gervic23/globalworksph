<?php 
    use App\Controller\GlobalworksController; $globalworks = new GlobalworksController();
    
    $this->Html->css('jquery-ui.min', ['block' => true]);
    $this->Html->script('jquery-ui.min', ['block' => true]);

    echo $this->CKEditor->loadJs();
?>

<div class="employers-jobpost-view"></div>
<div class="employers-dashboard">
    <div class="left">
        <?= $this->element('Nav/dashboard-employer') ?>
    </div>
    <div class="right">
        <div class="contents">
            <?php if ($jobposts_count !== 0): ?>
                <div class="employers-jobposts">
                    <?= $this->Flash->render() ?>
                    <?= $this->element('Nav/employers-joblist-nav') ?>
                    <?php foreach ($jobposts as $post): ?>
                        <div class="the-post">
                            <?php if ($post['company_logo']): ?>
                                <div class="the-logo">
                                    <a href="javascript:" onclick="javascript: job_view(<?= $post['id'] ?>)"><img src="/img/users/company_logo/<?= $post['company_logo'] ?>" /></a>
                                </div>
                            <?php endif; ?>
                            <div class="post-contents">
                                <div class="post-header">
                                    <h1><a href="javascript:" onclick="javascript: job_view(<?= $post['id'] ?>)"><?= $post['job_title'] ?></a></h1>
                                    <h3><?= $post['company_name'] ?></h3>
                                </div>
                                <div class="post-description">
                                    <?= (strlen($post['job_description']) >= 400) ? substr($post['job_description'], 0, 400) . '...' : $post['job_description']; ?>
                                </div>
                                <div class="post-date">
                                    <?= $globalworks->when($post['created_at']) ?>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    <?php endforeach; ?>
                    <?= $globalworks->_paginate($jobposts_count, $limit, $page) ?>
                    <?= $globalworks->_m_paginate($jobposts_count, $limit, $page) ?>
                </div>
            <?php else: ?>
                <div class="employers-jobposts">
                    <?= $this->Flash->render() ?>
                    <?= $this->element('Nav/employers-joblist-nav') ?>
                    <div class="employers-postjobs-empty">
                        You dont have a job posts at this time. <a href="/employers/jobpost">Post Job</a> now.
                    </div>
                </div>
            <?php endif ?>
        </div>
    </div>
    <div class="clear"></div>
</div>
<?= $this->Html->script('employersdashboard') ?>
<script>
    var new_height = 0;
    
    $(".the-post").scrollAnimate({
        animation: "fadeInDown"
    });
    
    jopostview_size();
    
    $(window).resize(function() {
        jopostview_size();
    });
    
    function jopostview_size() {
        var height = $(window).height();
        var width = $(window).width() + 15;
        new_height = height - 3;
        
        $(".employers-jobpost-view").css({
            "height": new_height + "px",
            "width": width + "px"
        });
    }
    
    function job_view(id) {
        $(".main-header").hide();
        
        $(".employers-jobpost-view").animate({
            top: "0"
        }, 500, function() {
            var data = {
                "operation": "employers_jobpost_view",
                "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>",
                "id": id
            };
            
            var new_height2 = new_height / 2 + 300;
            
            $("body").css({
                "overflow": "hidden"
            });
            
            $(this).css({
                "line-height": new_height2 + "px",
                "text-align": "center"
            }).html("<img src='/img/loading.gif' />");
            
            $.post("/ajax", data, function(r) {
                $(".employers-jobpost-view").css({
                    "line-height": "normal",
                    "text-align": "left"
                }).html(r);
            });
        });
    }
</script>