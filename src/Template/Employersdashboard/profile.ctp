<?php
    use App\Controller\GlobalworksController;

    $globalworks = new GlobalworksController();

    $this->Html->script('jquery.dragdropupload.1.0', ['block' => true]);
    $this->Html->css('cropper', ['block' => true]);
    $this->Html->script('cropper', ['block' => true]);

    $countries = [
    '' => 'Select Country',
    'AF'=>'AFGHANISTAN',
    'AL'=>'ALBANIA',
    'DZ'=>'ALGERIA',
    'AS'=>'AMERICAN SAMOA',
    'AD'=>'ANDORRA',
    'AO'=>'ANGOLA',
    'AI'=>'ANGUILLA',
    'AQ'=>'ANTARCTICA',
    'AG'=>'ANTIGUA AND BARBUDA',
    'AR'=>'ARGENTINA',
    'AM'=>'ARMENIA',
    'AW'=>'ARUBA',
    'AU'=>'AUSTRALIA',
    'AT'=>'AUSTRIA',
    'AZ'=>'AZERBAIJAN',
    'BS'=>'BAHAMAS',
    'BH'=>'BAHRAIN',
    'BD'=>'BANGLADESH',
    'BB'=>'BARBADOS',
    'BY'=>'BELARUS',
    'BE'=>'BELGIUM',
    'BZ'=>'BELIZE',
    'BJ'=>'BENIN',
    'BM'=>'BERMUDA',
    'BT'=>'BHUTAN',
    'BO'=>'BOLIVIA',
    'BA'=>'BOSNIA AND HERZEGOVINA',
    'BW'=>'BOTSWANA',
    'BV'=>'BOUVET ISLAND',
    'BR'=>'BRAZIL',
    'IO'=>'BRITISH INDIAN OCEAN TERRITORY',
    'BN'=>'BRUNEI DARUSSALAM',
    'BG'=>'BULGARIA',
    'BF'=>'BURKINA FASO',
    'BI'=>'BURUNDI',
    'KH'=>'CAMBODIA',
    'CM'=>'CAMEROON',
    'CA'=>'CANADA',
    'CV'=>'CAPE VERDE',
    'KY'=>'CAYMAN ISLANDS',
    'CF'=>'CENTRAL AFRICAN REPUBLIC',
    'TD'=>'CHAD',
    'CL'=>'CHILE',
    'CN'=>'CHINA',
    'CX'=>'CHRISTMAS ISLAND',
    'CC'=>'COCOS (KEELING) ISLANDS',
    'CO'=>'COLOMBIA',
    'KM'=>'COMOROS',
    'CG'=>'CONGO',
    'CD'=>'CONGO, THE DEMOCRATIC REPUBLIC OF THE',
    'CK'=>'COOK ISLANDS',
    'CR'=>'COSTA RICA',
    'CI'=>'COTE D IVOIRE',
    'HR'=>'CROATIA',
    'CU'=>'CUBA',
    'CY'=>'CYPRUS',
    'CZ'=>'CZECH REPUBLIC',
    'DK'=>'DENMARK',
    'DJ'=>'DJIBOUTI',
    'DM'=>'DOMINICA',
    'DO'=>'DOMINICAN REPUBLIC',
    'TP'=>'EAST TIMOR',
    'EC'=>'ECUADOR',
    'EG'=>'EGYPT',
    'SV'=>'EL SALVADOR',
    'GQ'=>'EQUATORIAL GUINEA',
    'ER'=>'ERITREA',
    'EE'=>'ESTONIA',
    'ET'=>'ETHIOPIA',
    'FK'=>'FALKLAND ISLANDS (MALVINAS)',
    'FO'=>'FAROE ISLANDS',
    'FJ'=>'FIJI',
    'FI'=>'FINLAND',
    'FR'=>'FRANCE',
    'GF'=>'FRENCH GUIANA',
    'PF'=>'FRENCH POLYNESIA',
    'TF'=>'FRENCH SOUTHERN TERRITORIES',
    'GA'=>'GABON',
    'GM'=>'GAMBIA',
    'GE'=>'GEORGIA',
    'DE'=>'GERMANY',
    'GH'=>'GHANA',
    'GI'=>'GIBRALTAR',
    'GR'=>'GREECE',
    'GL'=>'GREENLAND',
    'GD'=>'GRENADA',
    'GP'=>'GUADELOUPE',
    'GU'=>'GUAM',
    'GT'=>'GUATEMALA',
    'GN'=>'GUINEA',
    'GW'=>'GUINEA-BISSAU',
    'GY'=>'GUYANA',
    'HT'=>'HAITI',
    'HM'=>'HEARD ISLAND AND MCDONALD ISLANDS',
    'VA'=>'HOLY SEE (VATICAN CITY STATE)',
    'HN'=>'HONDURAS',
    'HK'=>'HONG KONG',
    'HU'=>'HUNGARY',
    'IS'=>'ICELAND',
    'IN'=>'INDIA',
    'ID'=>'INDONESIA',
    'IR'=>'IRAN, ISLAMIC REPUBLIC OF',
    'IQ'=>'IRAQ',
    'IE'=>'IRELAND',
    'IL'=>'ISRAEL',
    'IT'=>'ITALY',
    'JM'=>'JAMAICA',
    'JP'=>'JAPAN',
    'JO'=>'JORDAN',
    'KZ'=>'KAZAKSTAN',
    'KE'=>'KENYA',
    'KI'=>'KIRIBATI',
    'KP'=>'KOREA DEMOCRATIC PEOPLES REPUBLIC OF',
    'KR'=>'KOREA REPUBLIC OF',
    'KW'=>'KUWAIT',
    'KG'=>'KYRGYZSTAN',
    'LA'=>'LAO PEOPLES DEMOCRATIC REPUBLIC',
    'LV'=>'LATVIA',
    'LB'=>'LEBANON',
    'LS'=>'LESOTHO',
    'LR'=>'LIBERIA',
    'LY'=>'LIBYAN ARAB JAMAHIRIYA',
    'LI'=>'LIECHTENSTEIN',
    'LT'=>'LITHUANIA',
    'LU'=>'LUXEMBOURG',
    'MO'=>'MACAU',
    'MK'=>'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF',
    'MG'=>'MADAGASCAR',
    'MW'=>'MALAWI',
    'MY'=>'MALAYSIA',
    'MV'=>'MALDIVES',
    'ML'=>'MALI',
    'MT'=>'MALTA',
    'MH'=>'MARSHALL ISLANDS',
    'MQ'=>'MARTINIQUE',
    'MR'=>'MAURITANIA',
    'MU'=>'MAURITIUS',
    'YT'=>'MAYOTTE',
    'MX'=>'MEXICO',
    'FM'=>'MICRONESIA, FEDERATED STATES OF',
    'MD'=>'MOLDOVA, REPUBLIC OF',
    'MC'=>'MONACO',
    'MN'=>'MONGOLIA',
    'MS'=>'MONTSERRAT',
    'MA'=>'MOROCCO',
    'MZ'=>'MOZAMBIQUE',
    'MM'=>'MYANMAR',
    'NA'=>'NAMIBIA',
    'NR'=>'NAURU',
    'NP'=>'NEPAL',
    'NL'=>'NETHERLANDS',
    'AN'=>'NETHERLANDS ANTILLES',
    'NC'=>'NEW CALEDONIA',
    'NZ'=>'NEW ZEALAND',
    'NI'=>'NICARAGUA',
    'NE'=>'NIGER',
    'NG'=>'NIGERIA',
    'NU'=>'NIUE',
    'NF'=>'NORFOLK ISLAND',
    'MP'=>'NORTHERN MARIANA ISLANDS',
    'NO'=>'NORWAY',
    'OM'=>'OMAN',
    'PK'=>'PAKISTAN',
    'PW'=>'PALAU',
    'PS'=>'PALESTINIAN TERRITORY, OCCUPIED',
    'PA'=>'PANAMA',
    'PG'=>'PAPUA NEW GUINEA',
    'PY'=>'PARAGUAY',
    'PE'=>'PERU',
    'PH'=>'PHILIPPINES',
    'PN'=>'PITCAIRN',
    'PL'=>'POLAND',
    'PT'=>'PORTUGAL',
    'PR'=>'PUERTO RICO',
    'QA'=>'QATAR',
    'RE'=>'REUNION',
    'RO'=>'ROMANIA',
    'RU'=>'RUSSIAN FEDERATION',
    'RW'=>'RWANDA',
    'SH'=>'SAINT HELENA',
    'KN'=>'SAINT KITTS AND NEVIS',
    'LC'=>'SAINT LUCIA',
    'PM'=>'SAINT PIERRE AND MIQUELON',
    'VC'=>'SAINT VINCENT AND THE GRENADINES',
    'WS'=>'SAMOA',
    'SM'=>'SAN MARINO',
    'ST'=>'SAO TOME AND PRINCIPE',
    'SA'=>'SAUDI ARABIA',
    'SN'=>'SENEGAL',
    'SC'=>'SEYCHELLES',
    'SL'=>'SIERRA LEONE',
    'SG'=>'SINGAPORE',
    'SK'=>'SLOVAKIA',
    'SI'=>'SLOVENIA',
    'SB'=>'SOLOMON ISLANDS',
    'SO'=>'SOMALIA',
    'ZA'=>'SOUTH AFRICA',
    'GS'=>'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS',
    'ES'=>'SPAIN',
    'LK'=>'SRI LANKA',
    'SD'=>'SUDAN',
    'SR'=>'SURINAME',
    'SJ'=>'SVALBARD AND JAN MAYEN',
    'SZ'=>'SWAZILAND',
    'SE'=>'SWEDEN',
    'CH'=>'SWITZERLAND',
    'SY'=>'SYRIAN ARAB REPUBLIC',
    'TW'=>'TAIWAN, PROVINCE OF CHINA',
    'TJ'=>'TAJIKISTAN',
    'TZ'=>'TANZANIA, UNITED REPUBLIC OF',
    'TH'=>'THAILAND',
    'TG'=>'TOGO',
    'TK'=>'TOKELAU',
    'TO'=>'TONGA',
    'TT'=>'TRINIDAD AND TOBAGO',
    'TN'=>'TUNISIA',
    'TR'=>'TURKEY',
    'TM'=>'TURKMENISTAN',
    'TC'=>'TURKS AND CAICOS ISLANDS',
    'TV'=>'TUVALU',
    'UG'=>'UGANDA',
    'UA'=>'UKRAINE',
    'AE'=>'UNITED ARAB EMIRATES',
    'GB'=>'UNITED KINGDOM',
    'US'=>'UNITED STATES',
    'UM'=>'UNITED STATES MINOR OUTLYING ISLANDS',
    'UY'=>'URUGUAY',
    'UZ'=>'UZBEKISTAN',
    'VU'=>'VANUATU',
    'VE'=>'VENEZUELA',
    'VN'=>'VIET NAM',
    'VG'=>'VIRGIN ISLANDS, BRITISH',
    'VI'=>'VIRGIN ISLANDS, U.S.',
    'WF'=>'WALLIS AND FUTUNA',
    'EH'=>'WESTERN SAHARA',
    'YE'=>'YEMEN',
    'YU'=>'YUGOSLAVIA',
    'ZM'=>'ZAMBIA',
    'ZW'=>'ZIMBABWE',
    ];

    $profile_picture = '';
    $nickname = '';
    $business_name = '';
    $business_description = '';
    $business_website = '';
    $business_email = '';
    $business_contact_number = '';
    $business_address = '';
    $business_region = '';
    $business_country = '';

    if ($profile_count > 0) {
        foreach ($profile as $pro) {
            $profile_picture = $pro['profile_picture'];
            $nickname = $pro['nickname'];
            $business_name = $pro['business_name'];
            $business_description = $pro['business_description'];
            $business_email = $pro['business_email'];
            $business_website = $pro['business_website'];
            $business_contact_number = $pro['business_contact_number'];
            $business_address = $pro['business_address'];
            $business_region = $pro['business_state'];
            $business_country = $pro['business_country'];
        }
    }
?>

<div class="employers-dashboard">
    <div class="upload-profile-pic-view"></div>
    <div class="left">
        <?= $this->element('Nav/dashboard-employer') ?>
    </div>
    <div class="right">
        <div class="user-profile">
            <h1>Your Profile</h1>
            <?= $this->Flash->render() ?>
            <div class="form">
                <?= $this->Form->create($profiles) ?>
                <div class="header">
                    <div class="loading">
                        <img src="/img/loading.gif" />
                    </div>
                    <div class="img1"></div>
                    <div class="img-btn-1">
                        <span id="upload">Replace</span> <span id="delete">Delete</span>
                    </div>
                    <div class="img-btn-2">
                        <span id="upload2">Upload Profile Image</span>
                    </div>
                </div>
                <div class="details">
                    <?= $this->Form->control('nickname', ['label' => 'Nickname:', 'value' => $nickname, 'autocomplete' => 'off']); ?>
                    <?= $this->Form->control('business_name', ['label' => 'Business/Company Name:', 'value' => $business_name, 'autocomplete' => 'off']) ?>
                    <?= $this->Form->control('business_description', ['label' => 'Business/Company Description:', 'type' => 'textarea', 'value' => $business_description]) ?>
                    <?= $this->Form->control('business_email', ['label' => 'Business/Company Email:', 'value' => $business_email, 'autocomplete' => 'off']) ?>
                    <?= $this->Form->control('business_website', ['label' => 'Business Website:', 'value' => $business_website]) ?>
                    <?= $this->Form->control('business_contact_number', ['label' => 'Business/Company Contact Number:', 'value' => $business_contact_number, 'autocomplete' => 'off']) ?>
                    <?= $this->Form->control('business_address', ['label' => 'Business/Company Address:', 'type' => 'textarea', 'value' => $business_address]) ?>
                    <?= $this->Form->control('business_region', ['label' => 'Business/Company Region:', 'value' => $business_region, 'autocomplete' => 'off']) ?>
                    <?= $this->Form->control('business_country', ['label' => 'Business Country:', 'type' => 'select', 'options' => $countries, 'default' => $business_country]) ?>
                    <div class="submit-btn"><?= $this->Form->submit('Save'); ?></div>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
<?= $this->Html->script('employersdashboard') ?>
<script>
    var new_height = 0;

    uploadPictureSize();

    $("#nickname").on("click focus", function() {
        inputAnimate("nickname");
    }).on("focusout", function() {
        focusoutAnimate("nickname");
    });

    $("#business-name").on("click focus", function() {
        inputAnimate("business-name");
    }).on("focusout", function() {
        focusoutAnimate("business-name");
    });

    $("#business-description").on("click focus", function() {
        $(this).animate({
            height: "100px"
        }, 500, function() {
            $(this).css({
                "border": "2px solid #ccc"
            });
        });
    }).focusout(function() {
        $(this).animate({
            height: "20px"
        }, 500, function() {
            $(this).css({
                "border": 0,
                "border-bottom": "2px solid #ccc"
            });
        });

        footerSize();
    });

    $("#business-email").on("click focus", function() {
        inputAnimate("business-email");
    }).on("focusout", function() {
        focusoutAnimate("business-email");
    });

    $("#business-website").on("click focus", function() {
        inputAnimate("business-website");
    }).on("focusout", function() {
        focusoutAnimate("business-website");
    });

    $("#business-contact-number").on("click focus", function() {
        inputAnimate("business-contact-number");
    }).on("focusout", function() {
        focusoutAnimate("business-contact-number");
    });

    $("#business-address").on("click focus", function() {
        $(this).animate({
            height: "100px"
        }, 500, function() {
            $(this).css({
                "border": "2px solid #ccc"
            });
        });

        footerSize();
    }).focusout(function() {
        $(this).animate({
            height: "20px"
        }, 500, function() {
            $(this).css({
                "border": 0,
                "border-bottom": "2px solid #ccc"
            });
        });
    });

    $("#business-region").on("click focus", function() {
        inputAnimate("business-region");
    }).on("focusout", function() {
        focusoutAnimate("business-region");
    });

    <?php if ($profile_picture): ?>
        loadProfilePic("<?= $profile_picture ?>");
    <?php else: ?>
        loadProfilePic();
    <?php endif; ?>

    $(window).resize(function() {
        uploadPictureSize();
    });

    $("#upload, #upload2").on("click", function() {
        $(".main-header").hide();
        
        $(".upload-profile-pic-view").animate({
            top: "0"
        }, 500, function() {
            var data = {
                "operation": "upload_employers_profile_picture",
                "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>"
            };
            
            var new_height2 = new_height / 2 + 300;
            
            $("body").css({
                "overflow": "hidden"
            });
            
            $(this).css({
                "line-height": new_height2 + "px",
                "text-align": "center"
            }).html("<img src='/img/loading.gif' />");
            
            $.post("/ajax", data, function(r) {
                $(".upload-profile-pic-view").css({
                    "line-height": "normal",
                    "text-align": "left"
                }).html(r);
            });
        });
    });

    $("#delete").on("click", function() {
        var c = confirm("Are you sure you want to delete you profile picture?");

        if (c) {
            var data = {
                "operation": "image_delete",
                "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>"
            };

            $.post("/ajax", data, function(r) {
                if (r == "ok") {
                    $(".img1").hide();
                    $(".img-btn-1, .img-btn-2").hide();
                    $(".loading").show();

                    setTimeout(function() {
                        $(".loading").hide();
                        $(".img1").fadeIn(500).html('<img src="/img/users/nopropic.png" />');
                        $(".img-btn-2").show();
                    }, 2500);
                }
            });
        }
    });

    function uploadPictureSize() {
        var height = $(window).height();
        var width = $(window).width() + 15;
        new_height = height - 2;
        
        $(".upload-profile-pic-view").css({
            "height": new_height + "px",
            "width": width + "px"
        });
    }

    function loadProfilePic(image = '', cropped = false) {
        if (cropped) {
            $(".upload-profile-pic-view").animate({
                top: "-100%"
            }, 500, function() {
                $(this).html("");
                $(".main-header").show();
                $("body").css({
                    "overflow": "auto"
                });
                
                $(".img1").hide();
                $(".img-btn-1, .img-btn-2").hide();
                $(".loading").show();

                setTimeout(function() {
                    $(".loading").hide();
                    $(".img1").fadeIn(500).html('<img src="/img/users/profile_pictures/' + image + '" />');
                    $(".img-btn-1").show();
                }, 2500);
            });
        } else {
            if (image) {
                $(".loading").show();

                setTimeout(function() {
                    $(".loading").hide();
                    $(".img1").fadeIn(500).html('<img src="/img/users/profile_pictures/' + image + '" />');
                    $(".img-btn-1").show();
                }, 1000);
            } else {
                $(".loading").show();

                setTimeout(function() {
                    $(".loading").hide();
                    $(".img1").fadeIn(500).html('<img src="/img/users/nopropic.png" />');
                    $(".img-btn-2").show();
                }, 1000);
            }
        }
    }

    function inputAnimate(field) {
        $("#" + field).animate({
            padding: "10px 5px"
        }, 500, function() {
            $("#" + field).css({
                "font-size": "18px"
            });
        });
    }

    function focusoutAnimate(field) {
        $("#" + field).animate({
            padding: "0px"
        }, 500, function() {
            $("#" + field).css({
                "font-size": "15px"
            });
        });
    }
</script>