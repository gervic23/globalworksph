<?php use App\Controller\GlobalworksController; $globalworks = new GlobalworksController(); ?>
<div class="job-post-preview">
    <div>&nbsp;</div>
    <div class="prev-main-header">
        <div class="header-logo">
            <?php if ($this->request->data('header-img')): ?>
                <div id="header" class="header">
                    <img src="/img/users/job_header_tmp/<?= $this->request->data('header-img') ?>" />
                </div>
            <?php endif; ?>

            <?php if ($this->request->data('logo-img')): ?>
                <div class="logo">
                    <img src="/img/users/company_logo_tmp/<?= $this->request->data('logo-img') ?>" />
                </div>
            <?php endif; ?>
        </div>
        <h1><?= $this->request->data('job-title') ?></h1>
        <h4><?= $this->request->data('company-name') ?></h4>
    </div>
    
    <div class="contents">
        <div class="left">
            <div class="description">
                <h2>Job Description</h2>
                <?= nl2br($this->request->data('description')) ?>
            </div>
            
            <?php if ($this->request->data('job-website')): ?>
                <div class="website">
                    <h2>Company Website</h2>
                    <?php
                        $str1 = substr($this->request->data('job-website'), 0, 7) ;
                        $str2 = substr($this->request->data('job-website'), 0, 8) ;
                    
                        if ($str1 == 'http://' || $str2 == 'https://') {
                            echo '<a target="_blank" href="' . $this->request->data('job-website') . '">' . $this->request->data('job-website') . '</a>';
                        } else {
                            echo '<a target="_blank" href="http://' . $this->request->data('job-website') . '">http://' . $this->request->data('job-website') . '</a>';
                        }
                    ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="right">
            <div class="job-details">
                <h2>Job Details</h2>
                <table>
                    <tr>
                        <td>Job Category:</td>
                        <td><?= $this->request->data('job-category') ?></td>
                    </tr>
                    <tr>
                        <td>Job Type:</td>
                        <td><?= $this->request->data('job-type') ?></td>
                    </tr>
                    <?php if ($this->request->data('job-base')): ?>
                        <tr>
                            <td>Job Base:</td>
                            <td><?= $this->request->data('job-base') ?></td>
                        </tr>
                    <?php endif; ?>
                    <tr>
                        <td>Working Experience:</td>
                        <td><?= $this->request->data('years-exp-start') ?> <?= ($this->request->data('years-exp-end') !== '') ? 'to ' . $this->request->data('years-exp-end') : ''; ?></td>
                    </tr>
                    <tr>
                        <td>Closing Date:</td>
                        <td><?= $this->request->data('closing-date') ?></td>
                    </tr>
                </table>
            </div>
            <?php if ($this->request->data('job-email') || $this->request->data('job-contact-number')): ?>
                <div class="contact-info">
                    <h2>Contact Information</h2>
                    <table>
                        <?php if ($this->request->data('job-email')): ?>
                            <tr>
                                <td>Email:</td>
                                <td><?= $this->request->data('job-email') ?></td>
                            </tr>
                        <?php endif; ?>

                        <?php if ($this->request->data('job-contact-number')): ?>
                            <tr>
                                <td>Contact Number:</td>
                                <td><?= $this->request->data('job-contact-number') ?></td>
                            </tr>
                        <?php endif; ?>
                    </table>
                </div>
            <?php endif; ?>
            
            <?php if ($this->request->data('job-location')): ?>
                <div class="location">
                    <h2>Location</h2>
                    <?= $this->request->data('job-location') ?>
                    <div class="country"><?= $globalworks->country_to_str($this->request->data('country')) ?>.</div>
                </div>
            <?php endif; ?>

            <?php if ($this->request->data('googlemap')): $location = urlencode($this->request->data('job-location') . ' ' . $this->request->data('country')); ?>
                <div class="googlemap">
                    <h2>Map</h2>
                    <iframe
                        frameborder="0" style="border:0"
                        src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDg3sqfQCdmusqPP_M4j40R_xuIBpiUSG8
                        &q='<?= $location ?>" allowfullscreen>
                    </iframe>
                </div>
            <?php endif; ?>

            <?php if ($this->request->data('qualifications')): ?>
                <div class="qualifications">
                    <h2>Qualifications</h2>
                    <ul>
                        <?php foreach ($this->request->data('qualifications') as $qlf): ?>
                            <?php if ($qlf !== ''): ?>
                                <li><?= $qlf ?></li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>
            
            <?php if ($this->request->data('responsibilities')): ?>
                <div class="responsibilities">
                    <h2>Responsibilities</h2>
                    <ul>
                        <?php foreach ($this->request->data('responsibilities') as $res): ?>
                            <?php if ($res !== ''): ?>
                                <li><?= $res ?></li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>
        </div>
        <div class="clear"></div>
    </div>
    
    <?= $this->Form->create() ?>
    <?= $this->Form->hidden('post-type', ['id' => 'post-type', 'value' => '']) ?>
    <?= $this->Form->hidden('header-img', ['value' => $this->request->data('header-img')]); ?>
    <?= $this->Form->hidden('logo-img', ['value' => $this->request->data('logo-img')]); ?>
    <?= $this->Form->hidden('job-title', ['value' => $this->request->data('job-title')]) ?>
    <?= $this->Form->hidden('company-name', ['value' => $this->request->data('company-name')]) ?>
    <?= $this->Form->hidden('job-category', ['value' => $this->request->data('job-category')]) ?>
    <?= $this->Form->hidden('job-type', ['value' => $this->request->data('job-type')]) ?>
    <?= $this->Form->hidden('job-base', ['value' => $this->request->data('job-base')]); ?>
    <?= $this->Form->hidden('country', ['value' => $this->request->data('country')]); ?>
    <?= $this->Form->hidden('description', ['value' => $this->request->data('description')]); ?>
    <?php if ($this->request->data('qualifications')): ?>
        <?php foreach ($this->request->data('qualifications') as $qlf): ?>
            <?= $this->Form->hidden('qualifications[]', ['value' => $qlf]); ?>
        <?php endforeach; ?>
    <?php endif; ?>
    <?php if ($this->request->data('responsibilities')): ?>
        <?php foreach ($this->request->data('responsibilities') as $res): ?>
            <?= $this->Form->hidden('responsibilities[]', ['value' => $res]); ?>
        <?php endforeach; ?>
    <?php endif; ?>
    <?= $this->Form->hidden('years-exp-start', ['value' => $this->request->data('years-exp-start')]); ?>
    <?= $this->Form->hidden('years-exp-end', ['value' => $this->request->data('years-exp-end')]); ?>
    <?= $this->Form->hidden('job-location', ['value' => $this->request->data('job-location')]); ?>
    <?= $this->Form->hidden('job-website', ['value' => $this->request->data('job-website')]); ?>
    <?= $this->Form->hidden('job-email', ['value' => $this->request->data('job-email')]); ?>
    <?= $this->Form->hidden('job-contact-number', ['value' => $this->request->data('job-contact-number')]); ?>
    <?= $this->Form->hidden('closing-date', ['value' => $this->request->data('closing-date')]); ?>
    <?= $this->Form->hidden('googlemap', ['value' => $this->request->data('googlemap')]); ?>
    <div class="btn-container">
        <?= $this->Form->button('Publish', ['type' => 'button', 'id' => 'publish']) ?> <?= $this->Form->button('Go Back', ['type' => 'button', 'id' => 'go-back']) ?>
    </div>
    <?= $this->Form->end() ?>
</div>

<script>
    resize_job_preview();
    
    $(window).resize(function() {
        resize_job_preview();   
    });
    
    if (!$("#header").parent().length) {
        $(".job-post-preview .prev-main-header .header-logo .logo").css({
            "position": "inherit",
            "margin-top": "0px"
        });
    }
    
    $("#publish").on("click", function() {
        $("#post-type").attr("value", "publish");
        $(".job-post-preview form").submit();
    });
    
    $("#go-back").on("click", function() {
        $(".job-post-preview form").submit();
    });
    
    function resize_job_preview() {
        var height = $(window).height();
        var new_height = height - 33;
        var job_post_preview = $(".job-post-preview").height();
        
        if (job_post_preview <= height) {
            $(".job-post-preview").css({
                "height": new_height + "px"
            });   
        }
    }
</script>