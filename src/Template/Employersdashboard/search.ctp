<?php 
    use App\Controller\GlobalworksController; $globalworks = new GlobalworksController(); 

    $employment_stats = [
        '' => 'All', 
        'Unemployed' => 'Unemployed', 
        'Employed' => 'Employed',
    ];

    $countries = [
    '' => 'All Countries',
    'AF'=>'AFGHANISTAN',
    'AL'=>'ALBANIA',
    'DZ'=>'ALGERIA',
    'AS'=>'AMERICAN SAMOA',
    'AD'=>'ANDORRA',
    'AO'=>'ANGOLA',
    'AI'=>'ANGUILLA',
    'AQ'=>'ANTARCTICA',
    'AG'=>'ANTIGUA AND BARBUDA',
    'AR'=>'ARGENTINA',
    'AM'=>'ARMENIA',
    'AW'=>'ARUBA',
    'AU'=>'AUSTRALIA',
    'AT'=>'AUSTRIA',
    'AZ'=>'AZERBAIJAN',
    'BS'=>'BAHAMAS',
    'BH'=>'BAHRAIN',
    'BD'=>'BANGLADESH',
    'BB'=>'BARBADOS',
    'BY'=>'BELARUS',
    'BE'=>'BELGIUM',
    'BZ'=>'BELIZE',
    'BJ'=>'BENIN',
    'BM'=>'BERMUDA',
    'BT'=>'BHUTAN',
    'BO'=>'BOLIVIA',
    'BA'=>'BOSNIA AND HERZEGOVINA',
    'BW'=>'BOTSWANA',
    'BV'=>'BOUVET ISLAND',
    'BR'=>'BRAZIL',
    'IO'=>'BRITISH INDIAN OCEAN TERRITORY',
    'BN'=>'BRUNEI DARUSSALAM',
    'BG'=>'BULGARIA',
    'BF'=>'BURKINA FASO',
    'BI'=>'BURUNDI',
    'KH'=>'CAMBODIA',
    'CM'=>'CAMEROON',
    'CA'=>'CANADA',
    'CV'=>'CAPE VERDE',
    'KY'=>'CAYMAN ISLANDS',
    'CF'=>'CENTRAL AFRICAN REPUBLIC',
    'TD'=>'CHAD',
    'CL'=>'CHILE',
    'CN'=>'CHINA',
    'CX'=>'CHRISTMAS ISLAND',
    'CC'=>'COCOS (KEELING) ISLANDS',
    'CO'=>'COLOMBIA',
    'KM'=>'COMOROS',
    'CG'=>'CONGO',
    'CD'=>'CONGO, THE DEMOCRATIC REPUBLIC OF THE',
    'CK'=>'COOK ISLANDS',
    'CR'=>'COSTA RICA',
    'CI'=>'COTE D IVOIRE',
    'HR'=>'CROATIA',
    'CU'=>'CUBA',
    'CY'=>'CYPRUS',
    'CZ'=>'CZECH REPUBLIC',
    'DK'=>'DENMARK',
    'DJ'=>'DJIBOUTI',
    'DM'=>'DOMINICA',
    'DO'=>'DOMINICAN REPUBLIC',
    'TP'=>'EAST TIMOR',
    'EC'=>'ECUADOR',
    'EG'=>'EGYPT',
    'SV'=>'EL SALVADOR',
    'GQ'=>'EQUATORIAL GUINEA',
    'ER'=>'ERITREA',
    'EE'=>'ESTONIA',
    'ET'=>'ETHIOPIA',
    'FK'=>'FALKLAND ISLANDS (MALVINAS)',
    'FO'=>'FAROE ISLANDS',
    'FJ'=>'FIJI',
    'FI'=>'FINLAND',
    'FR'=>'FRANCE',
    'GF'=>'FRENCH GUIANA',
    'PF'=>'FRENCH POLYNESIA',
    'TF'=>'FRENCH SOUTHERN TERRITORIES',
    'GA'=>'GABON',
    'GM'=>'GAMBIA',
    'GE'=>'GEORGIA',
    'DE'=>'GERMANY',
    'GH'=>'GHANA',
    'GI'=>'GIBRALTAR',
    'GR'=>'GREECE',
    'GL'=>'GREENLAND',
    'GD'=>'GRENADA',
    'GP'=>'GUADELOUPE',
    'GU'=>'GUAM',
    'GT'=>'GUATEMALA',
    'GN'=>'GUINEA',
    'GW'=>'GUINEA-BISSAU',
    'GY'=>'GUYANA',
    'HT'=>'HAITI',
    'HM'=>'HEARD ISLAND AND MCDONALD ISLANDS',
    'VA'=>'HOLY SEE (VATICAN CITY STATE)',
    'HN'=>'HONDURAS',
    'HK'=>'HONG KONG',
    'HU'=>'HUNGARY',
    'IS'=>'ICELAND',
    'IN'=>'INDIA',
    'ID'=>'INDONESIA',
    'IR'=>'IRAN, ISLAMIC REPUBLIC OF',
    'IQ'=>'IRAQ',
    'IE'=>'IRELAND',
    'IL'=>'ISRAEL',
    'IT'=>'ITALY',
    'JM'=>'JAMAICA',
    'JP'=>'JAPAN',
    'JO'=>'JORDAN',
    'KZ'=>'KAZAKSTAN',
    'KE'=>'KENYA',
    'KI'=>'KIRIBATI',
    'KP'=>'KOREA DEMOCRATIC PEOPLES REPUBLIC OF',
    'KR'=>'KOREA REPUBLIC OF',
    'KW'=>'KUWAIT',
    'KG'=>'KYRGYZSTAN',
    'LA'=>'LAO PEOPLES DEMOCRATIC REPUBLIC',
    'LV'=>'LATVIA',
    'LB'=>'LEBANON',
    'LS'=>'LESOTHO',
    'LR'=>'LIBERIA',
    'LY'=>'LIBYAN ARAB JAMAHIRIYA',
    'LI'=>'LIECHTENSTEIN',
    'LT'=>'LITHUANIA',
    'LU'=>'LUXEMBOURG',
    'MO'=>'MACAU',
    'MK'=>'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF',
    'MG'=>'MADAGASCAR',
    'MW'=>'MALAWI',
    'MY'=>'MALAYSIA',
    'MV'=>'MALDIVES',
    'ML'=>'MALI',
    'MT'=>'MALTA',
    'MH'=>'MARSHALL ISLANDS',
    'MQ'=>'MARTINIQUE',
    'MR'=>'MAURITANIA',
    'MU'=>'MAURITIUS',
    'YT'=>'MAYOTTE',
    'MX'=>'MEXICO',
    'FM'=>'MICRONESIA, FEDERATED STATES OF',
    'MD'=>'MOLDOVA, REPUBLIC OF',
    'MC'=>'MONACO',
    'MN'=>'MONGOLIA',
    'MS'=>'MONTSERRAT',
    'MA'=>'MOROCCO',
    'MZ'=>'MOZAMBIQUE',
    'MM'=>'MYANMAR',
    'NA'=>'NAMIBIA',
    'NR'=>'NAURU',
    'NP'=>'NEPAL',
    'NL'=>'NETHERLANDS',
    'AN'=>'NETHERLANDS ANTILLES',
    'NC'=>'NEW CALEDONIA',
    'NZ'=>'NEW ZEALAND',
    'NI'=>'NICARAGUA',
    'NE'=>'NIGER',
    'NG'=>'NIGERIA',
    'NU'=>'NIUE',
    'NF'=>'NORFOLK ISLAND',
    'MP'=>'NORTHERN MARIANA ISLANDS',
    'NO'=>'NORWAY',
    'OM'=>'OMAN',
    'PK'=>'PAKISTAN',
    'PW'=>'PALAU',
    'PS'=>'PALESTINIAN TERRITORY, OCCUPIED',
    'PA'=>'PANAMA',
    'PG'=>'PAPUA NEW GUINEA',
    'PY'=>'PARAGUAY',
    'PE'=>'PERU',
    'PH'=>'PHILIPPINES',
    'PN'=>'PITCAIRN',
    'PL'=>'POLAND',
    'PT'=>'PORTUGAL',
    'PR'=>'PUERTO RICO',
    'QA'=>'QATAR',
    'RE'=>'REUNION',
    'RO'=>'ROMANIA',
    'RU'=>'RUSSIAN FEDERATION',
    'RW'=>'RWANDA',
    'SH'=>'SAINT HELENA',
    'KN'=>'SAINT KITTS AND NEVIS',
    'LC'=>'SAINT LUCIA',
    'PM'=>'SAINT PIERRE AND MIQUELON',
    'VC'=>'SAINT VINCENT AND THE GRENADINES',
    'WS'=>'SAMOA',
    'SM'=>'SAN MARINO',
    'ST'=>'SAO TOME AND PRINCIPE',
    'SA'=>'SAUDI ARABIA',
    'SN'=>'SENEGAL',
    'SC'=>'SEYCHELLES',
    'SL'=>'SIERRA LEONE',
    'SG'=>'SINGAPORE',
    'SK'=>'SLOVAKIA',
    'SI'=>'SLOVENIA',
    'SB'=>'SOLOMON ISLANDS',
    'SO'=>'SOMALIA',
    'ZA'=>'SOUTH AFRICA',
    'GS'=>'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS',
    'ES'=>'SPAIN',
    'LK'=>'SRI LANKA',
    'SD'=>'SUDAN',
    'SR'=>'SURINAME',
    'SJ'=>'SVALBARD AND JAN MAYEN',
    'SZ'=>'SWAZILAND',
    'SE'=>'SWEDEN',
    'CH'=>'SWITZERLAND',
    'SY'=>'SYRIAN ARAB REPUBLIC',
    'TW'=>'TAIWAN, PROVINCE OF CHINA',
    'TJ'=>'TAJIKISTAN',
    'TZ'=>'TANZANIA, UNITED REPUBLIC OF',
    'TH'=>'THAILAND',
    'TG'=>'TOGO',
    'TK'=>'TOKELAU',
    'TO'=>'TONGA',
    'TT'=>'TRINIDAD AND TOBAGO',
    'TN'=>'TUNISIA',
    'TR'=>'TURKEY',
    'TM'=>'TURKMENISTAN',
    'TC'=>'TURKS AND CAICOS ISLANDS',
    'TV'=>'TUVALU',
    'UG'=>'UGANDA',
    'UA'=>'UKRAINE',
    'AE'=>'UNITED ARAB EMIRATES',
    'GB'=>'UNITED KINGDOM',
    'US'=>'UNITED STATES',
    'UM'=>'UNITED STATES MINOR OUTLYING ISLANDS',
    'UY'=>'URUGUAY',
    'UZ'=>'UZBEKISTAN',
    'VU'=>'VANUATU',
    'VE'=>'VENEZUELA',
    'VN'=>'VIET NAM',
    'VG'=>'VIRGIN ISLANDS, BRITISH',
    'VI'=>'VIRGIN ISLANDS, U.S.',
    'WF'=>'WALLIS AND FUTUNA',
    'EH'=>'WESTERN SAHARA',
    'YE'=>'YEMEN',
    'YU'=>'YUGOSLAVIA',
    'ZM'=>'ZAMBIA',
    'ZW'=>'ZIMBABWE',
    ];
?>

<div class="employers-dashboard">
    <div class="left">
        <?= $this->element('Nav/dashboard-employer') ?>
    </div>
    <div class="right">
        <div class="contents">
            <div class="employers-search">
                <h1>Search Candidates To Hire</h2>
                <div class="form">
                    <form method="GET" accept-chaset="UTF-8" action="/employers/search">
                        <div class="input">
                            <?= $this->Form->label('keywords', 'Keywords:') ?>
                            <?= $this->Form->text('keywords', ['id' => 'keywords', 'placeholder' => 'Enter keywords like Job Position, Job Experience, Skills and etc...', 'autocomplete' => 'off']) ?>
                            <div class="employer-search-flash-error"></div>
                        </div>
                        <div class="input">
                            <?= $this->Form->label('employment_status', 'Employment Status') ?>
                            <?= $this->Form->select('employment_status', $employment_stats, ['id' => 'employment_status', 'default' => 'Unemployed']) ?>
                        </div>
                        <div class="input">
                            <?= $this->Form->input('desire_salary', ['type' => 'hidden', 'id' => 'desire_salary']) ?>
                            <div class="salary">
                                <h3>Candidates Desired Salary</h3>
                                <div class="container">
                                    <ul>
                                        <li>
                                            <?= $this->Form->checkbox('salary_range0', ['id' => 'salary_range0', 'value' => 'Php 8000 - Php 10000']); ?>
                                            <label for="salary_range0">Php 8,000 - Php 10,000</label>
                                        </li>

                                        <li>
                                            <?= $this->Form->checkbox('salary_range1', ['id' => 'salary_range1', 'value' => 'Php 10000 - Php 15000']); ?>
                                            <label for="salary_range1">Php 10,000 - Php 15,000</label>
                                        </li>
                                    </ul>
                                </div>
                                <div class="container">
                                    <ul>
                                        <li>
                                            <?= $this->Form->checkbox('salary_range2', ['id' => 'salary_range2', 'value' => 'Php 15000 - Php 20000']); ?>
                                            <label for="salary_range2">Php 15,000 - Php 20,000</label>
                                        </li>
                                        <li>
                                            <?= $this->Form->checkbox('salary_range3', ['id' => 'salary_range3', 'value' => 'Php 20000 - Php 25000']); ?>
                                            <label for="salary_range3">Php 20,000 - Php 25,000</label>
                                        </li>
                                    </ul>
                                </div>
                                <div class="container">
                                    <ul>
                                        <li>
                                            <?= $this->Form->checkbox('salary_range4', ['id' => 'salary_range4', 'value' => 'Php 25000 - Php 30000']); ?>
                                            <label for="salary_range4">Php 25,000 - Php 30,000</label>
                                        </li>
                                        <li>
                                            <?= $this->Form->checkbox('salary_range5', ['id' => 'salary_range5', 'value' => 'Php 30000 - Php 35000']); ?>
                                            <label for="salary_range5">Php 30,000 - Php 35,000</label>
                                        </li>
                                    </ul>
                                </div>
                                <div class="container">
                                    <ul>
                                        <li>
                                            <?= $this->Form->checkbox('salary_range6', ['id' => 'salary_range6', 'value' => 'Php 35000 - Php 40000']); ?>
                                            <label for="salary_range6">Php 35,000 - Php 40,000</label>
                                        </li>
                                        <li>
                                            <?= $this->Form->checkbox('salary_range7', ['id' => 'salary_range7', 'value' => 'Php 40000 - Php 45000']); ?>
                                            <label for="salary_range7">Php 40,000 - Php 45,000</label>
                                        </li>
                                    </ul>
                                </div>
                                <div class="container">
                                    <ul>
                                        <li>
                                            <?= $this->Form->checkbox('salary_range8', ['id' => 'salary_range8', 'value' => 'Php 45000 - Php 50000']); ?>
                                            <label for="salary_range8">Php 45,000 - Php 50,000</label>
                                        </li>
                                        <li>
                                            <?= $this->Form->checkbox('salary_range9', ['id' => 'salary_range9', 'value' => 'Morethan Php 50000']); ?>
                                            <label for="salary_range9">Morethan Php 50,000</label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="input">
                            <div class="education">
                                <h3>Candidates Education Attainment</h3>
                                <div class="content">
                                    <ul>
                                        <li>
                                            <input type="radio" name="education" id="education-1" value="College/Bachelor Degree"/>
                                            <label for="education-1">College/Bachelor Degree</label>
                                            <div class="check"></div>
                                        </li>
                                        <li>
                                            <input type="radio" name="education" id="education-2" value="College/Associated Degree" />
                                            <label for="education-2">College/Associated Degree</label>
                                            <div class="check"></div>
                                        </li>
                                        <li>
                                            <input type="radio" name="education" id="education-3" value="Post Graduate Degree" />
                                            <label for="education-3">Post Graduate Degree</label>
                                            <div class="check"></div>
                                        </li>
                                        <li>
                                            <input type="radio" name="education" id="education-4" value="Highschool Graduate" />
                                            <label for="education-4">Highschool Graduate</label>
                                            <div class="check"></div>
                                        </li>
                                        <li>
                                            <input type="radio" name="education" id="education-5" value="Not Graduated Highschool" />
                                            <label for="education-5">Not Graduated Highschool</label>
                                            <div class="check"></div>
                                        </li>
                                        <li>
                                            <input type="radio" name="education" id="education-6" value="All" checked />
                                            <label for="education-6">All</label>
                                            <div class="check"></div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="input">
                            <?= $this->Form->label('country', 'Candidate\'s Country:') ?>
                            <?= $this->Form->select('country', $countries, ['id' => 'country', 'default' => 'PH']) ?>
                        </div>
                        <div class="btn-container">
                            <span id="search-btn">Search</span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>
<?= $this->Html->script('employersdashboard') ?>
<script>
    var salary_range = [];

    $("#search-btn").on("click", function() {
        if ($("#keywords").val() == "") {
            $("#keywords").focus();
            $("#keywords").css({
                "border-color": "red",
                "background-color": "#ffe4e4"
            });
            $(".employer-search-flash-error").html("Please enter a keyword.");
        } else if ($("#keywords").val().length <= 2) {
            $("#keywords").focus();
            $("#keywords").css({
                "border-color": "red",
                "background-color": "#ffe4e4"
            });
            $(".employer-search-flash-error").html("Keywords is too short.");
        } else {
            $(".employers-search .form form").submit();
        }
    });

     $("#keywords").on("keypress", function(e) {
        var code = e.keyCode || e.which;
        if (code == 13) {
            e.preventDefault();
            return false;
        }
     }).on("keyup input", function() {
        $(this).css({
            "border-color": "#ccc",
            "background-color": "#fff"
        });
        $(".employer-search-flash-error").html("");
     });

    $("#salary_range0").on("change", function() {
        var range = $(this).val();
        if ($(this).is(":checked")) {
            salary_range.push(range);
            addSalaryRange();
        } else {
            removeSalaryRange(salary_range.indexOf(range));
        }
    });
    
    $("#salary_range1").on("change", function() {
        var range = $(this).val();
        if ($(this).is(":checked")) {
            salary_range.push(range);
            addSalaryRange();
        } else {
            removeSalaryRange(salary_range.indexOf(range));
        }
    });
    
    $("#salary_range2").on("change", function() {
        var range = $(this).val();
        if ($(this).is(":checked")) {
            salary_range.push(range);
            addSalaryRange();
        } else {
            removeSalaryRange(salary_range.indexOf(range));
        }
    });
    
    $("#salary_range3").on("change", function() {
        var range = $(this).val();
        if ($(this).is(":checked")) {
            salary_range.push(range);
            addSalaryRange();
        } else {
            removeSalaryRange(salary_range.indexOf(range));
        }
    });
    
    $("#salary_range4").on("change", function() {
        var range = $(this).val();
        if ($(this).is(":checked")) {
            salary_range.push(range);
            addSalaryRange();
        } else {
            removeSalaryRange(salary_range.indexOf(range));
        }
    });
    
    $("#salary_range5").on("change", function() {
        var range = $(this).val();
        if ($(this).is(":checked")) {
            salary_range.push(range);
            addSalaryRange();
        } else {
            removeSalaryRange(salary_range.indexOf(range));
        }
    });
    
    $("#salary_range6").on("change", function() {
        var range = $(this).val();
        if ($(this).is(":checked")) {
            salary_range.push(range);
            addSalaryRange();
        } else {
            removeSalaryRange(salary_range.indexOf(range));
        }
    });
    
    $("#salary_range7").on("change", function() {
        var range = $(this).val();
        if ($(this).is(":checked")) {
            salary_range.push(range);
            addSalaryRange();
        } else {
            removeSalaryRange(salary_range.indexOf(range));
        }
    });
    
    $("#salary_range8").on("change", function() {
        var range = $(this).val();
        if ($(this).is(":checked")) {
            salary_range.push(range);
            addSalaryRange();
        } else {
            removeSalaryRange(salary_range.indexOf(range));
        }
    });
    
    $("#salary_range9").on("change", function() {
        var range = $(this).val();
        if ($(this).is(":checked")) {
            salary_range.push(range);
            addSalaryRange();
        } else {
            removeSalaryRange(salary_range.indexOf(range));
        }
    });
    
    function addSalaryRange() {
        $("#desire_salary").attr("value", salary_range);
    }
    
    function removeSalaryRange(index) {
        range = salary_range[index];
        salary_range.splice(index, 1);
        addSalaryRange();
    }
</script>