<?php use App\Controller\GlobalworksController; $globalworks = new GlobalworksController(); ?>

<div class="employers-dashboard">
    <div class="left">
        <?= $this->element('Nav/dashboard-employer') ?>
    </div>
    <div class="right">
        <div class="contents">
            <?= $this->element('Nav/account_nav'); ?>
            <div class="changepassword-main">
                <?= $this->Flash->render() ?>
                <?= $this->Form->create($user) ?>
                <?= $this->Form->control('current_password', ['label' => 'Current Password:', 'type' => 'password']) ?>
                <?= $this->Form->control('new_password', ['label' => 'New Password:', 'type' => 'password']) ?>
                <?= $this->Form->control('rpassword', ['label' => 'Confirm Password:', 'type' => 'password']) ?>
                <div class="btn-container">
                    <?= $this->Form->submit('Save') ?>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>
<?= $this->Html->script('employersdashboard') ?>
<script>
    $("#current-password").on("keyup innput", function() {
        $(this).css({
            "border-color": "#ccc",
            "background-color": "#fff"
        });
        
        $(".employers-dashboard .changepassword-main .error:nth-child(2) .error-message").hide();
    }).on("click focus", function() {
        inputAnimate("current-password");
    }).on("focusout", function() {
        focusoutAnimate("current-password");
    });

    $("#new-password").on("keyup innput", function() {
        $(this).css({
            "border-color": "#ccc",
            "background-color": "#fff"
        });
        
        $(".employers-dashboard .changepassword-main .error:nth-child(3) .error-message").hide();
    }).on("click focus", function() {
        inputAnimate("new-password");
    }).on("focusout", function() {
        focusoutAnimate("new-password");
    });

    $("#rpassword").on("keyup innput", function() {
        $(this).css({
            "border-color": "#ccc",
            "background-color": "#fff"
        });
        
        $(".employers-dashboard .changepassword-main .error:nth-child(4) .error-message").hide();
    }).on("click focus", function() {
        inputAnimate("rpassword");
    }).on("focusout", function() {
        focusoutAnimate("rpassword");
    });

    function inputAnimate(field) {
        $("#" + field).animate({
            padding: "10px 5px"
        }, 500, function() {
            $("#" + field).css({
                "font-size": "18px"
            });
        });
    }

    function focusoutAnimate(field) {
        $("#" + field).animate({
            padding: "0px"
        }, 500, function() {
            $("#" + field).css({
                "font-size": "15px"
            });
        });
    }
</script>