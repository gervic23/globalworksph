<?php 
    use App\Controller\GlobalworksController; $globalworks = new GlobalworksController();

    $countries = [
        'AF'=>'AFGHANISTAN',
        'AL'=>'ALBANIA',
        'DZ'=>'ALGERIA',
        'AS'=>'AMERICAN SAMOA',
        'AD'=>'ANDORRA',
        'AO'=>'ANGOLA',
        'AI'=>'ANGUILLA',
        'AQ'=>'ANTARCTICA',
        'AG'=>'ANTIGUA AND BARBUDA',
        'AR'=>'ARGENTINA',
        'AM'=>'ARMENIA',
        'AW'=>'ARUBA',
        'AU'=>'AUSTRALIA',
        'AT'=>'AUSTRIA',
        'AZ'=>'AZERBAIJAN',
        'BS'=>'BAHAMAS',
        'BH'=>'BAHRAIN',
        'BD'=>'BANGLADESH',
        'BB'=>'BARBADOS',
        'BY'=>'BELARUS',
        'BE'=>'BELGIUM',
        'BZ'=>'BELIZE',
        'BJ'=>'BENIN',
        'BM'=>'BERMUDA',
        'BT'=>'BHUTAN',
        'BO'=>'BOLIVIA',
        'BA'=>'BOSNIA AND HERZEGOVINA',
        'BW'=>'BOTSWANA',
        'BV'=>'BOUVET ISLAND',
        'BR'=>'BRAZIL',
        'IO'=>'BRITISH INDIAN OCEAN TERRITORY',
        'BN'=>'BRUNEI DARUSSALAM',
        'BG'=>'BULGARIA',
        'BF'=>'BURKINA FASO',
        'BI'=>'BURUNDI',
        'KH'=>'CAMBODIA',
        'CM'=>'CAMEROON',
        'CA'=>'CANADA',
        'CV'=>'CAPE VERDE',
        'KY'=>'CAYMAN ISLANDS',
        'CF'=>'CENTRAL AFRICAN REPUBLIC',
        'TD'=>'CHAD',
        'CL'=>'CHILE',
        'CN'=>'CHINA',
        'CX'=>'CHRISTMAS ISLAND',
        'CC'=>'COCOS (KEELING) ISLANDS',
        'CO'=>'COLOMBIA',
        'KM'=>'COMOROS',
        'CG'=>'CONGO',
        'CD'=>'CONGO, THE DEMOCRATIC REPUBLIC OF THE',
        'CK'=>'COOK ISLANDS',
        'CR'=>'COSTA RICA',
        'CI'=>'COTE D IVOIRE',
        'HR'=>'CROATIA',
        'CU'=>'CUBA',
        'CY'=>'CYPRUS',
        'CZ'=>'CZECH REPUBLIC',
        'DK'=>'DENMARK',
        'DJ'=>'DJIBOUTI',
        'DM'=>'DOMINICA',
        'DO'=>'DOMINICAN REPUBLIC',
        'TP'=>'EAST TIMOR',
        'EC'=>'ECUADOR',
        'EG'=>'EGYPT',
        'SV'=>'EL SALVADOR',
        'GQ'=>'EQUATORIAL GUINEA',
        'ER'=>'ERITREA',
        'EE'=>'ESTONIA',
        'ET'=>'ETHIOPIA',
        'FK'=>'FALKLAND ISLANDS (MALVINAS)',
        'FO'=>'FAROE ISLANDS',
        'FJ'=>'FIJI',
        'FI'=>'FINLAND',
        'FR'=>'FRANCE',
        'GF'=>'FRENCH GUIANA',
        'PF'=>'FRENCH POLYNESIA',
        'TF'=>'FRENCH SOUTHERN TERRITORIES',
        'GA'=>'GABON',
        'GM'=>'GAMBIA',
        'GE'=>'GEORGIA',
        'DE'=>'GERMANY',
        'GH'=>'GHANA',
        'GI'=>'GIBRALTAR',
        'GR'=>'GREECE',
        'GL'=>'GREENLAND',
        'GD'=>'GRENADA',
        'GP'=>'GUADELOUPE',
        'GU'=>'GUAM',
        'GT'=>'GUATEMALA',
        'GN'=>'GUINEA',
        'GW'=>'GUINEA-BISSAU',
        'GY'=>'GUYANA',
        'HT'=>'HAITI',
        'HM'=>'HEARD ISLAND AND MCDONALD ISLANDS',
        'VA'=>'HOLY SEE (VATICAN CITY STATE)',
        'HN'=>'HONDURAS',
        'HK'=>'HONG KONG',
        'HU'=>'HUNGARY',
        'IS'=>'ICELAND',
        'IN'=>'INDIA',
        'ID'=>'INDONESIA',
        'IR'=>'IRAN, ISLAMIC REPUBLIC OF',
        'IQ'=>'IRAQ',
        'IE'=>'IRELAND',
        'IL'=>'ISRAEL',
        'IT'=>'ITALY',
        'JM'=>'JAMAICA',
        'JP'=>'JAPAN',
        'JO'=>'JORDAN',
        'KZ'=>'KAZAKSTAN',
        'KE'=>'KENYA',
        'KI'=>'KIRIBATI',
        'KP'=>'KOREA DEMOCRATIC PEOPLES REPUBLIC OF',
        'KR'=>'KOREA REPUBLIC OF',
        'KW'=>'KUWAIT',
        'KG'=>'KYRGYZSTAN',
        'LA'=>'LAO PEOPLES DEMOCRATIC REPUBLIC',
        'LV'=>'LATVIA',
        'LB'=>'LEBANON',
        'LS'=>'LESOTHO',
        'LR'=>'LIBERIA',
        'LY'=>'LIBYAN ARAB JAMAHIRIYA',
        'LI'=>'LIECHTENSTEIN',
        'LT'=>'LITHUANIA',
        'LU'=>'LUXEMBOURG',
        'MO'=>'MACAU',
        'MK'=>'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF',
        'MG'=>'MADAGASCAR',
        'MW'=>'MALAWI',
        'MY'=>'MALAYSIA',
        'MV'=>'MALDIVES',
        'ML'=>'MALI',
        'MT'=>'MALTA',
        'MH'=>'MARSHALL ISLANDS',
        'MQ'=>'MARTINIQUE',
        'MR'=>'MAURITANIA',
        'MU'=>'MAURITIUS',
        'YT'=>'MAYOTTE',
        'MX'=>'MEXICO',
        'FM'=>'MICRONESIA, FEDERATED STATES OF',
        'MD'=>'MOLDOVA, REPUBLIC OF',
        'MC'=>'MONACO',
        'MN'=>'MONGOLIA',
        'MS'=>'MONTSERRAT',
        'MA'=>'MOROCCO',
        'MZ'=>'MOZAMBIQUE',
        'MM'=>'MYANMAR',
        'NA'=>'NAMIBIA',
        'NR'=>'NAURU',
        'NP'=>'NEPAL',
        'NL'=>'NETHERLANDS',
        'AN'=>'NETHERLANDS ANTILLES',
        'NC'=>'NEW CALEDONIA',
        'NZ'=>'NEW ZEALAND',
        'NI'=>'NICARAGUA',
        'NE'=>'NIGER',
        'NG'=>'NIGERIA',
        'NU'=>'NIUE',
        'NF'=>'NORFOLK ISLAND',
        'MP'=>'NORTHERN MARIANA ISLANDS',
        'NO'=>'NORWAY',
        'OM'=>'OMAN',
        'PK'=>'PAKISTAN',
        'PW'=>'PALAU',
        'PS'=>'PALESTINIAN TERRITORY, OCCUPIED',
        'PA'=>'PANAMA',
        'PG'=>'PAPUA NEW GUINEA',
        'PY'=>'PARAGUAY',
        'PE'=>'PERU',
        'PH'=>'PHILIPPINES',
        'PN'=>'PITCAIRN',
        'PL'=>'POLAND',
        'PT'=>'PORTUGAL',
        'PR'=>'PUERTO RICO',
        'QA'=>'QATAR',
        'RE'=>'REUNION',
        'RO'=>'ROMANIA',
        'RU'=>'RUSSIAN FEDERATION',
        'RW'=>'RWANDA',
        'SH'=>'SAINT HELENA',
        'KN'=>'SAINT KITTS AND NEVIS',
        'LC'=>'SAINT LUCIA',
        'PM'=>'SAINT PIERRE AND MIQUELON',
        'VC'=>'SAINT VINCENT AND THE GRENADINES',
        'WS'=>'SAMOA',
        'SM'=>'SAN MARINO',
        'ST'=>'SAO TOME AND PRINCIPE',
        'SA'=>'SAUDI ARABIA',
        'SN'=>'SENEGAL',
        'SC'=>'SEYCHELLES',
        'SL'=>'SIERRA LEONE',
        'SG'=>'SINGAPORE',
        'SK'=>'SLOVAKIA',
        'SI'=>'SLOVENIA',
        'SB'=>'SOLOMON ISLANDS',
        'SO'=>'SOMALIA',
        'ZA'=>'SOUTH AFRICA',
        'GS'=>'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS',
        'ES'=>'SPAIN',
        'LK'=>'SRI LANKA',
        'SD'=>'SUDAN',
        'SR'=>'SURINAME',
        'SJ'=>'SVALBARD AND JAN MAYEN',
        'SZ'=>'SWAZILAND',
        'SE'=>'SWEDEN',
        'CH'=>'SWITZERLAND',
        'SY'=>'SYRIAN ARAB REPUBLIC',
        'TW'=>'TAIWAN, PROVINCE OF CHINA',
        'TJ'=>'TAJIKISTAN',
        'TZ'=>'TANZANIA, UNITED REPUBLIC OF',
        'TH'=>'THAILAND',
        'TG'=>'TOGO',
        'TK'=>'TOKELAU',
        'TO'=>'TONGA',
        'TT'=>'TRINIDAD AND TOBAGO',
        'TN'=>'TUNISIA',
        'TR'=>'TURKEY',
        'TM'=>'TURKMENISTAN',
        'TC'=>'TURKS AND CAICOS ISLANDS',
        'TV'=>'TUVALU',
        'UG'=>'UGANDA',
        'UA'=>'UKRAINE',
        'AE'=>'UNITED ARAB EMIRATES',
        'GB'=>'UNITED KINGDOM',
        'US'=>'UNITED STATES',
        'UM'=>'UNITED STATES MINOR OUTLYING ISLANDS',
        'UY'=>'URUGUAY',
        'UZ'=>'UZBEKISTAN',
        'VU'=>'VANUATU',
        'VE'=>'VENEZUELA',
        'VN'=>'VIET NAM',
        'VG'=>'VIRGIN ISLANDS, BRITISH',
        'VI'=>'VIRGIN ISLANDS, U.S.',
        'WF'=>'WALLIS AND FUTUNA',
        'EH'=>'WESTERN SAHARA',
        'YE'=>'YEMEN',
        'YU'=>'YUGOSLAVIA',
        'ZM'=>'ZAMBIA',
        'ZW'=>'ZIMBABWE',
      ];

    $months = [
        'January' => 'January',
        'February' => 'February',
        'March' => 'March',
        'April' => 'April',
        'May' => 'May',
        'June' => 'June',
        'July' => 'July',
        'August' => 'August',
        'September' => 'September',
        'October' => 'October',
        'November' => 'November',
        'December' => 'December'
    ];

    $days = [];

    for ($i = 1; $i < 32; $i++) {
        $days[$i] = $i;
    }

    $years = [];

    for ($x = date('Y'); $x > 1899; $x--) {
        $years[$x] = $x;
    }

    $bday = explode('/', $birthday);
    $month = $bday[0];
    $day = $bday[1];
    $year = $bday[2];
?>

<div class="employers-dashboard">
    <div class="left">
        <?= $this->element('Nav/dashboard-employer') ?>
    </div>
    <div class="right">
        <div class="contents">
            <?php if ($this->request->session()->read('Auth.User')['fb_user'] == 0 || ($this->request->session()->read('Auth.User')['fb_user'] == 1 && $this->request->session()->read('Auth.User')['password'] !== '')): ?>
                <?= $this->element('Nav/account_nav') ?>
            <?php else: ?>
                <div style="margin-bottom: 20px">&nbsp;</div>
            <?php endif; ?>
            <div class="account-main">
                <?= $this->Flash->render() ?>
                <div class="form">
                    <?= $this->Form->create($user2); ?>
                        <?= $this->Form->control('firstname', ['label' => 'Firstame:' , 'maxlength' => 16, 'autocomplete' => 'off', 'value' => $firstname]) ?>
                        <?= $this->Form->control('lastname', ['label' => 'Lastname:', 'maxlength' => 255, 'autocomplete' => 'off', 'value' => $lastname]) ?>
                        <div class="input select">
                            <?= $this->Form->label('birthday', 'Date of Birth:') ?>
                            <?= $this->Form->select('month', $months, ['id' => 'month', 'default' => $month]); ?>
                            <?= $this->Form->select('day', $days, ['id' => 'day', 'default' => $day]) ?>
                            <?= $this->Form->select('years', $years, ['id' => 'year', 'default' => $year]) ?>
                        </div>
                        <?= $this->Form->control('address', ['label' => 'Address:', 'type' => 'textarea', 'value' => $address]) ?>
                        <?= $this->Form->control('region', ['label' => 'State/Region:', 'maxlength' => 255, 'autocomplete' => 'off', 'value' => $region]) ?>
                        <?= $this->Form->control('country', ['label' => 'Country:', 'type' => 'select', 'options' => $countries, 'default' => $country]) ?>
                        <?= $this->Form->control('contact_number', ['label' => 'Contact Number:', 'maxlength' => 255, 'autocomplete' => 'off', 'value' => $contact_number]) ?>
                        <div class="btn-container">
                            <?= $this->Form->submit('Save') ?>
                        </div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>
<?= $this->Html->script('employersdashboard') ?>
<script>
    $("#firstname").on("keyup input", function() {
        $(this).attr("style", "border-color: #ccc; background-color: #fff");
        $(".employers-dashboard .account-main .error:nth-child(2) .error-message").hide();
    }).on("click focus", function() {
        inputAnimate("firstname");
    }).on("focusout", function() {
        focusoutAnimate("firstname");
    });

    $("#lastname").on("keyup input", function() {
        $(this).attr("style", "border-color: #ccc; background-color: #fff");
        $(".employers-dashboard .account-main .error:nth-child(3) .error-message").hide();
    }).on("click focus", function() {
        inputAnimate("lastname");
    }).on("focusout", function() {
        focusoutAnimate("lastname");
    });

    $("#address").on("keyup input", function() {
        $(this).attr("style", "border-color: #ccc; background-color: #fff");
        $(".employers-dashboard .account-main .error:nth-child(5) .error-message").hide();
    }).on("click focus", function() {
        $(this).animate({
            height: "100px"
        }, 500, function() {
            $(this).css({
                "border": "2px solid #ccc"
            });
        });
    }).focusout(function() {
        $(this).animate({
            height: "20px"
        }, 500, function() {
            $(this).css({
                "border": 0,
                "border-bottom": "2px solid #ccc"
            });
        });
    });

    $("#region").on("keyup input", function() {
        $(this).attr("style", "border-color: #ccc; background-color: #fff");
        $(".employers-dashboard .account-main .error:nth-child(6) .error-message").hide();
    }).on("click focus", function() {
        inputAnimate("region");
    }).on("focusout", function() {
        focusoutAnimate("region");
    });

    $("#contact-number").on("keyup input", function() {
        $(this).attr("style", "border-color: #ccc; background-color: #fff");
        $(".employers-dashboard .account-main .error:nth-child(8) .error-message").hide();
    }).on("click focus", function() {
        inputAnimate("contact-number");
    }).on("focusout", function() {
        focusoutAnimate("contact-number");
    });

    function inputAnimate(field) {
        $("#" + field).animate({
            padding: "10px 5px"
        }, 500, function() {
            $("#" + field).css({
                "font-size": "18px"
            });
        });
    }

    function focusoutAnimate(field) {
        $("#" + field).animate({
            padding: "0px"
        }, 500, function() {
            $("#" + field).css({
                "font-size": "15px"
            });
        });
    }
</script>