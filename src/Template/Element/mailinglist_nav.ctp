<?php use App\Controller\GlobalworksController; $globalworks = new GlobalworksController(); ?>

<div class="jobposts-nav mailinglist-nav">
    <ul>
        <li><a <?= ($globalworks->uri(3) == '') ? 'class="jobposts-nav-active"' : ''; ?> href="/admin/mailinglist">Maling List</a></li>
        <li><a <?= ($globalworks->uri(3) == 'create') ? 'class="jobposts-nav-active"' : ''; ?> href="/admin/mailinglist/create">Write Mail</a></li>
        <li><a <?= ($globalworks->uri(3) == 'subscribers') ? 'class="jobposts-nav-active"' : ''; ?> href="/admin/mailinglist/subscribers">Subscribers</a></li>
    </ul>
</div>