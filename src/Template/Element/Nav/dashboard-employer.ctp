<?php use App\Controller\GlobalworksController; $globalworks = new GlobalworksController(); ?>

<div class="nav">
    <ul>
        <li><a <?= ($globalworks->uri(2) == 'dashboard') ? 'class="active"' : ''; ?> href="/employers/dashboard"><span class="employers-icon employers-icon-dashboard"></span>Dashboard</a></li>
        <li><a <?= ($globalworks->uri(2) == 'jobpost') ? 'class="active"' : ''; ?> href="/employers/jobpost"><span class="employers-icon employers-icon-job-post"></span>Post Job</a></li>
        <li><a <?= ($globalworks->uri(2) == 'joblist') ? 'class="active"' : ''; ?> href="/employers/joblist"><span class="employers-icon employers-icon-job-post"></span>Your Job List</a></li>
        <li><a <?= ($globalworks->uri(2) == 'messages') ? 'class="active' : ''; ?> href="/employers/messages"><span class="employers-icon employers-icon-messages"></span>Messages <?= ($globalworks->display_unread_msgs_count()) ? '<span class="dashboard-unread-notification">'. $globalworks->display_unread_msgs_count() .'</span>' : ''; ?></a></li>
        <li><a <?= ($globalworks->uri(2) == 'search') ? 'class="active"' : ''; ?> href="/employers/search"><span class="employers-icon employers-icon-search"></span>Search</a></li>
        <li><a <?= ($globalworks->uri(2) == 'profile') ? 'class="active' : ''; ?> href="/employers/profile"><span class="employers-icon employers-icon-profile"></span>Your Profile</a></li>
        <li><a <?= $globalworks->uri(2) == 'account' ? 'class="active"' : ''; ?> href="/employers/account"><span class="employers-icon employers-icon-account"></span>Your Account</a></li>
    </ul>
</div>