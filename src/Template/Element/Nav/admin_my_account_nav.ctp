<?php use App\Controller\GlobalworksController; $globalworks = new GlobalworksController(); ?>

<div class="admin-users-nav">
    <ul>
        <li><a <?= ($globalworks->uri(3) == '' || $globalworks->uri(3) == 'edit') ? 'class="users-nav-selected"' : ''; ?> href="/admin/myaccount">Account Details</a></li>
        <li><a <?= ($globalworks->uri(3) == 'profiles') ? 'class="users-nav-selected"' : ''; ?> href="/admin/myaccount/profiles">My Profiles</a></li>
        <li><a <?= ($globalworks->uri(3) == 'changepassword') ? 'class="users-nav-selected"' : ''; ?> href="/admin/myaccount/changepassword">Change Password</a></li>
    </ul>
</div>