<header class="main-header">
    <div class="left">
        <a href="/"><img alt="logo" src="/img/logo-01.png" /></a>
    </div>
    <div class="right">
        Welcome <?= $username ?>
        <span class="thumbpic"></span>
        <span class="dropdown">▼</span>
    </div>
    <div class="clear"></div>
</header>
<div class="dropdown-menu">
    <ul>
        <li><a href="/admin/dashboard">Dashboard</a></li>
        <li><a href="/admin/myaccount">My Account</a></li>
        <li><a href="/logout">Logout</a></li>
    </ul>
</div>