<?php use App\Controller\GlobalworksController; $globalworks = new GlobalworksController(); ?>

<div class="admin-users-nav">
    <ul>
        <li><a <?= ($globalworks->uri(3) == 'candidates') ? 'class="users-nav-selected"' : ''; ?> href="/admin/users/candidates">Candidates</a></li>
        <li><a <?= ($globalworks->uri(3) == 'employers') ? 'class="users-nav-selected"' : ''; ?> href="/admin/users/employers">Employers</a></li>
        <?php if ($this->request->session()->read('Auth.User')['role'] == 'Super Admin'): ?>
            <li><a <?= ($globalworks->uri(3) == 'admins') ? 'class="users-nav-selected"' : ''; ?> href="/admin/users/admins">Admins</a></li>
        <?php endif; ?>
    </ul>
</div>