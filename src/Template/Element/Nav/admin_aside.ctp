<?php use App\Controller\GlobalworksController; $globalworks = new GlobalworksController(); ?>

<aside class="main-aside">
    <ul>
        <li><a <?= ($globalworks->uri(2) == 'dashboard') ? 'class="active"' : ''; ?> href="/admin/dashboard">Dashboard</a></li>
        <li><a <?= ($globalworks->uri(2) == 'jobposts') ? 'class="active"' : ''; ?> href="/admin/jobposts">Manage Job Posts</a></li>
        <li><a <?= ($globalworks->uri(2) == 'users') ? 'class="active"' : ''; ?> href="/admin/users">Manage Users</a></li>
        <li><a <?= ($globalworks->uri(2) == 'announcements') ? 'class="active"' : ''; ?> href="/admin/announcements">Announcements</a></li>
        <li><a <?= ($globalworks->uri(2) == 'logs') ? 'class="active"' : ''; ?> href="/admin/logs">Logs</a></li>
        <li><a <?= ($globalworks->uri(2) == 'visitors_statistics') ? 'class="active"' : ''; ?> href="/admin/visitors_statistics">Visitor's Statistics</a></li>
        <li><a <?= ($globalworks->uri(2) == 'mailinglist') ? 'class="active"' : ''; ?> href="/admin/mailinglist">Mailing List</a></li>
        <li><a <?= ($globalworks->uri(2) == 'settings') ? 'class="active"' : ''; ?> href="/admin/settings">Settings</a></li>
    </ul>
</aside>