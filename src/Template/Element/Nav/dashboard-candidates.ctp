<?php use App\Controller\GlobalworksController; $globalworks = new GlobalworksController(); ?>
<nav>
    <ul>
        <li><a <?= ($globalworks->uri(2) == 'dashboard') ? 'class="active"' : ''; ?> href="/candidates/dashboard"><span class="employers-icon employers-icon-dashboard"></span>Dashboard</a></li>
        <li><a <?= ($globalworks->uri(2) == 'search') ? 'class="active"' : ''; ?> href="/candidates/search"><span class="employers-icon employers-icon-search"></span>Search Jobs</a></li>
        <li><a <?= ($globalworks->uri(2) == 'messages') ? 'class="active"' : ''; ?> href="/candidates/messages"><span class="employers-icon employers-icon-messages"></span>Messages <?= ($globalworks->display_unread_msgs_count()) ? '<span class="dashboard-unread-notification">'. $globalworks->display_unread_msgs_count() .'</span>' : ''; ?></a></li>
        <li><a <?= ($globalworks->uri(2) == 'profile') ? 'class="active"' : ''; ?> href="/candidates/profile"><span class="employers-icon employers-icon-profile"></span>My Profile</a></li>
        <li><a <?= ($globalworks->uri(2) == 'account') ? 'class="active"' : ''; ?> href="/candidates/account"><span class="employers-icon employers-icon-account"></span>My Account</a></li>
    </ul>
</nav>