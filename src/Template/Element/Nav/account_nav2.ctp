<?php use App\Controller\GlobalworksController; $globalworks = new GlobalworksController(); ?>

<div class="candidates-account-nav">
    <ul>
        <li><a <?= ($globalworks->uri(3) == '') ? 'class="active"' : '' ?> href="/candidates/account">Your Information</a></li>
        <li><a <?= ($globalworks->uri(3) == 'changepassword') ? 'class="active"' : '' ?> href="/candidates/account/changepassword">Change Password</a></li>
    </ul>
</div>
<div class="m-candidates-account-nav">
    <select id="page">
        <option value="/candidates/account"  <?= ($globalworks->uri(3) == '') ? 'selected' : '' ?>>Your Information</option>
        <option value="/candidates/account/changepassword" <?= ($globalworks->uri(3) == 'changepassword') ? 'selected' : '' ?>>Change Password</option>
    </select>
</div>

<script>
    $("#page").on("change", function() {
        location = $(this).val();
    });
</script>