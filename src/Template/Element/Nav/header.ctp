<?php use App\Controller\GlobalworksController; $globalworks = new GlobalworksController(); ?>

<?php if (!$this->request->session()->read('Auth.User')): ?>
    <header class="main-header">
        <div class="container"></div>
        <nav class="main-nav">
            <div class="left">
                <a href="/"><img alt="logo" src="/img/logo-01.png" /></a>
            </div>
            <div class="right">
                <ul>
                    <li><a href="/login">Jobseekers</a></li>
                    <li><a href="/login/employers">Employers</a></li>
                </ul>
            </div>
            <div class="clear"></div>
        </nav>
    </header>
<?php else: ?>
    <header class="main-header">
        <nav class="main-nav">
            <div class="left">
                <a href="/"><img alt="logo" src="/img/logo-01.png" /></a>
            </div>
            <div class="right user-menu">
                Welcome <?= $username ?>
                <span class="thumbpic"></span>
                <span class="dropdown">▼</span>
                <?= ($globalworks->display_unread_msgs_count()) ? '<span class="top-unread-notification">'.$globalworks->display_unread_msgs_count().'</span>' : ''; ?>
            </div>
        </nav>
    </header>
    <div class="dropdown-menu">
        <ul>
            <?php if ($this->request->session()->read('Auth.User')['role'] == 'Applicant'): ?>
                <li><a href="/candidates/dashboard">Dashboard</a></li>
                <li><a href="/candidates/messages">Messages <?= ($globalworks->display_unread_msgs_count()) ? '<span class="nav-unread-notification">'.$globalworks->display_unread_msgs_count().'</span>' : ''; ?></a></li>
            <?php elseif ($this->request->session()->read('Auth.User')['role'] == 'Employer'): ?>
                <li><a href="/employers/dashboard">Dashboard</a></li>
                <li><a href="/employers/messages">Messages <?= ($globalworks->display_unread_msgs_count()) ? '<span class="nav-unread-notification">'.$globalworks->display_unread_msgs_count().'</span>' : ''; ?></a></li>
            <?php elseif ($this->request->session()->read('Auth.User')['role'] == 'Admin' || $this->request->session()->read('Auth.User')['role'] == 'Super Admin'): ?>
                <li><a href="/admin/dashboard">Dashboard</a></li>
                <li><a href="#">My Account</a></li>
            <?php endif ?>
            <li><a href="/logout">Logout</a></li>
        </ul>
    </div>
<?php endif; ?>

<!-- Responsive Nav 800px and lower -->

<?php if (!$this->request->session()->read('Auth.User')): ?>
    <header class="m-main-header">
        <div class="left">
            <a href="/"><img alt="logo" src="/img/logo-01.png" /></a>
        </div>
        <div class="right">
            <ul>
                <li><a href="/login">Jobseekers</a></li>
                <li><a href="/login/employers">Employers</a></li>
            </ul>
        </div>
        <div class="clear"></div>
    </header>

<?php else: ?>
    <header class="m-main-header">
        <nav class="main-nav">
            <div class="left">
                <a href="/"><img alt="logo" src="/img/logo-01.png" /></a>
            </div>
            <div class="right user-menu">
                <span class="welcome">Welcome <?= $username ?></span>
                <span class="thumbpic"></span>
                <span class="m-dropdown">▼</span>
                <?= ($globalworks->display_unread_msgs_count()) ? '<span class="top-unread-notification">'.$globalworks->display_unread_msgs_count().'</span>' : ''; ?>
            </div>
        </nav>
    </header>
    <div class="m-dropdown-menu">
        <ul>
            <?php if ($this->request->session()->read('Auth.User')['role'] == 'Applicant'): ?>
                <li><a href="/candidates/dashboard"><span class="m-employers-icon m-employers-icon-dashboard"></span>Dashboard</a></li>
                <li><a href="/candidates/messages"><span class="m-employers-icon m-employers-icon-messages"></span>Messages <?= ($globalworks->display_unread_msgs_count()) ? '<span class="nav-unread-notification">'.$globalworks->display_unread_msgs_count().'</span>' : ''; ?></a></li>
                <li><a href="/candidates/search"><span class="m-employers-icon m-employers-icon-search"></span>Search Jobs</a></li>
                <li><a href="/candidates/profile"><span class="m-employers-icon m-employers-icon-profile"></span>My Profile</a></li>
                <li><a href="/candidates/account"><span class="m-employers-icon m-employers-icon-account"></span>Your Account</a></li>
            <?php elseif ($this->request->session()->read('Auth.User')['role'] == 'Employer'): ?>
                <li><a href="/employers/dashboard"><span class="m-employers-icon m-employers-icon-dashboard"></span>Dashboard</a></li>
                <li><a href="/employers/messages"><span class="m-employers-icon m-employers-icon-messages"></span>Messages <?= ($globalworks->display_unread_msgs_count()) ? '<span class="nav-unread-notification">'.$globalworks->display_unread_msgs_count().'</span>' : ''; ?></a></li>
                <li><a href="/employers/jobpost"><span class="m-employers-icon m-employers-icon-job-post"></span>Post Job</a></li>    
                <li><a href="/employers/joblist"><span class="m-employers-icon m-employers-icon-job-list"></span>Your Job List</a></li>    
                <li><a href="/employers/search"><span class="m-employers-icon m-employers-icon-search"></span>Search</a></li>
                <li><a href="/employers/profile"><span class="m-employers-icon m-employers-icon-profile"></span>Your Profile</a></li>
                <li><a href="/employers/account"><span class="m-employers-icon m-employers-icon-account"></span>Your Account</a></li>
            <?php elseif ($this->request->session()->read('Auth.User')['role'] == 'Admin' || $this->request->session()->read('Auth.User')['role'] == 'Super Admin'): ?>
                <li><a href="/admin/dashboard">Dashboard</a></li>
                <li><a href="#">My Account</a></li>
            <?php endif ?>
            <li><a href="/logout"><span class="m-logout-icon"></span>Logout</a></li>
        </ul>
    </div>
<?php endif; ?>