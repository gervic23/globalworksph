<?php use App\Controller\GlobalworksController; $globalworks = new GlobalworksController(); ?>

<div class="employers-account-nav">
    <ul>
        <li><a <?= ($globalworks->uri(3) == '') ? 'class="active"' : '' ?> href="/employers/account">Your Information</a></li>
        <li><a <?= ($globalworks->uri(3) == 'changepassword') ? 'class="active"' : '' ?> href="/employers/account/changepassword">Change Password</a></li>
    </ul>
</div>

<div class="m-employers-account-nav">
    <select id="page">
        <option value="/employers/account" <?= ($globalworks->uri(3) == '') ? 'selected' : '' ?>>Your Information</option>
        <option value="/employers/account/changepassword" <?= ($globalworks->uri(3) == 'changepassword') ? 'selected' : '' ?>>Change Password</option>
    </select>
</div>

<script>
    $("#page").on("change", function() {
        location = $(this).val();
    });
</script>