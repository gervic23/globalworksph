<?php use App\Controller\GlobalworksController; $globalworks = new GlobalworksController(); ?>

<div class="jobposts-nav">
    <ul>
        <li><a <?= ($globalworks->uri(3) == '' || $globalworks->uri(3) == 'search') ? 'class="jobposts-nav-active"' : ''; ?> href="/admin/jobposts">Active Job Posts</a></li>
        <li><a <?= ($globalworks->uri(3) == 'inactive') ? 'class="jobposts-nav-active"' : ''; ?> href="/admin/jobposts/inactive">Inactive Job Posts</a></li>
    </ul>
</div>