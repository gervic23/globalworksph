<?php use App\Controller\GlobalworksController; $globalworks = new GlobalworksController(); ?>
<div class="nav">
    <ul>
        <li><a <?= ($globalworks->uri(3) == '') ? 'class="active"' : ''; ?> href="/employers/joblist">Active Job Posts</a></li>
        <li><a <?= ($globalworks->uri(3) == 'inactive') ? 'class="active"' : ''; ?> href="/employers/joblist/inactive">Inactive Job Posts</a></li>
    </ul>
</div>

<div class="m-nav">
    <select id="page">
        <option value="/employers/joblist" <?= ($globalworks->uri(3) == '') ? 'selected' : ''; ?>>Active Job Post</option>
        <option value="/employers/joblist/inactive" <?= ($globalworks->uri(3) == 'inactive') ? 'selected' : ''; ?>>Inactive Job Posts</option>
    </select>
</div>

<script>
    $("#page").on("change", function() {
        location = $(this).val();
    });
</script>