<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="message flash-success" onclick="this.classList.add('hidden')">
    <img alt="success" src="/img/success.png" /> <?= $message ?>
</div>
