<?php
    $this->Html->script('jquery.dragdropupload.1.0', ['block' => true]);
?>
<div class="setprofile">
    <div class="progress">
        <span class="account-registration complete1">Account Registration</span>
        <span class="profile-setup complete2">Create Your Profile</span>
        <span class="upload-profile-pic <?= ($role == 'Employer') ? 'end-nav' : ''; ?>">Upload Your Picture</span>
        <?= ($role == 'Applicant') ? '<span class="upload-resume">Upload Your Resume</span>' : ''; ?>
        <div class="clear"></div>
    </div>
    <div class="content">
        <h1>Upload Your Picture</h1>
        <div class="profile-upload-form">
            <div class="profile-picture-upload">
                <div class="img-upload"></div>
                <ul>
                    <li>Maximum of 1 MB size.</li>
                    <li>ONLY JPEG and PNG image formats are allowed</li>
                    <li>Image dimension should be 370x370 "PIXELS" or higher.</li>
                </ul>
                <div class="btn-container">
                    <?= $this->Form->button('Skip', ['type' => 'button', 'id' => 'skip']) ?>
                </div>
            </div>
            <div>&nbsp;</div>
        </div>
    </div>
</div>
<script>
    var label;
    var label_font_size;

    uploadAttr();

    $(window).resize(function() {
        uploadAttr();
    });

    $(".img-upload").dragdropupload('/ajax', {
        label: label,
        width: "100%",
        height: "300px",
        border_size: "3px",
        background_color: "#e3f8fd",
        label_font_size: label_font_size,
        error_font_size: "100%",
        post_data: {
            operation: "image_upload",
            _csrfToken: "<?= $this->request->getParam('_csrfToken'); ?>",
        },
        validation: {
            file_types: {
                types: ["image/jpeg", "image/png"],
                validation_message: "is not an image file. Only jpeg and png formats are allowed."
            },
            file_sizes: {
                max: {
                    size: 1024,
                    validation_message: "size to large."
                }
            },
            image_dimension: {
                min: {
                    width: 370,
					height: 370,
					validation_message: "dimension should be 370x370 or higher."
                }
            }
        }
    }, function(r) {
        if (r) {
            location = "/imagecrop/new_user"
        }
    });
    
    $("#skip").on("click", function() {
        <?= ($role == 'Applicant') ? 'location = "/uploadresume";' : 'location = "/employers/dashboard/"'; ?>
    });

    function uploadAttr() {
        if ($(window).width() > 650) {
            label = "Drag Image file here or click here to browse image.";
            label_font_size = "130%";
        } else {
            label = "Click Here to Upload.";
            label_font_size = "100%";            
        }
    }
</script>
<?= $this->Html->script('register') ?>