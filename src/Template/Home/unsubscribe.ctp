<div class="unsubscribe">
    <div class="contents">
        <?= $message; ?>
    </div>
</div>

<?php if ($success): ?>
    <script>
        setTimeout(function() {
            location = '/';
        }, 3000);
    </script>
<?php endif; ?>