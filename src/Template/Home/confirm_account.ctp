<?php
    use App\Controller\GlobalworksController;

    $globalworks = new GlobalworksController();
?>

<div class="account-confirm">
    <?php if ($globalworks->uri(2) == 'new'): ?>
        <?= $this->Flash->render() ?>
    <?php else: ?>
        <div>&nbsp;</div>
    <?php endif; ?>
    <div class="flash">
            <?= $this->Flash->render() ?>
        </div>
    <div class="form">
        <?= $this->Form->create($confirm) ?>
        <?= $this->Form->hidden('option', ['value' => 'confirm']) ?>
        <?= $this->Form->control('confirmation_code', ['label' => 'Enter Confirmation Code Here', 'autocomplete' => 'off']) ?>
        <div class="btn-container">
            <?= $this->Form->submit('Confirm Now') ?>
        </div>
        <?= $this->Form->end() ?>

        <div class="resend">
            <?= $this->Form->create() ?>
            <?= $this->Form->hidden('option', ['value' => 'resend']) ?>
            <?= $this->Form->submit('Resend Confirmation Code', ['class' => 'btn-resend']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
<script>

    account_confirm();

    $(window).resize(function() {
        account_confirm();
    });

    <?php if ($resend): ?>
        $(".btn-resend").attr("disabled", "disabled");

        setTimeout(function() {
            $(".flash-success").fadeOut(500);
        }, 5000);

        setTimeout(function() {
            $(".btn-resend").removeAttr("disabled");
        }, 60000);
    <?php endif; ?>

    function account_confirm() {
        var height = $(window).height();
        var new_height = height - 50;
        var account_confirm = $(".account-confirm").height();

        if (account_confirm < height) {
            $(".account-confirm").css({
                "height": new_height + "px"
            });
        }
    }
</script>