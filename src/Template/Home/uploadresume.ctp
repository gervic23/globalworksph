<?php
    $this->Html->script('jquery.dragdropupload.1.0', ['block' => true]);
?>

<div class="setprofile">
    <div class="progress">
        <span class="account-registration complete1">Account Registration</span>
        <span class="profile-setup complete2">Create Your Profile</span>
        <span class="upload-profile-pic complete3">Upload Your Picture</span>
        <span class="upload-resume">Upload Your Resume</span>
        <div class="clear"></div>
    </div>
    <div class="content">
        <h1>Upload Your Resume</h1>
        <div class="form resume-upload">
            <div class="resume-upload-container"></div>
            <ul>
                <li>File size maximun of 500 KB.</li>
                <li>Only word documents .doc, .docx are allowed.</li>
            </ul>
            <div class="btn-container">
                <?= $this->Form->button('Skip', ['type' => 'button', 'id' => 'skip']) ?>
            </div>
        </div>
    </div>
</div>
<div>&nbsp;</div>

<script>
    var label;
    var label_font_size;

    resumeAttr();

    $(window).on("resize", function() {
        resumeAttr();
    });

    $(".resume-upload-container").dragdropupload('/ajax', {
        width: "100%",
        height: "200px",
        label: label,
        border_size: "3px",
        background_color: "#e1fcfc",
        label_font_size: label_font_size,
        error_font_size: "100%",
        post_data: {
            operation: "resume_upload",
            _csrfToken: "<?= $this->request->getParam('_csrfToken'); ?>"
        },
        validation: {
            file_types: {
                types: ["application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/msword"],
                validation_message: "is not a word document."
            },
            file_sizes: {
                max: 512,
                validation_message: "is too large. Only 500kb and below are allowed."
            }
        }
    }, function(r) {
        if (r == "ok") {
            location = "/candidates/dashboard"
        }
    });
    
    $("#skip").on("click", function() {
        location = "/candidates/dashboard";
    });

    function resumeAttr() {
        if ($(window).width() > 800) {
            label = "Drag Your Resume File Here or Click Here to Browse File.";
            label_font_size = "130%";
        } else {
            label = "Click Here to Upload.";
            label_font_size = "100%";
        }
    }
</script>
<?= $this->Html->script('register') ?>