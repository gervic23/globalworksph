<?php
    //Include shogotags plugin.
    $this->Html->script('jquery.shogotags-1.0', ['block' => true]);

    if (!isset($nickname)) {
        $nickname = '';
    }

    if (!isset($job_title)) {
        $job_title = '';
    }

    if (!isset($experience)) {
        $experience = '';
    }

    if (!isset($employment_status)) {
        $employment_status = '';
    }

    if (!isset($skills)) {
        $skills = '';
    }

    if (!isset($desire_salary)) {
        $desire_salary = '';
    }

    if (!isset($education)) {
        $education = '';
    }

    if (!isset($skype_id)) {
        $skype_id = '';
    }

    if (!isset($job_subscribe)) {
        $job_subscribe = 0;
    }
?>

<div class="setprofile">
    <div class="progress">
        <span class="account-registration complete1">Account Registration</span>
        <span class="profile-setup">Create Your Profile</span>
        <span class="upload-profile-pic">Upload Your Picture</span>
        <span class="upload-resume">Upload Your Resume</span>
        <div class="clear"></div>
    </div>
    <div class="content">
        <h2>Create Your Profile</h2>
        <div class="flash-error">
            
        </div>
        <div class="form">
            <?= $this->Form->input('nickname', ['autocomplete' => 'off', 'label' => 'Nickname:', 'id' => 'nickname', 'maxlength' => 15]); ?>
            <?= $this->Form->input('job_title', ['autocomplete' => 'off', 'label' => 'Job Title:', 'id' => 'job_title']) ?>
            <?= $this->Form->hidden('experience', ['id' => 'myField', 'value' => $experience]); ?>
            <div class="exp-container">
                <label for="experience">Experience: <span style="font-size: 13px">Eg. (4 Years in Graphic Arts,) Each experience seperated by pressing coma.</span></label>
                <div id="experience"></div>
            </div>
            <?= $this->Form->input('skype_id', ['type' => 'text', 'auto_complete' => 'off', 'label' => 'Skype ID:', 'id' => 'skype_id']) ?>
            <label for="employment_status">Employment Status:</label>
            <?= $this->Form->select('employment_status', ['' => 'Select Status', 'Unemployed' => 'Unemployed', 'Employed' => 'Employed'], ['id' => 'employment_status']); ?>
            <div class="skills">
                <h3>Skills</h3>
                <div class="skills-contents">
                    <div class="skills_contents"></div>
                    <div class="btn-container">
                        <span id="add-skill">Add Skills</span>
                    </div>
                </div>
            </div>
            <?= $this->Form->input('desire_salary', ['type' => 'hidden', 'id' => 'desire_salary', 'value' => $desire_salary]) ?>
            
            <div class="salary">
                <h3>Desired Salary</h3>
                <div class="container">
                    <ul>
                        <li>
                            <?= $this->Form->checkbox('salary_range0', ['id' => 'salary_range0', 'value' => 'Php 8000 - Php 10000']); ?>
                            <label for="salary_range0">Php 8,000 - Php 10,000</label>
                        </li>

                        <li>
                            <?= $this->Form->checkbox('salary_range1', ['id' => 'salary_range1', 'value' => 'Php 10000 - Php 15000']); ?>
                            <label for="salary_range1">Php 10,000 - Php 15,000</label>
                        </li>
                    </ul>
                </div>
                <div class="container">
                    <ul>
                        <li>
                            <?= $this->Form->checkbox('salary_range2', ['id' => 'salary_range2', 'value' => 'Php 15000 - Php 20000']); ?>
                            <label for="salary_range2">Php 15,000 - Php 20,000</label>
                        </li>
                        <li>
                            <?= $this->Form->checkbox('salary_range3', ['id' => 'salary_range3', 'value' => 'Php 20000 - Php 25000']); ?>
                            <label for="salary_range3">Php 20,000 - Php 25,000</label>
                        </li>
                    </ul>
                </div>
                <div class="container">
                    <ul>
                        <li>
                            <?= $this->Form->checkbox('salary_range4', ['id' => 'salary_range4', 'value' => 'Php 25000 - Php 30000']); ?>
                            <label for="salary_range4">Php 25,000 - Php 30,000</label>
                        </li>
                        <li>
                            <?= $this->Form->checkbox('salary_range5', ['id' => 'salary_range5', 'value' => 'Php 30000 - Php 35000']); ?>
                            <label for="salary_range5">Php 30,000 - Php 35,000</label>
                        </li>
                    </ul>
                </div>
                <div class="container">
                    <ul>
                        <li>
                            <?= $this->Form->checkbox('salary_range6', ['id' => 'salary_range6', 'value' => 'Php 35000 - Php 40000']); ?>
                            <label for="salary_range6">Php 35,000 - Php 40,000</label>
                        </li>
                        <li>
                            <?= $this->Form->checkbox('salary_range7', ['id' => 'salary_range7', 'value' => 'Php 40000 - Php 45000']); ?>
                            <label for="salary_range7">Php 40,000 - Php 45,000</label>
                        </li>
                    </ul>
                </div>
                <div class="container">
                    <ul>
                        <li>
                            <?= $this->Form->checkbox('salary_range8', ['id' => 'salary_range8', 'value' => 'Php 45000 - Php 50000']); ?>
                            <label for="salary_range8">Php 45,000 - Php 50,000</label>
                        </li>
                        <li>
                            <?= $this->Form->checkbox('salary_range9', ['id' => 'salary_range9', 'value' => 'Morethan Php 50000']); ?>
                            <label for="salary_range9">Morethan Php 50,000</label>
                        </li>
                    </ul>
                </div>
            </div>
            
            <div class="education">
                <h3>Education Attainment</h3>
                <div class="content">
                    <?= $this->Form->hidden('edu', ['id' => 'education']) ?>
                    <ul>
                        <li>
                            <input type="radio" name="education" id="education-1" value="College/Bachelor Degree" <?= ($education == 'College/Bachelor Degree') ? 'checked' : ''; ?> />
                            <label for="education-1">College/Bachelor Degree</label>
                            <div class="check"></div>
                        </li>
                        <li>
                            <input type="radio" name="education" id="education-2" value="College/Associated Degree" <?= ($education == 'College/Associated Degree') ? 'checked' : '' ?> />
                            <label for="education-2">College/Associated Degree</label>
                            <div class="check"></div>
                        </li>
                        <li>
                            <input type="radio" name="education" id="education-3" value="Post Graduate Degree" <?= ($education == 'Post Graduate Degree') ? 'checked' : ''; ?> />
                            <label for="education-3">Post Graduate Degree</label>
                            <div class="check"></div>
                        </li>
                        <li>
                            <input type="radio" name="education" id="education-4" value="Highschool Graduate" <?= ($education == 'Highschool Graduate') ? 'checked' : ''; ?> />
                            <label for="education-4">Highschool Graduate</label>
                            <div class="check"></div>
                        </li>
                        <li>
                            <input type="radio" name="education" id="education-5" value="Not Graduated Highschool" <?= ($education == 'Not Graduated Highschool') ? 'checked' : ''; ?> />
                            <label for="education-5">Not Graduated Highschool</label>
                            <div class="check"></div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="subscribe">
                <?= $this->Form->hidden('job_subscribe', ['id' => 'job_subscribe']); ?>
                <input type="checkbox" name="j_subscribe" id="j_subscribe" />
                <label for="j_subscribe">Subscribe for latest jobs.</label>
                <div class="check"></div>
            </div>
            <div class="btn-container">
                <?= $this->Form->button('Skip', ['type' => 'button', 'id' => 'skip']) ?>
                <?= $this->Form->button('Save', ['type' => 'button', 'id' => 'next']); ?>
            </div>
        </div>
    </div>
</div>
<?= $this->Html->script('add-skills') ?>
<script>
    $("#experience").shogotags({
        tag_border_color: "#ff4338",
		tag_background_color: "#ff4338",
        tag_border_radius: "5px"
    });

    $("#nickname").on("click focus", function() {
        inputAnimate("nickname");
    }).on("focusout", function() {
        focusoutAnimate("nickname");
    });

    $("#job-title").on("click focus", function() {
        inputAnimate("job-title");
    }).on("focusout", function() {
        focusoutAnimate("job-title");
    });

    var salary_range = [];    
    
    $("#salary_range0").on("change", function() {
        var range = $(this).val();
        if ($(this).is(":checked")) {
            salary_range.push(range);
            addSalaryRange();
        } else {
            removeSalaryRange(salary_range.indexOf(range));
        }
    });
    
    $("#salary_range1").on("change", function() {
        var range = $(this).val();
        if ($(this).is(":checked")) {
            salary_range.push(range);
            addSalaryRange();
        } else {
            removeSalaryRange(salary_range.indexOf(range));
        }
    });
    
    $("#salary_range2").on("change", function() {
        var range = $(this).val();
        if ($(this).is(":checked")) {
            salary_range.push(range);
            addSalaryRange();
        } else {
            removeSalaryRange(salary_range.indexOf(range));
        }
    });
    
    $("#salary_range3").on("change", function() {
        var range = $(this).val();
        if ($(this).is(":checked")) {
            salary_range.push(range);
            addSalaryRange();
        } else {
            removeSalaryRange(salary_range.indexOf(range));
        }
    });
    
    $("#salary_range4").on("change", function() {
        var range = $(this).val();
        if ($(this).is(":checked")) {
            salary_range.push(range);
            addSalaryRange();
        } else {
            removeSalaryRange(salary_range.indexOf(range));
        }
    });
    
    $("#salary_range5").on("change", function() {
        var range = $(this).val();
        if ($(this).is(":checked")) {
            salary_range.push(range);
            addSalaryRange();
        } else {
            removeSalaryRange(salary_range.indexOf(range));
        }
    });
    
    $("#salary_range6").on("change", function() {
        var range = $(this).val();
        if ($(this).is(":checked")) {
            salary_range.push(range);
            addSalaryRange();
        } else {
            removeSalaryRange(salary_range.indexOf(range));
        }
    });
    
    $("#salary_range7").on("change", function() {
        var range = $(this).val();
        if ($(this).is(":checked")) {
            salary_range.push(range);
            addSalaryRange();
        } else {
            removeSalaryRange(salary_range.indexOf(range));
        }
    });
    
    $("#salary_range8").on("change", function() {
        var range = $(this).val();
        if ($(this).is(":checked")) {
            salary_range.push(range);
            addSalaryRange();
        } else {
            removeSalaryRange(salary_range.indexOf(range));
        }
    });
    
    $("#salary_range9").on("change", function() {
        var range = $(this).val();
        if ($(this).is(":checked")) {
            salary_range.push(range);
            addSalaryRange();
        } else {
            removeSalaryRange(salary_range.indexOf(range));
        }
    });



    $("#education-1").on("change", function() {
        if ($(this).is(":checked")) {
            $("#education").attr("value", $(this).val());
        }
    });

    $("#education-2").on("change", function() {
        if ($(this).is(":checked")) {
            $("#education").attr("value", $(this).val());
        }
    });

    $("#education-3").on("change", function() {
        if ($(this).is(":checked")) {
            $("#education").attr("value", $(this).val());
        }
    });

    $("#education-4").on("change", function() {
        if ($(this).is(":checked")) {
            $("#education").attr("value", $(this).val());
        }
    });

    $("#education-5").on("change", function() {
        if ($(this).is(":checked")) {
            $("#education").attr("value", $(this).val());
        }
    });

    
    function addSalaryRange() {
        $("#desire_salary").attr("value", salary_range);
    }
    
    function removeSalaryRange(index) {
        range = salary_range[index];
        salary_range.splice(index, 1);
        addSalaryRange();
    }
    
    if (detectOs.isWindows()) {
        $("head").append('<style> .setprofile .content .form .subscribe .check { font-size: 15px !important; } </style>');
    }
    
    $("#j_subscribe").on("change", function() {
        if ($(this).is(":checked")) {
            $("#job_subscribe").attr("value", 1);
        } else {
            $("#job_subscribe").attr("value", 0);
        }
    });
    
    $("#skip").on("click", function() {
        location = "/uploadpicture/new_user";
    });

    $("#next").on("click", function() {
        if (has_filled()) {
            var skills = '';

            if ($(".skill-entry").length > 0) {
                skills = [];

                $(".skill-entry").each(function() {
                    skills.push($(this).val());
                });
            }

            $(".btn-container").html('<img src="/img/loading2.gif" />');

            var data = {
                "operation": "setprofile",
                "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
                "nickname": $("#nickname").val(),
                "job_title": $("#job_title").val(),
                "s_tags": $(".s_tags").val(),
                "skype_id": $("#skype_id").val(),
                "employment_status": $("#employment_status").val(),
                "skills": skills,
                "education": $("#education").val(),
                "desire_salary": $("#desire_salary").val(),
                "job_subscribe": $("#job_subscribe").val()
            };

            $.post("/ajax", data, function(r) {
                if (r == "ok") {
                    location = "/uploadpicture";
                }
            });
        }
    });

    function has_filled() {
        if ($("#nickname").val() || $("#job_title").val() || $(".s_tags").val() || $("#skype_id").val() || $("#employment_status").val() || $(".skill-entry").length > 0 || $("#education").val() && $("#job_subscribe").val()) {
            return true;
        }
        
        $(".flash-error").html("Please fill in atleast 1 field.");

        $("#nickname").focus();

        return false;
    }
</script>
<?= $this->Html->script('register'); ?>