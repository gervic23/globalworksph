<div class="search-home">
    <div class="search-background"></div>
    <div class="search-content">
        <p>Find The Job That is Right For You</p>
        <form method="get" accept-charset="utf-8" action="/jobs/search">
            <field>
            <?= $this->Form->hidden('searchType', ['value' => 'basic']); ?>
            <?= $this->Form->text('keywords', ['id' => 'search-keywords', 'placeholder' => 'Job Title, Keywords or Company', 'autocomplete' => 'off']) ?>
            <?= $this->Form->select('job_class', ['Homebase/Full Time' => 'Homebase/Full Time', 'Homebase/Part Time' => 'Homebase/Part time', 'Office/Full Time' => 'Office/Full Time', 'Office/Part Time' => 'Office/Part Time', 'All' => 'All']); ?>
            <?= $this->Form->select('location', ['Local' => 'Local', 'Abroad' => 'Abroad', 'All' => 'All']); ?>
                <span class="search-btn"></span>
            </field>
            <div class="keywords-flash-error">&nbsp;</div>
            <div><a href="/jobs/search">Advance Search</a></div>
        </form>
    </div>
    <div class="m-search-content">
        <p>Find The Job That is Right For You</p>
        <form method="get" accept-charset="utf-8" action="/jobs/search">
            <field>
            <?= $this->Form->hidden('searchType', ['value' => 'basic']); ?>
            <?= $this->Form->text('keywords', ['id' => 'm-search-keywords', 'placeholder' => 'Job Title, Keywords or Company', 'autocomplete' => 'off']) ?>
            <?= $this->Form->hidden('job_class', ['value' => 'All']) ?>
            <?= $this->Form->hidden('location', ['value' => 'All']) ?>
                <span class="m-search-btn"></span>
            </field>
            <div class="keywords-flash-error">&nbsp;</div>
            <div><a href="/jobs/search">Advance Search</a></div>
        </form>
    </div>
    <script>
        $("#search-keywords").on("keypress", function(e) {
            var code = e.keyCode || e.which;
            if (code == 13) {
                e.preventDefault();
                return false;
            }
        });

        $("#m-search-keywords").on("keypress", function(e) {
            var code = e.keyCode || e.which;
            if (code == 13) {
                e.preventDefault();
                return false;
            }
        });
        
        $(".search-btn").on("click", function() {
            if ($("#search-keywords").val() == "") {
                $(".keywords-flash-error").html("Please write your keywords.");
            } else if ($("#search-keywords").val().length <= 2) {
                $(".keywords-flash-error").html("Keyword too short.");
            } else {
                $(".search-content form").submit();   
            }
        });

        $(".m-search-btn").on("click", function() {
            if ($("#m-search-keywords").val() == "") {
                $(".keywords-flash-error").html("Please write your keywords.");
            } else if ($("#m-search-keywords").val() .length <= 2) {
                $(".keywords-flash-error").html("Keyword too short.");
            } else {
                $(".m-search-content form").submit();   
            }
        });
        
        $("#search-keywords").on("keyup", function() {
            $(".keywords-flash-error").html("&nbsp;");
        });
    </script>
</div>
<h1 class="home-h1">Welcome and Explore Current Job Posts</h1>
<div class="home-jobs-categories">
    <ul>
        <li>
            <div class="image">
                <a href="/jobs/category/Accounting-Finance"><img src="/img/cat/accounting-01-01.png" /></a>
            </div>
            <div>Accounting Finance</div>
        </li>
        <li>
            <div class="image">
                <a href="/jobs/category/Arts_Media"><img src="/img/cat/arts-01-01.png" /></a>
            </div>
            <div>Arts / Media</div>
        </li>
        <li>
            <div class="image">
                <a href="/jobs/category/Computer_I.T"><img src="/img/cat/computer-01-01.png" /></a>
            </div>
            <div>Computer / I.T</div>
            <div></div>
        </li>
        <li>
            <div class="image">
                <a href="/jobs/category/Engineering"><img src="/img/cat/eng-01-01.png" /></a>
            </div>
            <div>Engineering</div>
        </li>
        <li>
            <div class="image">
                <a href="/jobs/category/Admin_Office"><img src="/img/cat/admin-01-01.png" /></a>
            </div>
            <div>Admin / Office</div>
        </li>
        <li>
            <div class="image">
                <a href="/jobs/category/Building_Construction"><img src="/img/cat/bldg-01-01.png" /></a>
            </div>
            <div>Building / Construction</div>
        </li>
        <li>
            <div class="image">
                <a href="/jobs/category/Education_Training"><img src="/img/cat/educ-01-01.png" /></a>
            </div>
            <div>Education / Training</div>
        </li>
        <li>
            <div class="image">
                <a href="/jobs/category/Health-Care"><img src="/img/cat/health-01-01.png" /></a>
            </div>
            <div>Health Care</div>
        </li>
        <li>
            <div class="image">
                <a href="/jobs/category/Hotel_Restaurant"><img src="/img/cat/hotel-01-01.png" /></a>
            </div>
            <div>Hotel / Restaurant</div>
        </li>
        <li>
            <div class="image">
                <a href="/jobs/category/Manufacturing"><img src="/img/cat/manu-01-01.png" /></a>
            </div>
            <div>Manufacturing</div>
        </li>
        <li>
            <div class="image">
                <a href="/jobs/category/Sales-Marketing"><img src="/img/cat/sales-01-01.png" /></a>
            </div>
            <div>Sales Marketing</div>
        </li>
        <li>
            <div class="image">
                <a href="/jobs/category/Services_Others"><img src="/img/cat/service-01-01.png" /></a>
            </div>
            <div>Services / Others</div>
        </li>
    </ul>
    <div class="clear"></div>
</div>
<div class="home-mid-btns">
    <ul>
        <li>
            <a href="/jobs/all">Find Jobs</a>
            <div>Browse All Jobs</div>
        </li>
        <li>
            <a href="#">Post Your Job</a>
            <div>Post Your Job</div>
        </li>
    </ul>
</div>
<div class="home-who">
    <h2>Who We Are</h2>
    <div class="content">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer pharetra eros tellus, non vestibulum massa dignissim feugiat. Aenean interdum odio eget orci consequat semper quis vitae turpis. Ut consequat odio eu nisl elementum lobortis. Phasellus porta tortor in ipsum suscipit efficitur. Ut ullamcorper enim libero, et vehicula eros venenatis vitae. Vestibulum feugiat vitae nulla in vehicula. Fusce eu ex ac ante malesuada aliquam. Nam nec elit a lacus ornare eleifend eget sed nulla. Sed eu consequat libero.
    </div>
</div>
<div class="home-blog-news">
    <h2>Blog + News</h2>
    <div class="content">
        <iframe width="100%" height="315" src="https://www.youtube.com/embed/Yxq-GEXSlNk" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
    </div>
</div>
<div>&nbsp;</div>

<?= $this->Html->script('home') ?>