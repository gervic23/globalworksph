<div class="no-email">
    <div class="error-img">
        <img src="/img/errorstop.png" />
    </div>
    <div class="contents">
        <h2>Opps! Did you uncheck your email while trying to login with facebook?</h2>
        <p>
            We hightly recommended to include your facebook email during login. Email address is our primary basis of identification of 
            accounts.
        </p>
        <p>Try to hit login with facebook again.</p>
        <p>If you keep seeing this page do the following steps...</p>

        <ul>
            <li>Go to your facebook account</li>
            <li>Click the navigation menu at the upper right and click settings.</li>
            <li>Click Apps and Websites.</li>
            <li>Active Apps and Websites, Remove Globalworks from the list.</li>
            <li>Try to Login with Facebook again.</li>
        </ul>
    </div>
</div>
<script>
    no_email();

    $(window).resize(function() {
        no_email();
    });

    function no_email() {
        var height = $(window).height();
        var new_height = height - 50;
        var no_email = $(".no-email").height();
        
        if (no_email < height) {
            $(".no-email").css({
                "height": new_height + "px"
            });
        }
    }
</script>