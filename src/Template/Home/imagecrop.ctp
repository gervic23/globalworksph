<?php
    $this->Html->css('cropper', ['block' => true]);
    $this->Html->script('cropper', ['block' => true]);
?>
<div class="setprofile">
    <?php if ($_SERVER['REQUEST_URI'] == '/imagecrop/new_user' || $_SERVER['REQUEST_URI'] == '/imagecrop/new_user/'): ?>
        <div class="progress">
            <span class="account-registration complete1">Account Registration</span>
            <span class="profile-setup complete2">Create Your Profile</span>
            <span class="upload-profile-pic <?= ($role == 'Employer') ? 'end-nav' : ''; ?>">Upload Your Picture</span>
            <?= ($role == 'Applicant') ? '<span class="upload-resume">Upload Your Resume</span>' : ''; ?>
            <div class="clear"></div>
        </div>
    <?php endif; ?>
    <div class="content">
        <h1>Crop Your Picture</h1>
        <div class="form">
            <div class="image">
                <img id="image" alt="crop" src="/img/users/tmp/<?= $filename ?>" />
            </div>
            <div class="btn-container btn-crop">
                <?= $this->Form->button('Crop', ['type' => 'button', 'id' => 'crop']) ?>
            </div>
        </div>
        <div>&nbsp;</div>
    </div>
</div>
<?= $this->Html->script('register'); ?>
<script>
    $(function() {
        $("#image").cropper();
        
        $("#crop").on("click", function() {
            $(this).hide();
            $("#image").cropper("getCroppedCanvas").toBlob(function(blob) {
                var formData = new FormData();
                
                formData.append("cropperImage", blob);
                formData.append("operation", "crop_image");
                formData.append("_csrfToken", "<?= $this->request->getParam('_csrfToken'); ?>");
                formData.append("user_id", "<?= $this->request->session()->read('Auth.User')['id'] ?>");
                formData.append("filename", "<?= $filename ?>");
				
                $.ajax('/ajax', {
					method: "post",
					data: formData,
					processData: false,
					contentType: false,
					success: function (r) {
                        <?php if ($_SERVER['REQUEST_URI'] == '/imagecrop' || $_SERVER['REQUEST_URI'] == '/imagecrop/' || $_SERVER['REQUEST_URI'] == '/imagecrop/new_user' || $_SERVER['REQUEST_URI'] == '/imagecrop/new_user/'): ?>
                            if (r == "ok") {
                                <?= ($role == 'Applicant') ? 'location = "/uploadresume/new_user"' : 'location = "/employers/dashboard/"' ?>
                            }
                        <?php else: ?>
                            console.log(r);
                        <?php endif; ?>
					},
					error: function () {
					  console.log('Upload error');
					}
				});
            });
        });
    });
</script>