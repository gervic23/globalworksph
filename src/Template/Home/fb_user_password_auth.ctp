<div class="fb-password">
    <div class="form">
        <h4>Your email "<?= $email ?>" associated with your facebook account is already registered in our database</h4>
        <p>If you are the owner of this account, please enter your password.</p>
        <div class="flash-error">
            <?= $this->Flash->render() ?>
        </div>
        <?= $this->Form->create($users) ?>
        <?= $this->Form->hidden('email', ['value' => $email]); ?>
        <?= $this->Form->control('password', ['label' => 'Enter Your Password Here']) ?>
        <div class="recaptcha">
            <?= $this->Recaptcha->display() ?>
        </div>
        <div class="btn-container">
            <?= $this->Form->submit('Verify Password and Login') ?>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
<?= $this->Html->script('fb-password') ?>