<div class="login-main">
    <div class="background"></div>
    <div class="form">
        <?php if ($_SERVER['REQUEST_URI'] == '/login/employers' || $_SERVER['REQUEST_URI'] == '/login/employers/'): ?>
            <h1>Employer's Login</h1>
        <?php else: ?>
            <h1>User's Login</h1>
        <?php endif; ?>
        <div class="login-flash">
            <?= $this->Flash->render() ?>
        </div>
        <?= $this->Facebook->fblogin() ?>
        <?= $this->Form->create($home) ?>
        <field>
            <div class="email">
                <span class="login-icons icon-login"></span>
                <?= $this->Form->text('email', ['placeholder' => 'Your Email', 'autocomplete' => 'off']) ?>
            </div>
            <div class="password">
                <span class="login-icons icon-password"></span>
                <?= $this->Form->password('password', ['placeholder' => 'Your Password']) ?>
            </div>
        </field>
        <div class="recaptcha">
            <?= $this->Recaptcha->display() ?>
        </div>
        <div class="submit">
            <span class="login-icons icon-btn"></span>
            <span>Login</span>
        </div>
        <?= $this->Form->end() ?>
        <div class="bottom">
            <ul>
                <li><a href="/forgot_password">Forgot Password</a></li>
                <li>
                    <div class="divider"></div>
                    <div class="label">Not Registered Yet?</div>
                    <?php if ($_SERVER['REQUEST_URI'] == '/login/employers' || $_SERVER['REQUEST_URI'] == '/login/employers/'): ?>
                        <a class="register-btn" href="/register/employers">Register Here</a>
                    <?php else: ?>
                        <a class="register-btn" href="/register">Register Here</a>
                    <?php endif; ?>
                </li>
            </ul>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(".submit").on("click", function() {
        $(".login-main .form form").submit();
    });
</script>

<?= $this->Html->script('login') ?>