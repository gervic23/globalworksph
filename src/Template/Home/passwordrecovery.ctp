<div class="password-recovery">
    <h1>Password Recovery</h1>
    <div class="flash">
        <?= $this->Flash->render() ?>
    </div>
    <div class="form">
        <?= $this->Form->create($passwords) ?>
        <?= $this->Form->control('new-password', ['label' => 'New Password:', 'type' => 'password', 'class' => 'password-input']) ?>
        <?= $this->Form->control('rpassword', ['label' => 'Confirm Password:', 'type' => 'password', 'class' => 'password-input']) ?>
        <div class="btn-container">
            <?= $this->Form->submit('Change Password') ?>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
<script>

    password_recovery();

    $(window).resize(function() {
        password_recovery();
    });

    <?php if ($redirect): ?>
        $(".btn-container [type=submit]").hide();

        setTimeout(function() {
            location = "/";
        }, 5000);
    <?php endif; ?>

    $("#new-password").on("click focus", function() {
        inputAnimate("new-password");
    }).on("focusout", function() {
        focusoutAnimate("new-password");
    });

    $("#rpassword").on("click focus", function() {
        inputAnimate("rpassword");
    }).on("focusout", function() {
        focusoutAnimate("rpassword");
    });

    $("#new-password").on("keyup input", function() {
        $(this).css({
            "border-color": "#ccc",
            "background-color": "#fff"
        });
        $(".password-recovery .form .error:nth-child(2) .error-message").hide();
    });

    $("#rpassword").on("keyup input", function() {
        $(this).css({
            "border-color": "#ccc",
            "background-color": "#fff"
        });
        $(".password-recovery .form .error:nth-child(3) .error-message").hide();
    });

    function password_recovery() {
        var height = $(window).height();
        var new_height = height - 50;
        var password_recovery = $(".password-recovery").height();

        if (password_recovery < height) {
            $(".password-recovery").css({
                "height": new_height + "px"
            });
        }
    }

    function inputAnimate(field) {
        $("#" + field).animate({
            padding: "10px 5px"
        }, 500, function() {
            $("#" + field).css({
                "font-size": "18px"
            });
        });
    }

    function focusoutAnimate(field) {
        $("#" + field).animate({
            padding: "0px"
        }, 500, function() {
            $("#" + field).css({
                "font-size": "15px"
            });
        });
    }
</script>