<?php
    $countries = [
        '' => 'Select Your Country',
        'AF'=>'AFGHANISTAN',
        'AL'=>'ALBANIA',
        'DZ'=>'ALGERIA',
        'AS'=>'AMERICAN SAMOA',
        'AD'=>'ANDORRA',
        'AO'=>'ANGOLA',
        'AI'=>'ANGUILLA',
        'AQ'=>'ANTARCTICA',
        'AG'=>'ANTIGUA AND BARBUDA',
        'AR'=>'ARGENTINA',
        'AM'=>'ARMENIA',
        'AW'=>'ARUBA',
        'AU'=>'AUSTRALIA',
        'AT'=>'AUSTRIA',
        'AZ'=>'AZERBAIJAN',
        'BS'=>'BAHAMAS',
        'BH'=>'BAHRAIN',
        'BD'=>'BANGLADESH',
        'BB'=>'BARBADOS',
        'BY'=>'BELARUS',
        'BE'=>'BELGIUM',
        'BZ'=>'BELIZE',
        'BJ'=>'BENIN',
        'BM'=>'BERMUDA',
        'BT'=>'BHUTAN',
        'BO'=>'BOLIVIA',
        'BA'=>'BOSNIA AND HERZEGOVINA',
        'BW'=>'BOTSWANA',
        'BV'=>'BOUVET ISLAND',
        'BR'=>'BRAZIL',
        'IO'=>'BRITISH INDIAN OCEAN TERRITORY',
        'BN'=>'BRUNEI DARUSSALAM',
        'BG'=>'BULGARIA',
        'BF'=>'BURKINA FASO',
        'BI'=>'BURUNDI',
        'KH'=>'CAMBODIA',
        'CM'=>'CAMEROON',
        'CA'=>'CANADA',
        'CV'=>'CAPE VERDE',
        'KY'=>'CAYMAN ISLANDS',
        'CF'=>'CENTRAL AFRICAN REPUBLIC',
        'TD'=>'CHAD',
        'CL'=>'CHILE',
        'CN'=>'CHINA',
        'CX'=>'CHRISTMAS ISLAND',
        'CC'=>'COCOS (KEELING) ISLANDS',
        'CO'=>'COLOMBIA',
        'KM'=>'COMOROS',
        'CG'=>'CONGO',
        'CD'=>'CONGO, THE DEMOCRATIC REPUBLIC OF THE',
        'CK'=>'COOK ISLANDS',
        'CR'=>'COSTA RICA',
        'CI'=>'COTE D IVOIRE',
        'HR'=>'CROATIA',
        'CU'=>'CUBA',
        'CY'=>'CYPRUS',
        'CZ'=>'CZECH REPUBLIC',
        'DK'=>'DENMARK',
        'DJ'=>'DJIBOUTI',
        'DM'=>'DOMINICA',
        'DO'=>'DOMINICAN REPUBLIC',
        'TP'=>'EAST TIMOR',
        'EC'=>'ECUADOR',
        'EG'=>'EGYPT',
        'SV'=>'EL SALVADOR',
        'GQ'=>'EQUATORIAL GUINEA',
        'ER'=>'ERITREA',
        'EE'=>'ESTONIA',
        'ET'=>'ETHIOPIA',
        'FK'=>'FALKLAND ISLANDS (MALVINAS)',
        'FO'=>'FAROE ISLANDS',
        'FJ'=>'FIJI',
        'FI'=>'FINLAND',
        'FR'=>'FRANCE',
        'GF'=>'FRENCH GUIANA',
        'PF'=>'FRENCH POLYNESIA',
        'TF'=>'FRENCH SOUTHERN TERRITORIES',
        'GA'=>'GABON',
        'GM'=>'GAMBIA',
        'GE'=>'GEORGIA',
        'DE'=>'GERMANY',
        'GH'=>'GHANA',
        'GI'=>'GIBRALTAR',
        'GR'=>'GREECE',
        'GL'=>'GREENLAND',
        'GD'=>'GRENADA',
        'GP'=>'GUADELOUPE',
        'GU'=>'GUAM',
        'GT'=>'GUATEMALA',
        'GN'=>'GUINEA',
        'GW'=>'GUINEA-BISSAU',
        'GY'=>'GUYANA',
        'HT'=>'HAITI',
        'HM'=>'HEARD ISLAND AND MCDONALD ISLANDS',
        'VA'=>'HOLY SEE (VATICAN CITY STATE)',
        'HN'=>'HONDURAS',
        'HK'=>'HONG KONG',
        'HU'=>'HUNGARY',
        'IS'=>'ICELAND',
        'IN'=>'INDIA',
        'ID'=>'INDONESIA',
        'IR'=>'IRAN, ISLAMIC REPUBLIC OF',
        'IQ'=>'IRAQ',
        'IE'=>'IRELAND',
        'IL'=>'ISRAEL',
        'IT'=>'ITALY',
        'JM'=>'JAMAICA',
        'JP'=>'JAPAN',
        'JO'=>'JORDAN',
        'KZ'=>'KAZAKSTAN',
        'KE'=>'KENYA',
        'KI'=>'KIRIBATI',
        'KP'=>'KOREA DEMOCRATIC PEOPLES REPUBLIC OF',
        'KR'=>'KOREA REPUBLIC OF',
        'KW'=>'KUWAIT',
        'KG'=>'KYRGYZSTAN',
        'LA'=>'LAO PEOPLES DEMOCRATIC REPUBLIC',
        'LV'=>'LATVIA',
        'LB'=>'LEBANON',
        'LS'=>'LESOTHO',
        'LR'=>'LIBERIA',
        'LY'=>'LIBYAN ARAB JAMAHIRIYA',
        'LI'=>'LIECHTENSTEIN',
        'LT'=>'LITHUANIA',
        'LU'=>'LUXEMBOURG',
        'MO'=>'MACAU',
        'MK'=>'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF',
        'MG'=>'MADAGASCAR',
        'MW'=>'MALAWI',
        'MY'=>'MALAYSIA',
        'MV'=>'MALDIVES',
        'ML'=>'MALI',
        'MT'=>'MALTA',
        'MH'=>'MARSHALL ISLANDS',
        'MQ'=>'MARTINIQUE',
        'MR'=>'MAURITANIA',
        'MU'=>'MAURITIUS',
        'YT'=>'MAYOTTE',
        'MX'=>'MEXICO',
        'FM'=>'MICRONESIA, FEDERATED STATES OF',
        'MD'=>'MOLDOVA, REPUBLIC OF',
        'MC'=>'MONACO',
        'MN'=>'MONGOLIA',
        'MS'=>'MONTSERRAT',
        'MA'=>'MOROCCO',
        'MZ'=>'MOZAMBIQUE',
        'MM'=>'MYANMAR',
        'NA'=>'NAMIBIA',
        'NR'=>'NAURU',
        'NP'=>'NEPAL',
        'NL'=>'NETHERLANDS',
        'AN'=>'NETHERLANDS ANTILLES',
        'NC'=>'NEW CALEDONIA',
        'NZ'=>'NEW ZEALAND',
        'NI'=>'NICARAGUA',
        'NE'=>'NIGER',
        'NG'=>'NIGERIA',
        'NU'=>'NIUE',
        'NF'=>'NORFOLK ISLAND',
        'MP'=>'NORTHERN MARIANA ISLANDS',
        'NO'=>'NORWAY',
        'OM'=>'OMAN',
        'PK'=>'PAKISTAN',
        'PW'=>'PALAU',
        'PS'=>'PALESTINIAN TERRITORY, OCCUPIED',
        'PA'=>'PANAMA',
        'PG'=>'PAPUA NEW GUINEA',
        'PY'=>'PARAGUAY',
        'PE'=>'PERU',
        'PH'=>'PHILIPPINES',
        'PN'=>'PITCAIRN',
        'PL'=>'POLAND',
        'PT'=>'PORTUGAL',
        'PR'=>'PUERTO RICO',
        'QA'=>'QATAR',
        'RE'=>'REUNION',
        'RO'=>'ROMANIA',
        'RU'=>'RUSSIAN FEDERATION',
        'RW'=>'RWANDA',
        'SH'=>'SAINT HELENA',
        'KN'=>'SAINT KITTS AND NEVIS',
        'LC'=>'SAINT LUCIA',
        'PM'=>'SAINT PIERRE AND MIQUELON',
        'VC'=>'SAINT VINCENT AND THE GRENADINES',
        'WS'=>'SAMOA',
        'SM'=>'SAN MARINO',
        'ST'=>'SAO TOME AND PRINCIPE',
        'SA'=>'SAUDI ARABIA',
        'SN'=>'SENEGAL',
        'SC'=>'SEYCHELLES',
        'SL'=>'SIERRA LEONE',
        'SG'=>'SINGAPORE',
        'SK'=>'SLOVAKIA',
        'SI'=>'SLOVENIA',
        'SB'=>'SOLOMON ISLANDS',
        'SO'=>'SOMALIA',
        'ZA'=>'SOUTH AFRICA',
        'GS'=>'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS',
        'ES'=>'SPAIN',
        'LK'=>'SRI LANKA',
        'SD'=>'SUDAN',
        'SR'=>'SURINAME',
        'SJ'=>'SVALBARD AND JAN MAYEN',
        'SZ'=>'SWAZILAND',
        'SE'=>'SWEDEN',
        'CH'=>'SWITZERLAND',
        'SY'=>'SYRIAN ARAB REPUBLIC',
        'TW'=>'TAIWAN, PROVINCE OF CHINA',
        'TJ'=>'TAJIKISTAN',
        'TZ'=>'TANZANIA, UNITED REPUBLIC OF',
        'TH'=>'THAILAND',
        'TG'=>'TOGO',
        'TK'=>'TOKELAU',
        'TO'=>'TONGA',
        'TT'=>'TRINIDAD AND TOBAGO',
        'TN'=>'TUNISIA',
        'TR'=>'TURKEY',
        'TM'=>'TURKMENISTAN',
        'TC'=>'TURKS AND CAICOS ISLANDS',
        'TV'=>'TUVALU',
        'UG'=>'UGANDA',
        'UA'=>'UKRAINE',
        'AE'=>'UNITED ARAB EMIRATES',
        'GB'=>'UNITED KINGDOM',
        'US'=>'UNITED STATES',
        'UM'=>'UNITED STATES MINOR OUTLYING ISLANDS',
        'UY'=>'URUGUAY',
        'UZ'=>'UZBEKISTAN',
        'VU'=>'VANUATU',
        'VE'=>'VENEZUELA',
        'VN'=>'VIET NAM',
        'VG'=>'VIRGIN ISLANDS, BRITISH',
        'VI'=>'VIRGIN ISLANDS, U.S.',
        'WF'=>'WALLIS AND FUTUNA',
        'EH'=>'WESTERN SAHARA',
        'YE'=>'YEMEN',
        'YU'=>'YUGOSLAVIA',
        'ZM'=>'ZAMBIA',
        'ZW'=>'ZIMBABWE',
    ];
?>

<div class="setprofile">
    <?php if ($_SERVER['REQUEST_URI'] == '/setprofile/new_user' || $_SERVER['REQUEST_URI'] == '/setprofile/new_user/'): ?>
        <div class="progress">
            <span class="account-registration complete1">Account Registration</span>
            <span class="profile-setup">Create Your Profile</span>
            <span class="upload-profile-pic end-nav">Upload Your Picture</span>
            <div class="clear"></div>
        </div>
    <?php endif; ?>
    <div class="content">
        <h2><?= ($_SERVER['REQUEST_URI'] == '/setprofile/new_user') ? 'Create Your Profile' : 'Your Profile' ?></h2>
        <div class="flash-error">
            
        </div>
        <div class="form">
            <?= $this->Form->control('business_name', ['autocomplete' => 'off', 'label' => 'Business/Compay Name:', 'id' => 'business_name']) ?>
            <?= $this->Form->control('business_description', ['autocomplete' => 'off', 'label' => 'Business/Company Description:', 'type' => 'textarea', 'id' => 'business_description']) ?>
            <?= $this->Form->control('business_website', ['autocomplete' => 'off', 'label' => 'Buisness/Company Webste:', 'id' => 'business_website']) ?>
            <?= $this->Form->control('business_email', ['autocomplete' => 'off', 'label' => 'Business/Company Email:', 'id' => 'business_email']) ?>
            <?= $this->Form->control('business_contact_number', ['autocomplete' => 'off', 'label' => 'Business/Company Contact Number:', 'id' => 'business_contact_number']) ?>
            <?= $this->Form->control('business_address', ['autocomplete' => 'off', 'label' => 'Business/Company Address:', 'type' => 'textarea', 'id' => 'business_address']) ?>
            <?= $this->Form->control('business_state', ['autocomplete' => 'off', 'label' => 'Business/Company State/Region:', 'id' => 'business_state']) ?>
            <?= $this->Form->control('business_country', ['label' => 'Business/Company Country:', 'type' => 'select', 'options' => $countries, 'id' => 'business_country']) ?>
            <div class="btn-container">
                <?= $this->Form->button('Skip', ['type' => 'button', 'id' => 'skip']) ?>
                <?= $this->Form->button('Save', ['type' => 'button', 'id' => 'next']); ?>
            </div>
        </div>
    </div>
</div>
<script>
    $("#skip").on("click", function() {
        location = "/uploadpicture";
    });

    $("#business-name").on("click focus", function() {
        inputAnimate("business-name");
    }).on("focusout", function() {
        focusoutAnimate("business-name");
    });

    $("#business-description").on("click focus", function() {
        $(this).animate({
            height: "100px"
        }, 500, function() {
            $(this).css({
                "border": "2px solid #ccc"
            });
        });
    }).focusout(function() {
        $(this).animate({
            height: "20px"
        }, 500, function() {
            $(this).css({
                "border": 0,
                "border-bottom": "2px solid #ccc"
            });
        });
    });

    $("#skip").on("click", function() {
        location = "/uploadpicture/new_user";
    });

    $("#business-website").on("click focus", function() {
        inputAnimate("business-website");
    }).on("focusout", function() {
        focusoutAnimate("business-website");
    });

    $("#business-email").on("click focus", function() {
        inputAnimate("business-email");
    }).on("focusout", function() {
        focusoutAnimate("business-email");
    });

    $("#business-contact-number").on("click focus", function() {
        inputAnimate("business-contact-number");
    }).on("focusout", function() {
        focusoutAnimate("business-contact-number");
    });

    $("#business-address").on("click focus", function() {
        $(this).animate({
            height: "100px"
        }, 500, function() {
            $(this).css({
                "border": "2px solid #ccc"
            });
        });
    }).focusout(function() {
        $(this).animate({
            height: "20px"
        }, 500, function() {
            $(this).css({
                "border": 0,
                "border-bottom": "2px solid #ccc"
            });
        });
    });

    $("#business-state").on("click focus", function() {
        inputAnimate("business-state");
    }).on("focusout", function() {
        focusoutAnimate("business-state");
    });

    $("#next").on("click", function() {
        if (has_filled()) {

            $(".btn-container").html('<img src="/img/loading2.gif" />');

            var data = {
                "operation": "setprofile",
                "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
                "business_name": $("#business_name").val(),
                "business_description": $("#business_description").val(),
                "business_email": $("#business_email").val(),
                "business_website": $("#business_website").val(),
                "business_contact_number": $("#business_contact_number").val(),
                "business_address": $("#business_address").val(),
                "business_country": $("#business_country").val()
            }

            $.post("/ajax", data, function(r) {
                if (r == "ok") {
                    location = "/uploadpicture";
                }
            });
        }
    });

    function has_filled() {
        if ($("#business_name").val() || $("#business_description").val() || $("#business_contact_number").val() || $("#business_contact_number").val() || $("#business_country").val()) {
            
            return true;
        }

        $(".flash-error").html("Please fill in atleast 1 field.");

        $("#business_name").focus();

        return false;
    }
</script>
<?= $this->Html->script('register'); ?>