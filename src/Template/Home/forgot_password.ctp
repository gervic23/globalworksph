<div class="forgot-password">
    <h1>Forgot Password</h1>
    <div class="flash">
        <?= $this->Flash->render() ?>
    </div>
    <div class="form">
        <?= $this->Form->create($users) ?>
        <?= $this->Form->control('email', ['label' => 'Enter Your Email Address', 'autocomplete' => 'off']) ?>
        <div class="btn-container">
            <?= $this->Form->submit('Submit') ?>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
<script>

    forgotPassword();

    $(window).resize(function() {
        forgotPassword();
    });

    <?php if ($disabled): ?>
        $("#email").attr("disabled", "disabled");
        $(".btn-container").hide();
    <?php endif; ?>

    $("#email").on("click focus", function() {
        inputAnimate("email");
    }).on("focusout", function() {
        focusoutAnimate("email");
    });

    function forgotPassword() {
        var height = $(window).height();
        var new_height = height - 40;
        var forgot_password = $(".forgot-password").height();
        
        if (forgot_password < height) {
            $(".forgot-password").css({
                "height": new_height
            });
        }
    }

    function inputAnimate(field) {
        $("#" + field).animate({
            padding: "10px 5px"
        }, 500, function() {
            $("#" + field).css({
                "font-size": "25px"
            });
        });
    }

    function focusoutAnimate(field) {
        $("#" + field).animate({
            padding: "0px"
        }, 500, function() {
            $("#" + field).css({
                "font-size": "20px"
            });
        });
    }
</script>