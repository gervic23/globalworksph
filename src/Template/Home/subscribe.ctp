<div class="subscribe">
    <div>&nbsp;</div>
    <div class="contents">
        <h1>Subscribe To Our Mailing Joblist</h1>
        <div class="form">
            <?= $this->Flash->render() ?>
            <?= $this->Form->create($subscribers) ?>
            <?= $this->Form->control('email', ['label' => 'Your Email Address:', 'autocomplete' => 'off' , 'maxlength' => 50]) ?>
            <div class="btn-container">
                <?= $this->Form->submit('Subscribe') ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
    <div>&nbsp;</div>
    <div>&nbsp;</div>
</div>

<script>

    _subscribe();

    $(window).on("change", function() {
        _subscribe();
    });

    function _subscribe() {
        var height = $(window).height();
        var new_height = height;

        if (height > $(".main-section").height()) {
            $(".subscribe").css({
                "height": height + "px"
            });
        }
    }
</script>