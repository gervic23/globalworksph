<?php use App\Controller\GlobalworksController; $globalworks = new GlobalworksController(); ?>

<?php foreach ($ann_published as $announcement): ?>
    <tr>
        <th><?= $globalworks->ann_shorten(strip_tags($announcement['announcement'])) ?></th>
        <th><?= $announcement['created_at'] ?></th>
        <th><input type="checkbox" id="<?= $announcement['id'] ?>" onchange="javascript: _publishing(<?= $announcement['id'] ?>)" checked="checked" /></th>
        <th><a href="/admin/announcements/edit/<?= $announcement['id'] ?>">Edit</a></th>
        <th><a href="#">Delete</a></th>
    </tr>
<?php endforeach; ?>
<?php foreach ($ann_notpublished as $announcement): ?>
    <tr>
        <td><?= $globalworks->ann_shorten(strip_tags($announcement['announcement'])) ?></td>
        <td><?= $announcement['created_at'] ?></td>
        <td><input type="checkbox" id="<?= $announcement['id'] ?>" onchange="javascript: _publishing(<?= $announcement['id'] ?>)" /></td>
        <td><a href="/admin/announcements/edit/<?= $announcement['id'] ?>">Edit</a></td>
        <td><a href="javascript:" onclick="javascript: _delete(<?= $announcement['id'] ?>)">Delete</a></td>
    </tr>
<?php endforeach; ?>

<script>
    
    function _publishing(id) {
        var the_id = $("#" + id);

        if (the_id.is(":checked")) {
            var data = {
                "operation": "announcement_published",
                "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
                "id": id
            };
        } else {
            var data = {
                "operation": "announcement_unpublished",
                "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
                "id": id
            };
        }

        $.post("/adminajax", data, function(r) {
            _announcements();
        })
    }

    function _delete(id) {
        var c = confirm("Are you sure you want to delete this announcement?");

        if (c) {
            var data = {
                "operation": "announcement_delete",
                "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
                "id": id
            };

            $.post("/adminajax", data, function(r) {
                _announcements();
            });
        }
    }
</script>