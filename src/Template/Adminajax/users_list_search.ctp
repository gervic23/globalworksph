<?php use App\Controller\GlobalworksController; $globalworks = new GlobalworksController(); ?>

<table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th>&nbsp;</th>
            <th>Email</th>
            <th>Firstname</th>
            <th>Lastname</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($users as $user): ?>
            <?php if ($user['home']['role'] == $role): ?>
                <tr>
                    <?php if ($user['profiles']['profile_picture']): ?>
                        <td><img src="/img/users/profile_thumbnails/<?= $user['profiles']['profile_picture'] ?>" /></td>
                    <?php else: ?>
                        <td><img src="/img/users/nopropic.png" /></td>
                    <?php endif; ?>
                    <td><?= $user['home']['email'] ?></td>
                    <td><?= $user['home']['firstname'] ?></td>
                    <td><?= $user['home']['lastname'] ?></td>
                    <td><span onclick="javascript: _view(<?= $user['home']['id'] ?>)">View</span></td>
                    <td><span onclick="javascript: _edit(<?= $user['home']['id'] ?>)">Edit</span></td>
                    <td><span onclick="javascript: change_role(<?= $user['home']['id'] ?>)">Change Role</span></td>
                    <td><span onclick="javascript: _suspend(<?= $user['home']['id'] ?>)">Suspend</span></td>
                    <td><span onclick="javascript: _delete(<?= $user['home']['id'] ?>)">Delete</span></td>
                </tr>
            <?php endif; ?>
        <?php endforeach; ?>
    </tbody>
</table>