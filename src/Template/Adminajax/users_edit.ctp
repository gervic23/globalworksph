<?php 
    use App\Controller\GlobalworksController; 
    
    $globalworks = new GlobalworksController();

    $countries = [
        '' => 'Select Your Country',
        'AF'=>'AFGHANISTAN',
        'AL'=>'ALBANIA',
        'DZ'=>'ALGERIA',
        'AS'=>'AMERICAN SAMOA',
        'AD'=>'ANDORRA',
        'AO'=>'ANGOLA',
        'AI'=>'ANGUILLA',
        'AQ'=>'ANTARCTICA',
        'AG'=>'ANTIGUA AND BARBUDA',
        'AR'=>'ARGENTINA',
        'AM'=>'ARMENIA',
        'AW'=>'ARUBA',
        'AU'=>'AUSTRALIA',
        'AT'=>'AUSTRIA',
        'AZ'=>'AZERBAIJAN',
        'BS'=>'BAHAMAS',
        'BH'=>'BAHRAIN',
        'BD'=>'BANGLADESH',
        'BB'=>'BARBADOS',
        'BY'=>'BELARUS',
        'BE'=>'BELGIUM',
        'BZ'=>'BELIZE',
        'BJ'=>'BENIN',
        'BM'=>'BERMUDA',
        'BT'=>'BHUTAN',
        'BO'=>'BOLIVIA',
        'BA'=>'BOSNIA AND HERZEGOVINA',
        'BW'=>'BOTSWANA',
        'BV'=>'BOUVET ISLAND',
        'BR'=>'BRAZIL',
        'IO'=>'BRITISH INDIAN OCEAN TERRITORY',
        'BN'=>'BRUNEI DARUSSALAM',
        'BG'=>'BULGARIA',
        'BF'=>'BURKINA FASO',
        'BI'=>'BURUNDI',
        'KH'=>'CAMBODIA',
        'CM'=>'CAMEROON',
        'CA'=>'CANADA',
        'CV'=>'CAPE VERDE',
        'KY'=>'CAYMAN ISLANDS',
        'CF'=>'CENTRAL AFRICAN REPUBLIC',
        'TD'=>'CHAD',
        'CL'=>'CHILE',
        'CN'=>'CHINA',
        'CX'=>'CHRISTMAS ISLAND',
        'CC'=>'COCOS (KEELING) ISLANDS',
        'CO'=>'COLOMBIA',
        'KM'=>'COMOROS',
        'CG'=>'CONGO',
        'CD'=>'CONGO, THE DEMOCRATIC REPUBLIC OF THE',
        'CK'=>'COOK ISLANDS',
        'CR'=>'COSTA RICA',
        'CI'=>'COTE D IVOIRE',
        'HR'=>'CROATIA',
        'CU'=>'CUBA',
        'CY'=>'CYPRUS',
        'CZ'=>'CZECH REPUBLIC',
        'DK'=>'DENMARK',
        'DJ'=>'DJIBOUTI',
        'DM'=>'DOMINICA',
        'DO'=>'DOMINICAN REPUBLIC',
        'TP'=>'EAST TIMOR',
        'EC'=>'ECUADOR',
        'EG'=>'EGYPT',
        'SV'=>'EL SALVADOR',
        'GQ'=>'EQUATORIAL GUINEA',
        'ER'=>'ERITREA',
        'EE'=>'ESTONIA',
        'ET'=>'ETHIOPIA',
        'FK'=>'FALKLAND ISLANDS (MALVINAS)',
        'FO'=>'FAROE ISLANDS',
        'FJ'=>'FIJI',
        'FI'=>'FINLAND',
        'FR'=>'FRANCE',
        'GF'=>'FRENCH GUIANA',
        'PF'=>'FRENCH POLYNESIA',
        'TF'=>'FRENCH SOUTHERN TERRITORIES',
        'GA'=>'GABON',
        'GM'=>'GAMBIA',
        'GE'=>'GEORGIA',
        'DE'=>'GERMANY',
        'GH'=>'GHANA',
        'GI'=>'GIBRALTAR',
        'GR'=>'GREECE',
        'GL'=>'GREENLAND',
        'GD'=>'GRENADA',
        'GP'=>'GUADELOUPE',
        'GU'=>'GUAM',
        'GT'=>'GUATEMALA',
        'GN'=>'GUINEA',
        'GW'=>'GUINEA-BISSAU',
        'GY'=>'GUYANA',
        'HT'=>'HAITI',
        'HM'=>'HEARD ISLAND AND MCDONALD ISLANDS',
        'VA'=>'HOLY SEE (VATICAN CITY STATE)',
        'HN'=>'HONDURAS',
        'HK'=>'HONG KONG',
        'HU'=>'HUNGARY',
        'IS'=>'ICELAND',
        'IN'=>'INDIA',
        'ID'=>'INDONESIA',
        'IR'=>'IRAN, ISLAMIC REPUBLIC OF',
        'IQ'=>'IRAQ',
        'IE'=>'IRELAND',
        'IL'=>'ISRAEL',
        'IT'=>'ITALY',
        'JM'=>'JAMAICA',
        'JP'=>'JAPAN',
        'JO'=>'JORDAN',
        'KZ'=>'KAZAKSTAN',
        'KE'=>'KENYA',
        'KI'=>'KIRIBATI',
        'KP'=>'KOREA DEMOCRATIC PEOPLES REPUBLIC OF',
        'KR'=>'KOREA REPUBLIC OF',
        'KW'=>'KUWAIT',
        'KG'=>'KYRGYZSTAN',
        'LA'=>'LAO PEOPLES DEMOCRATIC REPUBLIC',
        'LV'=>'LATVIA',
        'LB'=>'LEBANON',
        'LS'=>'LESOTHO',
        'LR'=>'LIBERIA',
        'LY'=>'LIBYAN ARAB JAMAHIRIYA',
        'LI'=>'LIECHTENSTEIN',
        'LT'=>'LITHUANIA',
        'LU'=>'LUXEMBOURG',
        'MO'=>'MACAU',
        'MK'=>'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF',
        'MG'=>'MADAGASCAR',
        'MW'=>'MALAWI',
        'MY'=>'MALAYSIA',
        'MV'=>'MALDIVES',
        'ML'=>'MALI',
        'MT'=>'MALTA',
        'MH'=>'MARSHALL ISLANDS',
        'MQ'=>'MARTINIQUE',
        'MR'=>'MAURITANIA',
        'MU'=>'MAURITIUS',
        'YT'=>'MAYOTTE',
        'MX'=>'MEXICO',
        'FM'=>'MICRONESIA, FEDERATED STATES OF',
        'MD'=>'MOLDOVA, REPUBLIC OF',
        'MC'=>'MONACO',
        'MN'=>'MONGOLIA',
        'MS'=>'MONTSERRAT',
        'MA'=>'MOROCCO',
        'MZ'=>'MOZAMBIQUE',
        'MM'=>'MYANMAR',
        'NA'=>'NAMIBIA',
        'NR'=>'NAURU',
        'NP'=>'NEPAL',
        'NL'=>'NETHERLANDS',
        'AN'=>'NETHERLANDS ANTILLES',
        'NC'=>'NEW CALEDONIA',
        'NZ'=>'NEW ZEALAND',
        'NI'=>'NICARAGUA',
        'NE'=>'NIGER',
        'NG'=>'NIGERIA',
        'NU'=>'NIUE',
        'NF'=>'NORFOLK ISLAND',
        'MP'=>'NORTHERN MARIANA ISLANDS',
        'NO'=>'NORWAY',
        'OM'=>'OMAN',
        'PK'=>'PAKISTAN',
        'PW'=>'PALAU',
        'PS'=>'PALESTINIAN TERRITORY, OCCUPIED',
        'PA'=>'PANAMA',
        'PG'=>'PAPUA NEW GUINEA',
        'PY'=>'PARAGUAY',
        'PE'=>'PERU',
        'PH'=>'PHILIPPINES',
        'PN'=>'PITCAIRN',
        'PL'=>'POLAND',
        'PT'=>'PORTUGAL',
        'PR'=>'PUERTO RICO',
        'QA'=>'QATAR',
        'RE'=>'REUNION',
        'RO'=>'ROMANIA',
        'RU'=>'RUSSIAN FEDERATION',
        'RW'=>'RWANDA',
        'SH'=>'SAINT HELENA',
        'KN'=>'SAINT KITTS AND NEVIS',
        'LC'=>'SAINT LUCIA',
        'PM'=>'SAINT PIERRE AND MIQUELON',
        'VC'=>'SAINT VINCENT AND THE GRENADINES',
        'WS'=>'SAMOA',
        'SM'=>'SAN MARINO',
        'ST'=>'SAO TOME AND PRINCIPE',
        'SA'=>'SAUDI ARABIA',
        'SN'=>'SENEGAL',
        'SC'=>'SEYCHELLES',
        'SL'=>'SIERRA LEONE',
        'SG'=>'SINGAPORE',
        'SK'=>'SLOVAKIA',
        'SI'=>'SLOVENIA',
        'SB'=>'SOLOMON ISLANDS',
        'SO'=>'SOMALIA',
        'ZA'=>'SOUTH AFRICA',
        'GS'=>'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS',
        'ES'=>'SPAIN',
        'LK'=>'SRI LANKA',
        'SD'=>'SUDAN',
        'SR'=>'SURINAME',
        'SJ'=>'SVALBARD AND JAN MAYEN',
        'SZ'=>'SWAZILAND',
        'SE'=>'SWEDEN',
        'CH'=>'SWITZERLAND',
        'SY'=>'SYRIAN ARAB REPUBLIC',
        'TW'=>'TAIWAN, PROVINCE OF CHINA',
        'TJ'=>'TAJIKISTAN',
        'TZ'=>'TANZANIA, UNITED REPUBLIC OF',
        'TH'=>'THAILAND',
        'TG'=>'TOGO',
        'TK'=>'TOKELAU',
        'TO'=>'TONGA',
        'TT'=>'TRINIDAD AND TOBAGO',
        'TN'=>'TUNISIA',
        'TR'=>'TURKEY',
        'TM'=>'TURKMENISTAN',
        'TC'=>'TURKS AND CAICOS ISLANDS',
        'TV'=>'TUVALU',
        'UG'=>'UGANDA',
        'UA'=>'UKRAINE',
        'AE'=>'UNITED ARAB EMIRATES',
        'GB'=>'UNITED KINGDOM',
        'US'=>'UNITED STATES',
        'UM'=>'UNITED STATES MINOR OUTLYING ISLANDS',
        'UY'=>'URUGUAY',
        'UZ'=>'UZBEKISTAN',
        'VU'=>'VANUATU',
        'VE'=>'VENEZUELA',
        'VN'=>'VIET NAM',
        'VG'=>'VIRGIN ISLANDS, BRITISH',
        'VI'=>'VIRGIN ISLANDS, U.S.',
        'WF'=>'WALLIS AND FUTUNA',
        'EH'=>'WESTERN SAHARA',
        'YE'=>'YEMEN',
        'YU'=>'YUGOSLAVIA',
        'ZM'=>'ZAMBIA',
        'ZW'=>'ZIMBABWE',
      ];
    
    $months = [
        '' => 'Select Month of Birth',
        'January' => 'January',
        'February' => 'February',
        'March' => 'March',
        'April' => 'April',
        'May' => 'May',
        'June' => 'June',
        'July' => 'July',
        'August' => 'August',
        'September' => 'September',
        'October' => 'October',
        'November' => 'November',
        'December' => 'December'
    ];

    $days = [];
    $days[''] = 'Select Day of Birth';

    for ($i = 1; $i < 32; $i++) {
        $days[$i] = $i;
    }

    $years = [];
    $years[''] = 'Select Year of Brith';

    for ($x = date('Y'); $x > 1899; $x--) {
        $years[$x] = $x;
    }
?>

<?php foreach ($users as $user): ?>
    <?php
        $bday = explode('/', $user['birthday']);
        $b_month = $bday[0];
        $b_day = $bday[1];
        $b_year = $bday[2];
    ?>

    <div class="admin-user-edit-notify">
        <div class="contents">
            <h1>Please write your message</h1>
            <field>
                <?= $this->Form->textarea('del-edit-notify', ['id' => 'suspend-notify', 'class' => 'notify']) ?>
                <div class="notify-btns">
                    <ul>
                        <li><?= $this->Form->button('Notify User and Save', ['id' => 'notify-suspend-submit', 'class' => 'notify-btn']) ?></li>
                        <li><?= $this->Form->button('Cancel', ['id' => 'notify-edit-cancel', 'class' => 'notify-btn']) ?></li>
                    </ul>
                </div>
            </field>
        </div>
    </div>
    
    <div class="user-edit">
        <div class="input">
            <?= $this->Form->label('email', 'Email:') ?>
            <?= $this->Form->text('email', ['id' => 'email', 'class' => 'text-input', 'value' => $user['email'], 'disabled' => 'disabled']) ?>
        </div>
        <div class="input">
            <?= $this->Form->label('firstname', 'Firstname:') ?>
            <?= $this->Form->text('firstname', ['id' => 'firstname', 'class' => 'text-input', 'value' => $user['firstname'], 'autocomplete' => 'off']) ?>
            <div class="flash firstname-flash"></div>
        </div>
        <div class="input">
            <?= $this->Form->label('lastname', 'Lastname:') ?>
            <?= $this->Form->text('lastname', ['id' => 'lastname', 'class' => 'text-input', 'value' => $user['lastname'], 'autocomplete' => 'off']) ?>
            <div class="flash lastname-flash"></div>
        </div>
        <div class="input">
            <div class="password-toggle">
                <h3>Change Password</h3>
                <div>
                    <?= $this->Form->label('new_password', 'New Password:') ?>
                    <?= $this->Form->text('new_password', ['id' => 'new_password', 'class' => 'password-input', 'type' => 'password']); ?>
                </div>
                <div>
                    <?= $this->Form->label('rpassword', 'Confirm Password:') ?>
                    <?= $this->Form->text('new_password', ['id' => 'rpassword', 'class' => 'password-input', 'type' => 'password']); ?>
                </div>
                <div class="flash password-flash"></div>
            </div>
            <div class="password-btn">
                <span id="pass-toggle">Change Password</span>
            </div>
        </div>
        <div class="input">
            <?= $this->Form->label('birthday','Birthday:') ?>
            <?= $this->Form->select('month', $months, ['id' => 'month', 'class' => 'select-input', 'default' => $b_month]) ?>
            <?= $this->Form->select('day', $days, ['id' => 'day', 'class' => 'select-input', 'default' => $b_day]) ?>
            <?= $this->Form->select('year', $years, ['id' => 'year', 'class' => 'select-input', 'default' => $b_year]) ?>
            <div class="flash birthday-flash"></div>
        </div>
        <div class="input">
            <?= $this->Form->label('address', 'Address:') ?>
            <?= $this->Form->textarea('address', ['id' => 'address', 'class' => 'textarea-input', 'value' => $user['address']]) ?>
            <div class="flash address-flash"></div>
        </div>
        <div class="input">
            <?= $this->Form->label('region', 'State/Region:') ?>
            <?= $this->Form->text('region', ['id' => 'region', 'class' => 'text-input', 'value' => $user['region']]) ?>
            <div class="flash region-flash"></div>
        </div>
        <div class="input">
            <?= $this->Form->label('country', 'Country:') ?>
            <?= $this->Form->select('country', $countries, ['id' => 'country', 'class' => 'select-input', 'default' => $user['country']]) ?>
            <div class="flash country-flash"></div>
        </div>
        <div class="input">
            <?= $this->Form->label('contact_number', 'Contact Number:') ?>
            <?= $this->Form->text('contact_number', ['id' => 'contact_number', 'class' => 'text-input', 'value' => $user['contact_number']]) ?>
            <div class="flash contact-number-flash"></div>
        </div>
        <div class="btns">
            <ul>
                <li><span id="save">Save Changes</span></li>
                <li><span onclick="javascript: _edit_profile(<?= $user['id'] ?>)">Edit Profile</span></li>
                <li onclick="javascript: _edit_close(<?= $user['id'] ?>)"><span class="close-window"></span></li>
            </ul>
        </div>
        <div class="user-edit-success"></div>
    </div>

    <script>
        var password_toggle = false;

        if (window_toggled) {
            $(".close-window").html("Go Back");
        } else {
            $(".close-window").html("Close This Window");
        }

        $("#firstname").on("click focus", function() {
            inputAnimate("firstname");
        }).on("focusout", function() {
            focusoutAnimate("firstname");
        }).on("keyup input", function() {
            $(".firstname-flash").html("");
        });

        $("#lastname").on("click focus", function() {
            inputAnimate("lastname");
        }).on("focusout", function() {
            focusoutAnimate("lastname");
        }).on("keyup input", function() {
            $(".lastname-flash").html("");
        });

        $("#region").on("click focus", function() {
            inputAnimate("region");
        }).on("focusout", function() {
            focusoutAnimate("region");
        }).on("keyup input", function() {
            $(".region-flash").html("");
        });

        $("#contact_number").on("click focus", function() {
            inputAnimate("contact_number");
        }).on("focusout", function() {
            focusoutAnimate("contact_number");
        }).on("keyup input", function() {
            $(".contact-number-flash").html("");
        });

        $("#address").on("click focus", function() {
            $(this).animate({
                height: "100px"
            }, 500, function() {
                $(this).css({
                    "border": "2px solid #ccc"
                });
            });
        }).on("focusout", function() {
            $(this).animate({
                height: "20px"
            }, 500, function() {
                $(this).css({
                    "border": 0,
                    "border-bottom": "2px solid #ccc"
                });
            });
        }).on("keyup input", function() {
            $(".address-flash").html("");
        });

        $("#month, #day, #year").on("change", function() {
            $(".birthday-flash").html("");
        });

        $("#country").on("change", function() {
            $(".country-flash").html("");
        });

        $("#new_password").on("click focus", function() {
            inputAnimate("new_password");
        }).on("focusout", function() {
            focusoutAnimate("new_password");
        });

        $("#rpassword").on("click focus", function() {
            inputAnimate("rpassword");
        }).on("focusout", function() {
            focusoutAnimate("rpassword");
        });

        $("#pass-toggle").on("click", function() {
            if (!password_toggle) {
                password_toggle = true;
                $(".password-toggle").slideDown(500);
            } else {
                password_toggle = false;
                $(".password-toggle").slideUp(500);
            }
        });

        $("#save").on("click", function() {
            if ($("#firstname").val() == "") {
                $(".firstname-flash").html("Firstname is required.");
            } else if ($("#firstname").val().length < 2) {
                $(".fistname-flash").html("Firstname is too short.");
            } else if ($("#lastname").val() == "") {
                $(".lastname-flash").html("Lastname is required.");
            } else if ($("#lastname").val().length < 2) {
                $(".lastname-flash").html("Lastname is too short.");
            } else if ($("#month").val() == "") {
                $(".birthday-flash").html("Month of Birth is required.");
            } else if ($("#day").val() == "") {
                $(".birthday-flash").html("Day of Birth is required.");
            } else if ($("#year").val() == "") {
                $(".birthday-flash").html("Year of Birth is required.");
            } else if ($("#address").val() == "") {
                $(".address-flash").html("Address is required.");
            } else if ($("#address").val().length < 7) {
                $(".address-flash").html("Address is too short. Minimum of 8 characters.");
            } else if ($("#country").val() == "") {
                $(".country-flash").html("Address is required.");
            } else if ($("#region").val() == "") {
                $(".region-flash").html("State/Region is required.");
            } else if ($("#region").val().length < 2) {
                $(".region-flash").html("State/Region is too short.");
            } else if ($("#contact_number").val() == "") {
                $(".contact-number-flash").html("Contact Number is required.");
            } else if ($("#contact_number").val().length < 3) {
                $(".contact-number-flash").html("Contact Number is too short.");
            } else if (password_toggle && $("#new_password").val() == "") {
                $(".password-flash").html("New Password is required.");
            } else if (password_toggle && $("#new_password").val().length < 5) {
                $(".password-flash").html("New Password is too short. Minimum of 6 characters.");
            } else if (password_toggle && $("#rpassword").val() == "") {
                $(".password-flash").html("Please Confirm Password.");
            } else if (password_toggle && $("#new_password").val() !== $("#rpassword").val()) {
                $(".password-flash").html("Passwords did not match.");
            } else {
                var values = {
                    "edit_notify": 0,
                    "edit_notify_msg": ""
                };
                var c = confirm("You you like to notify the user about the changes?");

                if (c) {
                    values['edit_notify'] = 1;
                    $(".admin-user-edit-notify").fadeIn(500);

                    $("#notify-edit-cancel").on("click", function() {
                        values['edit_notify'] = 0;
                        $(".admin-user-edit-notify").fadeOut(500);
                    });

                    $("#notify-edit-submit").on("click", function() {
                        if ($("#edit-notify").val() == "") {
                            alert("Please write your message.");
                        } else {
                            values['edit_notify'] = 1;
                            values['edit_notify_msg'] = $("#edit-notify").val();

                            save_info(values);
                        }
                    });
                } else {
                    save_info(values);
                }
            }
        });

        function save_info(values) {
            var data = {
                "operation": "user_info_save",
                "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
                "id": <?= $user['id'] ?>,
                "firstname": $("#firstname").val(),
                "lastname": $("#lastname").val(),
                "password": $("#new_password").val(),
                "birthday": $("#month").val() + "/" + $("#day").val() + "/" + $("#year").val(),
                "address": $("#address").val(),
                "region": $("#region").val(),
                "country": $("#country").val(),
                "contact_number": $("#contact_number").val(),
                "notify": values['edit_notify'],
                "notify_msg": values['edit_notify_msg']
            };

            $(".user-edit-success").html('<img src="/img/loading2.gif" />');

            $.post("/adminajax", data, function(r) {
                if (r == "ok") {
                    $(".user-edit-success").html("User record saved!");
                    $(".admin-user-edit-notify").fadeOut(500);
                    setTimeout(function() {
                        record_modified = true;
                        _edit_close(<?= $user['id'] ?>);
                    }, 3000);
                }
            });
        }

        function _edit_profile(id) {
            var data = {
                "operation": "user_edit_profile",
                "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
                "id": <?= $user['id'] ?>,
                "role": "<?= $user['role'] ?>"
            };

            $.post("/adminajax", data, function(r) {
                $(".user-view").css({
                    "text-align": "center",
                    "line-height": new_height + "px"
                }).html('<img src="/img/loading.gif" />');

                $.post("/adminajax", data, function(r) {
                    $(".user-view").css({
                        "text-align": "left",
                        "line-height": "normal"
                    }).html(r);
                });
            });
        }

        function inputAnimate(field) {
            $("#" + field).animate({
                padding: "10px 5px"
            }, 500, function() {
                $("#" + field).css({
                    "font-size": "18px"
                });
            });
        }

        function focusoutAnimate(field) {
            $("#" + field).animate({
                padding: "0px"
            }, 500, function() {
                $("#" + field).css({
                    "font-size": "15px"
                });
            });
        }
    </script>
<?php endforeach; ?>