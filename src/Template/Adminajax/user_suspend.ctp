<?php foreach ($users as $user): ?>
    <div class="user-suspend">
        <div class="profile-pic">
            <?php if ($user['profiles']['profile_picture']): ?>
                <img src="/img/users/profile_pictures/<?= $user['profiles']['profile_picture'] ?>" />
            <?php else: ?>
                <img src="/img/users/nopropic.png" />
            <?php endif; ?>
        </div>

        <div class="form">
            <h1><?= $user['home']['firstname'] . ' ' . $user['home']['lastname'] ?></h1>
            <div class="btn">
                <ul>
                    <li><span id="suspend-btn"></span></li>
                    <li><span id="close">Close this Window</span></li>
                </ul>
            </div>
        </div>
    </div>

    <script>
       <?php if (!$user['home']['suspend']): ?>
            var suspend_bool = false;
       <?php else: ?>
            var suspend_bool = true;
       <?php endif; ?>

        if (!suspend_bool) {
            $("#suspend-btn").html("Suspend");
        } else {
            $("#suspend-btn").html("Unsuspend");
        }

        $("#close").on("click", function() {
            _view_close(<?= $user_id ?>);
        });

        $("#suspend-btn").on("click", function() {
            if (!suspend_bool) {
                $(".black-screen").fadeIn(500, function() {
                    $(".suspend-window").slideDown(500, function() {
                        $(".suspend-window").css({
                            "text-align": "center",
                            "line-height": "400px"
                        }).html('<img src="/img/loading.gif" />');

                        var data = {
                            "operation": "suspend_opt",
                            "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
                            "id": <?= $user_id ?>
                        };

                        $.post("/adminajax", data, function(r) {
                            $(".suspend-window").css({
                                "text-align": "left",
                                "line-height": "normal"
                            }).html(r);

                            suspend_bool = true;
                        });
                    });
                });
            } else {
                var data = {
                    "operation": "suspend_user",
                    "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
                    "id": <?= $user_id ?>,
                    "suspend": 0,
                    "suspend_time": ($("#suspend_time").val()) ? $("#suspend_time").val() : null
                };

                $.post("/adminajax", data, function(r) {
                    if (r == "ok") {
                        $("#suspend-btn").html("Suspend");
                        record_modified = true;
                        _close_suspend_modified();
                    }
                });
            }
        });

        function _close_suspend_form() {
            $(".suspend-window").slideUp(500, function() {
                $(".suspend-window").html("");
                $(".black-screen").fadeOut(500);
            });
        }

        function _close_suspend_modified() {
            $(".suspend-window").slideUp(500, function() {
                $(".suspend-window").html("");
                $(".black-screen").fadeOut(500, function() {
                    _view_close(<?= $user_id ?>);
                });
            });
        }
    </script>
<?php endforeach; ?>