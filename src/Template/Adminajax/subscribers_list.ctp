<div class="subscribers_list">
    <div class="list">
        <ul>
        <?php foreach ($subscribers as $subscriber): ?>
            <li><?= $subscriber['email'] ?><span class="small-close" onclick="javascript: _remove(<?= $subscriber['id'] ?>)"></span></li>
        <?php endforeach; ?>
        </ul>
    </div>
</div>

<script>

    function _remove(id) {
        var data = {
            "operation": "remove_subscriber",
            "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
            "id": id
        };

        $.post("/adminajax", data, function(r) {
            _subscribers();
        });
    }
</script>