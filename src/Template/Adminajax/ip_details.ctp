<?php foreach ($logs as $log): ?>
    <div class="ip-details">
        <ul>
            <li>
                <h4>IP Address:</h4>
                <div class="input"><?= $log['query'] ?></div>
            </li>
            <li>
                <h4>Country:</h4>
                <div><?= $log['country'] ?></div>
            </li>
            <li>
                <h4>City:</h4>
                <div class="input"><?= $log['city'] ?></div>
            </li>
            <li>
                <h4>Region:</h4>
                <div class="input"><?= $log['region_name'] ?><div>
            </li>
            <li>
                <h4>Internet Service Provider:</h4>
                <div class="input"><?= $log['isp'] ?></div>
            </li>
            <li>
                <h4>Timezone:</h4>
                <div class="input"><?= $log['timezone'] ?></div>
            </li>
            <li>
                <h4>Zip Code:</h4>
                <div class="input"><?= $log['zip'] ?></div>
            </li>
            <li>
                <h4>Hits/Page Load</h4>
                <div class="input"><?= $log['hits'] ?></div>
            </li>
        </ul>
        <div id="map"></div>
        <div class="btn">
            <span id="close">Close Window</span>
        </div>
    </div>

    <script>
        $("#close").on("click", function() {
            _close();
        });

        function initMap() {
            var myLatLng = {lat: <?= $log['lat'] ?>, lng: <?= $log['lon'] ?>};

            // Create a map object and specify the DOM element
            // for display.
            var map = new google.maps.Map(document.getElementById('map'), {
            center: myLatLng,
            zoom: 15
            });

            // Create a marker and set its position.
            var marker = new google.maps.Marker({
            map: map,
            position: myLatLng,
            title: 'Hello World!'
            });
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDg3sqfQCdmusqPP_M4j40R_xuIBpiUSG8&callback=initMap"
        async defer></script>
<?php endforeach; ?>