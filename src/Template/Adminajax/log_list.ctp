<?php if ($logs): ?>
    <?php foreach ($logs as $log): ?>
        <tr>
            <td><?= strip_tags($log['message']) ?></td>
            <td><?= $log['date'] ?></td>
        </tr>
    <?php endforeach; ?>
<?php else: ?>
    <tr>
        <td colspan="2">There are no logs at this log type at this time.</td>
    </tr>
<?php endif; ?>