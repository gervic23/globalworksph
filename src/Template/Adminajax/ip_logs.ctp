<?php foreach ($logs as $log): ?>
    <tr>
        <td><?= $log['query'] ?></td>
        <td><?= $log['country'] ?></td>
        <td><?= $log['region_name'] ?></td>
        <td><?= $log['hits'] ?></td>
        <td><a href="javascript:" onclick="javascript: _viewIpDetails(<?= $log['id'] ?>)">View More</a></td>
    </tr>
<?php endforeach; ?>

<script>

    $(".ip-log-blackscreen").on("click", function() {
        _close();
    });

    function _viewIpDetails(id) {
        $(".ip-log-blackscreen, .ip-log-view").fadeIn(500, function() {
            $(".ip-log-view .ip-log-content").html('<div class="loading"><img src="/img/loading.gif" /></div>');

            var data = {
                "operation": "ip_log_view_details",
                "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
                "id": id
            };

            $.post("/adminajax", data, function(r) {
                $(".ip-log-view .ip-log-content").html(r);
            });
        });
    }

    function _close() {
        $(".ip-log-blackscreen, .ip-log-view").fadeOut(500, function() {
            $(".ip-log-view .ip-log-content").html("");
        });
    };
</script>