<div class="user-image-crop">
    <h1>Crop Your Picture</h1>
    <div class="form">
        <div class="image">
            <img id="image" alt="crop" src="/img/users/tmp/<?= $filename ?>" />
        </div>
        <div class="btn-container btn-crop">
            <?= $this->Form->button('Crop', ['type' => 'button', 'id' => 'crop']) ?>
        </div>
    </div>
    <div>&nbsp;</div>
</div>

<script>
    $(function() {
        $("#image").cropper();
        
        $("#crop").on("click", function() {
            $(this).hide();
            $("#image").cropper("getCroppedCanvas").toBlob(function(blob) {
                var formData = new FormData();
                
                formData.append("cropperImage", blob);
                formData.append("operation", "crop_image");
                formData.append("_csrfToken", "<?= $this->request->getParam('_csrfToken'); ?>");
                formData.append("user_id", "<?= $user_id ?>");
                formData.append("filename", "<?= $filename ?>");
				
                $.ajax('/adminajax', {
					method: "post",
					data: formData,
					processData: false,
					contentType: false,
					success: function (r) {
                        if (r == "ok") {
                            record_modified = true;
                            _edit_profile(<?= $user_id ?>);
                        }
					},
					error: function () {
					  console.log('Upload error');
					}
				});
            });
        });
    });
</script>