<?php
    use App\Controller\GlobalworksController; 
    
    $globalworks = new GlobalworksController();

    if (!isset($nickname)) {
        $nickname = '';
    }

    if (!isset($profile_picture)) {
        $profile_picture = '';
    }

    if (!isset($job_title)) {
        $job_title = '';
    }

    if (!isset($skills)) {
        $skills = '';
    }

    if (!isset($desire_salary)) {
        $desire_salary = '';
    }

    if (!isset($education)) {
        $education = '';
    }

    if (!isset($experience)) {
        $experience = '';
    }

    if (!isset($employment_status)) {
        $employment_status = '';
    }

    if (!isset($skype_id)) {
        $skype_id = '';
    }

    if (!isset($job_subscribe)) {
        $job_subscribe = '';
    }

    if (!isset($business_name)) {
        $business_name = '';
    }

    if (!isset($business_description)) {
        $business_description = '';
    }

    if (!isset($business_email)) {
        $business_email = '';
    }

    if (!isset($business_contact_number)) {
        $business_contact_number = '';
    }

    if (!isset($business_website)) {
        $business_website = '';
    }

    if (!isset($business_address)) {
        $business_address = '';
    }

    if (!isset($business_state)) {
        $business_state = '';
    }

    if (!isset($business_country)) {
        $business_country = '';
    }

    $countries = [
        '' => 'Select Your Country',
        'AF'=>'AFGHANISTAN',
        'AL'=>'ALBANIA',
        'DZ'=>'ALGERIA',
        'AS'=>'AMERICAN SAMOA',
        'AD'=>'ANDORRA',
        'AO'=>'ANGOLA',
        'AI'=>'ANGUILLA',
        'AQ'=>'ANTARCTICA',
        'AG'=>'ANTIGUA AND BARBUDA',
        'AR'=>'ARGENTINA',
        'AM'=>'ARMENIA',
        'AW'=>'ARUBA',
        'AU'=>'AUSTRALIA',
        'AT'=>'AUSTRIA',
        'AZ'=>'AZERBAIJAN',
        'BS'=>'BAHAMAS',
        'BH'=>'BAHRAIN',
        'BD'=>'BANGLADESH',
        'BB'=>'BARBADOS',
        'BY'=>'BELARUS',
        'BE'=>'BELGIUM',
        'BZ'=>'BELIZE',
        'BJ'=>'BENIN',
        'BM'=>'BERMUDA',
        'BT'=>'BHUTAN',
        'BO'=>'BOLIVIA',
        'BA'=>'BOSNIA AND HERZEGOVINA',
        'BW'=>'BOTSWANA',
        'BV'=>'BOUVET ISLAND',
        'BR'=>'BRAZIL',
        'IO'=>'BRITISH INDIAN OCEAN TERRITORY',
        'BN'=>'BRUNEI DARUSSALAM',
        'BG'=>'BULGARIA',
        'BF'=>'BURKINA FASO',
        'BI'=>'BURUNDI',
        'KH'=>'CAMBODIA',
        'CM'=>'CAMEROON',
        'CA'=>'CANADA',
        'CV'=>'CAPE VERDE',
        'KY'=>'CAYMAN ISLANDS',
        'CF'=>'CENTRAL AFRICAN REPUBLIC',
        'TD'=>'CHAD',
        'CL'=>'CHILE',
        'CN'=>'CHINA',
        'CX'=>'CHRISTMAS ISLAND',
        'CC'=>'COCOS (KEELING) ISLANDS',
        'CO'=>'COLOMBIA',
        'KM'=>'COMOROS',
        'CG'=>'CONGO',
        'CD'=>'CONGO, THE DEMOCRATIC REPUBLIC OF THE',
        'CK'=>'COOK ISLANDS',
        'CR'=>'COSTA RICA',
        'CI'=>'COTE D IVOIRE',
        'HR'=>'CROATIA',
        'CU'=>'CUBA',
        'CY'=>'CYPRUS',
        'CZ'=>'CZECH REPUBLIC',
        'DK'=>'DENMARK',
        'DJ'=>'DJIBOUTI',
        'DM'=>'DOMINICA',
        'DO'=>'DOMINICAN REPUBLIC',
        'TP'=>'EAST TIMOR',
        'EC'=>'ECUADOR',
        'EG'=>'EGYPT',
        'SV'=>'EL SALVADOR',
        'GQ'=>'EQUATORIAL GUINEA',
        'ER'=>'ERITREA',
        'EE'=>'ESTONIA',
        'ET'=>'ETHIOPIA',
        'FK'=>'FALKLAND ISLANDS (MALVINAS)',
        'FO'=>'FAROE ISLANDS',
        'FJ'=>'FIJI',
        'FI'=>'FINLAND',
        'FR'=>'FRANCE',
        'GF'=>'FRENCH GUIANA',
        'PF'=>'FRENCH POLYNESIA',
        'TF'=>'FRENCH SOUTHERN TERRITORIES',
        'GA'=>'GABON',
        'GM'=>'GAMBIA',
        'GE'=>'GEORGIA',
        'DE'=>'GERMANY',
        'GH'=>'GHANA',
        'GI'=>'GIBRALTAR',
        'GR'=>'GREECE',
        'GL'=>'GREENLAND',
        'GD'=>'GRENADA',
        'GP'=>'GUADELOUPE',
        'GU'=>'GUAM',
        'GT'=>'GUATEMALA',
        'GN'=>'GUINEA',
        'GW'=>'GUINEA-BISSAU',
        'GY'=>'GUYANA',
        'HT'=>'HAITI',
        'HM'=>'HEARD ISLAND AND MCDONALD ISLANDS',
        'VA'=>'HOLY SEE (VATICAN CITY STATE)',
        'HN'=>'HONDURAS',
        'HK'=>'HONG KONG',
        'HU'=>'HUNGARY',
        'IS'=>'ICELAND',
        'IN'=>'INDIA',
        'ID'=>'INDONESIA',
        'IR'=>'IRAN, ISLAMIC REPUBLIC OF',
        'IQ'=>'IRAQ',
        'IE'=>'IRELAND',
        'IL'=>'ISRAEL',
        'IT'=>'ITALY',
        'JM'=>'JAMAICA',
        'JP'=>'JAPAN',
        'JO'=>'JORDAN',
        'KZ'=>'KAZAKSTAN',
        'KE'=>'KENYA',
        'KI'=>'KIRIBATI',
        'KP'=>'KOREA DEMOCRATIC PEOPLES REPUBLIC OF',
        'KR'=>'KOREA REPUBLIC OF',
        'KW'=>'KUWAIT',
        'KG'=>'KYRGYZSTAN',
        'LA'=>'LAO PEOPLES DEMOCRATIC REPUBLIC',
        'LV'=>'LATVIA',
        'LB'=>'LEBANON',
        'LS'=>'LESOTHO',
        'LR'=>'LIBERIA',
        'LY'=>'LIBYAN ARAB JAMAHIRIYA',
        'LI'=>'LIECHTENSTEIN',
        'LT'=>'LITHUANIA',
        'LU'=>'LUXEMBOURG',
        'MO'=>'MACAU',
        'MK'=>'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF',
        'MG'=>'MADAGASCAR',
        'MW'=>'MALAWI',
        'MY'=>'MALAYSIA',
        'MV'=>'MALDIVES',
        'ML'=>'MALI',
        'MT'=>'MALTA',
        'MH'=>'MARSHALL ISLANDS',
        'MQ'=>'MARTINIQUE',
        'MR'=>'MAURITANIA',
        'MU'=>'MAURITIUS',
        'YT'=>'MAYOTTE',
        'MX'=>'MEXICO',
        'FM'=>'MICRONESIA, FEDERATED STATES OF',
        'MD'=>'MOLDOVA, REPUBLIC OF',
        'MC'=>'MONACO',
        'MN'=>'MONGOLIA',
        'MS'=>'MONTSERRAT',
        'MA'=>'MOROCCO',
        'MZ'=>'MOZAMBIQUE',
        'MM'=>'MYANMAR',
        'NA'=>'NAMIBIA',
        'NR'=>'NAURU',
        'NP'=>'NEPAL',
        'NL'=>'NETHERLANDS',
        'AN'=>'NETHERLANDS ANTILLES',
        'NC'=>'NEW CALEDONIA',
        'NZ'=>'NEW ZEALAND',
        'NI'=>'NICARAGUA',
        'NE'=>'NIGER',
        'NG'=>'NIGERIA',
        'NU'=>'NIUE',
        'NF'=>'NORFOLK ISLAND',
        'MP'=>'NORTHERN MARIANA ISLANDS',
        'NO'=>'NORWAY',
        'OM'=>'OMAN',
        'PK'=>'PAKISTAN',
        'PW'=>'PALAU',
        'PS'=>'PALESTINIAN TERRITORY, OCCUPIED',
        'PA'=>'PANAMA',
        'PG'=>'PAPUA NEW GUINEA',
        'PY'=>'PARAGUAY',
        'PE'=>'PERU',
        'PH'=>'PHILIPPINES',
        'PN'=>'PITCAIRN',
        'PL'=>'POLAND',
        'PT'=>'PORTUGAL',
        'PR'=>'PUERTO RICO',
        'QA'=>'QATAR',
        'RE'=>'REUNION',
        'RO'=>'ROMANIA',
        'RU'=>'RUSSIAN FEDERATION',
        'RW'=>'RWANDA',
        'SH'=>'SAINT HELENA',
        'KN'=>'SAINT KITTS AND NEVIS',
        'LC'=>'SAINT LUCIA',
        'PM'=>'SAINT PIERRE AND MIQUELON',
        'VC'=>'SAINT VINCENT AND THE GRENADINES',
        'WS'=>'SAMOA',
        'SM'=>'SAN MARINO',
        'ST'=>'SAO TOME AND PRINCIPE',
        'SA'=>'SAUDI ARABIA',
        'SN'=>'SENEGAL',
        'SC'=>'SEYCHELLES',
        'SL'=>'SIERRA LEONE',
        'SG'=>'SINGAPORE',
        'SK'=>'SLOVAKIA',
        'SI'=>'SLOVENIA',
        'SB'=>'SOLOMON ISLANDS',
        'SO'=>'SOMALIA',
        'ZA'=>'SOUTH AFRICA',
        'GS'=>'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS',
        'ES'=>'SPAIN',
        'LK'=>'SRI LANKA',
        'SD'=>'SUDAN',
        'SR'=>'SURINAME',
        'SJ'=>'SVALBARD AND JAN MAYEN',
        'SZ'=>'SWAZILAND',
        'SE'=>'SWEDEN',
        'CH'=>'SWITZERLAND',
        'SY'=>'SYRIAN ARAB REPUBLIC',
        'TW'=>'TAIWAN, PROVINCE OF CHINA',
        'TJ'=>'TAJIKISTAN',
        'TZ'=>'TANZANIA, UNITED REPUBLIC OF',
        'TH'=>'THAILAND',
        'TG'=>'TOGO',
        'TK'=>'TOKELAU',
        'TO'=>'TONGA',
        'TT'=>'TRINIDAD AND TOBAGO',
        'TN'=>'TUNISIA',
        'TR'=>'TURKEY',
        'TM'=>'TURKMENISTAN',
        'TC'=>'TURKS AND CAICOS ISLANDS',
        'TV'=>'TUVALU',
        'UG'=>'UGANDA',
        'UA'=>'UKRAINE',
        'AE'=>'UNITED ARAB EMIRATES',
        'GB'=>'UNITED KINGDOM',
        'US'=>'UNITED STATES',
        'UM'=>'UNITED STATES MINOR OUTLYING ISLANDS',
        'UY'=>'URUGUAY',
        'UZ'=>'UZBEKISTAN',
        'VU'=>'VANUATU',
        'VE'=>'VENEZUELA',
        'VN'=>'VIET NAM',
        'VG'=>'VIRGIN ISLANDS, BRITISH',
        'VI'=>'VIRGIN ISLANDS, U.S.',
        'WF'=>'WALLIS AND FUTUNA',
        'EH'=>'WESTERN SAHARA',
        'YE'=>'YEMEN',
        'YU'=>'YUGOSLAVIA',
        'ZM'=>'ZAMBIA',
        'ZW'=>'ZIMBABWE',
    ];
?>

<div class="admin-user-profile-notify">
    <div class="contents">
        <h1>Please write your message</h1>
        <field>
            <?= $this->Form->textarea('profile-notify', ['id' => 'profile-notify', 'class' => 'notify']) ?>
            <div class="notify-btns">
                <ul>
                    <li><?= $this->Form->button('Notify User and Save', ['id' => 'notify-profile-submit', 'class' => 'notify-btn']) ?></li>
                    <li><?= $this->Form->button('Cancel', ['id' => 'notify-profile-cancel', 'class' => 'notify-btn']) ?></li>
                </ul>
            </div>
        </field>
    </div>
</div>

<div class="user-edit-profile">
    <div class="profile-pic">
        <div class="image">
            <?php if ($profile_picture): ?>
                <img src="/img/users/profile_pictures/<?= $profile_picture ?>" />
            <?php else: ?>
            <img src="/img/users/nopropic.png" />
            <?php endif; ?>
        </div>
        <div class="btns">
            <?php if ($profile_picture): ?>
                <ul>
                    <li><span onclick="javascript: upload_profile_picture(<?= $user_id ?>)">Change Picture</span></li>
                    <li><span onclick="javascript: delete_profile_pic(<?= $user_id ?>)">Delete Picture</span></li>
                </ul>
            <?php else: ?>
                <ul>
                    <li><span onclick="javascript: upload_profile_picture(<?= $user_id ?>)">Upload Profile Picture</span></li>
                </ul>
            <?php endif; ?>
        </div>
    </div>
    <div class="form">
        <div class="input">
            <?= $this->Form->label('nickname', 'Nickname:') ?>
            <?= $this->Form->text('nickname', ['id' => 'nickname', 'class' => 'text-input', 'autocomplete' => 'off', 'value' => $nickname]) ?>
        </div>

        <?php if ($role == 'Applicant'): ?>
            <div class="input">
                <?= $this->Form->label('job_title', 'Job Title:') ?>
                <?= $this->Form->text('job_title', ['id' => 'job_title', 'class' => 'text-input', 'autocomplete' => 'off', 'value' => $job_title]) ?>
            </div>

            <div class="input">
                <?= $this->Form->hidden('experience', ['id' => 'myField', 'value' => $experience]); ?>
                <div class="input">
                    <label for="experience">Experience: <span style="font-size: 13px">Eg. (4 Years in Graphic Arts,) Each experience seperated by pressing coma.</span></label>
                    <div id="experience"></div>
                </div>
            </div>

            <div class="input">
                <?= $this->Form->label('skype_id', 'Skype ID:') ?>
                <?= $this->Form->text('skype_id', ['id' => 'skype_id', 'class' => 'text-input', 'autocomplete' => 'off', 'value' => $skype_id]) ?>
            </div>

            <div class="input">
                <?= $this->Form->label('employment_status', 'Employment Status:') ?>
                <?= $this->Form->select('employment_status', ['' => 'Select Status', 'Unemployed' => 'Unemployed', 'Employed' => 'Employed'], ['id' => 'employment_status', 'class' => 'select-input', 'default' => $employment_status]) ?>
            </div>

            <div class="input">
                <div class="skills">
                    <h3>Skills</h3>
                    <div class="skills-contents">
                        <div class="skills_contents"></div>
                        <div class="btn-container">
                            <span id="add-skill">Add Skills</span>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="input">
                <?= $this->Form->input('desire_salary', ['type' => 'hidden', 'id' => 'desire_salary', 'value' => $desire_salary]) ?>
                <div class="salary">
                    <h3>Desired Salary</h3>
                    <div class="container">
                        <ul>
                            <li>
                                <?= $this->Form->checkbox('salary_range0', ['id' => 'salary_range0', 'value' => 'Php 8000 - Php 10000']); ?>
                                <label for="salary_range0">Php 8,000 - Php 10,000</label>
                            </li>

                            <li>
                                <?= $this->Form->checkbox('salary_range1', ['id' => 'salary_range1', 'value' => 'Php 10000 - Php 15000']); ?>
                                <label for="salary_range1">Php 10,000 - Php 15,000</label>
                            </li>
                        </ul>
                    </div>
                    <div class="container">
                        <ul>
                            <li>
                                <?= $this->Form->checkbox('salary_range2', ['id' => 'salary_range2', 'value' => 'Php 15000 - Php 20000']); ?>
                                <label for="salary_range2">Php 15,000 - Php 20,000</label>
                            </li>
                            <li>
                                <?= $this->Form->checkbox('salary_range3', ['id' => 'salary_range3', 'value' => 'Php 20000 - Php 25000']); ?>
                                <label for="salary_range3">Php 20,000 - Php 25,000</label>
                            </li>
                        </ul>
                    </div>
                    <div class="container">
                        <ul>
                            <li>
                                <?= $this->Form->checkbox('salary_range4', ['id' => 'salary_range4', 'value' => 'Php 25000 - Php 30000']); ?>
                                <label for="salary_range4">Php 25,000 - Php 30,000</label>
                            </li>
                            <li>
                                <?= $this->Form->checkbox('salary_range5', ['id' => 'salary_range5', 'value' => 'Php 30000 - Php 35000']); ?>
                                <label for="salary_range5">Php 30,000 - Php 35,000</label>
                            </li>
                        </ul>
                    </div>
                    <div class="container">
                        <ul>
                            <li>
                                <?= $this->Form->checkbox('salary_range6', ['id' => 'salary_range6', 'value' => 'Php 35000 - Php 40000']); ?>
                                <label for="salary_range6">Php 35,000 - Php 40,000</label>
                            </li>
                            <li>
                                <?= $this->Form->checkbox('salary_range7', ['id' => 'salary_range7', 'value' => 'Php 40000 - Php 45000']); ?>
                                <label for="salary_range7">Php 40,000 - Php 45,000</label>
                            </li>
                        </ul>
                    </div>
                    <div class="container">
                        <ul>
                            <li>
                                <?= $this->Form->checkbox('salary_range8', ['id' => 'salary_range8', 'value' => 'Php 45000 - Php 50000']); ?>
                                <label for="salary_range8">Php 45,000 - Php 50,000</label>
                            </li>
                            <li>
                                <?= $this->Form->checkbox('salary_range9', ['id' => 'salary_range9', 'value' => 'Morethan Php 50000']); ?>
                                <label for="salary_range9">Morethan Php 50,000</label>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="input">
                    <div class="education">
                    <h3>Education Attainment</h3>
                    <div class="content">
                        <ul>
                            <li>
                                <input type="radio" name="education" id="education-1" class="r_education" value="College/Bachelor Degree" <?= ($education == 'College/Bachelor Degree') ? 'checked' : ''; ?> />
                                <label for="education-1">College/Bachelor Degree</label>
                                <div class="check"></div>
                            </li>
                            <li>
                                <input type="radio" name="education" id="education-2" class="r_education" value="College/Associated Degree" <?= ($education == 'College/Associated Degree') ? 'checked' : '' ?> />
                                <label for="education-2">College/Associated Degree</label>
                                <div class="check"></div>
                            </li>
                            <li>
                                <input type="radio" name="education" id="education-3" class="r_education" value="Post Graduate Degree" <?= ($education == 'Post Graduate Degree') ? 'checked' : ''; ?> />
                                <label for="education-3">Post Graduate Degree</label>
                                <div class="check"></div>
                            </li>
                            <li>
                                <input type="radio" name="education" id="education-4" class="r_education" value="Highschool Graduate" <?= ($education == 'Highschool Graduate') ? 'checked' : ''; ?> />
                                <label for="education-4">Highschool Graduate</label>
                                <div class="check"></div>
                            </li>
                            <li>
                                <input type="radio" name="education" id="education-5" class="r_education" value="Not Graduated Highschool" <?= ($education == 'Not Graduated Highschool') ? 'checked' : ''; ?> />
                                <label for="education-5">Not Graduated Highschool</label>
                                <div class="check"></div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="input">
                <div class="subscribe">
                    <?= $this->Form->hidden('job_subscribe', ['id' => 'job_subscribe']); ?>
                    <input type="checkbox" name="j_subscribe" id="j_subscribe" />
                    <label for="j_subscribe">Subscribe for latest jobs.</label>
                    <div class="check"></div>
                </div>
            </div>
        <?php elseif ($role == 'Employer'): ?>
            <div class="input">
                <?= $this->Form->label('business_name', 'Business Name:') ?>
                <?= $this->Form->text('business_name', ['id' => 'business_name', 'class' => 'text-input', 'autocomplete' => 'off', 'value' => $business_name]) ?>
            </div>

            <div class="input">
                <?= $this->Form->label('business_description', 'Business Description:') ?>
                <?= $this->Form->textarea('business_description', ['id' => 'business_description', 'class' => 'textarea-input', 'value' => $business_description]) ?>
            </div>
            
            <div class="input">
                <?= $this->Form->label('business_email', 'Business Email:') ?>
                <?= $this->Form->text('business_email', ['id' => 'business_email', 'class' => 'text-input', 'autocomplete' => 'off', 'value' => $business_email]) ?>
            </div>

            <div class="input">
                <?= $this->Form->label('business_contact_number', 'Business Contact Number:') ?>
                <?= $this->Form->text('business_contact_number', ['id' => 'business_contact_number', 'class' => 'text-input', 'autocomplete' => 'off', 'value' => $business_contact_number]) ?>
            </div>

            <div class="input">
                <?= $this->Form->label('business_website', 'Business Website:') ?>
                <?= $this->Form->text('business_website', ['id' => 'business_website', 'class' => 'text-input', 'autocomplete' => 'off', 'value' => $business_website]) ?>
            </div>

            <div class="input">
                <?= $this->Form->label('business_address', 'Business Address:') ?>
                <?= $this->Form->textarea('business_address', ['id' => 'business_address', 'class' => 'textarea-input', 'value' => $business_address]) ?>
            </div>

            <div class="input">
                <?= $this->Form->label('business_state', 'Business State/Region:') ?>
                <?= $this->Form->text('business_state', ['id' => 'business_state', 'class' => 'text-input', 'autocomplete' => 'off', 'value' => $business_state]) ?>
            </div>

            <div class="input">
                <?= $this->Form->label('business_country', 'Business Country:') ?>
                <?= $this->Form->select('business_country', $countries, ['id' => 'business_country', 'class' => 'select-input', 'default' => $business_country]) ?>
            </div>
        <?php endif; ?>
        <div class="btn">
            <ul>
                <li><span id="save">Save Profile</span></li>
                <li><span id="goback">Go Back</span></li>
            </ul>
        </div>
        <div class="flash"></div>
    </div>
</div>

<?= $this->Html->script('add-skills') ?>
<script>
    $("#goback").on("click", function() {
        _edit(<?= $user_id ?>);
    });

    $("#save").on("click", function() {
        var c = confirm("Would you like to notify the user about the changes?");
        var values = {
            "has_notify": 0,
            "notify_msg": ""
        };

        if (c) {
            values['has_notify'] = 1;
            $(".admin-user-profile-notify").fadeIn(500);
            $("#notify-profile-cancel").on("click", function() {
                values['has_notify'] = 0;
                $(".admin-user-profile-notify").fadeOut(500);
            });
            $("#notify-profile-submit").on("click", function() {
                if ($("#profile-notify").val() == "") {
                    alert("Please write your message.");
                } else {
                    values['notify_msg'] = $("#profile-notify").val();
                    save_profile(values);
                }
            });
        } else {
            save_profile(values);
        }
    });

    function save_profile(values) {
        var education = '';
        var skills = '';

        $(".r_education").each(function() {
            if ($(this).is(":checked")) {
                education = $(this).val();
            }
        });

        if ($(".skill-entry").length > 0) {
            var edu = [];

            $(".skill-entry").each(function() {
                edu.push($(this).val());
            });

            skills = JSON.stringify(edu);
        }

        var data = {
            "operation": "user_profile_save",
            "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
            "user_id":  <?= $user_id ?>,
            "nickname": ($("#nickname").val()) ? $("#nickname").val() : "",
            "job_title": ($("#job_title").val()) ? $("#job_title").val() : "",
            "experience": ($(".s_tags").val()) ? $(".s_tags").val() : "",
            "skype_id": ($("#skype_id").val()) ? $("#skype_id").val() : "",
            "employment_status": $("#employment_status").val(),
            "desire_salary": ($("#desire_salary").val()) ? $("#desire_salary").val() : "",
            "skills": skills,
            "education": education,
            "job_subscribe": $("#job_subscribe").val(),
            "business_name": ($("#business_name").val()) ? $("#business_name").val() : "",
            "business_description": ($("#business_description").val()) ? $("#business_description").val() : "",
            "business_email": ($("#business_email").val()) ? $("#business_email").val() : "",
            "business_contact_number": ($("#business_contact_number").val()) ? $("#business_contact_number").val() : "",
            "business_website": ($("#business_website").val()) ? $("#business_website").val() : "",
            "business_address": ($("#business_address").val()) ? $("#business_address").val() : "",
            "business_state": ($("#business_state").val()) ? $("#business_state").val() : "",
            "business_country": ($("#business_country").val()) ? $("#business_country").val() : "",
            "has_notification": values['has_notify'],
            "notification_msg": values['notify_msg']
        };

        $(".flash").html('<img src="/img/loading2.gif" />');

        $.post("/adminajax", data, function(r) {
            $(".flash").html("Profile Saved!");

            setTimeout(function() {
                _edit(<?= $user_id ?>)
            }, 3000);
        });
    }

     $("#experience").shogotags({
        tag_border_color: "#ff4338",
        tag_background_color: "#ff4338",
        tag_border_radius: "5px",
        data: "<?= $experience ?>"
    });

    <?php if ($skills): ?>
        <?php foreach (json_decode($skills, true) as $skill): ?>
            skill_add("<?= $skill ?>");
        <?php endforeach; ?>
    <?php endif; ?>

    <?php if ($desire_salary): $c = 0; ?>
        var salary_range = [];
        <?php foreach (explode(',', $desire_salary) as $salary): ?>
            salary_range[<?= $c ?>] = "<?= $salary; ?>";
        <?php $c++; endforeach; ?>

        <?php if (in_array('Php 8000 - Php 10000', explode(',', $desire_salary) )): ?>
            $("#salary_range0").attr("checked", "checked");
        <?php endif; ?>
    
        <?php if (in_array('Php 10000 - Php 15000', explode(',', $desire_salary) )): ?>
            $("#salary_range1").attr("checked", "checked");
        <?php endif; ?>
    
        <?php if (in_array('Php 15000 - Php 20000', explode(',', $desire_salary) )): ?>
            $("#salary_range2").attr("checked", "checked");
        <?php endif; ?>
    
        <?php if (in_array('Php 20000 - Php 25000', explode(',', $desire_salary) )): ?>
            $("#salary_range3").attr("checked", "checked");
        <?php endif; ?>
    
        <?php if (in_array('Php 25000 - Php 30000', explode(',', $desire_salary) )): ?>
            $("#salary_range4").attr("checked", "checked");
        <?php endif; ?>
    
        <?php if (in_array('Php 30000 - Php 35000', explode(',', $desire_salary) )): ?>
            $("#salary_range5").attr("checked", "checked");
        <?php endif; ?>
    
        <?php if (in_array('Php 35000 - Php 40000', explode(',', $desire_salary) )): ?>
            $("#salary_range6").attr("checked", "checked");
        <?php endif; ?>
    
        <?php if (in_array('Php 40000 - Php 45000', explode(',', $desire_salary) )): ?>
            $("#salary_range7").attr("checked", "checked");
        <?php endif; ?>
    
        <?php if (in_array('Php 45000 - Php 50000', explode(',', $desire_salary) )): ?>
            $("#salary_range8").attr("checked", "checked");
        <?php endif; ?>
    
        <?php if (in_array('Morethan Php 50000', explode(',', $desire_salary) )): ?>
            $("#salary_range9").attr("checked", "checked");
        <?php endif; ?>
    <?php else: ?>
        var salary_range = [];
    <?php endif; ?>

    $("#salary_range0").on("change", function() {
        var range = $(this).val();
        if ($(this).is(":checked")) {
            salary_range.push(range);
            addSalaryRange();
        } else {
            removeSalaryRange(salary_range.indexOf(range));
        }
    });
    
    $("#salary_range1").on("change", function() {
        var range = $(this).val();
        if ($(this).is(":checked")) {
            salary_range.push(range);
            addSalaryRange();
        } else {
            removeSalaryRange(salary_range.indexOf(range));
        }
    });
    
    $("#salary_range2").on("change", function() {
        var range = $(this).val();
        if ($(this).is(":checked")) {
            salary_range.push(range);
            addSalaryRange();
        } else {
            removeSalaryRange(salary_range.indexOf(range));
        }
    });
    
    $("#salary_range3").on("change", function() {
        var range = $(this).val();
        if ($(this).is(":checked")) {
            salary_range.push(range);
            addSalaryRange();
        } else {
            removeSalaryRange(salary_range.indexOf(range));
        }
    });
    
    $("#salary_range4").on("change", function() {
        var range = $(this).val();
        if ($(this).is(":checked")) {
            salary_range.push(range);
            addSalaryRange();
        } else {
            removeSalaryRange(salary_range.indexOf(range));
        }
    });
    
    $("#salary_range5").on("change", function() {
        var range = $(this).val();
        if ($(this).is(":checked")) {
            salary_range.push(range);
            addSalaryRange();
        } else {
            removeSalaryRange(salary_range.indexOf(range));
        }
    });
    
    $("#salary_range6").on("change", function() {
        var range = $(this).val();
        if ($(this).is(":checked")) {
            salary_range.push(range);
            addSalaryRange();
        } else {
            removeSalaryRange(salary_range.indexOf(range));
        }
    });
    
    $("#salary_range7").on("change", function() {
        var range = $(this).val();
        if ($(this).is(":checked")) {
            salary_range.push(range);
            addSalaryRange();
        } else {
            removeSalaryRange(salary_range.indexOf(range));
        }
    });
    
    $("#salary_range8").on("change", function() {
        var range = $(this).val();
        if ($(this).is(":checked")) {
            salary_range.push(range);
            addSalaryRange();
        } else {
            removeSalaryRange(salary_range.indexOf(range));
        }
    });
    
    $("#salary_range9").on("change", function() {
        var range = $(this).val();
        if ($(this).is(":checked")) {
            salary_range.push(range);
            addSalaryRange();
        } else {
            removeSalaryRange(salary_range.indexOf(range));
        }
    });

    <?php if ($job_subscribe == 1): ?>
        $("#jobs_subscribe").attr("value", <?= $job_subscribe ?>);
        $("#j_subscribe").attr("checked", "checked");
    <?php endif; ?>

    $("#j_subscribe").on("change", function() {
        if ($(this).is(":checked")) {
            $("#job_subscribe").attr("value", 1);
        } else {
            $("#job_subscribe").attr("value", 0);
        }
    });

    function addSalaryRange() {
        $("#desire_salary").attr("value", salary_range);
    }
    
    function removeSalaryRange(index) {
        range = salary_range[index];
        salary_range.splice(index, 1);
        addSalaryRange();
    }

    function upload_profile_picture(id) {
        var data = {
            "operation": "user_upload_profile_pic",
            "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
            "id": id
        };

        $.post("/adminajax", data, function(r) {
            $(".user-view").css({
                "text-align": "center",
                "line-height": new_height + "px"
            }).html('<img src="/img/loading.gif" />');

            $.post("/adminajax", data, function(r) {
                $(".user-view").css({
                    "text-align": "left",
                    "line-height": "normal"
                }).html(r);
            });
        });
    }

    function delete_profile_pic(id) {
        var c = confirm("Are you sure you want to delete this profile picture?");

        if (c) {
            var data = {
                "operation": "user_delete_profile_pic",
                "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
                "user_id": id
            };

            $.post("/adminajax", data, function(r) {
                if (r == "ok") {
                    record_modified = true;
                    _edit_profile(id);
                }
            });
        }
    }
</script>
