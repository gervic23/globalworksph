<div class="user-suspend-form">
    <h1>Leave Suspend Time Blank means permanent suspension.</h1>
    <div class="input">
        <?= $this->Form->label('suspend_time', 'Suspend Time:'); ?>
        <?= $this->Form->text('suspend_time', ['id' => 'suspend_time', 'autocomplete' => 'off']) ?>
    </div>
    <div class="btn">
        <ul>
            <li><span id="_suspend">Suspend</span></li>
            <li><span id="_close">Close this Window</span></li>
        </ul>
    </div>
</div>

<div class="admin-suspend-del-notify">
    <div class="contents">
        <h1>Please write your message</h1>
        <field>
            <?= $this->Form->textarea('del-edit-notify', ['id' => 'suspend-notify', 'class' => 'notify']) ?>
            <div class="notify-btns">
                <ul>
                    <li><?= $this->Form->button('Notify User and Save', ['id' => 'notify-suspend-submit', 'class' => 'notify-btn']) ?></li>
                    <li><?= $this->Form->button('Cancel', ['id' => 'notify-edit-cancel', 'class' => 'notify-btn']) ?></li>
                </ul>
            </div>
        </field>
    </div>
</div>

<script>
    var date = new Date();
    var hours = date.getHours();
    var minute = date.getMinutes();
    var second = (date.getSeconds().length !== 1) ? date.getSeconds() : "0" + date.getSeconds();

    $("#suspend_time").datepicker({dateFormat: "yy-mm-dd " + hours + ":" + minute + ":" + second});

    $("#_close, .black-screen").on("click", function() {
        _close_suspend_form();
    });

    $("#_suspend").on("click", function() {
        var c = confirm("Would you like to notify user via email?");
        var values = {
            "has_notify": 0,
            "notify_msg": ""
        };

        if (c) {
            values['has_notify'] = 1;
            $(".admin-suspend-del-notify").show();
            $("#notify-edit-cancel").on("click", function() {
                values['has_notify'] = 0;
                $(".admin-suspend-del-notify").fadeOut(500);
            });
            $("#notify-suspend-submit").on("click", function() {
                if ($("#suspend-notify").val() == "") {
                    alert("Please write your message.");
                } else {
                    values["notify_msg"] = $("#suspend-notify").val();
                    __suspend(values);
                }
            });
        } else {
            __suspend(values);
        }
    });

    function __suspend(values) {
        var data = {
            "operation": "suspend_user",
            "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
            "id": <?= $user_id ?>,
            "suspend": 1,
            "suspend_time": ($("#suspend_time").val()) ? $("#suspend_time").val() : null,
            "has_notify": values['has_notify'],
            "notify_msg": values['notify_msg']
        };

        $.post("/adminajax", data, function(r) {
            if (r == "ok") {
                record_modified = true;
                $(".admin-suspend-del-notify").fadeOut(500);
                _close_suspend_modified();
            }
        });
    }
</script>