<?php use App\Controller\GlobalworksController; $globalworks = new GlobalworksController(); ?>

<?php foreach ($users as $user): ?>
    <div class="user-pic">
        <?php if ($user['profiles']['profile_picture']): ?>
            <img src="/img/users/profile_pictures/<?= $user['profiles']['profile_picture'] ?>" />
        <?php else: ?>
            <img src="/img/users/nopropic.png" />
        <?php endif; ?>
    </div>
    <div class="user-info">
        <table celllpadding="0" cellspacing="0">
            <tbody>
                <tr>
                    <td>ID:</td>
                    <td><?= $user['home']['id'] ?></td>
                </tr>
                <tr>
                    <td>Fullname:</td>
                    <td><?= $user['home']['firstname'] . ' ' . $user['home']['lastname'] ?></td>
                </tr>
                <tr>
                    <td>Email:</td>
                    <td><?= $user['home']['email'] ?></td>
                </tr>
                <tr>
                    <td>Role:</td>
                    <td><?= ($user['home']['role'] == 'Applicant') ? 'Candidate' : $user['home']['role']; ?></td>
                </tr>
                <tr>
                    <td>Gender:</td>
                    <td><?= $user['home']['gender'] ?></td>
                </tr>
                <tr>
                    <td>Birthday:</td>
                    <td><?= $user['home']['birthday'] ?></td>
                </tr>
                <tr>
                    <td>Address:</td>
                    <td><?= $user['home']['address'] ?></td>
                </tr>
                <tr>
                    <td>State/Region:</td>
                    <td><?= $user['home']['region'] ?></td>
                </tr>
                <tr>
                    <td>Country:</td>
                    <td><?= $globalworks->country_to_str($user['home']['country']) ?></td>
                </tr>
                <tr>
                    <td>Contact Number:</td>
                    <td><?= $user['home']['contact_number'] ?></td>
                </tr>
                <tr>
                    <td>Current IP:</td>
                    <td><?= $user['home']['current_ip'] ?></td>
                </tr>
                <tr>
                    <td>Registered IP:</td>
                    <td><?= $user['home']['registered_ip'] ?></td>
                </tr>
                <tr>
                    <td>Date Created:</td>
                    <td><?= $globalworks->datetimetodate($user['home']['date_created']) ?></td>
                </tr>
                <tr>
                    <td>Last Login:</td>
                    <td><?= $globalworks->datetimetodate($user['home']['last_login']); ?></td>
                </tr>
                <tr>
                    <td>Email Confirmed:</td>
                    <td><?= ($user['home']['confirmation_code']) ? 'No' : 'Yes'; ?></td>
                </td>
                <tr>
                    <td>Facebook User:</td>
                    <td><?= ($user['home']['fb_user']) ? 'Yes' : 'No'; ?></td>
                </tr>

                <?php if ($user['home']['role'] == 'Applicant'): ?>
                    <?php if ($user['profiles']['nickname']): ?>
                        <tr>
                            <td>Nickname:</td>
                            <td><?= $user['profiles']['nickname'] ?></td>
                        </tr>
                    <?php endif; ?>
                    <?php if ($user['profiles']['job_title']): ?>
                        <tr>
                            <td>Job Title:</td>
                            <td><?= $user['profiles']['job_title'] ?></td>
                        </tr>
                    <?php endif; ?>
                    <?php if ($user['profiles']['skills']): ?>
                        <tr>
                            <td>Skills</td>
                            <td>
                                <ul>
                                    <?php foreach (json_decode($user['profiles']['skills'], true) as $skill): ?>
                                        <li><?= $skill ?></li>
                                    <?php endforeach; ?>
                                </ul>
                            </td>
                        </tr>
                    <?php endif; ?>
                    <?php if ($user['profiles']['desire_salary']): ?>
                        <tr>
                            <td>Desired Salary:</td>
                            <td>
                                <?php $salary = explode(',',$user['profiles']['desire_salary']) ?>
                                <ul>
                                    <?php foreach ($salary as $s): ?>
                                        <li><?= $s ?></li>
                                    <?php endforeach; ?>
                                </ul>
                            </td>
                        </tr>
                    <?php endif; ?>
                    <?php if ($user['profiles']['education']): ?>
                        <tr>
                            <td>Education:</td>
                            <td><?= $user['profiles']['education'] ?></td>
                        </tr>
                    <?php endif; ?>
                    <?php if ($user['profiles']['experience']): ?>
                        <tr>
                            <td>Experience:</td>
                            <td>
                                <?php $experience = explode(',', $user['profiles']['experience']); ?>
                                <ul>
                                    <?php foreach ($experience as $exp): ?>
                                        <li><?= $exp ?></li>
                                    <?php endforeach; ?>
                                </ul>
                            </td>
                        </tr>
                    <?php endif; ?>
                    <?php if ($user['profiles']['employment_status']): ?>
                        <tr>
                            <td>Employment Status:</td>
                            <td><?= $user['profiles']['employment_status'] ?></td>
                        </tr>
                    <?php endif; ?>
                    <?php if ($user['profiles']['skype_id']): ?>
                        <tr>
                            <td>Skype ID:</td>
                            <td><?= $user['profiles']['skype_id'] ?></td>
                        </tr>
                    <?php endif; ?>
                    <?php if ($user['profiles']['job_subscribe']): ?>
                        <tr>
                            <td>Job Subscription:</td>
                            <td><?= ($user['profiles']['job_subscribe']) ? 'Yes' : 'No'; ?></td>
                        </tr>
                    <?php endif; ?>
                    <?php if ($user['resumes']['filename'] && $user['resumes']['id']): ?>
                        <tr>
                            <td><?= $user['home']['firstname'] . ' ' . $user['home']['lastname'] ?>'s Resume:</td>
                            <td>
                                <div class="resume-row">
                                    <a href="/download/<?= $user['resumes']['id'] ?>"><img src="/img/resume-icon.png" /></a> <a href="/download/<?= $user['resumes']['id'] ?>"><span id="orig-filename"></span></a>
                                </div>
                            </td>
                        </tr>
                    <?php endif; ?>
                <?php endif; ?>
                
                <?php if ($user['home']['role'] == 'Employer'): ?>
                    <?php if ($user['profiles']['business_name']): ?>
                        <tr>
                            <td>Business Name:</td>
                            <td><?= $user['profiles']['business_name'] ?></td>
                        </tr>
                    <?php endif; ?>
                    <?php if ($user['profiles']['business_description']): ?>
                        <tr>
                            <td>Business Description:</td>
                            <td><?= $user['profiles']['business_description'] ?></td>
                        </tr>
                    <?php endif; ?>
                    <?php if ($user['profiles']['business_email']): ?>
                        <tr>
                            <td>Business Email:</td>
                            <td><?= $user['profiles']['business_email'] ?></td>
                        </tr>
                    <?php endif; ?>
                    <?php if ($user['profiles']['business_contact_number']): ?>
                        <tr>
                            <td>Business Contact Number:</td>
                            <td><?= $user['profiles']['business_contact_number'] ?></td>
                        </tr>
                    <?php endif; ?>
                    <?php if ($user['profiles']['business_address']): ?>
                        <tr>
                            <td>Business Address:</td>
                            <td><?= $user['profiles']['business_address'] ?></td>
                        </tr>
                    <?php endif; ?>
                    <?php if ($user['profiles']['business_state']): ?>
                        <tr>
                            <td>Business State/Region:</td>
                            <td><?= $user['profiles']['business_state'] ?></td>
                        </tr>
                    <?php endif; ?>
                    <?php if ($user['profiles']['business_country']): ?>
                        <tr>
                            <td>Business Country:</td>
                            <td><?= $user['profiles']['business_country'] ?></td>
                        </tr>
                    <?php endif; ?>
                <?php endif; ?>
            </tbody>
        </table>
        <div class="btns">
            <ul>
                <li><span onclick="javascript: _edit(<?= $user['home']['id'] ?>)">Edit</span></li>
                <li><span onclick="javascript: change_role(<?= $user['home']['id'] ?>)">Change Role</span></li>
                <li><span onclick="javascript: _suspend(<?= $user['home']['id'] ?>)">Suspend</span></li>
                <li><span onclick="javascript: _delete(<?= $user['home']['id'] ?>)">Delete</span></li>
                <li><span onclick="javascript: _view_close()">Close Window</span></li>
            </ul>
        </div>
    </div>

    <script>
        <?php if ($user['resumes']['orig_filename']): ?>
            $("#orig-filename").html( _split_end("<?= $user['resumes']['orig_filename'] ?>", ".", 20));

            function _split_end(str, splitter, len) {
                var str_split = str.split(splitter);
                var last = str_split.length - 1;

                var new_string = '';

                for (var i = 0; i < last; i++) {
                    new_string += str_split[i] + splitter;
                }

                if (new_string.length > len) {
                    return new_string.substring(0, len) + ".." + str_split[last];
                }

                return new_string + str_split[last];
            }
        <?php endif; ?>
    </script>
<?php endforeach; ?>