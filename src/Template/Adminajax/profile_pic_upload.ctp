<div class="admin-upload-pic">
    <div class="progress">
        <span class="admin-setup-profile-pic">Upload Your Profile Picture</span>
        <span class="admin-crop-profile-pic">Crop Your Profile Picture</span>
        <div class="clear"></div>
    </div>
    <div class="uploader"></div>
    <div class="clear"></div>
    <div class="btn">
        <span id="close">Close This Window</span>
    </div>
</div>

<script>

    resizeUploadPic();

    $(window).resize(function() {
        resizeUploadPic();
    });

    $("#close").on("click", function() {
        $(".upload-profile-pic-view").animate({
            top: "-100%"
        }, 500, function() {
            $(this).html("");
            $(".main-header").show();
            $("body").css({
                "overflow-y": "auto"
            });
        });
    });

    //$(".applicants-upload-pic .uploader").dragdropupload();

    $(".admin-upload-pic .uploader").dragdropupload('/adminajax', {
        label: "Drag Image file here or click here to browse image.",
        width: "80%",
        height: "300px",
        border_size: "3px",
        background_color: "#e3f8fd",
        label_font_size: "130%",
        error_font_size: "100%",
        post_data: {
            operation: "image_upload",
            _csrfToken: "<?= $this->request->getParam('_csrfToken'); ?>",
        },
        validation: {
            file_types: {
                types: ["image/jpeg", "image/png"],
                validation_message: "is not an image file. Only jpeg and png formats are allowed."
            },
            file_sizes: {
                max: {
                    size: 1024,
                    validation_message: "size to large."
                }
            },
            image_dimension: {
                min: {
                    width: 370,
                    height: 370,
                    validation_message: "dimension should be 370x370 or higher."
                }
            }
        }
    }, function(r) {
        if (r == "ok") {
            var data = {
                "operation": "crop_admin_profile_picture",
                "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>"
            };

            var new_height2 = new_height / 2 + 300;

            $(".upload-profile-pic-view").css({
                "line-height": new_height2 + "px",
                "text-align": "center"
            }).html("<img src='/img/loading.gif' />");
            
            $.post("/adminajax", data, function(r) {
                $(".upload-profile-pic-view").css({
                    "line-height": "normal",
                    "text-align": "left"
                }).html(r);
            });
        }
    });
    
    function resizeUploadPic() {
        var height = $(window).height();
        var new_height = height - 25;
        var upload_pic = $(".admin-upload-pic").height();
        
        if (new_height > upload_pic) {
            $(".admin-upload-pic").css({
                "height": new_height + "px"
            });
        }
    }
</script>