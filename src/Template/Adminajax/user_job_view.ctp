<?php use App\Controller\GlobalworksController; $globalworks = new GlobalworksController(); ?>

<div class="admin-jobposts-notify-delete">
    <div class="contents">
        <h1>Please write your message</h1>
        <field>
            <?= $this->Form->textarea('del-notify', ['id' => 'del-notify', 'class' => 'notify']) ?>
            <div class="notify-btns">
                <ul>
                    <li><?= $this->Form->button('Notify User and Delete', ['id' => 'del-notify-submit', 'class' => 'notify-btn']) ?></li>
                    <li><?= $this->Form->button('Cancel', ['id' => 'del-notify-cancel', 'class' => 'notify-btn']) ?></li>
                </ul>
            </div>
        </field>
    </div>
</div>

<div class="admin-jobopostview-btn">
    <span id="close">Close This Window</span>
</div>

<?php foreach ($jobposts as $post): ?>
    <div class="admin-jobpostview">
        <div class="em-jobpost-main-header">
            <div class="header-logo">
                <?php if ($post['jobposts']['header_image']): ?>
                    <div class="header">
                        <img src="/img/users/job_header/<?= $post['jobposts']['header_image'] ?>" />
                    </div>
                <?php else: ?>
                    <script>
                        $(".employers-jobpostview .logo").css({
                            "margin-top": "0px",
                            "position": "inherit"
                        });
                    </script>
                <?php endif; ?>

                <?php if ($post['jobposts']['company_logo']): ?>
                    <div class="logo">
                        <img src="/img/users/company_logo/<?= $post['jobposts']['company_logo'] ?>" />
                    </div>
                <?php endif; ?>
            </div>
            
            <h1><?= $post['jobposts']['job_title'] ?></h1>
            <h4><?= $post['jobposts']['company_name'] ?></h4>
        </div>
        
        <div class="contents">
            <div class="left">
                <div class="description">
                    <h2>Job Description</h2>
                    <?= $globalworks->filter_description($post['jobposts']['job_description']) ?>
                </div>
                
                <?php if ($post['jobposts']['job_website']): ?>
                    <div class="website">
                        <h2>Company Website</h2>
                        <?php
                            $str1 = substr($post['jobposts']['job_website'], 0, 7) ;
                            $str2 = substr($post['jobposts']['job_website'], 0, 8) ;

                            if ($str1 == 'http://' || $str2 == 'https://') {
                                echo '<a target="_blank" href="' . $post['jobposts']['job_website'] . '">' . $post['jobposts']['job_website'] . '</a>';
                            } else {
                                echo '<a target="_blank" href="http://' . $post['jobposts']['job_website'] . '">http://' . $post['jobposts']['job_website'] . '</a>';
                            }
                        ?>
                    </div>
                <?php endif; ?>
            </div>
            <div class="right">
                <div class="job-details">
                    <h2>Job Details</h2>
                    <table>
                        <tr>
                            <td>Job Category:</td>
                            <td><?= $post['jobposts']['job_category'] ?></td>
                        </tr>
                        <tr>
                            <td>Job Type:</td>
                            <td><?= $post['jobposts']['job_type'] ?></td>
                        </tr>
                        <tr>
                            <td>Job Base:</td>
                            <td><?= $globalworks->country_to_str($post['jobposts']['country']) ?></td>
                        </tr>
                        <tr>
                            <td>Working Experience:</td>
                            <td><?= $post['jobposts']['job_years_experience'] ?></td>
                        </tr>
                        <tr>
                            <td>Posted By:</td>
                            <td><?= $post['users']['firstname'] . ' ' . $post['users']['lastname'] ?></td>
                        </tr>
                        <tr>
                            <td>Closing Date:</td>
                            <td><?= $globalworks->datetimetodate($post['jobposts']['closing_date']) ?></td>
                        </tr>
                        <tr>
                            <td>Created At:</td>
                            <td><?= $globalworks->when($post['jobposts']['created_at']) ?></td>
                        </tr>
                        <tr>
                            <td>Modified At:</td>
                            <td><?= $globalworks->when($post['jobposts']['modified_at']) ?></td>
                        </tr>
                    </table>
                </div>
                <?php if ($post['jobposts']['job_email'] || $post['jobposts']['job_contact_number']): ?>
                    <div class="contact-info">
                        <h2>Contact Information</h2>
                        <table>
                            <?php if ($post['jobposts']['job_email']): ?>
                                <tr>
                                    <td>Email:</td>
                                    <td><?= $post['jobposts']['job_email'] ?></td>
                                </tr>
                            <?php endif; ?>

                            <?php if ($post['jobposts']['job_contact_number']): ?>
                                <tr>
                                    <td>Contact Number:</td>
                                    <td><?= $post['jobposts']['job_contact_number'] ?></td>
                                </tr>
                            <?php endif; ?>
                        </table>
                    </div>
                <?php endif; ?>
                
                <?php if ($post['jobposts']['job_location']): ?>
                    <div class="location">
                        <h2>Location</h2>
                        <?= $post['jobposts']['job_location'] ?>
                        <div class="country"><?= $globalworks->country_to_str($post['jobposts']['country']) ?>.</div>
                    </div>
                <?php endif; ?>

                <?php if ($post['jobposts']['googglemap']): $location = urlencode($post['jobposts']['job_location'] . ' ' . $post['jobposts']['country']); ?>
                    <div class="googlemap">
                        <h2>Map</h2>
                        <iframe
                            frameborder="0" style="border:0"
                            src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDg3sqfQCdmusqPP_M4j40R_xuIBpiUSG8
                            &q='<?= $location ?>" allowfullscreen>
                        </iframe>
                    </div>
                <?php endif; ?>
                
                <?php if ($post['jobposts']['job_qualifications']): $qualifications = json_decode($post['jobposts']['job_qualifications'], true); ?>
                    <div class="qualifications">
                        <h2>Qualifications</h2>
                        <ul>
                            <?php foreach ($qualifications as $qlf): ?>
                                <li><?= $qlf ?></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                <?php endif; ?>
                
                <?php if ($post['jobposts']['job_responsibilities']): $responsibilities = json_decode($post['jobposts']['job_responsibilities'], true) ?>
                    <div class="responsibilities">
                        <h2>Responsibilities</h2>
                        <ul>
                            <?php foreach ($responsibilities as $res): ?>
                                <li><?= $res ?></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                <?php endif; ?>

                <div class="url-container">
                    <h2>Job Post URL</h2>
                    <a target="_blank" href="<?= $globalworks->_url . '/job/' . $id ?>"><?= $globalworks->_url . '/job/' . $id ?></a>
                </div>

                <div class="options">
                    <h2>Job Post Options</h2>
                    <ul>
                        <li><div onclick="javascript: edit_post(<?= $id ?>)">Edit Job Post</div></li>
                        <li><div onclick="javascript: delete_post(<?= $id ?>)">Delete Job Post</div></li>
                        <li><div id="close2">Close This Window</div></li>
                    </ul>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    
    <script>
        
        $("#close, #close2").on("click", function() {
            $(".admin-jobpost-view").animate({
                top: "-100%"
            }, 500, function() {
                $(this).html("");
                $(".main-header").show();
                $("body").css({
                    "overflow": "auto"
                });
            });
        });
        
        function edit_post(id) {
            var height = $(window).height();
            var new_height = (height - 3) / 2 + 300;
            
            var data = {
                "operation": "admin_jobpost_edit",
                "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>",
                "id": id
            };
            
            $(".admin-jobpost-view").css({
                "line-height": new_height + "px",
                "text-align": "center"
            }).html("<img src='/img/loading.gif' />");
            
            $.post("/adminajax", data, function(r) {
                $(".admin-jobpost-view").css({
                    "line-height": "normal",
                    "text-align": "left"
                }).html(r);
            });
        }
        
        function delete_post(id) {
            var c = confirm("Are your sure you want to delete this Job Post?");
            var values = {
                "id": id,
                "del_notify": 0,
                "del_nofity_msg": ""
            };

            if (c) {
                var c2 = confirm("Do you want to notify the user?");

                if (c2) {
                    values["del_notify"] = 1;
                    $(".admin-jobposts-notify-delete").fadeIn(500);
                    $("#del-notify-cancel").on("click", function() {
                        values["del_notify"] = 0;
                        $(".admin-jobposts-notify-delete").fadeOut(500);
                    });
                    $("#del-notify-submit").on("click", function() {
                        if ($("#del-notify").val() == "") {
                            alert("Please write your message");
                        } else {
                            values['del_nofity_msg'] = $("#del-notify").val();
                            permanent_delete_post(values);
                        }
                    });
                } else {
                    permanent_delete_post(values);
                }
            }
        }

        function permanent_delete_post(values) {
            var data = {
                "operation": "admin_employers_jobpost_delete",
                "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
                "user_id": <?= $post['jobposts']['user_id'] ?>,
                "id": values['id'],
                "job_title": "<?= $post['jobposts']['job_title'] ?>",
                "del_notify": values['del_notify'],
                "del_notify_msg": values['del_nofity_msg']
            };

            $.post("/adminajax", data, function(r) {
                if (r == "ok") {
                    $(".admin-jobpost-view").animate({
                        top: "-100%"
                    }, 500, function() {
                        $(this).html("");
                        $(".main-header").show();
                        $("body").css({
                            "overflow": "auto"
                        });

                        location.reload();
                    });
                }
            });
        }
    </script>
 <?php endforeach; ?>