<div class="user-upload-pic">
<h1>Upload Your Picture</h1>
    <div class="profile-upload-form">
        <div class="profile-picture-upload">
            <div class="img-upload"></div>
            <ul>
                <li>Maximum of 1 MB size.</li>
                <li>ONLY JPEG and PNG image formats are allowed</li>
                <li>Image dimension should be 370x370 "PIXELS" or higher.</li>
            </ul>
            <div class="btn-container">
                <span id="goback">Go Back</span>
            </div>
        </div>
        <div class="error"></div>
        <div>&nbsp;</div>
    </div>
</div>

<script>
    $("#goback").on("click", function() {
        _edit_profile(<?= $id ?>);
    });

    $(".img-upload").dragdropupload('/adminajax', {
        label: "Drag Image file here or click here to browse image.",
        width: "100%",
        height: "300px",
        border_size: "3px",
        background_color: "#e3f8fd",
        label_font_size: "130%",
        error_font_size: "100%",
        post_data: {
            operation: "user_upload_pic",
            _csrfToken: "<?= $this->request->getParam('_csrfToken'); ?>",
            id: <?= $id ?>
        },
        validation: {
            file_types: {
                types: ["image/jpeg", "image/png"],
                validation_message: "is not an image file. Only jpeg and png formats are allowed."
            },
            file_sizes: {
                max: {
                    size: 1024,
                    validation_message: "size to large."
                }
            },
            image_dimension: {
                min: {
                    width: 370,
					height: 370,
					validation_message: "dimension should be 370x370 or higher."
                }
            }
        }
    }, function(r) {
        if (r) {
            var data = {
                "operation": "user_image_crop",
                "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
                "id": <?= $id ?>
            };

            if (window_toggled) {
                $(".user-view").css({
                    "text-align": "center",
                    "line-height": new_height + "px"
                }).html('<img src="/img/loading.gif" />');

                $.post("/adminajax", data, function(r) {
                    $(".user-view").css({
                        "text-align": "left",
                        "line-height": "normal"
                    }).html(r);
                });
            } else {
                $(".user-view").animate({
                    "top": "0"
                }, 1000, function() {

                    $(this).css({
                        "text-align": "center",
                        "line-height": new_height + "px"
                    }).html('<img src="/img/loading.gif" />');

                    $.post("/adminajax", data, function(r) {
                        $(".user-view").css({
                            "text-align": "left",
                            "line-height": "normal"
                        }).html(r);
                    });
                });
            }
        }
    });
</script>