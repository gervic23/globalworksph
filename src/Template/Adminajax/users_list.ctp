<?php use App\Controller\GlobalworksController; $globalworks = new GlobalworksController(); ?>

<div class="admin-user-del-notify">
    <div class="contents">
        <h1>Please write your message</h1>
        <field>
            <?= $this->Form->textarea('user-del-notify', ['id' => 'user-del-notify', 'class' => 'notify']) ?>
            <div class="notify-btns">
                <ul>
                    <li><?= $this->Form->button('Notify User and Delete', ['id' => 'user-del-submit', 'class' => 'notify-btn']) ?></li>
                    <li><?= $this->Form->button('Cancel', ['id' => 'user-del-cancel', 'class' => 'notify-btn']) ?></li>
                </ul>
            </div>
        </field>
    </div>
</div>

<table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th>&nbsp;</th>
            <th>Email</th>
            <th>Firstname</th>
            <th>Lastname</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($users as $user): ?>
            <tr>
                <?php if ($user['profiles']['profile_picture']): ?>
                   <td><img src="/img/users/profile_thumbnails/<?= $user['profiles']['profile_picture'] ?>" /></td>
                <?php else: ?>
                    <td><img src="/img/users/nopropic.png" /></td>
                <?php endif; ?>
                <td><?= $user['home']['email'] ?></td>
                <td><?= $user['home']['firstname'] ?></td>
                <td><?= $user['home']['lastname'] ?></td>
                <td><span onclick="javascript: _view(<?= $user['home']['id'] ?>)">View</span></td>
                <td><span onclick="javascript: _edit(<?= $user['home']['id'] ?>)">Edit</span></td>
                <td><span onclick="javascript: change_role(<?= $user['home']['id'] ?>)">Change Role</span></td>
                <?php if (!$user['home']['suspend']): ?>
                    <td><span onclick="javascript: _suspend(<?= $user['home']['id'] ?>)">Suspend</span></td>
                <?php else: ?>
                    <td><span onclick="javascript: _suspend(<?= $user['home']['id'] ?>)">Unsuspend</span></td>
                <?php endif; ?>
                <td><span onclick="javascript: _delete(<?= $user['home']['id'] ?>)">Delete</span></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>