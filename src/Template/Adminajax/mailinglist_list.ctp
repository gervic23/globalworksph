<?php foreach ($lists as $list): ?>
    <tr>
        <td><?= $list['subject'] ?></td>
        <td><?= $list['created_at'] ?></td>
        <td><a href="javascript:" onclick="javascript: _send(<?= $list['id'] ?>)">Send</a></td>
        <td><a href="/admin/mailinglist/edit/<?= $list['id'] ?>">Edit</a></td>
        <td><a href="javascript:" onclick="javascript: _delete(<?= $list['id'] ?>)">Delete</a></td>
    </tr>
<?php endforeach; ?>

<script>

    function _send(id) {
        var c = confirm("Are you sure you want to resend this?");

        if (c) {
            var data = {
                "operation": "resend_mailinglist",
                "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
                "id": id
            }

            $(".mailinglist_blackscreen, .mailinglist_flash").fadeIn(500);

            $.post("/adminajax", data, function(r) {
                if (r == "ok") {
                    $(".mailinglist_flash").html("Mailing List Message Sent!");

                    setTimeout(function() {
                        $(".mailinglist_blackscreen, .mailinglist_flash").fadeOut(500);
                    }, 2000);
                }
            });
        }
    }

    function _delete(id) {
        var c = confirm("Are you sure you want to delete this content?");

        if (c) {
            var data = {
                "operation": "delete_malinglist",
                "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
                "id": id
            };

            $.post("/adminajax", data, function(r) {
                _list();
            });
        }
    }
</script>