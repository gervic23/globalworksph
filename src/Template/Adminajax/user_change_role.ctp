<?php 
    $roles = [
        'Applicant' => 'Candidate',
        'Employer' => 'Employer',
        'Admin' => 'Admin',
    ];
?>

<div class="admin-changerole-notify">
    <div class="contents">
        <h1>Please write your message</h1>
        <field>
            <?= $this->Form->textarea('changerole-notify', ['id' => 'changerole-notify', 'class' => 'notify']) ?>
            <div class="notify-btns">
                <ul>
                    <li><?= $this->Form->button('Notify User and Change Role', ['id' => 'notify-changerole-submit', 'class' => 'notify-btn']) ?></li>
                    <li><?= $this->Form->button('Cancel', ['id' => 'notify-changerole-cancel', 'class' => 'notify-btn']) ?></li>
                </ul>
            </div>
        </field>
    </div>
</div>

<?php foreach ($users as $user): ?>
    <div class="user-role">
        <div class="profile-pic">
            <?php if ($user['profiles']['profile_picture']): ?>
                <img src="/img/users/profile_pictures/<?= $user['profiles']['profile_picture'] ?>" />
            <?php else: ?>
                <img src="/img/users/nopropic.png" />
            <?php endif; ?>
        </div>
        <div class="content">
            <div class="fullname">
                <?= $user['home']['firstname'] . ' ' . $user['home']['lastname'] ?>
            </div>
            <div class="changerole">
                <?= $this->Form->select('role', $roles, ['id' => 'role', 'class' => 'select-input', 'default' => $user['home']['role']]) ?>
            </div>
            <div class="user-role-loading"><img src="/img/loading.gif" /></div>
            <div class="btn">
                <ul>
                    <li><span id="change_role">Change Role</span></li>
                    <li><span id="goback">Go Back</span></li>
                </ul>
            </div>
        </div>
    </div>

    <script>
        if (!window_toggled) {
            $("#goback").html("Close Window");
        }

        $("#goback").on("click", function() {
            _edit_close(<?= $user_id ?>);
        });

        $("#change_role").on("click", function() {
            var c = confirm("Would you like to notify the user about the changes?");
            var values = {
                "has_notify": 0,
                "notify_msg": ""
            };

            if (c) {
                values['has_notify'] = 1;
                $(".admin-changerole-notify").fadeIn(500);
                $("#notify-changerole-cancel").on("click", function() {
                    values['has_notify'] = 0;
                    $(".admin-changerole-notify").fadeOut(500);
                });
                $("#notify-changerole-submit").on("click", function() {
                    if ($("#changerole-notify").val() == "") {
                        alert("Please write your message.");
                    } else {
                        values['notify_msg'] = $("#changerole-notify").val();
                        __change_role(values);
                    }
                });
            } else {
                __change_role(values);
            };
        });

        function __change_role(values) {
            var data1 = {
                "operation": "change_role",
                "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
                "user_id": <?= $user_id ?>,
                "role": $("#role").val(),
                "has_notify": values['has_notify'],
                "notify_msg": values['notify_msg']
            };

            $(".user-role-loading").show();

            $.post("/adminajax", data1, function(r) {
                if (r == "ok") {
                    record_modified = true;
                    $(".admin-changerole-notify").fadeOut(500);
                    _edit_close(<?= $user_id ?>);
                }
            });
        }
    </script>
<?php endforeach; ?>