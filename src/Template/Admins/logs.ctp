<div class="logs">
    <div class="log-types-container">
        <select id="logs">
            <option value="">All Logs</option>
            <option value="login_log">Login Log</option>
            <option value="register_log">Register Log</option>
            <option value="account_confirm_log">Account Confirm Log</option>
            <option value="forgot_password_log">Forgot Password Log</option>
            <option value="password_change_log">Password Change Log</option>
            <option value="create_jobposts_log">Create Jobposts Log</option>
            <option value="edit_jobposts_log">Edit Jobposts Log</option>
            <option value="delete_jobposts_log">Delete Jobposts Log</option>
            <option value="update_account_details_log">Update Account Details Log</option>
            <option value="create_account_profile_log">Create Account Profile Log</option>
            <option value="update_account_profile_log">Update Account Profile Log</option>
            <option value="admin_user_change_role">Admin User Change Role Log</option>
            <option value="admin_user_profile_change">AdminUser Profile Change Log</option>
            <option value="admin_user_details_change">Admin User Details Change Log</option>
            <option value="admin_user_suspend">Admin User Suspend Log</option>
            <option value="admin_user_account_delete">Admin User Account Delete Log</option>
            <option value="admin_jobpost_edit_log">Admin Jobposts Edit Log</option>
            <option value="admin_jobpost_delete_log">Admin Jobpost Delete Log</option>
            <option value="admin_create_announcement_logs">Admin Create Announcement Log</option>
            <option value="admin_published_announcement_logs">Admin Published Announcement Log</option>
            <option value="admin_edit_announcement_logs">Admin Edit Announcement Log</option>
            <option value="admin_create_mailinglist">Create Mailing List Log</option>
            <option value="admin_resend_mailinglist">Admin Resend Mailing List Log</option>
            <option value="admin_edit_mailinglist">Admin Edit Mailing List Log</option>
            <option value="admin_delete_mailinglist">Admin Delete Mailing List Log</option>
            <option value="user_subscribe_mailing_list">User Subscribe Mailing List Log</option>
        </select>
        <div id="clear">Clear Logs</div>
    </div>
    <div class="contents">
        <table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th>Log Message</th>
                    <th>Date</th>
                </tr>
                <tbody id="list">
                
                </tbody>
            </thead>
        </table>
        <div class="more-btn">
            <img class="loading2" src="/img/loading2.gif" />
            <span id="showmore">Show More</span>
        </div>
    </div>
</div>

<script>

    var log_type = "";
    var limit = 50;

    _list();

    $("#logs").on("change", function() {
        log_type = $(this).val();
        _list();
    });

    $("#clear").on("click", function() {
        var c = confirm("Are you sure you want to clear logs?");

        if (c) {
            var data = {
                "operation": "clear_logs",
                "_csrfToken": "<?= $this->request->param('_csrfToken') ?>"
            };

            $("#list").html('<tr><td colspan="2" class="loading"><img src="/img/loading.gif" /></td></tr>');

            $.post("/adminajax", data, function(r) {
                if (r == "ok") {
                    _list();
                }
            });
        }
    });

    $("#showmore").on("click", function() {
        limit = limit + 50;
        
        var data = {
            "operation": "log_list",
            "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
            "log_type": log_type,
            "limit": limit
        };
        
        $(".more-btn span").hide();
        $(".more-btn img").show();

        $.post("/adminajax", data, function(r) {
            $(".more-btn img").hide();
            $(".more-btn span").show();
            $("#list").html(r);
        });
    });

    function _list() {
        var data = {
            "operation": "log_list",
            "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
            "log_type": log_type,
            "limit": limit
        };

        $("#list").html('<tr><td colspan="2" class="loading"><img src="/img/loading.gif" /></td></tr>');

        $(".more-btn").hide();

        $.post("/adminajax", data, function(r) {
            $(".more-btn").show();
            $("#list").html(r);
        });
    }
</script>