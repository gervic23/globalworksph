<?php
    use App\Controller\GlobalworksController; $globalworks = new GlobalworksController();

    $this->Html->css('jquery-ui.min', ['block' => true]);
    $this->Html->script('jquery-ui.min', ['block' => true]);

    echo $this->CKEditor->loadJs();
?>

<div class="admin-jobpost-view"></div>
<div class="adminpanel">
    <div class="jobposts">
        <?= $this->element('Nav/admin-employers-joblist-nav'); ?>
        <div class="contents">
            <?php if ($jobposts_count !== 0): ?>
                <div class="jobposts-search-form">
                    <?= $this->Form->create($jps, ['method' => 'get', 'url' => '/admin/jobposts/search']) ?>
                        <field>
                            <?= $this->Form->control('search', ['name' => 'search', 'label' => 'Search Jobs:', 'autocomplete' => 'off']); ?>
                        </field>
                        <div class="submit-btn">
                            <?= $this->Form->submit('Search'); ?>
                        </div>
                    <?= $this->Form->end(); ?>
                </div>
                <ul class="jobposts-list">
                    <?php foreach ($jobposts as $post): ?>
                        <li>
                            <div class="jobpost">
                                <div class="left">
                                    <?php if ($post['jobposts']['company_logo']): ?>
                                        <a href="javascript:" onclick="javascript: job_view(<?= $post['jobposts']['id'] ?>)"><img src="/img/users/company_logo/<?= $post['jobposts']['company_logo'] ?>" /></a>
                                    <?php endif; ?>
                                </div>
                                <div class="right">
                                    <div class="post-header">
                                        <h1><a href="javascript:" onclick="javascript: job_view(<?= $post['jobposts']['id'] ?>)"><?= $post['jobposts']['job_title'] ?></a></h1>
                                    </div>
                                    <div class="post-company-name">
                                        <h3><a href="javascript:" onclick="javascript: job_view(<?= $post['jobposts']['id'] ?>)"><?= $post['jobposts']['company_name']; ?></a></h3>
                                    </div>
                                    <div class="post-description">
                                    <?= (strlen($post['jobposts']['job_description']) >= 400) ? substr($post['jobposts']['job_description'], 0, 400) . '...' : $post['jobposts']['job_description']; ?>
                                    </div>
                                    <div class="post-author">
                                        Posted by: <?= $post['users']['firstname'] . ' ' . $post['users']['lastname'] ?>
                                    </div>
                                    <div class="post-date">
                                        <?= $globalworks->when($post['jobposts']['created_at']) ?>
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>

                <?php if ($this->Paginator->numbers(['first' => 'First page'])): ?>
                    <div class="pagination">
                        <?= $this->Paginator->prev(' << ' . __('Previous')); ?>
                        <?= $this->Paginator->numbers(['first' => 'First page']) ?>
                        <?= $this->Paginator->next(__('Next') . ' >> '); ?>
                    </div>
                <?php endif; ?>
            <?php else: ?>

            <?php endif; ?>
        </div>
    </div>
</div>

<script>

    var new_height = 0;

    jopostview_size();
    
    $(window).resize(function() {
        jopostview_size();
    });
    
    function jopostview_size() {
        var height = $(window).height();
        var width = $(window).width() + 15;
        new_height = height - 3;
        
        $(".admin-jobpost-view").css({
            "height": new_height + "px",
            "width": width + "px"
        });
    }

    function job_view(id) {
        $(".main-header").hide();
        
        $(".admin-jobpost-view").animate({
            top: "0"
        }, 500, function() {
            var data = {
                "operation": "admin_jobpost_view",
                "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>",
                "id": id
            };
            
            var new_height2 = new_height / 2 + 300;
            
            $("body").css({
                "overflow": "hidden"
            });
            
            $(this).css({
                "line-height": new_height2 + "px",
                "text-align": "center"
            }).html("<img src='/img/loading.gif' />");
            
            $.post("/adminajax", data, function(r) {
                $(".admin-jobpost-view").css({
                    "line-height": "normal",
                    "text-align": "left"
                }).html(r);
            });
        });
    }
</script>