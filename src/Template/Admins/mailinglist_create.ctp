<?php
    echo $this->CKEditor->loadJs();
?>
<?= $this->element('mailinglist_nav') ?>

<div class="mailinglist-create">
    <div class="subject-container">
        <input type="text" id="subject" placeholder="Content Subject" />
    </field>
    <div class="editor">
        <input type="text" id="mailinglist" autocomplete="off" />
        <div class="btn-container">
            <input type="button" id="submit" value="Send" />
        </div>
    </div>
</div>

<script>
    CKEDITOR.replace('mailinglist', {
        on : {
            instanceReady : function( ev ) {
                this.dataProcessor.writer.setRules( 'p', {
                    indent : false,
                    breakBeforeOpen : false,
                    breakAfterOpen : false,
                    breakBeforeClose : false,
                    breakAfterClose : false
                });
            }
        },
        toolbar: [
            { name: 'basicstyles', groups: [ 'basicstyles'], items: [ 'Bold', 'Italic', 'Underline', 'Strike', '-', 'TextColor'] },
            { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
            { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] }
        ],
        height: "250px"
    });

    CKEDITOR.instances['mailinglist'].setData($("#mailinglist").val());

    $("#submit").on("click", function() {
        if ($("#subject").val() == "") {
            alert("Please write a subject.");
        } else if (CKEDITOR.instances["mailinglist"].getData() == "") {
            alert("Please write you content.");
        } else {
            var data = {
                "operation": "create_malinglist",
                "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
                "subject": $("#subject").val(),
                "content": CKEDITOR.instances["mailinglist"].getData()
            };
        }

        $.post("/adminajax", data, function(r) {
            if (r == "ok") {
                location = "/admin/mailinglist";
            }
        });
    });
</script>