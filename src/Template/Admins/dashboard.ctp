<div class="dashboard">
    <div class="left">
        <div class="contents">
            <div class="users">
                <h1>User's Statistics</h1>
                <div class="candidates_percent">
                    <div class="label">Candidates Statistics</div>
                    <div class="percent">
                        <div class="progess-bar">
                            <div class="number">0%</div>
                            <div class="bar"></div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="employers_percent">
                    <div class="label">Employers Statistics</div>
                    <div class="percent">
                        <div class="progess-bar">
                            <div class="number">0%</div>
                            <div class="bar"></div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="total-all">
                    <div class="label">Total Users</div>
                    <div class="percent">
                        0
                    </div>
                </div>
            </div>
            <div class="page-loads">
                <h1>Page Loads</h1>
                <div class="contents"></div>
            </div>
        </div>
    </div>
    <div class="right">
        <div class="contents">
            <div class="jobposts-stats">
                <h1>Jobpost Statistics</h1>
                <div class="jobposts-total-active">
                    <div class="label">Total Active</div>
                    <div class="count">&nbsp;</div>
                </div>
                <div class="jobposts-total-inactive">
                    <div class="label">Total Inactive</div>
                    <div class="count">&nbsp;</div>
                </div>
            </div>
            <div class="unique_visitors">
                <h1>Unique Visitors</h1>
                <div class="count">&nbsp;</div>
            </div>
        </div>        
    </div>
    <div class="clear"></div>
</div>

<script>
    $(".users .total-all .percent").html('<img src="/img/loading2.gif" />');
    $(".page-loads .contents").html('<img src="/img/loading2.gif" />');

    const candidates_count = new Promise(function(resolve, reject) {
        var data = {
            "operation": "candidates_count",
            "_csrfToken": "<?= $this->request->param('_csrfToken') ?>"
        };

        $.post("/adminajax", data, function(r) {
            resolve(r);
        });
    });

    const employers_count = new Promise(function(resolve, reject) {
        var data = {
            "operation": "employers_count",
            "_csrfToken": "<?= $this->request->param('_csrfToken') ?>"
        };

        $.post("/adminajax", data, function(r) {
            resolve(r);
        });
    });

    const users_count = new Promise(function(resolve, reject) {
        var data = {
            "operation": "users_count",
            "_csrfToken": "<?= $this->request->param('_csrfToken') ?>"
        };

        $.post("/adminajax", data, function(r) {
            resolve(r);
        });
    });

    Promise.all([candidates_count, employers_count, users_count]).then(function(values) {
        var candidates_count = values[0];
        var employers_count = values[1];
        var users_count = values[2];

        //Percentage.
        var candidates_percent = candidates_count / users_count * 100;
        var employers_percent = employers_count / users_count * 100;
        var c_i = 0;
        var e_i = 0;

        window.canidates_animate = setInterval(function() {
            if (c_i < candidates_percent.toFixed(0)) {
                $(".users .candidates_percent .number").html(c_i + "%");
                $(".users .candidates_percent .bar").css({
                    width: c_i + "%"
                });
                c_i++;
            } else {
                clearInterval(canidates_animate);
                $(".users .candidates_percent .number").html(candidates_percent.toFixed(0) + "%");
                $(".users .candidates_percent .bar").css({
                    width: candidates_percent.toFixed(0) + "%"
                });
            }
        }, 50);

        window.employers_animate = setInterval(function() {
            if (e_i < employers_percent.toFixed(0)) {
                $(".users .employers_percent .number").html(e_i + "%");
                $(".users .employers_percent .bar").css({
                    width: e_i + "%"
                });
                e_i++;
            } else {
                clearInterval(employers_animate);
                $(".users .employers_percent .number").html(employers_percent.toFixed(0) + "%");
                $(".users .employers_percent .bar").css({
                    width: employers_percent.toFixed(0) + "%"
                });
            }
        }, 50);

        $(".users .total-all .percent").html(users_count);
    });



    $(".jobposts-stats .jobposts-total-active .count").html('<img src="/img/loading2.gif" />');
    $(".jobposts-stats .jobposts-total-inactive .count").html('<img src="/img/loading2.gif" />');

    const jobposts_active_count = new Promise(function(resolve, reject) {
        var data = {
            "operation": "jobposts_active_count",
            "_csrfToken": "<?= $this->request->param('_csrfToken') ?>"
        };

        $.post("/adminajax", data, function(r) {
            resolve(r);
        });
    });

    const jobposts_inactive_count = new Promise(function(resolve, reject) {
        var data = {
            "operation": "jobposts_inactive_count",
            "_csrfToken": "<?= $this->request->param('_csrfToken') ?>"
        };

        $.post("/adminajax", data, function(r) {
            resolve(r);
        });
    });

    Promise.all([jobposts_active_count, jobposts_inactive_count]).then(function(values) {
        var active_counts = values[0];
        var inactive_counts = values[1];

        $(".jobposts-stats .jobposts-total-active .count").html(active_counts);
        $(".jobposts-stats .jobposts-total-inactive .count").html(inactive_counts);
    });

    const page_loads = new Promise(function(resolve, reject) {
        var data = {
            "operation": "visitor_stats",
            "_csrfToken": "<?= $this->request->param('_csrfToken') ?>"
        };

        $.post("/adminajax", data, function(r) {
            resolve(r);
        });
    });
    
    page_loads.then(function(value) {
        $(".page-loads .contents").html(value);
    });

    $(".unique_visitors .count").html('<img src="/img/loading2.gif" />');

    const uniques = new Promise(function(resolve, reject) {
        var data = {
            "operation": "visitors_unique",
            "_csrfToken": "<?= $this->request->param('_csrfToken') ?>"
        };

        $.post("/adminajax", data, function(r) {
            resolve(r);
        });
    });
    
    uniques.then(function(value) {
        $(".unique_visitors .count").html(value);
    });
</script>