<?php use App\Controller\GlobalworksController; $globalworks = new GlobalworksController(); ?>
<?= $this->element('nav/admin_my_account_nav'); ?>

<div class="myaccount">
    <?= $this->Flash->render(); ?>
    <table cellspacing="0" cellpadding="0">
        <tbody>
            <?php foreach ($users as $user): ?>
                <tr>
                    <td>Email:</td>
                    <td><?= $user['email'] ?></td>
                </tr>
                <tr>
                    <td>Firstname:</td>
                    <td><?= $user['firstname'] ?></td>
                </tr>
                <tr>
                    <td>Lastname:</td>
                    <td><?= $user['lastname'] ?></td>
                </tr>
                <tr>
                    <td>Address:</td>
                    <td><?= $user['address'] ?></td>
                </tr>
                <tr>
                    <td>Region:</td>
                    <td><?= $user['region'] ?></td>
                </tr>
                <tr>
                    <td>Country:</td>
                    <td><?= $globalworks->country_to_str($user['country']) ?></td>
                </tr>
                <tr>
                    <td>Contact Number:</td>
                    <td><?= $user['contact_number'] ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="btn-container">
        <span class="btn" id="edit">Edit Account Details</span>
    </div>
</div>

<script>
    $("#edit").on("click", function() {
        location = "/admin/myaccount/edit";
    });

    $(".flash-success").on("click", function() {
        $(this).hide();
    });
</script>