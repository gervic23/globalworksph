<?php
    echo $this->CKEditor->loadJs();
?>

<div class="create-announcement">
    <h1>Create Announcement</h1>
    <field>
        <?= $this->Form->control('announcement', ['label' => 'Announcement:', 'id' => 'announcement']) ?>
    </field>
    <div class="btn">
        <?= $this->Form->button('Create Announcement', ['id' => 'announcement-sub']) ?>
    </div>
</div>

<script>
    CKEDITOR.replace('announcement', {
        on : {
            instanceReady : function( ev ) {
                this.dataProcessor.writer.setRules( 'p', {
                    indent : false,
                    breakBeforeOpen : false,
                    breakAfterOpen : false,
                    breakBeforeClose : false,
                    breakAfterClose : false
                });
            }
        },
        toolbar: [
            { name: 'basicstyles', groups: [ 'basicstyles'], items: [ 'Bold', 'Italic', 'Underline', 'Strike', '-', 'TextColor'] },
            { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
            { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] }
        ],
        height: "250px"
    });

    CKEDITOR.instances['announcement'].setData($("#announcement").val());
    
    $("#announcement-sub").on("click", function() {
        var announcement = CKEDITOR.instances["announcement"].getData();

        if (announcement == "") {
            alert("Please write your announcement.");
        } else {
            var data = {
                "operation": "create_announcement",
                "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
                "announcement": announcement
            };

            $.post("/adminajax", data, function(r) {
                location = "/admin/announcements";
            });
        }
    });
</script>