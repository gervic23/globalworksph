<?= $this->element('nav/admin_my_account_nav'); ?>

<div class="changepassword">
    <?= $this->Form->create($users) ?>
    <field>
        <?= $this->Form->control('current_password', ['type' => 'password', 'label' => 'Current Password:', 'id' => 'current_password', 'class' => 'password-field']) ?>
        <?= $this->Form->control('new_password', ['type' => 'password', 'label' => 'New Password:', 'id' => 'new_password', 'class' => 'password-field']) ?>
        <?= $this->Form->control('rpassword', ['type' => 'password', 'label' => 'Confirm New Password:', 'id' => 'rpassword', 'class' => 'password-field']) ?>
    </field>
    <div class="btn-container">
        <?= $this->Form->submit('Change Password') ?>
    </div>
    <?= $this->Form->end() ?>
</div>

<script>
    $("#current_password").on("keyup input", function() {
        $(this).css({"border-color": "#ccc"});
        $(".changepassword form .input:nth-child(1) .error-message").hide();
    });
    
    $("#new_password").on("keyup input", function() {
        $(this).css({"border-color": "#ccc"});
        $(".changepassword form .input:nth-child(2) .error-message").hide();
    });

    $("#rpassword").on("keyup input", function() {
        $(this).css({"border-color": "#ccc"});
        $(".changepassword form .input:nth-child(3) .error-message").hide();
    });
</script>