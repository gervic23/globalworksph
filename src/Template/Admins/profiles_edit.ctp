<?php
    $this->Html->script('jquery.dragdropupload.1.0', ['block' => true]);
    $this->Html->css('cropper', ['block' => true]);
    $this->Html->script('cropper', ['block' => true]);
?>

<?= $this->element('nav/admin_my_account_nav'); ?>
<div class="upload-profile-pic-view"></div>
<div class="profiles_edit">
    <div class="form">
        <?= $this->Form->create($profiles2) ?>
            <div class="header">
                <div class="loading">
                    <img src="/img/loading.gif" />
                </div>
                <div class="img"></div>
                <div class="img-btn-1">
                    <span id="upload">Replace</span> <span id="delete">Delete</span>
                </div>
                <div class="img-btn-2">
                    <span id="upload2">Upload Profile Image</span>
                </div>
            </div>

            <div class="inputs">
                <?= $this->Form->control('nickname', ['label' => 'Nickname:', 'id' => 'nickname', 'class' => 'text-input', 'autocomplete' => 'off', 'maxlength' => 255, 'value' => $nickname]) ?>
                <div class="subscribe">
                    <?= $this->Form->checkbox('job_subscribe', ['id' => 'j_subscribe']); ?>
                    <label for="j_subscribe">Subscribe for latest jobs.</label>
                    <div class="check"></div>
                </div>
                <div class="btn-container">
                    <?= $this->Form->submit('Save') ?>
                </div>
            </div>
        <?= $this->Form->end() ?>
    </div>
</div>

<script>

    var new_height = 0;

    <?php if ($profile_picture): ?>
        loadProfilePic("<?= $profile_picture ?>");
    <?php else: ?>
        loadProfilePic();
    <?php endif; ?>

    uploadPictureSize();

    $("#upload, #upload2").on("click", function() {

        $(".upload-profile-pic-view").animate({
            top: "0"
        }, 500, function() {
            var data = {
                "operation": "upload_pic_page",
                "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>"
            };
            
            var new_height2 = new_height / 2 + 300;
            
            $("body").css({
                "overflow": "hidden"
            });
            
            $(this).css({
                "line-height": new_height2 + "px",
                "text-align": "center"
            }).html("<img src='/img/loading.gif' />");
            
            $.post("/adminajax", data, function(r) {
                $(".upload-profile-pic-view").css({
                    "line-height": "normal",
                    "text-align": "left"
                }).html(r);
            });
        });
    });

    function loadProfilePic(image, cropped = false) {
        if (cropped) {
            $(".upload-profile-pic-view").animate({
                top: "-100%"
            }, 500, function() {
                $(this).html("");
                $(".main-header").show();
                $("body").css({
                    "overflow-y": "auto"
                });
                
                $(".img").hide();
                $(".img-btn-1, .img-btn-2").hide();
                $(".loading").show();

                setTimeout(function() {
                    $(".loading").hide();
                    $(".img").fadeIn(500).html('<img src="/img/users/profile_pictures/' + image + '" />');
                    $(".img-btn-1").show();
                    //dashboardHeight();
                }, 2500);
            });
        } else {
            if (image) {
                $(".loading").show();

                setTimeout(function() {
                    $(".loading").hide();
                    $(".img").fadeIn(500).html('<img src="/img/users/profile_pictures/' + image + '" />');
                    $(".img-btn-1").show();
                    //dashboardHeight();
                }, 1000);
            } else {
                $(".loading").show();

                setTimeout(function() {
                    $(".loading").hide();
                    $(".img").fadeIn(500).html('<img src="/img/users/nopropic.png" />');
                    $(".img-btn-2").show();
                    //dashboardHeight();
                }, 1000);
            }
        }
    }

    function uploadPictureSize() {
        var height = $(window).height();
        var width = $(window).width() + 15;
        new_height = height - 2;
        
        $(".upload-profile-pic-view, .upload-profile-resume-view").css({
            "height": new_height + "px",
            "width": width + "px"
        });
    }
</script>