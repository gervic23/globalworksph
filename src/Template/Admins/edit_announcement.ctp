<?php
    echo $this->CKEditor->loadJs();
?>

<?php foreach ($announcements as $announcement): ?>
    <div class="edit-announcement">
        <h1>Edit Announcement</h1>
        <field>
            <input type="text" id="announcement" value="<?= $announcement['announcement'] ?>" />
        </field>
        <div class="publish-btn">
            <input type="checkbox" id="publish" /> <label>Publish Announcement?</label>
        </div>
        <div class="btn">
            <?= $this->Form->button('Save Announcement', ['id' => 'announcement-sub']) ?>
        </div>
    </div>
<?php endforeach; ?>

<script>

    CKEDITOR.replace('announcement', {
        on : {
            instanceReady : function( ev ) {
                this.dataProcessor.writer.setRules( 'p', {
                    indent : false,
                    breakBeforeOpen : false,
                    breakAfterOpen : false,
                    breakBeforeClose : false,
                    breakAfterClose : false
                });
            }
        },
        toolbar: [
            { name: 'basicstyles', groups: [ 'basicstyles'], items: [ 'Bold', 'Italic', 'Underline', 'Strike', '-', 'TextColor'] },
            { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
            { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] }
        ],
        height: "250px"
    });

    CKEDITOR.instances['announcement'].setData($("#announcement").val());

    var publish = 0;

    $("#publish").on("change", function() {
        if ($(this).is(":checked")) {
            publish = 1;
        } else {
            publish = 0;
        }
    });
    
    $("#announcement-sub").on("click", function() {
        var announcement = CKEDITOR.instances["announcement"].getData();

        if (announcement == "") {
            alert("Please write your announcement.");
        } else {
            var data = {
                "operation": "save_edit_announcement",
                "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
                "id": <?= $id ?>,
                "announcement": announcement,
                "publish": publish
            };

            $.post("/adminajax", data, function(r) {
                location = "/admin/announcements";
            });
        }
    });
</script>