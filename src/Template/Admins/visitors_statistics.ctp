<div class="ip-log-blackscreen"></div>
<div class="ip-log-view">
    <div class="ip-log-content">
        
    </div>
</div>
<div class="visitors-statistics">
    <table cellspacing="0" cellpadding="0">
        <thead>
            <tr>
                <th>IP Address</th>
                <th>Country</th>
                <th>Region</th>
                <th>Hits/Page Loads</th>
                <th>&nbsp;</th>
            </tr>
            <tbody id="list"></tbody>
        </thead>
    </table>
</div>

<script>
    _loadlist();

    function _loadlist() {
        $("#list").html('<tr><td colspan="5" class="loading"><img src="/img/loading.gif" /></td></tr>');

        var data = {
            "operation": "ip_logs_list",
            "_csrfToken": "<?= $this->request->param('_csrfToken') ?>"
        };

        $.post("/adminajax", data, function(r) {
            $("#list").html(r);
        });
    }
</script>