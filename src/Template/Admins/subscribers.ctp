<?= $this->element('mailinglist_nav') ?>
<div class="subscribers">
    <div style="text-align: center;"><img src="/img/loading.gif" /></div>
</div>

<script>

    _subscribers();
    
    function _subscribers() {
        var data = {
            "operation": "subscribers_list",
            "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
        }

        $.post("/adminajax", data, function(r) {
            $(".subscribers").html(r);
        });
    }
</script>