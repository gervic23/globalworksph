<div class="announcements">
    <div class="create-btn">
        <a href="/admin/createannouncement">Create Announcement</a>
    </div>
    <div class="list">
        <table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th>Announcement</th>
                    <th>Date</th>
                    <th>Published</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody class="ann-list">
                
            </tbody>
        </table>
    </div>
</div>

<script>

    _announcements();

    function _announcements() {
        $(".ann-list").html('<tr><td colspan="5" class="ann-loading"><img src="/img/loading.gif" /></td></tr>');

        var data =  {
            "operation": "announcement_list",
            "_csrfToken": "<?= $this->request->param('_csrfToken') ?>"
        };

        $.post("/adminajax", data, function(r) {
            $(".ann-list").html(r);
        });
    }
</script>