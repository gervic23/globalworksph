<?php

    //Include jQuery UI css.
    $this->Html->css('jquery-ui.min', ['block' => true]);

    //Include imagecropper css.
    $this->Html->css('cropper', ['block' => true]);

    //Include jQuery UI.
    $this->Html->script('jquery-ui-1.9.2.min', ['block' => true]);

    //Include shogotags plugin.
    $this->Html->script('jquery.shogotags-1.0', ['block' => true]);

    //Include dragdownuploader.
    $this->Html->script('jquery.dragdropupload.1.0', ['block' => true]);

    //Include image cropper js.
    $this->Html->script('cropper', ['block' => true]);
?>

<?= $this->element('Nav/admin_users_nav'); ?>
<div class="users-candidates">
    <div class="user-view"></div>
    <div class="black-screen"></div>
    <div class="suspend-window"></div>
    <div class="user-search">
        <div class="user-search-flash"></div>
        <?= $this->Form->text('s', ['id' => 's', 'class' => 'search-input', 'placeholder' => 'Search Here...', 'autocomplete' => 'off']) ?>
        <?= $this->Form->button('search', ['id' => 'search', 'type' => 'button', 'class' => 'search-btn', 'value' => 'Search']); ?>
    </div>
    <div class="candidates-list"></div>
    <div class="loading">
        <img src="/img/loading.gif" />
    </div>
    <div class="paginator">
        <span id="prev"><?= htmlentities('<<'); ?></span> Page <span id="current_page"></span> to <span id="max_page"></span> <span id="next"><?= htmlentities('>>'); ?></span>
    </div>
</div>
<script>

    var page = 1;
    var max_results = 50;
    var role = "Employer";

    var new_height = 0;
    var new_width = 0;

    var window_toggled = false;
    var is_search = false;
    var record_modified = false;
    
    loadPagination();
    dropdownWindowsSize();

    $(window).resize(function() {
        dropdownWindowsSize();
    });

    $("#prev").on("click", function() {
        page--;
        $(".paginator").hide();
        $(".loading").show();
        if (!is_search) {
            loadPagination();
        } else {
            paginationSearch();
        }
    });

    $("#next").on("click", function() {
        page++;
        $(".paginator").hide();
        $(".loading").show();
        if (!is_search) {
            loadPagination();
        } else {
            paginationSearch();
        }
    });

    $("#search").on("click", function() {
        paginationSearch();
    });

    $("#s").on("keyup input", function() {
        $(".user-search-flash").html("");
    });

    function loadPagination() {
        var main_data = {
            "operation": "users_list_pagination",
            "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
            "page": page,
            "role": role,
            "max_results": max_results
        };

        $(".loading").show();

        $.post("/adminajax", main_data, function(r) {
            if (r) {
                var current_page = JSON.parse(r).current_page;
                var pages = JSON.parse(r).pages;
                var users_count = JSON.parse(r).users_count;

                if (users_count > 0) {
                    $(".user-search").show();
                    $(".loading").hide();
                    $(".paginator").show();
                    $("#current_page").html(current_page);
                    $("#max_page").html(pages);

                    if (pages == 1) {
                        $(".users-candidates .paginator").hide();
                    }

                    if (current_page == 1) {
                        $("#prev").hide();
                    } else {
                        $("#prev").show();
                    }

                    if (current_page == pages) {
                        $("#next").hide();
                    } else {
                        $("#next").show();
                    }

                    loadList(current_page);
                } else {
                    $(".paginator").hide();
                    $(".loading").hide();

                    $(".candidates-list").css({
                        "text-align": "center",
                        "margin-top": "20px"
                    }).html('<img src="/img/loading.gif" />');

                    setTimeout(function() {
                        $(".candidates-list").css({
                            "text-align": "center",
                            "margin-top": "20px"
                        }).html('No are no candidates at this time.');
                    }, 1000);
                }
            }
        });
    }

    function loadList(current_page) {
        var main_data = {
            "operation": "users_list",
            "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
            "current_page": current_page,
            "role": role,
            "max_results": max_results
        };

        $(".candidates-list").css({
            "text-align": "center",
            "margin-top": "20px"
        }).html('<img src="/img/loading.gif" />');

        $.post("/adminajax", main_data, function(r) {
            $(".candidates-list").css({
                "text-align": "left",
                "margin-top": "0px"
            }).html(r, function() {
                alert(1);
            });
        });
    }

    function paginationSearch() {
        var s = $("#s").val();

        if (s.length < 3) {
            $(".user-search-flash").html("Search Keywords is too short.");
        } else {
            is_search = true;

            var data = {
                "operation": "users_list_pagination_search",
                "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
                "page": page,
                "role": role,
                "max_results": max_results,
                "keywords": s
            };

            $.post("/adminajax", data, function(r) {
                if (r) {
                    var current_page = JSON.parse(r).current_page;
                    var pages = JSON.parse(r).pages;
                    var users_count = JSON.parse(r).users_count;
                    var keywords = JSON.parse(r).keywords;

                    if (users_count > 0) {
                        $(".user-search").show();
                        $(".loading").hide();
                        $(".paginator").show();
                        $("#current_page").html(current_page);
                        $("#max_page").html(pages);

                        page = 1;

                        if (pages == 1) {
                            $(".users-candidates .paginator").hide();
                        }

                        if (current_page == 1) {
                            $("#prev").hide();
                        } else {
                            $("#prev").show();
                        }

                        if (current_page == pages) {
                            $("#next").hide();
                        } else {
                            $("#next").show();
                        }

                        loadListSearch(current_page, keywords);
                    } else {
                        $(".paginator").hide();

                        $(".candidates-list").css({
                            "text-align": "center",
                            "margin-top": "20px"
                        }).html('<img src="/img/loading.gif" />');

                        setTimeout(function() {
                            $(".candidates-list").css({
                                "text-align": "center",
                                "margin-top": "20px"
                            }).html('No results found');
                        }, 1000);
                    }
                }
            });
        }
    }

    function loadListSearch(current_page, keywords) {
        var main_data = {
            "operation": "users_list_search",
            "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
            "current_page": current_page,
            "role": role,
            "max_results": max_results,
            "keywords": keywords
        };

        $(".candidates-list").css({
            "text-align": "center",
            "margin-top": "20px"
        }).html('<img src="/img/loading.gif" />');

        $.post("/adminajax", main_data, function(r) {
            $(".candidates-list").css({
                "text-align": "left",
                "margin-top": "0px"
            }).html(r);
        });
    }

    function _view(id) {
        window_toggled = true;

        $("html, body").css({
            "overflow": "hidden"
        });

        $(".user-view").animate({
            "top": "0"
        }, 1000, function() {

            $(this).css({
                "text-align": "center",
                "line-height": new_height + "px"
            }).html('<img src="/img/loading.gif" />');

            var data = {
                "operation": "user_view",
                "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
                "id": id
            };

            $.post("/adminajax", data, function(r) {
                $(".user-view").css({
                    "text-align": "left",
                    "line-height": "normal"
                }).html(r);
            });
        });
    }

    function _view_close() {
        window_toggled = false;

        $(".user-view").animate({
            "top": "-100%"
        }, 1000, function() {
            $(this).html("");

            $("html, body").css({
                "overflow": "auto"
            });
        });

        if (record_modified) {
            if (is_search) {
                paginationSearch();
            } else {
                loadPagination();
            }

            record_modified = false;
        }
    }

    function _edit(id) {
        var data = {
            "operation": "user_edit",
            "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
            "id": id
        };

        $("html, body").css({
            "overflow": "hidden"
        });

        if (window_toggled) {
            $(".user-view").css({
                "text-align": "center",
                "line-height": new_height + "px"
            }).html('<img src="/img/loading.gif" />');

            $.post("/adminajax", data, function(r) {
                $(".user-view").css({
                    "text-align": "left",
                    "line-height": "normal"
                }).html(r);
            });
        } else {
            $(".user-view").animate({
                "top": "0"
            }, 1000, function() {

                $(this).css({
                    "text-align": "center",
                    "line-height": new_height + "px"
                }).html('<img src="/img/loading.gif" />');

                $.post("/adminajax", data, function(r) {
                    $(".user-view").css({
                        "text-align": "left",
                        "line-height": "normal"
                    }).html(r);
                });
            });
        }
    }

    function _edit_close(id) {
        if (window_toggled) {
            var data = {
                "operation": "user_view",
                "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
                "id": id
            };

            $(".user-view").css({
                "text-align": "center",
                "line-height": new_height + "px"
            }).html('<img src="/img/loading.gif" />');

            $.post("/adminajax", data, function(r) {
                $(".user-view").css({
                    "text-align": "left",
                    "line-height": "normal"
                }).html(r);
            });
        } else {
            $(".user-view").animate({
                "top": "-100%"
            }, 1000, function() {
                $(this).html("");

                $("html, body").css({
                    "overflow": "auto"
                });
            });
        }

        if (record_modified) {
            if (is_search) {
                paginationSearch();
            } else {
                loadPagination();
            }

            record_modified = false;
        }
    }

    function change_role(id) {
        var data = {
            "operation": "user_change_role",
            "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
            "id": id
        };

        $("html, body").css({
            "overflow": "hidden"
        });

        if (window_toggled) {
            $(".user-view").css({
                "text-align": "center",
                "line-height": new_height + "px"
            }).html('<img src="/img/loading.gif" />');

            $.post("/adminajax", data, function(r) {
                $(".user-view").css({
                    "text-align": "left",
                    "line-height": "normal"
                }).html(r);
            });
        } else {
            $(".user-view").animate({
                "top": "0"
            }, 1000, function() {

                $(this).css({
                    "text-align": "center",
                    "line-height": new_height + "px"
                }).html('<img src="/img/loading.gif" />');

                $.post("/adminajax", data, function(r) {
                    $(".user-view").css({
                        "text-align": "left",
                        "line-height": "normal"
                    }).html(r);
                });
            });
        }
    }

    function _suspend(id) {
        var data = {
            "operation": "user_suspend",
            "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
            "id": id
        };

        $("html, body").css({
            "overflow": "hidden"
        });

        if (window_toggled) {
            $(".user-view").css({
                "text-align": "center",
                "line-height": new_height + "px"
            }).html('<img src="/img/loading.gif" />');

            $.post("/adminajax", data, function(r) {
                $(".user-view").css({
                    "text-align": "left",
                    "line-height": "normal"
                }).html(r);
            });
        } else {
            $(".user-view").animate({
                "top": "0"
            }, 1000, function() {

                $(this).css({
                    "text-align": "center",
                    "line-height": new_height + "px"
                }).html('<img src="/img/loading.gif" />');

                $.post("/adminajax", data, function(r) {
                    $(".user-view").css({
                        "text-align": "left",
                        "line-height": "normal"
                    }).html(r);
                });
            });
        }
    }

    function _delete(id) {
        var c = confirm("Are you sure you want to delete this account?");
        var values = {
            "id": id,
            "has_notify": 0,
            "notify_msg": ""
        };

        if (c) {
            var c2 = confirm("Notify user by sending email message?");

            if (c2) {
                values['has_notify'] = 1;
                $(".admin-user-del-notify").fadeIn(500);
                $("#user-del-cancel").on("click", function() {
                    values['has_notify'] = 0;
                    $(".admin-user-del-notify").fadeOut(500);
                });
                $("#user-del-submit").on("click", function() {
                    if ($("#user-del-notify").val() == "") {
                        alert("Please write your message.");
                    } else {
                        values['notify_msg'] = $("#user-del-notify").val();
                        permanent_delete(values);
                    }
                });
            } else {
                permanent_delete(values);
            }
        }
    }

    function permanent_delete(values) {
        var data = {
            "operation": "delete_user",
            "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
            "id": values['id'],
            "has_notify": values['has_notify'],
            "notify_msg": values['notify_msg']
        };

        $.post("/adminajax", data, function(r) {
            //loadPagination();
            console.log(r);
        });
    }

    function dropdownWindowsSize() {
        var height = $(window).height();
        var width = $(window).width();
        new_height = height - 2;
        new_width = width - 2;

        $(".user-view").css({
            "height": new_height + "px",
            "width": new_width + "px"
        });
    }
</script>