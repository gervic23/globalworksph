<div class="mailinglist_blackscreen"></div>
<div class="mailinglist_flash">
    <img src="/img/loading.gif" />
</div>

<?= $this->element('mailinglist_nav') ?>

<div class="mailinglist">
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th>Subject</th>
                <th>Date</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody id="list">
        
        </tbody>
    </table>
    <div class="btn-container">
        <div class="loading"><img src="/img/loading2.gif" /></div>
        <span id="showmore">Show More</span>
    </div>
</div>

<script>
    var limit = 50;

    _list();

    $("#showmore").on("click", function() {
        $(this).hide();
        $(".loading").show();

        limit = limit + 5;
        
        var data = {
            "operation": "malinglist_list",
            "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
            "limit": limit
        };

        $.post("/adminajax", data, function(r) {
            $(".loading").hide();
            $("#showmore").show();
            $("#list").html(r);
        });
    });

    function _list() {
        var data = {
            "operation": "malinglist_list",
            "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
            "limit": limit
        };

        $("#list").html('<tr><td colspan="5"><img src="/img/loading.gif" /></td></tr>');

        $.post("/adminajax", data, function(r) {
            $("#list").html(r);
        });
    }
</script>