<?php
if (!isset($title)) {
    $title = 'Globalworks Philippines';
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en-US" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('admin.css') ?>
    <?= $this->Html->css('animate.css') ?>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Allerta" rel="stylesheet">
    <?= $this->Html->script('jquery-3.2.1.min') ?>
    <?= $this->Html->script('jquery-migrate-1.4.1.min') ?>
    <?= $this->Html->script('detect-os') ?>
    <?= $this->Html->script('jquery.scrollAnimate-1.0') ?>
    <?= $this->Html->script('jquery.slimscroll.min') ?>
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
    <title>
        <?= $title ?>
    </title>
    
    <?php if ($this->request->session()->read('Auth.User') && $thumbnail !== ''): ?>
        <style>
            .thumbpic {
                background-image: url('/img/users/profile_thumbnails/<?= $thumbnail; ?>') !important;
            }
        </style>
    <?php endif; ?>
</head>
<body>
    <div class="main-container">
        <!-- Main Header -->
        <?= $this->element('Nav/admin_header') ?>
        
        <!-- Main Aside -->
        <?= $this->element('Nav/admin_aside') ?>
        
        <section class="main-section">
            <?= $this->fetch('content') ?>
        </section>
        <div class="clear"></div>
    </div>
    <?= $this->Html->script('admin') ?>
</body>
</html>
