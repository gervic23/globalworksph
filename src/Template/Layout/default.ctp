<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

use App\Controller\GlobalworksController;
$globalworks = new GlobalworksController();

if (!isset($title)) {
    $title = 'Globalworks Philippines';
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en-US" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>
        <?= $title ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('style.css') ?>
    <?= $this->Html->css('animate.css') ?>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Allerta" rel="stylesheet">
    <?= $this->Html->script('jquery-3.2.1.min') ?>
    <?= $this->Html->script('jquery-migrate-1.4.1.min') ?>
    <?= $this->Html->script('detect-os') ?>
    <?= $this->Html->script('jquery.scrollAnimate-1.0') ?>
    <?= $this->Html->script('jquery.slimscroll.min') ?>
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
    
    <?php if ($this->request->session()->read('Auth.User') && $thumbnail !== ''): ?>
        <style>
            .thumbpic {
                background-image: url('/img/users/profile_thumbnails/<?= $thumbnail; ?>') !important;
            }
        </style>
    <?php endif; ?>
</head>
<body>
    <div class="main-container">
        
        <!-- Main Header -->
        <?= $this->element('Nav/header') ?>
        
        <section class="main-section">
            <?= $this->fetch('content'); ?>
        </section>
        <footer class="main-footer">
            <div class="contents">
                <div class="forjobseekers">
                    <h3>For Job Seekers</h3>
                    <ul>
                        <li><a href="/jobs/search">Search Jobs</a></li>
                        <li><a href="/jobs/all">Browse Jobs</a></li>
                        <li><a href="/login">Login</a></li>
                    </ul>
                </div>
                <div class="foremployers">
                    <h3>For Employers</h3>
                    <ul>
                        <li><a href="/employers/jobpost">Post a job New</a></li>
                        <li><a href="/employers/search">Search Candidates</a></li>
                        <li><a href="/login/employers">Login</a></li>
                    </ul>
                </div>
                <div class="about">
                    <h3>About</h3>
                    <ul>
                        <li><a href="/aboutus">About Us</a></li>
                        <li><a href="privacypolicy">Privacy Policy</a></li>
                        <li><a href="/tos">Terms of Use</a></li>
                        <li><a href="/disclaimer">Disclaimer</a></li>
                    </ul>
                </div>
                <div class="contactus">
                    <h3>Contact Us</h3>
                    <ul>
                        <li><a href="mailto: support@<?= $globalworks->_domain ?>">Email Us</a></li>
                        <li><a href="/subscribe">Subscribe</a></li>
                        <li><a href="/administrators">Admin</a></li>
                    </ul>
                </div>
                <div class="getsocial">
                    <h3>Get Social</h3>
                    <ul>
                        <li><a href="#"><img src="/img/sm/fb-01.png" /></a></li>
                        <li><a href="#"><img src="/img/sm/linkdin-01-01.png" /></a></li>
                        <li><a href="#"><img src="/img/sm/twitter-01.png" /></a></li>
                        <li><a href="#"><img src="/img/sm/googleplus-01.png" /></a></li>
                    </ul>
                </div>
                <div class="clear"></div>
            </div>
        </footer>
    </div>
    <?php if ($pop_announcement): ?>
        <div class="footer-blackscreen"></div>
        <div class="pop-announcement">
            <div class="container">
                <div class="btn">
                <img class="close" id="announcemet-close" title="Close this window" src="/img/close.png" />
                </div>
                <div class="contents">
                    <?= $pop_announcement ?>
                </div>
            </div>
        </div>

        <script>
            setTimeout(function() {
                $(".footer-blackscreen").fadeIn(100, function() {
                    $(".pop-announcement").fadeIn(500);
                });
            }, 3000);

            $("#announcemet-close").on("click", function() {
                $(".pop-announcement").fadeOut(500, function() {
                    $(".footer-blackscreen").fadeOut(100);
                });
            });
        </script>
    <?php endif; ?>
    <?= $this->Html->script('default'); ?>
</body>
</html>
