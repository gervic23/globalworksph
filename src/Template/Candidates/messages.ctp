<?php
    use App\Controller\GlobalworksController; $globalworks = new GlobalworksController();
    $this->Html->script('jquery.slimscroll.min', ['block' => true]);
?>

<div class="candidates-dashboard">
<div class="black-screen"></div>
<div class="message-view"></div>
    <div class="left">
        <?= $this->element('Nav/dashboard-candidates') ?>
    </div>
    <div class="right">
        <div>&nbsp;</div>
        <div class="messages-nav">
            <ul>
                <li class="msg-btn" id="unread"><span class="btn">Unead Messages</span> <?= ($globalworks->display_unread_msgs_count()) ? '<span class="unread-notification">'. $globalworks->display_unread_msgs_count() .'</span>' : ''; ?></li>
                <li class="msg-btn" id="ji"><span class="btn">Job Invitation</span></li>
                <li class="msg-btn" id="from-admin"><span>From Admin</span></li>
                <li class="msg-btn" id="st"><span class="btn">Sent Items</span></li>
            </ul>
        </div>
        <div class="m-message-nav">
            <select id="page">
                <option value="candidate_msg_unread">Unread Messages</option>
                <option value="candidate_msg_job_invitation">Job Invitation</option>
                <option value="candidate_msg_from_admin">From Admin</option>
                <option value="candidate_msg_sent_items">Sent Items</option>
            </select>
        </div>
        <div class="messages-contents"></div>
    </div>
    <div class="clear"></div>
</div>
<div>&nbsp;</div>
<?= $this->Html->script('candidatesdashboard') ?>
<script>

    var new_height = 0;
    
    var page = 'candidate_msg_unread';

    messageViewSize();
    unread_notification();

    $(window).resize(function() {
        messageViewSize();
    });

    $("#page").on("change", function() {
        removeBorder();

        switch ($(this).val()) {
            case 'candidate_msg_unread':
                page = 'candidate_msg_unread';
                $("#unread").addClass("message-active");
            break;

            case 'candidate_msg_job_invitation':
                page = 'candidate_msg_job_invitation';
                $("#ji").addClass("message-active");
            break;

            case 'candidate_msg_from_admin':
                page = 'candidate_msg_from_admin';
                $("#from-admin").addClass("message-active");
            break;

            case 'candidate_msg_sent_items':
                page = 'candidate_msg_sent_items';
                $("#st").addClass("message-active");
            break;
        }

        var data = {
            "operation": page,
            "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>"
        };

        $(".messages-contents").css({
            "text-align": "center"
        }).html('<img src="/img/loading.gif" />');

        $.post("/ajax", data, function(r) {
            $(".messages-contents").css({
                "text-align": "left"
            }).html(r);
        });
    });

    var data = {
        "operation": page,
        "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>"
    };
    
    $(".messages-contents").css({
        "text-align": "center"
    }).html('<img src="/img/loading.gif" />');

    $.post("/ajax", data, function(r) {
        $(".messages-contents").css({
            "text-align": "left"
        }).html(r);
    });

    $("#unread").addClass("message-active");

    $("#unread").on("click", function() {
        removeBorder();
        
        page = 'candidate_msg_unread';

        $(this).addClass("message-active");
        
        var data2 = {
            "operation": page,
            "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>"
        }
        
        $(".messages-contents").css({
            "text-align": "center"
        }).html('<img src="/img/loading.gif" />');

        $.post("/ajax", data2, function(r) {
            $(".messages-contents").css({
                "text-align": "left"
            }).html(r);
        });
    });

    $("#from-admin").on("click", function() {
        removeBorder();
        
        page = 'candidate_msg_from_admin';

        $(this).addClass("message-active");
        
        var data2 = {
            "operation": page,
            "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>"
        }
        
        $(".messages-contents").css({
            "text-align": "center"
        }).html('<img src="/img/loading.gif" />');

        $.post("/ajax", data2, function(r) {
            $(".messages-contents").css({
                "text-align": "left"
            }).html(r);
        });
    });

    $("#ji").on("click", function() {
        removeBorder();
        
        page = 'candidate_msg_job_invitation';

        $(this).addClass("message-active");
        
        var data2 = {
            "operation": page,
            "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>"
        }
        
        $(".messages-contents").css({
            "text-align": "center"
        }).html('<img src="/img/loading.gif" />');

        $.post("/ajax", data2, function(r) {
            $(".messages-contents").css({
                "text-align": "left"
            }).html(r);
        });
    });

    $("#st").on("click", function() {
        removeBorder();
        
        page = 'candidate_msg_sent_items';

        $(this).addClass("message-active");
        
        var data2 = {
            "operation": page,
            "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>"
        }
        
        $(".messages-contents").css({
            "text-align": "center"
        }).html('<img src="/img/loading.gif" />');

        $.post("/ajax", data2, function(r) {
            $(".messages-contents").css({
                "text-align": "left"
            }).html(r);
        });
    });

    function removeBorder() {
        $(".msg-btn").each(function(i) {
            $(this).removeClass("message-active");
        });
    }

    function messageViewSize() {
        var height = $(window).height();
        new_height = height - 160;

        $(".message-view").css({
            "height": new_height + "px"
        });
    }

    function readMessage(msg_id) {
        var data2 = {
            "operation": "candidate_read_message",
            "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>",
            "id": msg_id
        };

        $(".main-header").css({
            "z-index": 1
        });

        $("body").css({
            "overflow-y": "hidden"
        });

        $(".black-screen").show();

        $(".message-view").slideDown(500, function() {
            $(this).css({
               "text-align": "center",
               "line-height": new_height + "px" 
            }).html('<img src="/img/loading.gif" />');

            $.post("/ajax", data2, function(r) {
                $(".message-view").css({
                    "text-align": "left",
                    "line-height": "normal"
                }).html(r);
                
                var data3 = {
                    "operation": page,
                    "_csrfToken": "<?= $this->request->param('_csrfToken') ?>"
                };

                $.post("/ajax", data3, function(r) {
                    $(".messages-contents").html(r);
                    unread_notification();
                });
            });
        });
    }
    
    function unread_notification() {
        var data = {
            "operation": "manage_unread_counts",
            "_csrfToken": "<?= $this->request->param('_csrfToken') ?>"
        };

        if ($(".unread-notification").length > 0 && $(".nav-unread-notification").length > 0 && $(".dashboard-unread-notification").length > 0 && $(".top-unread-notification").length > 0) {
            var noti = $(".unread-notification").html();
        
            if (noti.length == 2) {
                $(".unread-notification, .nav-unread-notification, .dashboard-unread-notification, .top-unread-notification").css({
                    "padding": "2px 4px"
                });
            }
        }

        $.post("/ajax", data, function(r) {
            if (r == "") {
                $(".unread-notification, .nav-unread-notification, .dashboard-unread-notification, .top-unread-notification").hide();
            } else {
                $(".unread-notification, .nav-unread-notification, .dashboard-unread-notification, .top-unread-notification").html(r);
            }
        });
    }
</script>