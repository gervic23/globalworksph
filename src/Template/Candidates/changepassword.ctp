<div class="candidates-dashboard">
    <div class="left">
        <?= $this->element('Nav/dashboard-candidates') ?>
    </div>
    <div class="right">
        <div>&nbsp;</div>
        <div class="change_password">
            <div class="contents">
                <?= $this->element('Nav/account_nav2') ?>
                <div class="changepassword-main">
                    <?= $this->Flash->render() ?>
                    <h1>Change Password</h1>
                    <?= $this->Form->create($user) ?>
                    <?= $this->Form->control('current_password', ['type' => 'password', 'label' => 'Current Password:', 'id' => 'current_password']) ?>
                    <?= $this->Form->control('new_password', ['type' => 'password', 'label' => 'New Password:', 'id' => 'new_password']) ?>
                    <?= $this->Form->control('rpassword', ['type' => 'password', 'label' => 'Confirm Password:', 'id' => 'rpassword']) ?>
                    <div class="btn-container">
                        <?= $this->Form->submit('Save') ?>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>
<div>&nbsp;</div>
<?= $this->Html->script('candidatesdashboard') ?>
<script>
    $("#current_password").on("keyup input", function() {
        $(this).css({
            "border-color": "#ccc",
            "background": "#fff"
        });
        $(".candidates-dashboard .changepassword-main .error:nth-child(2) .error-message").hide();
    }).on("click focus", function() {
        inputAnimate("current_password");
    }).on("focusout", function() {
        focusoutAnimate("current_password");
    });
    
    $("#new_password").on("keyup input", function() {
        $(this).css({
            "border-color": "#ccc",
            "background": "#fff"
        });
        $(".candidates-dashboard .changepassword-main .error:nth-child(3) .error-message").hide();
    }).on("click focus", function() {
        inputAnimate("new_password");
    }).on("focusout", function() {
        focusoutAnimate("new_password");
    });
    
    $("#rpassword").on("keyup input", function() {
        $(this).css({
            "border-color": "#ccc",
            "background": "#fff"
        });
        $(".candidates-dashboard .changepassword-main .error:nth-child(4) .error-message").hide();
    }).on("click focus", function() {
        inputAnimate("rpassword");
    }).on("focusout", function() {
        focusoutAnimate("rpassword");
    });

    function inputAnimate(field) {
        $("#" + field).animate({
            padding: "10px 5px"
        }, 500, function() {
            $("#" + field).css({
                "font-size": "18px"
            });
        });
    }

    function focusoutAnimate(field) {
        $("#" + field).animate({
            padding: "0px"
        }, 500, function() {
            $("#" + field).css({
                "font-size": "15px"
            });
        });
    }
</script>