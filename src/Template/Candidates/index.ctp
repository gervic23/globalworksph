<?php use App\Controller\GlobalworksController; $globalworks = new GlobalworksController();  ?>
<?= $this->CKEditor->loadJs(); ?>
<div class="job-view"></div>
<div class="black-screen2"></div>
<div class="job-app-window"></div>
<div class="candidates-dashboard">
    <div class="left">
        <?= $this->element('Nav/dashboard-candidates') ?>
    </div>
    <div class="right">
        <div>&nbsp;</div>
        <div class="dashboard">
            <?php if ($globalworks->suggested_jobs()): ?>
                <div class="suggested_jobs">
                    <h1>Suggested Jobs For You</h1>
                    <ul>
                        <?php foreach ($globalworks->suggested_jobs() as $post): ?>
                            <li>
                                <div class="content">
                                    <div class="j_left">
                                        <?php if ($post['jobposts']['company_logo']): ?>
                                            <a href="javascript:" onclick="javascript: job_view(<?= $post['jobposts']['id'] ?>)"><img src="/img/users/company_logo/<?= $post['jobposts']['company_logo'] ?>" /></a>
                                        <?php endif; ?>
                                    </div>
                                    <div class="j_right">
                                        <h3><a href="javascript:" onclick="javascript: job_view(<?= $post['jobposts']['id'] ?>)"><?= $post['jobposts']['job_title'] ?></a></h3>
                                        <h5><a href="javascript:" onclick="javascript: job_view(<?= $post['jobposts']['id'] ?>)"><?= $post['jobposts']['company_name'] ?></a></h5>
                                        <div class="desc">
                                            <?php if (strlen($post['jobposts']['job_description']) >= 400):  ?>
                                                <?= substr($post['jobposts']['job_description'], 0, 400) . '...' ?>
                                            <?php else: ?>
                                                <?= $post['jobposts']['job_description'] ?>
                                            <?php endif; ?>
                                        </div>
                                        <div class="post-author">
                                            Posted by: <?= $post['users']['firstname'] . ' ' . $post['users']['lastname'] ?>
                                        </div>
                                        <div class="post-date">
                                            <?= $globalworks->when($post['jobposts']['created_at']) ?>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                    <div class="clear"></div>
                </div>
            <?php else: ?>
                <div class="latest_jobs">
                    <h1>Latest Jobs</h1>
                    <ul>
                        <?php foreach ($globalworks->last_jobposts() as $post): ?>
                            <li>
                                <div class="content">
                                    <div class="j_left">
                                        <?php if ($post['jobposts']['company_logo']): ?>
                                            <a href="javascript:" onclick="javascript: job_view(<?= $post['jobposts']['id'] ?>)"><img src="/img/users/company_logo/<?= $post['jobposts']['company_logo'] ?>" /></a>
                                        <?php endif; ?>
                                    </div>
                                    <div class="j_right">
                                        <h3><a href="javascript:" onclick="javascript: job_view(<?= $post['jobposts']['id'] ?>)"><?= $post['jobposts']['job_title'] ?></a></h3>
                                        <h5><a href="javascript:" onclick="javascript: job_view(<?= $post['jobposts']['id'] ?>)"><?= $post['jobposts']['company_name'] ?></a></h5>
                                        <div class="desc">
                                            <?php if (strlen($post['jobposts']['job_description']) >= 400):  ?>
                                                <?= substr($post['jobposts']['job_description'], 0, 400) . '...' ?>
                                            <?php else: ?>
                                                <?= $post['jobposts']['job_description'] ?>
                                            <?php endif; ?>
                                        </div>
                                        <div class="post-author">
                                            Posted by: <?= $post['users']['firstname'] . ' ' . $post['users']['lastname'] ?>
                                        </div>
                                        <div class="post-date">
                                            <?= $globalworks->when($post['jobposts']['created_at']) ?>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="clear"></div>
</div>
<div>&nbsp;</div>
<?= $this->Html->script('candidatesdashboard') ?>

<script>
    var new_height = '';

    jobview_size();

    resizeAppWindow();

    $(".suggested_jobs ul li, .latest_jobs ul li").scrollAnimate({"animation": "fadeInDown"});
    
    $(window).resize(function() {
        jobview_size();
        resizeAppWindow();
    });
    
    function jobview_size() {
        var height = $(window).height();
        var width = $(window).width() + 15;
        new_height = height - 2;
        
        $(".job-view").css({
            "height": new_height + "px",
            "width": width + "px"
        });
    }
    
    function job_view(id) {
        $(".main-header").hide();
        
        $(".job-view").animate({
            top: "0"
        }, 500, function() {
            var data = {
                "operation": "jobpost_view",
                "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>",
                "id": id
            };
            
            var new_height2 = new_height / 2 + 300;
            
            $("body").css({
                "overflow": "hidden"
            });
            
            $(this).css({
                "line-height": new_height2 + "px",
                "text-align": "center"
            }).html("<img src='/img/loading.gif' />");
            
            $.post("/ajax", data, function(r) {
                $(".job-view").css({
                    "line-height": "normal",
                    "text-align": "left"
                }).html(r);
            });
        });
    }
    
    function _apply(id) {
        var data = {
            "operation": "candidate_application",
            "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
            "id": id
        };
        
        $(".black-screen2").show();
        $(".job-app-window").slideDown(500, function() {
            $.post("/ajax", data, function(r) {
                $(".job-app-window").html(r);
            });
        });
    }

    function resizeAppWindow() {
        var height = $(window).height();
        var new_height = height - 100;

        $(".job-app-window").css({
            "height": new_height + "px"
        });
    }
</script>