<?php 
    use App\Controller\GlobalworksController;
    
    $globalworks = new GlobalworksController();

    $this->Html->script('jquery.shogotags-1.0', ['block' => true]);
    $this->Html->script('jquery.dragdropupload.1.0', ['block' => true]);
    $this->Html->css('cropper', ['block' => true]);
    $this->Html->script('cropper', ['block' => true]);

    if (!isset($resume_id)) {
        $resume_id = '';
    }

    if (!isset($resume_filename)) {
        $resume_filename = '';
    }

    if (!isset($resume_date)) {
        $resume_date = '';
    }

    if (!isset($nickname)) {
        $nickname = '';
    }

    if (!isset($job_title)) {
        $job_title = '';
    } else {
        $job_title = ($this->request->data('job_title')) ? $this->request->data('job_title') : $job_title;
    }

    if (!isset($experience)) {
        $experience = '';
    } else {
        $experience = ($this->request->data('experience')) ? $this->request->data('experience') : $experience;
    }

    if (!isset($employment_status)) {
        $employment_status = '';
    } else {
        $employment_status = ($this->request->data('experience')) ? $this->request->data('employment_status') : $employment_status;
    }

    if (!isset($skills)) {
        $skills = '';
    } else {
        $skills = ($this->request->data('skills')) ? $this->request->data('skills') : json_decode($skills, true);
    }

    if (!isset($desire_salary)) {
        $desire_salary = '';
    } else {
        $d_salary = explode(',', ($this->request->data('desire_salary')) ? $this->request->data('desire_salary') : $desire_salary);
    }

    if (!isset($education)) {
        $education = '';
    } else {
        $education = ($this->request->data('education')) ? $this->request->data('education') : $education;
    }

    if (!isset($skype_id)) {
        $skype_id = '';
    } else {
        $skype_id = ($this->request->data('skype_id')) ? $this->request->data('skype_id') : $skype_id;
    }

    if (!isset($job_subscribe)) {
        $job_subscribe = 0;
    } else {
        $job_subscribe = ($this->request->data('job_suubscribe')) ? $this->request->data('job_subscribe') : $job_subscribe;
    }

    $employment_stats = [
        '' => 'Select Status', 
        'Unemployed' => 'Unemployed', 
        'Employed' => 'Employed',
    ];
?>

<div class="candidates-dashboard">
    <div class="upload-profile-pic-view"></div>
    <div class="upload-profile-resume-view"></div>
    <div class="left">
        <?= $this->element('Nav/dashboard-candidates') ?>
    </div>
    <div class="right">
        <div class="candidates-profile">
            <h1>My Profile</h1>
            <?= $this->Flash->render() ?>
            <div class="form">
                <?= $this->Form->create() ?>
                <div class="header">
                    <div class="loading">
                        <img src="/img/loading.gif" />
                    </div>
                    <div class="img"></div>
                    <div class="img-btn-1">
                        <span id="upload">Replace</span> <span id="delete">Delete</span>
                    </div>
                    <div class="img-btn-2">
                        <span id="upload2">Upload Profile Image</span>
                    </div>
                </div>
                <div class="details">
                    <?= $this->Form->label('nickname', 'Nickname:') ?>
                    <?= $this->Form->text('nickname', ['id' => 'nickname', 'class' => 'nickname', 'autocomplete' => 'off', 'maxlength' => 255, 'value' => $nickname]) ?>
                    <?= $this->Form->label('job_title', 'Job Title:') ?>
                    <?= $this->Form->text('job_title', ['id' => 'job_title', 'class' => 'job_title', 'autocomplete' => 'off', 'maxlength' => 255, 'value' => $job_title]) ?>
                    <?= $this->Form->label('skype_id', 'Skype ID:') ?>
                    <?= $this->Form->text('skype_id', ['id' => 'skype_id', 'class' => 'skype_id', 'autocomplete' => 'off', 'maxlength' => 255, 'value' => $skype_id]) ?>
                    <?= $this->Form->hidden('experience', ['id' => 'myField', 'value' => $experience]) ?>
                    <div class="exp-container">
                        <label for="experience">Experience: <span style="font-size: 13px">Eg. (4 Years in Graphic Arts,) Each experience seperated by pressing coma.</span></label>
                        <div id="experience"></div>
                    </div>
                    <?= $this->Form->label('employment_status', 'Employment Status:') ?>
                    <?= $this->Form->select('employment_status', $employment_stats, ['id' => 'employment_status', 'class' => 'employment_status', 'default' => $employment_status]) ?>
                    <div class="skills">
                        <h3>Skills</h3>
                        <div class="skills-contents">
                            <div class="skills_contents"></div>
                            <div class="btn-container">
                                <span id="add-skill">Add Skills</span>
                            </div>
                        </div>
                    </div>
                    <?= $this->Form->input('desire_salary', ['type' => 'hidden', 'id' => 'desire_salary', 'value' => $desire_salary]) ?>
                    <div class="salary">
                        <h3>Desired Salary</h3>
                        <div class="container">
                            <ul>
                                <li>
                                    <?= $this->Form->checkbox('salary_range0', ['id' => 'salary_range0', 'value' => 'Php 8000 - Php 10000']); ?>
                                    <label for="salary_range0">Php 8,000 - Php 10,000</label>
                                </li>

                                <li>
                                    <?= $this->Form->checkbox('salary_range1', ['id' => 'salary_range1', 'value' => 'Php 10000 - Php 15000']); ?>
                                    <label for="salary_range1">Php 10,000 - Php 15,000</label>
                                </li>
                            </ul>
                        </div>
                        <div class="container">
                            <ul>
                                <li>
                                    <?= $this->Form->checkbox('salary_range2', ['id' => 'salary_range2', 'value' => 'Php 15000 - Php 20000']); ?>
                                    <label for="salary_range2">Php 15,000 - Php 20,000</label>
                                </li>
                                <li>
                                    <?= $this->Form->checkbox('salary_range3', ['id' => 'salary_range3', 'value' => 'Php 20000 - Php 25000']); ?>
                                    <label for="salary_range3">Php 20,000 - Php 25,000</label>
                                </li>
                            </ul>
                        </div>
                        <div class="container">
                            <ul>
                                <li>
                                    <?= $this->Form->checkbox('salary_range4', ['id' => 'salary_range4', 'value' => 'Php 25000 - Php 30000']); ?>
                                    <label for="salary_range4">Php 25,000 - Php 30,000</label>
                                </li>
                                <li>
                                    <?= $this->Form->checkbox('salary_range5', ['id' => 'salary_range5', 'value' => 'Php 30000 - Php 35000']); ?>
                                    <label for="salary_range5">Php 30,000 - Php 35,000</label>
                                </li>
                            </ul>
                        </div>
                        <div class="container">
                            <ul>
                                <li>
                                    <?= $this->Form->checkbox('salary_range6', ['id' => 'salary_range6', 'value' => 'Php 35000 - Php 40000']); ?>
                                    <label for="salary_range6">Php 35,000 - Php 40,000</label>
                                </li>
                                <li>
                                    <?= $this->Form->checkbox('salary_range7', ['id' => 'salary_range7', 'value' => 'Php 40000 - Php 45000']); ?>
                                    <label for="salary_range7">Php 40,000 - Php 45,000</label>
                                </li>
                            </ul>
                        </div>
                        <div class="container">
                            <ul>
                                <li>
                                    <?= $this->Form->checkbox('salary_range8', ['id' => 'salary_range8', 'value' => 'Php 45000 - Php 50000']); ?>
                                    <label for="salary_range8">Php 45,000 - Php 50,000</label>
                                </li>
                                <li>
                                    <?= $this->Form->checkbox('salary_range9', ['id' => 'salary_range9', 'value' => 'Morethan Php 50000']); ?>
                                    <label for="salary_range9">Morethan Php 50,000</label>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="education">
                        <h3>Education Attainment</h3>
                        <div class="content">
                            <ul>
                                <li>
                                    <input type="radio" name="education" id="education-1" value="College/Bachelor Degree" <?= ($education == 'College/Bachelor Degree') ? 'checked' : ''; ?> />
                                    <label for="education-1">College/Bachelor Degree</label>
                                    <div class="check"></div>
                                </li>
                                <li>
                                    <input type="radio" name="education" id="education-2" value="College/Associated Degree" <?= ($education == 'College/Associated Degree') ? 'checked' : '' ?> />
                                    <label for="education-2">College/Associated Degree</label>
                                    <div class="check"></div>
                                </li>
                                <li>
                                    <input type="radio" name="education" id="education-3" value="Post Graduate Degree" <?= ($education == 'Post Graduate Degree') ? 'selected' : ''; ?> />
                                    <label for="education-3">Post Graduate Degree</label>
                                    <div class="check"></div>
                                </li>
                                <li>
                                    <input type="radio" name="education" id="education-4" value="Highschool Graduate" <?= ($education == 'Highschool Graduate') ? 'checked' : ''; ?> />
                                    <label for="education-4">Highschool Graduate</label>
                                    <div class="check"></div>
                                </li>
                                <li>
                                    <input type="radio" name="education" id="education-5" value="Not Graduated Highschool" <?= ($education == 'Not Graduated Highschool') ? 'checked' : ''; ?> />
                                    <label for="education-5">Not Graduated Highschool</label>
                                    <div class="check"></div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="resume">
                        <h3>My Resume</h3>
                        <div class="resume-loading">
                            <img src="/img/loading.gif" />
                        </div>
                        <div class="resume-empty">
                            You don't have a resume. <a id="upload-resume" href="javascript:">Upload Now</a>.
                        </div>
                        <div class="resume-contents">
                            <a><img src="/img/resume-icon.png" /></a> <a><?= $resume_filename; ?></a> <span id="upload-resume2">Update</span> <span id="delete-resume">Delete</span>
                        </div>
                    </div>
                    <div class="subscribe">
                        <?= $this->Form->hidden('job_subscribe', ['id' => 'job_subscribe', 'value' =>  $job_subscribe]); ?>
                        <input type="checkbox" name="j_subscribe" id="j_subscribe" />
                        <label for="j_subscribe">Subscribe for latest jobs.</label>
                        <div class="check"></div>
                    </div>
                    <div class="btn-container">
                        <?= $this->Form->button('Save', ['type' => 'submit', 'id' => 'save']); ?>
                    </div>
                </div>
            <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>
<div>&nbsp;</div>
<?= $this->Html->script('candidatesdashboard') ?>
<?= $this->Html->script('add-skills') ?>
<script>
    var new_height = 0;
    
    uploadPictureSize();
    loadResume();
    
    $(window).resize(function() {
        uploadPictureSize();
    });
    
    <?php if ($profile_picture): ?>
       loadProfilePic("<?= $profile_picture ?>");
    <?php else: ?>
        loadProfilePic();
    <?php endif; ?>

    $("#nickname").on("click focus", function() {
        inputAnimate("nickname");
    }).focusout(function() {
        focusoutAnimate("nickname");
    });

    $("#job_title").on("click focus", function() {
        inputAnimate("job_title");
    }).focusout(function() {
        focusoutAnimate("job_title");
    });

    $("#skype_id").on("click focus", function() {
        inputAnimate("skype_id");
    }).focusout(function() {
        focusoutAnimate("skype_id");
    });
    
    $("#experience").shogotags({
        tag_border_color: "#ff4338",
		tag_background_color: "#ff4338",
        tag_border_radius: "5px",
        data: "<?= $experience ?>"
    });

    <?php if ($skills): ?>
        <?php foreach ($skills as $skill): ?>
            skill_add("<?= $skill ?>")
        <?php endforeach; ?>
    <?php endif; ?>
    
    $("#upload, #upload2").on("click", function() {
        $(".main-header").hide();
        
        $(".upload-profile-pic-view").animate({
            top: "0"
        }, 500, function() {
            var data = {
                "operation": "upload_candidates_profile_picture",
                "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>"
            };
            
            var new_height2 = new_height / 2 + 300;
            
            $("body").css({
                "overflow": "hidden"
            });
            
            $(this).css({
                "line-height": new_height2 + "px",
                "text-align": "center"
            }).html("<img src='/img/loading.gif' />");
            
            $.post("/ajax", data, function(r) {
                $(".upload-profile-pic-view").css({
                    "line-height": "normal",
                    "text-align": "left"
                }).html(r);
            });
        });
    });
    
     $("#delete").on("click", function() {
        var c = confirm("Are you sure you want to delete you profile picture?");

        if (c) {
            var data = {
                "operation": "image_delete",
                "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>"
            };

            $.post("/ajax", data, function(r) {
                if (r == "ok") {
                    $(".img").hide();
                    $(".img-btn-1, .img-btn-2").hide();
                    $(".loading").show();

                    setTimeout(function() {
                        $(".loading").hide();
                        $(".img").fadeIn(500).html('<img src="/img/users/nopropic.png" />');
                        $(".img-btn-2").show();
                    }, 2500);
                }
            });
        }
    });
    
    $("#upload-resume, #upload-resume2").on("click", function() {
        $(".main-header").hide();
        
        $(".upload-profile-resume-view").animate({
            top: "0"
        }, 500, function() {
            var data = {
                "operation": "upload_candidates_resume",
                "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>"
            };
            
            var new_height2 = new_height / 2 + 300;
            
            $("body").css({
                "overflow-y": "hidden"
            });
            
            $(this).css({
                "line-height": new_height2 + "px",
                "text-align": "center"
            }).html("<img src='/img/loading.gif' />");
            
            $.post("/ajax", data, function(r) {
                $(".upload-profile-resume-view").css({
                    "line-height": "normal",
                    "text-align": "left"
                }).html(r);
            });
        });
    });
    
    $("#delete-resume").on("click", function() {
        var c = confirm("Are you sure you want to delete you resume?");
        
        if (c) {
            var data = {
                "operation": "delete_resume",
                "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>"
            };
        }
        
        $.post("/ajax", data, function(r) {
            if (r == "ok") {
                loadResume();
            }
        });
    });
    
    function uploadPictureSize() {
        var height = $(window).height();
        var width = $(window).width() + 15;
        new_height = height - 2;
        
        $(".upload-profile-pic-view, .upload-profile-resume-view").css({
            "height": new_height + "px",
            "width": width + "px"
        });
    }
    
    function loadProfilePic(image, cropped = false) {
        if (cropped) {
            $(".upload-profile-pic-view").animate({
                top: "-100%"
            }, 500, function() {
                $(this).html("");
                
                if ($(window).width() > 800) {
                    $(".main-header").show();
                }

                $("body").css({
                    "overflow-y": "auto"
                });
                
                $(".img").hide();
                $(".img-btn-1, .img-btn-2").hide();
                $(".loading").show();

                setTimeout(function() {
                    $(".loading").hide();
                    $(".img").fadeIn(500).html('<img src="/img/users/profile_pictures/' + image + '" />');
                    $(".img-btn-1").show();
                    dashboardHeight();
                }, 2500);
            });
        } else {
            if (image) {
                $(".loading").show();

                setTimeout(function() {
                    $(".loading").hide();
                    $(".img").fadeIn(500).html('<img src="/img/users/profile_pictures/' + image + '" />');
                    $(".img-btn-1").show();
                    dashboardHeight();
                }, 1000);
            } else {
                $(".loading").show();

                setTimeout(function() {
                    $(".loading").hide();
                    $(".img").fadeIn(500).html('<img src="/img/users/nopropic.png" />');
                    $(".img-btn-2").show();
                    dashboardHeight();
                }, 1000);
            }
        }
    }
    
    <?php if ($desire_salary): $c = 0;?>
        var salary_range = [];
        <?php foreach ($d_salary as $salary): ?>
            salary_range[<?= $c ?>] = "<?= $salary; ?>";
        <?php $c++; endforeach; ?>
    
        <?php if (in_array('Php 8000 - Php 10000', $d_salary)): ?>
            $("#salary_range0").attr("checked", "checked");
        <?php endif; ?>
    
        <?php if (in_array('Php 10000 - Php 15000', $d_salary)): ?>
            $("#salary_range1").attr("checked", "checked");
        <?php endif; ?>
    
        <?php if (in_array('Php 15000 - Php 20000', $d_salary)): ?>
            $("#salary_range2").attr("checked", "checked");
        <?php endif; ?>
    
        <?php if (in_array('Php 20000 - Php 25000', $d_salary)): ?>
            $("#salary_range3").attr("checked", "checked");
        <?php endif; ?>
    
        <?php if (in_array('Php 25000 - Php 30000', $d_salary)): ?>
            $("#salary_range4").attr("checked", "checked");
        <?php endif; ?>
    
        <?php if (in_array('Php 30000 - Php 35000', $d_salary)): ?>
            $("#salary_range5").attr("checked", "checked");
        <?php endif; ?>
    
        <?php if (in_array('Php 35000 - Php 40000', $d_salary)): ?>
            $("#salary_range6").attr("checked", "checked");
        <?php endif; ?>
    
        <?php if (in_array('Php 40000 - Php 45000', $d_salary)): ?>
            $("#salary_range7").attr("checked", "checked");
        <?php endif; ?>
    
        <?php if (in_array('Php 45000 - Php 50000', $d_salary)): ?>
            $("#salary_range8").attr("checked", "checked");
        <?php endif; ?>
    
        <?php if (in_array('Morethan Php 50000', $d_salary)): ?>
            $("#salary_range9").attr("checked", "checked");
        <?php endif; ?>
    <?php else: ?>
        var salary_range = [];
    <?php endif; ?>
    
    $("#salary_range0").on("change", function() {
        var range = $(this).val();
        if ($(this).is(":checked")) {
            salary_range.push(range);
            addSalaryRange();
        } else {
            removeSalaryRange(salary_range.indexOf(range));
        }
    });
    
    $("#salary_range1").on("change", function() {
        var range = $(this).val();
        if ($(this).is(":checked")) {
            salary_range.push(range);
            addSalaryRange();
        } else {
            removeSalaryRange(salary_range.indexOf(range));
        }
    });
    
    $("#salary_range2").on("change", function() {
        var range = $(this).val();
        if ($(this).is(":checked")) {
            salary_range.push(range);
            addSalaryRange();
        } else {
            removeSalaryRange(salary_range.indexOf(range));
        }
    });
    
    $("#salary_range3").on("change", function() {
        var range = $(this).val();
        if ($(this).is(":checked")) {
            salary_range.push(range);
            addSalaryRange();
        } else {
            removeSalaryRange(salary_range.indexOf(range));
        }
    });
    
    $("#salary_range4").on("change", function() {
        var range = $(this).val();
        if ($(this).is(":checked")) {
            salary_range.push(range);
            addSalaryRange();
        } else {
            removeSalaryRange(salary_range.indexOf(range));
        }
    });
    
    $("#salary_range5").on("change", function() {
        var range = $(this).val();
        if ($(this).is(":checked")) {
            salary_range.push(range);
            addSalaryRange();
        } else {
            removeSalaryRange(salary_range.indexOf(range));
        }
    });
    
    $("#salary_range6").on("change", function() {
        var range = $(this).val();
        if ($(this).is(":checked")) {
            salary_range.push(range);
            addSalaryRange();
        } else {
            removeSalaryRange(salary_range.indexOf(range));
        }
    });
    
    $("#salary_range7").on("change", function() {
        var range = $(this).val();
        if ($(this).is(":checked")) {
            salary_range.push(range);
            addSalaryRange();
        } else {
            removeSalaryRange(salary_range.indexOf(range));
        }
    });
    
    $("#salary_range8").on("change", function() {
        var range = $(this).val();
        if ($(this).is(":checked")) {
            salary_range.push(range);
            addSalaryRange();
        } else {
            removeSalaryRange(salary_range.indexOf(range));
        }
    });
    
    $("#salary_range9").on("change", function() {
        var range = $(this).val();
        if ($(this).is(":checked")) {
            salary_range.push(range);
            addSalaryRange();
        } else {
            removeSalaryRange(salary_range.indexOf(range));
        }
    });
    
    function addSalaryRange() {
        $("#desire_salary").attr("value", salary_range);
    }
    
    function removeSalaryRange(index) {
        range = salary_range[index];
        salary_range.splice(index, 1);
        addSalaryRange();
    }
    
    <?php if ($job_subscribe == 1): ?>
        $("#jobs_subscribe").attr("value", <?= $job_subscribe ?>);
        $("#j_subscribe").attr("checked", "checked");
    <?php endif; ?>
    
    $("#j_subscribe").on("change", function() {
        if ($(this).is(":checked")) {
            $("#job_subscribe").attr("value", 1);
        } else {
            $("#job_subscribe").attr("value", 0);
        }
    });
    
    function loadResume() {
        $(".upload-profile-resume-view").animate({
            top: "-100%"
        }, 500, function() {
            $(this).html("");
            
            if ($(window).width() > 800) {
                $(".main-header").show();
            }

            $("body").css({
                "overflow": "auto"
            });
            
            $(".resume-empty").hide();
            $(".resume-contents").hide();
            $(".resume-loading").show();

            dashboardHeight();
            
            var data = {
                "operation": "user_resume_checker",
                "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>"
            };
            
            $.post("/ajax", data, function(r) {
                if (r == 1) {
                    setTimeout(function() {
                        $(".resume-loading").hide();
                        
                        var data2 = {
                            "operation": "user_resume_load",
                            "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>"
                        };
                        
                        $.post("/ajax", data2, function(r2) {
                            var resume = r2.split("|");
                            var id = resume[0];
                            var filename = resume[1];
                            var date = resume[2];
                            
                            $(".resume-contents a").attr("title", filename + " " + date);
                            $(".resume-contents a").attr("href", "/download/" + id);
                            $(".resume-contents a:nth-child(2)").html(_split_end(filename, ".", 20));
                            $(".resume-contents").show();
                            dashboardHeight();
                        });
                    }, 2500);
                } else {
                    setTimeout(function() {
                        $(".resume-loading").hide();
                        $(".resume-empty").show();
                        dashboardHeight();
                    }, 2500);
                }
            });
        });
    }

    function _split_end(str, splitter, len) {
        var str_split = str.split(splitter);
        var last = str_split.length - 1;

        var new_string = '';

        for (var i = 0; i < last; i++) {
            new_string += str_split[i] + splitter;
        }

        if (new_string.length > len) {
            return new_string.substring(0, len) + ".." + str_split[last];
        }

        return new_string + str_split[last];
    }

    function inputAnimate(field) {
        $("#" + field).animate({
            padding: "10px 5px"
        }, 500, function() {
            $("#" + field).css({
                "font-size": "18px"
            });
        });
    }

    function focusoutAnimate(field) {
        $("#" + field).animate({
            padding: "0px"
        }, 500, function() {
            $("#" + field).css({
                "font-size": "15px"
            });
        });
    }
</script>