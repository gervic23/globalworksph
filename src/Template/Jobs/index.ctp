<?php
    use App\Controller\GlobalworksController;
    $globalworks = new GlobalworksController();

    echo $this->CKEditor->loadJs();
?>

<div class="job-view"></div>
<div class="black-screen2"></div>
<div class="job-app-window"></div>
<div class="jobs-all">
    <?php if ($jobposts_count == 0): ?>
        <div class="jobs-all-empty">
            fsdfsdf
        </div>
    <?php else: ?>
        <div class="contents">
            <?php foreach ($jobposts as $post): ?>
                <div class="the-post">
                    <?php if ($post['company_logo']): ?>
                        <div class="left">
                            <a onclick="javascript: job_view(<?= $post['id'] ?>)" href="javascript:"><img alt="company_logo" src="/img/users/company_logo/<?= $post['company_logo'] ?>" /></a>
                        </div>
                    <?php endif; ?>
                    <div class="right">
                        <div class="the-header">
                            <h1><a onclick="javascript: job_view(<?= $post['id'] ?>)" href="javascript:"><?= $post['job_title'] ?></a></h1>
                            <h3><?= $post['company_name'] ?></h3> 
                        </div>
                        <div class="description">
                            <?= (strlen($post['job_description']) >= 400) ? substr($post['job_description'], 0, 400) . '...' : $post['job_description']; ?>
                        </div>
                        <div class="date">
                            <?= $globalworks->when($post['created_at']) ?> 
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            <?php endforeach; ?>
            <?= $globalworks->_paginate($jobposts_count, $limit, $page) ?>

            <?= $globalworks->_m_paginate($jobposts_count, $limit, $page) ?>
        </div>
    <?php endif; ?>
</div>

<script>
    var new_height = '';
    
    resizeJobsAll();
    jobview_size();
    
    $(window).resize(function() {
        resizeJobsAll();
        jobview_size();
    });
    
    function resizeJobsAll() {
        var height = $(window).height();
        new_height = height - 40;
        var jobs_all = $(".jobs-all").height();
        
        if (jobs_all < height) {
            $(".jobs-all").css({
                "height": new_height + "px"
            });
        }
    }
    
    function jobview_size() {
        var height = $(window).height();
        var width = $(window).width() + 15;
        new_height = height - 2;
        
        $(".job-view").css({
            "height": new_height + "px",
            "width": width + "px"
        });
    }
    
    function job_view(id) {
        $(".main-header").hide();
        
        $(".job-view").animate({
            top: "0"
        }, 500, function() {
            var data = {
                "operation": "jobpost_view",
                "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>",
                "id": id
            };
            
            var new_height2 = new_height / 2 + 300;
            
            $("body").css({
                "overflow": "hidden"
            });
            
            $(this).css({
                "line-height": new_height2 + "px",
                "text-align": "center"
            }).html("<img src='/img/loading.gif' />");
            
            $.post("/ajax", data, function(r) {
                $(".job-view").css({
                    "line-height": "normal",
                    "text-align": "left"
                }).html(r);
            });
        });
    }
    
    function _apply(id) {
        var data = {
            "operation": "candidate_application",
            "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
            "id": id
        };
        
        $(".black-screen2").show();
        $(".job-app-window").slideDown(500, function() {
            $.post("/ajax", data, function(r) {
                $(".job-app-window").html(r);
            });
        });
    }
</script>