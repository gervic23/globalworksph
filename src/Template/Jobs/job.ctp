<?php use App\Controller\GlobalworksController; $globalworks = new GlobalworksController(); ?>

<div class="single-jobpost">
    <?php foreach ($jobpost as $post): ?>
        <?php
            $nickname = $globalworks->getnickname($post['user_id']);
    
            if (!$nickname) {
                $fname = $globalworks->getfirstname($post['user_id']);
                $lname = $globalworks->getlastname($post['user_id']);
                
                $nickname = $fname . ' ' . $lname;
            }
        ?>
        <div class="jobpost-main-header">
            <div class="header-logo">
                <?php if ($post['header_image']): ?>
                    <div id="header" class="header">
                        <img alt="banner-image" src="/img/users/job_header/<?= $post['header_image'] ?>" />
                    </div>
                <?php endif; ?>

                <?php if ($post['company_logo']): ?>
                    <div class="logo">
                        <img alt="banner-image" src="/img/users/company_logo/<?= $post['company_logo'] ?>" />
                    </div>
                <?php endif; ?>
            </div>
            <h1><?= $post['job_title'] ?></h1>
            <h4><?= $post['company_name'] ?></h4>
        </div>
        <div class="contents">
            <div class="left">
                <div class="description">
                    <h2>Job Description</h2>
                    <?= $globalworks->filter_description($post['job_description']) ?>
                </div>
                <?php if ($post['job_website']): ?>
                    <div class="website">
                        <h2>Company Website</h2>
                        <?php
                            $str1 = substr($post['job_website'], 0, 7) ;
                            $str2 = substr($post['job_website'], 0, 8) ;

                            if ($str1 == 'http://' || $str2 == 'https://') {
                                echo '<a target="_blank" href="' . $post['job_website'] . '">' . $post['job_website'] . '</a>';
                            } else {
                                echo '<a target="_blank" href="http://' . $post['job_website'] . '">http://' . $post['job_website'] . '</a>';
                            }
                        ?>
                    </div>
                <?php endif; ?>
            </div>
            <div class="right">
                <div class="job-details">
                    <h2>Job Details</h2>
                    <table>
                        <tr>
                            <td>Job Category:</td>
                            <td><?= $post['job_category'] ?></td>
                        </tr>
                        <tr>
                            <td>Job Type:</td>
                            <td><?= $post['job_type'] ?></td>
                        </tr>
                        <?php if ($this->request->data('job-base')): ?>
                            <tr>
                                <td>Job Base:</td>
                                <td><?= $post['job_migration'] ?></td>
                            </tr>
                        <?php endif; ?>
                        <tr>
                            <td>Working Experience:</td>
                            <td><?= ($post['job_years_experience']) ?></td>
                        </tr>
                        <tr>
                            <td>Posted By:</td>
                            <td><?= $nickname ?></td>
                        </tr>
                        <tr>
                            <td>Closing Date:</td>
                            <td><?= $globalworks->datetimetodate($post['closing_date']) ?></td>
                        </tr>
                        <tr>
                            <td>Created At:</td>
                            <td><?= $globalworks->when($post['created_at']) ?></td>
                        </tr>
                    </table>
                    <div class="fb-like">
                        <iframe src="https://www.facebook.com/plugins/like.php?href=<?= urldecode($globalworks->_url . '/job/' .$id) ?>&width=154&layout=button_count&action=like&size=large&show_faces=true&share=true&height=46&appId=595785607158325" width="154" height="46" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                    </div>
                </div>
                
                <?php if ($post['job_email'] || $post['job_contact_number']): ?>
                    <div class="contact-info">
                        <h2>Contact Information</h2>
                        <table>
                            <?php if ($post['job_email']): ?>
                                <tr>
                                    <td>Email:</td>
                                    <td><?= $post['job_email'] ?></td>
                                </tr>
                            <?php endif; ?>

                            <?php if ($post['job_contact_number']): ?>
                                <tr>
                                    <td>Contact Number:</td>
                                    <td><?= $post['job_contact_number'] ?></td>
                                </tr>
                            <?php endif; ?>
                        </table>
                    </div>
                <?php endif; ?>
                
                <?php if ($post['job_location']): ?>
                    <div class="location">
                        <h2>Location</h2>
                        <?= $post['job_location'] ?>
                        <div class="country"><?= $globalworks->country_to_str($post['country']) ?>.</div>
                    </div>
                <?php endif; ?>
                
                <?php if ($post['googglemap']): $location = urlencode($post['job_location'] . ' ' . $post['county']); ?>
                    <div class="googlemap">
                        <h2>Map</h2>
                        <iframe
                            frameborder="0" style="border:0"
                            src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDg3sqfQCdmusqPP_M4j40R_xuIBpiUSG8
                            &q='<?= $location ?>" allowfullscreen>
                        </iframe>
                    </div>
                <?php endif; ?>

                <?php if ($post['job_qualifications']): ?>
                    <div class="qualifications">
                        <h2>Qualifications</h2>
                        <ul>
                            <?php foreach (json_decode($post['job_qualifications']) as $qlf): ?>
                                <?php if ($qlf !== ''): ?>
                                    <li><?= $qlf ?></li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                <?php endif; ?>
                
                <?php if ($post['job_responsibilities']): ?>
                    <div class="responsibilities">
                        <h2>Responsibilities</h2>
                        <ul>
                            <?php foreach (json_decode($post['job_responsibilities']) as $res): ?>
                                <?php if ($res !== ''): ?>
                                    <li><?= $res ?></li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                <?php endif; ?>
            </div>
            <div class="clear"></div>
        </div>
    <?php endforeach; ?>
</div>