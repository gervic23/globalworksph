<?php 
    use App\Controller\GlobalworksController; $globalworks = new GlobalworksController();

    echo $this->CKEditor->loadJs();

    $countries = [
    'All' => 'ALL COUNTRIES',
    'AF'=>'AFGHANISTAN',
    'AL'=>'ALBANIA',
    'DZ'=>'ALGERIA',
    'AS'=>'AMERICAN SAMOA',
    'AD'=>'ANDORRA',
    'AO'=>'ANGOLA',
    'AI'=>'ANGUILLA',
    'AQ'=>'ANTARCTICA',
    'AG'=>'ANTIGUA AND BARBUDA',
    'AR'=>'ARGENTINA',
    'AM'=>'ARMENIA',
    'AW'=>'ARUBA',
    'AU'=>'AUSTRALIA',
    'AT'=>'AUSTRIA',
    'AZ'=>'AZERBAIJAN',
    'BS'=>'BAHAMAS',
    'BH'=>'BAHRAIN',
    'BD'=>'BANGLADESH',
    'BB'=>'BARBADOS',
    'BY'=>'BELARUS',
    'BE'=>'BELGIUM',
    'BZ'=>'BELIZE',
    'BJ'=>'BENIN',
    'BM'=>'BERMUDA',
    'BT'=>'BHUTAN',
    'BO'=>'BOLIVIA',
    'BA'=>'BOSNIA AND HERZEGOVINA',
    'BW'=>'BOTSWANA',
    'BV'=>'BOUVET ISLAND',
    'BR'=>'BRAZIL',
    'IO'=>'BRITISH INDIAN OCEAN TERRITORY',
    'BN'=>'BRUNEI DARUSSALAM',
    'BG'=>'BULGARIA',
    'BF'=>'BURKINA FASO',
    'BI'=>'BURUNDI',
    'KH'=>'CAMBODIA',
    'CM'=>'CAMEROON',
    'CA'=>'CANADA',
    'CV'=>'CAPE VERDE',
    'KY'=>'CAYMAN ISLANDS',
    'CF'=>'CENTRAL AFRICAN REPUBLIC',
    'TD'=>'CHAD',
    'CL'=>'CHILE',
    'CN'=>'CHINA',
    'CX'=>'CHRISTMAS ISLAND',
    'CC'=>'COCOS (KEELING) ISLANDS',
    'CO'=>'COLOMBIA',
    'KM'=>'COMOROS',
    'CG'=>'CONGO',
    'CD'=>'CONGO, THE DEMOCRATIC REPUBLIC OF THE',
    'CK'=>'COOK ISLANDS',
    'CR'=>'COSTA RICA',
    'CI'=>'COTE D IVOIRE',
    'HR'=>'CROATIA',
    'CU'=>'CUBA',
    'CY'=>'CYPRUS',
    'CZ'=>'CZECH REPUBLIC',
    'DK'=>'DENMARK',
    'DJ'=>'DJIBOUTI',
    'DM'=>'DOMINICA',
    'DO'=>'DOMINICAN REPUBLIC',
    'TP'=>'EAST TIMOR',
    'EC'=>'ECUADOR',
    'EG'=>'EGYPT',
    'SV'=>'EL SALVADOR',
    'GQ'=>'EQUATORIAL GUINEA',
    'ER'=>'ERITREA',
    'EE'=>'ESTONIA',
    'ET'=>'ETHIOPIA',
    'FK'=>'FALKLAND ISLANDS (MALVINAS)',
    'FO'=>'FAROE ISLANDS',
    'FJ'=>'FIJI',
    'FI'=>'FINLAND',
    'FR'=>'FRANCE',
    'GF'=>'FRENCH GUIANA',
    'PF'=>'FRENCH POLYNESIA',
    'TF'=>'FRENCH SOUTHERN TERRITORIES',
    'GA'=>'GABON',
    'GM'=>'GAMBIA',
    'GE'=>'GEORGIA',
    'DE'=>'GERMANY',
    'GH'=>'GHANA',
    'GI'=>'GIBRALTAR',
    'GR'=>'GREECE',
    'GL'=>'GREENLAND',
    'GD'=>'GRENADA',
    'GP'=>'GUADELOUPE',
    'GU'=>'GUAM',
    'GT'=>'GUATEMALA',
    'GN'=>'GUINEA',
    'GW'=>'GUINEA-BISSAU',
    'GY'=>'GUYANA',
    'HT'=>'HAITI',
    'HM'=>'HEARD ISLAND AND MCDONALD ISLANDS',
    'VA'=>'HOLY SEE (VATICAN CITY STATE)',
    'HN'=>'HONDURAS',
    'HK'=>'HONG KONG',
    'HU'=>'HUNGARY',
    'IS'=>'ICELAND',
    'IN'=>'INDIA',
    'ID'=>'INDONESIA',
    'IR'=>'IRAN, ISLAMIC REPUBLIC OF',
    'IQ'=>'IRAQ',
    'IE'=>'IRELAND',
    'IL'=>'ISRAEL',
    'IT'=>'ITALY',
    'JM'=>'JAMAICA',
    'JP'=>'JAPAN',
    'JO'=>'JORDAN',
    'KZ'=>'KAZAKSTAN',
    'KE'=>'KENYA',
    'KI'=>'KIRIBATI',
    'KP'=>'KOREA DEMOCRATIC PEOPLES REPUBLIC OF',
    'KR'=>'KOREA REPUBLIC OF',
    'KW'=>'KUWAIT',
    'KG'=>'KYRGYZSTAN',
    'LA'=>'LAO PEOPLES DEMOCRATIC REPUBLIC',
    'LV'=>'LATVIA',
    'LB'=>'LEBANON',
    'LS'=>'LESOTHO',
    'LR'=>'LIBERIA',
    'LY'=>'LIBYAN ARAB JAMAHIRIYA',
    'LI'=>'LIECHTENSTEIN',
    'LT'=>'LITHUANIA',
    'LU'=>'LUXEMBOURG',
    'MO'=>'MACAU',
    'MK'=>'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF',
    'MG'=>'MADAGASCAR',
    'MW'=>'MALAWI',
    'MY'=>'MALAYSIA',
    'MV'=>'MALDIVES',
    'ML'=>'MALI',
    'MT'=>'MALTA',
    'MH'=>'MARSHALL ISLANDS',
    'MQ'=>'MARTINIQUE',
    'MR'=>'MAURITANIA',
    'MU'=>'MAURITIUS',
    'YT'=>'MAYOTTE',
    'MX'=>'MEXICO',
    'FM'=>'MICRONESIA, FEDERATED STATES OF',
    'MD'=>'MOLDOVA, REPUBLIC OF',
    'MC'=>'MONACO',
    'MN'=>'MONGOLIA',
    'MS'=>'MONTSERRAT',
    'MA'=>'MOROCCO',
    'MZ'=>'MOZAMBIQUE',
    'MM'=>'MYANMAR',
    'NA'=>'NAMIBIA',
    'NR'=>'NAURU',
    'NP'=>'NEPAL',
    'NL'=>'NETHERLANDS',
    'AN'=>'NETHERLANDS ANTILLES',
    'NC'=>'NEW CALEDONIA',
    'NZ'=>'NEW ZEALAND',
    'NI'=>'NICARAGUA',
    'NE'=>'NIGER',
    'NG'=>'NIGERIA',
    'NU'=>'NIUE',
    'NF'=>'NORFOLK ISLAND',
    'MP'=>'NORTHERN MARIANA ISLANDS',
    'NO'=>'NORWAY',
    'OM'=>'OMAN',
    'PK'=>'PAKISTAN',
    'PW'=>'PALAU',
    'PS'=>'PALESTINIAN TERRITORY, OCCUPIED',
    'PA'=>'PANAMA',
    'PG'=>'PAPUA NEW GUINEA',
    'PY'=>'PARAGUAY',
    'PE'=>'PERU',
    'PH'=>'PHILIPPINES',
    'PN'=>'PITCAIRN',
    'PL'=>'POLAND',
    'PT'=>'PORTUGAL',
    'PR'=>'PUERTO RICO',
    'QA'=>'QATAR',
    'RE'=>'REUNION',
    'RO'=>'ROMANIA',
    'RU'=>'RUSSIAN FEDERATION',
    'RW'=>'RWANDA',
    'SH'=>'SAINT HELENA',
    'KN'=>'SAINT KITTS AND NEVIS',
    'LC'=>'SAINT LUCIA',
    'PM'=>'SAINT PIERRE AND MIQUELON',
    'VC'=>'SAINT VINCENT AND THE GRENADINES',
    'WS'=>'SAMOA',
    'SM'=>'SAN MARINO',
    'ST'=>'SAO TOME AND PRINCIPE',
    'SA'=>'SAUDI ARABIA',
    'SN'=>'SENEGAL',
    'SC'=>'SEYCHELLES',
    'SL'=>'SIERRA LEONE',
    'SG'=>'SINGAPORE',
    'SK'=>'SLOVAKIA',
    'SI'=>'SLOVENIA',
    'SB'=>'SOLOMON ISLANDS',
    'SO'=>'SOMALIA',
    'ZA'=>'SOUTH AFRICA',
    'GS'=>'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS',
    'ES'=>'SPAIN',
    'LK'=>'SRI LANKA',
    'SD'=>'SUDAN',
    'SR'=>'SURINAME',
    'SJ'=>'SVALBARD AND JAN MAYEN',
    'SZ'=>'SWAZILAND',
    'SE'=>'SWEDEN',
    'CH'=>'SWITZERLAND',
    'SY'=>'SYRIAN ARAB REPUBLIC',
    'TW'=>'TAIWAN, PROVINCE OF CHINA',
    'TJ'=>'TAJIKISTAN',
    'TZ'=>'TANZANIA, UNITED REPUBLIC OF',
    'TH'=>'THAILAND',
    'TG'=>'TOGO',
    'TK'=>'TOKELAU',
    'TO'=>'TONGA',
    'TT'=>'TRINIDAD AND TOBAGO',
    'TN'=>'TUNISIA',
    'TR'=>'TURKEY',
    'TM'=>'TURKMENISTAN',
    'TC'=>'TURKS AND CAICOS ISLANDS',
    'TV'=>'TUVALU',
    'UG'=>'UGANDA',
    'UA'=>'UKRAINE',
    'AE'=>'UNITED ARAB EMIRATES',
    'GB'=>'UNITED KINGDOM',
    'US'=>'UNITED STATES',
    'UM'=>'UNITED STATES MINOR OUTLYING ISLANDS',
    'UY'=>'URUGUAY',
    'UZ'=>'UZBEKISTAN',
    'VU'=>'VANUATU',
    'VE'=>'VENEZUELA',
    'VN'=>'VIET NAM',
    'VG'=>'VIRGIN ISLANDS, BRITISH',
    'VI'=>'VIRGIN ISLANDS, U.S.',
    'WF'=>'WALLIS AND FUTUNA',
    'EH'=>'WESTERN SAHARA',
    'YE'=>'YEMEN',
    'YU'=>'YUGOSLAVIA',
    'ZM'=>'ZAMBIA',
    'ZW'=>'ZIMBABWE',
  ];
?>

<div class="job-view"></div>
<div class="black-screen2"></div>
<div class="job-app-window"></div>
<div class="search-results">
    <div class="s_left">
        <?php if ($count == 0): ?>
            <div class="search-count">
                There are <?= $count ?> results found.
            </div>
        <?php else: ?>
            <div class="contents">
                <div class="search-count">
                    There are <?= $count ?> results found.
                </div>
                <div class="m-advance-search-link">
                    <a href="/jobs/search">Advance Search</a>
                </div>
                <?php foreach ($jobposts as $post): ?>
                    <div class="the-post">
                        <?php if ($post['company_logo']): ?>
                            <div class="left">
                                <a onclick="javascript: job_view(<?= $post['id'] ?>)" href="javascript:"><img alt="company_logo" src="/img/users/company_logo/<?= $post['company_logo'] ?>" /></a>
                            </div>
                        <?php endif; ?>
                        <div class="right">
                            <div class="the-header">
                                <h1><a onclick="javascript: job_view(<?= $post['id'] ?>)" href="javascript:"><?= $post['job_title'] ?></a></h1>
                                <h3><?= $post['company_name'] ?></h3> 
                            </div>
                            <div class="description">
                                <?= (strlen($post['job_description']) >= 400) ? substr($post['job_description'], 0, 400) . '...' : $post['job_description']; ?>
                            </div>
                            <div class="date">
                                <?= $globalworks->when($post['created_at']) ?> 
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                <?php endforeach; ?>
                <?= $globalworks->_paginate($count, $limit, $page) ?>
                <?= $globalworks->_m_paginate($count, $limit, $page) ?>
            </div>
        <?php endif; ?>
    </div>
    <div class="s_right">
        <div class="search-right">
            <h1>Advance Search</h1>
            <div class="fields">
                <form method="GET" action="/jobs/search" accept-charset="UTF-8">
                    <?= $this->Form->hidden('searchType', ['value' => 'advance']) ?>
                    <div class="field">
                        <?= $this->Form->label('keywords', 'Your Keywords:'); ?>
                        <?= $this->Form->text('keywords', ['id' => 'keywords', 'class' => 'keywords', 'autocomplete' => 'off', 'value' => ($this->request->query('keywords')) ? $this->request->query('keywords') : '']) ?>
                        <div class="search-right-error"></div>
                    </div>
                    
                    <div class="field">
                        <h3>Job Base</h3>
                        <ul>
                            <li>
                                <input type="radio" name="job-class" id="job-class-1" value="Homebase/Full Time" <?= ($this->request->query('job-class') == 'Homebase/Full Time' || $this->request->query('job_class') == 'Homebase/Full Time') ? 'checked' : ''; ?> />
                                <?= $this->Form->label('job-class-1', 'Homebase/Full Time') ?>
                                <div class="check"></div>
                            </li>
                            <li>
                                <input type="radio" name="job-class" id="job-class-2" value="Homebase/Part Time" <?= ($this->request->query('job-class') == 'Homebase/Part Time' || $this->request->query('job_class') == 'Homebase/Part Time') ? 'checked' : ''; ?> />
                                <?= $this->Form->label('job-class-2', 'Homebase/Part Time') ?>
                                <div class="check"></div>
                            </li>
                            <li>
                                <input type="radio" name="job-class" id="job-class-3" value="Office/Full Time" <?= ($this->request->query('job-class') == 'Office/Full Time' || $this->request->query('job_class') == 'Office/Full Time') ? 'checked' : ''; ?> />
                                <?= $this->Form->label('job-class-3', 'Office/Full Time') ?>
                                <div class="check"></div>
                            </li>
                            <li>
                                <input type="radio" name="job-class" id="job-class-4" value="Office/Part Time" <?= ($this->request->query('job-class') == 'Office/Part Time' || $this->request->query('job_class') == 'Office/Part Time') ? 'checked' : ''; ?> />
                                <?= $this->Form->label('job-class-4', 'Office/Part Time') ?>
                                <div class="check"></div>
                            </li>
                            <li>
                                <input type="radio" name="job-class" id="job-class-5" value="All" <?= ($this->request->query('job-class') == 'All' || $this->request->query('job_class') == 'All') ? 'checked' : ''; ?> />
                                <?= $this->Form->label('job-class-5', 'All') ?>
                                <div class="check"></div>
                            </li>
                        </ul>
                    </div>
                    <div class="field">
                        <h3>Localization</h3>
                        <ul>
                            <li>
                                <input type="radio" name="localization" id="localization-1" value="Local" <?= ($this->request->query('localization') == 'Local' || $this->request->query('location') == 'Local') ? 'checked' : ''; ?> />
                                <?= $this->Form->label('localization-1', 'Local') ?>
                                <div class="check"></div>
                            </li>
                            <li>
                                <input type="radio" name="localization" id="localization-2" value="Abroad" <?= ($this->request->query('localization') == 'Abroad' || $this->request->query('location') == 'Abroad') ? 'checked' : ''; ?> />
                                <?= $this->Form->label('localization-2', 'Abroad') ?>
                                <div class="check"></div>
                            </li>
                            <li>
                                <input type="radio" name="localization" id="localization-3" value="All" <?= ($this->request->query('localization') == 'All' || $this->request->query('location') == 'All') ? 'checked' : ''; ?> />
                                <?= $this->Form->label('localization-3', 'All') ?>
                                <div class="check"></div>
                            </li>
                        </ul>
                    </div>
                    <div class="field">
                        <h3>Job Categories</h3>
                        <ul>
                            <li>
                                <input type="radio" name="category" id="category-1" value="Accounting Finance" />
                                <?= $this->Form->label('category-1', 'Accounting Finance') ?>
                                <div class="check"></div>
                            </li>
                            <li>
                                <input type="radio" name="category" id="category-2" value="Arts/Media" />
                                <?= $this->Form->label('category-2', 'Arts/Media') ?>
                                <div class="check"></div>
                            </li>
                            <li>
                                <input type="radio" name="category" id="category-3" value="Computer/I.T" />
                                <?= $this->Form->label('category-3', 'Computer/I.T') ?>
                                <div class="check"></div>
                            </li>
                            <li>
                                <input type="radio" name="category" id="category-4" value="Engineering" />
                                <?= $this->Form->label('category-4', 'Engineering') ?>
                                <div class="check"></div>
                            </li>
                            <li>
                                <input type="radio" name="category" id="category-5" value="Admin/Office" />
                                <?= $this->Form->label('category-5', 'Admin/Office') ?>
                                <div class="check"></div>
                            </li>
                            <li>
                                <input type="radio" name="category" id="category-6" value="Building/Construction" />
                                <?= $this->Form->label('category-6', 'Building/Construction') ?>
                                <div class="check"></div>
                            </li>
                            <li>
                                <input type="radio" name="category" id="category-8" value="Health Care" />
                                <?= $this->Form->label('category-8', 'Health Care') ?>
                                <div class="check"></div>
                            </li>
                            <li>
                                <input type="radio" name="category" id="category-9" value="Hotel/Restaurant" />
                                <?= $this->Form->label('category-9', 'Hotel/Restaurant') ?>
                                <div class="check"></div>
                            </li>
                            <li>
                                <input type="radio" name="category" id="category-10" value="Manufacturing" />
                                <?= $this->Form->label('category-10', 'Manufacturing') ?>
                                <div class="check"></div>
                            </li>
                            <li>
                                <input type="radio" name="category" id="category-11" value="Sales Marketing" />
                                <?= $this->Form->label('category-11', 'Sales Manufacturing') ?>
                                <div class="check"></div>
                            </li>
                            <li>
                                <input type="radio" name="category" id="category-12" value="Services/Others" />
                                <?= $this->Form->label('category-12', 'Services/Others') ?>
                                <div class="check"></div>
                            </li>
                            <li>
                                <input type="radio" name="category" id="category-13" value="All" <?= ($this->request->query('category') == 'All' || !$this->request->query('category')) ? 'checked' : ''; ?> />
                                <?= $this->Form->label('category-13', 'All') ?>
                                <div class="check"></div>
                            </li>
                        </ul>
                    </div>
                    <div class="field">
                        <?= $this->Form->label('country', 'Country:'); ?>
                        <?= $this->Form->select('country', $countries, ['id' => 'country', 'default' => ($this->request->query('country')) ? $this->request->query('country') : 'PH']); ?>
                    </div>
                    <div class="btn-container">
                        <?= $this->Form->button('Search', ['id' => 'search', 'type' => 'button']); ?>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>

<script>
    var new_height = '';
    
    resizeSearchResults();
    jobview_size();
    
    $(window).resize(function() {
        resizeSearchResults();
        jobview_size();
    });
    
    $("#keywords").on("keypress", function(e) {
        var code = e.keyCode || e.which;
        if (code == 13) {
            e.preventDefault();
            return false;
        }
    }).on("keyup", function() {
        $("#keywords").css("border-color", "#ccc");
        $(".search-right-error").html("");
    });
    
    $("#search").on("click", function() {
        if ($("#keywords").val() == "") {
            $("#keywords").focus();
            $("#keywords").css("border-color", "red");
            $(".search-right-error").html("Please write your keywords.");
        } else if ($("#keywords").val().length <= 2) {
            $("#keywords").focus();
            $("#keywords").css("border-color", "red");
            $(".search-right-error").html("Keyword too short.");
        } else {
            $(".search-right .fields form").submit();
        }
    });
    
    function resizeSearchResults() {
        var height = $(window).height();
        new_height = height - 39;
        var search_results = $(".search-results").height();
        
        if (height > search_results) {
            $(".search-results").css({
                "height": new_height + "px"
            });
        }
    }
    
    function jobview_size() {
        var height = $(window).height();
        var width = $(window).width() + 15;
        new_height = height - 2;
        
        $(".job-view").css({
            "height": new_height + "px",
            "width": width + "px"
        });
    }
    
    function job_view(id) {
        $(".main-header").hide();
        
        $(".job-view").animate({
            top: "0"
        }, 500, function() {
            var data = {
                "operation": "jobpost_view",
                "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>",
                "id": id
            };
            
            var new_height2 = new_height / 2 + 300;
            
            $("body").css({
                "overflow": "hidden"
            });
            
            $(this).css({
                "line-height": new_height2 + "px",
                "text-align": "center"
            }).html("<img src='/img/loading.gif' />");
            
            $.post("/ajax", data, function(r) {
                $(".job-view").css({
                    "line-height": "normal",
                    "text-align": "left"
                }).html(r);
            });
        });
    }

    function _apply(id) {
        var data = {
            "operation": "candidate_application",
            "_csrfToken": "<?= $this->request->param('_csrfToken') ?>",
            "id": id
        };
        
        $(".black-screen2").show();
        $(".job-app-window").slideDown(500, function() {
            $.post("/ajax", data, function(r) {
                $(".job-app-window").html(r);
            });
        });
    }
</script>