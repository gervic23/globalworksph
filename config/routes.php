<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 */
Router::defaultRouteClass(DashedRoute::class);

Router::scope('/', function (RouteBuilder $routes) {
    /**
     * Here, we are connecting '/' (base path) to a controller called 'Pages',
     * its action called 'display', and we pass a param to select the view file
     * to use (in this case, src/Template/Pages/home.ctp)...
     */
    $routes->connect('/', ['controller' => 'Home', 'action' => 'index', 'home']);
    
    //Main Login route.
    $routes->connect('/login/*', ['controller' => 'Home', 'action' => 'login']);
    
    //User Registration route.
    $routes->connect('/register/*', ['controller' => 'Home', 'action' => 'register']);

    //User's Confirmation Code Page.
    $routes->connect('/account_confirmation/*', ['controller' => 'Home', 'action' => 'confirmAccount']);
    
    //User Logout Route.
    $routes->connect('/logout', ['controller' => 'Home', 'action' => 'logout']);

    //Facebook Candidate Callback.
    $routes->connect('/candidate_callback', ['controller' => 'Home', 'action' => 'candidateCallback']);

    //Facebook Candidate Callback.
    $routes->connect('/employer_callback', ['controller' => 'Home', 'action' => 'employerCallback']);

    //Facebook Login No Email.
    $routes->connect('/facebook-email-required', ['controller' => 'Home', 'action' => 'noEmail']);

    //Facebook User's Basic Information Page.
    $routes->connect('/basicinfo', ['controller' => 'Home', 'action' => 'basicinfo']);

    //Facebook User's Password Authentication Page.
    $routes->connect('/fb_password_auth/*', ['controller' => 'Home', 'action' => 'fbUserPasswordAuth']);

    //Forgot Password Page.
    $routes->connect('/forgot_password/*', ['controller' => 'Home', 'action' => 'forgotPassword']);

    //Password Recovery Page.
    $routes->connect('/change_password/*', ['controller' => 'Home', 'action' => 'passwordrecovery']);
    
    //User Set Profile route.
    $routes->connect('/setprofile/*', ['controller' => 'Home', 'action' => 'setprofile']);
    
    //User's Upload Profile Picture Route.
    $routes->connect('/uploadpicture/*', ['controller' => 'Home', 'action' => 'uploadpicture']);
    
    //User's Image Crop Route.
    $routes->connect('/imagecrop/*', ['controller' => 'Home', 'action' => 'imagecrop']);
    
    //Upload Resume Route.
    $routes->connect('/uploadresume/*', ['controller' => 'Home', 'action' => 'uploadresume']);

    //About Us.
    $routes->connect('/aboutus', ['controller' => 'Home', 'action' => 'aboutus']);

    //Privacy Policy.
    $routes->connect('/privacypolicy', ['controller' => 'Home', 'action' => 'privacypolicy']);

    //Terms of Use.
    $routes->connect('/tos', ['controller' => 'Home', 'action' => 'tos']);

    //Disclaimer.
    $routes->connect('/disclaimer', ['controller' => 'Home', 'action' => 'disclaimer']);

    //Subscribe.
    $routes->connect('/subscribe', ['controller' => 'Home', 'action' => 'subscribe']);

    //Unsubscribe.
    $routes->connect('/unsubscribe/*', ['controller' => 'Home', 'action' => 'unsubscribe']);

    //Administrators.
    $routes->connect('/administrators', ['controller' => 'Home', 'action' => 'administrators']);
    
    //Employers Dashboard.
    $routes->connect('/employers/dashboard/', ['controller' => 'Employersdashboard', 'action' => 'index']);
    
    //Employer's Job Post.
    $routes->connect('/employers/jobpost/', ['controller' => 'Employersdashboard', 'action' => 'jobpost']);
    
    //Employer's Job List.
    $routes->connect('/employers/joblist/', ['controller' => 'Employersdashboard', 'action' => 'joblist']);

    //Employer's Messages.
    $routes->connect('/employers/messages', ['controller' => 'Employersdashboard', 'action' => 'messages']);

    //Employer's Search Candidates.
    $routes->connect('/employers/search', ['controller' => 'Employersdashboard', 'action' => 'search']);
    
    //Employer's Job List Inactive or Job List Closed.
    $routes->connect('/employers/joblist/inactive', ['controller' => 'Employersdashboard', 'action' => 'joblist_inactive']);

    //Employer's Profile Page.
    $routes->connect('/employers/profile', ['controller' => 'Employersdashboard', 'action' => 'profile']);

    //Employer's Account Page.
    $routes->connect('/employers/account', ['controller' => 'Employersdashboard', 'action' => 'account']);

    //Employer's Change Password Page.
    $routes->connect('/employers/account/changepassword', ['controller' => 'Employersdashboard', 'action' => 'changepassword']);
    
    //Candidates Dashboard.
    $routes->connect('/candidates/dashboard', ['controller' => 'Candidates', 'action' => 'index']);
    
    //Candidates Search jobs.
    $routes->connect('/candidates/search', ['controller' => 'Candidates', 'action' => 'search']);

    //Candidates Messages.
    $routes->connect('/candidates/messages', ['controller' => 'Candidates', 'action' => 'messages']);
    
    //Candidates Profile Page.
    $routes->connect('/candidates/profile', ['controller' => 'Candidates', 'action' => 'profile']);
    
    //Candidates Account Page.
    $routes->connect('/candidates/account', ['controller' => 'Candidates', 'action' => 'account']);
    
    //Candidates Change Password Page.
    $routes->connect('/candidates/account/changepassword', ['controller' => 'Candidates', 'action' => 'changepassword']);
    
    //Resume Download Route.
    $routes->connect('/download/*', ['controller' => 'Candidates', 'action' => 'download']);
    
    //Job List Route.
    $routes->connect('/jobs/all/', ['controller' => 'Jobs', 'action' => 'index']);
    
    //Job List sorted by categories.
    $routes->connect('/jobs/category/*', ['controller' => 'Jobs', 'action' => 'category']);
    
    //Single Job Page.
    $routes->connect('/job/*', ['controller' => 'Jobs', 'action' => 'job']);
    
    //Job Search Page.
    $routes->connect('/jobs/search', ['controller' => 'Jobs', 'action' => 'search']);
    
    //Admin Dashboard Page.
    $routes->connect('/admin/dashboard', ['controller' => 'Admins', 'action' => 'dashboard']);
    
    //Admin Manage Jobposts.
    $routes->connect('/admin/jobposts/', ['controller' => 'Admins', 'action' => 'jobposts']);

    //Admin Searh Jobposts Search.
    $routes->connect('/admin/jobposts/search', ['controller' => 'Admins', 'action' => 'jobpostsSearch']);

    //Admin Manage Inactive Jobposts.
    $routes->connect('/admin/jobposts/inactive', ['controller' => 'Admins', 'action' => 'jobpostsInactive']);

    //Admin Manage Inactive Jobposts Search.
    $routes->connect('/admin/jobposts/inactive/search', ['controller' => 'Admins', 'action' => 'jobpostsInactiveSearch']);
    
    //Admin Manage Users.
    $routes->connect('/admin/users/*', ['controller' => 'Admins', 'action' => 'users']);
    
    //Admin Announcements.
    $routes->connect('/admin/announcements', ['controller' => 'Admins', 'action' => 'announcements']);

    //Admin Announcement create page.
    $routes->connect('/admin/createannouncement', ['controller' => 'Admins', 'action' => 'createAnnouncement']);

    //Admin Edit Announcement.
    $routes->connect('/admin/announcements/edit/*', ['controller' => 'Admins', 'action' => 'editAnnouncement']);
    
    //Admin Logs.
    $routes->connect('/admin/logs', ['controller' => 'Admins', 'action' => 'logs']);

    //Admin Visitor's Statistics.
    $routes->connect('/admin/visitors_statistics', ['controller' => 'Admins', 'action' => 'visitorsStatistics']);
    
    //Admin Mailing List.
    $routes->connect('/admin/mailinglist', ['controller' => 'Admins', 'action' => 'mailinglist']);

    //Admin Create Mailing List.
    $routes->connect('/admin/mailinglist/create', ['controller' => 'Admins', 'action' => 'mailinglistCreate']);

    //Admin Edit Mailinglist.
    $routes->connect('/admin/mailinglist/edit/*', ['controller' => 'Admins', 'action' => 'mailinglistEdit']);

    //Admin Subscribers List.
    $routes->connect('/admin/mailinglist/subscribers', ['controller' => 'Admins', 'action' => 'subscribers']);
    
    //Admin Settings Page.
    $routes->connect('/admin/settings', ['controller' => 'Admins', 'action' => 'settings']);

    //Admin My Account Page.
    $routes->connect('/admin/myaccount', ['controller' => 'Admins', 'action' => 'myaccount']);

    //Admin Edit Account Details Page.
    $routes->connect('/admin/myaccount/edit/', ['controller' => 'Admins', 'action' => 'editMyaccount']);

    //Admin Profiles Page.
    $routes->connect('/admin/myaccount/profiles', ['controller' => 'Admins', 'action' => 'profiles']);

    //Admin Edit Profiles Page.
    $routes->connect('/admin/myaccount/profiles/edit', ['controller' => 'Admins', 'action' => 'profilesEdit']);

    //Admin Change Password Page.
    $routes->connect('/admin/myaccount/changepassword', ['controller' => 'Admins', 'action' => 'changepassword']);
    
    //Ajax Route.
    $routes->connect('/ajax', ['controller' => 'Ajax', 'action' => 'init']);
    
    //Admin Ajax Route.
    $routes->connect('/adminajax', ['controller' => 'Adminajax', 'action' => 'init']);
    
    //Route to sitemap site pages.
    $routes->connect('/maps/pages', ['controller' => 'Sitemaps', 'action' => 'pages']);
    
    //Route to sitemap jobs
    $routes->connect('/maps/jobs', ['controller' => 'Sitemaps', 'action' => 'jobs']);
    
    //Route to rss jobs
    $routes->connect('/rss/jobs', ['controller' => 'Sitemaps', 'action' => 'rssjobs']);

    //Route for Cronjob.
    $routes->connect('/crons', ['controller' => 'Crons', 'action' => 'init']);

    /**
     * ...and connect the rest of 'Pages' controller's URLs.
     */
    $routes->connect('/pages/*', ['controller' => 'Pages', 'action' => 'display']);

    /**
     * Connect catchall routes for all controllers.
     *
     * Using the argument `DashedRoute`, the `fallbacks` method is a shortcut for
     *    `$routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'DashedRoute']);`
     *    `$routes->connect('/:controller/:action/*', [], ['routeClass' => 'DashedRoute']);`
     *
     * Any route class can be used with this method, such as:
     * - DashedRoute
     * - InflectedRoute
     * - Route
     * - Or your own route class
     *
     * You can remove these routes once you've connected the
     * routes you want in your application.
     */
    $routes->fallbacks(DashedRoute::class);
});

/**
 * Load all plugin routes. See the Plugin documentation on
 * how to customize the loading of plugin routes.
 */
Plugin::routes();
