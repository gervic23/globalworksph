<?php
use Migrations\AbstractMigration;

class CreateUserLogMsg extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('user_log_msg');
        $table->addColumn('user_id', 'biginteger', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('job_app', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('job_inv', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('microtime', 'float', [
            'default' => null,
            'null' => false,
        ]);

        $table->create();
    }
}
