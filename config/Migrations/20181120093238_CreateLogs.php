<?php
use Migrations\AbstractMigration;

class CreateLogs extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('logs');
        $table->addColumn('log_type', 'string', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('message', 'text', [
            'default' => null,
            'null' => true,
        ]);

        $table->create();
    }
}
