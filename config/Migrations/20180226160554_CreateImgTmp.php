<?php
use Migrations\AbstractMigration;

class CreateImgTmp extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('img_tmp');
        $table->addColumn('user_id', 'biginteger', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);
        $table->addColumn('filename', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false
        ]);
        $table->addColumn('date', 'datetime', [
            'default' => null,
            'null' => false
        ]);
        
        $table->create();
    }
}
