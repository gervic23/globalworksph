<?php
use Migrations\AbstractMigration;

class CreateCompanylogoTmp extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('companylogo_tmp');
        $table->addColumn('user_id', 'biginteger', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);
        $table->addColumn('filename', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);
        $table->addColumn('uploaded_at', 'float', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);
        
        $table->create();
    }
}
