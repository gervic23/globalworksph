<?php
use Migrations\AbstractMigration;

class CreateProfiles extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('profiles');
        $table->addColumn('user_id', 'biginteger', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);
        $table->addColumn('nickname', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('job_title', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('skills', 'text', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('desire_salary', 'text', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('education', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('experience', 'text', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('employment_status', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('business_name', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('business_description', 'text', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('business_email', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('business_contact_number', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('business_website', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('business_address', 'text', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('business_state', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('business_country', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('skype_id', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('job_subscribe', 'biginteger', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        
        $table->create();
    }
}
