<?php
use Migrations\AbstractMigration;

class AddHitsToIpLogs extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('ip_logs');
        $table->addColumn('hits', 'integer', [
            'default' => null,
            'null' => false,
        ]);

        $table->update();
    }
}
