<?php
use Migrations\AbstractMigration;

class CreateAnnouncements extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('announcements');
        $table->addColumn('announcement', 'text', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('published', 'integer', [
            'default' => 0,
            'null' => false,
        ]);
        $table->addColumn('created_at', 'datetime', [
            'default' => null,
            'null' => false,
        ]);

        $table->create();
    }
}
