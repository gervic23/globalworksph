-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 31, 2019 at 07:20 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.2.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `globalworks`
--

-- --------------------------------------------------------

--
-- Table structure for table `announcements`
--

CREATE TABLE `announcements` (
  `id` int(11) NOT NULL,
  `announcement` text NOT NULL,
  `published` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `announcements`
--

INSERT INTO `announcements` (`id`, `announcement`, `published`, `created_at`, `user_id`) VALUES
(1, '<p><span style=\"font-size:72px\">Hello! This is a sample announcement.</span></p>', 1, '2019-01-10 22:42:23', 36);

-- --------------------------------------------------------

--
-- Table structure for table `companylogo_tmp`
--

CREATE TABLE `companylogo_tmp` (
  `id` int(11) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `uploaded_at` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `img_tmp`
--

CREATE TABLE `img_tmp` (
  `id` int(11) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ip_logs`
--

CREATE TABLE `ip_logs` (
  `id` int(11) NOT NULL,
  `assoc` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `country_code` varchar(255) DEFAULT NULL,
  `isp` varchar(255) DEFAULT NULL,
  `lat` varchar(255) DEFAULT NULL,
  `lon` varchar(255) DEFAULT NULL,
  `org` varchar(255) DEFAULT NULL,
  `query` varchar(255) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  `region_name` varchar(255) DEFAULT NULL,
  `timezone` varchar(255) DEFAULT NULL,
  `zip` varchar(255) DEFAULT NULL,
  `hits` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ip_logs`
--

INSERT INTO `ip_logs` (`id`, `assoc`, `city`, `country`, `country_code`, `isp`, `lat`, `lon`, `org`, `query`, `region`, `region_name`, `timezone`, `zip`, `hits`) VALUES
(1, 'AS9299 Philippine Long Distance Telephone Company', 'San Pedro', 'Philippines', 'PH', 'Philippine Long Distance Telephone', '14.3595', '121.0473', 'Philippine Long Distance Telephone', '127.0.0.2', '40', 'Calabarzon', 'Asia/Manila', '4023', 2),
(2, 'AS9299 Philippine Long Distance Telephone Company', 'Las Pinas', 'Philippines', 'PH', 'Philippine Long Distance Telephone', '14.4506', '120.9828', 'Philippine Long Distance Telephone', '127.0.0.1', '00', 'Metro Manila', 'Asia/Manila', '1612', 3910);

-- --------------------------------------------------------

--
-- Table structure for table `jobapplications`
--

CREATE TABLE `jobapplications` (
  `id` int(11) NOT NULL,
  `jobpost_id` bigint(20) NOT NULL,
  `applicant_id` bigint(20) NOT NULL,
  `employer_id` bigint(20) NOT NULL,
  `message` text NOT NULL,
  `archived` bigint(20) NOT NULL,
  `date_apply` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jobheaderimg_tmp`
--

CREATE TABLE `jobheaderimg_tmp` (
  `id` int(11) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `uploaded_at` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jobposts`
--

CREATE TABLE `jobposts` (
  `id` int(11) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `header_image` varchar(255) DEFAULT NULL,
  `company_logo` varchar(255) DEFAULT NULL,
  `job_title` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `job_category` varchar(255) NOT NULL,
  `job_type` varchar(255) NOT NULL,
  `job_migration` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `job_description` text NOT NULL,
  `job_responsibilities` text DEFAULT NULL,
  `job_qualifications` text DEFAULT NULL,
  `job_years_experience` varchar(255) DEFAULT NULL,
  `job_location` varchar(255) DEFAULT NULL,
  `job_website` varchar(255) DEFAULT NULL,
  `job_email` varchar(255) DEFAULT NULL,
  `job_contact_number` varchar(255) NOT NULL,
  `closing_date` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  `pos` bigint(20) NOT NULL,
  `googglemap` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jobposts`
--

INSERT INTO `jobposts` (`id`, `user_id`, `header_image`, `company_logo`, `job_title`, `company_name`, `job_category`, `job_type`, `job_migration`, `country`, `job_description`, `job_responsibilities`, `job_qualifications`, `job_years_experience`, `job_location`, `job_website`, `job_email`, `job_contact_number`, `closing_date`, `created_at`, `modified_at`, `pos`, `googglemap`) VALUES
(1, 1, '120180307005722VUWmfyUZvHPuh4t3.jpg', '1201803070057291oz6soAvjOEHDY1A.jpg', 'We Are Hiring', 'SKYES', 'Services/Others', 'Office/Full Time', 'Local', 'PH', '<p><code><em><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</strong></em></code> Nulla ultricies cursus arcu, quis feugiat magna mollis quis. Donec ac nibh id metus facilisis dapibus quis quis sem. Donec a ex at orci dapibus ullamcorper. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nec egestas sapien. Proin faucibus, turpis pulvinar fringilla condimentum, magna dolor suscipit nibh, a venenatis orci orci vel mauris. Ut a diam erat. Etiam lacinia justo quis arcu facilisis tincidunt. Suspendisse sit amet arcu nec metus iaculis viverra. Donec molestie, enim eget pellentesque egestas, enim nulla congue ligula, vitae lobortis sapien dui placerat enim. Nulla vehicula urna non aliquet scelerisque. Duis sed nisl at ex interdum pulvinar sit amet placerat lorem. Morbi ac ipsum eget libero porttitor lacinia. Aenean mollis metus diam, vitae luctus arcu dignissim eu. Suspendisse quis felis dictum, pharetra libero non, pulvinar ante. Aenean pharetra mauris sit amet risus accumsan vehicula. Suspendisse commodo libero urna, vitae dignissim mi blandit eu.</p><p>Donec sodales in enim vel accumsan. Sed in sodales dolor. Aliquam mi nulla, pretium ac eros at, ornare eleifend lectus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aenean non ultrices erat. Aliquam condimentum ut mauris a malesuada. Ut ipsum tortor, dignissim in congue in, imperdiet eu turpis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed eget lacus ut odio consequat laoreet. Pellentesque sed nulla eget purus varius pellentesque non ut metus. Mauris nec ultricies nulla. Praesent nisl nisl, ullamcorper at volutpat ut, rhoncus ut leo. Vestibulum quis commodo eros, vitae volutpat lacus. Cras vel iaculis magna. Nulla aliquam maximus porta. Ut ex ex, malesuada eleifend venenatis eget, pellentesque vitae turpis. Fusce quis imperdiet nulla, ut laoreet massa. Integer pharetra odio eu ante rhoncus molestie.</p>', '[\"Responsibility 1\",\"Responsibility 2\",\"Responsibility 3\",\"Responsibility 4\",\"Responsibility 5\"]', '[\"Qualification 1\",\"Qualification 2\",\"Qualification e\",\"Qualification 4\"]', '1 Year to 3 Years', '#31 18th St. Pacita Complex San Pedro Laguna.', 'http://www.google.com', 'user@google.com', '09267294114', '2019-01-31 00:00:00', '2018-03-07 01:01:19', '2019-01-06 01:46:59', 1, 1),
(4, 1, '120180309020959LyJCYoSQ1Jzf9fRm.jpg', '120180309021006P9rfx1BBOQL1g5gP.jpg', 'Company Nurse', 'Amherst Laboratories Inc.', 'Health Care', 'Office/Full Time', 'Local', 'PH', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce lacinia ornare commodo. Nullam rhoncus faucibus sapien at porta. Quisque a lobortis elit, euismod auctor odio. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer tempus blandit quam, at cursus nibh porta at. Morbi malesuada a quam et interdum. Quisque sit amet elit nibh.\n\nSuspendisse pulvinar auctor purus at commodo. Praesent a pharetra metus. Fusce at est lectus. Pellentesque mollis, mauris nec consectetur ultrices, ante augue efficitur leo, non accumsan ipsum urna non lacus. In non tortor dapibus, scelerisque felis eu, venenatis sem. Nunc mi orci, molestie eu odio sed, egestas aliquet est. Nullam in varius ante. Curabitur in mi nec risus scelerisque viverra. Nunc quis neque quis eros ultrices rutrum. Nam eu sem in metus fringilla tempor vel vitae magna. Donec porttitor ante ac quam gravida varius.\n\nCras sed mattis orci. Fusce vitae nulla non lorem aliquet pellentesque sit amet nec nisi. Maecenas non dignissim elit. Sed vehicula consequat mattis. Pellentesque ut euismod nisi, vitae feugiat dolor. Nullam dictum, nisl eu venenatis sodales, urna mi gravida dui, at maximus lorem ligula nec est. Nullam fringilla venenatis odio id elementum. Curabitur id lacinia erat. Duis et leo condimentum, feugiat quam quis, porta nulla. Vestibulum nec turpis non urna rutrum porttitor id ut lectus.\n\nIn neque sapien, ultrices sed massa ac, dignissim ullamcorper est. Quisque sit amet ante sed velit faucibus accumsan sollicitudin eu odio. Nullam vulputate lacus libero, rutrum mattis erat sollicitudin at. Proin pellentesque rhoncus volutpat. Fusce in pellentesque augue, ultricies tempus turpis. Suspendisse sed faucibus lorem, id egestas ipsum. Praesent sagittis iaculis odio a malesuada.\n\nInteger mi ante, dapibus vitae sodales ac, aliquam ac dui. Fusce pharetra lorem sed arcu interdum sollicitudin. Pellentesque magna dui, commodo consectetur tincidunt interdum, lobortis quis diam. Ut sagittis lacus at tincidunt feugiat. Nulla dictum consectetur pulvinar. Aenean erat augue, sodales et aliquam non, auctor non velit. Donec id nunc varius, vulputate metus et, faucibus sapien. Duis lorem mi, interdum eget fermentum nec, interdum vitae dui. Morbi dapibus urna non lectus scelerisque bibendum. Nunc sed malesuada ipsum. Maecenas et lacus justo. Suspendisse nec ligula diam. Nunc tortor justo, lobortis in placerat ac, sodales sed metus.', '[\"Responsibilitiy 1\",\"Responsibilitiy 2\",\"Responsibilitiy 3\",\"Responsibilitiy 4\",\"Responsibilitiy 5\"]', '[\"Qualification 1\",\"Qualification 2\",\"Qualification 3\",\"Qualification 4\",\"Qualification 5\"]', '3 Years', 'Unilab Pharma Campus Binan City, Laguna', '', '', '049 5124072', '2019-01-31 00:00:00', '2018-03-09 02:12:47', '2019-01-05 23:14:52', 2, 0),
(5, 1, '120180309224656tEsEXlnSl1u01R29.jpg', '120180309224736TurV3SV2BTbTYsx0.jpg', 'ATTN: Accounting Staff | Bacolod', 'Focus Inc.', 'Admin/Office', 'Office/Full Time', 'Local', 'PH', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut nisi diam, egestas sed molestie ac, tincidunt sed metus. Vestibulum cursus venenatis dolor, a tristique elit consectetur placerat. Duis quis commodo sem. Morbi eleifend mauris velit, nec porta nulla venenatis eu. Mauris tempor malesuada leo, non volutpat diam malesuada a. Quisque quis est ac enim ultricies ullamcorper. Curabitur consequat, tortor ac condimentum elementum, augue neque malesuada ipsum, nec suscipit mi purus tempus est. Nulla id lacus a velit accumsan commodo. Quisque cursus, nisi cursus laoreet consectetur, tellus dui vulputate augue, sit amet maximus erat felis nec leo.\n\nNam nibh arcu, iaculis facilisis risus sed, ullamcorper pellentesque quam. Morbi blandit metus mollis lorem lobortis egestas. Morbi tincidunt metus mi, vel maximus urna imperdiet et. Quisque rutrum non velit et blandit. Ut ante lacus, suscipit malesuada enim accumsan, porta dapibus nulla. Morbi iaculis volutpat ante bibendum fringilla. Nunc neque ante, sodales eu suscipit id, auctor vitae odio. Maecenas convallis eget justo id finibus. Suspendisse vel nulla sed velit hendrerit volutpat. Vivamus eros velit, facilisis non iaculis vel, pulvinar volutpat mi. Nullam quis eleifend nulla, vel sagittis nunc.\n\nPellentesque luctus nulla condimentum augue sodales, eu euismod erat ornare. Proin non vestibulum nulla. Nulla non tincidunt neque. In ut molestie tortor. Morbi justo arcu, rutrum sit amet enim at, ullamcorper finibus enim. Nunc nec tortor bibendum, molestie dui vitae, eleifend velit. Suspendisse sed ipsum sagittis, eleifend turpis sit amet, molestie lectus. Nam sit amet est gravida, egestas nunc ullamcorper, commodo risus. Vestibulum fringilla dapibus velit nec vulputate. Donec congue, arcu id varius viverra, elit mi mattis felis, vitae ullamcorper augue libero nec mi. Maecenas ut dictum dui. Mauris congue interdum turpis, in lobortis massa scelerisque non.\n\nDonec tempor ultricies turpis, at rutrum felis dictum sit amet. Cras ac nibh elementum, ultrices dui at, tempus lacus. Proin ligula orci, eleifend id magna id, ultricies scelerisque mi. Aliquam condimentum massa quis augue dignissim, scelerisque faucibus nisi laoreet. Mauris rhoncus ornare tellus. Aliquam eget laoreet tellus. Cras egestas, ipsum in eleifend ullamcorper, odio libero tempus turpis, ut fringilla nibh elit sit amet dui. Cras nec risus tincidunt, tempus leo at, maximus neque. Morbi eu tortor eget sem consectetur tristique ac sit amet orci. Aliquam libero velit, consequat ut dictum vitae, iaculis eu felis.\n\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Mauris sed metus ligula. Integer ultricies, turpis eget semper venenatis, mi odio varius justo, vitae fringilla lacus massa et metus. Nulla est mi, iaculis vitae viverra commodo, accumsan et odio. Sed id convallis mauris. Praesent eget neque enim. Nullam volutpat dapibus iaculis. Proin ut bibendum lectus. Vivamus posuere rutrum venenatis. Aliquam non mauris non velit tincidunt facilisis ut vitae metus. Phasellus consectetur arcu quis laoreet tincidunt. Phasellus non blandit ipsum. Cras molestie tortor et diam congue, ac malesuada eros accumsan. Suspendisse potenti.', '[\"Responsibilities 1\",\"Responsibilities 2\",\"Responsibilities 3\",\"Responsibilities 4\",\"Responsibilities 5\"]', '[\"Qualifications 1\",\"Qualifications 2\",\"Qualifications 3\",\"Qualifications 4\",\"Qualifications 5\"]', '2 Years', 'Western Visayas, Bacolod City.', 'https://www.facebook.com/focusincgroupcorp', '', '637674254', '2019-01-31 00:00:00', '2018-03-09 22:55:48', '2018-12-31 11:07:04', 3, 0),
(6, 1, '120180309230039Bxeg9JhwrQuMHN8O.jpg', '120180309230029DYeNjrTomDkP7sB1.jpg', 'PURE NON VOICE! CHAT AGENTS! NO EXPERIENCE NEEDED! MALL OF ASIA SITE! Job', 'Conduent', 'Services/Others', 'Office/Full Time', 'Local', 'PH', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut nisi diam, egestas sed molestie ac, tincidunt sed metus. Vestibulum cursus venenatis dolor, a tristique elit consectetur placerat. Duis quis commodo sem. Morbi eleifend mauris velit, nec porta nulla venenatis eu. Mauris tempor malesuada leo, non volutpat diam malesuada a. Quisque quis est ac enim ultricies ullamcorper. Curabitur consequat, tortor ac condimentum elementum, augue neque malesuada ipsum, nec suscipit mi purus tempus est. Nulla id lacus a velit accumsan commodo. Quisque cursus, nisi cursus laoreet consectetur, tellus dui vulputate augue, sit amet maximus erat felis nec leo.\n\nNam nibh arcu, iaculis facilisis risus sed, ullamcorper pellentesque quam. Morbi blandit metus mollis lorem lobortis egestas. Morbi tincidunt metus mi, vel maximus urna imperdiet et. Quisque rutrum non velit et blandit. Ut ante lacus, suscipit malesuada enim accumsan, porta dapibus nulla. Morbi iaculis volutpat ante bibendum fringilla. Nunc neque ante, sodales eu suscipit id, auctor vitae odio. Maecenas convallis eget justo id finibus. Suspendisse vel nulla sed velit hendrerit volutpat. Vivamus eros velit, facilisis non iaculis vel, pulvinar volutpat mi. Nullam quis eleifend nulla, vel sagittis nunc.\n\nPellentesque luctus nulla condimentum augue sodales, eu euismod erat ornare. Proin non vestibulum nulla. Nulla non tincidunt neque. In ut molestie tortor. Morbi justo arcu, rutrum sit amet enim at, ullamcorper finibus enim. Nunc nec tortor bibendum, molestie dui vitae, eleifend velit. Suspendisse sed ipsum sagittis, eleifend turpis sit amet, molestie lectus. Nam sit amet est gravida, egestas nunc ullamcorper, commodo risus. Vestibulum fringilla dapibus velit nec vulputate. Donec congue, arcu id varius viverra, elit mi mattis felis, vitae ullamcorper augue libero nec mi. Maecenas ut dictum dui. Mauris congue interdum turpis, in lobortis massa scelerisque non.\n\nDonec tempor ultricies turpis, at rutrum felis dictum sit amet. Cras ac nibh elementum, ultrices dui at, tempus lacus. Proin ligula orci, eleifend id magna id, ultricies scelerisque mi. Aliquam condimentum massa quis augue dignissim, scelerisque faucibus nisi laoreet. Mauris rhoncus ornare tellus. Aliquam eget laoreet tellus. Cras egestas, ipsum in eleifend ullamcorper, odio libero tempus turpis, ut fringilla nibh elit sit amet dui. Cras nec risus tincidunt, tempus leo at, maximus neque. Morbi eu tortor eget sem consectetur tristique ac sit amet orci. Aliquam libero velit, consequat ut dictum vitae, iaculis eu felis.\n\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Mauris sed metus ligula. Integer ultricies, turpis eget semper venenatis, mi odio varius justo, vitae fringilla lacus massa et metus. Nulla est mi, iaculis vitae viverra commodo, accumsan et odio. Sed id convallis mauris. Praesent eget neque enim. Nullam volutpat dapibus iaculis. Proin ut bibendum lectus. Vivamus posuere rutrum venenatis. Aliquam non mauris non velit tincidunt facilisis ut vitae metus. Phasellus consectetur arcu quis laoreet tincidunt. Phasellus non blandit ipsum. Cras molestie tortor et diam congue, ac malesuada eros accumsan. Suspendisse potenti.', '[\"Responsibility 1\",\"Responsibility 2\",\"Responsibility 3\",\"Responsibility 4\",\"Responsibility 5\"]', '[\"Qualification 1\",\"Qualification2\",\"Qualification 3\",\"Qualification 4\",\"Qualification 5\"]', '1 Year to 2 Years', 'Mall of Asia Pasay City.', '', '', '', '2019-12-31 00:00:00', '2018-03-09 23:03:38', '2019-09-01 01:17:15', 4, 0),
(7, 1, '120180310002656vLjsOMvVZk24mENc.jpg', '1201803100027103NmR8bD5Jm2nJ0tP.jpg', 'Tresury Assistant', 'Lica Management Inc.', 'Admin/Office', 'Office/Full Time', 'Local', 'PH', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sit amet elit rhoncus, tincidunt ipsum vel, fermentum lectus. Suspendisse a enim rhoncus, venenatis est id, dictum mauris. Morbi iaculis in felis sed posuere. Aenean ut euismod lorem. Praesent bibendum magna eget mi congue viverra. Morbi laoreet aliquam nisi, vulputate interdum orci ullamcorper vehicula. Interdum et malesuada fames ac ante ipsum primis in faucibus. Cras sed rutrum ligula, et interdum massa. Aliquam aliquet auctor elit quis tempor. In accumsan bibendum ex, id interdum mauris mollis volutpat. Ut ullamcorper posuere gravida. Aenean hendrerit est sapien, id aliquam ante finibus vel. Nullam gravida tellus ac eleifend molestie. Vivamus rutrum enim vel sagittis facilisis. Phasellus pulvinar sodales lorem. Pellentesque maximus quis est egestas placerat.\n\nNam ex lorem, maximus non ligula eu, posuere sodales ipsum. Nullam auctor varius odio quis dapibus. Sed iaculis ante vel orci fringilla commodo. Fusce vel faucibus tortor. Aenean ut urna sodales, facilisis nisl eu, scelerisque justo. Maecenas in lorem quis lacus pretium varius. Sed at placerat leo. Pellentesque ut egestas felis. Aliquam vitae libero odio.\n\nEtiam facilisis a eros eu hendrerit. Duis risus lorem, tristique ut lorem vitae, ullamcorper aliquam urna. Vivamus et eros eu nibh tincidunt malesuada. Donec fringilla vestibulum felis consequat porta. Duis vulputate, sapien non tempor pulvinar, orci mauris blandit magna, sit amet feugiat lectus metus sed elit. Nam fringilla vitae magna malesuada efficitur. Fusce consectetur arcu ut nunc hendrerit consequat. Ut libero ligula, venenatis et arcu eu, egestas iaculis elit. Aenean convallis, quam ac iaculis pellentesque, massa ligula pellentesque ligula, mollis tincidunt massa odio accumsan lacus. Mauris cursus ex vel tellus placerat, id condimentum quam ultrices. Nam scelerisque ornare augue at mollis.\n\nSed laoreet sagittis vestibulum. Aenean laoreet elementum lectus, vel laoreet erat ultrices et. Ut ac tellus nec mi efficitur maximus. Etiam ut lobortis quam, id molestie enim. In vitae risus id orci sollicitudin hendrerit. Nullam est ipsum, pretium ac sapien nec, pellentesque viverra nibh. Cras non felis at turpis consequat tempus et quis purus. Ut dictum volutpat lobortis. In mollis magna nec turpis vestibulum, et feugiat lectus dignissim. In gravida, felis et luctus dictum, arcu orci venenatis quam, ac rutrum elit eros ut risus.\n\nCras cursus consectetur enim, sed porttitor diam tristique quis. Maecenas tempor sed nibh nec pharetra. Mauris varius porttitor erat, et interdum est malesuada ut. Praesent feugiat sem non eros lacinia, non dictum sapien mattis. Nulla eu turpis sapien. Phasellus luctus euismod erat, a malesuada magna consequat sit amet. Cras ultrices maximus neque quis interdum. Vestibulum rutrum velit vitae nibh gravida faucibus vitae ut magna. Curabitur sed pulvinar tortor. Phasellus quis efficitur massa. Praesent vitae sem eget ex scelerisque rhoncus ac nec tellus.', '[\"Responsibility 1\",\"Responsibility 2\",\"Responsibility 3\",\"Responsibility 4\",\"Responsibility 5\"]', '[\"Qualification 1\",\"Qualification 2\",\"Qualification 3\",\"Qualification 4\",\"Qualification 5\"]', '2 Years to 4 Years', '500 Shaw Boulevard, Mandaluyong City.', 'http://www.licagroup.com/', '', '4776620', '2019-12-31 00:00:00', '2018-03-10 00:31:27', '2019-09-01 01:16:52', 5, 0),
(8, 1, '120180315155941l0QfiUb6jt9ekyls.jpg', '120180315155932ztagm8ZLEinxyz9v.jpg', 'Programmer', 'St Francis Group of Companies', 'Computer/I.T', 'Office/Full Time', 'Local', 'PH', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vitae mollis lorem. Etiam a elementum quam, porta consectetur nibh. Quisque porta egestas neque id semper. Curabitur semper velit quis turpis dignissim, semper gravida velit tempus. Fusce lobortis urna sed enim consequat cursus. Cras facilisis ligula tellus, euismod blandit nunc pretium non. Mauris quis erat semper, posuere odio non, viverra massa. Nulla facilisi. Curabitur id laoreet nunc.\n\nPhasellus mollis tempus lacus id venenatis. Suspendisse sollicitudin ex laoreet purus scelerisque viverra. Sed viverra risus felis, at volutpat mauris eleifend et. Aliquam eu dapibus orci, nec blandit lorem. Nunc ut mattis massa. Maecenas efficitur ipsum eget dolor luctus, eu molestie turpis pharetra. Etiam ornare, dui sed egestas vestibulum, est justo hendrerit nibh, gravida venenatis justo massa vitae ex. Nulla placerat lorem eu metus commodo, at maximus sem congue. Sed eu ligula nec ligula facilisis lobortis.\n\nNullam non leo ac erat aliquet condimentum eget non erat. Phasellus placerat mi diam, eget posuere lacus vulputate sit amet. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In felis ligula, dapibus at cursus in, bibendum sed ipsum. Praesent commodo, risus vel eleifend viverra, odio lectus feugiat lacus, nec tempus elit augue eu risus. Morbi enim augue, pretium sed orci vehicula, malesuada blandit dolor. Maecenas id nisl mollis, venenatis ipsum id, elementum purus. Etiam malesuada justo nunc, in efficitur massa efficitur sit amet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin efficitur scelerisque tellus at viverra. Duis posuere, arcu in ullamcorper dignissim, massa dolor imperdiet mi, et suscipit metus nulla ut augue. Maecenas consequat dolor at fermentum fringilla. Maecenas malesuada erat magna, in dapibus risus fermentum in. Vestibulum accumsan leo ut orci consequat, non rutrum magna finibus. Pellentesque luctus dictum mollis.\n\nPhasellus efficitur nisl et dapibus pellentesque. Aliquam gravida eros ex. Vivamus id eleifend dui. Nam lacinia, lacus sed pulvinar viverra, orci nibh finibus ligula, sit amet tristique sapien nibh sodales enim. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin consectetur libero nunc, ac rutrum ligula porttitor in. Duis venenatis, erat varius blandit convallis, ante risus luctus quam, iaculis vehicula libero tellus congue turpis. Vestibulum at velit pellentesque, placerat mauris et, pulvinar dolor. Cras porta nunc orci, eu blandit lacus dapibus eu. Nulla ornare felis ut dolor vestibulum cursus. Fusce et dignissim tellus. In in nulla mollis, dictum nisi tincidunt, suscipit eros. Phasellus posuere nulla vel ornare sagittis. Integer laoreet justo ut tempor facilisis. Suspendisse commodo consectetur nisl, pharetra feugiat lorem interdum ut.\n\nQuisque fringilla pretium nisl, nec sodales ante aliquam eget. Phasellus ac tempus purus. Donec et lectus sit amet lacus cursus ornare id porttitor ex. Sed porttitor, nisi non finibus egestas, velit dui laoreet felis, id sagittis nibh lorem vitae diam. Vestibulum mattis feugiat aliquam. Sed gravida lorem ut erat scelerisque, eget lacinia lectus lacinia. Vestibulum interdum velit sed vulputate dignissim. Vestibulum tincidunt hendrerit nisl aliquam dictum.', '[\"Responsibility 1\",\"Responsibility 2\",\"Responsibility 3\",\"Responsibility 4\",\"Responsibility 5\"]', '[\"Qualification 1\",\"Qualification 2\",\"Qualification 3\",\"Qualification 4\",\"Qualification 5\"]', '3 Years', '4th Floor, St. Francis Square Bldg, Doña Julia Vargas Ave, Philippines', '', '', '09231248576', '2019-12-31 00:00:00', '2018-03-10 01:19:13', '2019-09-01 01:16:11', 6, 1),
(9, 1, '', '12018031001535739aMwKcLLaI88Fib.jpg', 'Engineering Supervisor', 'AMICI FOODSERVICE VENTURES, INC.', 'Engineering', 'Office/Full Time', 'Local', 'PH', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vitae mollis lorem. Etiam a elementum quam, porta consectetur nibh. Quisque porta egestas neque id semper. Curabitur semper velit quis turpis dignissim, semper gravida velit tempus. Fusce lobortis urna sed enim consequat cursus. Cras facilisis ligula tellus, euismod blandit nunc pretium non. Mauris quis erat semper, posuere odio non, viverra massa. Nulla facilisi. Curabitur id laoreet nunc.\n\nPhasellus mollis tempus lacus id venenatis. Suspendisse sollicitudin ex laoreet purus scelerisque viverra. Sed viverra risus felis, at volutpat mauris eleifend et. Aliquam eu dapibus orci, nec blandit lorem. Nunc ut mattis massa. Maecenas efficitur ipsum eget dolor luctus, eu molestie turpis pharetra. Etiam ornare, dui sed egestas vestibulum, est justo hendrerit nibh, gravida venenatis justo massa vitae ex. Nulla placerat lorem eu metus commodo, at maximus sem congue. Sed eu ligula nec ligula facilisis lobortis.\n\nNullam non leo ac erat aliquet condimentum eget non erat. Phasellus placerat mi diam, eget posuere lacus vulputate sit amet. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In felis ligula, dapibus at cursus in, bibendum sed ipsum. Praesent commodo, risus vel eleifend viverra, odio lectus feugiat lacus, nec tempus elit augue eu risus. Morbi enim augue, pretium sed orci vehicula, malesuada blandit dolor. Maecenas id nisl mollis, venenatis ipsum id, elementum purus. Etiam malesuada justo nunc, in efficitur massa efficitur sit amet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin efficitur scelerisque tellus at viverra. Duis posuere, arcu in ullamcorper dignissim, massa dolor imperdiet mi, et suscipit metus nulla ut augue. Maecenas consequat dolor at fermentum fringilla. Maecenas malesuada erat magna, in dapibus risus fermentum in. Vestibulum accumsan leo ut orci consequat, non rutrum magna finibus. Pellentesque luctus dictum mollis.\n\nPhasellus efficitur nisl et dapibus pellentesque. Aliquam gravida eros ex. Vivamus id eleifend dui. Nam lacinia, lacus sed pulvinar viverra, orci nibh finibus ligula, sit amet tristique sapien nibh sodales enim. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin consectetur libero nunc, ac rutrum ligula porttitor in. Duis venenatis, erat varius blandit convallis, ante risus luctus quam, iaculis vehicula libero tellus congue turpis. Vestibulum at velit pellentesque, placerat mauris et, pulvinar dolor. Cras porta nunc orci, eu blandit lacus dapibus eu. Nulla ornare felis ut dolor vestibulum cursus. Fusce et dignissim tellus. In in nulla mollis, dictum nisi tincidunt, suscipit eros. Phasellus posuere nulla vel ornare sagittis. Integer laoreet justo ut tempor facilisis. Suspendisse commodo consectetur nisl, pharetra feugiat lorem interdum ut.\n\nQuisque fringilla pretium nisl, nec sodales ante aliquam eget. Phasellus ac tempus purus. Donec et lectus sit amet lacus cursus ornare id porttitor ex. Sed porttitor, nisi non finibus egestas, velit dui laoreet felis, id sagittis nibh lorem vitae diam. Vestibulum mattis feugiat aliquam. Sed gravida lorem ut erat scelerisque, eget lacinia lectus lacinia. Vestibulum interdum velit sed vulputate dignissim. Vestibulum tincidunt hendrerit nisl aliquam dictum.', '[\"Responsibility 1\",\"Responsibility 2\",\"Responsibility 3\",\"Responsibility 4\",\"Responsibility 5\"]', '[\"Qualification 1\",\"Qualification 2\",\"Qualification 3\",\"Qualification 4\",\"Qualification 5\"]', '6 Months to 1 Year', 'Greenhills, San Juan, Metro Manila', 'https://www.facebook.com/AmiciPH', '', '123373665', '2019-12-31 00:00:00', '2018-03-10 01:58:48', '2019-09-01 01:15:43', 7, 0),
(10, 1, '120180310020944oTiEiTrMTq7E1jnB.jpg', '1201803100209520tJDYmwFeIJWapuv.jpg', 'TREASURY ASSISTANT - QUEZON CITY', 'SoutheastAsia Retail Inc. - Cebu', 'Admin/Office', 'Office/Full Time', 'Local', 'PH', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vitae mollis lorem. Etiam a elementum quam, porta consectetur nibh. Quisque porta egestas neque id semper. Curabitur semper velit quis turpis dignissim, semper gravida velit tempus. Fusce lobortis urna sed enim consequat cursus. Cras facilisis ligula tellus, euismod blandit nunc pretium non. Mauris quis erat semper, posuere odio non, viverra massa. Nulla facilisi. Curabitur id laoreet nunc.\n\nPhasellus mollis tempus lacus id venenatis. Suspendisse sollicitudin ex laoreet purus scelerisque viverra. Sed viverra risus felis, at volutpat mauris eleifend et. Aliquam eu dapibus orci, nec blandit lorem. Nunc ut mattis massa. Maecenas efficitur ipsum eget dolor luctus, eu molestie turpis pharetra. Etiam ornare, dui sed egestas vestibulum, est justo hendrerit nibh, gravida venenatis justo massa vitae ex. Nulla placerat lorem eu metus commodo, at maximus sem congue. Sed eu ligula nec ligula facilisis lobortis.\n\nNullam non leo ac erat aliquet condimentum eget non erat. Phasellus placerat mi diam, eget posuere lacus vulputate sit amet. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In felis ligula, dapibus at cursus in, bibendum sed ipsum. Praesent commodo, risus vel eleifend viverra, odio lectus feugiat lacus, nec tempus elit augue eu risus. Morbi enim augue, pretium sed orci vehicula, malesuada blandit dolor. Maecenas id nisl mollis, venenatis ipsum id, elementum purus. Etiam malesuada justo nunc, in efficitur massa efficitur sit amet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin efficitur scelerisque tellus at viverra. Duis posuere, arcu in ullamcorper dignissim, massa dolor imperdiet mi, et suscipit metus nulla ut augue. Maecenas consequat dolor at fermentum fringilla. Maecenas malesuada erat magna, in dapibus risus fermentum in. Vestibulum accumsan leo ut orci consequat, non rutrum magna finibus. Pellentesque luctus dictum mollis.\n\nPhasellus efficitur nisl et dapibus pellentesque. Aliquam gravida eros ex. Vivamus id eleifend dui. Nam lacinia, lacus sed pulvinar viverra, orci nibh finibus ligula, sit amet tristique sapien nibh sodales enim. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin consectetur libero nunc, ac rutrum ligula porttitor in. Duis venenatis, erat varius blandit convallis, ante risus luctus quam, iaculis vehicula libero tellus congue turpis. Vestibulum at velit pellentesque, placerat mauris et, pulvinar dolor. Cras porta nunc orci, eu blandit lacus dapibus eu. Nulla ornare felis ut dolor vestibulum cursus. Fusce et dignissim tellus. In in nulla mollis, dictum nisi tincidunt, suscipit eros. Phasellus posuere nulla vel ornare sagittis. Integer laoreet justo ut tempor facilisis. Suspendisse commodo consectetur nisl, pharetra feugiat lorem interdum ut.\n\nQuisque fringilla pretium nisl, nec sodales ante aliquam eget. Phasellus ac tempus purus. Donec et lectus sit amet lacus cursus ornare id porttitor ex. Sed porttitor, nisi non finibus egestas, velit dui laoreet felis, id sagittis nibh lorem vitae diam. Vestibulum mattis feugiat aliquam. Sed gravida lorem ut erat scelerisque, eget lacinia lectus lacinia. Vestibulum interdum velit sed vulputate dignissim. Vestibulum tincidunt hendrerit nisl aliquam dictum.', '[\"Responsibility 1\",\"Responsibility 2\",\"Responsibility 3\",\"Responsibility 4\",\"Responsibility 5\"]', '[\"Qualification 1\",\"Qualification 2\",\"Qualification 3\",\"Qualification 4\",\"Qualification 5\"]', '1 Year', '1240 Brgy. Apolonio Samson EDSA, Balintawak, Quezon City', '', '', '', '2019-12-31 00:00:00', '2018-03-10 02:13:03', '2019-09-01 01:11:44', 8, 0),
(11, 1, '120180310022144vH82w4ndfm45MgqQ.jpg', '120180310022150ngEWMHacpkijeISH.jpg', 'Concession Staff		        							     ', 'Kitchen Beauty Marketing Corporation							      																					', 'Manufacturing', 'Office/Full Time', 'Local', 'PH', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vitae mollis lorem. Etiam a elementum quam, porta consectetur nibh. Quisque porta egestas neque id semper. Curabitur semper velit quis turpis dignissim, semper gravida velit tempus. Fusce lobortis urna sed enim consequat cursus. Cras facilisis ligula tellus, euismod blandit nunc pretium non. Mauris quis erat semper, posuere odio non, viverra massa. Nulla facilisi. Curabitur id laoreet nunc.\n\nPhasellus mollis tempus lacus id venenatis. Suspendisse sollicitudin ex laoreet purus scelerisque viverra. Sed viverra risus felis, at volutpat mauris eleifend et. Aliquam eu dapibus orci, nec blandit lorem. Nunc ut mattis massa. Maecenas efficitur ipsum eget dolor luctus, eu molestie turpis pharetra. Etiam ornare, dui sed egestas vestibulum, est justo hendrerit nibh, gravida venenatis justo massa vitae ex. Nulla placerat lorem eu metus commodo, at maximus sem congue. Sed eu ligula nec ligula facilisis lobortis.\n\nNullam non leo ac erat aliquet condimentum eget non erat. Phasellus placerat mi diam, eget posuere lacus vulputate sit amet. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In felis ligula, dapibus at cursus in, bibendum sed ipsum. Praesent commodo, risus vel eleifend viverra, odio lectus feugiat lacus, nec tempus elit augue eu risus. Morbi enim augue, pretium sed orci vehicula, malesuada blandit dolor. Maecenas id nisl mollis, venenatis ipsum id, elementum purus. Etiam malesuada justo nunc, in efficitur massa efficitur sit amet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin efficitur scelerisque tellus at viverra. Duis posuere, arcu in ullamcorper dignissim, massa dolor imperdiet mi, et suscipit metus nulla ut augue. Maecenas consequat dolor at fermentum fringilla. Maecenas malesuada erat magna, in dapibus risus fermentum in. Vestibulum accumsan leo ut orci consequat, non rutrum magna finibus. Pellentesque luctus dictum mollis.\n\nPhasellus efficitur nisl et dapibus pellentesque. Aliquam gravida eros ex. Vivamus id eleifend dui. Nam lacinia, lacus sed pulvinar viverra, orci nibh finibus ligula, sit amet tristique sapien nibh sodales enim. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin consectetur libero nunc, ac rutrum ligula porttitor in. Duis venenatis, erat varius blandit convallis, ante risus luctus quam, iaculis vehicula libero tellus congue turpis. Vestibulum at velit pellentesque, placerat mauris et, pulvinar dolor. Cras porta nunc orci, eu blandit lacus dapibus eu. Nulla ornare felis ut dolor vestibulum cursus. Fusce et dignissim tellus. In in nulla mollis, dictum nisi tincidunt, suscipit eros. Phasellus posuere nulla vel ornare sagittis. Integer laoreet justo ut tempor facilisis. Suspendisse commodo consectetur nisl, pharetra feugiat lorem interdum ut.\n\nQuisque fringilla pretium nisl, nec sodales ante aliquam eget. Phasellus ac tempus purus. Donec et lectus sit amet lacus cursus ornare id porttitor ex. Sed porttitor, nisi non finibus egestas, velit dui laoreet felis, id sagittis nibh lorem vitae diam. Vestibulum mattis feugiat aliquam. Sed gravida lorem ut erat scelerisque, eget lacinia lectus lacinia. Vestibulum interdum velit sed vulputate dignissim. Vestibulum tincidunt hendrerit nisl aliquam dictum.', '[\"Responsibility 1\",\"Responsibility 2\",\"Responsibility 3\",\"Responsibility 4\",\"Responsibility 5\"]', '[\"Qualification 1\",\"Qualification 2\",\"Qualification 3\",\"Qualification 4\",\"Qualification 5\"]', '3 Years', 'Unit 1203 State Investment Building, 333 Juan Luna, Binondo, Manila', '', '', '242-1731', '2019-12-31 00:00:00', '2018-03-10 02:25:48', '2019-09-01 01:11:19', 9, 0),
(14, 1, '1201803171614291C9NosB9RH5CcwTF.jpg', '120180317161441qsNjmS0pinK8NBmv.jpg', 'We Are Hiring', 'The New Boston', 'Services/Others', 'All', 'All', 'PH', '<h3 style=\"text-align:justify\">The Header</h3>\n\n<p style=\"text-align:justify\"><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit</strong>.&nbsp;<span style=\"font-family:comic sans ms,cursive\">Proin felis sem, feugiat non elit id, tempor tincidunt urna. Aliquam sit amet tristique leo. Vestibulum quis gravida neque. Sed ultrices efficitur scelerisque. Vestibulum pulvinar lacus quis nisl suscipit aliquet. Fusce imperdiet ante ipsum, nec molestie est vestibulum eu. Quisque a vehicula ligula. Nullam sit amet viverra enim.</span></p>\n\n<ul>\n	<li style=\"text-align:justify\">Hello</li>\n	<li style=\"text-align:justify\">World</li>\n</ul>\n\n<p style=\"text-align:justify\">aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa</p>\n<p style=\"text-align:justify\">Phasellus hendrerit suscipit lectus in volutpat. Quisque vulputate magna mattis, porta turpis id, tristique diam. Quisque at pulvinar nulla. Pellentesque lacinia, risus at elementum vulputate, massa nibh sodales ante, ac bibendum est erat id justo. Maecenas laoreet quam semper posuere vulputate. Vivamus blandit diam metus, non congue sapien tempor sed. Nullam metus tortor, pretium facilisis sodales nec, fringilla vitae nisl. Mauris ut est eu erat consectetur tincidunt eget sed massa. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nullam quis elit tellus. Quisque id laoreet diam, vitae interdum augue. Maecenas justo dui, feugiat quis lobortis a, hendrerit id felis.</p>\n<p style=\"text-align:justify\">Nullam ac faucibus leo, eget luctus risus. Aliquam auctor, nulla at laoreet facilisis, lectus est vulputate ex, quis imperdiet erat leo eu orci. Aenean ac neque posuere, blandit lectus nec, finibus elit. Nulla dignissim suscipit enim, ut mollis sapien ornare eget. Pellentesque mi elit, blandit at quam ac, feugiat pellentesque nisi. Proin efficitur vehicula finibus. Integer sed nulla et ex dapibus sodales elementum ac orci. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce accumsan urna libero, et sollicitudin libero iaculis et. Proin vel urna at neque tempor mattis. Quisque vitae leo leo. Aliquam dictum aliquam nisl, eget interdum dolor. Nam tempor euismod massa, ac fermentum odio malesuada a. Vestibulum fermentum dapibus sagittis. Maecenas interdum orci nisl, in cursus arcu dignissim ac. Vivamus sit amet enim libero.</p>\n<p style=\"text-align:justify\">Sed lacinia turpis ut justo eleifend, a cursus felis faucibus. Pellentesque consequat, mauris vel posuere gravida, ex dolor rutrum est, vel mollis felis elit vel ligula. Maecenas rutrum id nisi malesuada eleifend. Aliquam ipsum sem, tincidunt at pretium suscipit, posuere at nunc. Pellentesque id finibus tortor. Mauris in euismod quam, at vehicula sapien. Sed ac maximus dui. Maecenas porta convallis urna in lobortis. Ut fringilla vitae ante in fermentum.</p>\n<p style=\"text-align:justify\">Donec in massa vitae erat tempus iaculis et in nisl. Phasellus vel velit vel lorem iaculis viverra. Morbi placerat vitae odio eu eleifend. Integer pharetra elit porta ex mollis, nec fringilla erat efficitur. Fusce quis rhoncus lacus. Ut tempus leo tortor. Praesent mi elit, tincidunt sit amet tincidunt vitae, vehicula a enim. Nullam a magna nec est interdum molestie. Maecenas ut tempor quam. Praesent consectetur purus eget rhoncus varius. Praesent sit amet leo eu erat feugiat pretium venenatis eget eros. Vivamus ornare ipsum sit amet elit ultrices luctus. Nam eu placerat lectus. Integer in nisl ac elit mattis convallis in eget quam. In feugiat tincidunt consequat.</p>', '', '[\"Fluent in English with American or British Accent.\",\"Technical Support\"]', '6 Months to 1 Year', '#56 William Shaw St. Caloocan City.', 'https://thenewboston.com', 'user@google.com', '09267294114', '2019-12-31 00:00:00', '2018-03-17 16:14:57', '2019-09-01 01:07:21', 10, 1),
(15, 1, '120181130201315Am5YVyjmeeOqUoJI.jpg', '120181130201324oSX77dwDLrKOndTz.jpg', 'Fresh Graduates can Join VXI QC Panorama CS Account with ?19K/Month + Incentives Job - VXI Global Holdings B.V. (Philippines) - Quezon City - 8544806', 'Global Holdings B.V. (Philippines) - Quezon City', 'Services/Others', 'Office/Full Time', 'Local', 'PH', '<p><strong>VXI Global Solutions</strong>, formerly Multi-Cultural Marketing, was initially created as an integrated customer contact center in 1998. We rapidly grew our vertical market expertise and language capabilities, specializing in call center and BPO services, multilingual support, software development, quality assurance testing, and infrastructure outsourcing. It didn&amp;rsquo;t take long for us to join the ranks of the top 50 telemarketing companies in the United States. In 2001, we rebranded to VXI Global Solutions, Inc. to better reflect the breadth and sophistication of services and solutions that we provide to our clients.</p><p><br />\nContinuing on our growth path, VXI expanded operations into China in 2005 by acquiring the necessary licenses to operate throughout the country. Our experience in the Chinese market has allowed us to assist multinational companies looking to expand their reach into one of the world&amp;rsquo;s largest global economies shaping the next phase of their growth.</p><p><br />\nIn 2013, Bain Capital made a minority investment to help the company add experienced personnel, expand our geographic footprint, and to invest in best-in-class technologies and processes. VXI acquired global ITO and R&amp;amp;D technology development company Symbio in December 2014. The move signified an expansion in our programming and technology development strategy.</p><p><br />\nWith over 28,000 people across 42 locations worldwide, VXI Global Solutions is one of the fastest growing, privately held business services organizations in the United States. Today, our client partners rely on our complete range of customer management contact center and technology solutions to retain and grow their customer base while maintaining the highest level of quality and operational excellence.</p>', '', '[\"Minimum of 1 year experience.\",\"Graduate of Bachelor of Science, Vocational, or Short Courses is required.\",\"Fluent in English with American or British Accent.\"]', '6 Months to 1 Year', 'G/F Panorama TechnoCenter, 1029 EDSA, Quezon City.', '', 'vxi@gmail.com', '09267294114', '2020-05-31 00:00:00', '2018-11-30 20:14:42', '2019-09-01 01:06:34', 11, 1),
(16, 1, '120181202015109FbPE2Xasvt4KF1ry.jpg', '120181202015119LCSUoQ4xBAHzIKNs.jpg', 'MEDICAL ASSOCIATE-FIXED WEEKENDS OFF EARN AS MUCH AS 25K Job - Access Healthcare Services Manila, Inc.', 'Access Healthcare Services Manila, Inc', 'Health Care', 'Office/Full Time', 'Local', 'PH', '<p><span style=\"color:rgb(0, 0, 0); font-family:monospace; font-size:medium\">Give your career a boost by becoming a Client Partner- Clinical Review- Prior Authorization Services with Access Healthcare. We are always interested in talking to inspired, talented, and motivated people. Many opportunities are available to join our vibrant culture. Review and apply online below.</span></p>', '', '', '6 Months to 1 Year', '6th Floor, Unit B, One World Square, McKinley Hill, Taguig City.', '', '', '823-8280 / 0917-8332007', '2019-12-31 00:00:00', '2018-12-02 01:51:38', '2019-09-01 01:06:04', 12, 1),
(17, 57, '5720190110222705kAusP3ssIt5XB9fX.jpg', '5720190110222713yeqP6HaxrxFtHEGH.jpg', 'Graphic Artist', 'Data Computer Forms, Incorporated', 'Services/Others', 'Office/Full Time', 'Local', 'PH', '<p><strong>More than 43 years and going strong ! Data Computer Forms, Inc. is a company committed to integrity, quality products and excellent customer service. While our Business is our Client, our people are our greatest strength! &amp;nbsp;Extensive PRODUCTS/SERVICES that includes, among others:</strong></p>\n\n<ul>\n	<li>Business Forms in Continuous, Cut Sheet, Booklet, Padded Formats</li>\n	<li>High Quality Spot or Full Color printing</li>\n	<li>Customized computerized Checks</li>\n	<li>Transaction Rolls, ECG Rolls</li>\n	<li>Security Forms</li>\n	<li>Certificates</li>\n	<li>Adhesive Labels</li>\n</ul>\n', '[\"Create new design layout\",\"Edit existing design layout\",\"Revise existing design layout\"]', '[\"COLLEGE\\/VOCATIONAL LEVEL\",\"Minimum one (1) year working experience\",\"Experience working in a printing company an advantage\",\"Training and Seminars in Graphic Design an advantage\",\"Keen to details\",\"Has a sense of urgency\",\"High level of integrity\",\"Good interpersonal and communication skills\"]', '5 Years to 5 Years', '#86 Domingo M. Guevarra St. Brgy. Mauway Mandaluyong City', 'http://thenewboston.com', 'vxi@gmail.com', '632 7181888, 632 7182888, 632 5310378', '2019-03-31 00:00:00', '2019-01-10 22:31:34', '2019-01-10 22:31:34', 13, 0);

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `log_type` varchar(255) DEFAULT NULL,
  `message` text DEFAULT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `log_type`, `message`, `date`) VALUES
(45, 'admin_clear_logs', 'Super Admin Admin Cleared Logs.', '2018-11-25 01:09:20'),
(46, 'login_log', 'admin@localhost.com Logged in.', '2018-11-25 12:37:05'),
(47, 'forgot_password_log', 'yuel@localhost.com request for password.', '2018-11-25 20:14:08'),
(48, 'login_log', 'admin@localhost.com Logged in.', '2018-11-25 20:15:08'),
(49, 'admin_user_profile_change', 'Super Admin Admin update Ruby Rose\'s profile.', '2018-11-25 21:34:20'),
(50, 'admin_user_profile_change', 'Super Admin Admin update Serah Farron\'s profile.', '2018-11-25 21:35:56'),
(51, 'admin_user_profile_change', 'Super Admin Admin update Ruby Rose\'s profile.', '2018-11-25 22:09:33'),
(52, 'login_log', 'admin@localhost.com Logged in.', '2018-11-26 21:01:34'),
(53, 'admin_delete_mailinglist', 'Super Admin Admin delete a mailing list. (Test Mailing List).', '2018-11-27 07:43:53'),
(54, 'admin_delete_mailinglist', 'Super Admin Admin delete a mailing list. (aaa).', '2018-11-27 07:44:30'),
(55, 'admin_delete_mailinglist', 'Super Admin Admin delete a mailing list. (Test Mailing List).', '2018-11-27 07:44:41'),
(56, 'admin_delete_mailinglist', 'Super Admin Admin delete a mailing list. (ssss).', '2018-11-27 07:45:42'),
(57, 'admin_delete_mailinglist', 'Super Admin Admin delete a mailing list. (aaa).', '2018-11-27 07:46:04'),
(58, 'admin_delete_mailinglist', 'Super Admin Admin delete a mailing list. (aaa).', '2018-11-27 07:46:29'),
(59, 'admin_delete_mailinglist', 'Super Admin Admin delete a mailing list. (aaa).', '2018-11-27 07:46:34'),
(60, 'admin_delete_mailinglist', 'Super Admin Admin delete a mailing list. (aaa).', '2018-11-27 07:46:39'),
(61, 'login_log', 'victor@localhost.com Logged in.', '2018-11-27 08:07:42'),
(62, 'login_log', 'admin@localhost.com Logged in.', '2018-11-27 12:36:02'),
(63, 'admin_user_profile_change', 'Super Admin Admin update Ruby Rose\'s profile.', '2018-11-27 12:36:59'),
(64, 'admin_user_profile_change', 'Super Admin Admin update Ruby Rose\'s profile.', '2018-11-27 12:37:00'),
(65, 'login_log', 'rwby@localhost.com Logged in.', '2018-11-27 12:38:23'),
(66, 'admin_user_profile_change', 'Super Admin Admin update Ruby Rose\'s profile.', '2018-11-27 12:48:43'),
(67, 'login_log', 'admin@localhost.com Logged in.', '2018-11-27 13:00:29'),
(68, 'admin_user_profile_change', 'Super Admin Admin update Ruby Rose\'s profile.', '2018-11-27 13:01:08'),
(69, 'admin_user_profile_change', 'Super Admin Admin update Ruby Rose\'s profile.', '2018-11-27 13:01:59'),
(70, 'login_log', 'victor@localhost.com Logged in.', '2018-11-27 13:16:26'),
(71, 'login_log', 'admin@localhost.com Logged in.', '2018-11-29 20:25:12'),
(72, 'login_log', 'victor@localhost.com Logged in.', '2018-11-29 20:34:45'),
(73, 'login_log', 'victor@localhost.com Logged in.', '2018-11-30 19:18:43'),
(74, 'login_log', 'victor@localhost.com Logged in.', '2018-11-30 19:19:36'),
(75, 'create_jobposts_log', 'Victor Caviteno \"(victor@localhost.com)\" created a Job Post Fresh Graduates can Join VXI QC Panorama CS Account with ?19K/Month + Incentives Job - VXI Global Holdings B.V. (Philippines) - Quezon City - 8544806\".', '2018-11-30 20:14:42'),
(76, 'login_log', 'yuel@localhost.com Logged in.', '2018-12-01 03:43:35'),
(77, 'login_log', 'victor@localhost.com Logged in.', '2018-12-01 03:52:03'),
(78, 'login_log', 'admin@localhost.com Logged in.', '2018-12-01 04:35:22'),
(79, 'edit_jobposts_log', 'Victor Caviteno edit a Job Post \"(victor@localhost.com)\" \"We Are Hiring\"', '2018-12-01 06:31:18'),
(80, 'edit_jobposts_log', 'Victor Caviteno edit a Job Post \"(victor@localhost.com)\" \"Programmer\"', '2018-12-01 06:34:20'),
(81, 'edit_jobposts_log', 'Victor Caviteno edit a Job Post \"(victor@localhost.com)\" \"We Are Hiring\"', '2018-12-01 06:36:38'),
(82, 'login_log', 'admin@localhost.com Logged in.', '2018-12-01 18:28:37'),
(83, 'login_log', 'admin@localhost.com Logged in.', '2018-12-01 23:49:59'),
(84, 'admin_jobpost_edit_log', 'Super Admin Admin edit a jobpost (We Are Hiring)', '2018-12-02 00:22:41'),
(85, 'login_log', 'victor@localhost.com Logged in.', '2018-12-02 00:27:49'),
(86, 'login_log', 'admin@localhost.com Logged in.', '2018-12-02 01:33:03'),
(87, 'create_jobposts_log', 'Victor Caviteno \"(victor@localhost.com)\" created a Job Post \"MEDICAL ASSOCIATE-FIXED WEEKENDS OFF EARN AS MUCH AS 25K Job - Access Healthcare Services Manila, Inc.\".', '2018-12-02 01:51:39'),
(88, 'login_log', 'admin@localhost.com Logged in.', '2018-12-02 02:25:17'),
(89, 'admin_jobpost_edit_log', 'Super Admin Admin edit a jobpost (Fresh Graduates can Join VXI QC Panorama CS Account with ?19K/Month + Incentives Job - VXI Global Holdings B.V. (Philippines) - Quezon City - 8544806)', '2018-12-02 05:47:09'),
(90, 'login_log', 'yuel@localhost.com Logged in.', '2018-12-02 06:21:52'),
(91, 'login_log', 'yuel@localhost.com Logged in.', '2018-12-02 13:41:14'),
(92, 'login_log', 'admin@localhost.com Logged in.', '2018-12-02 15:01:32'),
(93, 'admin_jobpost_edit_log', 'Super Admin Admin edit a jobpost (We Are Hiring)', '2018-12-02 16:11:55'),
(94, 'admin_jobpost_edit_log', 'Super Admin Admin edit a jobpost (We Are Hiring)', '2018-12-02 18:17:05'),
(95, 'update_account_profile_log', 'Yuel Paddra Psu (yuel@localhost.com) update his/her profile.', '2018-12-02 20:11:59'),
(96, 'admin_jobpost_edit_log', 'Super Admin Admin edit a jobpost (We Are Hiring)', '2018-12-03 08:41:07'),
(97, 'login_log', 'victor@localhost.com Logged in.', '2018-12-03 13:08:03'),
(98, 'update_account_profile_log', 'Yuel Paddra Psu (yuel@localhost.com) update his/her profile.', '2018-12-04 13:31:09'),
(99, 'admin_jobpost_edit_log', 'Super Admin Admin edit a jobpost (We Are Hiring)', '2018-12-04 18:12:51'),
(100, 'login_log', 'yuel@localhost.com Logged in.', '2018-12-05 14:12:53'),
(101, 'login_log', 'admin@localhost.com Logged in.', '2018-12-05 20:04:02'),
(102, 'login_log', 'yuel@localhost.com Logged in.', '2018-12-05 20:04:49'),
(103, 'admin_jobpost_edit_log', 'Super Admin Admin edit a jobpost (Fresh Graduates can Join VXI QC Panorama CS Account with ?19K/Month + Incentives Job - VXI Global Holdings B.V. (Philippines) - Quezon City - 8544806)', '2018-12-06 00:55:18'),
(104, 'login_log', 'yuel@localhost.com Logged in.', '2018-12-06 02:14:14'),
(105, 'login_log', 'yuel@localhost.com Logged in.', '2018-12-06 13:49:39'),
(106, 'admin_user_profile_change', 'Super Admin Admin update Ruby Rose\'s profile.', '2018-12-06 14:57:55'),
(107, 'admin_user_profile_change', 'Super Admin Admin update Tifa Lockhart\'s profile.', '2018-12-06 15:00:59'),
(108, 'admin_user_profile_change', 'Super Admin Admin update Terra Brandford\'s profile.', '2018-12-06 15:02:31'),
(109, 'admin_user_profile_change', 'Super Admin Admin update Dia Vanille Oerba\'s profile.', '2018-12-06 18:36:34'),
(110, 'login_log', 'victor@localhost.com Logged in.', '2018-12-06 20:08:35'),
(111, 'login_log', 'yuel@localhost.com Logged in.', '2018-12-07 01:20:38'),
(112, 'login_log', 'yuel@localhost.com Logged in.', '2018-12-07 01:28:50'),
(113, 'login_log', 'victor@localhost.com Logged in.', '2018-12-07 02:01:08'),
(114, 'login_log', 'yuel@localhost.com Logged in.', '2018-12-08 00:08:20'),
(115, 'login_log', 'yuel@localhost.com Logged in.', '2018-12-09 18:56:58'),
(116, 'login_log', 'admin@localhost.com Logged in.', '2018-12-10 20:35:27'),
(117, 'login_log', 'yuel@localhost.com Logged in.', '2018-12-10 20:37:13'),
(118, 'login_log', 'victor@localhost.com Logged in.', '2018-12-10 22:32:59'),
(119, 'login_log', 'admin@localhost.com Logged in.', '2018-12-10 22:44:05'),
(120, 'login_log', 'yuel@localhost.com Logged in.', '2018-12-10 22:47:30'),
(121, 'login_log', 'yuel@localhost.com Logged in.', '2018-12-10 22:50:40'),
(122, 'login_log', 'yuel@localhost.com Logged in.', '2018-12-10 22:58:55'),
(123, 'login_log', 'yuel@localhost.com Logged in.', '2018-12-11 01:17:41'),
(124, 'login_log', 'admin@localhost.com Logged in.', '2018-12-11 01:18:12'),
(125, 'login_log', 'admin@localhost.com Logged in.', '2018-12-12 09:03:09'),
(126, 'login_log', 'admin@localhost.com Logged in.', '2018-12-12 20:32:21'),
(127, 'login_log', 'yuel@localhost.com Logged in.', '2018-12-12 23:17:19'),
(128, 'login_log', 'yuel@localhost.com Logged in.', '2018-12-13 05:20:43'),
(129, 'password_change_log', 'Super Admin Admin (admin@localhost.com) change his/her password.', '2018-12-13 23:18:12'),
(130, 'password_change_log', 'Super Admin Admin (admin@localhost.com) change his/her password.', '2018-12-14 03:58:01'),
(131, 'password_change_log', 'Super Admin Admin (admin@localhost.com) change his/her password.', '2018-12-14 03:59:25'),
(132, 'password_change_log', 'Super Admin Admin (admin@localhost.com) change his/her password.', '2018-12-14 04:00:31'),
(133, 'password_change_log', 'Super Admin Admin (admin@localhost.com) change his/her password.', '2018-12-14 04:01:26'),
(134, 'password_change_log', 'Super Admin Admin (admin@localhost.com) change his/her password.', '2018-12-14 04:02:31'),
(135, 'login_log', 'admin@localhost.com Logged in.', '2018-12-15 05:57:12'),
(136, 'admin_user_suspend', 'Super Admin Admin suspend.', '2018-12-15 05:58:03'),
(137, 'admin_user_suspend', 'Super Admin Admin suspend.', '2018-12-15 05:58:15'),
(138, 'admin_user_suspend', 'Super Admin Admin suspend.', '2018-12-15 06:37:10'),
(139, 'admin_user_suspend', 'Super Admin Admin suspend.', '2018-12-15 06:46:43'),
(140, 'admin_user_suspend', 'Super Admin Admin suspend.', '2018-12-15 06:46:50'),
(141, 'admin_user_suspend', 'Super Admin Admin suspend.', '2018-12-15 06:46:55'),
(142, 'admin_user_suspend', 'Super Admin Admin suspend.', '2018-12-15 06:47:01'),
(143, 'admin_user_suspend', 'Super Admin Admin suspend.', '2018-12-15 06:56:46'),
(144, 'admin_user_suspend', 'Super Admin Admin unsuspend Ruby Rose\'s account.', '2018-12-15 06:57:02'),
(145, 'admin_user_suspend', 'Super Admin Admin suspend.', '2018-12-15 06:57:35'),
(146, 'admin_user_suspend', 'Super Admin Admin suspend.', '2018-12-15 08:10:36'),
(147, 'admin_user_suspend', 'Super Admin Admin unsuspend Ruby Rose\'s account.', '2018-12-15 08:11:03'),
(148, 'admin_user_suspend', 'Super Admin Admin suspend.', '2018-12-15 08:15:44'),
(149, 'admin_user_suspend', 'Super Admin Admin unsuspend Ruby Rose\'s account.', '2018-12-15 08:15:53'),
(150, 'admin_user_suspend', 'Super Admin Admin suspend.', '2018-12-15 08:44:48'),
(151, 'admin_user_suspend', 'Super Admin Admin suspend.', '2018-12-15 09:00:50'),
(152, 'login_log', 'admin@localhost.com Logged in.', '2018-12-15 11:18:39'),
(153, 'login_log', 'yuel@localhost.com Logged in.', '2018-12-17 06:45:57'),
(154, 'update_account_profile_log', 'Yuel Paddra Psu (yuel@localhost.com) update his/her profile.', '2018-12-17 06:46:38'),
(155, 'create_account_profile_log', 'Super Admin Admin (admin@localhost.com) create his/her profile.', '2018-12-18 20:56:55'),
(156, 'create_account_profile_log', 'Super Admin Admin (admin@localhost.com) create his/her profile.', '2018-12-18 21:04:45'),
(157, 'create_account_profile_log', 'Super Admin Admin (admin@localhost.com) create his/her profile.', '2018-12-18 21:05:20'),
(158, 'login_log', 'yuel@localhost.com Logged in.', '2018-12-19 06:34:36'),
(159, 'register_log', '127.0.0.1 create an account. Email: cloud@localhost.com', '2018-12-19 08:27:43'),
(160, 'account_confirm_log', 'cloud@localhost.com confirmed his/her account', '2018-12-19 08:28:18'),
(161, 'register_log', '127.0.0.1 create an account. Email: cloud@localhost.com', '2018-12-19 08:46:31'),
(162, 'register_log', '127.0.0.1 create an account. Email: cloud@localhost.com', '2018-12-19 08:59:28'),
(163, 'register_log', '127.0.0.1 create an account. Email: cloud@localhost.com', '2018-12-19 09:37:11'),
(164, 'register_log', '127.0.0.1 create an account. Email: cloud@localhost.com', '2018-12-19 10:06:49'),
(165, 'register_log', '127.0.0.1 create an account. Email: cloud@localhost.com', '2018-12-19 10:10:34'),
(166, 'register_log', '127.0.0.1 create an account. Email: cloud@localhost.com', '2018-12-19 10:41:23'),
(167, 'account_confirm_log', 'cloud@localhost.com confirmed his/her account', '2018-12-19 10:45:58'),
(168, 'login_log', 'admin@localhost.com Logged in.', '2018-12-19 10:47:03'),
(169, 'admin_user_change_role', 'Super Admin Admin change Cloud Strife account role as Employer.', '2018-12-19 10:47:36'),
(170, 'login_log', 'victor@localhost.com Logged in.', '2018-12-19 23:19:29'),
(171, 'login_log', 'admin@localhost.com Logged in.', '2018-12-20 03:05:50'),
(172, 'login_log', 'yuel@localhost.com Logged in.', '2018-12-20 03:22:47'),
(173, 'login_log', 'victor@localhost.com Logged in.', '2018-12-21 01:48:53'),
(174, 'login_log', 'yuel@localhost.com Logged in.', '2018-12-21 03:14:21'),
(175, 'login_log', 'victor@localhost.com Logged in.', '2018-12-21 03:37:37'),
(176, 'login_log', 'victor@localhost.com Logged in.', '2018-12-21 03:38:13'),
(177, 'login_log', 'admin@localhost.com Logged in.', '2018-12-21 03:54:01'),
(178, 'login_log', 'rwby@localhost.com Logged in.', '2018-12-21 06:10:00'),
(179, 'login_log', 'yuel@localhost.com Logged in.', '2018-12-21 07:16:39'),
(180, 'login_log', 'victor@localhost.com Logged in.', '2018-12-21 08:07:33'),
(181, 'login_log', 'gervic@localhost.com Logged in.', '2018-12-22 04:43:03'),
(182, 'login_log', 'admin@localhost.com Logged in.', '2018-12-23 06:28:09'),
(183, 'user_subscribe_mailing_list', 'thor@localhost.com subscribed to our mailing joblist.', '2018-12-23 07:15:43'),
(184, 'admin_resend_mailinglist', 'Super Admin Admin resend a mailing list. (Test Mailing List).', '2018-12-23 07:19:07'),
(185, 'admin_resend_mailinglist', 'Super Admin Admin resend a mailing list. (Test Mailing List).', '2018-12-23 07:31:35'),
(186, 'register_log', '127.0.0.1 create an account. Email: thor@localhost.com', '2018-12-23 08:22:57'),
(187, 'user_subscribe_mailing_list', 'gervic.23@localhost.com subscribed to our mailing joblist.', '2018-12-23 14:47:53'),
(188, 'user_subscribe_mailing_list', 'boytusok@thenewboston.net subscribed to our mailing joblist.', '2018-12-24 05:24:48'),
(189, 'user_subscribe_mailing_list', 'edgar_the_baby_maker@localhost.com subscribed to our mailing joblist.', '2018-12-24 05:25:53'),
(190, 'user_subscribe_mailing_list', 'juanito_banana_eater@thenewboston.net subscribed to our mailing joblist.', '2018-12-24 05:27:54'),
(191, 'user_subscribe_mailing_list', 'gervic24.22@yahoo.net subscribed to our mailing joblist.', '2018-12-24 05:28:28'),
(192, 'user_subscribe_mailing_list', 'thor@localhost.com subscribed to our mailing joblist.', '2018-12-24 06:23:59'),
(193, 'admin_resend_mailinglist', 'Super Admin Admin resend a mailing list. (Test Mailing List).', '2018-12-24 06:24:39'),
(194, 'admin_resend_mailinglist', 'Super Admin Admin resend a mailing list. (Test Mailing List).', '2018-12-24 06:26:11'),
(195, 'admin_resend_mailinglist', 'Super Admin Admin resend a mailing list. (aaa).', '2018-12-24 06:29:34'),
(196, 'admin_resend_mailinglist', 'Super Admin Admin resend a mailing list. (ssss).', '2018-12-24 06:50:02'),
(197, 'admin_create_mailinglist', 'Super Admin Admin created a mailing list. (X-mas Test).', '2018-12-24 07:01:01'),
(198, 'login_log', 'yuel@localhost.com Logged in.', '2018-12-25 13:45:59'),
(199, 'forgot_password_log', 'yuel@localhost.com request for password.', '2018-12-26 15:19:34'),
(200, 'password_recovery_log', 'yuel@localhost.com recovered its password.', '2018-12-26 15:43:15'),
(201, 'login_log', 'yuel@localhost.com Logged in.', '2018-12-26 20:26:11'),
(202, 'login_log', 'yuel@localhost.com Logged in.', '2018-12-27 09:00:05'),
(203, 'login_log', 'victor@localhost.com Logged in.', '2018-12-27 11:01:49'),
(204, 'login_log', 'rwby@localhost.com Logged in.', '2018-12-27 16:22:51'),
(205, 'login_log', 'yuel@localhost.com Logged in.', '2018-12-28 08:02:04'),
(206, 'login_log', 'yuel@localhost.com Logged in.', '2018-12-29 15:51:24'),
(207, 'login_log', 'admin@localhost.com Logged in.', '2018-12-31 10:50:53'),
(208, 'login_log', 'victor@localhost.com Logged in.', '2018-12-31 11:03:02'),
(209, 'edit_jobposts_log', 'Victor Caviteno edit a Job Post \"(victor@localhost.com)\" \"We Are Hiring\"', '2018-12-31 11:04:11'),
(210, 'edit_jobposts_log', 'Victor Caviteno edit a Job Post \"(victor@localhost.com)\" \"Concession Staff		        							     \"', '2018-12-31 11:04:31'),
(211, 'edit_jobposts_log', 'Victor Caviteno edit a Job Post \"(victor@localhost.com)\" \"TREASURY ASSISTANT - QUEZON CITY\"', '2018-12-31 11:04:53'),
(212, 'edit_jobposts_log', 'Victor Caviteno edit a Job Post \"(victor@localhost.com)\" \"Engineering Supervisor\"', '2018-12-31 11:05:15'),
(213, 'edit_jobposts_log', 'Victor Caviteno edit a Job Post \"(victor@localhost.com)\" \"Programmer\"', '2018-12-31 11:05:40'),
(214, 'edit_jobposts_log', 'Victor Caviteno edit a Job Post \"(victor@localhost.com)\" \"Tresury Assistant\"', '2018-12-31 11:06:07'),
(215, 'edit_jobposts_log', 'Victor Caviteno edit a Job Post \"(victor@localhost.com)\" \"PURE NON VOICE! CHAT AGENTS! NO EXPERIENCE NEEDED! MALL OF ASIA SITE! Job\"', '2018-12-31 11:06:38'),
(216, 'edit_jobposts_log', 'Victor Caviteno edit a Job Post \"(victor@localhost.com)\" \"ATTN: Accounting Staff | Bacolod\"', '2018-12-31 11:07:05'),
(217, 'edit_jobposts_log', 'Victor Caviteno edit a Job Post \"(victor@localhost.com)\" \"Company Nurse\"', '2018-12-31 11:07:25'),
(218, 'login_log', 'yuel@localhost.com Logged in.', '2018-12-31 11:41:51'),
(219, 'password_change_log', 'Yuel Paddra Psu (yuel@localhost.com) change his/her password.', '2018-12-31 16:39:08'),
(220, 'password_change_log', 'Yuel Paddra Psu (yuel@localhost.com) change his/her password.', '2018-12-31 16:39:53'),
(221, 'update_account_details_log', 'Yuel Paddra Psu (yuel@localhost.com) update his/her account details.', '2018-12-31 16:42:18'),
(222, 'update_account_details_log', 'Yuel Paddra Psu (yuel@localhost.com) update his/her account details.', '2018-12-31 16:43:03'),
(223, 'update_account_details_log', 'Yuel Paddra Psu (yuel@localhost.com) update his/her account details.', '2018-12-31 16:43:29'),
(224, 'login_log', 'cloud@localhost.com Logged in.', '2018-12-31 17:09:51'),
(225, 'login_log', 'cloud@localhost.com Logged in.', '2018-12-31 17:11:23'),
(226, 'login_log', 'yuel@localhost.com Logged in.', '2018-12-31 17:29:00'),
(227, 'login_log', 'victor@localhost.com Logged in.', '2018-12-31 20:22:58'),
(228, 'login_log', 'yuel@localhost.com Logged in.', '2019-01-01 08:14:18'),
(229, 'login_log', 'victor@localhost.com Logged in.', '2019-01-01 08:24:37'),
(230, 'login_log', 'yuel@localhost.com Logged in.', '2019-01-02 22:35:40'),
(231, 'login_log', 'victor@localhost.com Logged in.', '2019-01-03 11:43:20'),
(232, 'create_jobposts_log', 'Victor Caviteno \"(victor@localhost.com)\" created a Job Post \"fsdfsdf\".', '2019-01-03 13:36:14'),
(233, 'delete_jobposts_log', 'Victor Caviteno \"(victor@localhost.com)\" delete a Job Post \"fsdfsdf\"', '2019-01-03 13:36:45'),
(234, 'create_jobposts_log', 'Victor Caviteno \"(victor@localhost.com)\" created a Job Post \"\".', '2019-01-03 13:58:14'),
(235, 'login_log', 'victor@localhost.com Logged in.', '2019-01-03 14:02:57'),
(236, 'login_log', 'victor@localhost.com Logged in.', '2019-01-03 14:03:34'),
(237, 'create_jobposts_log', 'Victor Caviteno \"(victor@localhost.com)\" created a Job Post \"The new boston\".', '2019-01-03 14:39:44'),
(238, 'delete_jobposts_log', 'Victor Caviteno \"(victor@localhost.com)\" delete a Job Post \"The new boston\"', '2019-01-03 14:41:33'),
(239, 'create_jobposts_log', 'Victor Caviteno \"(victor@localhost.com)\" created a Job Post \"The new boston\".', '2019-01-03 14:46:10'),
(240, 'create_jobposts_log', 'Victor Caviteno \"(victor@localhost.com)\" created a Job Post \"The new boston\".', '2019-01-03 14:46:21'),
(241, 'delete_jobposts_log', 'Victor Caviteno \"(victor@localhost.com)\" delete a Job Post \"The new boston\"', '2019-01-03 14:46:56'),
(242, 'delete_jobposts_log', 'Victor Caviteno \"(victor@localhost.com)\" delete a Job Post \"The new boston\"', '2019-01-03 14:47:08'),
(243, 'create_jobposts_log', 'Victor Caviteno \"(victor@localhost.com)\" created a Job Post \"The new boston\".', '2019-01-03 14:47:42'),
(244, 'delete_jobposts_log', 'Victor Caviteno \"(victor@localhost.com)\" delete a Job Post \"The new boston\"', '2019-01-03 15:16:25'),
(245, 'create_jobposts_log', 'Victor Caviteno \"(victor@localhost.com)\" created a Job Post \"The new boston\".', '2019-01-03 15:30:52'),
(246, 'create_jobposts_log', 'Victor Caviteno \"(victor@localhost.com)\" created a Job Post \"The new boston\".', '2019-01-03 16:14:40'),
(247, 'login_log', 'victor@localhost.com Logged in.', '2019-01-03 17:47:56'),
(248, 'create_jobposts_log', 'Victor Caviteno \"(victor@localhost.com)\" created a Job Post \"\".', '2019-01-04 13:34:21'),
(249, 'delete_jobposts_log', 'Victor Caviteno \"(victor@localhost.com)\" delete a Job Post \"The new boston\"', '2019-01-04 13:34:52'),
(250, 'delete_jobposts_log', 'Victor Caviteno \"(victor@localhost.com)\" delete a Job Post \"The new boston\"', '2019-01-04 13:35:08'),
(251, 'delete_jobposts_log', 'Victor Caviteno \"(victor@localhost.com)\" delete a Job Post \"The new boston\"', '2019-01-04 13:35:48'),
(252, 'create_jobposts_log', 'Victor Caviteno \"(victor@localhost.com)\" created a Job Post \"\".', '2019-01-04 14:11:04'),
(253, 'create_jobposts_log', 'Victor Caviteno \"(victor@localhost.com)\" created a Job Post \"\".', '2019-01-04 14:11:48'),
(254, 'delete_jobposts_log', 'Victor Caviteno \"(victor@localhost.com)\" delete a Job Post \"The new boston\"', '2019-01-04 14:12:33'),
(255, 'delete_jobposts_log', 'Victor Caviteno \"(victor@localhost.com)\" delete a Job Post \"The new boston\"', '2019-01-04 14:12:48'),
(256, 'create_jobposts_log', 'Victor Caviteno \"(victor@localhost.com)\" created a Job Post \"\".', '2019-01-04 14:19:39'),
(257, 'create_jobposts_log', 'Victor Caviteno \"(victor@localhost.com)\" created a Job Post \"\".', '2019-01-04 14:19:43'),
(258, 'delete_jobposts_log', 'Victor Caviteno \"(victor@localhost.com)\" delete a Job Post \"The new boston\"', '2019-01-04 14:20:41'),
(259, 'delete_jobposts_log', 'Victor Caviteno \"(victor@localhost.com)\" delete a Job Post \"The new boston\"', '2019-01-04 14:21:04'),
(260, 'create_jobposts_log', 'Victor Caviteno \"(victor@localhost.com)\" created a Job Post \"\".', '2019-01-04 14:21:41'),
(261, 'create_jobposts_log', 'Victor Caviteno \"(victor@localhost.com)\" created a Job Post \"\".', '2019-01-04 14:23:39'),
(262, 'delete_jobposts_log', 'Victor Caviteno \"(victor@localhost.com)\" delete a Job Post \"The new boston\"', '2019-01-04 14:23:54'),
(263, 'delete_jobposts_log', 'Victor Caviteno \"(victor@localhost.com)\" delete a Job Post \"The new boston\"', '2019-01-04 14:24:04'),
(264, 'update_account_profile_log', 'Victor Caviteno (victor@localhost.com) update his/her profile.', '2019-01-04 14:25:20'),
(265, 'edit_jobposts_log', 'Victor Caviteno edit a Job Post \"(victor@localhost.com)\" \"We Are Hiring\"', '2019-01-04 14:26:06'),
(266, 'edit_jobposts_log', 'Victor Caviteno edit a Job Post \"(victor@localhost.com)\" \"MEDICAL ASSOCIATE-FIXED WEEKENDS OFF EARN AS MUCH AS 25K Job - Access Healthcare Services Manila, Inc.\"', '2019-01-04 14:34:50'),
(267, 'edit_jobposts_log', 'Victor Caviteno edit a Job Post \"(victor@localhost.com)\" \"MEDICAL ASSOCIATE-FIXED WEEKENDS OFF EARN AS MUCH AS 25K Job - Access Healthcare Services Manila, Inc.\"', '2019-01-04 14:35:31'),
(268, 'edit_jobposts_log', 'Victor Caviteno edit a Job Post \"(victor@localhost.com)\" \"We Are Hiring\"', '2019-01-04 16:46:02'),
(269, 'job_invitation_log', 'Victor Caviteno (victor@localhost.com) send job invitation to Ruby Rose', '2019-01-04 20:11:27'),
(270, 'update_account_profile_log', 'Victor Caviteno (victor@localhost.com) update his/her profile.', '2019-01-04 21:13:16'),
(271, 'update_account_details_log', 'Victor Caviteno (victor@localhost.com) update his/her account details.', '2019-01-04 21:33:44'),
(272, 'update_account_details_log', 'Victor Caviteno (victor@localhost.com) update his/her account details.', '2019-01-04 21:39:08'),
(273, 'password_change_log', 'Victor Caviteno (victor@localhost.com) change his/her password.', '2019-01-04 21:41:48'),
(274, 'password_change_log', 'Victor Caviteno (victor@localhost.com) change his/her password.', '2019-01-04 21:42:58'),
(275, 'update_account_profile_log', 'Victor Caviteno (victor@localhost.com) update his/her profile.', '2019-01-05 22:48:18'),
(276, 'update_account_details_log', 'Victor Caviteno (victor@localhost.com) update his/her account details.', '2019-01-05 22:55:57'),
(277, 'password_change_log', 'Victor Caviteno (victor@localhost.com) change his/her password.', '2019-01-05 23:01:05'),
(278, 'edit_jobposts_log', 'Victor Caviteno edit a Job Post \"(victor@localhost.com)\" \"We Are Hiring\"', '2019-01-05 23:02:51'),
(279, 'edit_jobposts_log', 'Victor Caviteno edit a Job Post \"(victor@localhost.com)\" \"MEDICAL ASSOCIATE-FIXED WEEKENDS OFF EARN AS MUCH AS 25K Job - Access Healthcare Services Manila, Inc.\"', '2019-01-05 23:03:27'),
(280, 'edit_jobposts_log', 'Victor Caviteno edit a Job Post \"(victor@localhost.com)\" \"MEDICAL ASSOCIATE-FIXED WEEKENDS OFF EARN AS MUCH AS 25K Job - Access Healthcare Services Manila, Inc.\"', '2019-01-05 23:03:59'),
(281, 'edit_jobposts_log', 'Victor Caviteno edit a Job Post \"(victor@localhost.com)\" \"Company Nurse\"', '2019-01-05 23:07:22'),
(282, 'edit_jobposts_log', 'Victor Caviteno edit a Job Post \"(victor@localhost.com)\" \"Company Nurse\"', '2019-01-05 23:14:53'),
(283, 'edit_jobposts_log', 'Victor Caviteno edit a Job Post \"(victor@localhost.com)\" \"We Are Hiring\"', '2019-01-05 23:15:16'),
(284, 'login_log', 'yuel@localhost.com Logged in.', '2019-01-05 23:16:41'),
(285, 'update_account_profile_log', 'Yuel Paddra Psu (yuel@localhost.com) update his/her profile.', '2019-01-05 23:22:23'),
(286, 'login_log', 'admin@localhost.com Logged in.', '2019-01-06 01:32:50'),
(287, 'admin_jobpost_edit_log', 'Super Admin Admin edit a jobpost (We Are Hiring)', '2019-01-06 01:34:02'),
(288, 'admin_user_change_role', 'Super Admin Admin change Cloud Strife account role as Employer.', '2019-01-06 01:46:23'),
(289, 'admin_jobpost_edit_log', 'Super Admin Admin edit a jobpost (We Are Hiring)', '2019-01-06 01:46:59'),
(290, 'login_log', 'victor@localhost.com Logged in.', '2019-01-06 01:58:36'),
(291, 'login_log', 'yuel@localhost.com Logged in.', '2019-01-06 09:20:43'),
(292, 'login_log', 'victor@localhost.com Logged in.', '2019-01-09 07:49:42'),
(293, 'create_account_profile_log', 'Victor Gerard Gervic Caviteno (gervic.23@gmail.com) create his/her profile.', '2019-01-09 07:58:42'),
(294, 'update_account_profile_log', 'Victor Gerard Gervic Caviteno (gervic.23@gmail.com) update his/her profile.', '2019-01-09 08:00:01'),
(295, 'register_log', '127.0.0.1 create an account. Email: cloud@localhost.com', '2019-01-10 21:52:16'),
(296, 'account_confirm_log', 'cloud@localhost.com confirmed his/her account', '2019-01-10 21:52:48'),
(297, 'register_log', '127.0.0.1 create an account. Email: cloud@localhost.com', '2019-01-10 22:09:10'),
(298, 'account_confirm_log', 'cloud@localhost.com confirmed his/her account', '2019-01-10 22:09:38'),
(299, 'register_log', '127.0.0.1 create an account. Email: cloud@localhost.com', '2019-01-10 22:24:14'),
(300, 'account_confirm_log', 'cloud@localhost.com confirmed his/her account', '2019-01-10 22:24:40'),
(301, 'create_jobposts_log', 'Cloud Strife \"(cloud@localhost.com)\" created a Job Post \"\".', '2019-01-10 22:31:35'),
(302, 'job_invitation_log', 'Cloud Strife (cloud@localhost.com) send job invitation to Terra Brandford', '2019-01-10 22:33:36'),
(303, 'login_log', 'terra@localhost.com Logged in.', '2019-01-10 22:34:59'),
(304, 'job_application_log', 'Terra Brandford (terra@localhost.com) send job application to Victor Caviteno. <strong>Job Title:</strong> We Are Hiring <strong>Company Name:</strong> The New Boston.', '2019-01-10 22:38:41'),
(305, 'login_log', 'victor@localhost.com Logged in.', '2019-01-10 22:39:16'),
(306, 'login_log', 'admin@localhost.com Logged in.', '2019-01-10 22:40:55'),
(307, 'admin_create_announcement_logs', 'Super Admin Admin create an announcement (Hello! This is a sample announcement.).', '2019-01-10 22:42:24'),
(308, 'login_log', 'yuel@localhost.com Logged in.', '2019-01-11 13:58:05'),
(309, 'login_log', 'admin@localhost.com Logged in.', '2019-02-02 21:15:34'),
(310, 'forgot_password_log', 'yuel@localhost.com request for password.', '2019-02-02 23:03:14'),
(311, 'forgot_password_log', 'yuel@localhost.com request for password.', '2019-02-03 11:47:45'),
(312, 'forgot_password_log', 'yuel@localhost.com request for password.', '2019-02-03 11:49:52'),
(313, 'forgot_password_log', 'yuel@localhost.com request for password.', '2019-02-03 11:51:08'),
(314, 'forgot_password_log', 'yuel@localhost.com request for password.', '2019-02-03 11:52:53'),
(315, 'login_log', 'yuel@localhost.com Logged in.', '2019-07-21 22:29:43'),
(316, 'login_log', 'yuel@localhost.com Logged in.', '2019-09-01 01:00:29'),
(317, 'login_log', 'admin@localhost.com Logged in.', '2019-09-01 01:02:59'),
(318, 'login_log', 'victor@localhost.com Logged in.', '2019-09-01 01:04:30'),
(319, 'password_change_log', 'Victor Caviteno (victor@localhost.com) change his/her password.', '2019-09-01 01:04:56'),
(320, 'edit_jobposts_log', 'Victor Caviteno edit a Job Post \"(victor@localhost.com)\" \"MEDICAL ASSOCIATE-FIXED WEEKENDS OFF EARN AS MUCH AS 25K Job - Access Healthcare Services Manila, Inc.\"', '2019-09-01 01:06:04'),
(321, 'edit_jobposts_log', 'Victor Caviteno edit a Job Post \"(victor@localhost.com)\" \"Fresh Graduates can Join VXI QC Panorama CS Account with ?19K/Month + Incentives Job - VXI Global Holdings B.V. (Philippines) - Quezon City - 8544806\"', '2019-09-01 01:06:34'),
(322, 'edit_jobposts_log', 'Victor Caviteno edit a Job Post \"(victor@localhost.com)\" \"We Are Hiring\"', '2019-09-01 01:07:21'),
(323, 'edit_jobposts_log', 'Victor Caviteno edit a Job Post \"(victor@localhost.com)\" \"Concession Staff		        							     \"', '2019-09-01 01:11:19'),
(324, 'edit_jobposts_log', 'Victor Caviteno edit a Job Post \"(victor@localhost.com)\" \"TREASURY ASSISTANT - QUEZON CITY\"', '2019-09-01 01:11:44'),
(325, 'edit_jobposts_log', 'Victor Caviteno edit a Job Post \"(victor@localhost.com)\" \"Engineering Supervisor\"', '2019-09-01 01:15:44'),
(326, 'edit_jobposts_log', 'Victor Caviteno edit a Job Post \"(victor@localhost.com)\" \"Programmer\"', '2019-09-01 01:16:11'),
(327, 'edit_jobposts_log', 'Victor Caviteno edit a Job Post \"(victor@localhost.com)\" \"Tresury Assistant\"', '2019-09-01 01:16:52'),
(328, 'edit_jobposts_log', 'Victor Caviteno edit a Job Post \"(victor@localhost.com)\" \"PURE NON VOICE! CHAT AGENTS! NO EXPERIENCE NEEDED! MALL OF ASIA SITE! Job\"', '2019-09-01 01:17:15');

-- --------------------------------------------------------

--
-- Table structure for table `mailinglists`
--

CREATE TABLE `mailinglists` (
  `id` int(11) NOT NULL,
  `content` text DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mailinglists`
--

INSERT INTO `mailinglists` (`id`, `content`, `subject`, `created_at`) VALUES
(1, '<p>Hi Fans!</p>', 'Test Mailing List', '2018-11-25 19:47:43'),
(2, '<p>Hello world</p>', 'Mailing Test', '2018-11-25 19:52:09'),
(3, '<p>Hello world</p>', 'Mailing Test', '2018-11-25 19:54:02'),
(4, '<p>Hello world</p>', 'Mailing Test', '2018-11-25 19:54:22'),
(5, '<p>Hello World!</p>', 'Mailing Test', '2018-11-25 19:55:07'),
(6, '<p>Hello World!</p>', 'Mailing Test', '2018-11-25 19:56:14'),
(7, '<p>Hello World!</p>', 'Mailing Test', '2018-11-25 20:00:18'),
(8, '<p>Hello World!</p>', 'Mailing Test', '2018-11-25 20:00:38'),
(9, '<p>Hello World!</p>', 'Mailing Test', '2018-11-25 20:04:31'),
(11, '<p>ddddd</p>', 'ssss', '2018-11-25 20:17:00'),
(13, '<p>bbbb</p>', 'aaa', '2018-11-25 20:36:49'),
(14, '<p>bbbb</p>', 'aaa', '2018-11-25 20:37:45'),
(15, '<p>bbbb</p>', 'aaa', '2018-11-25 20:40:33'),
(16, '<p>bbbb</p>', 'aaa', '2018-11-25 21:02:46'),
(24, '<p>Hello Fans! Now everything is working.</p>', 'Test Mailing List', '2018-11-25 21:36:37'),
(25, '<p>Hello Fans! Now everything is working. :D</p>', 'Test Mailing List', '2018-11-25 22:10:25'),
(26, '<p>Test Xnas</p>', 'X-mas Test', '2018-12-24 07:01:00'),
(27, '<p>This is a test send.</p>', 'Hello Users!', '2019-01-10 22:43:35'),
(28, '<p>This is a test send.</p>', 'Hello Users!', '2019-01-10 22:43:40');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `sender_id` bigint(20) NOT NULL,
  `unread` int(1) NOT NULL,
  `sent_item` int(1) NOT NULL,
  `message_type` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `user_id`, `sender_id`, `unread`, `sent_item`, `message_type`, `subject`, `message`, `date`) VALUES
(1, 2, 57, 0, 0, 'job_invitation', 'Cloud Strife invites you to be part of their business.', '<p>Hey terra! I want to hire you. call me 0909 5452458.</p>', '2019-01-10 22:33:36'),
(2, 2, 57, 0, 1, 'job_invitation', 'You invited Terra Brandford to your business.', '<p>Hey terra! I want to hire you. call me 0909 5452458.</p>', '2019-01-10 22:33:36'),
(3, 1, 2, 0, 0, 'job_application', 'Terra Brandford wants to be part of your company.', '\n                            <div class=\"intro2\"><a href=\"javascript:\" onclick=\"javascript: viewCandidate(2)\">Terra Brandford</a>  \n                            want\'s to be part of your company (The New Boston) with a job title of We Are Hiring.</div>\n                            <div class=\"link\"><span onclick=\"javascript: viewCandidate(2)\">View Terra Brandford Information</span></div>\n                        \n                                <div class=\"c_head\">Terra Brandford has a message.</div>\n                                <div class=\"c_msg\">\n                                    <p>I wanna try this.</p>\n                                </div>\n                            ', '2019-01-10 22:38:41'),
(4, 1, 2, 0, 1, 'job_application', 'Your application in The New Boston.', '\n                            <div class=\"intro2\">You\'ve submitted an application to The New Boston</div>\n                        \n                                <div class=\"c_msg\">\n                                    <p>I wanna try this.</p>\n                                </div>\n                            ', '2019-01-10 22:38:41');

-- --------------------------------------------------------

--
-- Table structure for table `phinxlog`
--

CREATE TABLE `phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phinxlog`
--

INSERT INTO `phinxlog` (`version`, `migration_name`, `start_time`, `end_time`, `breakpoint`) VALUES
(20180226160554, 'CreateImgTmp', '2018-02-26 09:54:23', '2018-02-26 09:54:24', 0),
(20180226161011, 'CreateJobapplications', '2018-02-26 09:54:24', '2018-02-26 09:54:24', 0),
(20180226163030, 'CreateProfiles', '2018-02-26 09:54:24', '2018-02-26 09:54:25', 0),
(20180226172206, 'CreateResumes', '2018-02-26 09:54:25', '2018-02-26 09:54:25', 0),
(20180226172632, 'CreateUsers', '2018-02-26 09:54:25', '2018-02-26 09:54:25', 0),
(20180226210811, 'AddProfilesToProfiles', '2018-02-26 13:11:23', '2018-02-26 13:11:23', 0),
(20180227211047, 'CreateJobheaderimgTmp', '2018-02-27 13:15:51', '2018-02-27 13:15:51', 0),
(20180301105733, 'CreateCompanylogoTmp', '2018-03-01 03:02:25', '2018-03-01 03:02:25', 0),
(20180303145107, 'CreateJobposts', '2018-03-03 07:12:49', '2018-03-03 07:12:49', 0),
(20180309000835, 'AddColumnToJobposts', '2018-03-08 16:12:07', '2018-03-08 16:12:08', 0),
(20180329072310, 'AddColumnToResumes', '2018-03-29 07:25:56', '2018-03-29 07:25:57', 0),
(20180407012414, 'CreateMessages', '2018-04-07 01:35:46', '2018-04-07 01:35:46', 0),
(20180408201022, 'CreateUserLogMsg', '2018-04-08 20:22:50', '2018-04-08 20:22:51', 0),
(20180413223913, 'CreateIpLogs', '2018-04-14 01:05:07', '2018-04-14 01:05:07', 0),
(20180422123158, 'AddFbcolumsToUsers', '2018-04-22 12:35:18', '2018-04-22 12:35:20', 0),
(20180429031152, 'AddPasswordrecoveryToUsers', '2018-04-29 03:13:12', '2018-04-29 03:13:13', 0),
(20180502075358, 'RemoveSecurityFromUsers', '2018-05-02 07:56:56', '2018-05-02 07:56:57', 0),
(20180518040712, 'AddSuspendToUsers', '2018-05-18 04:11:39', '2018-05-18 04:11:40', 0),
(20181112105508, 'CreateAnnouncements', '2018-11-12 11:01:51', '2018-11-12 11:01:52', 0),
(20181114132128, 'AddNameToAnnouncements', '2018-11-14 13:24:58', '2018-11-14 13:24:59', 0),
(20181117053446, 'AddHitsToIpLogs', '2018-11-17 05:38:00', '2018-11-17 05:38:01', 0),
(20181119125441, 'CreateSettings', '2018-11-19 12:57:55', '2018-11-19 12:57:56', 0),
(20181120093238, 'CreateLogs', '2018-11-20 09:40:40', '2018-11-20 09:40:41', 0),
(20181120113829, 'AddDateToLogs', '2018-11-20 11:39:41', '2018-11-20 11:39:42', 0),
(20181125100936, 'CreateMailinglists', '2018-11-25 10:12:37', '2018-11-25 10:12:38', 0),
(20181125114219, 'CreateMailinglists', '2018-11-25 11:42:33', '2018-11-25 11:42:33', 0),
(20181127082152, 'AddGoolemapToProfiles', '2018-11-27 10:24:37', '2018-11-27 10:24:39', 0),
(20181127102718, 'AddGooglemapToJobposts', '2018-11-27 10:29:16', '2018-11-27 10:29:16', 0),
(20181222140947, 'CreateSubscribers', '2018-12-22 14:13:53', '2018-12-22 14:13:54', 0);

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` int(11) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `job_title` varchar(255) DEFAULT NULL,
  `skills` text DEFAULT NULL,
  `desire_salary` text DEFAULT NULL,
  `education` varchar(255) DEFAULT NULL,
  `experience` text DEFAULT NULL,
  `employment_status` varchar(255) DEFAULT NULL,
  `business_name` varchar(255) DEFAULT NULL,
  `business_description` text DEFAULT NULL,
  `business_email` varchar(255) DEFAULT NULL,
  `business_contact_number` varchar(255) DEFAULT NULL,
  `business_website` varchar(255) DEFAULT NULL,
  `business_address` text DEFAULT NULL,
  `business_state` varchar(255) DEFAULT NULL,
  `business_country` varchar(255) DEFAULT NULL,
  `skype_id` varchar(255) DEFAULT NULL,
  `job_subscribe` bigint(20) DEFAULT NULL,
  `profile_picture` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `user_id`, `nickname`, `job_title`, `skills`, `desire_salary`, `education`, `experience`, `employment_status`, `business_name`, `business_description`, `business_email`, `business_contact_number`, `business_website`, `business_address`, `business_state`, `business_country`, `skype_id`, `job_subscribe`, `profile_picture`) VALUES
(1, 1, 'Gervic', '', '', '', '', '', NULL, 'The Freelancer', 'No Description yet.', 'gervic@gmx.com', '09267294114', 'http://onegerv.cf', 'No. 31 18th St. Pacita Complex San Pedro Laguna', 'Calabarzon', 'PH', NULL, NULL, '120190104211216DgDBUzUpbGxAC06H.jpg'),
(2, 2, 'Terra', 'Web Designer', '[\"I can create good web pages designs in adobe photoshop.\",\"Good in HTML and CSS.\"]', 'Php 45000 - Php 50000,Morethan Php 50000', 'Highschool Graduate', '4 Years Web Development in myTextmate,2 Years in HTML CSS at home', 'Unemployed', '', '', '', '', '', '', '', '', 'gervic23', NULL, '220180329135757czgGAjq7P1TQHrQq.jpg'),
(3, 3, 'Lightning', '', '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '320180326203529EmJmajJCJZRZ6kCm.jpg'),
(4, 4, 'Serah', 'Web Developer', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '420180616165212ojH8ZQ2Pq9CWLw8a.jpg'),
(6, 6, 'Vanille', 'Web Designer', '[\"I can design web pages via adobe photoshop.\",\"I can make and design via HTML and CSS\",\"I can make responsive web pages.\"]', 'Php 25000 - Php 30000,Morethan Php 50000,Php 40000 - Php 45000', 'College/Bachelor Degree', '3 Years Web Designer,4 Years HTML CSS,4 Years Adobe Photoshop', 'Unemployed', '', '', '', '', '', '', '', '', 'vanile_oerba', NULL, '6201804031509527dGaTmiAyfqUNKHy.jpg'),
(7, 7, 'Yuffie', 'Graphics Designer', '[\"Skill 1\",\"Skill 2\",\"Skill 3\"]', 'Php 15000 - Php 20000,Php 40000 - Php 45000', 'Highschool Graduate', '5 Years Adobe Photoshop, 5 Years Adobe Illustrator', 'Unemployed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'kisaragi_yufi', NULL, '720180403152308VdBCr6lvP4Yzs0Xt.jpg'),
(8, 8, 'Tieefuh', 'Web Developer', '[\"Skillful in HTML and CSS.\",\"Skillful to design web pages in adobe photoshop.\",\"Moderete skills in PHP and MySQL.\",\"Creating Wordpress Themes.\"]', 'Morethan Php 50000,Php 45000 - Php 50000', 'Post Graduate Degree', '8 Years HTML CSS,8 Years PHP MYSQL,6 Years Codeigniter,4 Years CakePHP and Laravel', 'Unemployed', '', '', '', '', '', '', '', '', 'tifa_heaven', NULL, '82018040700013259OwlgMcP22XnW0Q.jpg'),
(9, 9, 'Aeris', 'Nurse', '[\"Skill 1\",\"Skill 2\",\"Skill 3\"]', 'Morethan Php 50000', 'College/Bachelor Degree', '3 Years Service in some Hospitals,1 Year Caregiver', 'Unemployed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'aeris23', 1, '920180403154453Cp1G6timAxRZ9CGy.jpg'),
(14, 35, 'Ruby', 'Web Developer', '[\"I got 4 years knowledge in HTML5, CSS.\",\"I got 3 years knowledge in PHP and MySQL.\",\"I got knowledge in Javascript Frameworks like jQuery and AngularJS\",\"I got knowledge in PHP Frameworks like Codeigniter, Laravel and Cakephp\"]', 'Php 35000 - Php 40000,Php 40000 - Php 45000', 'Highschool Graduate', '4 Years Html and Css,4 Years Javascript and Php,4 Years Adobe Photoshop', 'Unemployed', '', '', '', '', '', '', '', '', 'rwby_23', 1, '3520180617074455YcTew9muNgU1dEEg.jpg'),
(15, 5, 'Yuel', 'Call Center Agent', '[\"Can speak fluent in english\",\"Can speak American and British Accent\",\"Computer Typing Speed 150 words per minute\"]', 'Php 45000 - Php 50000,Morethan Php 50000', 'Highschool Graduate', '4 years in Teleperformance as Customer Service Representative,3 Years in Teletech as Technical Support Representative', 'Unemployed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'yuel@localhost.com', 1, '520181211010813SnXT0s3NAkSQgXR7.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `resumes`
--

CREATE TABLE `resumes` (
  `id` int(11) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `orig_filename` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `resumes`
--

INSERT INTO `resumes` (`id`, `user_id`, `filename`, `date`, `orig_filename`) VALUES
(8, 2, '220180330195829rusQFlNMUDimgvgy.docx', '2018-03-30 19:58:29', 'victorresume2.docx'),
(10, 6, '6201804031510370AfP7wza5DVni9bU.docx', '2018-04-03 15:10:37', 'US Visa.docx'),
(11, 7, '720180403152336dCxC9g5Oesi6jwWs.docx', '2018-04-03 15:23:36', 'pnr.docx'),
(12, 8, '820180403152831lpBV83J8BeSET2Jt.docx', '2018-04-03 15:28:31', 'MAXTELCOM.docx'),
(13, 9, '920180403154514AXpGr8pilHn60jV7.rtf', '2018-04-03 15:45:14', 'visual composer.rtf'),
(19, 5, '520181110115454LiMj8ICiF56uiRyg.docx', '2018-11-10 11:54:54', 'victorresume2.docx');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `value`) VALUES
(1, 'registration_availability', '1');

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE `subscribers` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `birthday` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `region` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `contact_number` varchar(255) NOT NULL,
  `confirmation_code` varchar(255) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `current_ip` varchar(255) NOT NULL,
  `registered_ip` varchar(255) NOT NULL,
  `last_login` datetime NOT NULL,
  `fb_user` int(2) DEFAULT NULL,
  `fb_complete_reg` int(2) DEFAULT NULL,
  `password_recovery_code` varchar(255) DEFAULT NULL,
  `employer_review` int(2) DEFAULT NULL,
  `suspend` int(2) NOT NULL,
  `suspend_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `role`, `status`, `firstname`, `lastname`, `gender`, `birthday`, `address`, `region`, `country`, `contact_number`, `confirmation_code`, `date_created`, `current_ip`, `registered_ip`, `last_login`, `fb_user`, `fb_complete_reg`, `password_recovery_code`, `employer_review`, `suspend`, `suspend_time`) VALUES
(1, 'victor@localhost.com', '$2y$10$abrD0FQ2oWNJiRzlNO9YKeT6ANznYyUaKjByfSF/CFugO1lB8rlym', 'Employer', 'Inactive', 'Victor', 'Caviteno', 'Male', 'August/15/', 'No. 31 18th St. Pacita Complex San Pedro Laguna', 'Calabarzon', 'PH', '099265425', NULL, '2018-02-26 21:02:13', '127.0.0.1', '127.0.0.1', '2019-09-01 01:04:30', NULL, NULL, '', NULL, 0, NULL),
(2, 'terra@localhost.com', '$2y$10$jonzamPjn6e3EL662KqLZOSktt.o0dJWffN9dEWsUXpNLED3Tlc3i', 'Applicant', 'Active', 'Terra', 'Brandford', 'Female', 'August/20/1996', '#313 18 st. Pacita Complex, San Pedro Laguna.', 'Calabarzon', 'PH', '09211235478', NULL, '2018-03-26 20:25:07', '127.0.0.1', '127.0.0.1', '2019-01-10 22:34:59', NULL, NULL, NULL, NULL, 0, NULL),
(3, 'lightning@localhost.com', '$2y$10$DzxtkrTP.nhEoVVWOsNovuLip3/0u0Uf1dw0zIIQqQflK8AZOPA0G', 'Applicant', 'Active', 'Claire2', 'Farron', 'Female', 'November/28/1991', '#214 18 St. Pacita Complex, San Pedro Laguna.', 'Calabarzon', 'PH', '0921542321', NULL, '2018-03-26 20:34:53', '127.0.0.1', '127.0.0.1', '2018-03-26 20:34:53', NULL, NULL, NULL, NULL, 0, NULL),
(4, 'serah@localhost.com', '$2y$10$kT7lA5BBuJBnEpno.zwLSuqJIKXBdF5zUjHuKqMN5r3FowiSY9V7i', 'Applicant', 'Active', 'Serah', 'Farron', 'Female', 'September/8/1997', '#123 18. st. Pacita Complex San Pedro Laguna.', 'Calabarzon', 'PH', '9267294114', NULL, '2018-03-26 20:37:59', '127.0.0.1', '127.0.0.1', '2018-03-26 20:37:59', NULL, NULL, NULL, NULL, 0, NULL),
(5, 'yuel@localhost.com', '$2y$10$UuHjRQuUMMd8wUGzy3EsuOZrnnAKSTEMgcoB.3t4MCylqWIcMuWOy', 'Applicant', 'Active', 'Yuel', 'Paddra Psu', 'Female', 'February/8/1998', '#323 18th st. Pacita Complex San Pedro Laguna.', 'Calabarzon', 'PH', '09095652145', NULL, '2018-04-03 14:01:34', '127.0.0.1', '127.0.0.1', '2019-09-01 01:00:28', NULL, NULL, 'vXU9IeLWOGREWmwr', NULL, 0, NULL),
(6, 'vanille@localhost.com', '$2y$10$vrklgI07df474rJIDyO0jexEOTGXM2mKoBvPRDUSajoC67WZ2qqkK', 'Applicant', 'Active', 'Dia Vanille', 'Oerba', 'Female', 'October/28/1997', '#419 18 St. Pacita Complex San Pedro Laguna.', 'Calabarzon', 'PH', '09325456214', NULL, '2018-04-03 15:07:43', '127.0.0.1', '127.0.0.1', '2018-04-03 15:07:43', NULL, NULL, NULL, NULL, 0, NULL),
(7, 'yuffie@localhost.com', '$2y$10$Fu.xfxX1l46TQTG8dwDxTeKdf.QNjFPNfWP8caLzZS9rmrpefIBz2', 'Applicant', 'Active', 'Yuffie', 'Kisaragi', 'Female', 'September/12/2000', '#758 18 St. Pacita Complex San Pedro Laguna', 'Calabarzon', 'PH', '09542565214', NULL, '2018-04-03 15:21:14', '127.0.0.1', '127.0.0.1', '2018-04-03 15:21:14', NULL, NULL, NULL, NULL, 0, NULL),
(8, 'tifa@localhost.com', '$2y$10$b0KVjIwUyLPDseWb5YIMBe/qguSRofB6rCqxORaLqVH5wKvUQtH8q', 'Applicant', 'Active', 'Tifa', 'Lockhart', 'Female', 'July/13/1994', '#456 18th St. Pacita Complex San Pedro Laguna', 'Calabarzon', 'PH', '09095638471', NULL, '2018-04-03 15:25:48', '127.0.0.1', '127.0.0.1', '2018-06-05 06:24:11', NULL, NULL, NULL, NULL, 0, NULL),
(9, 'aeris@localhost.com', '$2y$10$KHm.n6XE6OR.9Ww1IvqcyOSAwoUppLJOahK9Z.YbudtszmcoblEQa', 'Applicant', 'Active', 'Aeris', 'Gainsborough', 'Female', 'October/12/1992', '#598 18th St. Pacita Complex San Pedro Laguna', 'Calabarzon', 'PH', '09568214583', NULL, '2018-04-03 15:42:04', '127.0.0.1', '127.0.0.1', '2018-04-03 15:42:04', NULL, NULL, NULL, NULL, 0, NULL),
(10, 'gervic@localhost.com', '$2y$10$kl95vWyjeo.3.xP91JanYuKNbKnOTCw7H71U0Mu/0IQjxWDH0zO6e', 'Employer', 'Inactive', 'Victor', 'Caviteno', 'Male', 'August/15/1983', '#31 18th St. Pacita Complex San Pedro Laguna', 'Calabarzon', 'PH', '0909854525', NULL, '2018-04-11 08:51:43', '127.0.0.1', '127.0.0.1', '2018-12-22 04:43:03', NULL, NULL, NULL, NULL, 0, NULL),
(35, 'rwby@localhost.com', '$2y$10$LIbdw6aImWJPWXHigqQ.wOpVDTX2XbLuP6Gem1g7moz.v2LSZLah2', 'Applicant', 'Active', 'Ruby', 'Rose', 'Female', 'April/11/1993', '#313 18th. Street, Pacita Complex San Pedro Laguna.', 'Calabarzon', 'PH', '09568471254', '', '2018-04-30 21:46:34', '127.0.0.1', '127.0.0.1', '2018-12-27 16:22:51', 0, 1, NULL, NULL, 0, NULL),
(36, 'admin@localhost.com', '$2y$10$GvO1OIfkOATXr1BLEUJzqOKstzXRvhewsfh9qotdhnIUw4nIEZkhG', 'Super Admin', 'Active', 'Super Admin', 'Admin', 'Male', 'August/15/1900', '#31 18th street. Pacita Complex San Pedro Laguna.', 'Calabarzon', 'PH', '092323232122', '', '2018-05-04 17:41:52', '127.0.0.1', '127.0.0.1', '2019-09-01 01:02:59', 0, 1, NULL, NULL, 0, NULL),
(57, 'cloud@localhost.com', '$2y$10$QrCAQqU58m6.ZITpzQqJ.OD/0sr1a/51YB0QP0vAgnp5juOn.Z4Pu', 'Employer', 'Inactive', 'Cloud', 'Strife', 'Male', 'May/18/1989', '#56 William Shaw St. Caloocan City.', 'Metro Manila', 'PH', '09095654125', '', '2019-01-10 22:24:14', '127.0.0.1', '127.0.0.1', '2019-01-10 22:24:14', 0, 1, NULL, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_log_msg`
--

CREATE TABLE `user_log_msg` (
  `id` int(11) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `job_app` int(11) DEFAULT NULL,
  `job_inv` int(11) DEFAULT NULL,
  `microtime` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companylogo_tmp`
--
ALTER TABLE `companylogo_tmp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `img_tmp`
--
ALTER TABLE `img_tmp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ip_logs`
--
ALTER TABLE `ip_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobapplications`
--
ALTER TABLE `jobapplications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobheaderimg_tmp`
--
ALTER TABLE `jobheaderimg_tmp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobposts`
--
ALTER TABLE `jobposts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mailinglists`
--
ALTER TABLE `mailinglists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phinxlog`
--
ALTER TABLE `phinxlog`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `resumes`
--
ALTER TABLE `resumes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_log_msg`
--
ALTER TABLE `user_log_msg`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcements`
--
ALTER TABLE `announcements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `companylogo_tmp`
--
ALTER TABLE `companylogo_tmp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `img_tmp`
--
ALTER TABLE `img_tmp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ip_logs`
--
ALTER TABLE `ip_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `jobapplications`
--
ALTER TABLE `jobapplications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobheaderimg_tmp`
--
ALTER TABLE `jobheaderimg_tmp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobposts`
--
ALTER TABLE `jobposts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=329;

--
-- AUTO_INCREMENT for table `mailinglists`
--
ALTER TABLE `mailinglists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `resumes`
--
ALTER TABLE `resumes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `user_log_msg`
--
ALTER TABLE `user_log_msg`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
