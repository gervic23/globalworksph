
basic_info_size();

$(window).resize(function() {
    basic_info_size();
});

function basic_info_size() {
    var height = $(window).height();
    var new_height = height - 50;
    var basic_info = $(".sm-basic-info").height();

    if (basic_info < height) {
        $(".sm-basic-info").css({
            "height": new_height + "px"
        });
    }
}