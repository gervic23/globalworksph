//Set .search-home equal to the window screen size.
setHomeBgSize();

homeScreen();

//Set (When window change size) .search-home equal to the screen size.
$(window).resize(function() {
    setHomeBgSize();
});

//Animate .home-jobs-categories ul li
$(".home-jobs-categories ul li").scrollAnimate({"animation": "flipInX"});

$(".home-mid-btns").scrollAnimate({"animation": "bounceInUp"});

$(".home-who h2").scrollAnimate({"animation": "bounceInLeft"});

$(".home-who .content").scrollAnimate({"animation": "lightSpeedIn"});

$(".home-blog-news h2").scrollAnimate({"animation": "bounceInLeft"});

$(".home-blog-news .content").scrollAnimate({"animation": "lightSpeedIn"});

//Set .search-home equal to the screen size.
function setHomeBgSize() {
    var height = $(window).height();
    var new_height = height - 15;
    
    $(".search-home").css({
        "height": new_height + "px"
    });
}

//Set $(".search-home .search-background") equal to $(".search-home .search-content") height.
function homeScreen() {
    var height = $(".search-home .search-content").innerHeight();
    
    $(".search-home .search-background").css({
        "height": height + "px"
    });
}