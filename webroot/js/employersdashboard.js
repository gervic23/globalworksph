dashboardHeight();

$(window).on("change resize scroll click input", function() {
    dashboardHeight();
});

// $(".employers-dashboard .right").on("change scroll click input", function() {
//     dashboardHeight();
// });

function dashboardHeight() {
    var height = $(window).height();
    var new_height = height - 31;
    var new_height2 = height - 33;
    var left = $(".employers-dashboard .left").height();
    var right = $(".employers-dashboard .right").height() + 19;
    
    if (right < height) {
        if (left < height) {
            $(".employers-dashboard").css({
                "height": new_height + "px"
            });
            
            $(".employers-dashboard .left").css({
                "height": new_height2 + "px"
            });
        } else {
            $(".employers-dashboard").css({
                "height": left + "px"
            });
        }
    } else if (right > height) {
        $(".employers-dashboard").css({
            "height": right + "px"
        });
        
        $(".employers-dashboard .left").css({
            "height": right - 1 + "px"
        });
    }
}