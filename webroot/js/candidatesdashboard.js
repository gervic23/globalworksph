
dashboardHeight();

$(window).resize(function() {
    dashboardHeight();
});

$(".candidates-dashboard .right").on("change scroll click input", function() {
    dashboardHeight();
});

function dashboardHeight() {
    var height = $(window).height();
    var new_height = height - 50;
    var new_height2 = height - 33;
    var left = $(".candidates-dashboard  .left").height();
    var right = $(".candidates-dashboard .right").height();
    
    if (right < new_height) {
        if (left < height) {
            $(".candidates-dashboard").css({
                "height": new_height + "px"
            });

            $(".candidates-dashboard .left").css({
                "height": new_height2 + "px"
            });      
        } else {
            $(".candidates-dashboard").css({
                "height": left + "px"
            });
        }
    } else if (right > new_height) {
        $(".candidates-dashboard").css({
            "height": right + "px"
        });

        $(".candidates-dashboard .left").css({
            "height": right + 18 + "px"
        });
    }
}