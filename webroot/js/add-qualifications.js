
var qualifications = [];

if (qualifications.length == 0) {
    qlf_set_empty_message();
}

$("#add-qlf").on("click", function() {
    add_qlf();
});

function add_qlf(text = "") {
    qlf_remove_empty_message();
    
    var qlf = document.createElement("div");
    qlf.classList.add("qlf-element");

    var input = document.createElement("input");
    input.setAttribute("name", "qualifications[]");
    input.setAttribute("type", "text");
    input.classList.add("qlf-entry");
    input.setAttribute("value", text);
    input.setAttribute("autocomplete", "off");
    input.setAttribute("placeholder", "Write Qualification Here.");

    var del_container = document.createElement('div');
    del_container.classList.add("qlf-del-btn-container");

    var del_btn = document.createElement('span');
    del_btn.textContent = "Delete";
    del_btn.addEventListener("click", function() {
        delete_qlf(qualifications.indexOf(qlf));
    });

    qlf.appendChild(input);
    del_container.appendChild(del_btn);
    qlf.appendChild(del_container);

    $(".qlf_contents").append(qlf);

    qualifications.push(qlf);
}

function delete_qlf(index) {
    var qlf = qualifications[index];
    qualifications.splice(index, 1);
    document.getElementsByClassName("qlf_contents")[0].removeChild(qlf);
    qlf_restore_empty_message();
}

function qlf_set_empty_message() {
    var empty = document.createElement("div");
    empty.classList.add("qlf-empty");
    empty.setAttribute("id", "qlf-empty");
    empty.textContent = "Qualifications like skills, Degree Holder and etc.";
    
    $(".qlf_contents").append(empty);
}

function qlf_remove_empty_message() {
    var empty = document.getElementById("qlf-empty");
    
    if (empty) {
        empty.parentNode.removeChild(empty);   
    }
}

function qlf_restore_empty_message() {
    if (qualifications.length == 0) {
        qlf_set_empty_message();
    }
}
