/*
*	Copyright (c) 2018 jQuery Drag Drag Upload. Created by Victor Caviteno.
*	http://onegerv.cf
*	GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.
*	Version 2.0;
*/

// (function($) {
// 	$.fn.dragdropupload = function(upload_path, params, success) {
		
// 		//Main Element Container value.
// 		var main_div = $(this);

// 		//File Value to upload.
// 		var file = '';

// 		/*
// 		*	Create Html Elements.
// 		*/

// 		//Main Container
// 		var container = document.createElement("div");
// 		container.classList.add("ddu_container");

// 		//Div Left.
// 		var left = document.createElement("div");
// 		left.classList.add("ddu_left");

// 		//Div Right.
// 		var right = document.createElement("div");
// 		right.classList.add("ddu_right");

// 		//Photo Container
// 		var photo_container = document.createElement("div");
// 		photo_container.classList.add("ddu_photo_container");
// 		photo_container.innerHTML = '<img src="/img/users/nopropic.png" />';
		
// 		//Option Container
// 		var opt_container = document.createElement("div");
// 		opt_container.classList.add("ddu_opt_container");

// 		//Label Container
// 		var label = document.createElement("label");
// 		label.classList.add("ddu_label");
// 		label.setAttribute("for", "ddu_upload");
// 		label.innerHTML = "Drag file here or click here to browse";

// 		//Input File.
// 		var input_file = document.createElement("input");
// 		input_file.setAttribute("type", "file");
// 		input_file.setAttribute("id", "ddu_upload");
// 		input_file.setAttribute("name", "ddu_upload");
// 		input_file.classList.add("ddu_input_file");

// 		//Filename div container.
// 		var file_div = document.createElement("div");
// 		file_div.setAttribute("id", "ddu_filename");
// 		file_div.classList.add("du_filename");

// 		//Flash Error div container.
// 		var flash_error = document.createElement("div");
// 		flash_error.setAttribute("ddu_error");
// 		flash_error.classList.add("ddu_error");

// 		//Upload Button.
// 		var upload = document.createElement("input");
// 		upload.setAttribute("id", "ddu_upload_submit");
// 		upload.setAttribute("value", "Upload");
// 		upload.setAttribute("type", "button");
// 		upload.classList.add("ddu_button");
		
// 		//Cancel Button.
// 		var cancel = document.createElement("input");
// 		cancel.setAttribute("type", "button");
// 		cancel.setAttribute("id", "ddu_cancel");
// 		cancel.setAttribute("value", "Cancel");
// 		cancel.classList.add("ddu_cancel");

// 		//Clear Div. To clear floated elements.
// 		var clear = document.createElement("div");
// 		clear.classList.add("ddu_clear");

// 		//Div Progress Bar.
// 		var progress = document.createElement("div");
// 		progress.classList.add("ddu_progress");
// 		progress.setAttribute("id", "ddu_progress");
// 		progress.innerHTML = "&nbsp;";

// 		//Append Elements to Main Container.
// 		left.append(photo_container);
// 		right.append(label);
// 		right.append(file_div);
// 		right.append(flash_error);
// 		right.append(input_file);
// 		right.append(upload);
// 		right.append(cancel);
// 		clear.append(progress);

// 		container.append(left);
// 		container.append(right);
// 		container.append(clear);
// 		main_div.append(container);

// 		/*
// 		*	Style HTML Elements.
// 		*/
// 		var container_style = ".ddu_container {border: 1px solid #ccc; padding: 10px; width: 75%; margin: 0 auto; height: 300px;} ";
// 		var left_style = ".ddu_left {width: 25%; float: left; height: 300px;} ";
// 		var right_style = ".ddu_right {width: 74.5%; float: left; height: 300px; text-align: center;} ";
// 		var clear_style = ".ddu_clear {clear: both; padding: 10px; position: relative; top: -40px;} ";
// 		var progres_style = ".ddu_progress {border: 1px solid #ccc; background-color: #ccc; width: 50%;} ";
// 		var photo_container = ".ddu_photo_container {border: 1px solid #ccc; width: 80%; position: relative; top: 15%; height: 150px; margin: 0 auto;} .ddu_photo_container img {width: 100%; height: 100%} ";
// 		var label_style = ".ddu_label {display: block; font-size: 25px; position: relative; top: 35%; display: none;} ";
// 		var input_file_style = ".ddu_input_file {position: absolute; visibility: hidden;} ";
// 		var button_styles = ".ddu_upload_submit, .ddu_cancel {} ";
// 		var ddu_style = container_style + left_style + right_style + clear_style + progres_style + photo_container + label_style + input_file_style + button_styles;

// 		$("head").append("<style>" + ddu_style + "</style>");
// 	}
// })(jQuery);

(function($) {
	$.fn.dragdropupload = function(upload_path, params, success) {
		var main_div = $(this);
		var file = '';

		//Default params properties.
		var border_size = "1px";
		var border_style = "dashed"
		var border_color = '#ccc';
		var border_radius = "10px";
		var background_color = "#fff";
		var height = "400px";
		var width = "80%";
		var dragover_border_color = "red";
		var dragover_background_color = "#f9e3e2";
		var dragover_font_color = "#fff";
		var error_font_color = "red";
		var error_font_size = "16px";
		var error_border_color = "red";
		var error_background = "#f9e3e2";
		var success_font_size = "20px";
		var success_font_color = "#7c7979";
		var success_border_color = "green";
		var success_background_color = "#d9f8d2";
		var label = "Drag Files Here or Click Here to Browse File.";
		var label_font_size = "20px";
		var label_font_color = "#000";
		var upload_success_message = 'Upload Complete.';

		//If params are set apply user's params as attributes to the elements.
		if (params) {
			if (params['border_size']) {
				border_size = params['border_size'];
			}

			if (params['border_style']) {
				border_style = params['border_style'];
			}

			if (params['border_color']) {
				border_color = params['border_color'];
			}

			if (params['background_color']) {
				background_color = params['background_color'];
			}

			if (params['height']) {
				height = params['height'];
			}

			if (params['width']) {
				width = params['width'];
			}

			if (params['dragover_border_color']) {
				dragover_border_color = params['dragover_border_color'];
			}

			if (params['dragover_background_color']) {
				dragover_background_color = params['dragover_background_color'];
			}

			if (params['dragover_font_color']) {
				dragover_font_color = params['dragover_font_color'];
			}

			if (params['error_font_color']) {
				error_font_color = params['error_font_color'];
			}

			if (params['error_border_color']) {
				error_border_color = params['error_border_color'];
			}

			if (params['error_font_size']) {
				error_font_size = params['error_font_size'];
			}

			if (params['error_background']) {
				error_background = params['error_background'];
			}

			if (params['success_font_size']) {
				success_font_size = params['success_font_size'];
			}

			if (params['success_font_color']) {
				success_font_color = params['success_font_color'];
			}

			if (params['success_border_color']) {
				success_border_color = params['success_border_color'];
			}

			if (params['success_background_color']) {
				success_background_color = params['success_background_color'];
			}

			if (params['label']) {
				label = params['label'];
			}

			if (params['label_font_size']) {
				label_font_size = params['label_font_size'];
			}

			if (params['label_font_color']) {
				label_font_color = params['label_font_color'];
			}

			if (params['upload_success_message']) {
				upload_success_message = params['upload_success_message'];
			}
		}

		/*
		*	Create Elements Variables.
		*/
		var file_input = '<input type="file" id="ddu-upload" class="ddu-upload" name="ddu-upload" />';
		var label_input = '<label for="ddu-upload" class="ddu-label"> ' + label + ' </label>';
		var default_page = '<div class="ddu-default-page"> ' + file_input + label_input + ' </div>';
		var error_page = '<div class="ddu-error">' + file_input + '<label for="ddu-upload" class="ddu-error-label"></label></div>';
		var upload_progress_page = '<div class="ddu-upload-progress"><progress class="ddu-progress-bar" value="0" max="100"></progress></div>';
		var success_page = '<div class="ddu-success">' + upload_success_message + '</div>';

		//Append Elements.
		$(this).append('<div class="ddu-main"> ' + default_page + error_page + upload_progress_page + success_page + ' </div>');

		//$(this) css.
 		$(this).css({
			"width": width,
			"margin": "0 auto"
		});

		//.ddu-upload css.
		$(".ddu-upload").css({
			"display": "none"
		});

		//.ddu-label.
		$(".ddu-label").css({
			"font-size": label_font_size,
			"cursor": "pointer",
			"color": label_font_color
		});

		//.ddu-label hover.
		$(".ddu-label").on("mouseover", function() {
			$(this).css({
				"color": "red"
			});
		}).on("mouseleave", function() {
			$(this).css({
				"color": "#363433"
			});
		});

		//.ddu-default-page css
		$(".ddu-default-page").css({
			"height": height,
			"line-height": height,
			"text-align": "center",
			"border": border_size + " " + border_style + " " + border_color,
			"background-color": background_color,
			"border-radius": border_radius
		});

		//.ddu-error css.
		$(".ddu-error").css({
			"display": "none",
			"border": border_size + " " + border_style + " " + error_border_color,
			"border-radius": border_radius,
			"height": height,
			"line-height": height,
			"background-color": error_background,
			"text-align": "center"
		});

		//.ddu-error-label css.
		$(".ddu-error-label").css({
			"color": error_font_color,
			"font-size": error_font_size,
			"cursor": "pointer"
		});

		//.ddu-upload-progress css.
		$(".ddu-upload-progress").css({
			"display": "none",
			"height": height,
			"line-height": height,
			"border": border_size + " " + border_style + " " + border_color,
			"background-color": background_color,
			"text-align": "center"
		});

		//.ddu-progress-bar style
		$(".ddu-progress-bar").css({
			"width": "98%",
			"padding": "10px"
		});

		//.ddu-success css.
		$(".ddu-success").css({
			"display": "none",
			"height": height,
			"line-height": height,
			"color": success_font_color,
			"font-size": success_font_size,
			"border": border_size + " " + border_style + " " + success_border_color,
			"background-color": success_background_color,
			"text-align": "center"
		});

		//.ddu-main when file drag dragleave and drop.
		$(".ddu-main").on("dragover", function() {
			$(".ddu-default-page").css({
				"border-color": dragover_border_color,
				"background-color": dragover_background_color
			});

			$(".ddu-label").css({
				"color": dragover_font_color
			});

			$(".ddu-error-label").css({
				"color": dragover_font_color
			});

			return false;
		}).on("dragleave", function() {
			$(".ddu-default-page").css({
				"border-color": border_color,
				"background-color": background_color,
			});

			$(".ddu-label").css({
				"font-size": label_font_size,
				"cursor": "pointer",
				"color": label_font_color
			});

			$(".ddu-error-label").css({
				"color": error_font_color,
				"font-size": error_font_size,
				"cursor": "pointer"
			});

			return false;
		}).on("drop", function(e) {
			e.preventDefault();

			//Restore default style.
			$(this).css({
				"border-color": border_color,
				"background-color": background_color
			});

			$(".ddu-label").css({
				"color": dragover_font_color
			});

			$(".ddu-error-label").css({
				"color": error_font_color
			});

			//Global file properties.
			file = e.originalEvent.dataTransfer.files[0];

			//Upload file.
			fileValidation();
		});

		//If file has been browsed.
		$(".ddu-upload").on("change", function() {

			//Global file properties.
			file = $(this).get(0).files[0];

			//Upload file.
			fileValidation();
		});

		function fileValidation() {
			if (file) {
				//If file has type erros set boolean.
				var types_error = false;

				//If file has max size error. set boolean.
				var max_size_error = false;

				//If file has min size error. set boolean.
				var min_size_error = false;

				//Name of the file.
				var filename = file['name'];

				//Size of the File.
				var size = parseFloat(file.size / 1024).toFixed(2);

				//Type of file.
				var type = file.type;

				//File Extension.
				var ext = filename.split(".").reverse()[0];

				//Limit filename string length.
				if (filename.length >= 20) {
					filename = filename.substring(0, 20) + "...." + ext;
				}

				//If user's set a validation rule.
				if (params && params['validation']) {

					//If Minumum file size validation is set.
					if (params['validation']['file_sizes'] && params['validation']['file_sizes']['min'] && params['validation']['file_sizes']['min']['size'] !== 'undefined' && params['validation']['file_sizes']['min']['size'] > 0) {
						
						//If file size is lower from the minimum file size rule.
						if (size < params['validation']['file_sizes']['min']['size']) {

							//Set Boolean.
							min_size_error = true;

							//If Minimum file size rule validation message is set or not.
							if (params['validation']['file_sizes']['min']['validation_message']) {
								$(".ddu-error-label").html(filename + " " + params['validation']['file_sizes']['min']['validation_message']);
							} else {
								$(".ddu-error-label").html(filename + " is too small. Click Here to Browse File.");
							}
						}
					}

					//If Maximum file size validation is set.
					if (params['validation']['file_sizes'] && params['validation']['file_sizes']['max'] && params['validation']['file_sizes']['max']['size'] && params['validation']['file_sizes']['max']['size'] > 0) {
						
						//If file size is higher from the maximum file size rule.
						if (size > params['validation']['file_sizes']['max']['size']) {

							//Set Boolean.
							max_size_error = true;

							//If Maximum file size rule validation message is set or not.
							if (params['validation']['file_sizes']['max']['validation_message']) {
								$(".ddu-error-label").html(filename + " " + params['validation']['file_sizes']['max']['validation_message']);
							} else {
								$(".ddu-error-label").html(filename + " is too large. Click Here to Browse File.");
							}
						}
					}
				}

				//If File Types Validation is set.
				if (params['validation']['file_types'] && params['validation']['file_types']['types']) {

					//If file type does not found in the file types rule.
					if ($.inArray(type, params['validation']['file_types']['types']) == -1) {

						//Set Boolean.
						types_error = true;

						//If File Types rule validation message is set or not.
						if (params['validation']['file_types']['validation_message']) {
							$(".ddu-error-label").html(filename + " " + params['validation']['file_types']['validation_message']);
						} else {
							$(".ddu-error-label").html(filename + " is not a valid file format.");
						}
					}
				}

				//If no validation errors. Proceed to next method.
				if (!types_error && !max_size_error && !min_size_error) {
					imageDimensionMinValidation();
				} else {
					//Hide default page.
					$(".ddu-default-page").hide();

					//Show error page.
					$(".ddu-error").css("display", "block");
				}
			}
		}

		//Method to validate Minimum Image Dimension.
		function imageDimensionMinValidation() {

			//If Minimum Image Dimension validation is set.
			if (params['validation']['image_dimension'] && params['validation']['image_dimension']['min'] && params['validation']['image_dimension']['min']['width'] && params['validation']['image_dimension']['min']['height']) {
				
				//Name of the file.
				var filename = file['name'];

				//File Extension.
				var ext = filename.split(".").reverse()[0];

				//Limit filename string length.
				if (filename.length >= 20) {
					filename = filename.substring(0, 20) + "...." + ext;
				}

				//File type.
				var type = file.type;

				//Allowed Image format to validate.
				var image_types = ["image/jpeg", "image/png", "image/gif", "image/bmp"];

				//If file is a image, file format matched image types.
				if ($.inArray(image_types, type) == -1 && type !== "") {

					//Initiate Image Class. 
					var image = new Image();

					//Source of Image.
					image.src = window.URL.createObjectURL(file);				
					image.onload = function() {

						//Image Width.
						var image_width = this.width;

						//Image Height.
						var image_height = this.height;

						//If image dimension is lower from the image dimension minimum rule.
						if (image_width < params['validation']['image_dimension']['min']['width'] && image_height < params['validation']['image_dimension']['min']['width']) {
							
							//If image dimension minimum validation rule has validation message set.
							if (params['validation']['image_dimension']['min']['validation_message']) {
								$(".ddu-error-label").html(filename + " " + params['validation']['image_dimension']['min']['validation_message']);
							} else {
								$(".ddu-error-label").html(filename + " dimensions are too small.");
							}

							//Hide default page.
							$(".ddu-default-page").hide();

							//Show error page.
							$(".ddu-error").css("display", "block");
						} else {
							imageDimensionMaxValidation();
						}
					}
				} else {
					imageDimensionMaxValidation();
				}
			} else {
				imageDimensionMaxValidation();
			}
		}

		//Method to validate MAximum Image Dimension.
		function imageDimensionMaxValidation() {

			//If Minimum Image Dimension validation is set.
			if (params['validation']['image_dimension'] && params['validation']['image_dimension']['max'] && params['validation']['image_dimension']['max']['width'] && params['validation']['image_dimension']['max']['height']) {

				//Name of the file.
				var filename = file['name'];

				//File Extension.
				var ext = filename.split(".").reverse()[0];

				//Limit filename string length.
				if (filename.length >= 20) {
					filename = filename.substring(0, 20) + "...." + ext;
				}

				//File type.
				var type = file.type;

				//Allowed Image format to validate.
				var image_types = ["image/jpeg", "image/png", "image/gif", "image/bmp"];

				//If file is a image, file format matched image types.
				if ($.inArray(image_types, type) == -1 && type !== "") {

					//Initiate Image Class.
					var image = new Image();

					//Source of Image.
					image.src = window.URL.createObjectURL(file);
					image.onload = function() {

						//Image Width.
						var image_width = this.width;

						//Image Height.
						var image_height = this.height;

						//If image dimension minimum validation rule has validation message set.
						if (image_width > params['validation']['image_dimension']['max']['width'] && image_height > params['validation']['image_dimension']['max']['width']) {

							//If image dimension minimum validation rule has validation message set.
							if (params['validation']['image_dimension']['max']['validation_message']) {
								$(".ddu-error-label").html(filename + " " + params['validation']['image_dimension']['max']['validation_message']);
							} else {
								$(".ddu-error-label").html(filename + " dimensions are too small.");
							}

							//Hide default page.
							$(".ddu-default-page").hide();

							//Show error page.
							$(".ddu-error").css("display", "block");
						} else {
							fileUpload();
						}
					}
				}
			} else {
				fileUpload();
			}
		}

		//Method to Upload file.
		function fileUpload() {
			if (file) {

				$(".ddu-default-page, .ddu-error").hide();

				//Initialize forData.
				var formData = new FormData();

				//Append file properties.
				formData.append("ddu-upload", file);

				//If post_data is set, append post_data.
				if (params['post_data']) {
					$.each(params['post_data'], function(key, val) {
						formData.append(key, val);
					});
				}

				//Ajax Upload Process.
				$.ajax({
					url: upload_path,
					type: "post",
					contentType: false,
					catch: false,
					processData: false,
					data: formData,
					xhr: function() {
						var xhr = $.ajaxSettings.xhr();
						xhr.upload.addEventListener("progress", function(prog) {
							var value = Math.round((prog.loaded / prog.total) * 100);

							$(".ddu-upload-progress").show();
							$(".ddu-progress-bar").attr("value", value);
						}, false);

						return xhr;
					},
					success: function(data) {
						if (typeof success === "function") {
							$(".ddu-upload-progress").hide();
							$(".ddu-success").show();
							success(data);
						}
					}
				});
			}
		}
	}
})(jQuery);