adminLayout();

var has_scrolled = false;

$(window).on("resize change", function() {
    adminLayout();
});

$(window).on("scroll", function() {
    if (!has_scrolled) {
        has_scrolled = true;
    }

    adminLayout();
});

var dropdown_menu_bool = false;

$(".dropdown").on("click", function() {
    if (!dropdown_menu_bool) {
        dropdown_menu_bool = true;
        $(this).css({
            "color": "#e68c8c"
        });
        
        $(".dropdown-menu").fadeIn(500);
    } else {
        dropdown_menu_bool = false;
        $(this).css({
            "color": "#fff"
        });
        $(".dropdown-menu").fadeOut(500);
    }
});

$(window).on("click", function(event) {
    if (!event.target.matches(".dropdown")) {
        dropdown_menu_bool = false;
        $(this).css({
            "color": "#fff"
        });
        $(".dropdown").css({
            "color": "#fff"
        });
        $(".dropdown-menu").fadeOut(500);
    }
});


function adminLayout() {
    var height = $(window).height();
    var new_height = height - 42;
    var aside = $(".main-aside").height();
    var section = $(".main-section").height() + 20;
    var body = $("body").height();
    
    if (height > aside && height > body) {
        $(".main-aside").css({
            "height": new_height + "px"
        });
    } else if (height < body) {
        $(".main-aside").css({
            "height": section + "px"
        });
    }
}