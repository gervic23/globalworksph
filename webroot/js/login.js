setLoginBgSize();
loginScreen();
scaleCaptcha();

$(window).resize(function() {
    setLoginBgSize();
    loginScreen();
    scaleCaptcha();
});

//Set .search-home equal to the screen size.
function setLoginBgSize() {
    var height = $(window).height();
    var form = $(".login-main .form").innerHeight() + 100;

    if (height < form) {
        $(".login-main").css({
            "height": + form + "px"
        });
    } else {
        $(".login-main").css({
            "height": + height + "px"
        });
    }
}

//Set $(".login-main .background") equal to $(".login-main .form") height.
function loginScreen() {
    var height = $(".login-main .form").innerHeight() - 20;
    
    $(".login-main .background").css({
        "height": height + "px"
    });
}

//Scaling Recaptcha.
function scaleCaptcha() {
    var recaptcha_width = 304;
    var login_main = $(".login-main .form").width();
    var i = 0;

    if (recaptcha_width > login_main) {
        captchaScale = login_main / recaptcha_width;

        $(".login-main .form .recaptcha").css({
            "position": "relative",
            "right": captchaScale * 2 + "px",
            "width": "100%",
            "overflow": "hidden"
        });

        // var left = login_main - 288;

        // $(".recaptcha").css({
        //     "transform": "scale("+captchaScale+")"
        // });
    } else {
        $(".login-main .form .recaptcha").css({
            "position": "initial",
            "width": "303px",
            "overflow": "none"
        });
    }
}