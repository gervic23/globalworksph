
//Disable scroll of element.
$(".register-main").css({
    "overflow-x": "hidden"
});

accountSetupPages();

//If window resize. Resize element.
$(window).resize(function() {
    accountSetupPages();
});

$(".register-main .form, .register-main h2, .register-main .progress").scrollAnimate({
    animation: "fadeInDown"
});


$("#email").on("click focus", function() {
    inputAnimate("email");
}).on("focusout", function() {
    focusoutAnimate("email");
}).on("keyup input", function() {
    $(this).css({
        "border": "0px",
        "border-bottom": "2px solid #ccc"
    });
    $(".register-main .form .input:nth-child(2) .error-message").hide();
});

$("#password").on("click focus", function() {
    inputAnimate("password");
}).on("focusout", function() {
    focusoutAnimate("password");
}).on("keyup input", function() {
    $(this).css({
        "border": "0px",
        "border-bottom": "2px solid #ccc"
    });
    $(".register-main .form .input:nth-child(3) .error-message").hide();
});

$("#rpassword").on("click focus", function() {
    inputAnimate("rpassword");
}).on("focusout", function() {
    focusoutAnimate("rpassword");
}).on("keyup input", function() {
    $(this).css({
        "border": "0px",
        "border-bottom": "2px solid #ccc"
    });
    $(".register-main .form .input:nth-child(4) .error-message").hide();
});

$("#firstname").on("click focus", function() {
    inputAnimate("firstname");
}).on("focusout",function() {
    focusoutAnimate("firstname");
}).on("keyup input", function() {
    $(this).css({
        "border": "0px",
        "border-bottom": "2px solid #ccc"
    });
    $(".register-main .form .input:nth-child(5) .error-message").hide();
});

$("#lastname").on("click focus", function() {
    inputAnimate("lastname");
}).on("focusout", function() {
    focusoutAnimate("lastname");
}).on("keyup input", function() {
    $(this).css({
        "border": "0px",
        "border-bottom": "2px solid #ccc"
    });
    $(".register-main .form .input:nth-child(6) .error-message").hide();
});

$("#region").on("click focus", function() {
    inputAnimate("region");
}).on("focusout", function() {
    focusoutAnimate("region");
}).on("keyup input", function() {
    $(this).css({
        "border": "0px",
        "border-bottom": "2px solid #ccc"
    });
    $(".register-main .form .input:nth-child(13) .error-message").hide();
});

$("#country").on("click focus", function() {
    inputAnimate("country");
}).on("focus", function() {
    focusoutAnimate("country");
});

$("#contact_number").on("click focus", function() {
    inputAnimate("contact_number");
}).on("focusout",function() {
    focusoutAnimate("contact_number");
}).on("keyup input", function() {
    $(this).css({
        "border": "0px",
        "border-bottom": "2px solid #ccc"
    });
    $(".register-main .form .input:nth-child(15) .error-message").hide();
});

$("#address").on("click focus", function() {
    $(this).animate({
        height: "100px"
    }, 500, function() {
        $(this).css({
            "border": "2px solid #ccc"
        });
    });
}).on("focusout", function() {
    $(this).animate({
        height: "20px"
    }, 500, function() {
        $(this).css({
            "border": 0,
            "border-bottom": "2px solid #ccc"
        });
    });
}).on("keyup input", function() {
    $(this).css({
        "border": "0px",
        "border-bottom": "2px solid #ccc"
    });
    $(".register-main .form .input:nth-child(12) .error-message").hide();
});

/*
*   Making a Web Browser compatibility.
*/
//If Operating System is Windows and Web Browser is Mozilla FireFox.
if (detectOs.isWindows() && $.browser.mozilla) {
    $("head").append("<style> .setprofile .progress .complete1::after { border-bottom: 15px solid transparent; } .setprofile .progress .complete2::after { border-bottom: 15px solid transparent; } .setprofile .progress .complete3::after { border-bottom: 15px solid transparent; } </style>");
}

//If Operating System is Linux and Web Browser is Google Chrome.
if (detectOs.isLinux() && $.browser.chrome) {
    $("head").append("<style> .setprofile .progress .complete1::after { border-bottom: 13px solid transparent; } .setprofile .progress .complete2::after { border-bottom: 13px solid transparent; } .setprofile .progress .complete3::after { border-bottom: 13px solid transparent; } </style>");
}
//
////If Web Browser is Microsoft Edge.
//if (navigator.userAgent.match(/.* Edge\/([0-9.]+)$/)) {
//    $("head").append("<style> .setprofile .progress .complete1::after { margin-left: 20px } </style>");
//}
//
////If Web Browser is Opera.
//if (navigator.userAgent.toLowerCase().indexOf("op") > -1) {
//    $("head").append("<style> .setprofile .progress .complete1::after { margin-left: 17px } </style>");
//}

function accountSetupPages() {
    var height = $(window).height();
    var setprofile = $(".setprofile").height();
    var new_height = height - 50;
    
    if (setprofile < height) {
        $(".setprofile").css({
            "height": new_height + "px"
        });   
    }
}

function inputAnimate(field) {
    $("#" + field).animate({
        padding: "10px 5px"
    }, 500, function() {
        $("#" + field).css({
            "font-size": "18px"
        });
    });
}

function focusoutAnimate(field) {
    $("#" + field).animate({
        padding: "0px"
    }, 500, function() {
        $("#" + field).css({
            "font-size": "15px"
        });
    });
}