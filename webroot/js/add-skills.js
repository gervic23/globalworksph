
var skills = [];

if (skills.length == 0) {
    skill_set_empty_message();
}

$("#add-skill").on("click", function() {
    skill_add();
});

function skill_add(text = "") {
    skill_remove_empty_message();
    
    var skill = document.createElement("div");
    skill.classList.add("skills-element");

    var input = document.createElement("input");
    input.setAttribute("name", "skills[]");
    input.setAttribute("type", "text");
    input.classList.add("skill-entry");
    input.setAttribute("value", text);
    input.setAttribute("autocomplete", "off");
    input.setAttribute("placeholder", "Write Your Skill Here.");

    var del_container = document.createElement('div');
    del_container.classList.add("skill-del-btn-container");

    var del_btn = document.createElement('span');
    del_btn.textContent = "Delete";
    del_btn.addEventListener("click", function() {
        skill_delete(skills.indexOf(skill));
    });

    skill.appendChild(input);
    del_container.appendChild(del_btn);
    skill.appendChild(del_container);

    $(".skills_contents").append(skill);

    skills.push(skill);
}

function skill_delete(index) {
    var skill = skills[index];
    skills.splice(index, 1);
    document.getElementsByClassName("skills_contents")[0].removeChild(skill);
    skill_restore_empty_message();
}

function skill_set_empty_message() {
    var empty = document.createElement("div");
    empty.classList.add("skill-empty");
    empty.setAttribute("id", "skill-empty");
    empty.textContent = "Add Your Skills Here";
    
    $(".skills_contents").append(empty);
}

function skill_remove_empty_message() {
    var empty = document.getElementById("skill-empty");
    
    if (empty) {
        empty.parentNode.removeChild(empty);   
    }
}

function skill_restore_empty_message() {
    if (skills.length == 0) {
        skill_set_empty_message();
    }
}
