
var responsibilities = [];

if (responsibilities.length == 0) {
    res_set_empty_message();
}

$("#add-res").on("click", function() {
    add_res();
});

function add_res(text = "") {
    res_remove_empty_mesage();
    
    var res = document.createElement("div");
    res.classList.add("res-entry");
    
    var input = document.createElement("input");
    input.setAttribute("name", "responsibilities[]");
    input.setAttribute("type", "text");
    input.setAttribute("value", text);
    input.setAttribute("autocomplete", "off");
    input.classList.add("res");
    input.setAttribute("placeholder", "Write Responsibility Here.");
    
    var del_container = document.createElement("div");
    del_container.classList.add("res-del-btn-container");
    
    var del_btn = document.createElement("span");
    del_btn.textContent = "Delete";
    del_btn.addEventListener("click", function() {
        delete_res(responsibilities.indexOf(res));
    });
    
    res.appendChild(input);
    del_container.appendChild(del_btn);
    res.appendChild(del_container);
    
    $(".res-contents").append(res);
    
    responsibilities.push(res);
}

function delete_res(index) {
    var res = responsibilities[index];
    responsibilities.splice(index, 1);
    document.getElementsByClassName("res-contents")[0].removeChild(res);
    res_restore_empty_message();
}

function res_set_empty_message() {
    var empty = document.createElement("div");
    empty.classList.add("res-empty");
    empty.setAttribute("id", "res-empty");
    empty.textContent = "Responsibility like maintain the security of the software and etc.";
    
    $(".res-contents").append(empty);
}

function res_remove_empty_mesage() {
    var empty = document.getElementById("res-empty");
    
    if (empty) {
        empty.parentNode.removeChild(empty);
    }
}

function res_restore_empty_message() {
    if (responsibilities.length == 0) {
        res_set_empty_message();
    }
}