footerSize();
adjustMainHeader();

if ($(".employers-dashboard .left").height() > $(".employers-dashboard .right").height()) {
    var new_h = $(".employers-dashboard .left").height() + 100;

    $(".employers-dashboard .right").css({
        "height": new_h + "px"
    });
}

$(window).on("keyup click change resize input scroll",function() {
    footerSize();
    adjustMainHeader();
});

$(".main-section").on("keyup click change resize input scroll", function() {
    footersizeChangeByMainSection();
});

var dropdown_menu_bool = false;
var m_menu_bool = false;

$(".dropdown").on("click", function() {
    if (!dropdown_menu_bool) {
        dropdown_menu_bool = true;
        $(this).css({
            "color": "#e68c8c"
        });
        
        $(".dropdown-menu").fadeIn(500);
    } else {
        dropdown_menu_bool = false;
        $(this).css({
            "color": "#fff"
        });
        $(".dropdown-menu").fadeOut(500);
    }
});

$(".m-dropdown").on("click", function() {
    if (!m_menu_bool) {
        m_menu_bool = true;
        $(this).css({
            "color": "#e68c8c"
        });
        
        $(".m-dropdown-menu").fadeIn(500);
    } else {
        m_menu_bool = false;
        $(this).css({
            "color": "#fff"
        });
        $(".m-dropdown-menu").fadeOut(500);
    }
});

$(window).on("click", function(event) {
    if (!event.target.matches(".dropdown") && !event.target.matches(".m-dropdown")) {
        dropdown_menu_bool = false;
        m_menu_bool = false;
        $(this).css({
            "color": "#fff"
        });
        $(".dropdown, .m-dropdown").css({
            "color": "#fff"
        });
        $(".dropdown-menu, .m-dropdown-menu").fadeOut(500);
    }
});

function footerSize() {
    if ($(window).width() > 420) {
        var sec_height = $(".main-section").innerHeight();
        var footer_height = $(".main-footer").innerHeight();
        var win_height = sec_height + footer_height;

        $("body").css({
            "height": win_height + "px"
        });
    }
}

function footersizeChangeByMainSection() {
    if ($(window).width() > 420) {
        var sec_height = $(".main-section").innerHeight();
        var footer_height = $(".main-footer").innerHeight();
        var win_height = sec_height + footer_height;

        $("body").css({
            "height": win_height + "px"
        });
    }
}

function adjustMainHeader() {
    var width = $(window).width();
    var new_width = width - 10;

    $(".main-header").css({
        "width": new_width + "px"
    });
}