# Globalworks
Written in CakePHP 3.7. is a job portal website where users can search for a job or users can post jobs.

# System Requirements.
*   Xampp with PHP 7.0 to 7.2.
*   Mercury Mail for Email System.
*   Clone this project to C:\xampp\htdocs folder.
```bash
git clone https://gervic23@bitbucket.org/gervic23/globalworksph.git
```
*   Import globalworks.sql to your database.
*   Open with notepad C:\Windows\System32\drivers\etc\host and add "127.0.0.1 globalworks.local"
*   Open with notepad C:\xampp\apache\conf\extra\httpd-vhosts.conf and add the following code.

```bash
    <VirtualHost *:80>
        ServerAdmin webmaster@dummy-host.example.com
        DocumentRoot "C:/xampp/htdocs/globalworksph"
        ServerName globalworks.local
        ServerAlias www.globalworks.local
    </VirtualHost>
```
*   Open /config/app.php and change your Database Credentials.
*   Restart Apache Server.
*   Create "sessions" directory in /tmp if does not exists.

# User Accounts
## Super Admin's
```bash
    Username: admin@localhost.com
    Password: thenewboston
```
## Candidate's Account
```bash
    Username: yuel@localhost.com
    Password: thenewboston
```

## Employer's Account
```bash
    Username: victor@localhost.com
    Password: thenewboston
```
